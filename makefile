all:main3.cpp
	g++ -O3 -w -std=c++20 -funroll-loops -mavx512f -mfma -march=native -S main3.cpp
	g++ main3.s -o main3
	./main3.exe

demo:demo.cpp d2sci.cpp
	g++ -O3 -w -std=c++20 -funroll-loops -mavx512f -mfma -march=native -lpthread demo.cpp d2sci.cpp -o demo

test:test.cpp
	g++ -mavx512f -mfma -march=native test.cpp -o test
	./test
test2:test2.cpp
	g++ -mavx512f -mfma -march=native test2.cpp -o test2
	./test2