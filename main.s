	.file	"main.cpp"
	.text
	.section	.text$_ZNKSt5ctypeIcE8do_widenEc,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt5ctypeIcE8do_widenEc
	.def	_ZNKSt5ctypeIcE8do_widenEc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt5ctypeIcE8do_widenEc
_ZNKSt5ctypeIcE8do_widenEc:
.LFB2269:
	.seh_endprologue
	movl	%edx, %eax
	ret
	.seh_endproc
	.section	.text$_ZNSt8__format5_SinkIcE10_M_reserveEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format5_SinkIcE10_M_reserveEy
	.def	_ZNSt8__format5_SinkIcE10_M_reserveEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format5_SinkIcE10_M_reserveEy
_ZNSt8__format5_SinkIcE10_M_reserveEy:
.LFB14852:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	24(%rcx), %rax
	movq	%rdx, %rsi
	movq	16(%rcx), %rdx
	movq	%rcx, %rbx
	subq	8(%rcx), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jnb	.L5
	cmpq	%rsi, %rdx
	jb	.L6
	movq	(%rbx), %r8
	movq	%rbx, %rcx
	call	*(%r8)
	movq	24(%rbx), %rdx
	movq	16(%rbx), %r9
	subq	8(%rbx), %rdx
	subq	%rdx, %r9
	cmpq	%rsi, %r9
	jb	.L6
.L5:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L6:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.seh_endproc
	.section	.text$_ZNSt8__format5_SinkIcE7_M_bumpEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format5_SinkIcE7_M_bumpEy
	.def	_ZNSt8__format5_SinkIcE7_M_bumpEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format5_SinkIcE7_M_bumpEy
_ZNSt8__format5_SinkIcE7_M_bumpEy:
.LFB15031:
	.seh_endprologue
	addq	%rdx, 24(%rcx)
	ret
	.seh_endproc
	.text
	.p2align 4
	.def	__tcf_0;	.scl	3;	.type	32;	.endef
	.seh_proc	__tcf_0
__tcf_0:
.LFB15132:
	.seh_endprologue
	movq	all_info(%rip), %rcx
	testq	%rcx, %rcx
	je	.L11
	movq	16+all_info(%rip), %rdx
	subq	%rcx, %rdx
	jmp	_ZdlPvy
	.p2align 4
	.p2align 3
.L11:
	ret
	.seh_endproc
	.section	.text$_ZNSt12format_errorD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt12format_errorD1Ev
	.def	_ZNSt12format_errorD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt12format_errorD1Ev
_ZNSt12format_errorD1Ev:
.LFB5227:
	.seh_endprologue
	leaq	16+_ZTVSt12format_error(%rip), %rax
	movq	%rax, (%rcx)
	jmp	_ZNSt13runtime_errorD2Ev
	.seh_endproc
	.section	.text$_ZNSt12format_errorD0Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt12format_errorD0Ev
	.def	_ZNSt12format_errorD0Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt12format_errorD0Ev
_ZNSt12format_errorD0Ev:
.LFB5228:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	leaq	16+_ZTVSt12format_error(%rip), %rax
	movq	%rcx, %rbx
	movq	%rax, (%rcx)
	call	_ZNSt13runtime_errorD2Ev
	movl	$16, %edx
	movq	%rbx, %rcx
	addq	$32, %rsp
	popq	%rbx
	jmp	_ZdlPvy
	.seh_endproc
	.section .rdata,"dr"
.LC0:
	.ascii "%.16le\0"
	.text
	.p2align 4
	.def	_Z7sprintfPcPKcz.constprop.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_Z7sprintfPcPKcz.constprop.0
_Z7sprintfPcPKcz.constprop.0:
.LFB15903:
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	leaq	.LC0(%rip), %rdx
	movq	%r8, 80(%rsp)
	leaq	80(%rsp), %r8
	movq	%r9, 88(%rsp)
	movq	%r8, 40(%rsp)
	call	__mingw_vsprintf
	addq	$56, %rsp
	ret
	.seh_endproc
	.section	.text$_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv
	.def	_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv
_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv:
.LFB15345:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	24(%rcx), %rbp
	movq	8(%rcx), %rsi
	movq	296(%rcx), %rax
	movq	%rcx, %rdi
	movq	%rbp, %r12
	subq	%rsi, %r12
	testq	%rax, %rax
	js	.L72
	movq	304(%rcx), %r13
	cmpq	%rax, %r13
	jb	.L73
.L20:
	addq	%r12, %r13
	movq	%rsi, 24(%rdi)
	movq	%r13, 304(%rdi)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L73:
	subq	%r13, %rax
	movq	288(%rcx), %rbx
	cmpq	%r12, %rax
	cmova	%r12, %rax
	testq	%rax, %rax
	jle	.L21
	leaq	(%rsi,%rax), %r13
	andl	$3, %eax
	je	.L23
	cmpq	$1, %rax
	je	.L58
	cmpq	$2, %rax
	je	.L59
	movq	24(%rbx), %r8
	movzbl	(%rsi), %edx
	leaq	1(%r8), %rcx
	movq	%rcx, 24(%rbx)
	movb	%dl, (%r8)
	movq	24(%rbx), %r9
	subq	8(%rbx), %r9
	cmpq	16(%rbx), %r9
	je	.L74
.L41:
	incq	%rsi
.L59:
	movq	24(%rbx), %rbp
	movzbl	(%rsi), %r11d
	leaq	1(%rbp), %rax
	movq	%rax, 24(%rbx)
	movb	%r11b, 0(%rbp)
	movq	24(%rbx), %rdx
	subq	8(%rbx), %rdx
	cmpq	16(%rbx), %rdx
	je	.L75
.L44:
	incq	%rsi
.L58:
	movq	24(%rbx), %r10
	movzbl	(%rsi), %r9d
	leaq	1(%r10), %rcx
	movq	%rcx, 24(%rbx)
	movb	%r9b, (%r10)
	movq	24(%rbx), %r11
	subq	8(%rbx), %r11
	cmpq	16(%rbx), %r11
	je	.L76
.L47:
	incq	%rsi
	cmpq	%r13, %rsi
	je	.L71
.L23:
	movq	24(%rbx), %rax
	movzbl	(%rsi), %edx
	leaq	1(%rax), %r8
	movq	%r8, 24(%rbx)
	movb	%dl, (%rax)
	movq	24(%rbx), %r9
	subq	8(%rbx), %r9
	cmpq	16(%rbx), %r9
	je	.L77
.L22:
	movq	24(%rbx), %r11
	leaq	1(%rsi), %rbp
	movzbl	1(%rsi), %esi
	leaq	1(%r11), %rcx
	movq	%rcx, 24(%rbx)
	movb	%sil, (%r11)
	movq	24(%rbx), %rdx
	subq	8(%rbx), %rdx
	cmpq	16(%rbx), %rdx
	je	.L78
.L50:
	movq	24(%rbx), %r9
	movzbl	1(%rbp), %r8d
	leaq	1(%r9), %r10
	movq	%r10, 24(%rbx)
	movb	%r8b, (%r9)
	movq	24(%rbx), %rsi
	subq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L79
.L52:
	movq	24(%rbx), %rax
	movzbl	2(%rbp), %edx
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%rbx)
	movb	%dl, (%rax)
	movq	24(%rbx), %r8
	subq	8(%rbx), %r8
	cmpq	16(%rbx), %r8
	je	.L80
.L54:
	leaq	3(%rbp), %rsi
	cmpq	%r13, %rsi
	jne	.L23
.L71:
	movq	8(%rdi), %rsi
	movq	304(%rdi), %r13
.L21:
	movq	%rbx, 288(%rdi)
	jmp	.L20
	.p2align 4
	.p2align 3
.L72:
	movq	288(%rcx), %rbx
	testq	%r12, %r12
	jle	.L17
	movq	%r12, %r10
	andl	$3, %r10d
	je	.L19
	cmpq	$1, %r10
	je	.L56
	cmpq	$2, %r10
	je	.L57
	movq	24(%rbx), %rdx
	movzbl	(%rsi), %r11d
	leaq	1(%rdx), %rax
	movq	%rax, 24(%rbx)
	movb	%r11b, (%rdx)
	movq	24(%rbx), %rcx
	subq	8(%rbx), %rcx
	cmpq	16(%rbx), %rcx
	je	.L81
.L26:
	incq	%rsi
.L57:
	movq	24(%rbx), %r13
	movzbl	(%rsi), %r9d
	leaq	1(%r13), %r10
	movq	%r10, 24(%rbx)
	movb	%r9b, 0(%r13)
	movq	24(%rbx), %r11
	subq	8(%rbx), %r11
	cmpq	16(%rbx), %r11
	je	.L82
.L29:
	incq	%rsi
.L56:
	movq	24(%rbx), %rax
	movzbl	(%rsi), %r8d
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%rbx)
	movb	%r8b, (%rax)
	movq	24(%rbx), %r9
	subq	8(%rbx), %r9
	cmpq	16(%rbx), %r9
	je	.L83
.L32:
	incq	%rsi
	cmpq	%rsi, %rbp
	je	.L70
.L19:
	movq	24(%rbx), %r11
	movzbl	(%rsi), %r10d
	leaq	1(%r11), %rdx
	movq	%rdx, 24(%rbx)
	movb	%r10b, (%r11)
	movq	24(%rbx), %r8
	subq	8(%rbx), %r8
	cmpq	16(%rbx), %r8
	je	.L84
.L18:
	movq	24(%rbx), %r9
	leaq	1(%rsi), %r13
	movzbl	1(%rsi), %esi
	leaq	1(%r9), %rcx
	movq	%rcx, 24(%rbx)
	movb	%sil, (%r9)
	movq	24(%rbx), %r10
	subq	8(%rbx), %r10
	cmpq	16(%rbx), %r10
	je	.L85
.L35:
	movq	24(%rbx), %r8
	movzbl	1(%r13), %edx
	leaq	1(%r8), %rax
	movq	%rax, 24(%rbx)
	movb	%dl, (%r8)
	movq	24(%rbx), %rsi
	subq	8(%rbx), %rsi
	cmpq	16(%rbx), %rsi
	je	.L86
.L37:
	movq	24(%rbx), %r11
	movzbl	2(%r13), %r10d
	leaq	1(%r11), %rcx
	movq	%rcx, 24(%rbx)
	movb	%r10b, (%r11)
	movq	24(%rbx), %rdx
	subq	8(%rbx), %rdx
	cmpq	16(%rbx), %rdx
	je	.L87
.L39:
	leaq	3(%r13), %rsi
	cmpq	%rsi, %rbp
	jne	.L19
.L70:
	movq	8(%rdi), %rsi
.L17:
	movq	304(%rdi), %r13
	movq	%rbx, 288(%rdi)
	jmp	.L20
.L74:
	movq	(%rbx), %r10
	movq	%rbx, %rcx
	call	*(%r10)
	jmp	.L41
	.p2align 4
	.p2align 3
.L84:
	movq	(%rbx), %rax
	movq	%rbx, %rcx
	call	*(%rax)
	jmp	.L18
	.p2align 4
	.p2align 3
.L85:
	movq	(%rbx), %r11
	movq	%rbx, %rcx
	call	*(%r11)
	jmp	.L35
	.p2align 4
	.p2align 3
.L86:
	movq	(%rbx), %r9
	movq	%rbx, %rcx
	call	*(%r9)
	jmp	.L37
	.p2align 4
	.p2align 3
.L87:
	movq	(%rbx), %r8
	movq	%rbx, %rcx
	call	*(%r8)
	jmp	.L39
	.p2align 4
	.p2align 3
.L77:
	movq	(%rbx), %r10
	movq	%rbx, %rcx
	call	*(%r10)
	jmp	.L22
	.p2align 4
	.p2align 3
.L80:
	movq	(%rbx), %r9
	movq	%rbx, %rcx
	call	*(%r9)
	jmp	.L54
	.p2align 4
	.p2align 3
.L78:
	movq	(%rbx), %rax
	movq	%rbx, %rcx
	call	*(%rax)
	jmp	.L50
	.p2align 4
	.p2align 3
.L79:
	movq	(%rbx), %r11
	movq	%rbx, %rcx
	call	*(%r11)
	jmp	.L52
.L83:
	movq	(%rbx), %r13
	movq	%rbx, %rcx
	call	*0(%r13)
	jmp	.L32
.L76:
	movq	(%rbx), %rbp
	movq	%rbx, %rcx
	call	*0(%rbp)
	jmp	.L47
.L82:
	movq	(%rbx), %rdx
	movq	%rbx, %rcx
	call	*(%rdx)
	jmp	.L29
.L75:
	movq	(%rbx), %r8
	movq	%rbx, %rcx
	call	*(%r8)
	jmp	.L44
.L81:
	movq	(%rbx), %r8
	movq	%rbx, %rcx
	call	*(%r8)
	jmp	.L26
	.seh_endproc
	.section	.text$_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc
	.def	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc
_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc:
.LFB15250:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	8(%rcx), %rbp
	movq	48(%rcx), %r12
	subq	%rbp, %rdx
	movq	16(%r12), %rsi
	movq	%rdx, %rdi
	jne	.L102
.L89:
	movq	%rsi, 16(%r12)
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L102:
	movq	24(%rsi), %rcx
	movq	16(%rsi), %rbx
	movq	%rcx, %rax
	subq	8(%rsi), %rax
	subq	%rax, %rbx
	cmpq	%rbx, %rdx
	jnb	.L94
	jmp	.L90
	.p2align 4
	.p2align 3
.L104:
	movq	%rbp, %rdx
	call	memcpy
	addq	%rbx, 24(%rsi)
.L101:
	movq	(%rsi), %rdx
	movq	%rsi, %rcx
	addq	%rbx, %rbp
	subq	%rbx, %rdi
	call	*(%rdx)
	movq	24(%rsi), %rcx
	movq	16(%rsi), %rbx
	movq	%rcx, %r8
	subq	8(%rsi), %r8
	subq	%r8, %rbx
	cmpq	%rbx, %rdi
	jb	.L103
.L94:
	cmpq	%rdi, %rbx
	movq	%rdi, %r8
	cmovbe	%rbx, %r8
	testq	%r8, %r8
	jne	.L104
	addq	%rbx, %rcx
	movq	%rcx, 24(%rsi)
	jmp	.L101
	.p2align 4
	.p2align 3
.L103:
	testq	%rdi, %rdi
	je	.L89
.L90:
	movq	%rdi, %r8
	movq	%rbp, %rdx
	call	memcpy
	addq	%rdi, 24(%rsi)
	movq	%rsi, 16(%r12)
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.seh_endproc
	.section .rdata,"dr"
.LC1:
	.ascii "basic_string::_M_replace_aux\0"
.LC2:
	.ascii "basic_string::_M_create\0"
	.text
	.align 2
	.p2align 4
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0:
.LFB15906:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	movq	8(%rcx), %r13
	movq	%rdx, %rbp
	movabsq	$9223372036854775807, %rdx
	movl	176(%rsp), %r14d
	addq	%r8, %rdx
	movq	%rcx, %rbx
	movq	%r8, %rax
	movq	%r9, %rdi
	subq	%r13, %rdx
	cmpq	%r9, %rdx
	jb	.L144
	movq	(%rcx), %rsi
	movq	%r9, %r12
	leaq	16(%rcx), %r9
	subq	%r8, %r12
	addq	%r13, %r12
	cmpq	%r9, %rsi
	je	.L145
	movq	16(%rcx), %r15
	cmpq	%r12, %r15
	jb	.L146
.L108:
	leaq	(%rax,%rbp), %rdx
	subq	%rdx, %r13
	je	.L112
	cmpq	%rdi, %rax
	je	.L112
	addq	%rbp, %rsi
	leaq	(%rsi,%rax), %rdx
	leaq	(%rsi,%rdi), %rcx
	cmpq	$1, %r13
	je	.L147
	movq	%r13, %r8
	call	memmove
	movq	(%rbx), %rsi
.L112:
	testq	%rdi, %rdi
	je	.L123
.L152:
	leaq	(%rsi,%rbp), %rcx
	cmpq	$1, %rdi
	je	.L148
	movsbl	%r14b, %edx
	movq	%rdi, %r8
	call	memset
	movq	(%rbx), %rsi
.L123:
	movq	%r12, 8(%rbx)
	movb	$0, (%rsi,%r12)
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L146:
	testq	%r12, %r12
	js	.L109
	addq	%rbp, %rax
	addq	%r15, %r15
	subq	%rax, %r13
	movq	%rax, 48(%rsp)
	cmpq	%r15, %r12
	jb	.L149
.L115:
	movq	%r12, %rcx
	movq	%r12, %r15
	incq	%rcx
	js	.L116
.L117:
	movq	%r9, 40(%rsp)
	call	_Znwy
	testq	%rbp, %rbp
	movq	(%rbx), %r11
	movq	40(%rsp), %r8
	movq	%rax, %rsi
	je	.L118
	cmpq	$1, %rbp
	je	.L150
	movq	%r11, %rdx
	movq	%r8, 56(%rsp)
	movq	%rax, %rcx
	movq	%rbp, %r8
	movq	%r11, 40(%rsp)
	call	memcpy
	movq	56(%rsp), %r8
	movq	40(%rsp), %r11
.L118:
	testq	%r13, %r13
	jne	.L151
.L120:
	cmpq	%r11, %r8
	je	.L122
	movq	16(%rbx), %rax
	movq	%r11, %rcx
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L122:
	movq	%rsi, (%rbx)
	movq	%r15, 16(%rbx)
	testq	%rdi, %rdi
	je	.L123
	jmp	.L152
	.p2align 4
	.p2align 3
.L151:
	movq	48(%rsp), %rdx
	leaq	(%rdi,%rbp), %rcx
	addq	%rsi, %rcx
	addq	%r11, %rdx
	cmpq	$1, %r13
	je	.L153
	movq	%r8, 40(%rsp)
	movq	%r13, %r8
	call	memcpy
	movq	(%rbx), %r11
	movq	40(%rsp), %r8
	jmp	.L120
	.p2align 4
	.p2align 3
.L149:
	leaq	1(%r15), %rcx
	testq	%r15, %r15
	jns	.L117
.L116:
	call	_ZSt17__throw_bad_allocv
	.p2align 4
	.p2align 3
.L145:
	cmpq	$15, %r12
	jbe	.L108
	testq	%r12, %r12
	js	.L109
	addq	%rbp, %rax
	subq	%rax, %r13
	movq	%rax, 48(%rsp)
	cmpq	$29, %r12
	ja	.L115
	movl	$31, %ecx
	movl	$30, %r15d
	jmp	.L117
	.p2align 4
	.p2align 3
.L148:
	movb	%r14b, (%rcx)
	movq	(%rbx), %rsi
	jmp	.L123
	.p2align 4
	.p2align 3
.L147:
	movzbl	(%rdx), %esi
	movb	%sil, (%rcx)
	movq	(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L123
	jmp	.L152
	.p2align 4
	.p2align 3
.L153:
	movzbl	(%rdx), %r10d
	movb	%r10b, (%rcx)
	movq	(%rbx), %r11
	jmp	.L120
	.p2align 4
	.p2align 3
.L150:
	movzbl	(%r11), %ecx
	movb	%cl, (%rax)
	jmp	.L118
.L109:
	leaq	.LC2(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L144:
	leaq	.LC1(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy
	.def	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy
_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy:
.LFB15404:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	24(%rcx), %r9
	movq	%rcx, %rbx
	subq	8(%rcx), %r9
	addq	%rdx, %r9
	movq	296(%rcx), %rdx
	cmpq	%r9, %rdx
	jb	.L157
	cmpq	%rdx, %r9
	jnb	.L156
	movq	288(%rcx), %rax
	movq	%r9, 296(%rcx)
	movb	$0, (%rax,%r9)
.L156:
	leaq	32(%rbx), %rdx
	movq	$256, 16(%rbx)
	movq	%rdx, 8(%rbx)
	movq	%rdx, 24(%rbx)
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4
	.p2align 3
.L157:
	leaq	288(%rcx), %rcx
	subq	%rdx, %r9
	xorl	%r8d, %r8d
	movl	$0, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	jmp	.L156
	.seh_endproc
	.section	.text$_Z6printfPKcz,"x"
	.linkonce discard
	.p2align 4
	.globl	_Z6printfPKcz
	.def	_Z6printfPKcz;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z6printfPKcz
_Z6printfPKcz:
.LFB405:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$64, %rsp
	.seh_stackalloc	64
	.seh_endprologue
	movq	%r8, 96(%rsp)
	leaq	88(%rsp), %r8
	movq	%rcx, %rbx
	movq	%rdx, 88(%rsp)
	movq	%r9, 104(%rsp)
	movq	%r8, 56(%rsp)
	movq	%r8, 40(%rsp)
	movl	$1, %ecx
	call	*__imp___acrt_iob_func(%rip)
	movq	40(%rsp), %r8
	movq	%rbx, %rdx
	movq	%rax, %rcx
	call	__mingw_vfprintf
	addq	$64, %rsp
	popq	%rbx
	ret
	.seh_endproc
	.section	.text$_Z7sprintfPcPKcz,"x"
	.linkonce discard
	.p2align 4
	.globl	_Z7sprintfPcPKcz
	.def	_Z7sprintfPcPKcz;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z7sprintfPcPKcz
_Z7sprintfPcPKcz:
.LFB406:
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	%r8, 80(%rsp)
	leaq	80(%rsp), %r8
	movq	%r9, 88(%rsp)
	movq	%r8, 40(%rsp)
	call	__mingw_vsprintf
	addq	$56, %rsp
	ret
	.seh_endproc
	.section	.text$_ZSt20__throw_format_errorPKc,"x"
	.linkonce discard
	.globl	_ZSt20__throw_format_errorPKc
	.def	_ZSt20__throw_format_errorPKc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZSt20__throw_format_errorPKc
_ZSt20__throw_format_errorPKc:
.LFB5224:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rcx, %rsi
	movl	$16, %ecx
	call	__cxa_allocate_exception
	movq	%rsi, %rdx
	movq	%rax, %rcx
	movq	%rax, %rbx
.LEHB0:
	call	_ZNSt13runtime_errorC2EPKc
.LEHE0:
	leaq	16+_ZTVSt12format_error(%rip), %rax
	leaq	_ZNSt12format_errorD1Ev(%rip), %r8
	leaq	_ZTISt12format_error(%rip), %rdx
	movq	%rbx, %rcx
	movq	%rax, (%rbx)
.LEHB1:
	call	__cxa_throw
.L162:
	movq	%rax, %rsi
	movq	%rbx, %rcx
	vzeroupper
	call	__cxa_free_exception
	movq	%rsi, %rcx
	call	_Unwind_Resume
	nop
.LEHE1:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA5224:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5224-.LLSDACSB5224
.LLSDACSB5224:
	.uleb128 .LEHB0-.LFB5224
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L162-.LFB5224
	.uleb128 0
	.uleb128 .LEHB1-.LFB5224
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE5224:
	.section	.text$_ZSt20__throw_format_errorPKc,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC3:
	.ascii "format error: unmatched '{' in format string\0"
	.section	.text$_ZNSt8__format39__unmatched_left_brace_in_format_stringEv,"x"
	.linkonce discard
	.globl	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
	.def	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
_ZNSt8__format39__unmatched_left_brace_in_format_stringEv:
.LFB5229:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	.LC3(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC4:
	.ascii "format error: conflicting indexing style in format string\0"
	.section	.text$_ZNSt8__format39__conflicting_indexing_in_format_stringEv,"x"
	.linkonce discard
	.globl	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
	.def	_ZNSt8__format39__conflicting_indexing_in_format_stringEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
_ZNSt8__format39__conflicting_indexing_in_format_stringEv:
.LFB5231:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	.LC4(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC5:
	.ascii "format error: invalid arg-id in format string\0"
	.section	.text$_ZNSt8__format33__invalid_arg_id_in_format_stringEv,"x"
	.linkonce discard
	.globl	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.def	_ZNSt8__format33__invalid_arg_id_in_format_stringEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
_ZNSt8__format33__invalid_arg_id_in_format_stringEv:
.LFB5232:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	.LC5(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC6:
	.ascii "format error: argument used for width or precision must be a non-negative integer\0"
	.text
	.align 2
	.p2align 4
	.def	_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0
_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0:
.LFB15909:
	subq	$104, %rsp
	.seh_stackalloc	104
	.seh_endprologue
	movzbl	(%rdx), %eax
	movzwl	%cx, %ecx
	movl	%eax, %r8d
	andl	$15, %eax
	andl	$15, %r8d
	cmpq	%rax, %rcx
	jnb	.L167
	leaq	(%rcx,%rcx,4), %r8
	movl	$4, %eax
	shrx	%rax, (%rdx), %r9
	salq	$4, %rcx
	shrx	%r8, %r9, %r11
	addq	8(%rdx), %rcx
	andl	$31, %r11d
	vmovdqa	(%rcx), %xmm1
	vmovdqa	%xmm1, 64(%rsp)
.L168:
	leaq	.L172(%rip), %rdx
	movzbl	%r11b, %ecx
	movb	%r11b, 80(%rsp)
	vmovdqu	64(%rsp), %ymm0
	movslq	(%rdx,%rcx,4), %r10
	addq	%rdx, %r10
	vmovdqu	%ymm0, 32(%rsp)
	jmp	*%r10
	.section .rdata,"dr"
	.align 4
.L172:
	.long	.L179-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L176-.L172
	.long	.L175-.L172
	.long	.L174-.L172
	.long	.L173-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.long	.L177-.L172
	.text
	.p2align 4
	.p2align 3
.L167:
	testb	%r8b, %r8b
	jne	.L169
	movl	$4, %r9d
	shrx	%r9, (%rdx), %r10
	cmpq	%r10, %rcx
	jnb	.L169
	salq	$5, %rcx
	addq	8(%rdx), %rcx
	vmovdqu	(%rcx), %xmm2
	movzbl	16(%rcx), %r11d
	vmovdqa	%xmm2, 64(%rsp)
	jmp	.L168
	.p2align 4
	.p2align 3
.L174:
	movq	32(%rsp), %rax
	testq	%rax, %rax
	js	.L177
.L166:
	vzeroupper
	addq	$104, %rsp
	ret
	.p2align 4
	.p2align 3
.L175:
	movl	32(%rsp), %eax
	vzeroupper
	addq	$104, %rsp
	ret
	.p2align 4
	.p2align 3
.L176:
	movslq	32(%rsp), %rax
	testl	%eax, %eax
	jns	.L166
.L177:
	leaq	.LC6(%rip), %rcx
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L173:
	movq	32(%rsp), %rax
	vzeroupper
	addq	$104, %rsp
	ret
	.p2align 4
	.p2align 3
.L179:
	vzeroupper
.L169:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC7:
	.ascii "format error: failed to parse format-spec\0"
	.section	.text$_ZNSt8__format29__failed_to_parse_format_specEv,"x"
	.linkonce discard
	.globl	_ZNSt8__format29__failed_to_parse_format_specEv
	.def	_ZNSt8__format29__failed_to_parse_format_specEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format29__failed_to_parse_format_specEv
_ZNSt8__format29__failed_to_parse_format_specEv:
.LFB5233:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	leaq	.LC7(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.text
	.p2align 4
	.globl	_Z8getcyclev
	.def	_Z8getcyclev;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z8getcyclev
_Z8getcyclev:
.LFB13443:
	.seh_endprologue
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	salq	$32, %rdx
	orq	%rdx, %rax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z11mult64to128yyPyS_
	.def	_Z11mult64to128yyPyS_;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z11mult64to128yyPyS_
_Z11mult64to128yyPyS_:
.LFB13455:
	pushq	%rbx
	.seh_pushreg	%rbx
	.seh_endprologue
	movl	%ecx, %eax
	movl	%edx, %r10d
	movq	%rax, %r11
	imulq	%r10, %r11
	shrq	$32, %rcx
	shrq	$32, %rdx
	imulq	%rcx, %r10
	movq	%r11, %rbx
	shrq	$32, %rbx
	imulq	%rdx, %rax
	addq	%rbx, %r10
	movl	%r10d, %ebx
	imulq	%rdx, %rcx
	shrq	$32, %r10
	addq	%rbx, %rax
	addq	%r10, %rcx
	movq	%rax, %rdx
	shrq	$32, %rdx
	salq	$32, %rax
	addq	%rdx, %rcx
	movq	%rcx, (%r8)
	movl	%r11d, %ecx
	addq	%rcx, %rax
	movq	%rax, (%r9)
	popq	%rbx
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z8fast_expd
	.def	_Z8fast_expd;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z8fast_expd
_Z8fast_expd:
.LFB13457:
	.seh_endprologue
	vdivsd	.LC8(%rip), %xmm0, %xmm0
	vaddsd	.LC9(%rip), %xmm0, %xmm1
	vmulsd	.LC10(%rip), %xmm1, %xmm2
	vcvttsd2sil	%xmm2, %eax
	salq	$32, %rax
	vmovq	%rax, %xmm0
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z15double_to_int64d
	.def	_Z15double_to_int64d;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z15double_to_int64d
_Z15double_to_int64d:
.LFB13458:
	.seh_endprologue
	vmovsd	.LC12(%rip), %xmm1
	vaddsd	%xmm1, %xmm0, %xmm0
	vsubsd	%xmm1, %xmm0, %xmm2
	vcvttsd2siq	%xmm2, %rax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z11getexponentd
	.def	_Z11getexponentd;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z11getexponentd
_Z11getexponentd:
.LFB13459:
	.seh_endprologue
	vmovq	%xmm0, %rax
	shrq	$52, %rax
	andl	$2047, %eax
	subl	$1023, %eax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z7getfracd
	.def	_Z7getfracd;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z7getfracd
_Z7getfracd:
.LFB13460:
	.seh_endprologue
	movabsq	$4503599627370495, %rax
	vmovq	%xmm0, %rdx
	andq	%rdx, %rax
	btsq	$52, %rax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z11getfracnot1d
	.def	_Z11getfracnot1d;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z11getfracnot1d
_Z11getfracnot1d:
.LFB13461:
	.seh_endprologue
	movabsq	$4503599627370495, %rax
	vmovq	%xmm0, %rdx
	andq	%rdx, %rax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z4getbd
	.def	_Z4getbd;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z4getbd
_Z4getbd:
.LFB13467:
	.seh_endprologue
	vxorpd	%xmm1, %xmm1, %xmm1
	vucomisd	%xmm1, %xmm0
	jp	.L192
	movl	$0, %eax
	jne	.L192
	ret
	.p2align 4
	.p2align 3
.L192:
	vmovq	%xmm0, %rax
	vandpd	.LC13(%rip), %xmm0, %xmm2
	vxorps	%xmm0, %xmm0, %xmm0
	leaq	_ZL5_10en(%rip), %rcx
	shrq	$52, %rax
	andl	$2047, %eax
	subl	$1023, %eax
	vcomisd	.LC15(%rip), %xmm2
	vcvtsi2sdl	%eax, %xmm0, %xmm3
	vmulsd	.LC14(%rip), %xmm3, %xmm4
	vcvttsd2sil	%xmm4, %eax
	sbbl	$-1, %eax
	leal	324(%rax), %edx
	xorl	%r9d, %r9d
	movslq	%edx, %r8
	vmovsd	(%rcx,%r8,8), %xmm5
	vcomisd	%xmm2, %xmm5
	seta	%r9b
	subl	%r9d, %eax
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z8getpow10i
	.def	_Z8getpow10i;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z8getpow10i
_Z8getpow10i:
.LFB13468:
	.seh_endprologue
	addl	$324, %ecx
	leaq	_ZL5_10en(%rip), %rax
	movslq	%ecx, %rcx
	vmovsd	(%rax,%rcx,8), %xmm0
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z19getpow10n_mul_pow2niii
	.def	_Z19getpow10n_mul_pow2niii;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z19getpow10n_mul_pow2niii
_Z19getpow10n_mul_pow2niii:
.LFB13469:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$88, %rsp
	.seh_stackalloc	88
	vmovaps	%xmm6, 32(%rsp)
	.seh_savexmm	%xmm6, 32
	vmovaps	%xmm7, 48(%rsp)
	.seh_savexmm	%xmm7, 48
	vmovaps	%xmm8, 64(%rsp)
	.seh_savexmm	%xmm8, 64
	.seh_endprologue
	vxorps	%xmm7, %xmm7, %xmm7
	vmovsd	.LC17(%rip), %xmm8
	vcvtsi2sdl	%edx, %xmm7, %xmm1
	movl	%ecx, %ebx
	movl	%edx, %esi
	vucomisd	.LC16(%rip), %xmm1
	jnb	.L197
.L195:
	vcvtsi2sdl	%ebx, %xmm7, %xmm1
	vmovsd	.LC18(%rip), %xmm0
	call	pow
	addl	%esi, %ebx
	vmovapd	%xmm0, %xmm6
	vmovapd	%xmm8, %xmm0
	vcvtsi2sdl	%ebx, %xmm7, %xmm1
	call	pow
	nop
	vmovaps	48(%rsp), %xmm7
	vmovaps	64(%rsp), %xmm8
	vmulsd	%xmm0, %xmm6, %xmm0
	vmovaps	32(%rsp), %xmm6
	addq	$88, %rsp
	popq	%rbx
	popq	%rsi
	ret
.L197:
	vmovapd	%xmm8, %xmm0
	call	pow
	jmp	.L195
	.seh_endproc
	.p2align 4
	.globl	_Z9info_initv
	.def	_Z9info_initv;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z9info_initv
_Z9info_initv:
.LFB13517:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$216, %rsp
	.seh_stackalloc	216
	vmovaps	%xmm6, 48(%rsp)
	.seh_savexmm	%xmm6, 48
	vmovaps	%xmm7, 64(%rsp)
	.seh_savexmm	%xmm7, 64
	vmovaps	%xmm8, 80(%rsp)
	.seh_savexmm	%xmm8, 80
	vmovaps	%xmm9, 96(%rsp)
	.seh_savexmm	%xmm9, 96
	vmovaps	%xmm10, 112(%rsp)
	.seh_savexmm	%xmm10, 112
	vmovaps	%xmm11, 128(%rsp)
	.seh_savexmm	%xmm11, 128
	vmovaps	%xmm12, 144(%rsp)
	.seh_savexmm	%xmm12, 144
	vmovaps	%xmm13, 160(%rsp)
	.seh_savexmm	%xmm13, 160
	vmovaps	%xmm14, 176(%rsp)
	.seh_savexmm	%xmm14, 176
	vmovaps	%xmm15, 192(%rsp)
	.seh_savexmm	%xmm15, 192
	.seh_endprologue
	vxorps	%xmm6, %xmm6, %xmm6
	vxorpd	%xmm7, %xmm7, %xmm7
	vmovsd	.LC14(%rip), %xmm8
	vmovsd	.LC16(%rip), %xmm11
	vmovsd	.LC17(%rip), %xmm9
	vmovsd	.LC18(%rip), %xmm10
	movq	all_info(%rip), %rdi
	vmovq	.LC19(%rip), %xmm12
	xorl	%r13d, %r13d
	movl	$-52, %edx
	movl	$16, %r15d
	leaq	65408(%rdi), %rbx
	jmp	.L203
	.p2align 4
	.p2align 3
.L217:
	vmovd	.LC20(%rip), %xmm4
	decl	%r15d
	movl	%esi, (%rbx)
	vpinsrd	$1, %r12d, %xmm12, %xmm14
	vmovd	%r15d, %xmm5
	movl	$1, 24(%rbx)
	vucomisd	%xmm11, %xmm2
	vpinsrd	$1, %edx, %xmm5, %xmm3
	vmovq	%xmm3, %rsi
	vpinsrd	$1, %r15d, %xmm4, %xmm15
	vpaddd	%xmm12, %xmm15, %xmm15
	vpextrd	$1, %xmm15, %r14d
	jnb	.L215
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%ecx, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm13
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%r12d, %xmm6, %xmm1
	call	pow
	vmovq	%xmm12, 44(%rbx)
	vmulsd	%xmm0, %xmm13, %xmm2
	vmovsd	%xmm2, 8(%rbx)
	vmovq	%xmm14, 28(%rbx)
.L210:
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%r15d, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm12
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%r14d, %xmm6, %xmm1
	call	pow
	movq	%rsi, 52(%rbx)
	vmovq	%xmm15, 36(%rbx)
	vmulsd	%xmm0, %xmm12, %xmm14
	vmovsd	%xmm14, 16(%rbx)
.L201:
	leal	1(%rbp), %edx
	vmovd	%r15d, %xmm5
	addq	$64, %rbx
	vpinsrd	$1, %edx, %xmm5, %xmm12
	cmpq	$1024, %r13
	je	.L216
.L203:
	incq	%r13
	vpextrd	$1, %xmm12, %ebp
	vmulsd	%xmm8, %xmm7, %xmm7
	vmovd	%xmm12, %ecx
	vcvttsd2sil	%xmm7, %r9d
	leal	-1(%r13), %esi
	vcvtsi2sdq	%r13, %xmm6, %xmm7
	leal	(%r15,%rdx), %r12d
	vmulsd	%xmm8, %xmm7, %xmm0
	vcvtsi2sdl	%ebp, %xmm6, %xmm2
	vcvttsd2sil	%xmm0, %r8d
	cmpl	%r8d, %r9d
	jne	.L217
	vucomisd	%xmm11, %xmm2
	jnb	.L218
.L202:
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%ecx, %xmm6, %xmm1
	movl	%ecx, 32(%rsp)
	call	pow
	vmovapd	%xmm0, %xmm13
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%r12d, %xmm6, %xmm1
	call	pow
	movl	32(%rsp), %r15d
	movl	%esi, (%rbx)
	movl	$0, 24(%rbx)
	vmulsd	%xmm0, %xmm13, %xmm1
	vmovq	%xmm12, 44(%rbx)
	vmovsd	%xmm1, 8(%rbx)
	movl	%r12d, 32(%rbx)
	movl	%r15d, 28(%rbx)
	jmp	.L201
.L216:
	movq	$-1, %r12
	movl	$17, %r13d
	jmp	.L208
	.p2align 4
	.p2align 3
.L221:
	vmovd	%r13d, %xmm1
	leal	1(%r13), %r14d
	movl	%r12d, (%rbx)
	movl	$1, 24(%rbx)
	vpinsrd	$1, %ebp, %xmm1, %xmm14
	vucomisd	%xmm11, %xmm7
	jnb	.L219
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%r13d, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm12
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%ebp, %xmm6, %xmm1
	call	pow
	movl	%r13d, 52(%rbx)
	movl	%r15d, 56(%rbx)
	vmovq	%xmm14, 36(%rbx)
	vmulsd	%xmm0, %xmm12, %xmm5
	vmovsd	%xmm5, 16(%rbx)
.L209:
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%r14d, %xmm6, %xmm1
	leal	(%r15,%r14), %ebp
	movl	%r14d, %r13d
	call	pow
	vmovapd	%xmm0, %xmm7
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%ebp, %xmm6, %xmm1
	call	pow
	vmulsd	%xmm0, %xmm7, %xmm0
.L206:
	vmovd	%r13d, %xmm4
	decq	%r12
	vmovsd	%xmm0, 8(%rbx)
	movl	%r13d, 28(%rbx)
	vpinsrd	$1, %r15d, %xmm4, %xmm2
	movl	%ebp, 32(%rbx)
	vmovq	%xmm2, 44(%rbx)
	cmpq	$-1023, %r12
	je	.L220
.L208:
	leal	1022(%r12), %ebx
	leal	-52(%r12), %r15d
	leaq	1(%r12), %r11
	vcvtsi2sdq	%r12, %xmm6, %xmm3
	salq	$6, %rbx
	vmulsd	%xmm8, %xmm3, %xmm4
	vcvtsi2sdq	%r11, %xmm6, %xmm15
	vcvtsi2sdl	%r15d, %xmm6, %xmm7
	vmulsd	%xmm8, %xmm15, %xmm2
	vcvttsd2sil	%xmm4, %r10d
	vcvttsd2sil	%xmm2, %r9d
	movl	%r12d, %esi
	addq	%rdi, %rbx
	leal	0(%r13,%r15), %ebp
	cmpl	%r9d, %r10d
	jne	.L221
	vucomisd	%xmm11, %xmm7
	jnb	.L222
.L207:
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%r13d, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm13
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%ebp, %xmm6, %xmm1
	call	pow
	movl	%esi, (%rbx)
	movl	$0, 24(%rbx)
	vmulsd	%xmm0, %xmm13, %xmm0
	jmp	.L206
.L220:
	vmovaps	48(%rsp), %xmm6
	vmovaps	64(%rsp), %xmm7
	vmovaps	80(%rsp), %xmm8
	vmovaps	96(%rsp), %xmm9
	vmovaps	112(%rsp), %xmm10
	vmovaps	128(%rsp), %xmm11
	vmovaps	144(%rsp), %xmm12
	vmovaps	160(%rsp), %xmm13
	vmovaps	176(%rsp), %xmm14
	vmovaps	192(%rsp), %xmm15
	addq	$216, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L215:
	vmovapd	%xmm2, %xmm1
	vmovapd	%xmm9, %xmm0
	vmovd	%xmm12, 44(%rsp)
	vmovsd	%xmm2, 32(%rsp)
	call	pow
	movl	44(%rsp), %eax
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%eax, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm13
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%r12d, %xmm6, %xmm1
	call	pow
	vmovsd	32(%rsp), %xmm1
	vmulsd	%xmm0, %xmm13, %xmm0
	vmovq	%xmm12, 44(%rbx)
	vmovsd	%xmm0, 8(%rbx)
	vmovapd	%xmm9, %xmm0
	vmovq	%xmm14, 28(%rbx)
	call	pow
	jmp	.L210
.L219:
	vmovapd	%xmm7, %xmm1
	vmovapd	%xmm9, %xmm0
	call	pow
	vmovapd	%xmm10, %xmm0
	vcvtsi2sdl	%r13d, %xmm6, %xmm1
	call	pow
	vmovapd	%xmm0, %xmm15
	vmovapd	%xmm9, %xmm0
	vcvtsi2sdl	%ebp, %xmm6, %xmm1
	call	pow
	movl	%r13d, 52(%rbx)
	movl	%r15d, 56(%rbx)
	vmulsd	%xmm15, %xmm0, %xmm3
	vmovq	%xmm14, 36(%rbx)
	vmovapd	%xmm7, %xmm1
	vmovapd	%xmm9, %xmm0
	vmovsd	%xmm3, 16(%rbx)
	call	pow
	jmp	.L209
.L222:
	vmovapd	%xmm7, %xmm1
	vmovapd	%xmm9, %xmm0
	call	pow
	jmp	.L207
.L218:
	vmovapd	%xmm2, %xmm1
	vmovapd	%xmm9, %xmm0
	vmovd	%xmm12, 32(%rsp)
	call	pow
	movl	32(%rsp), %ecx
	jmp	.L202
	.seh_endproc
	.p2align 4
	.globl	_Z8MyPrintFPcPKcz
	.def	_Z8MyPrintFPcPKcz;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z8MyPrintFPcPKcz
_Z8MyPrintFPcPKcz:
.LFB13525:
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	%r8, 80(%rsp)
	movq	%r9, 88(%rsp)
	movq	%rdx, %r8
	leaq	80(%rsp), %r9
	movl	$32, %edx
	movq	%r9, 40(%rsp)
	call	__mingw_vsnprintf
	nop
	addq	$56, %rsp
	ret
	.seh_endproc
	.p2align 4
	.globl	_Z14findLastSetBitx
	.def	_Z14findLastSetBitx;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z14findLastSetBitx
_Z14findLastSetBitx:
.LFB13526:
	.seh_endprologue
	testq	%rcx, %rcx
	je	.L227
	xorl	%eax, %eax
	blsi	%rcx, %rcx
	je	.L224
	.p2align 3
	.p2align 4
	.p2align 3
.L226:
	incl	%eax
	sarq	%rcx
	jne	.L226
.L224:
	ret
.L227:
	movl	$-1, %eax
	ret
	.seh_endproc
	.section	.text$_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	.def	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev:
.LFB13625:
	.seh_endprologue
	movq	%rcx, %rax
	movq	16(%rcx), %rcx
	leaq	1(%rcx), %r8
	movq	%r8, 16(%rax)
	movsbl	(%rcx), %edx
	testb	%dl, %dl
	js	.L231
	movl	$1, %r8d
.L232:
	movb	%r8b, 26(%rax)
	movl	%edx, (%rax)
	movw	$256, 24(%rax)
	movq	%rcx, 16(%rax)
	ret
	.p2align 4
	.p2align 3
.L231:
	cmpb	$-63, %dl
	jbe	.L248
	movq	32(%rax), %r9
	cmpq	%r9, %r8
	je	.L248
	cmpb	$-33, %dl
	jbe	.L251
	cmpb	$-17, %dl
	ja	.L234
	cmpb	$-32, %dl
	je	.L242
	cmpb	$-19, %dl
	movl	$-65, %r10d
	movl	$-97, %r8d
	movl	$-128, %r11d
	cmove	%r8d, %r10d
.L235:
	movzbl	1(%rcx), %r8d
	andl	$15, %edx
	cmpb	%r11b, %r8b
	jb	.L248
	cmpb	%r8b, %r10b
	jb	.L248
	leaq	2(%rcx), %r11
	movq	%r11, 16(%rax)
	cmpq	%r11, %r9
	je	.L237
	sall	$6, %edx
	andl	$63, %r8d
	orl	%edx, %r8d
	movzbl	2(%rcx), %edx
	leal	-128(%rdx), %r9d
	cmpb	$63, %r9b
	ja	.L237
	sall	$6, %r8d
	andl	$63, %edx
	orl	%r8d, %edx
	movl	$3, %r8d
	jmp	.L232
	.p2align 4
	.p2align 3
.L251:
	movzbl	1(%rcx), %r10d
	andl	$31, %edx
	leal	-128(%r10), %r8d
	cmpb	$63, %r8b
	ja	.L248
	sall	$6, %edx
	andl	$63, %r10d
	movl	$2, %r8d
	orl	%r10d, %edx
	jmp	.L232
	.p2align 4
	.p2align 3
.L248:
	movl	$1, %r8d
	movl	$65533, %edx
	jmp	.L232
	.p2align 4
	.p2align 3
.L234:
	cmpb	$-12, %dl
	ja	.L248
	cmpb	$-16, %dl
	je	.L246
	cmpb	$-12, %dl
	movl	$-65, %r10d
	movl	$-113, %r8d
	movl	$-128, %r11d
	cmove	%r8d, %r10d
.L238:
	movzbl	1(%rcx), %r8d
	andl	$7, %edx
	cmpb	%r11b, %r8b
	jb	.L248
	cmpb	%r8b, %r10b
	jb	.L248
	leaq	2(%rcx), %r11
	movq	%r11, 16(%rax)
	cmpq	%r11, %r9
	je	.L237
	sall	$6, %edx
	andl	$63, %r8d
	orl	%edx, %r8d
	movzbl	2(%rcx), %edx
	leal	-128(%rdx), %r10d
	cmpb	$63, %r10b
	ja	.L237
	leaq	3(%rcx), %r11
	movq	%r11, 16(%rax)
	cmpq	%r11, %r9
	je	.L250
	sall	$6, %r8d
	andl	$63, %edx
	orl	%edx, %r8d
	movzbl	3(%rcx), %edx
	leal	-128(%rdx), %r9d
	cmpb	$63, %r9b
	ja	.L250
	sall	$6, %r8d
	andl	$63, %edx
	orl	%r8d, %edx
	movl	$4, %r8d
	jmp	.L232
.L242:
	movl	$-96, %r11d
	movl	$-65, %r10d
	jmp	.L235
.L237:
	movl	$2, %r8d
	movl	$65533, %edx
	jmp	.L232
.L246:
	movl	$-112, %r11d
	movl	$-65, %r10d
	jmp	.L238
.L250:
	movl	$3, %r8d
	movl	$65533, %edx
	jmp	.L232
	.seh_endproc
	.section	.text$_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_
	.def	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_
_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_:
.LFB13583:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$136, %rsp
	.seh_stackalloc	136
	.seh_endprologue
	movzbl	(%rdx), %edi
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%r8, %rsi
	cmpb	$123, %dil
	je	.L264
	cmpq	%r8, %rdx
	je	.L255
	leaq	32(%rsp), %r13
	vpbroadcastq	%rdx, %xmm0
	movq	%r8, 64(%rsp)
	movw	$0, 56(%rsp)
	movq	%r13, %rcx
	vmovdqu	%xmm0, 40(%rsp)
	movb	$0, 58(%rsp)
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movzbl	56(%rsp), %r12d
	vmovdqu	32(%rsp), %ymm1
	movq	64(%rsp), %rax
	movzbl	57(%rsp), %r8d
	movq	48(%rsp), %rdx
	movzbl	58(%rsp), %ecx
	movzbl	%r12b, %r9d
	movq	%rax, 112(%rsp)
	incl	%r9d
	vmovdqu	%ymm1, 80(%rsp)
	cmpl	%r8d, %r9d
	je	.L272
	vzeroupper
.L256:
	movl	80(%rsp,%r12,4), %r10d
	cmpl	$55295, %r10d
	ja	.L273
.L259:
	cmpq	%rdx, %rsi
	je	.L255
	movzbl	(%rdx), %esi
	cmpb	$62, %sil
	je	.L265
	cmpb	$94, %sil
	je	.L266
	movl	$1, %r13d
	cmpb	$60, %sil
	jne	.L255
.L261:
	movzbl	0(%rbp), %ebx
	leaq	1(%rdx), %rax
	movl	%r10d, 8(%rbp)
	andl	$-4, %ebx
	orl	%r13d, %ebx
	movb	%bl, 0(%rbp)
	addq	$136, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L272:
	cmpq	%rdx, %rsi
	je	.L271
	addq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jne	.L274
.L271:
	vzeroupper
	.p2align 4
	.p2align 3
.L255:
	cmpb	$62, %dil
	je	.L267
	cmpb	$94, %dil
	je	.L268
	movl	$1, %edx
	cmpb	$60, %dil
	jne	.L264
.L263:
	movzbl	0(%rbp), %edi
	leaq	1(%rbx), %rax
	movl	$32, 8(%rbp)
	andl	$-4, %edi
	orl	%edx, %edi
	movb	%dil, 0(%rbp)
	addq	$136, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L264:
	movq	%rbx, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L265:
	movl	$2, %r13d
	jmp	.L261
	.p2align 4
	.p2align 3
.L267:
	movl	$2, %edx
	jmp	.L263
	.p2align 4
	.p2align 3
.L268:
	movl	$3, %edx
	jmp	.L263
	.p2align 4
	.p2align 3
.L266:
	movl	$3, %r13d
	jmp	.L261
	.p2align 4
	.p2align 3
.L274:
	movq	%rcx, 48(%rsp)
	movq	%r13, %rcx
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	48(%rsp), %rdx
	jmp	.L256
	.p2align 4
	.p2align 3
.L273:
	leal	-57344(%r10), %r11d
	cmpl	$1056767, %r11d
	ja	.L255
	jmp	.L259
	.seh_endproc
	.text
	.p2align 4
	.globl	_Z9PrefixSumDv8_x
	.def	_Z9PrefixSumDv8_x;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z9PrefixSumDv8_x
_Z9PrefixSumDv8_x:
.LFB13683:
	.seh_endprologue
	vpxor	%xmm0, %xmm0, %xmm0
	vmovdqa64	(%rdx), %zmm2
	movq	%rcx, %rax
	valignd	$15, %zmm0, %zmm2, %zmm1
	vpaddd	%zmm2, %zmm1, %zmm3
	valignd	$14, %zmm0, %zmm3, %zmm4
	vpaddd	%zmm3, %zmm4, %zmm5
	valignd	$12, %zmm0, %zmm5, %zmm2
	vpaddd	%zmm5, %zmm2, %zmm1
	valignd	$8, %zmm0, %zmm1, %zmm0
	vpaddd	%zmm1, %zmm0, %zmm3
	vmovdqa32	%zmm3, (%rcx)
	vzeroupper
	ret
	.seh_endproc
	.section .rdata,"dr"
.LC22:
	.ascii "pre_sum[%d]=%d\12\0"
	.text
	.p2align 4
	.globl	_Z20test_prefix_sum_simdv
	.def	_Z20test_prefix_sum_simdv;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z20test_prefix_sum_simdv
_Z20test_prefix_sum_simdv:
.LFB13684:
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$160, %rsp
	.seh_stackalloc	160
	.seh_endprologue
	vmovdqa32	.LC21(%rip), %zmm3
	xorl	%ebx, %ebx
	leaq	32(%rsp), %rdi
	leaq	.LC22(%rip), %rsi
	vmovdqu32	%zmm3, 32(%rsp)
	vzeroupper
	.p2align 4
	.p2align 3
.L277:
	movl	(%rdi,%rbx,4), %r8d
	movl	%ebx, %edx
	movq	%rsi, %rcx
	incq	%rbx
	call	_Z6printfPKcz
	cmpq	$16, %rbx
	jne	.L277
	vmovdqa32	.LC21(%rip), %zmm4
	vpxor	%xmm0, %xmm0, %xmm0
	xorl	%ebx, %ebx
	leaq	96(%rsp), %rdi
	valignd	$15, %zmm0, %zmm4, %zmm1
	vpaddd	%zmm4, %zmm1, %zmm5
	valignd	$14, %zmm0, %zmm5, %zmm2
	vpaddd	%zmm5, %zmm2, %zmm3
	valignd	$12, %zmm0, %zmm3, %zmm4
	vpaddd	%zmm3, %zmm4, %zmm1
	valignd	$8, %zmm0, %zmm1, %zmm0
	vpaddd	%zmm1, %zmm0, %zmm5
	vmovdqu8	%zmm5, 96(%rsp)
	vzeroupper
	.p2align 4
	.p2align 3
.L278:
	movl	(%rdi,%rbx,4), %r8d
	movl	%ebx, %edx
	movq	%rsi, %rcx
	incq	%rbx
	call	_Z6printfPKcz
	cmpq	$16, %rbx
	jne	.L278
	addq	$160, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	ret
	.seh_endproc
	.section .rdata,"dr"
.LC23:
	.ascii "num_i=%llx\12\0"
	.text
	.p2align 4
	.globl	_Z10test_func5v
	.def	_Z10test_func5v;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z10test_func5v
_Z10test_func5v:
.LFB13685:
	.seh_endprologue
	movabsq	$4547007122018943789, %rdx
	leaq	.LC23(%rip), %rcx
	jmp	_Z6printfPKcz
	.seh_endproc
	.section	.text$_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev
	.def	_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev
_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev:
.LFB13774:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	__imp__errno(%rip), %rsi
	movq	%rcx, %rbx
	call	*%rsi
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L285
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L285:
	movl	(%rbx), %ebx
	call	*%rsi
	movl	%ebx, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13774:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13774-.LLSDACSB13774
.LLSDACSB13774:
.LLSDACSE13774:
	.section	.text$_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
	.def	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_:
.LFB13798:
	subq	$216, %rsp
	.seh_stackalloc	216
	.seh_endprologue
	vmovdqa64	.LC24(%rip), %zmm0
	vmovdqa64	.LC25(%rip), %zmm1
	movabsq	$4122263930388298034, %r10
	movabsq	$16106987313379638, %r11
	movq	%rcx, %r9
	movq	%r11, 193(%rsp)
	leal	-1(%rdx), %ecx
	vmovdqa64	.LC26(%rip), %zmm2
	vmovdqu64	%zmm0, (%rsp)
	vmovdqu64	%zmm1, 64(%rsp)
	vmovdqu64	%zmm2, 128(%rsp)
	movq	%r10, 185(%rsp)
	cmpl	$99, %r8d
	jbe	.L287
	.p2align 4
	.p2align 3
.L288:
	movl	%r8d, %edx
	movl	%r8d, %eax
	imulq	$1374389535, %rdx, %r11
	movl	%ecx, %edx
	shrq	$37, %r11
	imull	$100, %r11d, %r10d
	subl	%r10d, %eax
	movl	%r8d, %r10d
	movl	%r11d, %r8d
	addl	%eax, %eax
	leal	1(%rax), %r11d
	movzbl	(%rsp,%rax), %eax
	movzbl	(%rsp,%r11), %r11d
	movb	%r11b, (%r9,%rdx)
	leal	-1(%rcx), %edx
	subl	$2, %ecx
	movb	%al, (%r9,%rdx)
	cmpl	$9999, %r10d
	ja	.L288
.L287:
	leal	48(%r8), %r11d
	cmpl	$9, %r8d
	jbe	.L290
	addl	%r8d, %r8d
	leal	1(%r8), %ecx
	movzbl	(%rsp,%r8), %r11d
	movzbl	(%rsp,%rcx), %r10d
	movb	%r10b, 1(%r9)
.L290:
	movb	%r11b, (%r9)
	vzeroupper
	addq	$216, %rsp
	ret
	.seh_endproc
	.section	.text$_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
	.def	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_:
.LFB13827:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$208, %rsp
	.seh_stackalloc	208
	.seh_endprologue
	vmovdqa64	.LC24(%rip), %zmm0
	vmovdqa64	.LC25(%rip), %zmm1
	movabsq	$4122263930388298034, %r10
	movabsq	$16106987313379638, %r11
	movq	%rcx, %r9
	movq	%r11, 193(%rsp)
	leal	-1(%rdx), %ecx
	vmovdqa64	.LC26(%rip), %zmm2
	vmovdqu64	%zmm0, (%rsp)
	vmovdqu64	%zmm1, 64(%rsp)
	vmovdqu64	%zmm2, 128(%rsp)
	movq	%r10, 185(%rsp)
	cmpq	$99, %r8
	jbe	.L293
	movabsq	$2951479051793528259, %r11
	.p2align 4
	.p2align 3
.L294:
	movq	%r8, %rax
	movq	%r8, %r10
	shrq	$2, %rax
	mulq	%r11
	movq	%r8, %rax
	shrq	$2, %rdx
	imulq	$100, %rdx, %rbx
	movq	%rdx, %r8
	movl	%ecx, %edx
	subq	%rbx, %rax
	addq	%rax, %rax
	movzbl	1(%rsp,%rax), %ebx
	movzbl	(%rsp,%rax), %eax
	movb	%bl, (%r9,%rdx)
	leal	-1(%rcx), %edx
	subl	$2, %ecx
	movb	%al, (%r9,%rdx)
	cmpq	$9999, %r10
	ja	.L294
.L293:
	leal	48(%r8), %r11d
	cmpq	$9, %r8
	jbe	.L296
	addq	%r8, %r8
	movzbl	1(%rsp,%r8), %ecx
	movzbl	(%rsp,%r8), %r11d
	movb	%cl, 1(%r9)
.L296:
	movb	%r11b, (%r9)
	vzeroupper
	addq	$208, %rsp
	popq	%rbx
	ret
	.seh_endproc
	.section	.text$_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_
	.def	_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_
_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_:
.LFB14338:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	.seh_endprologue
	movq	%rdx, %r9
	movsbw	(%rdx), %dx
	cmpb	$48, %dl
	je	.L347
	leal	-49(%rdx), %eax
	cmpb	$8, %al
	ja	.L301
	leaq	1(%r9), %r10
	cmpq	%r8, %r10
	je	.L302
	movzbl	1(%r9), %ebx
	subl	$48, %ebx
	cmpb	$9, %bl
	ja	.L302
	movq	%r8, %rsi
	xorl	%eax, %eax
	movq	%r9, %r11
	movl	$16, %r10d
	subq	%r9, %rsi
	andl	$3, %esi
	je	.L303
	cmpq	$1, %rsi
	je	.L330
	cmpq	$2, %rsi
	je	.L331
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L304
	movl	$12, %r10d
	movzbl	%dl, %eax
	leaq	1(%r9), %r11
.L331:
	movzbl	(%r11), %ebx
	subl	$48, %ebx
	cmpb	$9, %bl
	ja	.L304
	subl	$4, %r10d
	js	.L348
	leal	(%rax,%rax,4), %eax
	movzbl	%bl, %ebx
	leal	(%rbx,%rax,2), %eax
.L338:
	incq	%r11
.L330:
	movzbl	(%r11), %esi
	subl	$48, %esi
	cmpb	$9, %sil
	ja	.L304
	subl	$4, %r10d
	js	.L349
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %esi
	leal	(%rsi,%rax,2), %eax
.L340:
	incq	%r11
	cmpq	%r11, %r8
	je	.L312
.L303:
	movzbl	(%r11), %edx
	leal	-48(%rdx), %ebx
	cmpb	$9, %bl
	ja	.L304
	subl	$4, %r10d
	js	.L305
	leal	(%rax,%rax,4), %eax
	movzbl	%bl, %ebx
	leal	(%rbx,%rax,2), %eax
.L306:
	movzbl	1(%r11), %esi
	leaq	1(%r11), %rbx
	movq	%rbx, %r11
	subl	$48, %esi
	cmpb	$9, %sil
	ja	.L304
	movl	%r10d, %r11d
	subl	$4, %r11d
	js	.L350
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %r11d
	leal	(%r11,%rax,2), %eax
.L342:
	movzbl	1(%rbx), %esi
	leaq	1(%rbx), %r11
	subl	$48, %esi
	cmpb	$9, %sil
	ja	.L304
	movl	%r10d, %edx
	subl	$8, %edx
	js	.L351
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %edx
	leal	(%rdx,%rax,2), %eax
.L344:
	movzbl	2(%rbx), %esi
	leaq	2(%rbx), %r11
	subl	$48, %esi
	cmpb	$9, %sil
	ja	.L304
	subl	$12, %r10d
	js	.L352
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %esi
	leal	(%rsi,%rax,2), %eax
.L346:
	leaq	3(%rbx), %r11
	cmpq	%r11, %r8
	jne	.L303
	.p2align 4
	.p2align 3
.L312:
	movw	%ax, (%rcx)
	movq	%r11, 8(%rcx)
	jmp	.L298
	.p2align 4
	.p2align 3
.L304:
	cmpq	%r11, %r9
	jne	.L312
	.p2align 4
	.p2align 3
.L301:
	movq	$0, (%rcx)
	movq	$0, 8(%rcx)
.L298:
	movq	%rcx, %rax
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L302:
	subl	$48, %edx
	movq	%rcx, %rax
	movq	%r10, 8(%rcx)
	movw	%dx, (%rcx)
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L347:
	incq	%r9
	movq	%rcx, %rax
	movw	$0, (%rcx)
	movq	%r9, 8(%rcx)
	popq	%rbx
	popq	%rsi
	ret
.L348:
	movl	$10, %edx
	mulw	%dx
	jo	.L301
	movzbl	%bl, %esi
	addw	%ax, %si
	jc	.L301
	movl	%esi, %eax
	jmp	.L338
	.p2align 4
	.p2align 3
.L305:
	movl	$10, %esi
	mulw	%si
	jo	.L301
	movzbl	%bl, %edx
	addw	%ax, %dx
	jc	.L301
	movl	%edx, %eax
	jmp	.L306
	.p2align 4
	.p2align 3
.L350:
	movl	$10, %edx
	mulw	%dx
	jo	.L301
	movzbl	%sil, %esi
	addw	%ax, %si
	jc	.L301
	movl	%esi, %eax
	jmp	.L342
	.p2align 4
	.p2align 3
.L351:
	movl	$10, %r11d
	mulw	%r11w
	jo	.L301
	movzbl	%sil, %esi
	addw	%ax, %si
	jc	.L301
	movl	%esi, %eax
	jmp	.L344
	.p2align 4
	.p2align 3
.L352:
	movl	$10, %r11d
	mulw	%r11w
	jo	.L301
	movzbl	%sil, %edx
	addw	%ax, %dx
	jc	.L301
	movl	%edx, %eax
	jmp	.L346
.L349:
	movl	$10, %edx
	mulw	%dx
	jo	.L301
	movzbl	%sil, %ebx
	addw	%ax, %bx
	jc	.L301
	movl	%ebx, %eax
	jmp	.L340
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC27:
	.ascii "format error: width must be non-zero in format string\0"
	.align 8
.LC28:
	.ascii "format error: invalid width or precision in format-spec\0"
	.section	.text$_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE
	.def	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE
_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE:
.LFB14345:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	%rdx, %rbx
	movzbl	(%rdx), %edx
	movq	%rcx, %rsi
	cmpb	$48, %dl
	je	.L455
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rcx
	movzbl	%dl, %eax
	cmpb	$9, (%rcx,%rax)
	movq	%rbx, %rcx
	ja	.L355
	movq	%r8, %rdi
	xorl	%eax, %eax
	movl	$16, %ebp
	subq	%rbx, %rdi
	andl	$3, %edi
	je	.L364
	cmpq	$1, %rdi
	je	.L420
	cmpq	$2, %rdi
	jne	.L456
.L421:
	movzbl	(%rcx), %edx
	leal	-48(%rdx), %r11d
	cmpb	$9, %r11b
	ja	.L356
	subl	$4, %ebp
	js	.L457
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %edi
	leal	(%rdi,%rax,2), %eax
.L435:
	incq	%rcx
.L420:
	movzbl	(%rcx), %edx
	leal	-48(%rdx), %r11d
	cmpb	$9, %r11b
	ja	.L356
	subl	$4, %ebp
	js	.L458
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %edi
	leal	(%rdi,%rax,2), %eax
.L437:
	incq	%rcx
	cmpq	%rcx, %r8
	je	.L365
.L364:
	movzbl	(%rcx), %edx
	leal	-48(%rdx), %r11d
	cmpb	$9, %r11b
	ja	.L356
	subl	$4, %ebp
	js	.L357
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %edi
	leal	(%rdi,%rax,2), %eax
.L358:
	movzbl	1(%rcx), %edx
	leaq	1(%rcx), %r10
	movq	%r10, %rcx
	leal	-48(%rdx), %r11d
	cmpb	$9, %r11b
	ja	.L356
	movl	%ebp, %ecx
	subl	$4, %ecx
	js	.L459
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %edx
	leal	(%rdx,%rax,2), %eax
.L439:
	movzbl	1(%r10), %r11d
	leaq	1(%r10), %rcx
	leal	-48(%r11), %r9d
	cmpb	$9, %r9b
	ja	.L356
	movl	%ebp, %ecx
	subl	$8, %ecx
	js	.L460
	leal	(%rax,%rax,4), %eax
	movzbl	%r9b, %r11d
	leal	(%r11,%rax,2), %eax
.L441:
	movzbl	2(%r10), %r9d
	leaq	2(%r10), %rcx
	leal	-48(%r9), %edi
	cmpb	$9, %dil
	ja	.L356
	subl	$12, %ebp
	js	.L461
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %r11d
	leal	(%r11,%rax,2), %eax
.L443:
	leaq	3(%r10), %rcx
	cmpq	%rcx, %r8
	jne	.L364
	.p2align 4
	.p2align 3
.L365:
	movl	$1, %r9d
	movw	%ax, 4(%rsi)
.L366:
	movzwl	(%rsi), %ebx
	andl	$3, %r9d
	sall	$7, %r9d
	andw	$-385, %bx
	orl	%r9d, %ebx
	movw	%bx, (%rsi)
.L353:
	movq	%rcx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
	.p2align 4
	.p2align 3
.L355:
	cmpb	$123, %dl
	jne	.L353
	leaq	1(%rbx), %rcx
	cmpq	%rcx, %r8
	je	.L462
	movsbw	1(%rbx), %ax
	cmpb	$125, %al
	je	.L463
	cmpb	$48, %al
	je	.L464
	leal	-49(%rax), %edi
	cmpb	$8, %dil
	ja	.L374
	leaq	2(%rbx), %r10
	cmpq	%r10, %r8
	je	.L374
	movzbl	2(%rbx), %ebp
	leal	-48(%rbp), %r11d
	cmpb	$9, %r11b
	ja	.L375
	movq	%r8, %rdx
	xorl	%eax, %eax
	movq	%rcx, %r11
	movl	$16, %r10d
	subq	%rcx, %rdx
	andl	$3, %edx
	je	.L383
	cmpq	$1, %rdx
	je	.L422
	cmpq	$2, %rdx
	je	.L423
	movzbl	(%rcx), %edi
	leal	-48(%rdi), %ebp
	cmpb	$9, %bpl
	ja	.L376
	movl	$12, %r10d
	movzbl	%bpl, %eax
	leaq	1(%rcx), %r11
.L423:
	movzbl	(%r11), %edi
	subl	$48, %edi
	cmpb	$9, %dil
	ja	.L376
	subl	$4, %r10d
	js	.L465
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %edi
	leal	(%rdi,%rax,2), %eax
.L445:
	incq	%r11
.L422:
	movzbl	(%r11), %ebp
	subl	$48, %ebp
	cmpb	$9, %bpl
	ja	.L376
	subl	$4, %r10d
	js	.L466
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L447:
	incq	%r11
	cmpq	%r11, %r8
	je	.L448
.L383:
	movzbl	(%r11), %edi
	subl	$48, %edi
	cmpb	$9, %dil
	ja	.L376
	subl	$4, %r10d
	js	.L377
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %edi
	leal	(%rdi,%rax,2), %eax
.L378:
	movzbl	1(%r11), %edx
	leaq	1(%r11), %rdi
	movq	%rdi, %r11
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L376
	movl	%r10d, %r11d
	subl	$4, %r11d
	js	.L467
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %r11d
	leal	(%r11,%rax,2), %eax
.L450:
	movzbl	1(%rdi), %edx
	leaq	1(%rdi), %r11
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L376
	movl	%r10d, %r11d
	subl	$8, %r11d
	js	.L468
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %r11d
	leal	(%r11,%rax,2), %eax
.L452:
	movzbl	2(%rdi), %edx
	leaq	2(%rdi), %r11
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L376
	subl	$12, %r10d
	js	.L469
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L454:
	leaq	3(%rdi), %r11
	cmpq	%r11, %r8
	jne	.L383
.L448:
	movq	%r8, %rcx
.L373:
	cmpq	%rcx, %r8
	je	.L374
.L386:
	cmpb	$125, (%rcx)
	jne	.L374
	cmpl	$2, 16(%r9)
	je	.L385
	movl	$1, 16(%r9)
	movw	%ax, 4(%rsi)
	jmp	.L371
	.p2align 4
	.p2align 3
.L456:
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L356
	movl	$12, %ebp
	movzbl	%dl, %eax
	leaq	1(%rbx), %rcx
	jmp	.L421
	.p2align 4
	.p2align 3
.L356:
	cmpq	%rcx, %rbx
	jne	.L365
.L361:
	leaq	.LC28(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L357:
	movl	$10, %r10d
	mulw	%r10w
	jo	.L361
	movzbl	%r11b, %r9d
	addw	%ax, %r9w
	jc	.L361
	movl	%r9d, %eax
	jmp	.L358
	.p2align 4
	.p2align 3
.L459:
	movl	$10, %r9d
	mulw	%r9w
	jo	.L361
	movzbl	%r11b, %edi
	addw	%ax, %di
	jc	.L361
	movl	%edi, %eax
	jmp	.L439
	.p2align 4
	.p2align 3
.L460:
	movl	$10, %edi
	mulw	%di
	jo	.L361
	movzbl	%r9b, %edx
	addw	%ax, %dx
	jc	.L361
	movl	%edx, %eax
	jmp	.L441
	.p2align 4
	.p2align 3
.L461:
	movl	$10, %ecx
	mulw	%cx
	jo	.L361
	movzbl	%dil, %edx
	addw	%ax, %dx
	jc	.L361
	movl	%edx, %eax
	jmp	.L443
	.p2align 4
	.p2align 3
.L463:
	cmpl	$1, 16(%r9)
	je	.L385
	movq	24(%r9), %r8
	movl	$2, 16(%r9)
	leaq	1(%r8), %r10
	movq	%r10, 24(%r9)
	movw	%r8w, 4(%rsi)
.L371:
	incq	%rcx
	cmpq	%rcx, %rbx
	je	.L353
	movl	$2, %r9d
	jmp	.L366
.L464:
	leaq	2(%rbx), %rcx
	xorl	%eax, %eax
	jmp	.L373
.L458:
	movl	$10, %r10d
	mulw	%r10w
	jo	.L361
	movzbl	%r11b, %r9d
	addw	%ax, %r9w
	jc	.L361
	movl	%r9d, %eax
	jmp	.L437
.L457:
	movl	$10, %r10d
	mulw	%r10w
	jo	.L361
	movzbl	%r11b, %r9d
	addw	%ax, %r9w
	jc	.L361
	movl	%r9d, %eax
	jmp	.L435
.L375:
	subl	$48, %eax
	movq	%r10, %rcx
	jmp	.L386
.L376:
	cmpq	%r11, %rcx
	je	.L374
	movq	%r11, %rcx
	jmp	.L373
.L467:
	movl	$10, %edx
	mulw	%dx
	jo	.L374
	movzbl	%bpl, %ebp
	addw	%ax, %bp
	jc	.L374
	movl	%ebp, %eax
	jmp	.L450
.L377:
	movl	$10, %edx
	mulw	%dx
	jo	.L374
	movzbl	%dil, %ebp
	addw	%ax, %bp
	jc	.L374
	movl	%ebp, %eax
	jmp	.L378
.L469:
	movl	$10, %r11d
	mulw	%r11w
	jo	.L374
	movzbl	%bpl, %edx
	addw	%ax, %dx
	jc	.L374
	movl	%edx, %eax
	jmp	.L454
.L468:
	movl	$10, %edx
	mulw	%dx
	jo	.L374
	movzbl	%bpl, %ebp
	addw	%ax, %bp
	jc	.L374
	movl	%ebp, %eax
	jmp	.L452
.L466:
	movl	$10, %edx
	mulw	%dx
	jo	.L374
	movzbl	%bpl, %edi
	addw	%ax, %di
	jc	.L374
	movl	%edi, %eax
	jmp	.L447
.L465:
	movl	$10, %edx
	mulw	%dx
	jo	.L374
	movzbl	%dil, %ebp
	addw	%ax, %bp
	jc	.L374
	movl	%ebp, %eax
	jmp	.L445
.L455:
	leaq	.LC27(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L462:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
.L385:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.L374:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	nop
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC29:
	.ascii "format error: missing precision after '.' in format string\0"
	.section	.text$_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	.def	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE:
.LFB13580:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$184, %rsp
	.seh_stackalloc	184
	.seh_endprologue
	movq	8(%rdx), %r13
	movq	(%rdx), %rbx
	movq	%rcx, %rbp
	movq	%rdx, %rsi
	movq	$0, 68(%rsp)
	cmpq	%r13, %rbx
	je	.L471
	movzbl	(%rbx), %r15d
	cmpb	$125, %r15b
	je	.L471
	cmpb	$123, %r15b
	je	.L541
	leaq	80(%rsp), %r12
	movq	%rbx, 88(%rsp)
	movq	%rbx, 96(%rsp)
	movw	$0, 104(%rsp)
	movq	%r12, %rcx
	movb	$0, 106(%rsp)
	movq	%r13, 112(%rsp)
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movzbl	104(%rsp), %edi
	vmovdqu	80(%rsp), %ymm0
	movq	112(%rsp), %rax
	movzbl	105(%rsp), %r8d
	movq	96(%rsp), %rdx
	movzbl	106(%rsp), %ecx
	movzbl	%dil, %r9d
	movq	%rax, 160(%rsp)
	incl	%r9d
	vmovdqu	%ymm0, 128(%rsp)
	cmpl	%r8d, %r9d
	je	.L649
	vzeroupper
.L473:
	movl	128(%rsp,%rdi,4), %r12d
	cmpl	$55295, %r12d
	ja	.L650
.L476:
	cmpq	%rdx, %r13
	je	.L480
	movzbl	(%rdx), %r11d
	cmpb	$62, %r11b
	je	.L542
	cmpb	$94, %r11b
	je	.L543
	movl	$1, %r14d
	cmpb	$60, %r11b
	je	.L479
.L480:
	cmpb	$62, %r15b
	je	.L482
	cmpb	$94, %r15b
	je	.L483
	cmpb	$60, %r15b
	je	.L484
	xorl	%r14d, %r14d
	movl	$32, %r12d
.L485:
	movzbl	(%rbx), %r15d
	cmpb	$125, %r15b
	je	.L487
.L539:
	leal	-32(%r15), %edx
	xorl	%edi, %edi
	cmpb	$13, %dl
	ja	.L490
	movzbl	%dl, %edi
	leaq	CSWTCH.923(%rip), %rcx
	movl	(%rcx,%rdi,4), %eax
	xorl	%edi, %edi
	testl	%eax, %eax
	je	.L491
	movl	%eax, %edi
	incq	%rbx
	andl	$3, %edi
.L491:
	cmpq	%rbx, %r13
	je	.L492
	movzbl	(%rbx), %r15d
	cmpb	$125, %r15b
	je	.L492
	cmpb	$35, %r15b
	jne	.L490
	leaq	1(%rbx), %r9
	cmpq	%r9, %r13
	jne	.L651
.L494:
	movzwl	68(%rsp), %r13d
	movzbl	%dil, %ecx
	orl	$16, %r14d
	movq	%r9, %rbx
	movw	$0, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %ecx
	orl	%r14d, %ecx
	andw	$-32384, %r13w
	orl	%r13d, %ecx
	movw	%cx, 68(%rsp)
.L488:
	movq	68(%rsp), %r12
	movl	76(%rsp), %r9d
	movq	%rbx, %rax
	movq	%r12, 0(%rbp)
	movl	%r9d, 8(%rbp)
	addq	$184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L541:
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	xorl	%r10d, %r10d
	movl	$32, %r12d
	xorl	%r14d, %r14d
.L472:
	movzbl	%r11b, %ecx
	movb	%r11b, 46(%rsp)
	movzbl	%dil, %r8d
	movzwl	68(%rsp), %r11d
	movzbl	%r10b, %edx
	movzbl	%r14b, %r15d
	movq	%rsi, %r9
	movb	%r10b, 47(%rsp)
	sall	$2, %r8d
	movw	$0, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$4, %edx
	sall	$6, %ecx
	orl	%r15d, %r8d
	andw	$-32384, %r11w
	orl	%edx, %r8d
	movq	%rbx, %rdx
	orl	%ecx, %r8d
	leaq	68(%rsp), %rcx
	orl	%r11d, %r8d
	movw	%r8w, 68(%rsp)
	movq	%r13, %r8
	call	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE
	movq	%rax, %rbx
	cmpq	%r13, %rax
	je	.L488
	movzbl	(%rax), %r15d
	cmpb	$125, %r15b
	je	.L488
	cmpb	$46, %r15b
	movzbl	46(%rsp), %r11d
	movzbl	47(%rsp), %r10d
	je	.L498
	xorl	%r8d, %r8d
	xorl	%eax, %eax
.L499:
	cmpb	$76, %r15b
	je	.L652
	subl	$65, %r15d
	xorl	%r9d, %r9d
	cmpb	$38, %r15b
	ja	.L547
	leaq	.L527(%rip), %rcx
	movzbl	%r15b, %r15d
	movslq	(%rcx,%r15,4), %rsi
	addq	%rcx, %rsi
	jmp	*%rsi
	.section .rdata,"dr"
	.align 4
.L527:
	.long	.L534-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L533-.L527
	.long	.L532-.L527
	.long	.L531-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L530-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L547-.L527
	.long	.L529-.L527
	.long	.L528-.L527
	.long	.L526-.L527
	.section	.text$_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L471:
	movl	$32, 76(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L649:
	cmpq	%rdx, %r13
	je	.L475
	addq	%rdx, %rcx
	cmpq	%rcx, %r13
	je	.L475
	movq	%rcx, 96(%rsp)
	movq	%r12, %rcx
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	96(%rsp), %rdx
	jmp	.L473
	.p2align 4
	.p2align 3
.L543:
	movl	$3, %r14d
.L479:
	leaq	1(%rdx), %rbx
.L481:
	cmpq	%rbx, %r13
	jne	.L485
.L487:
	movzwl	68(%rsp), %r11d
	andl	$3, %r14d
	movw	$0, 74(%rsp)
	movl	%r12d, 76(%rsp)
	andw	$-32384, %r11w
	orl	%r14d, %r11d
	movw	%r11w, 68(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L492:
	movzwl	68(%rsp), %r8d
	movzbl	%dil, %edi
	movw	$0, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %edi
	orl	%r14d, %edi
	andw	$-32384, %r8w
	orl	%r8d, %edi
	movw	%di, 68(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L482:
	movl	$2, %r14d
.L486:
	incq	%rbx
	movl	$32, %r12d
	jmp	.L481
	.p2align 4
	.p2align 3
.L484:
	movl	$1, %r14d
	jmp	.L486
	.p2align 4
	.p2align 3
.L483:
	movl	$3, %r14d
	jmp	.L486
	.p2align 4
	.p2align 3
.L651:
	movzbl	1(%rbx), %r15d
	cmpb	$125, %r15b
	je	.L494
	movl	$1, %r10d
.L538:
	cmpb	$48, %r15b
	jne	.L653
	leaq	1(%r9), %rbx
	cmpq	%rbx, %r13
	jne	.L654
.L497:
	movzwl	68(%rsp), %esi
	movzbl	%dil, %eax
	orl	$64, %r14d
	movw	$0, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %eax
	sall	$4, %r10d
	orl	%r14d, %eax
	andw	$-32384, %si
	orl	%r10d, %eax
	orl	%esi, %eax
	movw	%ax, 68(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L475:
	movl	128(%rsp,%rdi,4), %r14d
	cmpl	$55295, %r14d
	ja	.L655
.L645:
	vzeroupper
	jmp	.L480
	.p2align 4
	.p2align 3
.L547:
	xorl	%edx, %edx
.L525:
	cmpq	%rbx, %r13
	je	.L535
.L536:
	cmpb	$125, (%rbx)
	jne	.L537
.L535:
	movzbl	%dil, %r15d
	movw	%ax, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %r15d
	sall	$4, %r10d
	orl	%r14d, %r15d
	orl	%r10d, %r15d
	movzwl	68(%rsp), %r10d
	sall	$5, %r9d
	sall	$6, %r11d
	sall	$9, %r8d
	orl	%r9d, %r15d
	andw	$-32384, %r10w
	sall	$11, %edx
	orl	%r11d, %r15d
	orl	%r8d, %r15d
	orl	%edx, %r15d
	orl	%r10d, %r15d
	movw	%r15w, 68(%rsp)
	jmp	.L488
.L564:
	movl	$1, %r9d
.L526:
	incq	%rbx
	movl	$7, %edx
	jmp	.L525
.L563:
	movl	$1, %r9d
.L528:
	incq	%rbx
	movl	$5, %edx
	jmp	.L525
.L562:
	movl	$1, %r9d
.L529:
	incq	%rbx
	movl	$3, %edx
	jmp	.L525
.L561:
	movl	$1, %r9d
.L530:
	incq	%rbx
	movl	$1, %edx
	jmp	.L525
.L560:
	movl	$1, %r9d
.L531:
	incq	%rbx
	movl	$8, %edx
	jmp	.L525
.L559:
	movl	$1, %r9d
.L532:
	incq	%rbx
	movl	$6, %edx
	jmp	.L525
.L558:
	movl	$1, %r9d
.L533:
	incq	%rbx
	movl	$4, %edx
	jmp	.L525
.L557:
	movl	$1, %r9d
.L534:
	incq	%rbx
	movl	$2, %edx
	jmp	.L525
.L653:
	movq	%r9, %rbx
	xorl	%r11d, %r11d
.L496:
	cmpb	$46, %r15b
	jne	.L472
	.p2align 4
	.p2align 3
.L498:
	leaq	1(%rbx), %r15
	cmpq	%r15, %r13
	je	.L513
	movzbl	1(%rbx), %r9d
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %r8
	cmpb	$9, (%r8,%r9)
	ja	.L501
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%r15, %rbx
	movl	$16, %ecx
	subq	%r15, %rsi
	andl	$3, %esi
	je	.L510
	cmpq	$1, %rsi
	je	.L607
	cmpq	$2, %rsi
	je	.L608
	movzbl	(%r15), %edx
	leal	-48(%rdx), %r9d
	cmpb	$9, %r9b
	ja	.L502
	movl	$12, %ecx
	movzbl	%r9b, %eax
	leaq	1(%r15), %rbx
.L608:
	movzbl	(%rbx), %r8d
	leal	-48(%r8), %esi
	cmpb	$9, %sil
	ja	.L502
	subl	$4, %ecx
	js	.L656
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %r8d
	leal	(%r8,%rax,2), %eax
.L631:
	incq	%rbx
.L607:
	movzbl	(%rbx), %esi
	leal	-48(%rsi), %r9d
	cmpb	$9, %r9b
	ja	.L502
	subl	$4, %ecx
	js	.L657
	leal	(%rax,%rax,4), %eax
	movzbl	%r9b, %esi
	leal	(%rsi,%rax,2), %eax
.L633:
	incq	%rbx
	cmpq	%rbx, %r13
	je	.L634
.L510:
	movzbl	(%rbx), %r9d
	leal	-48(%r9), %r8d
	cmpb	$9, %r8b
	ja	.L502
	subl	$4, %ecx
	js	.L503
	leal	(%rax,%rax,4), %eax
	movzbl	%r8b, %r9d
	leal	(%r9,%rax,2), %eax
.L504:
	movzbl	1(%rbx), %edx
	leaq	1(%rbx), %r8
	movq	%r8, %rbx
	leal	-48(%rdx), %r9d
	cmpb	$9, %r9b
	ja	.L502
	movl	%ecx, %ebx
	subl	$4, %ebx
	js	.L658
	leal	(%rax,%rax,4), %eax
	movzbl	%r9b, %r9d
	leal	(%r9,%rax,2), %eax
.L636:
	movzbl	1(%r8), %esi
	leaq	1(%r8), %rbx
	leal	-48(%rsi), %r9d
	cmpb	$9, %r9b
	ja	.L502
	movl	%ecx, %ebx
	subl	$8, %ebx
	js	.L659
	leal	(%rax,%rax,4), %eax
	movzbl	%r9b, %r9d
	leal	(%r9,%rax,2), %eax
.L638:
	movzbl	2(%r8), %edx
	leaq	2(%r8), %rbx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L502
	subl	$12, %ecx
	js	.L660
	leal	(%rax,%rax,4), %eax
	movzbl	%sil, %edx
	leal	(%rdx,%rax,2), %eax
.L640:
	leaq	3(%r8), %rbx
	cmpq	%rbx, %r13
	jne	.L510
.L634:
	movl	$1, %r8d
	.p2align 4
	.p2align 3
.L511:
	movzbl	%dil, %r13d
	movw	%ax, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %r13d
	orl	%r14d, %r13d
	movzwl	68(%rsp), %r14d
	sall	$4, %r10d
	sall	$6, %r11d
	sall	$9, %r8d
	orl	%r13d, %r10d
	andw	$-32384, %r14w
	orl	%r10d, %r11d
	orl	%r11d, %r8d
	orl	%r14d, %r8d
	movw	%r8w, 68(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L542:
	movl	$2, %r14d
	jmp	.L479
	.p2align 4
	.p2align 3
.L654:
	movzbl	1(%r9), %r15d
	cmpb	$125, %r15b
	je	.L497
	movl	$1, %r11d
	jmp	.L496
	.p2align 4
	.p2align 3
.L652:
	leaq	1(%rbx), %r9
	cmpq	%r9, %r13
	jne	.L661
.L523:
	movzbl	%dil, %ebx
	orl	$32, %r14d
	movw	%ax, 74(%rsp)
	movl	%r12d, 76(%rsp)
	sall	$2, %ebx
	orl	%r14d, %ebx
	movzwl	68(%rsp), %r14d
	sall	$4, %r10d
	sall	$6, %r11d
	orl	%ebx, %r10d
	andw	$-32384, %r14w
	movq	%r9, %rbx
	sall	$9, %r8d
	orl	%r10d, %r11d
	orl	%r11d, %r8d
	orl	%r14d, %r8d
	movw	%r8w, 68(%rsp)
	jmp	.L488
	.p2align 4
	.p2align 3
.L650:
	leal	-57344(%r12), %r10d
	cmpl	$1056767, %r10d
	jbe	.L476
.L477:
	cmpb	$62, %r15b
	je	.L482
	cmpb	$94, %r15b
	je	.L483
	cmpb	$60, %r15b
	je	.L484
	xorl	%r14d, %r14d
	movl	$32, %r12d
	jmp	.L539
	.p2align 4
	.p2align 3
.L501:
	cmpb	$123, %r9b
	je	.L662
.L513:
	leaq	.LC29(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L661:
	movzbl	1(%rbx), %edx
	cmpb	$125, %dl
	je	.L523
	subl	$65, %edx
	movq	%r9, %rbx
	cmpb	$38, %dl
	ja	.L556
	leaq	.L540(%rip), %rsi
	movzbl	%dl, %r15d
	movslq	(%rsi,%r15,4), %rcx
	addq	%rsi, %rcx
	jmp	*%rcx
	.section .rdata,"dr"
	.align 4
.L540:
	.long	.L557-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L558-.L540
	.long	.L559-.L540
	.long	.L560-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L561-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L556-.L540
	.long	.L562-.L540
	.long	.L563-.L540
	.long	.L564-.L540
	.section	.text$_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L655:
	subl	$57344, %r14d
	cmpl	$1056767, %r14d
	jbe	.L645
	vzeroupper
	jmp	.L477
.L662:
	leaq	2(%rbx), %rdx
	cmpq	%rdx, %r13
	je	.L663
	cmpb	$125, 2(%rbx)
	je	.L664
	leaq	48(%rsp), %rcx
	movq	%r13, %r8
	movb	%r10b, 47(%rsp)
	movb	%r11b, 46(%rsp)
	call	_ZNSt8__format14__parse_arg_idIcEESt4pairItPKT_ES4_S4_
	movq	56(%rsp), %rdx
	movzwl	48(%rsp), %eax
	testq	%rdx, %rdx
	je	.L518
	cmpq	%rdx, %r13
	je	.L518
	cmpb	$125, (%rdx)
	movzbl	46(%rsp), %r11d
	movzbl	47(%rsp), %r10d
	jne	.L518
	cmpl	$2, 16(%rsi)
	je	.L520
	movl	$1, 16(%rsi)
.L517:
	leaq	1(%rdx), %rbx
	cmpq	%rbx, %r15
	je	.L513
	movl	$2, %r8d
.L512:
	cmpq	%r13, %rbx
	je	.L511
	movzbl	(%rbx), %r15d
	cmpb	$125, %r15b
	jne	.L499
	jmp	.L511
.L502:
	cmpq	%rbx, %r15
	je	.L507
	movl	$1, %r8d
	jmp	.L512
.L503:
	movl	$10, %edx
	mulw	%dx
	jo	.L507
	movzbl	%r8b, %esi
	addw	%ax, %si
	jc	.L507
	movl	%esi, %eax
	jmp	.L504
.L658:
	movl	$10, %esi
	mulw	%si
	jo	.L507
	movzbl	%r9b, %edx
	addw	%ax, %dx
	jc	.L507
	movl	%edx, %eax
	jmp	.L636
.L659:
	movl	$10, %edx
	mulw	%dx
	jo	.L507
	movzbl	%r9b, %esi
	addw	%ax, %si
	jc	.L507
	movl	%esi, %eax
	jmp	.L638
.L660:
	movl	$10, %ebx
	mulw	%bx
	jo	.L507
	movzbl	%sil, %r9d
	addw	%ax, %r9w
	jc	.L507
	movl	%r9d, %eax
	jmp	.L640
.L664:
	cmpl	$1, 16(%rsi)
	je	.L520
	movq	24(%rsi), %rax
	movl	$2, 16(%rsi)
	leaq	1(%rax), %rbx
	movq	%rbx, 24(%rsi)
	jmp	.L517
.L657:
	movl	$10, %edx
	mulw	%dx
	jo	.L507
	movzbl	%r9b, %r8d
	addw	%ax, %r8w
	jc	.L507
	movl	%r8d, %eax
	jmp	.L633
.L656:
	movl	$10, %edx
	mulw	%dx
	jo	.L507
	movzbl	%sil, %r9d
	addw	%ax, %r9w
	jc	.L507
	movl	%r9d, %eax
	jmp	.L631
.L490:
	movq	%rbx, %r9
	xorl	%r10d, %r10d
	jmp	.L538
.L556:
	movl	$1, %r9d
	xorl	%edx, %edx
	jmp	.L536
.L663:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
.L520:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.L518:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
.L537:
	call	_ZNSt8__format29__failed_to_parse_format_specEv
.L507:
	leaq	.LC28(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv:
.LFB14400:
	.seh_endprologue
	movq	(%rcx), %rax
	leaq	16(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L667
	movq	16(%rcx), %rdx
	movq	%rax, %rcx
	incq	%rdx
	jmp	_ZdlPvy
	.p2align 4
	.p2align 3
.L667:
	ret
	.seh_endproc
	.section	.text$_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	.def	_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE:
.LFB14293:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$80, %rsp
	.seh_stackalloc	80
	.seh_endprologue
	movq	(%rcx), %rax
	movq	$0, 56(%rsp)
	movb	$0, 64(%rsp)
	movq	%rdx, %r12
	leaq	40(%rsp), %rbx
	movq	%rcx, %rdi
	leaq	64(%rsp), %rbp
	movq	%rbp, 48(%rsp)
	movq	-24(%rax), %rdx
	addq	%rcx, %rdx
	movq	%rbx, %rcx
	addq	$208, %rdx
	call	_ZNSt6localeC1ERKS_
	movq	.refptr._ZNSt5ctypeIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	40(%rsp), %rcx
	movq	8(%rcx), %rsi
	movq	(%rsi,%rax,8), %rsi
	testq	%rsi, %rsi
	je	.L669
	movq	%rbx, %rcx
	call	_ZNSt6localeD1Ev
	cmpb	$0, 56(%rsi)
	je	.L693
	movzbl	106(%rsi), %ebx
.L672:
	movzbl	105(%rsi), %r13d
.L675:
	movq	48(%rsp), %rsi
	cmpq	%rbp, %rsi
	je	.L676
	cmpq	$0, 64(%rsp)
	je	.L694
.L676:
	movb	%r13b, (%rsi)
	movq	48(%rsp), %r10
	movq	$1, 56(%rsp)
	movb	$0, 1(%r10)
	movl	(%r12), %r11d
	movq	48(%rsp), %rdx
	testl	%r11d, %r11d
	jne	.L695
	.p2align 4
	.p2align 3
.L678:
	movq	56(%rsp), %r8
	movq	%rdi, %rcx
.LEHB2:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	48(%rsp), %rcx
	movq	%rax, %rdi
	cmpq	%rbp, %rcx
	je	.L668
	movq	64(%rsp), %r12
	leaq	1(%r12), %rdx
	call	_ZdlPvy
.L668:
	movq	%rdi, %rax
	addq	$80, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L695:
	testb	$1, %r11b
	je	.L678
	movb	%bl, (%rdx)
	movq	48(%rsp), %rdx
	jmp	.L678
	.p2align 4
	.p2align 3
.L694:
	movl	$2, %ecx
	call	_Znwy
	movq	48(%rsp), %rcx
	movq	%rax, %rsi
	cmpq	%rbp, %rcx
	je	.L677
	movq	64(%rsp), %r9
	leaq	1(%r9), %rdx
	call	_ZdlPvy
.L677:
	movq	%rsi, 48(%rsp)
	movq	$1, 64(%rsp)
	jmp	.L676
	.p2align 4
	.p2align 3
.L693:
	movq	%rsi, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rsi), %rbx
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %r14
	movq	48(%rbx), %r13
	movl	$49, %ebx
	cmpq	%r14, %r13
	jne	.L696
.L673:
	cmpb	$0, 56(%rsi)
	jne	.L672
	movq	%rsi, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rsi), %r8
	movl	$48, %r13d
	movq	48(%r8), %rax
	cmpq	%r14, %rax
	je	.L675
	movl	$48, %edx
	movq	%rsi, %rcx
	call	*%rax
	movl	%eax, %r13d
	jmp	.L675
	.p2align 4
	.p2align 3
.L696:
	movl	$49, %edx
	movq	%rsi, %rcx
	call	*%r13
.LEHE2:
	movl	%eax, %ebx
	jmp	.L673
.L685:
	movq	%rax, %rbp
	vzeroupper
.L681:
	leaq	48(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbp, %rcx
.LEHB3:
	call	_Unwind_Resume
.LEHE3:
.L669:
.LEHB4:
	call	_ZSt16__throw_bad_castv
.LEHE4:
.L684:
	movq	%rbx, %rcx
	movq	%rax, %rbp
	vzeroupper
	call	_ZNSt6localeD1Ev
	jmp	.L681
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14293:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14293-.LLSDACSB14293
.LLSDACSB14293:
	.uleb128 .LEHB2-.LFB14293
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L685-.LFB14293
	.uleb128 0
	.uleb128 .LEHB3-.LFB14293
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB4-.LFB14293
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L684-.LFB14293
	.uleb128 0
.LLSDACSE14293:
	.section	.text$_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	.def	_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE:
.LFB14298:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$80, %rsp
	.seh_stackalloc	80
	.seh_endprologue
	movq	(%rcx), %rax
	movq	$0, 56(%rsp)
	movb	$0, 64(%rsp)
	movq	%rdx, %rbx
	leaq	40(%rsp), %rdi
	movq	%rcx, %rsi
	leaq	64(%rsp), %r13
	movq	%r13, 48(%rsp)
	movq	-24(%rax), %rdx
	addq	%rcx, %rdx
	movq	%rdi, %rcx
	addq	$208, %rdx
	call	_ZNSt6localeC1ERKS_
	movq	.refptr._ZNSt5ctypeIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	40(%rsp), %rcx
	movq	8(%rcx), %rbp
	movq	0(%rbp,%rax,8), %r12
	testq	%r12, %r12
	je	.L698
	movq	%rdi, %rcx
	call	_ZNSt6localeD1Ev
	cmpb	$0, 56(%r12)
	je	.L726
	movzbl	106(%r12), %edi
.L701:
	movzbl	105(%r12), %ebp
.L704:
	movq	48(%rsp), %r12
	cmpq	%r13, %r12
	je	.L705
	movq	64(%rsp), %r9
	cmpq	$10, %r9
	jbe	.L727
.L705:
	movabsq	$72340172838076673, %r11
	imulq	%rbp, %r11
	movq	%r11, (%r12)
	movl	%r11d, 7(%r12)
	movq	48(%rsp), %rcx
	movq	$11, 56(%rsp)
	movb	$0, 11(%rcx)
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L708
	tzcntl	%edx, %eax
	cmpl	$10, %eax
	jg	.L708
	movl	$10, %ebp
	.p2align 6
	.p2align 4
	.p2align 3
.L709:
	movq	%rbp, %r9
	subq	%rax, %r9
	incq	%rax
	movb	%dil, (%rcx,%r9)
	movq	48(%rsp), %rcx
	shrx	%eax, (%rbx), %r14d
	testl	%r14d, %r14d
	je	.L708
	tzcntl	%r14d, %r12d
	addq	%r12, %rax
	cmpq	$10, %rax
	jbe	.L709
	.p2align 4
	.p2align 3
.L708:
	movq	56(%rsp), %r8
	movq	%rcx, %rdx
	movq	%rsi, %rcx
.LEHB5:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	48(%rsp), %rcx
	movq	%rax, %rsi
	cmpq	%r13, %rcx
	je	.L697
	movq	64(%rsp), %rbx
	leaq	1(%rbx), %rdx
	call	_ZdlPvy
.L697:
	movq	%rsi, %rax
	addq	$80, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L727:
	leaq	(%r9,%r9), %r14
	leaq	1(%r14), %rcx
	cmpq	$11, %r14
	jbe	.L728
.L706:
	call	_Znwy
	movq	48(%rsp), %rcx
	movq	%rax, %r12
	cmpq	%r13, %rcx
	je	.L707
	movq	64(%rsp), %r10
	leaq	1(%r10), %rdx
	call	_ZdlPvy
.L707:
	movq	%r12, 48(%rsp)
	movq	%r14, 64(%rsp)
	jmp	.L705
	.p2align 4
	.p2align 3
.L726:
	movq	%r12, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%r12), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %r14
	movq	48(%rdi), %rax
	movl	$49, %edi
	cmpq	%r14, %rax
	jne	.L729
.L702:
	cmpb	$0, 56(%r12)
	jne	.L701
	movq	%r12, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%r12), %r8
	movl	$48, %ebp
	movq	48(%r8), %rax
	cmpq	%r14, %rax
	je	.L704
	movl	$48, %edx
	movq	%r12, %rcx
	call	*%rax
	movzbl	%al, %ebp
	jmp	.L704
	.p2align 4
	.p2align 3
.L728:
	movl	$11, %r14d
	movl	$12, %ecx
	jmp	.L706
	.p2align 4
	.p2align 3
.L729:
	movl	$49, %edx
	movq	%r12, %rcx
	call	*%rax
.LEHE5:
	movl	%eax, %edi
	jmp	.L702
.L717:
	movq	%rax, %r13
	vzeroupper
.L712:
	leaq	48(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r13, %rcx
.LEHB6:
	call	_Unwind_Resume
.LEHE6:
.L698:
.LEHB7:
	call	_ZSt16__throw_bad_castv
.LEHE7:
.L716:
	movq	%rdi, %rcx
	movq	%rax, %r13
	vzeroupper
	call	_ZNSt6localeD1Ev
	jmp	.L712
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14298:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14298-.LLSDACSB14298
.LLSDACSB14298:
	.uleb128 .LEHB5-.LFB14298
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L717-.LFB14298
	.uleb128 0
	.uleb128 .LEHB6-.LFB14298
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB7-.LFB14298
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L716-.LFB14298
	.uleb128 0
.LLSDACSE14298:
	.section	.text$_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	.def	_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE:
.LFB14302:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$80, %rsp
	.seh_stackalloc	80
	.seh_endprologue
	movq	(%rcx), %rax
	movq	$0, 56(%rsp)
	movb	$0, 64(%rsp)
	movq	%rdx, %rsi
	leaq	40(%rsp), %rdi
	movq	%rcx, %rbx
	leaq	64(%rsp), %r13
	movq	%r13, 48(%rsp)
	movq	-24(%rax), %rdx
	addq	%rcx, %rdx
	movq	%rdi, %rcx
	addq	$208, %rdx
	call	_ZNSt6localeC1ERKS_
	movq	.refptr._ZNSt5ctypeIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	40(%rsp), %rcx
	movq	8(%rcx), %rbp
	movq	0(%rbp,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.L731
	movq	%rdi, %rcx
	call	_ZNSt6localeD1Ev
	cmpb	$0, 56(%rbp)
	je	.L765
	movzbl	106(%rbp), %edi
.L734:
	movzbl	105(%rbp), %r12d
.L737:
	movq	48(%rsp), %rbp
	cmpq	%r13, %rbp
	je	.L756
	movq	64(%rsp), %r9
	cmpq	$51, %r9
	jbe	.L766
.L739:
	vpbroadcastb	%r12d, %ymm0
	vmovdqu8	%ymm0, 0(%rbp)
	movq	48(%rsp), %rdx
	movq	$52, 56(%rsp)
	vmovdqu8	%ymm0, 20(%rbp)
	movb	$0, 52(%rdx)
	movl	(%rsi), %r11d
	testl	%r11d, %r11d
	jne	.L741
	movl	4(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L742
.L757:
	movl	$52, %r8d
.L743:
	movq	%rbx, %rcx
	vzeroupper
.LEHB8:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	48(%rsp), %rcx
	movq	%rax, %rbx
	cmpq	%r13, %rcx
	je	.L730
	movq	64(%rsp), %rsi
	leaq	1(%rsi), %rdx
	call	_ZdlPvy
.L730:
	movq	%rbx, %rax
	addq	$80, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L765:
	movq	%rbp, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	0(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %r14
	movq	48(%rdi), %r12
	movl	$49, %edi
	cmpq	%r14, %r12
	jne	.L767
.L735:
	cmpb	$0, 56(%rbp)
	jne	.L734
	movq	%rbp, %rcx
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	0(%rbp), %r8
	movl	$48, %r12d
	movq	48(%r8), %rax
	cmpq	%r14, %rax
	je	.L737
	movl	$48, %edx
	movq	%rbp, %rcx
	call	*%rax
	movl	%eax, %r12d
	jmp	.L737
	.p2align 4
	.p2align 3
.L756:
	movl	$52, %r14d
	movl	$53, %ecx
.L738:
	call	_Znwy
	movq	48(%rsp), %rcx
	movq	%rax, %rbp
	cmpq	%r13, %rcx
	je	.L740
	movq	64(%rsp), %r10
	leaq	1(%r10), %rdx
	call	_ZdlPvy
.L740:
	movq	%rbp, 48(%rsp)
	movq	%r14, 64(%rsp)
	jmp	.L739
	.p2align 4
	.p2align 3
.L766:
	leaq	(%r9,%r9), %r14
	cmpq	$52, %r14
	jbe	.L756
	leaq	1(%r14), %rcx
	jmp	.L738
	.p2align 4
	.p2align 3
.L767:
	movl	$49, %edx
	movq	%rbp, %rcx
	call	*%r12
.LEHE8:
	movl	%eax, %edi
	jmp	.L735
.L741:
	tzcntl	%r11d, %eax
.L744:
	movl	$51, %r14d
	movl	$-1, %ebp
	.p2align 4
	.p2align 3
.L749:
	movq	%r14, %r9
	subq	%rax, %r9
	incq	%rax
	movb	%dil, (%rdx,%r9)
	movq	%rax, %rdx
	shlx	%eax, %ebp, %eax
	shrq	$5, %rdx
	andl	(%rsi,%rdx,4), %eax
	jne	.L768
	testq	%rdx, %rdx
	jne	.L748
	movl	4(%rsi), %r10d
	testl	%r10d, %r10d
	jne	.L769
.L748:
	movq	56(%rsp), %r8
	movq	48(%rsp), %rdx
	jmp	.L743
	.p2align 4
	.p2align 3
.L768:
	salq	$5, %rdx
	tzcntl	%eax, %eax
	addq	%rdx, %rax
.L746:
	movq	48(%rsp), %rdx
	cmpq	$51, %rax
	jbe	.L749
	movq	56(%rsp), %r8
	jmp	.L743
	.p2align 4
	.p2align 3
.L769:
	tzcntl	%r10d, %eax
	addl	$32, %eax
	cltq
	jmp	.L746
.L742:
	tzcntl	%ecx, %r8d
	leal	32(%r8), %r12d
	movslq	%r12d, %rax
	cmpl	$51, %r12d
	jle	.L744
	jmp	.L757
.L731:
.LEHB9:
	call	_ZSt16__throw_bad_castv
.LEHE9:
.L759:
	movq	%rax, %r13
	vzeroupper
.L752:
	leaq	48(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r13, %rcx
.LEHB10:
	call	_Unwind_Resume
.LEHE10:
.L758:
	movq	%rdi, %rcx
	movq	%rax, %r13
	vzeroupper
	call	_ZNSt6localeD1Ev
	jmp	.L752
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA14302:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE14302-.LLSDACSB14302
.LLSDACSB14302:
	.uleb128 .LEHB8-.LFB14302
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L759-.LFB14302
	.uleb128 0
	.uleb128 .LEHB9-.LFB14302
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L758-.LFB14302
	.uleb128 0
	.uleb128 .LEHB10-.LFB14302
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0
	.uleb128 0
.LLSDACSE14302:
	.section	.text$_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
.LC30:
	.ascii "binary double : \0"
.LC31:
	.ascii "ieee754 exp=\0"
	.text
	.p2align 4
	.globl	_Z12print_doubled
	.def	_Z12print_doubled;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z12print_doubled
_Z12print_doubled:
.LFB13456:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	movq	.refptr._ZSt4cout(%rip), %rsi
	vmovq	%xmm0, %rbx
	movl	$16, %r8d
	leaq	.LC30(%rip), %rdx
	movq	%rsi, %rcx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	%rbx, %rax
	movq	%rsi, %rcx
	leaq	48(%rsp), %rdx
	shrq	$63, %rax
	movl	%eax, 48(%rsp)
	call	_ZStlsIcSt11char_traitsIcELy1EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	movb	$32, 56(%rsp)
	movq	(%rax), %rdx
	movq	%rax, %rsi
	movq	-24(%rdx), %rcx
	cmpq	$0, 16(%rax,%rcx)
	je	.L771
	leaq	56(%rsp), %rbp
	movl	$1, %r8d
	movq	%rax, %rcx
	movq	%rbp, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	%rax, %rsi
.L772:
	movq	%rbx, %rdi
	movq	%rsi, %rcx
	leaq	52(%rsp), %rdx
	shrq	$52, %rdi
	movq	%rdi, %r8
	andl	$2047, %r8d
	movl	%r8d, 52(%rsp)
	call	_ZStlsIcSt11char_traitsIcELy11EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	movb	$32, 56(%rsp)
	movq	(%rax), %r9
	movq	%rax, %rsi
	movq	-24(%r9), %r10
	cmpq	$0, 16(%rax,%r10)
	je	.L773
	movl	$1, %r8d
	movq	%rbp, %rdx
	movq	%rax, %rcx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	%rax, %rsi
.L774:
	movabsq	$4503599627370495, %r11
	movq	%rbp, %rdx
	movq	%rsi, %rcx
	andq	%r11, %rbx
	movq	%rbx, %rax
	vmovq	%rbx, %xmm1
	shrq	$32, %rax
	vpinsrq	$1, %rax, %xmm1, %xmm0
	vpmovqd	%xmm0, %xmm2
	vmovq	%xmm2, 56(%rsp)
	call	_ZStlsIcSt11char_traitsIcELy52EERSt13basic_ostreamIT_T0_ES6_RKSt6bitsetIXT1_EE
	movb	$32, 47(%rsp)
	movq	(%rax), %rdx
	movq	%rax, %rbp
	movq	-24(%rdx), %rcx
	cmpq	$0, 16(%rax,%rcx)
	je	.L775
	leaq	47(%rsp), %rdx
	movl	$1, %r8d
	movq	%rax, %rcx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	%rax, %rbp
.L776:
	movl	$12, %r8d
	andl	$2047, %edi
	leaq	.LC31(%rip), %rdx
	movq	%rbp, %rcx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	leal	-1023(%rdi), %edx
	movq	%rbp, %rcx
	call	_ZNSolsEi
	movb	$10, 47(%rsp)
	movq	(%rax), %rdi
	movq	%rax, %rcx
	movq	-24(%rdi), %r8
	cmpq	$0, 16(%rax,%r8)
	je	.L777
	leaq	47(%rsp), %rdx
	movl	$1, %r8d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	nop
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
	.p2align 4
	.p2align 3
.L771:
	movl	$32, %edx
	movq	%rax, %rcx
	leaq	56(%rsp), %rbp
	call	_ZNSo3putEc
	jmp	.L772
	.p2align 4
	.p2align 3
.L777:
	movl	$10, %edx
	call	_ZNSo3putEc
	nop
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
	.p2align 4
	.p2align 3
.L775:
	movl	$32, %edx
	movq	%rax, %rcx
	call	_ZNSo3putEc
	jmp	.L776
	.p2align 4
	.p2align 3
.L773:
	movl	$32, %edx
	movq	%rax, %rcx
	call	_ZNSo3putEc
	jmp	.L774
	.seh_endproc
	.section	.text$_ZNSt13random_deviceC1Ev,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt13random_deviceC1Ev
	.def	_ZNSt13random_deviceC1Ev;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt13random_deviceC1Ev
_ZNSt13random_deviceC1Ev:
.LFB6148:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$72, %rsp
	.seh_stackalloc	72
	.seh_endprologue
	leaq	32(%rsp), %rsi
	leaq	48(%rsp), %rbx
	movl	$1634100580, 48(%rsp)
	movq	$7, 40(%rsp)
	movq	%rsi, %rdx
	movq	%rbx, 32(%rsp)
	movl	$1953264993, 51(%rsp)
	movb	$0, 55(%rsp)
.LEHB11:
	call	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE11:
	movq	32(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.L784
	movq	48(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
	nop
.L784:
	addq	$72, %rsp
	popq	%rbx
	popq	%rsi
	ret
.L783:
	movq	%rax, %rbx
	movq	%rsi, %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbx, %rcx
.LEHB12:
	call	_Unwind_Resume
	nop
.LEHE12:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA6148:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6148-.LLSDACSB6148
.LLSDACSB6148:
	.uleb128 .LEHB11-.LFB6148
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L783-.LFB6148
	.uleb128 0
	.uleb128 .LEHB12-.LFB6148
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0
	.uleb128 0
.LLSDACSE6148:
	.section	.text$_ZNSt13random_deviceC1Ev,"x"
	.linkonce discard
	.seh_endproc
	.section	.text.startup,"x"
	.p2align 4
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
.LFB13686:
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$2608, %rsp
	.seh_stackalloc	2608
	.seh_endprologue
	leaq	96(%rsp), %rbx
	call	__main
.LEHB13:
	call	_Z20test_prefix_sum_simdv
	movq	%rbx, %rcx
	call	_ZNSt13random_deviceC1Ev
.LEHE13:
	movq	%rbx, %rcx
.LEHB14:
	call	_ZNSt13random_device9_M_getvalEv
.LEHE14:
	leaq	80(%rsp), %rdx
	leaq	48(%rsp), %rax
	leaq	64(%rsp), %rdi
	leaq	32(%rsp), %rsi
	movq	%rdx, 64(%rsp)
	movq	%rax, 32(%rsp)
	movq	$0, 40(%rsp)
	movb	$0, 48(%rsp)
	movq	$0, 72(%rsp)
	movb	$0, 80(%rsp)
	call	_ZNSt6chrono3_V212system_clock3nowEv
	call	_ZNSt6chrono3_V212system_clock3nowEv
	call	_ZNSt6chrono3_V212system_clock3nowEv
	call	_ZNSt6chrono3_V212system_clock3nowEv
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rdi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rsi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbx, %rcx
	call	_ZNSt13random_device7_M_finiEv
	xorl	%eax, %eax
	addq	$2608, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	ret
.L787:
	movq	%rax, %rsi
	movq	%rbx, %rcx
	vzeroupper
	call	_ZNSt13random_device7_M_finiEv
	movq	%rsi, %rcx
.LEHB15:
	call	_Unwind_Resume
	nop
.LEHE15:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13686:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13686-.LLSDACSB13686
.LLSDACSB13686:
	.uleb128 .LEHB13-.LFB13686
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB14-.LFB13686
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L787-.LFB13686
	.uleb128 0
	.uleb128 .LEHB15-.LFB13686
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0
	.uleb128 0
.LLSDACSE13686:
	.section	.text.startup,"x"
	.seh_endproc
	.section .rdata,"dr"
.LC70:
	.ascii "%s\12\0"
.LC71:
	.ascii "process %d double num\12\0"
	.align 8
.LC74:
	.ascii "all cycle = %lld every cycle=%lf\12\0"
	.align 8
.LC75:
	.ascii "this func duration1= %lf ms one is %lf ns\12\0"
	.text
	.p2align 4
	.globl	_Z10test_func3v
	.def	_Z10test_func3v;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z10test_func3v
_Z10test_func3v:
.LFB13680:
	pushq	%r15
	.seh_pushreg	%r15
	movl	$8360, %eax
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	8360
	vmovaps	%xmm6, 8192(%rsp)
	.seh_savexmm	%xmm6, 8192
	vmovaps	%xmm7, 8208(%rsp)
	.seh_savexmm	%xmm7, 8208
	vmovaps	%xmm8, 8224(%rsp)
	.seh_savexmm	%xmm8, 8224
	vmovaps	%xmm9, 8240(%rsp)
	.seh_savexmm	%xmm9, 8240
	vmovaps	%xmm10, 8256(%rsp)
	.seh_savexmm	%xmm10, 8256
	vmovaps	%xmm11, 8272(%rsp)
	.seh_savexmm	%xmm11, 8272
	vmovaps	%xmm12, 8288(%rsp)
	.seh_savexmm	%xmm12, 8288
	vmovaps	%xmm13, 8304(%rsp)
	.seh_savexmm	%xmm13, 8304
	vmovaps	%xmm14, 8320(%rsp)
	.seh_savexmm	%xmm14, 8320
	vmovaps	%xmm15, 8336(%rsp)
	.seh_savexmm	%xmm15, 8336
	.seh_endprologue
	leaq	2048(%rsp), %rbx
	leaq	3072(%rsp), %rbp
	leaq	5647(%rsp), %rdi
	leaq	2064(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movl	$1634100580, 2064(%rsp)
	andq	$-64, %rdi
	movq	%rsi, 2048(%rsp)
	movl	$1953264993, 2067(%rsp)
	movq	$7, 2056(%rsp)
	movb	$0, 2071(%rsp)
.LEHB16:
	call	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE16:
	movq	2048(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L791
	movq	2064(%rsp), %rdx
	leaq	1(%rdx), %rdx
	call	_ZdlPvy
.L791:
	movq	%rbp, %rcx
.LEHB17:
	call	_ZNSt13random_device9_M_getvalEv
	vmovapd	.LC32(%rip), %zmm6
	vmovapd	.LC33(%rip), %zmm0
	leaq	1536(%rsp), %r14
	leaq	1792(%rsp), %r13
	vmovapd	.LC34(%rip), %zmm1
	vmovapd	.LC35(%rip), %zmm2
	vmovupd	%zmm6, 1280(%rsp)
	vmovupd	%zmm0, 1344(%rsp)
	vmovupd	%zmm1, 1408(%rsp)
	vmovupd	%zmm2, 1472(%rsp)
	vzeroupper
.L794:
	movl	$32, %ecx
	call	_Znay
	movl	$32, %ecx
	leaq	8(%r14), %r12
	movq	%rax, (%r14)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 8(%r14)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 8(%r12)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 16(%r12)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 24(%r12)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 32(%r12)
	call	_Znay
	movl	$32, %ecx
	movq	%rax, 40(%r12)
	call	_Znay
	leaq	56(%r12), %r14
	movq	%rax, 48(%r12)
	cmpq	%r13, %r14
	jne	.L794
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, %r9
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	vmovdqa64	.LC42(%rip), %zmm11
	vmovdqa64	.LC36(%rip), %zmm8
	movl	$11, %ecx
	movabsq	$-9223372036854775808, %r8
	movl	$15, %r10d
	movq	$-48, %r11
	movl	$78913, %esi
	movl	$325, %r12d
	vmovdqa64	.LC39(%rip), %zmm4
	vmovdqa64	.LC41(%rip), %zmm9
	vbroadcastsd	.LC65(%rip), %zmm26
	vpsrlq	$32, .LC84(%rip), %zmm18
	salq	$32, %rdx
	vpbroadcastq	%rcx, %zmm15
	vpbroadcastq	%r8, %zmm0
	orq	%rax, %rdx
	movq	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E8call_num(%rip), %r8
	vmovdqa64	.LC40(%rip), %zmm5
	vmovdqa64	.LC43(%rip), %zmm13
	movq	%rdx, %r15
	leaq	1264(%rsp), %rdx
	movq	%rdx, 88(%rsp)
	movl	$16, %edx
	leaq	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E11short_array(%rip), %rax
	vmovdqa64	.LC57(%rip), %zmm27
	vpbroadcastq	%r10, %zmm14
	movl	$23, %r10d
	vpsllvq	%zmm15, %zmm8, %zmm3
	vporq	%zmm0, %zmm3, %zmm6
	vpsubq	%zmm15, %zmm11, %zmm3
	vmovdqa64	.LC47(%rip), %zmm11
	vpbroadcastq	%r11, %zmm1
	movq	%r8, %rcx
	movl	$359, %r11d
	leaq	1000000(%r8), %r14
	vpsubq	%zmm15, %zmm4, %zmm7
	vpsllvq	%zmm15, %zmm9, %zmm10
	vporq	%zmm0, %zmm10, %zmm4
	vpsrlq	$32, %zmm6, %zmm28
	vmovdqu64	%zmm7, 720(%rsp)
	vpbroadcastq	%r10, %zmm25
	movl	$1741647, %r10d
	vpsrlq	$32, %zmm4, %zmm17
	salq	$5, %rcx
	movq	%r14, 1112(%rsp)
	vmovdqu64	%zmm3, 848(%rsp)
	vpbroadcastq	%r10, %zmm31
	vpsubq	%zmm15, %zmm5, %zmm12
	vpsubq	%zmm15, %zmm14, %zmm5
	movabsq	$10000000000000000, %r10
	vmovdqa64	.LC44(%rip), %zmm10
	vpsubq	%zmm15, %zmm1, %zmm14
	vmovdqa64	.LC49(%rip), %zmm1
	vpsubq	%zmm15, %zmm13, %zmm13
	vmovdqu64	%zmm5, 912(%rsp)
	vpsllvq	%zmm15, %zmm11, %zmm2
	vporq	%zmm0, %zmm2, %zmm22
	vmovdqa64	.LC48(%rip), %zmm0
	vmovdqu64	%zmm22, 976(%rsp)
	vpsrlq	$32, %zmm22, %zmm30
	vpbroadcastq	%rdx, %zmm22
	movl	$1, %edx
	vmovdqu64	%zmm4, 784(%rsp)
	vpbroadcastq	%r10, %zmm21
	vpbroadcastq	%rdx, %zmm4
	vpbroadcastq	%r11, %zmm24
	movl	$63, %r11d
	vmovdqu64	%zmm6, 656(%rsp)
	vpbroadcastq	%r11, %zmm6
	movq	%rbx, %r11
	movq	%r15, %rbx
	vpsubq	%zmm15, %zmm0, %zmm23
	vpbroadcastq	%rsi, %zmm0
	vpsubq	%zmm15, %zmm1, %zmm15
	leaq	1136(%rsp), %rsi
	vpbroadcastq	%r12, %zmm1
	movl	$4294967295, %r12d
	vmovdqu64	%zmm23, 1040(%rsp)
	vpmullq	%zmm7, %zmm0, %zmm7
	vpmullq	%zmm3, %zmm0, %zmm3
	vpmullq	%zmm5, %zmm0, %zmm5
	vpmullq	%zmm23, %zmm0, %zmm0
	vpbroadcastq	%r12, %zmm23
	movl	$64, %r12d
	vpsraq	$18, %zmm7, %zmm2
	vpaddq	%zmm2, %zmm1, %zmm7
	vmovdqu64	%zmm2, 144(%rsp)
	vpsraq	$18, %zmm3, %zmm2
	vmovdqu64	%zmm7, 208(%rsp)
	vpaddq	%zmm2, %zmm1, %zmm7
	vpsraq	$18, %zmm5, %zmm3
	vmovdqu64	%zmm2, 272(%rsp)
	vpaddq	%zmm3, %zmm1, %zmm2
	vmovdqu64	%zmm7, 336(%rsp)
	vpsraq	$18, %zmm0, %zmm7
	vpaddq	%zmm7, %zmm1, %zmm1
	vmovdqu64	%zmm7, 528(%rsp)
	vpbroadcastq	%r12, %zmm7
	vmovdqu64	%zmm3, 400(%rsp)
	vmovdqu64	%zmm2, 464(%rsp)
	vbroadcastsd	.LC54(%rip), %zmm2
	vmovdqu64	%zmm1, 592(%rsp)
	movq	%rdi, 1120(%rsp)
	movq	%rbp, 1128(%rsp)
	movq	%r9, %rdi
	vpxor	%xmm3, %xmm3, %xmm3
	movq	%r13, %rbp
.L795:
	leaq	8(%rcx), %r9
	vpbroadcastq	%rcx, %zmm5
	leaq	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8(%rip), %r13
	leaq	16(%rcx), %r15
	vpbroadcastq	%r9, %zmm1
	leaq	24(%rcx), %r14
	movq	1120(%rsp), %r12
	incq	%r8
	movq	%r8, _ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E8call_num(%rip)
	kxnorb	%k1, %k1, %k1
	leaq	_ZL5_10en(%rip), %r10
	vpxord	%xmm19, %xmm19, %xmm19
	kmovb	%k1, %k3
	vpternlogd	$0xFF, %zmm19, %zmm19, %zmm19
	movl	$11824, %r9d
	movl	$100, %edx
	kmovb	%k1, %k4
	vpaddq	%zmm27, %zmm5, %zmm0
	kmovb	%k1, %k6
	vpaddq	%zmm27, %zmm1, %zmm5
	vmovdqa64	%zmm0, 0(%r13)
	vpbroadcastq	%r15, %zmm0
	leaq	_ZL10powers_ten(%rip), %r13
	movq	%rsi, %r15
	vmovdqa64	%zmm5, 64+_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8(%rip)
	vpbroadcastq	%r14, %zmm5
	vpaddq	%zmm27, %zmm0, %zmm1
	vmovdqa64	%zmm8, 2048(%r12)
	vmovdqa64	%zmm1, 128+_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8(%rip)
	vmovapd	.LC32(%rip), %zmm1
	vmovdqa64	%zmm8, 1792(%r12)
	vpaddq	%zmm27, %zmm5, %zmm0
	vmovdqu64	656(%rsp), %zmm5
	vmovdqa64	%zmm0, 192+_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8(%rip)
	vmovdqu64	720(%rsp), %zmm0
	vmovdqa64	%zmm12, 1280(%r12)
	vmovapd	%zmm1, 2304(%r12)
	vmovapd	.LC33(%rip), %zmm1
	vmovdqa64	%zmm9, 2112(%r12)
	vmovdqa64	%zmm9, 1856(%r12)
	vmovdqa64	%zmm13, 1344(%r12)
	vmovdqa64	%zmm10, 2176(%r12)
	vmovdqa64	%zmm10, 1920(%r12)
	vmovdqa64	%zmm5, 1536(%r12)
	vmovdqu64	784(%rsp), %zmm5
	vmovdqa64	%zmm0, 1024(%r12)
	vmovdqu64	848(%rsp), %zmm0
	vmovapd	%zmm1, 2368(%r12)
	vmovapd	.LC34(%rip), %zmm1
	vmovapd	%zmm1, 2432(%r12)
	vmovdqa64	%zmm5, 1600(%r12)
	vmovdqa64	.LC84(%rip), %zmm5
	vmovdqa64	%zmm0, 1088(%r12)
	vmovdqu64	912(%rsp), %zmm0
	vmovdqa64	%zmm5, 1664(%r12)
	vmovdqa64	%zmm0, 1152(%r12)
	vmovdqa64	%zmm14, 1408(%r12)
	vmovapd	.LC35(%rip), %zmm1
	vmovdqu64	976(%rsp), %zmm5
	vmovdqu64	1040(%rsp), %zmm0
	vmovdqu64	592(%rsp), %zmm29
	vmovdqa64	%zmm11, 2240(%r12)
	vmovdqa64	%zmm11, 1984(%r12)
	vmovdqa64	%zmm15, 1472(%r12)
	vmovapd	%zmm1, 2496(%r12)
	vmovdqu64	208(%rsp), %zmm1
	vmovdqa64	%zmm5, 1728(%r12)
	vmovdqa64	%zmm0, 1216(%r12)
	vmovapd	.LC32(%rip), %zmm0
	vgatherqpd	(%r10,%zmm1,8), %zmm5{%k3}
	vmovdqu64	336(%rsp), %zmm1
	vmovdqa64	%zmm25, 512(%r12)
	vcmppd	$1, %zmm5, %zmm0, %k2
	vmovapd	.LC33(%rip), %zmm0
	kmovb	%k1, %k3
	vmovdqa64	%zmm19, %zmm16{%k2}{z}
	vpaddq	144(%rsp), %zmm16, %zmm20
	vpsubq	%zmm19, %zmm20, %zmm16
	vmovdqa64	%zmm16, 768(%r12)
	vmovdqu64	%zmm16, 1792(%rsp)
	vgatherqpd	(%r10,%zmm1,8), %zmm5{%k4}
	kmovb	%k1, %k4
	vmovdqa64	%zmm25, 576(%r12)
	vcmppd	$1, %zmm5, %zmm0, %k5
	vmovdqa64	%zmm19, %zmm1{%k5}{z}
	vpaddq	272(%rsp), %zmm1, %zmm5
	vmovdqu64	464(%rsp), %zmm1
	vpsubq	%zmm19, %zmm5, %zmm5
	vmovdqa64	%zmm5, 832(%r12)
	vmovdqu64	%zmm5, 1856(%rsp)
	vgatherqpd	(%r10,%zmm1,8), %zmm0{%k6}
	vmovapd	.LC34(%rip), %zmm1
	kmovb	%k1, %k6
	vmovdqa64	%zmm25, 640(%r12)
	vcmppd	$1, %zmm0, %zmm1, %k7
	vmovdqa64	%zmm19, %zmm0{%k7}{z}
	vpaddq	400(%rsp), %zmm0, %zmm1
	vpsubq	%zmm19, %zmm1, %zmm1
	vmovdqa64	%zmm1, 896(%r12)
	vmovdqu64	%zmm1, 1920(%rsp)
	vgatherqpd	(%r10,%zmm29,8), %zmm0{%k3}
	vmovapd	.LC35(%rip), %zmm20
	vmovdqa64	%zmm25, 704(%r12)
	movb	$45, 2048(%rsp)
	movb	$45, 2071(%rsp)
	movb	$45, 2094(%rsp)
	movb	$45, 2117(%rsp)
	movb	$45, 2140(%rsp)
	movb	$45, 2163(%rsp)
	movb	$45, 2186(%rsp)
	movb	$45, 2209(%rsp)
	movb	$45, 2232(%rsp)
	movb	$45, 2255(%rsp)
	movb	$45, 2278(%rsp)
	movb	$45, 2301(%rsp)
	movb	$45, 2324(%rsp)
	movb	$45, 2347(%rsp)
	movb	$45, 2370(%rsp)
	movb	$45, 2393(%rsp)
	movb	$45, 2416(%rsp)
	movb	$45, 2439(%rsp)
	movb	$45, 2462(%rsp)
	movb	$45, 2485(%rsp)
	movb	$45, 2508(%rsp)
	vcmppd	$1, %zmm0, %zmm20, %k2
	movb	$45, 2531(%rsp)
	movb	$45, 2554(%rsp)
	movb	$45, 2577(%rsp)
	movb	$45, 2600(%rsp)
	movb	$45, 2623(%rsp)
	movb	$45, 2646(%rsp)
	kmovb	%k1, %k3
	vmovdqa64	%zmm19, %zmm0{%k2}{z}
	vpaddq	528(%rsp), %zmm0, %zmm0
	vpsubq	%zmm19, %zmm0, %zmm0
	vpsubq	%zmm16, %zmm24, %zmm19
	vpsubq	%zmm16, %zmm22, %zmm16
	vmovdqa64	%zmm0, 960(%r12)
	vmovdqu64	%zmm0, 1984(%rsp)
	movb	$45, 2669(%rsp)
	movb	$45, 2692(%rsp)
	movb	$45, 2715(%rsp)
	movb	$45, 2738(%rsp)
	movl	$736, 1264(%rsp)
	movb	$45, 2761(%rsp)
	vpgatherqq	0(%r13,%zmm19,8), %zmm29{%k4}
	vpsrlq	$32, %zmm29, %zmm20
	vpandq	%zmm23, %zmm29, %zmm29
	vpmuludq	%zmm20, %zmm28, %zmm19
	vpmuludq	%zmm29, %zmm28, %zmm20
	vpsrlq	$32, %zmm20, %zmm29
	vpmullq	%zmm31, %zmm16, %zmm20
	vpaddq	%zmm29, %zmm19, %zmm19
	vpsraq	$19, %zmm20, %zmm29
	vpsubq	%zmm6, %zmm29, %zmm16
	vpaddq	%zmm12, %zmm16, %zmm20
	vpaddq	%zmm7, %zmm20, %zmm29
	vmovdqa64	%zmm29, %zmm16
	vpsubq	%zmm29, %zmm3, %zmm29
	vpternlogq	$0x55, %zmm16, %zmm16, %zmm16
	vpsrlvq	%zmm16, %zmm19, %zmm20
	vpsrlvq	%zmm29, %zmm19, %zmm16
	vpandq	%zmm4, %zmm20, %zmm19
	vpaddq	%zmm16, %zmm19, %zmm29
	vcvtqq2pd	%zmm29, %zmm20
	vmulpd	%zmm26, %zmm20, %zmm16
	vcvttpd2qq	%zmm16, %zmm19
	vpmullq	%zmm21, %zmm19, %zmm20
	vpsubq	%zmm20, %zmm29, %zmm16
	vpcmpq	$1, %zmm3, %zmm16, %k5
	vmovdqa64	%zmm4, %zmm20{%k5}{z}
	vpsubq	%zmm20, %zmm19, %zmm16
	vpmullq	%zmm21, %zmm16, %zmm19
	vpsubq	%zmm19, %zmm29, %zmm29
	vpbroadcastq	%r9, %zmm19
	vmovdqa64	%zmm29, (%r12)
	vpsubq	%zmm5, %zmm24, %zmm29
	vpsubq	%zmm5, %zmm22, %zmm5
	vpmullq	%zmm31, %zmm5, %zmm5
	vpaddq	%zmm19, %zmm16, %zmm16
	vmovdqa64	%zmm16, 256(%r12)
	vpextrq	$1, %xmm16, %r14
	vpsraq	$19, %zmm5, %zmm5
	vpsubq	%zmm6, %zmm5, %zmm5
	vpaddq	%zmm13, %zmm5, %zmm5
	vpaddq	%zmm7, %zmm5, %zmm5
	vpgatherqq	0(%r13,%zmm29,8), %zmm20{%k6}
	vpsrlq	$32, %zmm20, %zmm29
	vpandq	%zmm23, %zmm20, %zmm20
	vpmuludq	%zmm29, %zmm17, %zmm29
	vpmuludq	%zmm20, %zmm17, %zmm20
	vpsrlq	$32, %zmm20, %zmm20
	vpaddq	%zmm20, %zmm29, %zmm20
	vmovdqa64	%zmm5, %zmm29
	vpsubq	%zmm5, %zmm3, %zmm5
	vpsrlvq	%zmm5, %zmm20, %zmm5
	vpternlogq	$0x55, %zmm29, %zmm29, %zmm29
	vpsrlvq	%zmm29, %zmm20, %zmm29
	vpandq	%zmm4, %zmm29, %zmm20
	vpaddq	%zmm5, %zmm20, %zmm20
	vcvtqq2pd	%zmm20, %zmm5
	vmulpd	%zmm26, %zmm5, %zmm5
	vcvttpd2qq	%zmm5, %zmm5
	vpmullq	%zmm21, %zmm5, %zmm29
	vpsubq	%zmm29, %zmm20, %zmm29
	vpcmpq	$1, %zmm3, %zmm29, %k7
	vmovdqa64	%zmm4, %zmm29{%k7}{z}
	vpsubq	%zmm29, %zmm5, %zmm5
	vpmullq	%zmm21, %zmm5, %zmm29
	vpaddq	%zmm19, %zmm5, %zmm5
	vmovdqa64	%zmm5, 320(%r12)
	vpsubq	%zmm29, %zmm20, %zmm20
	vpsubq	%zmm1, %zmm24, %zmm29
	vpsubq	%zmm1, %zmm22, %zmm1
	vpmullq	%zmm31, %zmm1, %zmm1
	vmovdqa64	%zmm20, 64(%r12)
	vpsraq	$19, %zmm1, %zmm1
	vpsubq	%zmm6, %zmm1, %zmm1
	vpaddq	%zmm14, %zmm1, %zmm1
	vpaddq	%zmm7, %zmm1, %zmm1
	vpgatherqq	0(%r13,%zmm29,8), %zmm20{%k3}
	vpsrlq	$32, %zmm20, %zmm29
	vpandq	%zmm23, %zmm20, %zmm20
	vpmuludq	%zmm29, %zmm18, %zmm29
	vpmuludq	%zmm20, %zmm18, %zmm20
	vpsrlq	$32, %zmm20, %zmm20
	vpaddq	%zmm20, %zmm29, %zmm20
	vmovdqa64	%zmm1, %zmm29
	vpsubq	%zmm1, %zmm3, %zmm1
	vpsrlvq	%zmm1, %zmm20, %zmm1
	vpternlogq	$0x55, %zmm29, %zmm29, %zmm29
	vpsrlvq	%zmm29, %zmm20, %zmm29
	vpandq	%zmm4, %zmm29, %zmm20
	vpaddq	%zmm1, %zmm20, %zmm20
	vcvtqq2pd	%zmm20, %zmm1
	vmulpd	%zmm26, %zmm1, %zmm1
	vcvttpd2qq	%zmm1, %zmm1
	vpmullq	%zmm21, %zmm1, %zmm29
	vpsubq	%zmm29, %zmm20, %zmm29
	vpcmpq	$1, %zmm3, %zmm29, %k2
	vmovdqa64	%zmm4, %zmm29{%k2}{z}
	vpsubq	%zmm29, %zmm1, %zmm1
	vpmullq	%zmm21, %zmm1, %zmm29
	vpaddq	%zmm19, %zmm1, %zmm1
	vmovdqa64	%zmm1, 384(%r12)
	vpsubq	%zmm29, %zmm20, %zmm20
	vmovdqa64	%zmm20, 128(%r12)
	vpsubq	%zmm0, %zmm24, %zmm20
	vpsubq	%zmm0, %zmm22, %zmm0
	vpmullq	%zmm31, %zmm0, %zmm0
	vpsraq	$19, %zmm0, %zmm0
	vpsubq	%zmm6, %zmm0, %zmm0
	vpgatherqq	0(%r13,%zmm20,8), %zmm29{%k1}
	vpextrw	$0, %xmm16, 2048(%rsp)
	vpaddq	%zmm15, %zmm0, %zmm0
	movw	%r14w, 2071(%rsp)
	vpaddq	%zmm7, %zmm0, %zmm0
	vpextrq	$1, %xmm5, %r14
	vpextrw	$0, %xmm5, 2232(%rsp)
	movw	%r14w, 2255(%rsp)
	vpextrq	$1, %xmm1, %r14
	vpsrlq	$32, %zmm29, %zmm20
	vpandq	%zmm23, %zmm29, %zmm29
	vpmuludq	%zmm20, %zmm30, %zmm20
	vpmuludq	%zmm29, %zmm30, %zmm29
	vpsrlq	$32, %zmm29, %zmm29
	vpaddq	%zmm29, %zmm20, %zmm20
	vmovdqa64	%zmm0, %zmm29
	vpsubq	%zmm0, %zmm3, %zmm0
	vpsrlvq	%zmm0, %zmm20, %zmm0
	vpternlogq	$0x55, %zmm29, %zmm29, %zmm29
	vpsrlvq	%zmm29, %zmm20, %zmm29
	vpandq	%zmm4, %zmm29, %zmm20
	vpaddq	%zmm0, %zmm20, %zmm20
	vcvtqq2pd	%zmm20, %zmm29
	vmulpd	%zmm26, %zmm29, %zmm29
	vcvttpd2qq	%zmm29, %zmm29
	vpmullq	%zmm21, %zmm29, %zmm0
	vpsubq	%zmm0, %zmm20, %zmm0
	vpcmpq	$1, %zmm3, %zmm0, %k1
	vmovdqa64	%zmm4, %zmm0{%k1}{z}
	vpsubq	%zmm0, %zmm29, %zmm0
	vpmullq	%zmm21, %zmm0, %zmm29
	vpaddq	%zmm19, %zmm0, %zmm0
	valignq	$3, %ymm16, %ymm16, %ymm19
	vmovdqa64	%zmm0, 448(%r12)
	vpsubq	%zmm29, %zmm20, %zmm20
	vmovdqa64	%zmm20, 192(%r12)
	vextracti64x2	$1, %ymm16, %xmm20
	vextracti64x4	$0x1, %zmm16, %ymm16
	vpextrw	$0, %xmm19, 2117(%rsp)
	vpextrq	$1, %xmm16, %r13
	movw	%r13w, 2163(%rsp)
	vpextrw	$0, %xmm16, 2140(%rsp)
	valignq	$3, %ymm16, %ymm16, %ymm19
	vpextrw	$0, %xmm20, 2094(%rsp)
	vextracti64x2	$1, %ymm16, %xmm20
	vpextrw	$0, %xmm19, 2209(%rsp)
	vpextrw	$0, %xmm20, 2186(%rsp)
	valignq	$3, %ymm5, %ymm5, %ymm19
	vextracti64x2	$1, %ymm5, %xmm20
	vextracti64x4	$0x1, %zmm5, %ymm5
	vpextrw	$0, %xmm19, 2301(%rsp)
	vpextrq	$1, %xmm5, %r13
	movw	%r13w, 2347(%rsp)
	vpextrw	$0, %xmm5, 2324(%rsp)
	vextracti64x2	$1, %ymm5, %xmm29
	vpextrw	$0, %xmm20, 2278(%rsp)
	vpbroadcastq	%rdx, %zmm20
	valignq	$3, %ymm5, %ymm5, %ymm5
	vpextrw	$0, %xmm29, 2370(%rsp)
	vpextrw	$0, %xmm5, 2393(%rsp)
	movw	%r14w, 2439(%rsp)
	vpextrq	$1, %xmm0, %r14
	movq	%rbx, 96(%rsp)
	valignq	$3, %ymm1, %ymm1, %ymm5
	movw	%r14w, 2623(%rsp)
	movq	%r8, 104(%rsp)
	movq	%r12, 40(%rsp)
	vpextrw	$0, %xmm1, 2416(%rsp)
	movq	%rcx, 112(%rsp)
	movq	%rsi, 120(%rsp)
	movq	%rdi, 128(%rsp)
	vpextrw	$0, %xmm0, 2600(%rsp)
	movq	%rsi, 136(%rsp)
	movq	%rbp, 1104(%rsp)
	vextracti64x2	$1, %ymm1, %xmm19
	vextracti64x4	$0x1, %zmm1, %ymm1
	vpextrw	$0, %xmm5, 2485(%rsp)
	vpextrq	$1, %xmm1, %r13
	movw	%r13w, 2531(%rsp)
	vpextrw	$0, %xmm1, 2508(%rsp)
	vextracti64x2	$1, %ymm1, %xmm5
	vpextrw	$0, %xmm19, 2462(%rsp)
	vmovdqa32	%zmm18, %zmm19
	vmovdqa32	%zmm17, %zmm18
	vmovdqa32	%zmm28, %zmm17
	vmovdqa32	%zmm30, %zmm28
	valignq	$3, %ymm1, %ymm1, %ymm1
	vpextrw	$0, %xmm5, 2554(%rsp)
	vpextrw	$0, %xmm1, 2577(%rsp)
	valignq	$3, %ymm0, %ymm0, %ymm1
	vextracti64x2	$1, %ymm0, %xmm5
	vextracti64x4	$0x1, %zmm0, %ymm0
	vpextrw	$0, %xmm1, 2669(%rsp)
	vpextrq	$1, %xmm0, %r13
	movw	%r13w, 2715(%rsp)
	vpextrw	$0, %xmm0, 2692(%rsp)
	vextracti64x2	$1, %ymm0, %xmm1
	vpextrw	$0, %xmm5, 2646(%rsp)
	vpextrw	$0, %xmm1, 2738(%rsp)
	vmovdqa32	.LC68(%rip), %zmm5
	vmovdqa32	.LC69(%rip), %zmm1
	valignq	$3, %ymm0, %ymm0, %ymm0
	vpextrw	$0, %xmm0, 2761(%rsp)
	vmovdqu32	%zmm5, 1136(%rsp)
	vmovdqu32	%zmm1, 1200(%rsp)
.L796:
	movq	40(%rsp), %rbx
	movl	(%r15), %edi
	movl	4(%r15), %r8d
	addq	$32, %r15
	movl	-24(%r15), %r9d
	vmovdqa64	(%rbx), %zmm5
	addl	$2, %edi
	leal	2(%r8), %esi
	movl	-20(%r15), %r8d
	movslq	%edi, %rdi
	addl	$2, %r9d
	movslq	%esi, %rsi
	leaq	(%r11,%rdi), %r14
	movslq	%r9d, %rbx
	leaq	(%r11,%rsi), %r12
	vcvtqq2pd	%zmm5, %zmm0
	vmulpd	%zmm2, %zmm0, %zmm1
	vcvttpd2qq	%zmm1, %zmm0
	vpmullq	%zmm20, %zmm0, %zmm1
	vpsubq	%zmm1, %zmm5, %zmm5
	vpcmpq	$1, %zmm3, %zmm5, %k4
	vmovdqa64	%zmm4, %zmm1{%k4}{z}
	vpsubq	%zmm1, %zmm0, %zmm0
	vmovdqa64	%zmm5, %zmm1
	vpaddq	%zmm20, %zmm5, %zmm1{%k4}
	vextracti64x2	$1, %ymm1, %xmm16
	vpextrq	$1, %xmm1, %r10
	vmovq	%xmm1, %rbp
	movzwl	(%rax,%rbp,2), %ecx
	vmovq	%xmm16, %rdx
	movzwl	(%rax,%r10,2), %r13d
	leaq	(%r11,%rbx), %rbp
	movq	%rbp, 48(%rsp)
	leal	2(%r8), %r10d
	valignq	$3, %ymm1, %ymm1, %ymm5
	vextracti64x4	$0x1, %zmm1, %ymm1
	movw	%cx, 14(%r14)
	movw	%r13w, 14(%r12)
	movzwl	(%rax,%rdx,2), %ecx
	vmovq	%xmm5, %r9
	movl	-16(%r15), %edx
	movslq	%r10d, %r10
	leaq	(%r11,%r10), %r13
	movq	%r13, 56(%rsp)
	vextracti64x2	$1, %ymm1, %xmm5
	movw	%cx, 14(%rbp)
	movzwl	(%rax,%r9,2), %ebp
	leal	2(%rdx), %ecx
	movl	-12(%r15), %edx
	movslq	%ecx, %r9
	leaq	(%r11,%r9), %r8
	movq	%r8, 64(%rsp)
	movw	%bp, 14(%r13)
	vmovq	%xmm1, %r13
	leal	2(%rdx), %ecx
	movl	-8(%r15), %edx
	movzwl	(%rax,%r13,2), %ebp
	vpextrq	$1, %xmm1, %r13
	valignq	$3, %ymm1, %ymm1, %ymm1
	movw	%bp, 14(%r8)
	movzwl	(%rax,%r13,2), %ebp
	movslq	%ecx, %r8
	vmovq	%xmm5, %r13
	vcvtqq2pd	%zmm0, %zmm5
	leaq	(%r11,%r8), %rcx
	movq	%rcx, 72(%rsp)
	movw	%bp, 14(%rcx)
	leal	2(%rdx), %ecx
	movzwl	(%rax,%r13,2), %edx
	movslq	%ecx, %rcx
	leaq	(%r11,%rcx), %rbp
	movq	%rbp, 80(%rsp)
	movw	%dx, 14(%rbp)
	movl	-4(%r15), %ebp
	addl	$2, %ebp
	movslq	%ebp, %rdx
	vmovq	%xmm1, %rbp
	vmulpd	%zmm2, %zmm5, %zmm1
	movzwl	(%rax,%rbp,2), %ebp
	leaq	(%r11,%rdx), %r13
	vcvttpd2qq	%zmm1, %zmm5
	vpmullq	%zmm20, %zmm5, %zmm1
	vpsubq	%zmm1, %zmm0, %zmm0
	movw	%bp, 14(%r13)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm30
	movw	%bp, 2060(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm1
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2060(%rsp,%rsi)
	vmovq	%xmm30, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2060(%rsp,%rbx)
	vmovq	%xmm1, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm1
	movw	%bp, 2060(%rsp,%r10)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2060(%rsp,%r9)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%bp, 2060(%rsp,%r8)
	vmovq	%xmm1, %rbp
	vcvtqq2pd	%zmm5, %zmm1
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2060(%rsp,%rcx)
	vmovq	%xmm0, %rbp
	vmulpd	%zmm2, %zmm1, %zmm0
	movzwl	(%rax,%rbp,2), %ebp
	vcvttpd2qq	%zmm0, %zmm1
	vpmullq	%zmm20, %zmm1, %zmm0
	vpsubq	%zmm0, %zmm5, %zmm0
	movw	%bp, 2060(%rsp,%rdx)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm29
	movw	%bp, 2058(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm5
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2058(%rsp,%rsi)
	vmovq	%xmm29, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2058(%rsp,%rbx)
	vmovq	%xmm5, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm5
	movw	%bp, 2058(%rsp,%r10)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2058(%rsp,%r9)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%bp, 2058(%rsp,%r8)
	vmovq	%xmm5, %rbp
	vcvtqq2pd	%zmm1, %zmm5
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2058(%rsp,%rcx)
	vmovq	%xmm0, %rbp
	vmulpd	%zmm2, %zmm5, %zmm0
	movzwl	(%rax,%rbp,2), %ebp
	vcvttpd2qq	%zmm0, %zmm5
	vpmullq	%zmm20, %zmm5, %zmm0
	vpsubq	%zmm0, %zmm1, %zmm0
	movw	%bp, 2058(%rsp,%rdx)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm16
	movw	%bp, 2056(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm1
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2056(%rsp,%rsi)
	vmovq	%xmm16, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2056(%rsp,%rbx)
	vmovq	%xmm1, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm1
	movw	%bp, 2056(%rsp,%r10)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2056(%rsp,%r9)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%bp, 2056(%rsp,%r8)
	vmovq	%xmm1, %rbp
	vcvtqq2pd	%zmm5, %zmm1
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2056(%rsp,%rcx)
	vmovq	%xmm0, %rbp
	vmulpd	%zmm2, %zmm1, %zmm0
	movzwl	(%rax,%rbp,2), %ebp
	vcvttpd2qq	%zmm0, %zmm1
	vpmullq	%zmm20, %zmm1, %zmm0
	vpsubq	%zmm0, %zmm5, %zmm0
	movw	%bp, 2056(%rsp,%rdx)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm30
	movw	%bp, 2054(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm5
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2054(%rsp,%rsi)
	vmovq	%xmm30, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2054(%rsp,%rbx)
	vmovq	%xmm5, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm5
	movw	%bp, 2054(%rsp,%r10)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2054(%rsp,%r9)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%bp, 2054(%rsp,%r8)
	vmovq	%xmm5, %rbp
	vcvtqq2pd	%zmm1, %zmm5
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2054(%rsp,%rcx)
	vmovq	%xmm0, %rbp
	vmulpd	%zmm2, %zmm5, %zmm0
	movzwl	(%rax,%rbp,2), %ebp
	vcvttpd2qq	%zmm0, %zmm5
	vpmullq	%zmm20, %zmm5, %zmm0
	vpsubq	%zmm0, %zmm1, %zmm0
	movw	%bp, 2054(%rsp,%rdx)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm29
	movw	%bp, 2052(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm1
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2052(%rsp,%rsi)
	vmovq	%xmm29, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2052(%rsp,%rbx)
	vmovq	%xmm1, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm1
	movw	%bp, 2052(%rsp,%r10)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2052(%rsp,%r9)
	vpextrq	$1, %xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%bp, 2052(%rsp,%r8)
	vmovq	%xmm1, %rbp
	vcvtqq2pd	%zmm5, %zmm1
	movzwl	(%rax,%rbp,2), %ebp
	movw	%bp, 2052(%rsp,%rcx)
	vmovq	%xmm0, %rbp
	vmulpd	%zmm2, %zmm1, %zmm0
	movzwl	(%rax,%rbp,2), %ebp
	vcvttpd2qq	%zmm0, %zmm1
	vpmullq	%zmm20, %zmm1, %zmm0
	vpsubq	%zmm0, %zmm5, %zmm0
	movw	%bp, 2052(%rsp,%rdx)
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%rbp,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm16
	movw	%bp, 2050(%rsp,%rdi)
	vpextrq	$1, %xmm0, %rdi
	movzwl	(%rax,%rdi,2), %ebp
	valignq	$3, %ymm0, %ymm0, %ymm5
	vextracti64x4	$0x1, %zmm0, %ymm0
	movw	%bp, 2050(%rsp,%rsi)
	vmovq	%xmm16, %rsi
	movzwl	(%rax,%rsi,2), %edi
	movw	%di, 2050(%rsp,%rbx)
	vmovq	%xmm5, %rbx
	movzwl	(%rax,%rbx,2), %ebp
	vextracti64x2	$1, %ymm0, %xmm5
	movw	%bp, 2050(%rsp,%r10)
	vmovq	%xmm0, %r10
	movzwl	(%rax,%r10,2), %esi
	movw	%si, 2050(%rsp,%r9)
	vpextrq	$1, %xmm0, %r9
	movzwl	(%rax,%r9,2), %edi
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%di, 2050(%rsp,%r8)
	vmovq	%xmm5, %r8
	vcvtqq2pd	%zmm1, %zmm5
	movzwl	(%rax,%r8,2), %ebx
	movw	%bx, 2050(%rsp,%rcx)
	vmovq	%xmm0, %rcx
	vmulpd	%zmm2, %zmm5, %zmm0
	movzwl	(%rax,%rcx,2), %ebp
	vcvttpd2qq	%zmm0, %zmm5
	vpmullq	%zmm20, %zmm5, %zmm0
	vpsubq	%zmm0, %zmm1, %zmm0
	movw	%bp, 2050(%rsp,%rdx)
	vmovq	%xmm0, %rdx
	movzwl	(%rax,%rdx,2), %r10d
	valignq	$3, %ymm0, %ymm0, %ymm1
	movw	%r10w, (%r14)
	vpextrq	$1, %xmm0, %r14
	vextracti64x2	$1, %ymm0, %xmm30
	vmovq	%xmm1, %r8
	vextracti64x4	$0x1, %zmm0, %ymm0
	vmovq	%xmm0, %rbp
	movzwl	(%rax,%r14,2), %esi
	movzwl	(%rax,%r8,2), %ebx
	movq	48(%rsp), %rdi
	vpextrq	$1, %xmm0, %r14
	movq	56(%rsp), %rcx
	movzwl	(%rax,%rbp,2), %edx
	movq	64(%rsp), %r10
	movq	80(%rsp), %r8
	movq	88(%rsp), %rbp
	vextracti64x2	$1, %ymm0, %xmm1
	movw	%si, (%r12)
	vmovq	%xmm30, %r12
	movzwl	(%rax,%r12,2), %r9d
	movzwl	(%rax,%r14,2), %esi
	movq	72(%rsp), %r12
	valignq	$3, %ymm0, %ymm0, %ymm0
	movw	%r9w, (%rdi)
	movw	%bx, (%rcx)
	vmovq	%xmm1, %r9
	movw	%dx, (%r10)
	movw	%si, (%r12)
	vmovq	%xmm0, %rbx
	movzwl	(%rax,%r9,2), %edi
	movzwl	(%rax,%rbx,2), %ecx
	movw	%di, (%r8)
	movw	%cx, 0(%r13)
	movq	40(%rsp), %r13
	vmovdqa64	%zmm5, 0(%r13)
	addq	$64, %r13
	movq	%r13, 40(%rsp)
	cmpq	%rbp, %r15
	jne	.L796
	movq	1104(%rsp), %rbp
	movq	96(%rsp), %rbx
	movq	104(%rsp), %r8
	vmovdqa32	%zmm28, %zmm30
	movq	112(%rsp), %rcx
	movq	120(%rsp), %rdx
	movq	128(%rsp), %rdi
	vmovdqa32	%zmm17, %zmm28
	movq	136(%rsp), %rsi
	vmovdqa32	%zmm18, %zmm17
	xorl	%r10d, %r10d
	vmovdqa32	%zmm19, %zmm18
	movq	%rbp, %r9
.L797:
	movq	(%r9), %r15
	leal	18(%r10), %r13d
	movslq	%r13d, %r14
	movl	%r15d, %r12d
	negl	%r12d
	cmovs	%r15d, %r12d
	addq	%r11, %r14
	sarq	$63, %r15
	cmpl	$99, %r12d
	jle	.L802
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L804:
	movq	8(%r9), %r15
	movl	%r10d, (%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	4(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L806
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L808:
	movq	16(%r9), %r15
	movl	%r10d, 4(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	8(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L810
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L812:
	movq	24(%r9), %r15
	movl	%r10d, 8(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	12(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L814
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L816:
	movq	32(%r9), %r15
	movl	%r10d, 12(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	16(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L818
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L820:
	movq	40(%r9), %r15
	movl	%r10d, 16(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	20(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L822
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L824:
	movq	48(%r9), %r15
	movl	%r10d, 20(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	24(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	sarq	$63, %r15
	addq	%r11, %r14
	cmpl	$99, %r12d
	jle	.L826
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
.L828:
	movq	56(%r9), %r15
	movl	%r10d, 24(%rdx)
	movslq	%r10d, %r10
	movb	$10, 2048(%rsp,%r10)
	movl	28(%rdx), %r10d
	movl	%r15d, %r12d
	negl	%r12d
	leal	18(%r10), %r13d
	cmovs	%r15d, %r12d
	movslq	%r13d, %r14
	addq	%r11, %r14
	cmpl	$99, %r12d
	jg	.L867
	movslq	%r12d, %r13
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	sarq	$63, %r15
	andl	$512, %r15d
	sall	$16, %r12d
	addl	$11109, %r15d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
.L799:
	movl	%r10d, 28(%rdx)
	movslq	%r10d, %r10
	addq	$32, %rdx
	movb	$10, 2048(%rsp,%r10)
	cmpq	%rdx, 88(%rsp)
	je	.L801
	movl	(%rdx), %r10d
	addq	$64, %r9
	jmp	.L797
.L802:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L804
.L867:
	sarq	$63, %r15
	andw	$512, %r15w
	leal	20(%r10), %r13d
	addw	$11109, %r15w
	movw	%r15w, (%r14)
	imull	$82, %r12d, %r15d
	movslq	%r13d, %r14
	sarl	$13, %r15d
	leal	48(%r15), %r13d
	imull	$-100, %r15d, %r15d
	movb	%r13b, 2048(%rsp,%r14)
	leal	21(%r10), %r14d
	addl	$23, %r10d
	movslq	%r14d, %r13
	addl	%r12d, %r15d
	movslq	%r15d, %r12
	movzwl	(%rax,%r12,2), %r14d
	movw	%r14w, 2048(%rsp,%r13)
	jmp	.L799
.L826:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L828
.L822:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L824
.L818:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L820
.L814:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L816
.L810:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L812
.L806:
	movslq	%r12d, %r13
	andl	$512, %r15d
	addl	$22, %r10d
	movswl	(%rax,%r13,2), %r12d
	addl	$11109, %r15d
	sall	$16, %r12d
	orl	%r12d, %r15d
	movl	%r15d, (%r14)
	jmp	.L808
.L801:
	movq	1112(%rsp), %rdx
	addq	$32, %rcx
	cmpq	%rdx, %r8
	jne	.L795
	movq	%rbx, %r15
	movq	1128(%rsp), %rbp
	movq	%r11, %rbx
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	salq	$32, %rdx
	movq	%rdx, %rsi
	orq	%rax, %rsi
	vzeroupper
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rbx, %rdx
	leaq	.LC70(%rip), %rcx
	subq	%rdi, %rax
	movq	%rax, %r14
	call	_Z6printfPKcz
	movl	$32000000, %edx
	leaq	.LC71(%rip), %rcx
	call	_Z6printfPKcz
	subq	%r15, %rsi
	vxorps	%xmm6, %xmm6, %xmm6
	vmovsd	.LC72(%rip), %xmm8
	vmovsd	.LC73(%rip), %xmm7
	vcvtsi2sdq	%rsi, %xmm6, %xmm5
	movq	%rsi, %rdx
	vdivsd	%xmm8, %xmm5, %xmm1
	leaq	.LC74(%rip), %rcx
	vmulsd	%xmm7, %xmm1, %xmm0
	vmovq	%xmm0, %r8
	vmovapd	%xmm0, %xmm2
	call	_Z6printfPKcz
	vcvtsi2sdq	%r14, %xmm6, %xmm2
	leaq	.LC75(%rip), %rcx
	vdivsd	%xmm8, %xmm2, %xmm1
	vmulsd	%xmm7, %xmm1, %xmm3
	vmovq	%xmm1, %rdx
	vmovq	%xmm3, %r8
	vmovapd	%xmm3, %xmm2
	call	_Z6printfPKcz
.LEHE17:
	movq	%rbp, %rcx
	call	_ZNSt13random_device7_M_finiEv
	nop
	vmovaps	8192(%rsp), %xmm6
	vmovaps	8208(%rsp), %xmm7
	vmovaps	8224(%rsp), %xmm8
	vmovaps	8240(%rsp), %xmm9
	vmovaps	8256(%rsp), %xmm10
	vmovaps	8272(%rsp), %xmm11
	vmovaps	8288(%rsp), %xmm12
	vmovaps	8304(%rsp), %xmm13
	vmovaps	8320(%rsp), %xmm14
	vmovaps	8336(%rsp), %xmm15
	addq	$8360, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L851:
	movq	%rax, %rbp
	movq	%rbx, %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbp, %rcx
.LEHB18:
	call	_Unwind_Resume
.LEHE18:
.L850:
	movq	%rax, %r13
	movq	%rbp, %rcx
	vzeroupper
	call	_ZNSt13random_device7_M_finiEv
	movq	%r13, %rcx
.LEHB19:
	call	_Unwind_Resume
	nop
.LEHE19:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13680:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13680-.LLSDACSB13680
.LLSDACSB13680:
	.uleb128 .LEHB16-.LFB13680
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L851-.LFB13680
	.uleb128 0
	.uleb128 .LEHB17-.LFB13680
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L850-.LFB13680
	.uleb128 0
	.uleb128 .LEHB18-.LFB13680
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB19-.LFB13680
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0
	.uleb128 0
.LLSDACSE13680:
	.text
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC99:
	.ascii "test_single_double duration1= %lf ms one is %lf ns\12\0"
.LC100:
	.ascii "buffer= %s\12\0"
	.text
	.p2align 4
	.globl	_Z14test_func_exp0v
	.def	_Z14test_func_exp0v;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z14test_func_exp0v
_Z14test_func_exp0v:
.LFB13682:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$3624, %rsp
	.seh_stackalloc	3624
	vmovaps	%xmm6, 3600(%rsp)
	.seh_savexmm	%xmm6, 3600
	.seh_endprologue
	leaq	64(%rsp), %rbx
	leaq	80(%rsp), %rsi
	leaq	1088(%rsp), %rcx
	movl	$1634100580, 80(%rsp)
	movq	%rbx, %rdx
	movq	%rsi, 64(%rsp)
	movl	$1953264993, 83(%rsp)
	movq	$7, 72(%rsp)
	movb	$0, 87(%rsp)
	movq	%rcx, 40(%rsp)
.LEHB20:
	call	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE20:
	movq	64(%rsp), %rcx
	cmpq	%rsi, %rcx
	je	.L869
	movq	80(%rsp), %rax
	leaq	1(%rax), %rdx
	call	_ZdlPvy
.L869:
	movq	40(%rsp), %rcx
.LEHB21:
	call	_ZNSt13random_device9_M_getvalEv
	vxorps	%xmm6, %xmm6, %xmm6
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, 56(%rsp)
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	salq	$32, %rdx
	vmovq	.LC13(%rip), %xmm18
	vmovq	%rbx, %xmm19
	orq	%rax, %rdx
	vmovdqa64	.LC96(%rip), %zmm16
	vmovapd	.LC95(%rip), %zmm17
	vmovapd	.LC97(%rip), %zmm5
	movl	$1, %r11d
	leaq	_ZL5_10en(%rip), %rsi
	leaq	_ZL10powers_ten(%rip), %rdi
	leaq	_ZZ19my_dou_to_sci_pure3ILi16EEidPcE11short_array(%rip), %r12
	movabsq	$1000000000000000, %rbp
	movq	%rdx, 48(%rsp)
	jmp	.L877
	.p2align 4
	.p2align 3
.L889:
	xorl	%r9d, %r9d
	movl	$-1011, %ecx
	cmpq	$2, %r8
	movl	$16, %r10d
	sete	%r9b
	lzcntq	%r8, %r13
	subl	%r13d, %ecx
	movl	$359, %r14d
	imull	$78913, %ecx, %eax
	sarl	$18, %eax
	leal	325(%rax), %edx
	movslq	%edx, %rbx
	movl	$-1074, %edx
	vcomisd	(%rsi,%rbx,8), %xmm1
	sbbl	$-1, %eax
	subl	%r13d, %edx
	movl	%eax, %ecx
	subl	%r9d, %ecx
	subl	%ecx, %r10d
	subl	%ecx, %r14d
	imull	$1741647, %r10d, %r10d
	movslq	%r14d, %r15
	movq	(%rdi,%r15,8), %r14
	sarl	$19, %r10d
	addl	%edx, %r10d
	testq	%r8, %r8
	jne	.L887
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
.L873:
	movq	%r8, %rax
	movl	$-2, %r8d
	movb	$46, 65(%rsp)
	mulq	%r14
	subl	%r10d, %r8d
	notl	%r10d
	movabsq	$5192296858534827629, %rax
	sarl	$31, %ecx
	shrx	%r8, %rdx, %r13
	andl	$1, %r13d
	shrx	%r10, %rdx, %rbx
	andw	$512, %cx
	addq	%rbx, %r13
	addw	$11109, %cx
	imulq	%r13
	movq	%r13, %r14
	movw	%cx, 82(%rsp)
	sarq	$63, %r14
	sarq	$48, %rdx
	subq	%r14, %rdx
	movq	%rdx, %r15
	imull	$103, %edx, %r8d
	imulq	%rbp, %r15
	sarl	$10, %r8d
	subq	%r15, %r13
	imull	$-10, %r8d, %r10d
	addl	$48, %r8d
	vcvtsi2sdq	%r13, %xmm6, %xmm2
	vbroadcastsd	%xmm2, %zmm3
	vpbroadcastq	%r13, %zmm0
	movb	%r8b, 64(%rsp)
	vmulpd	%zmm17, %zmm3, %zmm4
	addl	%r10d, %edx
	vcvttpd2qq	%zmm4, %zmm1
	vpmullq	%zmm16, %zmm1, %zmm2
	vpsubq	%zmm2, %zmm0, %zmm3
	vcvtqq2pd	%zmm3, %zmm4
	vmulpd	%zmm5, %zmm4, %zmm1
	vcvttpd2qq	%zmm1, %zmm0
	cmpl	$99, %r9d
	jg	.L875
	movslq	%r9d, %r14
	movl	$22, %ebx
	movzwl	(%r12,%r14,2), %r15d
	movw	%r15w, 84(%rsp)
.L876:
	vextracti64x2	$1, %ymm0, %xmm4
	vpextrq	$1, %xmm0, %r10
	vmovq	%xmm0, %r8
	valignq	$3, %ymm0, %ymm0, %ymm1
	vextracti64x4	$0x1, %zmm0, %ymm0
	vmovq	%xmm4, %rcx
	vmovq	%xmm0, %r15
	vextracti64x2	$1, %ymm0, %xmm3
	vmovq	%xmm1, %r14
	valignq	$3, %ymm0, %ymm0, %ymm4
	leal	(%rdx,%rdx,4), %edx
	movzwl	(%r12,%r10,2), %r13d
	incl	%r11d
	vmovq	%xmm3, %r10
	leal	(%r8,%rdx,2), %eax
	movzwl	(%r12,%rcx,2), %r8d
	movzwl	(%r12,%r15,2), %ecx
	movzwl	(%r12,%r14,2), %edx
	cltq
	vmovq	%xmm4, %r14
	movzwl	(%r12,%r10,2), %r10d
	movzwl	(%r12,%rax,2), %r9d
	vpextrq	$1, %xmm0, %rax
	movzwl	(%r12,%rax,2), %r15d
	vmovq	%xmm19, %rax
	addq	%rbx, %rax
	movzwl	(%r12,%r14,2), %r14d
	movb	$0, (%rax)
	cmpl	$10000001, %r11d
	je	.L888
.L877:
	vcvtsi2sdl	%r11d, %xmm6, %xmm0
	vandpd	%xmm18, %xmm0, %xmm1
	vmovq	%xmm1, %r8
	movq	%r8, %r9
	shrq	$52, %r9
	je	.L889
	leal	-1023(%r9), %r10d
	movabsq	$-9223372036854775808, %rbx
	movl	$16, %r13d
	imull	$78913, %r10d, %ecx
	salq	$11, %r8
	sarl	$18, %ecx
	leal	325(%rcx), %eax
	orq	%rbx, %r8
	cltq
	vcomisd	(%rsi,%rax,8), %xmm1
	sbbl	$-1, %ecx
	movl	$359, %eax
	subl	%ecx, %r13d
	subl	%ecx, %eax
	imull	$1741647, %r13d, %r15d
	cltq
	movq	(%rdi,%rax,8), %r14
	sarl	$19, %r15d
	leal	-1086(%r15,%r9), %r10d
	movl	%ecx, %r9d
	negl	%r9d
	cmovs	%ecx, %r9d
	jmp	.L873
	.p2align 4
	.p2align 3
.L875:
	imull	$82, %r9d, %r13d
	movl	$23, %ebx
	sarl	$13, %r13d
	imull	$-100, %r13d, %eax
	leal	48(%r13), %ecx
	movb	%cl, 84(%rsp)
	addl	%r9d, %eax
	cltq
	movzwl	(%r12,%rax,2), %r9d
	movw	%r9w, 85(%rsp)
	jmp	.L876
.L888:
	vmovd	%r9d, %xmm5
	movl	%r10d, %edi
	vmovd	%r8d, %xmm1
	vpinsrw	$1, %r13d, %xmm5, %xmm2
	vmovd	%ecx, %xmm0
	vmovd	%edi, %xmm5
	vpinsrw	$1, %edx, %xmm1, %xmm4
	vpinsrw	$1, %r15d, %xmm0, %xmm3
	vpinsrw	$1, %r14d, %xmm5, %xmm1
	vmovq	%xmm19, %r12
	vpunpckldq	%xmm4, %xmm2, %xmm2
	vpunpckldq	%xmm1, %xmm3, %xmm4
	vpunpcklqdq	%xmm4, %xmm2, %xmm0
	vmovdqu16	%xmm0, 66(%rsp)
	movb	$0, (%rax)
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	vzeroupper
	movq	%rdx, %rbx
	movq	%rax, %r13
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	48(%rsp), %r8
	movq	56(%rsp), %r9
	movq	%rbx, %rdx
	leaq	.LC74(%rip), %rcx
	salq	$32, %rdx
	orq	%r13, %rdx
	subq	%r8, %rdx
	subq	%r9, %rax
	vcvtsi2sdq	%rdx, %xmm6, %xmm3
	vdivsd	.LC98(%rip), %xmm3, %xmm5
	vmovq	%xmm5, %r8
	vmovapd	%xmm5, %xmm2
	movq	%rax, %r15
	call	_Z6printfPKcz
	vcvtsi2sdq	%r15, %xmm6, %xmm6
	vdivsd	.LC98(%rip), %xmm6, %xmm1
	vmovq	%xmm1, %r8
	leaq	.LC99(%rip), %rcx
	vdivsd	.LC72(%rip), %xmm6, %xmm1
	vmovq	%r8, %xmm2
	vmovq	%xmm1, %rdx
	call	_Z6printfPKcz
	movq	%r12, %rdx
	leaq	.LC100(%rip), %rcx
	call	_Z6printfPKcz
.LEHE21:
	movq	40(%rsp), %rcx
	call	_ZNSt13random_device7_M_finiEv
	nop
	vmovaps	3600(%rsp), %xmm6
	addq	$3624, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L887:
	subl	%eax, %r9d
	shlx	%r13, %r8, %r8
	jmp	.L873
.L881:
	movq	40(%rsp), %rcx
	movq	%rax, %r14
	vzeroupper
	call	_ZNSt13random_device7_M_finiEv
	movq	%r14, %rcx
.LEHB22:
	call	_Unwind_Resume
.L882:
	movq	%rax, %rdi
	movq	%rbx, %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rdi, %rcx
	call	_Unwind_Resume
	nop
.LEHE22:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13682:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13682-.LLSDACSB13682
.LLSDACSB13682:
	.uleb128 .LEHB20-.LFB13682
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L882-.LFB13682
	.uleb128 0
	.uleb128 .LEHB21-.LFB13682
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L881-.LFB13682
	.uleb128 0
	.uleb128 .LEHB22-.LFB13682
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0
	.uleb128 0
.LLSDACSE13682:
	.text
	.seh_endproc
	.section .rdata,"dr"
.LC102:
	.ascii "error i=%d\12\0"
.LC103:
	.ascii "error exp_plus1 i=%d\12\0"
.LC104:
	.ascii "error exp_sub1 i=%d\12\0"
.LC118:
	.ascii "num=%.16le b=%d\12\0"
	.text
	.p2align 4
	.globl	_Z10test_func4v
	.def	_Z10test_func4v;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z10test_func4v
_Z10test_func4v:
.LFB13681:
	pushq	%r14
	.seh_pushreg	%r14
	movl	$5168, %eax
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	5168
	vmovaps	%xmm6, 5056(%rsp)
	.seh_savexmm	%xmm6, 5056
	vmovaps	%xmm7, 5072(%rsp)
	.seh_savexmm	%xmm7, 5072
	vmovaps	%xmm8, 5088(%rsp)
	.seh_savexmm	%xmm8, 5088
	vmovaps	%xmm9, 5104(%rsp)
	.seh_savexmm	%xmm9, 5104
	vmovaps	%xmm10, 5120(%rsp)
	.seh_savexmm	%xmm10, 5120
	vmovaps	%xmm11, 5136(%rsp)
	.seh_savexmm	%xmm11, 5136
	vmovaps	%xmm12, 5152(%rsp)
	.seh_savexmm	%xmm12, 5152
	.seh_endprologue
	vmovq	.LC13(%rip), %xmm9
	vmovsd	.LC101(%rip), %xmm3
	vxorps	%xmm6, %xmm6, %xmm6
	vmovapd	%xmm3, %xmm4
	vmovapd	%xmm9, %xmm11
	vmovapd	%xmm9, %xmm10
	vmovapd	%xmm9, %xmm8
	vxorpd	%xmm7, %xmm7, %xmm7
	vmovsd	.LC14(%rip), %xmm12
	movq	$-308, %rbx
	leaq	2464+_ZL5_10en(%rip), %rsi
	jmp	.L901
	.p2align 4
	.p2align 3
.L952:
	movq	(%rsi,%rbx,8), %rbp
	vmovsd	.LC14(%rip), %xmm5
	vmovsd	.LC15(%rip), %xmm1
	leaq	_ZL5_10en(%rip), %r13
	leaq	1(%rbp), %r14
	movq	%rbp, %r8
	movq	%r14, %r12
	vmovq	%r14, %xmm3
	shrq	$52, %r12
	vandpd	%xmm8, %xmm3, %xmm0
	vmovq	%rbp, %xmm3
	andl	$2047, %r12d
	subl	$1023, %r12d
	vcomisd	%xmm1, %xmm0
	vandpd	%xmm8, %xmm3, %xmm0
	vcvtsi2sdl	%r12d, %xmm6, %xmm4
	vmulsd	%xmm5, %xmm4, %xmm2
	vcvttsd2sil	%xmm2, %eax
	sbbl	$-1, %eax
	shrq	$52, %r8
	addl	$324, %eax
	andl	$2047, %r8d
	cltq
	subl	$1023, %r8d
	vcomisd	%xmm1, %xmm0
	vmovsd	0(%r13,%rax,8), %xmm4
	vcvtsi2sdl	%r8d, %xmm6, %xmm2
	vmulsd	%xmm5, %xmm2, %xmm5
	vcvttsd2sil	%xmm5, %eax
	sbbl	$-1, %eax
	addl	$324, %eax
	cltq
	vmovsd	0(%r13,%rax,8), %xmm3
.L901:
	vmovsd	(%rsi,%rbx,8), %xmm0
	vmovq	%xmm0, %r9
	movl	%ebx, %edi
	movl	%ebx, %ebp
	leaq	1(%r9), %rcx
	decq	%r9
	vucomisd	%xmm7, %xmm0
	vmovq	%rcx, %xmm2
	vmovq	%r9, %xmm1
	jp	.L920
	movl	$0, %r8d
	je	.L891
.L920:
	vmovq	%xmm0, %rdx
	vandpd	%xmm9, %xmm0, %xmm5
	shrq	$52, %rdx
	andl	$2047, %edx
	subl	$1023, %edx
	vcomisd	.LC15(%rip), %xmm5
	vcvtsi2sdl	%edx, %xmm6, %xmm0
	vmulsd	%xmm12, %xmm0, %xmm0
	vcvttsd2sil	%xmm0, %r8d
	sbbl	$-1, %r8d
	xorl	%r10d, %r10d
	vcomisd	%xmm5, %xmm3
	seta	%r10b
	subl	%r10d, %r8d
.L891:
	vucomisd	%xmm7, %xmm2
	jp	.L921
	movl	$0, %r13d
	je	.L893
.L921:
	shrq	$52, %rcx
	vandpd	%xmm11, %xmm2, %xmm3
	andl	$2047, %ecx
	vcomisd	.LC15(%rip), %xmm3
	leal	-1023(%rcx), %r11d
	vcvtsi2sdl	%r11d, %xmm6, %xmm2
	vmulsd	.LC14(%rip), %xmm2, %xmm5
	vcvttsd2sil	%xmm5, %r13d
	sbbl	$-1, %r13d
	xorl	%r12d, %r12d
	vcomisd	%xmm3, %xmm4
	seta	%r12b
	subl	%r12d, %r13d
.L893:
	vucomisd	%xmm7, %xmm1
	jp	.L922
	movl	$0, %r12d
	je	.L895
.L922:
	shrq	$52, %r9
	vandpd	%xmm10, %xmm1, %xmm4
	andl	$2047, %r9d
	leaq	_ZL5_10en(%rip), %r14
	subl	$1023, %r9d
	vcomisd	.LC15(%rip), %xmm4
	vcvtsi2sdl	%r9d, %xmm6, %xmm1
	vmulsd	.LC14(%rip), %xmm1, %xmm0
	vcvttsd2sil	%xmm0, %r12d
	sbbl	$-1, %r12d
	leal	324(%r12), %eax
	xorl	%r9d, %r9d
	cltq
	vmovsd	(%r14,%rax,8), %xmm3
	vcomisd	%xmm4, %xmm3
	seta	%r9b
	subl	%r9d, %r12d
.L895:
	movl	%ebx, %r14d
	cmpl	%ebx, %r8d
	je	.L897
	movl	%ebp, %edx
	leaq	.LC102(%rip), %rcx
.LEHB23:
	call	_Z6printfPKcz
.L897:
	cmpl	%r14d, %r13d
	je	.L898
	movl	%ebp, %edx
	leaq	.LC103(%rip), %rcx
	call	_Z6printfPKcz
.L898:
	decl	%edi
	cmpl	%edi, %r12d
	je	.L899
	movl	%ebp, %edx
	leaq	.LC104(%rip), %rcx
	call	_Z6printfPKcz
.L899:
	incq	%rbx
	cmpq	$309, %rbx
	jne	.L952
	leaq	2544(%rsp), %rsi
	leaq	32(%rsp), %rbp
	vmovsd	.LC105(%rip), %xmm0
	leaq	2560(%rsp), %rbx
	call	_Z12print_doubled
	vmovsd	.LC106(%rip), %xmm0
	call	_Z12print_doubled
.LEHE23:
	movq	%rsi, %rdx
	movq	%rbp, %rcx
	movl	$1634100580, 2560(%rsp)
	movq	%rbx, 2544(%rsp)
	movl	$1953264993, 2563(%rsp)
	movq	$7, 2552(%rsp)
	movb	$0, 2567(%rsp)
.LEHB24:
	call	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE24:
	movq	2544(%rsp), %rcx
	cmpq	%rbx, %rcx
	je	.L902
	movq	2560(%rsp), %rdx
	leaq	1(%rdx), %rdx
	call	_ZdlPvy
.L902:
	movq	%rbp, %rcx
.LEHB25:
	call	_ZNSt13random_device9_M_getvalEv
	movl	%eax, %ecx
	movabsq	$6364136223846793005, %r8
	movl	$2, %ebx
	movq	%rcx, 2544(%rsp)
	imulq	%r8, %rcx
	leaq	1(%rcx), %rdx
	movq	%rdx, 8(%rsi)
	.p2align 4
	.p2align 3
.L905:
	movq	%rdx, %r10
	shrq	$62, %r10
	xorq	%rdx, %r10
	imulq	%r8, %r10
	addq	%rbx, %r10
	movq	%r10, %r11
	movq	%r10, (%rsi,%rbx,8)
	shrq	$62, %r11
	xorq	%r11, %r10
	imulq	%r8, %r10
	leaq	1(%rbx,%r10), %r13
	movq	%r13, %r12
	movq	%r13, 8(%rsi,%rbx,8)
	shrq	$62, %r12
	xorq	%r13, %r12
	imulq	%r8, %r12
	leaq	2(%rbx,%r12), %r9
	movq	%r9, %rax
	movq	%r9, 16(%rsi,%rbx,8)
	shrq	$62, %rax
	xorq	%r9, %rax
	imulq	%r8, %rax
	leaq	3(%rbx,%rax), %r14
	movq	%r14, %rdi
	movq	%r14, 24(%rsi,%rbx,8)
	shrq	$62, %rdi
	xorq	%r14, %rdi
	imulq	%r8, %rdi
	leaq	4(%rbx,%rdi), %rdx
	movq	%rdx, %rcx
	movq	%rdx, 32(%rsi,%rbx,8)
	shrq	$62, %rcx
	xorq	%rdx, %rcx
	imulq	%r8, %rcx
	leaq	5(%rbx,%rcx), %r10
	movq	%r10, %r11
	movq	%r10, 40(%rsi,%rbx,8)
	shrq	$62, %r11
	xorq	%r10, %r11
	imulq	%r8, %r11
	leaq	6(%rbx,%r11), %r13
	movq	%r13, %r12
	movq	%r13, 48(%rsi,%rbx,8)
	shrq	$62, %r12
	xorq	%r13, %r12
	imulq	%r8, %r12
	leaq	7(%rbx,%r12), %r9
	movq	%r9, %rax
	movq	%r9, 56(%rsi,%rbx,8)
	shrq	$62, %rax
	xorq	%r9, %rax
	imulq	%r8, %rax
	leaq	8(%rbx,%rax), %r14
	movq	%r14, %rdi
	movq	%r14, 64(%rsi,%rbx,8)
	shrq	$62, %rdi
	xorq	%r14, %rdi
	imulq	%r8, %rdi
	leaq	9(%rbx,%rdi), %rdx
	movq	%rdx, 72(%rsi,%rbx,8)
	addq	$10, %rbx
	cmpq	$312, %rbx
	jne	.L905
	movabsq	$-5403634167711393303, %r12
	movl	$64, %r14d
	leaq	.LC118(%rip), %r13
	vmovq	.LC13(%rip), %xmm7
	vpbroadcastq	%r12, %zmm0
	jmp	.L912
	.p2align 4
	.p2align 3
.L954:
	movq	2544(%rsp,%rbx,8), %r9
	incq	%rbx
.L907:
	movq	%r9, %rax
	movabsq	$6148914691236517205, %r8
	movabsq	$8202884508482404352, %rcx
	vxorpd	%xmm8, %xmm8, %xmm8
	shrq	$29, %rax
	andq	%r8, %rax
	xorq	%r9, %rax
	movabsq	$-2270628950310912, %r9
	movq	%rax, %r10
	salq	$17, %r10
	andq	%rcx, %r10
	xorq	%r10, %rax
	movq	%rax, %r11
	salq	$37, %r11
	andq	%r9, %r11
	xorq	%r11, %rax
	movq	%rax, %rdi
	shrq	$43, %rdi
	xorq	%rdi, %rax
	vmovq	%rax, %xmm1
	vucomisd	%xmm8, %xmm1
	jp	.L923
	movl	$0, %r8d
	je	.L910
.L923:
	shrq	$52, %rax
	vandpd	%xmm7, %xmm1, %xmm10
	andl	$2047, %eax
	leaq	_ZL5_10en(%rip), %rdx
	subl	$1023, %eax
	vcomisd	.LC15(%rip), %xmm10
	vcvtsi2sdl	%eax, %xmm6, %xmm0
	vmulsd	.LC14(%rip), %xmm0, %xmm9
	vcvttsd2sil	%xmm9, %r8d
	sbbl	$-1, %r8d
	leal	324(%r8), %eax
	xorl	%r10d, %r10d
	cltq
	vmovsd	(%rdx,%rax,8), %xmm12
	vcomisd	%xmm10, %xmm12
	seta	%r10b
	subl	%r10d, %r8d
.L910:
	vmovq	%xmm1, %rdx
	movq	%r13, %rcx
	vzeroupper
	call	_Z6printfPKcz
.LEHE25:
	decl	%r14d
	vmovdqa64	.LC109(%rip), %zmm0
	je	.L953
.L912:
	cmpq	$312, %rbx
	jne	.L954
	movq	$-2147483648, %r10
	vmovdqu64	1248(%rsi), %zmm5
	movl	$2147483647, %r8d
	movl	$1, %ecx
	vpbroadcastq	%r10, %zmm12
	vpandq	(%rsi), %zmm12, %zmm10
	leaq	3760(%rsp), %rbx
	leaq	64(%rsi), %r11
	vpbroadcastq	%r8, %zmm9
	vpbroadcastq	%rcx, %zmm11
	vpternlogq	$248, 8(%rsi), %zmm9, %zmm10
	vpandq	%zmm11, %zmm10, %zmm2
	vpsrlq	$1, %zmm10, %zmm8
	vpmullq	%zmm0, %zmm2, %zmm4
	vpternlogq	$150, %zmm8, %zmm5, %zmm4
	vmovdqu64	%zmm4, (%rsi)
.L908:
	vpandq	(%r11), %zmm12, %zmm1
	vmovdqu64	1248(%r11), %zmm2
	vpandq	64(%r11), %zmm12, %zmm4
	addq	$384, %r11
	vpternlogq	$248, -376(%r11), %zmm9, %zmm1
	vpternlogq	$248, -312(%r11), %zmm9, %zmm4
	vpandq	%zmm11, %zmm1, %zmm10
	vpsrlq	$1, %zmm1, %zmm3
	vpandq	%zmm11, %zmm4, %zmm5
	vpmullq	%zmm0, %zmm10, %zmm8
	vmovdqu64	928(%r11), %zmm10
	vpmullq	%zmm0, %zmm5, %zmm1
	vmovdqu64	992(%r11), %zmm5
	vpternlogq	$150, %zmm3, %zmm2, %zmm8
	vpandq	-256(%r11), %zmm12, %zmm2
	vpsrlq	$1, %zmm4, %zmm3
	vmovdqu64	%zmm8, -384(%r11)
	vpternlogq	$248, -248(%r11), %zmm9, %zmm2
	vpternlogq	$150, %zmm3, %zmm10, %zmm1
	vmovdqu64	%zmm1, -320(%r11)
	vpandq	%zmm11, %zmm2, %zmm4
	vpandq	-192(%r11), %zmm12, %zmm1
	vpsrlq	$1, %zmm2, %zmm8
	vpmullq	%zmm0, %zmm4, %zmm3
	vmovdqu64	1056(%r11), %zmm4
	vpternlogq	$150, %zmm8, %zmm5, %zmm3
	vmovdqu64	%zmm3, -256(%r11)
	vpandq	-128(%r11), %zmm12, %zmm3
	vpternlogq	$248, -184(%r11), %zmm9, %zmm1
	vpandq	%zmm11, %zmm1, %zmm2
	vpsrlq	$1, %zmm1, %zmm10
	vpmullq	%zmm0, %zmm2, %zmm8
	vmovdqu64	1120(%r11), %zmm2
	vpternlogq	$150, %zmm10, %zmm4, %zmm8
	vpandq	-64(%r11), %zmm12, %zmm4
	vmovdqu64	%zmm8, -192(%r11)
	vpternlogq	$248, -120(%r11), %zmm9, %zmm3
	vpandq	%zmm11, %zmm3, %zmm5
	vpsrlq	$1, %zmm3, %zmm10
	vpmullq	%zmm0, %zmm5, %zmm1
	vmovdqu64	1184(%r11), %zmm5
	vpternlogq	$248, -56(%r11), %zmm9, %zmm4
	vpternlogq	$150, %zmm10, %zmm2, %zmm1
	vpsrlq	$1, %zmm4, %zmm8
	vmovdqu64	%zmm1, -128(%r11)
	vpandq	%zmm11, %zmm4, %zmm3
	vpmullq	%zmm0, %zmm3, %zmm10
	vpternlogq	$150, %zmm8, %zmm5, %zmm10
	vmovdqu64	%zmm10, -64(%r11)
	cmpq	%rbx, %r11
	jne	.L908
	vpbroadcastq	%r10, %ymm12
	vpandq	3760(%rsp), %ymm12, %ymm11
	vpbroadcastq	%r8, %ymm9
	movq	$-2147483648, %rdx
	vpbroadcastq	%rcx, %ymm1
	vpbroadcastq	%rdx, %zmm12
	vpandq	3792(%rsp), %zmm12, %zmm5
	movl	$2147483647, %edi
	vpbroadcastq	%r12, %ymm2
	movl	$1, %ebx
	vmovdqu	5008(%rsp), %ymm10
	leaq	2464(%rsi), %r9
	leaq	3856(%rsp), %rax
	vpternlogq	$248, 3768(%rsp), %ymm9, %ymm11
	vpbroadcastq	%rdi, %zmm9
	vpandq	%ymm1, %ymm11, %ymm8
	vpsrlq	$1, %ymm11, %ymm4
	vpbroadcastq	%rbx, %zmm11
	vpmullq	%ymm2, %ymm8, %ymm3
	vmovdqu64	2544(%rsp), %zmm2
	vpternlogq	$248, 3800(%rsp), %zmm9, %zmm5
	vpandq	%zmm11, %zmm5, %zmm1
	vpternlogq	$150, %ymm4, %ymm10, %ymm3
	vpsrlq	$1, %zmm5, %zmm4
	vpmullq	%zmm0, %zmm1, %zmm8
	vmovdqu	%ymm3, 3760(%rsp)
	vpternlogq	$150, %zmm4, %zmm2, %zmm8
	vmovdqu64	%zmm8, 3792(%rsp)
.L909:
	vpandq	(%rax), %zmm12, %zmm3
	vmovdqu64	-1248(%rax), %zmm1
	vpandq	64(%rax), %zmm12, %zmm2
	addq	$384, %rax
	vpternlogq	$248, -376(%rax), %zmm9, %zmm3
	vpternlogq	$248, -312(%rax), %zmm9, %zmm2
	vpandq	%zmm11, %zmm3, %zmm5
	vpsrlq	$1, %zmm3, %zmm10
	vpandq	%zmm11, %zmm2, %zmm3
	vpsrlq	$1, %zmm2, %zmm8
	vpmullq	%zmm0, %zmm5, %zmm4
	vmovdqu64	-1568(%rax), %zmm5
	vpternlogq	$150, %zmm10, %zmm1, %zmm4
	vpmullq	%zmm0, %zmm3, %zmm10
	vmovdqu64	-1504(%rax), %zmm3
	vmovdqu64	%zmm4, -384(%rax)
	vpandq	-256(%rax), %zmm12, %zmm4
	vpternlogq	$150, %zmm8, %zmm5, %zmm10
	vpternlogq	$248, -248(%rax), %zmm9, %zmm4
	vmovdqu64	%zmm10, -320(%rax)
	vpandq	-192(%rax), %zmm12, %zmm10
	vpandq	%zmm11, %zmm4, %zmm1
	vpsrlq	$1, %zmm4, %zmm2
	vpmullq	%zmm0, %zmm1, %zmm8
	vmovdqu64	-1440(%rax), %zmm1
	vpternlogq	$150, %zmm2, %zmm3, %zmm8
	vpandq	-128(%rax), %zmm12, %zmm3
	vmovdqu64	%zmm8, -256(%rax)
	vpternlogq	$248, -184(%rax), %zmm9, %zmm10
	vpandq	%zmm11, %zmm10, %zmm5
	vpsrlq	$1, %zmm10, %zmm4
	vpmullq	%zmm0, %zmm5, %zmm2
	vmovdqu64	-1376(%rax), %zmm5
	vpternlogq	$150, %zmm4, %zmm1, %zmm2
	vpternlogq	$248, -120(%rax), %zmm9, %zmm3
	vmovdqu64	%zmm2, -192(%rax)
	vpandq	-64(%rax), %zmm12, %zmm2
	vpandq	%zmm11, %zmm3, %zmm10
	vpsrlq	$1, %zmm3, %zmm8
	vpmullq	%zmm0, %zmm10, %zmm4
	vmovdqu64	-1312(%rax), %zmm10
	vpternlogq	$150, %zmm8, %zmm5, %zmm4
	vmovdqu64	%zmm4, -128(%rax)
	vpternlogq	$248, -56(%rax), %zmm9, %zmm2
	vpandq	%zmm11, %zmm2, %zmm1
	vpsrlq	$1, %zmm2, %zmm3
	vpmullq	%zmm0, %zmm1, %zmm8
	vpternlogq	$150, %zmm3, %zmm10, %zmm8
	vmovdqu64	%zmm8, -64(%rax)
	cmpq	%r9, %rax
	jne	.L909
	movq	5032(%rsp), %r8
	movq	2544(%rsp), %r9
	movq	5024(%rsp), %r10
	vpbroadcastq	%rdx, %xmm9
	vpandq	5008(%rsp), %xmm9, %xmm12
	vpbroadcastq	%rdi, %xmm0
	vmovdqa	3760(%rsp), %xmm1
	vpbroadcastq	%rbx, %xmm4
	vpbroadcastq	%r12, %xmm2
	movl	$1, %ebx
	movq	%r8, %rcx
	movq	%r9, %rdi
	andq	$-2147483648, %r10
	andq	$-2147483648, %r8
	andl	$2147483647, %ecx
	andl	$2147483647, %edi
	orq	%rcx, %r10
	orq	%rdi, %r8
	vpternlogq	$248, 5016(%rsp), %xmm0, %xmm12
	movq	%r10, %r11
	movq	%r8, %rdx
	vpandq	%xmm4, %xmm12, %xmm5
	andl	$1, %r10d
	shrq	%r11
	vpsrlq	$1, %xmm12, %xmm11
	vpmullq	%xmm2, %xmm5, %xmm3
	xorq	3776(%rsp), %r11
	andl	$1, %r8d
	negq	%r10
	negq	%r8
	andq	%r12, %r10
	shrq	%rdx
	vpternlogq	$150, %xmm11, %xmm1, %xmm3
	xorq	3784(%rsp), %rdx
	andq	%r12, %r8
	vmovdqa	%xmm3, 5008(%rsp)
	xorq	%r11, %r10
	movq	%r10, 5024(%rsp)
	xorq	%rdx, %r8
	movq	%r8, 5032(%rsp)
	jmp	.L907
.L953:
	movq	%rbp, %rcx
	vzeroupper
	call	_ZNSt13random_device7_M_finiEv
	nop
	vmovaps	5056(%rsp), %xmm6
	vmovaps	5072(%rsp), %xmm7
	vmovaps	5088(%rsp), %xmm8
	vmovaps	5104(%rsp), %xmm9
	vmovaps	5120(%rsp), %xmm10
	vmovaps	5136(%rsp), %xmm11
	vmovaps	5152(%rsp), %xmm12
	addq	$5168, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L919:
	movq	%rax, %rdi
	movq	%rsi, %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rdi, %rcx
.LEHB26:
	call	_Unwind_Resume
.LEHE26:
.L918:
	movq	%rax, %rsi
	movq	%rbp, %rcx
	vzeroupper
	call	_ZNSt13random_device7_M_finiEv
	movq	%rsi, %rcx
.LEHB27:
	call	_Unwind_Resume
	nop
.LEHE27:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13681:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13681-.LLSDACSB13681
.LLSDACSB13681:
	.uleb128 .LEHB23-.LFB13681
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB24-.LFB13681
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L919-.LFB13681
	.uleb128 0
	.uleb128 .LEHB25-.LFB13681
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L918-.LFB13681
	.uleb128 0
	.uleb128 .LEHB26-.LFB13681
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB27-.LFB13681
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0
	.uleb128 0
.LLSDACSE13681:
	.text
	.seh_endproc
	.section	.text$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy:
.LFB14401:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	movq	%rcx, %rbx
	movq	%rdx, %rsi
	leaq	16(%rcx), %rdi
	cmpq	(%rcx), %rdi
	je	.L971
	movq	16(%rcx), %rax
	cmpq	%rdx, %rax
	jb	.L972
.L970:
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L972:
	testq	%rdx, %rdx
	js	.L958
	addq	%rax, %rax
	cmpq	%rax, %rdx
	jb	.L973
.L959:
	movq	%rsi, %rcx
	incq	%rcx
	js	.L961
.L962:
	call	_Znwy
	movq	8(%rbx), %rcx
	movq	(%rbx), %r12
	movq	%rax, %rbp
	leaq	1(%rcx), %r8
	testq	%rcx, %rcx
	je	.L974
	testq	%r8, %r8
	jne	.L975
.L964:
	movq	16(%rbx), %r9
	movq	%r12, %rcx
	leaq	1(%r9), %rdx
	call	_ZdlPvy
.L965:
	movq	%rbp, (%rbx)
	movq	%rsi, 16(%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L975:
	movq	%r12, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r12, %rdi
	jne	.L964
	jmp	.L965
	.p2align 4
	.p2align 3
.L973:
	leaq	1(%rax), %rcx
	movq	%rax, %rsi
	testq	%rax, %rax
	jns	.L962
.L961:
	call	_ZSt17__throw_bad_allocv
	.p2align 4
	.p2align 3
.L971:
	cmpq	$15, %rdx
	jbe	.L970
	testq	%rdx, %rdx
	js	.L958
	movl	$30, %edx
	cmpq	%rdx, %rsi
	cmovb	%rdx, %rsi
	jmp	.L959
	.p2align 4
	.p2align 3
.L974:
	movzbl	(%r12), %r8d
	movb	%r8b, (%rax)
	cmpq	%r12, %rdi
	jne	.L964
	jmp	.L965
.L958:
	leaq	.LC2(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	.def	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_:
.LFB14545:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$24, %rsp
	.seh_stackalloc	24
	.seh_endprologue
	movl	$35, %eax
	movq	%rcx, %r10
	movq	%rdx, %rcx
	lzcntl	%r9d, %edx
	subl	%edx, %eax
	movq	%r8, %rbx
	shrl	$2, %eax
	subq	%rcx, %rbx
	movl	%eax, %r11d
	cmpq	%r11, %rbx
	jl	.L982
	movabsq	$3978425819141910832, %r8
	movabsq	$7378413942531504440, %rsi
	decl	%eax
	movq	%r8, (%rsp)
	movq	%rsi, 8(%rsp)
	cmpl	$255, %r9d
	jbe	.L978
	.p2align 6
	.p2align 4
	.p2align 3
.L979:
	movl	%r9d, %ebx
	movl	%r9d, %esi
	movl	%eax, %edx
	andl	$15, %ebx
	movzbl	(%rsp,%rbx), %r8d
	leal	-1(%rax), %ebx
	subl	$2, %eax
	shrl	$4, %esi
	andl	$15, %esi
	movb	%r8b, (%rcx,%rdx)
	movzbl	(%rsp,%rsi), %edx
	shrl	$8, %r9d
	movb	%dl, (%rcx,%rbx)
	cmpl	$255, %r9d
	ja	.L979
.L978:
	cmpl	$15, %r9d
	ja	.L984
	movl	%r9d, %r9d
	movzbl	(%rsp,%r9), %eax
.L981:
	leaq	(%rcx,%r11), %r8
	movb	%al, (%rcx)
	xorl	%ecx, %ecx
.L977:
	movq	%r10, %rax
	movq	%r8, (%r10)
	movl	%ecx, 8(%r10)
	addq	$24, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L984:
	movl	%r9d, %r8d
	andl	$15, %r8d
	movzbl	(%rsp,%r8), %esi
	shrl	$4, %r9d
	movb	%sil, 1(%rcx)
	movzbl	(%rsp,%r9), %eax
	jmp	.L981
	.p2align 4
	.p2align 3
.L982:
	movl	$132, %ecx
	jmp	.L977
	.seh_endproc
	.section	.text$_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
	.def	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_:
.LFB14548:
	.seh_endprologue
	movq	%rdx, %r10
	movl	$34, %edx
	movl	$2863311531, %r11d
	lzcntl	%r9d, %eax
	subl	%eax, %edx
	movq	%r8, %rax
	imulq	%r11, %rdx
	subq	%r10, %rax
	shrq	$33, %rdx
	movl	%edx, %r11d
	cmpq	%r11, %rax
	jl	.L991
	decl	%edx
	cmpl	$63, %r9d
	jbe	.L987
	.p2align 6
	.p2align 4
	.p2align 3
.L988:
	movl	%r9d, %eax
	movl	%edx, %r8d
	andl	$7, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	movl	%r9d, %eax
	leal	-1(%rdx), %r8d
	subl	$2, %edx
	shrl	$3, %eax
	shrl	$6, %r9d
	andl	$7, %eax
	addl	$48, %eax
	movb	%al, (%r10,%r8)
	cmpl	$63, %r9d
	ja	.L988
.L987:
	leal	48(%r9), %eax
	cmpl	$7, %r9d
	ja	.L993
.L990:
	leaq	(%r10,%r11), %r8
	xorl	%r9d, %r9d
	movb	%al, (%r10)
	movq	%rcx, %rax
	movq	%r8, (%rcx)
	movl	%r9d, 8(%rcx)
	ret
	.p2align 4
	.p2align 3
.L993:
	movl	%r9d, %edx
	andl	$7, %edx
	shrl	$3, %r9d
	addl	$48, %edx
	leal	48(%r9), %eax
	movb	%dl, 1(%r10)
	jmp	.L990
	.p2align 4
	.p2align 3
.L991:
	movl	$132, %r9d
	movq	%rcx, %rax
	movq	%r8, (%rcx)
	movl	%r9d, 8(%rcx)
	ret
	.seh_endproc
	.section .rdata,"dr"
.LC119:
	.ascii "false\0"
.LC120:
	.ascii "true\0"
	.section	.text$_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE
	.def	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE
_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE:
.LFB14582:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	%rdx, %rbx
	movzbl	%r8b, %r8d
	leaq	.L997(%rip), %rdx
	movq	%rcx, %rsi
	movslq	(%rdx,%r8,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section .rdata,"dr"
	.align 4
.L997:
	.long	.L1107-.L997
	.long	.L1005-.L997
	.long	.L1004-.L997
	.long	.L1003-.L997
	.long	.L1002-.L997
	.long	.L1001-.L997
	.long	.L1000-.L997
	.long	.L1107-.L997
	.long	.L1107-.L997
	.long	.L1107-.L997
	.long	.L999-.L997
	.long	.L998-.L997
	.long	.L1107-.L997
	.long	.L1107-.L997
	.long	.L1107-.L997
	.long	.L1107-.L997
	.section	.text$_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L1000:
	movq	(%rcx), %rbp
	cmpq	$9, %rbp
	jbe	.L1041
	movq	%rbp, %rcx
	movl	$1, %r13d
	movabsq	$3777893186295716171, %r9
	jmp	.L1034
	.p2align 4
	.p2align 3
.L1031:
	cmpq	$999, %rcx
	jbe	.L1113
	cmpq	$9999, %rcx
	jbe	.L1114
	movq	%rcx, %rax
	addl	$4, %r13d
	mulq	%r9
	shrq	$11, %rdx
	cmpq	$99999, %rcx
	jbe	.L1115
	movq	%rdx, %rcx
.L1034:
	cmpq	$99, %rcx
	ja	.L1031
	leal	1(%r13), %r12d
	movq	%r12, %r13
.L1030:
	movq	(%rbx), %rdi
	movq	%r12, %rdx
	movq	(%rdi), %rcx
	movq	(%rcx), %rsi
	call	*8(%rsi)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1107
	movq	24(%rax), %rcx
	movq	%rbp, %r8
	movl	%r13d, %edx
	movb	$45, (%rcx)
	call	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
.L1109:
	movq	(%rdi), %r11
	movq	%r12, %rdx
	movq	%rdi, %rcx
	call	*16(%r11)
	movq	8(%rbx), %rbx
	movb	$1, (%rbx)
.L1107:
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L999:
	movq	(%rcx), %rdi
	movq	%rdi, %rcx
	call	strlen
	movq	%rax, %r12
.L1112:
	movq	(%rbx), %rax
	movq	%r12, %rdx
	movq	(%rax), %rcx
	movq	(%rcx), %rbp
	call	*8(%rbp)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1107
	movq	24(%rax), %rcx
	movq	%r12, %r8
	movq	%rdi, %rdx
	call	memcpy
	movq	0(%r13), %r10
	movq	%r12, %rdx
	movq	%r13, %rcx
	call	*16(%r10)
	movq	8(%rbx), %rbx
	movb	$1, (%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L998:
	movq	(%rcx), %r12
	movq	8(%rcx), %rdi
	jmp	.L1112
	.p2align 4
	.p2align 3
.L1005:
	movq	(%rbx), %rax
	movzbl	(%rcx), %edx
	vmovq	.LC121(%rip), %xmm1
	leaq	.LC120(%rip), %r10
	movq	(%rax), %rcx
	xorl	$1, %edx
	movzbl	%dl, %r13d
	vpinsrq	$1, %r10, %xmm1, %xmm0
	addl	$4, %r13d
	vmovdqa	%xmm0, 32(%rsp)
	movslq	%r13d, %rbp
	movq	%rbp, %rdx
	movq	(%rcx), %rdi
	call	*8(%rdi)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1107
	movzbl	(%rsi), %esi
	movq	24(%rax), %r11
	movq	32(%rsp,%rsi,8), %r12
	cmpl	$8, %r13d
	jb	.L1116
	movq	(%r12), %rdi
	movl	%r13d, %esi
	leaq	8(%r11), %r9
	andq	$-8, %r9
	movq	%rdi, (%r11)
	movq	-8(%r12,%rsi), %r8
	movq	%r8, -8(%r11,%rsi)
	subq	%r9, %r11
	addl	%r11d, %r13d
	subq	%r11, %r12
	movl	%r13d, %edi
	andl	$-8, %edi
	cmpl	$8, %edi
	jb	.L1008
	xorl	%edx, %edx
	leal	-1(%rdi), %r11d
	movl	$8, %eax
	movq	(%r12,%rdx), %r10
	shrl	$3, %r11d
	andl	$7, %r11d
	movq	%r10, (%r9,%rdx)
	cmpl	%edi, %eax
	jnb	.L1008
	testl	%r11d, %r11d
	je	.L1011
	cmpl	$1, %r11d
	je	.L1085
	cmpl	$2, %r11d
	je	.L1086
	cmpl	$3, %r11d
	je	.L1087
	cmpl	$4, %r11d
	je	.L1088
	cmpl	$5, %r11d
	je	.L1089
	cmpl	$6, %r11d
	je	.L1090
	movq	(%r12,%rax), %r13
	movq	%r13, (%r9,%rax)
	movl	$16, %eax
.L1090:
	movl	%eax, %esi
	addl	$8, %eax
	movq	(%r12,%rsi), %r8
	movq	%r8, (%r9,%rsi)
.L1089:
	movl	%eax, %r11d
	addl	$8, %eax
	movq	(%r12,%r11), %rdx
	movq	%rdx, (%r9,%r11)
.L1088:
	movl	%eax, %r13d
	addl	$8, %eax
	movq	(%r12,%r13), %r10
	movq	%r10, (%r9,%r13)
.L1087:
	movl	%eax, %esi
	addl	$8, %eax
	movq	(%r12,%rsi), %r8
	movq	%r8, (%r9,%rsi)
.L1086:
	movl	%eax, %r11d
	addl	$8, %eax
	movq	(%r12,%r11), %rdx
	movq	%rdx, (%r9,%r11)
.L1085:
	movl	%eax, %r13d
	addl	$8, %eax
	movq	(%r12,%r13), %r10
	movq	%r10, (%r9,%r13)
	cmpl	%edi, %eax
	jnb	.L1008
.L1011:
	movl	%eax, %esi
	leal	8(%rax), %r11d
	leal	16(%rax), %r13d
	movq	(%r12,%rsi), %r8
	movq	%r8, (%r9,%rsi)
	movq	(%r12,%r11), %rdx
	leal	24(%rax), %esi
	movq	%rdx, (%r9,%r11)
	movq	(%r12,%r13), %r10
	leal	32(%rax), %r11d
	movq	%r10, (%r9,%r13)
	movq	(%r12,%rsi), %r8
	leal	40(%rax), %r13d
	movq	%r8, (%r9,%rsi)
	movq	(%r12,%r11), %rdx
	leal	48(%rax), %esi
	movq	%rdx, (%r9,%r11)
	movq	(%r12,%r13), %r10
	leal	56(%rax), %r11d
	addl	$64, %eax
	movq	%r10, (%r9,%r13)
	movq	(%r12,%rsi), %r8
	movq	%r8, (%r9,%rsi)
	movq	(%r12,%r11), %rdx
	movq	%rdx, (%r9,%r11)
	cmpl	%edi, %eax
	jb	.L1011
	jmp	.L1008
	.p2align 4
	.p2align 3
.L1004:
	movq	(%rbx), %r13
	movl	$1, %edx
	movq	0(%r13), %rcx
	movq	(%rcx), %rdi
	call	*8(%rdi)
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1107
	movzbl	(%rsi), %r9d
	movq	24(%rax), %rbp
	movl	$1, %edx
	movb	%r9b, 0(%rbp)
	movq	(%rax), %r12
	call	*16(%r12)
	movq	8(%rbx), %rbx
	movb	$1, (%rbx)
	jmp	.L1107
	.p2align 4
	.p2align 3
.L1003:
	movl	(%rcx), %r8d
	movl	%r8d, %r12d
	movl	%r8d, %ebp
	shrl	$31, %r12d
	negl	%ebp
	cmovs	%r8d, %ebp
	cmpl	$9, %ebp
	jbe	.L1035
	movl	%ebp, %edx
	movl	$1, %esi
	movl	$3518437209, %edi
	jmp	.L1018
	.p2align 4
	.p2align 3
.L1015:
	cmpl	$999, %edx
	jbe	.L1117
	cmpl	$9999, %edx
	jbe	.L1118
	movl	%edx, %r9d
	addl	$4, %esi
	imulq	%rdi, %r9
	shrq	$45, %r9
	cmpl	$99999, %edx
	jbe	.L1014
	movl	%r9d, %edx
.L1018:
	cmpl	$99, %edx
	ja	.L1015
	incl	%esi
.L1014:
	movq	(%rbx), %r10
	leal	(%r12,%rsi), %r13d
	movq	%r13, %rdx
	movq	(%r10), %rcx
	movq	(%rcx), %rax
	call	*8(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1107
	movq	24(%rax), %rcx
	movzbl	%r12b, %r11d
	movl	%ebp, %r8d
	movl	%esi, %edx
	movb	$45, (%rcx)
	addq	%r11, %rcx
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
.L1108:
	movq	(%rdi), %r8
	movq	%rdi, %rcx
	movq	%r13, %rdx
	call	*16(%r8)
	movq	8(%rbx), %rcx
	movb	$1, (%rcx)
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L1002:
	movl	(%rcx), %ebp
	cmpl	$9, %ebp
	jbe	.L1037
	movl	%ebp, %edx
	movl	$1, %esi
	movl	$3518437209, %ecx
	jmp	.L1024
	.p2align 4
	.p2align 3
.L1021:
	cmpl	$999, %edx
	jbe	.L1119
	cmpl	$9999, %edx
	jbe	.L1120
	movl	%edx, %r10d
	addl	$4, %esi
	imulq	%rcx, %r10
	shrq	$45, %r10
	cmpl	$99999, %edx
	jbe	.L1121
	movl	%r10d, %edx
.L1024:
	cmpl	$99, %edx
	ja	.L1021
	leal	1(%rsi), %r12d
	movq	%r12, %rsi
.L1020:
	movq	(%rbx), %rax
	movq	%r12, %rdx
	movq	(%rax), %rcx
	movq	(%rcx), %r13
	call	*8(%r13)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1107
	movq	24(%rax), %rcx
	movl	%ebp, %r8d
	movl	%esi, %edx
	movb	$45, (%rcx)
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
	jmp	.L1109
	.p2align 4
	.p2align 3
.L1001:
	movq	(%rcx), %r10
	movq	%r10, %r12
	movq	%r10, %rbp
	shrq	$63, %r12
	negq	%rbp
	cmovs	%r10, %rbp
	cmpq	$9, %rbp
	jbe	.L1039
	movq	%rbp, %r8
	movl	$1, %esi
	movabsq	$3777893186295716171, %r11
	jmp	.L1029
	.p2align 4
	.p2align 3
.L1026:
	cmpq	$999, %r8
	jbe	.L1122
	cmpq	$9999, %r8
	jbe	.L1123
	movq	%r8, %rax
	addl	$4, %esi
	mulq	%r11
	shrq	$11, %rdx
	cmpq	$99999, %r8
	jbe	.L1025
	movq	%rdx, %r8
.L1029:
	cmpq	$99, %r8
	ja	.L1026
	incl	%esi
.L1025:
	movq	(%rbx), %r9
	leal	(%rsi,%r12), %r13d
	movq	%r13, %rdx
	movq	(%r9), %rcx
	movq	(%rcx), %rdi
	call	*8(%rdi)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1107
	movq	24(%rax), %rcx
	movq	%rbp, %r8
	movl	%esi, %edx
	movb	$45, (%rcx)
	addq	%r12, %rcx
	call	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
	jmp	.L1108
	.p2align 4
	.p2align 3
.L1116:
	testb	$4, %r13b
	jne	.L1124
	testl	%r13d, %r13d
	je	.L1008
	movzbl	(%r12), %r10d
	movb	%r10b, (%r11)
	testb	$2, %r13b
	jne	.L1125
.L1008:
	movq	(%rcx), %r9
	movq	%rbp, %rdx
	call	*16(%r9)
	movq	8(%rbx), %rcx
	movb	$1, (%rcx)
	jmp	.L1107
	.p2align 4
	.p2align 3
.L1117:
	addl	$2, %esi
	jmp	.L1014
	.p2align 4
	.p2align 3
.L1119:
	leal	2(%rsi), %r12d
	movq	%r12, %rsi
	jmp	.L1020
	.p2align 4
	.p2align 3
.L1113:
	leal	2(%r13), %r12d
	movq	%r12, %r13
	jmp	.L1030
	.p2align 4
	.p2align 3
.L1122:
	addl	$2, %esi
	jmp	.L1025
	.p2align 4
	.p2align 3
.L1118:
	addl	$3, %esi
	jmp	.L1014
	.p2align 4
	.p2align 3
.L1120:
	leal	3(%rsi), %r12d
	movq	%r12, %rsi
	jmp	.L1020
	.p2align 4
	.p2align 3
.L1123:
	addl	$3, %esi
	jmp	.L1025
	.p2align 4
	.p2align 3
.L1114:
	leal	3(%r13), %r12d
	movq	%r12, %r13
	jmp	.L1030
	.p2align 4
	.p2align 3
.L1115:
	movl	%r13d, %r12d
	jmp	.L1030
	.p2align 4
	.p2align 3
.L1121:
	movl	%esi, %r12d
	jmp	.L1020
.L1041:
	movl	$1, %r12d
	movl	$1, %r13d
	jmp	.L1030
.L1039:
	movl	$1, %esi
	jmp	.L1025
.L1037:
	movl	$1, %r12d
	movl	$1, %esi
	jmp	.L1020
.L1035:
	movl	$1, %esi
	jmp	.L1014
.L1124:
	movl	(%r12), %r8d
	movl	%r13d, %r9d
	movl	%r8d, (%r11)
	movl	-4(%r12,%r9), %edx
	movl	%edx, -4(%r11,%r9)
	jmp	.L1008
.L1125:
	movl	%r13d, %r13d
	movzwl	-2(%r12,%r13), %eax
	movw	%ax, -2(%r11,%r13)
	jmp	.L1008
	.seh_endproc
	.section	.text$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy:
.LFB15002:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	8(%rcx), %rax
	movq	144(%rsp), %rsi
	leaq	(%rdx,%r8), %r13
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%r9, %rbp
	leaq	16(%rcx), %r14
	movq	%rax, %r15
	subq	%r8, %rsi
	subq	%r13, %r15
	addq	%rax, %rsi
	cmpq	(%rcx), %r14
	je	.L1153
	movq	16(%rcx), %rdx
	testq	%rsi, %rsi
	js	.L1128
	cmpq	%rsi, %rdx
	jnb	.L1130
	addq	%rdx, %rdx
	cmpq	%rdx, %rsi
	jnb	.L1130
	testq	%rdx, %rdx
	jns	.L1154
.L1132:
	call	_ZSt17__throw_bad_allocv
	.p2align 4
	.p2align 3
.L1153:
	testq	%rsi, %rsi
	js	.L1128
	leaq	-16(%rsi), %rcx
	cmpq	$13, %rcx
	jbe	.L1155
	.p2align 4
	.p2align 3
.L1130:
	movq	%rsi, %rcx
	incq	%rcx
	js	.L1132
.L1129:
	call	_Znwy
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L1133
	movq	(%rbx), %rdx
	cmpq	$1, %r12
	je	.L1156
	movq	%r12, %r8
	movq	%rax, %rcx
	call	memcpy
.L1133:
	testq	%rbp, %rbp
	je	.L1135
	cmpq	$0, 144(%rsp)
	je	.L1135
	cmpq	$1, 144(%rsp)
	leaq	(%rdi,%r12), %rcx
	je	.L1157
	movq	144(%rsp), %r8
	movq	%rbp, %rdx
	call	memcpy
.L1135:
	movq	(%rbx), %rbp
	testq	%r15, %r15
	jne	.L1158
.L1137:
	cmpq	%rbp, %r14
	je	.L1139
	movq	16(%rbx), %r11
	movq	%rbp, %rcx
	leaq	1(%r11), %rdx
	call	_ZdlPvy
.L1139:
	movq	%rdi, (%rbx)
	movq	%rsi, 16(%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L1158:
	movq	144(%rsp), %rcx
	leaq	0(%rbp,%r13), %rdx
	addq	%r12, %rcx
	addq	%rdi, %rcx
	cmpq	$1, %r15
	je	.L1159
	movq	%r15, %r8
	call	memcpy
	jmp	.L1137
	.p2align 4
	.p2align 3
.L1154:
	leaq	1(%rdx), %rcx
	movq	%rdx, %rsi
	jmp	.L1129
	.p2align 4
	.p2align 3
.L1156:
	movzbl	(%rdx), %r8d
	movb	%r8b, (%rax)
	jmp	.L1133
	.p2align 4
	.p2align 3
.L1157:
	movzbl	0(%rbp), %r9d
	movq	(%rbx), %rbp
	movb	%r9b, (%rcx)
	testq	%r15, %r15
	je	.L1137
	jmp	.L1158
	.p2align 4
	.p2align 3
.L1159:
	movzbl	(%rdx), %r10d
	movb	%r10b, (%rcx)
	jmp	.L1137
	.p2align 4
	.p2align 3
.L1155:
	movl	$30, %esi
	movl	$31, %ecx
	jmp	.L1129
.L1128:
	leaq	.LC2(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC122:
	.ascii "basic_string::_M_replace\0"
	.text
	.align 2
	.p2align 4
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0:
.LFB15934:
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	%rcx, %rbx
	movq	8(%rcx), %rcx
	movq	128(%rsp), %rdi
	movabsq	$9223372036854775807, %rax
	addq	%r8, %rax
	movq	%r9, %rbp
	subq	%rcx, %rax
	cmpq	%rdi, %rax
	jb	.L1179
	movq	(%rbx), %r10
	movq	%rdi, %rsi
	leaq	16(%rbx), %r9
	subq	%r8, %rsi
	addq	%rcx, %rsi
	cmpq	%r9, %r10
	je	.L1169
	movq	16(%rbx), %r11
.L1162:
	cmpq	%rsi, %r11
	jb	.L1163
	leaq	(%r10,%rdx), %r12
	movq	%rcx, %rax
	addq	%r8, %rdx
	subq	%rdx, %rax
	cmpq	%r10, %rbp
	jnb	.L1180
.L1164:
	testq	%rax, %rax
	je	.L1166
	cmpq	%rdi, %r8
	je	.L1166
	leaq	(%r12,%r8), %rdx
	leaq	(%r12,%rdi), %rcx
	cmpq	$1, %rax
	je	.L1181
	movq	%rax, %r8
	call	memmove
.L1166:
	testq	%rdi, %rdi
	je	.L1165
.L1183:
	cmpq	$1, %rdi
	je	.L1182
	movq	%rdi, %r8
	movq	%rbp, %rdx
	movq	%r12, %rcx
	call	memcpy
.L1165:
	movq	%rsi, 8(%rbx)
	movq	(%rbx), %rbx
	movb	$0, (%rbx,%rsi)
	addq	$48, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	ret
	.p2align 4
	.p2align 3
.L1180:
	addq	%rcx, %r10
	cmpq	%rbp, %r10
	jb	.L1164
	movq	%rbp, %r9
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%rax, 40(%rsp)
	movq	%rdi, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE15_M_replace_coldEPcyPKcyy
	jmp	.L1165
	.p2align 4
	.p2align 3
.L1163:
	movq	%rbp, %r9
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	jmp	.L1165
	.p2align 4
	.p2align 3
.L1169:
	movl	$15, %r11d
	jmp	.L1162
	.p2align 4
	.p2align 3
.L1181:
	movzbl	(%rdx), %edx
	movb	%dl, (%rcx)
	testq	%rdi, %rdi
	je	.L1165
	jmp	.L1183
	.p2align 4
	.p2align 3
.L1182:
	movzbl	0(%rbp), %r8d
	movb	%r8b, (%r12)
	jmp	.L1165
.L1179:
	leaq	.LC122(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.p2align 4
	.globl	_Z18doubleToScientificB5cxx11di
	.def	_Z18doubleToScientificB5cxx11di;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z18doubleToScientificB5cxx11di
_Z18doubleToScientificB5cxx11di:
.LFB13463:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$568, %rsp
	.seh_stackalloc	568
	vmovaps	%xmm6, 496(%rsp)
	.seh_savexmm	%xmm6, 496
	vmovaps	%xmm7, 512(%rsp)
	.seh_savexmm	%xmm7, 512
	vmovaps	%xmm8, 528(%rsp)
	.seh_savexmm	%xmm8, 528
	vmovaps	%xmm9, 544(%rsp)
	.seh_savexmm	%xmm9, 544
	.seh_endprologue
	movq	.refptr._ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdx
	leaq	224(%rsp), %r15
	leaq	96(%rsp), %r14
	movq	%rcx, %rbx
	movq	%r15, %rcx
	vmovapd	%xmm1, %xmm7
	movq	%r14, 72(%rsp)
	movslq	%r8d, %rdi
	leaq	64(%rsi), %rax
	addq	$16, %rdx
	vmovq	%rax, %xmm6
	vpinsrq	$1, %rdx, %xmm6, %xmm8
	call	_ZNSt8ios_baseC2Ev
	movq	.refptr._ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r13
	movq	.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rcx
	movw	$0, 448(%rsp)
	vpxor	%xmm0, %xmm0, %xmm0
	vmovdqu	%ymm0, 456(%rsp)
	movq	$0, 440(%rsp)
	xorl	%edx, %edx
	movq	16(%r13), %rbp
	movq	24(%r13), %r8
	addq	$16, %rcx
	movq	%rcx, 224(%rsp)
	movq	-24(%rbp), %r9
	movq	%rbp, 96(%rsp)
	movq	%r8, 48(%rsp)
	movq	%r8, 96(%rsp,%r9)
	movq	$0, 104(%rsp)
	movq	-24(%rbp), %rcx
	addq	%r14, %rcx
	vzeroupper
.LEHB28:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E
.LEHE28:
	movq	32(%r13), %r12
	movq	40(%r13), %rax
	leaq	112(%rsp), %r10
	xorl	%edx, %edx
	vmovq	%r10, %xmm9
	movq	-24(%r12), %r11
	movq	%r12, 112(%rsp)
	movq	%rax, 56(%rsp)
	leaq	112(%rsp,%r11), %rcx
	movq	%rax, (%rcx)
.LEHB29:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E
.LEHE29:
	movq	8(%r13), %r14
	movq	48(%r13), %r13
	leaq	24(%rsi), %rcx
	vpxor	%xmm1, %xmm1, %xmm1
	leaq	104(%rsi), %r8
	movq	-24(%r14), %rdx
	movq	%r13, 96(%rsp,%rdx)
	vmovdqa	%xmm8, 112(%rsp)
	movq	%rcx, 96(%rsp)
	leaq	176(%rsp), %rcx
	vmovdqu	%ymm1, 128(%rsp)
	movq	%rcx, 64(%rsp)
	movq	%r8, 224(%rsp)
	movq	$0, 160(%rsp)
	movq	$0, 168(%rsp)
	vzeroupper
	call	_ZNSt6localeC1Ev
	movq	.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %r9
	leaq	208(%rsp), %r10
	leaq	120(%rsp), %rdx
	movq	%r15, %rcx
	movl	$24, 184(%rsp)
	vmovq	%r10, %xmm8
	movq	%r10, 192(%rsp)
	movq	$0, 200(%rsp)
	movb	$0, 208(%rsp)
	addq	$16, %r9
	movq	%r9, 120(%rsp)
.LEHB30:
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E
.LEHE30:
	movq	112(%rsp), %r11
	vmovq	%xmm9, %rcx
	vmovapd	%xmm7, %xmm1
	movq	-24(%r11), %rdx
	addq	%rcx, %rdx
	movl	24(%rdx), %eax
	movq	%rdi, 8(%rdx)
	andl	$-261, %eax
	orb	$1, %ah
	movl	%eax, 24(%rdx)
.LEHB31:
	call	_ZNSo9_M_insertIdEERSoT_
.LEHE31:
	movq	160(%rsp), %rdi
	leaq	16(%rbx), %r11
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	movq	%r11, (%rbx)
	testq	%rdi, %rdi
	je	.L1189
	movq	144(%rsp), %rdx
	movq	152(%rsp), %r9
	movq	%rdi, %rax
	movq	%rbx, %rcx
	cmpq	%rdi, %rdx
	cmovnb	%rdx, %rdi
	testq	%rdx, %rdx
	cmove	%rax, %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	subq	%r9, %rdi
	movq	%rdi, 32(%rsp)
.LEHB32:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
.L1191:
	leaq	24(%rsi), %rdi
	addq	$104, %rsi
	movq	192(%rsp), %rcx
	vmovq	%xmm8, %r8
	movq	%rsi, 224(%rsp)
	movq	.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rsi
	movq	%rdi, 96(%rsp)
	addq	$16, %rsi
	vpinsrq	$1, %rsi, %xmm6, %xmm2
	vmovdqa	%xmm2, 112(%rsp)
	cmpq	%r8, %rcx
	je	.L1200
	movq	208(%rsp), %r9
	leaq	1(%r9), %rdx
	call	_ZdlPvy
.L1200:
	movq	.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %r10
	movq	64(%rsp), %rcx
	addq	$16, %r10
	movq	%r10, 120(%rsp)
	call	_ZNSt6localeD1Ev
	movq	-24(%r14), %r14
	movq	48(%rsp), %rcx
	movq	.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r11
	movq	%r13, 96(%rsp,%r14)
	movq	-24(%r12), %r13
	movq	%r12, 112(%rsp)
	movq	56(%rsp), %r12
	addq	$16, %r11
	movq	%r12, 112(%rsp,%r13)
	movq	%rbp, 96(%rsp)
	movq	-24(%rbp), %rbp
	movq	%rcx, 96(%rsp,%rbp)
	movq	%r15, %rcx
	movq	$0, 104(%rsp)
	movq	%r11, 224(%rsp)
	call	_ZNSt8ios_baseD2Ev
	nop
	vmovaps	496(%rsp), %xmm6
	vmovaps	512(%rsp), %xmm7
	vmovaps	528(%rsp), %xmm8
	vmovaps	544(%rsp), %xmm9
	movq	%rbx, %rax
	addq	$568, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L1189:
	movq	200(%rsp), %rdi
	cmpq	$15, %rdi
	ja	.L1214
	testq	%rdi, %rdi
	je	.L1198
	movq	192(%rsp), %rdx
	cmpq	$1, %rdi
	jne	.L1196
	movzbl	(%rdx), %r8d
	movb	%r8b, 16(%rbx)
.L1198:
	movq	%rdi, 8(%rbx)
	movb	$0, (%r11,%rdi)
	jmp	.L1191
	.p2align 4
	.p2align 3
.L1214:
	testq	%rdi, %rdi
	js	.L1215
	cmpq	$29, %rdi
	ja	.L1216
	movl	$31, %ecx
	movq	$30, 80(%rsp)
.L1194:
	movq	%r11, 88(%rsp)
	call	_Znwy
	movq	(%rbx), %rcx
	movq	88(%rsp), %r9
	cmpq	%rcx, %r9
	je	.L1195
	movq	16(%rbx), %r10
	movq	%rax, 72(%rsp)
	leaq	1(%r10), %rdx
	call	_ZdlPvy
	movq	72(%rsp), %rax
.L1195:
	movq	80(%rsp), %rcx
	movq	192(%rsp), %rdx
	movq	%rax, %r11
	movq	%rax, (%rbx)
	movq	%rcx, 16(%rbx)
.L1196:
	movq	%r11, %rcx
	movq	%rdi, %r8
	call	memcpy
	movq	(%rbx), %r11
	jmp	.L1198
	.p2align 4
	.p2align 3
.L1216:
	movq	%rdi, %rcx
	movq	%rdi, 80(%rsp)
	incq	%rcx
	jns	.L1194
	call	_ZSt17__throw_bad_allocv
.LEHE32:
.L1206:
	movq	%rax, %r15
	vzeroupper
.L1203:
	movq	72(%rsp), %rcx
	call	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev
	movq	%r15, %rcx
.LEHB33:
	call	_Unwind_Resume
.LEHE33:
.L1215:
	leaq	.LC2(%rip), %rcx
.LEHB34:
	call	_ZSt20__throw_length_errorPKc
.LEHE34:
.L1208:
	movq	%rax, %rbx
	movq	.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	192(%rsp), %rcx
	addq	$16, %rax
	movq	%rax, 120(%rsp)
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rdi
	movq	64(%rsp), %rcx
	addq	$16, %rdi
	movq	%rdi, 120(%rsp)
	call	_ZNSt6localeD1Ev
	movq	-24(%r14), %rsi
	movq	-24(%r12), %r8
	movq	-24(%rbp), %r10
	movq	56(%rsp), %r9
	movq	48(%rsp), %r14
	movq	%r13, 96(%rsp,%rsi)
	xorl	%r13d, %r13d
	movq	%r12, 112(%rsp)
	movq	%r9, 112(%rsp,%r8)
	movq	%rbp, 96(%rsp)
	movq	%r14, 96(%rsp,%r10)
	movq	%r13, 104(%rsp)
	jmp	.L1187
.L1209:
	movq	%rbp, 96(%rsp)
	movq	48(%rsp), %r12
	movq	-24(%rbp), %rbp
	xorl	%edi, %edi
	movq	%rax, %rbx
	movq	%r12, 96(%rsp,%rbp)
	movq	%rdi, 104(%rsp)
	vzeroupper
.L1187:
	movq	.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rdx
	movq	%r15, %rcx
	addq	$16, %rdx
	movq	%rdx, 224(%rsp)
	call	_ZNSt8ios_baseD2Ev
	movq	%rbx, %rcx
.LEHB35:
	call	_Unwind_Resume
.LEHE35:
.L1207:
	movq	%rax, %rbx
	vzeroupper
	jmp	.L1187
.L1210:
	movq	%rbx, %rcx
	movq	%rax, %r15
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	jmp	.L1203
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13463:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13463-.LLSDACSB13463
.LLSDACSB13463:
	.uleb128 .LEHB28-.LFB13463
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L1207-.LFB13463
	.uleb128 0
	.uleb128 .LEHB29-.LFB13463
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L1209-.LFB13463
	.uleb128 0
	.uleb128 .LEHB30-.LFB13463
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L1208-.LFB13463
	.uleb128 0
	.uleb128 .LEHB31-.LFB13463
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L1206-.LFB13463
	.uleb128 0
	.uleb128 .LEHB32-.LFB13463
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L1210-.LFB13463
	.uleb128 0
	.uleb128 .LEHB33-.LFB13463
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB34-.LFB13463
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L1210-.LFB13463
	.uleb128 0
	.uleb128 .LEHB35-.LFB13463
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0
	.uleb128 0
.LLSDACSE13463:
	.text
	.seh_endproc
	.section .rdata,"dr"
.LC123:
	.ascii "basic_string::append\0"
	.section	.text$_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv
	.def	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv
_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv:
.LFB14605:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	8(%rcx), %r9
	movq	24(%rcx), %r8
	movq	%rcx, %rbx
	subq	%r9, %r8
	je	.L1228
	movq	296(%rcx), %rdx
	movabsq	$9223372036854775807, %rax
	subq	%rdx, %rax
	cmpq	%r8, %rax
	jb	.L1229
	movq	288(%rcx), %rcx
	leaq	304(%rbx), %r10
	leaq	(%r8,%rdx), %rsi
	cmpq	%r10, %rcx
	je	.L1224
	movq	304(%rbx), %r11
.L1220:
	cmpq	%rsi, %r11
	jb	.L1221
	addq	%rdx, %rcx
	cmpq	$1, %r8
	je	.L1230
	movq	%r9, %rdx
	call	memcpy
	jmp	.L1223
	.p2align 4
	.p2align 3
.L1221:
	movq	%r8, 32(%rsp)
	leaq	288(%rbx), %rcx
	xorl	%r8d, %r8d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
.L1223:
	movq	288(%rbx), %rcx
	movq	%rsi, 296(%rbx)
	movb	$0, (%rcx,%rsi)
	movq	8(%rbx), %r8
	movq	%r8, 24(%rbx)
.L1228:
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L1230:
	movzbl	(%r9), %edx
	movb	%dl, (%rcx)
	jmp	.L1223
	.p2align 4
	.p2align 3
.L1224:
	movl	$15, %r11d
	jmp	.L1220
.L1229:
	leaq	.LC123(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy
	.def	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy
_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy:
.LFB15381:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$48, %rsp
	.seh_stackalloc	48
	.seh_endprologue
	movq	8(%rcx), %r9
	movq	24(%rcx), %r8
	movq	%rcx, %rbx
	movq	%rdx, %rsi
	leaq	304(%rcx), %r13
	subq	%r9, %r8
	jne	.L1258
.L1232:
	movq	296(%rbx), %r12
	movq	288(%rbx), %rdi
	addq	%r12, %rsi
	cmpq	%r13, %rdi
	je	.L1259
	movq	304(%rbx), %rbp
	cmpq	%rsi, %rbp
	jb	.L1260
.L1239:
	movq	%rsi, 296(%rbx)
	movb	$0, (%rdi,%rsi)
	movq	288(%rbx), %r13
	movq	296(%rbx), %rsi
	movq	%rbx, %rax
	movq	%r13, 8(%rbx)
	addq	%r12, %r13
	movq	%rsi, 16(%rbx)
	movq	%r13, 24(%rbx)
	addq	$48, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L1260:
	testq	%rsi, %rsi
	js	.L1240
	addq	%rbp, %rbp
	cmpq	%rbp, %rsi
	jb	.L1261
	movq	%rsi, %rbp
.L1241:
	movq	%rbp, %rcx
	incq	%rcx
	js	.L1243
.L1244:
	call	_Znwy
	movq	296(%rbx), %r9
	movq	288(%rbx), %r14
	movq	%rax, %rdi
	leaq	1(%r9), %r8
	testq	%r9, %r9
	je	.L1262
	testq	%r8, %r8
	je	.L1246
	movq	%r14, %rdx
	movq	%rax, %rcx
	call	memcpy
	cmpq	%r14, %r13
	je	.L1247
.L1246:
	movq	304(%rbx), %r11
	movq	%r14, %rcx
	leaq	1(%r11), %rdx
	call	_ZdlPvy
.L1247:
	movq	%rdi, 288(%rbx)
	movq	%rbp, 304(%rbx)
	jmp	.L1239
	.p2align 4
	.p2align 3
.L1261:
	leaq	1(%rbp), %rcx
	testq	%rbp, %rbp
	jns	.L1244
.L1243:
	call	_ZSt17__throw_bad_allocv
	.p2align 4
	.p2align 3
.L1259:
	cmpq	$15, %rsi
	jbe	.L1239
	testq	%rsi, %rsi
	js	.L1240
	movl	$30, %ebp
	cmpq	%rbp, %rsi
	cmovnb	%rsi, %rbp
	jmp	.L1241
	.p2align 4
	.p2align 3
.L1262:
	movzbl	(%r14), %r10d
	movb	%r10b, (%rax)
	cmpq	%r14, %r13
	jne	.L1246
	jmp	.L1247
	.p2align 4
	.p2align 3
.L1258:
	movq	296(%rcx), %rdx
	movabsq	$9223372036854775807, %rax
	subq	%rdx, %rax
	cmpq	%r8, %rax
	jb	.L1263
	movq	288(%rcx), %rcx
	leaq	(%r8,%rdx), %rdi
	cmpq	%r13, %rcx
	je	.L1249
	movq	304(%rbx), %rbp
.L1234:
	cmpq	%rdi, %rbp
	jb	.L1235
	addq	%rdx, %rcx
	cmpq	$1, %r8
	je	.L1264
	movq	%r9, %rdx
	call	memcpy
.L1237:
	movq	288(%rbx), %rcx
	movq	%rdi, 296(%rbx)
	movb	$0, (%rcx,%rdi)
	movq	8(%rbx), %r8
	movq	%r8, 24(%rbx)
	jmp	.L1232
	.p2align 4
	.p2align 3
.L1264:
	movzbl	(%r9), %edx
	movb	%dl, (%rcx)
	jmp	.L1237
	.p2align 4
	.p2align 3
.L1235:
	movq	%r8, 32(%rsp)
	leaq	288(%rbx), %rcx
	xorl	%r8d, %r8d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	jmp	.L1237
	.p2align 4
	.p2align 3
.L1249:
	movl	$15, %ebp
	jmp	.L1234
.L1263:
	leaq	.LC123(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L1240:
	leaq	.LC2(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv
	.def	_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv
_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv:
.LFB15128:
	subq	$24, %rsp
	.seh_stackalloc	24
	vmovaps	%xmm6, (%rsp)
	.seh_savexmm	%xmm6, 0
	.seh_endprologue
	movq	%rcx, %rdx
	movl	$2147483647, %ecx
	movl	$-2147483648, %eax
	movl	$1, %r8d
	vpbroadcastd	%ecx, %zmm4
	vpandd	4(%rdx), %zmm4, %zmm0
	vpbroadcastd	%eax, %zmm3
	movl	$-1727483681, %r9d
	leaq	2444(%rdx), %rcx
	vpbroadcastd	%r8d, %zmm2
	vpbroadcastd	%r9d, %zmm1
	leaq	908(%rdx), %r8
	vpternlogd	$248, (%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpternlogd	$150, 1588(%rdx), %zmm5, %zmm0
	vpandd	68(%rdx), %zmm4, %zmm5
	vmovdqu32	%zmm0, (%rdx)
	vpternlogd	$248, 64(%rdx), %zmm3, %zmm5
	vpsrld	$1, %zmm5, %zmm6
	vpandd	%zmm2, %zmm5, %zmm0
	vmovdqu32	1652(%rdx), %zmm5
	vpmulld	%zmm1, %zmm0, %zmm0
	vpternlogd	$150, %zmm6, %zmm5, %zmm0
	vpandd	132(%rdx), %zmm4, %zmm6
	vmovdqu32	%zmm0, 64(%rdx)
	vpternlogd	$248, 128(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	196(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 1716(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 128(%rdx)
	vpternlogd	$248, 192(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	260(%rdx), %zmm4, %zmm6
	vpternlogd	$150, 1780(%rdx), %zmm5, %zmm0
	vmovdqu32	%zmm0, 192(%rdx)
	vpternlogd	$248, 256(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	324(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 1844(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 256(%rdx)
	vpternlogd	$248, 320(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	388(%rdx), %zmm4, %zmm6
	vpternlogd	$150, 1908(%rdx), %zmm5, %zmm0
	vmovdqu32	%zmm0, 320(%rdx)
	vpternlogd	$248, 384(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	452(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 1972(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 384(%rdx)
	vpternlogd	$248, 448(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpternlogd	$150, 2036(%rdx), %zmm5, %zmm0
	vmovdqu32	%zmm0, 448(%rdx)
	vpandd	516(%rdx), %zmm4, %zmm6
	vpternlogd	$248, 512(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	580(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 2100(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 512(%rdx)
	vpternlogd	$248, 576(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	644(%rdx), %zmm4, %zmm6
	vpternlogd	$150, 2164(%rdx), %zmm5, %zmm0
	vmovdqu32	%zmm0, 576(%rdx)
	vpternlogd	$248, 640(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	708(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 2228(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 640(%rdx)
	vpternlogd	$248, 704(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	772(%rdx), %zmm4, %zmm6
	vpternlogd	$150, 2292(%rdx), %zmm5, %zmm0
	vmovdqu32	%zmm0, 704(%rdx)
	vpternlogd	$248, 768(%rdx), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	836(%rdx), %zmm4, %zmm0
	vpternlogd	$150, 2356(%rdx), %zmm5, %zmm6
	vmovdqu32	%zmm6, 768(%rdx)
	vpternlogd	$248, 832(%rdx), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vmovq	896(%rdx), %xmm6
	vpternlogd	$150, 2420(%rdx), %zmm5, %zmm0
	vmovq	.LC128(%rip), %xmm5
	vmovdqu32	%zmm0, 832(%rdx)
	vpand	%xmm5, %xmm6, %xmm0
	vmovq	900(%rdx), %xmm5
	vmovq	.LC129(%rip), %xmm6
	vpand	%xmm6, %xmm5, %xmm5
	vpor	%xmm5, %xmm0, %xmm0
	vmovq	2484(%rdx), %xmm5
	vpsrld	$1, %xmm0, %xmm6
	vpxor	%xmm6, %xmm5, %xmm5
	vmovq	.LC130(%rip), %xmm6
	vpand	%xmm6, %xmm0, %xmm0
	vmovq	.LC131(%rip), %xmm6
	vpmulld	%xmm6, %xmm0, %xmm0
	vpxor	%xmm0, %xmm5, %xmm5
	vmovq	%xmm5, 896(%rdx)
	movl	904(%rdx), %r10d
	movl	908(%rdx), %r11d
	andl	$-2147483648, %r10d
	andl	$2147483647, %r11d
	orl	%r11d, %r10d
	movl	%r10d, %eax
	andl	$1, %r10d
	shrl	%eax
	xorl	2492(%rdx), %eax
	negl	%r10d
	andl	$-1727483681, %r10d
	xorl	%eax, %r10d
	movl	%r10d, 904(%rdx)
.L1266:
	vpandd	4(%r8), %zmm4, %zmm6
	addq	$384, %r8
	vpternlogd	$248, -384(%r8), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	-316(%r8), %zmm4, %zmm0
	vpternlogd	$150, -1292(%r8), %zmm5, %zmm6
	vmovdqu32	%zmm6, -384(%r8)
	vpternlogd	$248, -320(%r8), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	-252(%r8), %zmm4, %zmm6
	vpternlogd	$150, -1228(%r8), %zmm5, %zmm0
	vmovdqu32	%zmm0, -320(%r8)
	vpternlogd	$248, -256(%r8), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	-188(%r8), %zmm4, %zmm0
	vpternlogd	$150, -1164(%r8), %zmm5, %zmm6
	vmovdqu32	%zmm6, -256(%r8)
	vpternlogd	$248, -192(%r8), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpandd	-124(%r8), %zmm4, %zmm6
	vpternlogd	$150, -1100(%r8), %zmm5, %zmm0
	vmovdqu32	%zmm0, -192(%r8)
	vpternlogd	$248, -128(%r8), %zmm3, %zmm6
	vpandd	%zmm2, %zmm6, %zmm0
	vpsrld	$1, %zmm6, %zmm5
	vpmulld	%zmm1, %zmm0, %zmm6
	vpandd	-60(%r8), %zmm4, %zmm0
	vpternlogd	$150, -1036(%r8), %zmm5, %zmm6
	vmovdqu32	%zmm6, -128(%r8)
	vpternlogd	$248, -64(%r8), %zmm3, %zmm0
	vpandd	%zmm2, %zmm0, %zmm6
	vpsrld	$1, %zmm0, %zmm5
	vpmulld	%zmm1, %zmm6, %zmm0
	vpternlogd	$150, -972(%r8), %zmm5, %zmm0
	vmovdqu32	%zmm0, -64(%r8)
	cmpq	%r8, %rcx
	jne	.L1266
	movl	$-2147483648, %r9d
	movl	2476(%rdx), %ecx
	movl	$1, %r11d
	movl	$2147483647, %r10d
	vpbroadcastd	%r9d, %ymm2
	movl	2480(%rdx), %r9d
	vpbroadcastd	%r11d, %ymm1
	movl	2484(%rdx), %r11d
	vpbroadcastd	%r10d, %ymm3
	movl	$-1727483681, %eax
	vpandd	2448(%rdx), %ymm3, %ymm4
	movq	$0, 2496(%rdx)
	vpbroadcastd	%eax, %ymm0
	andl	$-2147483648, %ecx
	movl	%r9d, %r8d
	movl	%r11d, %eax
	andl	$-2147483648, %r9d
	andl	$-2147483648, %r11d
	andl	$2147483647, %r8d
	andl	$2147483647, %eax
	orl	%r8d, %ecx
	orl	%eax, %r9d
	vpternlogd	$248, 2444(%rdx), %ymm2, %ymm4
	movl	%ecx, %r10d
	andl	$1, %ecx
	vpandd	%ymm1, %ymm4, %ymm6
	vpsrld	$1, %ymm4, %ymm5
	shrl	%r10d
	vpmulld	%ymm0, %ymm6, %ymm3
	xorl	1568(%rdx), %r10d
	negl	%ecx
	andl	$-1727483681, %ecx
	vpternlogd	$150, 1536(%rdx), %ymm5, %ymm3
	vmovdqu	%ymm3, 2444(%rdx)
	xorl	%r10d, %ecx
	movl	%ecx, 2476(%rdx)
	movl	%r9d, %ecx
	andl	$1, %r9d
	shrl	%ecx
	xorl	1572(%rdx), %ecx
	negl	%r9d
	andl	$-1727483681, %r9d
	xorl	%ecx, %r9d
	movl	%r9d, 2480(%rdx)
	movl	2488(%rdx), %r9d
	movl	%r9d, %r8d
	andl	$-2147483648, %r9d
	andl	$2147483647, %r8d
	orl	%r8d, %r11d
	movl	%r11d, %r10d
	andl	$1, %r11d
	shrl	%r10d
	xorl	1576(%rdx), %r10d
	negl	%r11d
	andl	$-1727483681, %r11d
	xorl	%r10d, %r11d
	movl	%r11d, 2484(%rdx)
	movl	2492(%rdx), %r11d
	movl	%r11d, %eax
	andl	$2147483647, %eax
	orl	%eax, %r9d
	movl	%r9d, %ecx
	shrl	%ecx
	xorl	1580(%rdx), %ecx
	andl	$1, %r9d
	andl	$-2147483648, %r11d
	negl	%r9d
	andl	$-1727483681, %r9d
	xorl	%ecx, %r9d
	movl	%r9d, 2488(%rdx)
	movl	(%rdx), %r9d
	andl	$2147483647, %r9d
	orl	%r11d, %r9d
	movl	%r9d, %r8d
	andl	$1, %r9d
	shrl	%r8d
	xorl	1584(%rdx), %r8d
	negl	%r9d
	andl	$-1727483681, %r9d
	xorl	%r8d, %r9d
	movl	%r9d, 2492(%rdx)
	vzeroupper
	vmovaps	(%rsp), %xmm6
	addq	$24, %rsp
	ret
	.seh_endproc
	.text
	.p2align 4
	.globl	_Z20generateRandomDoubledd
	.def	_Z20generateRandomDoubledd;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z20generateRandomDoubledd
_Z20generateRandomDoubledd:
.LFB13462:
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$104, %rsp
	.seh_stackalloc	104
	vmovaps	%xmm6, 32(%rsp)
	.seh_savexmm	%xmm6, 32
	vmovaps	%xmm7, 48(%rsp)
	.seh_savexmm	%xmm7, 48
	vmovaps	%xmm8, 64(%rsp)
	.seh_savexmm	%xmm8, 64
	vmovaps	%xmm9, 80(%rsp)
	.seh_savexmm	%xmm9, 80
	.seh_endprologue
	movzbl	_ZGVZ20generateRandomDoubleddE9generator(%rip), %eax
	vmovapd	%xmm0, %xmm9
	vmovapd	%xmm1, %xmm6
	leaq	_ZZ20generateRandomDoubleddE9generator(%rip), %rbx
	testb	%al, %al
	je	.L1296
.L1275:
	movq	2496+_ZZ20generateRandomDoubleddE9generator(%rip), %rsi
	cmpq	$623, %rsi
	ja	.L1297
.L1278:
	movl	(%rbx,%rsi,4), %r10d
	leaq	1(%rsi), %rcx
	vxorps	%xmm7, %xmm7, %xmm7
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rcx, 2496+_ZZ20generateRandomDoubleddE9generator(%rip)
	movl	%r10d, %r8d
	shrl	$11, %r8d
	xorl	%r10d, %r8d
	movl	%r8d, %edx
	sall	$7, %edx
	andl	$-1658038656, %edx
	xorl	%r8d, %edx
	movl	%edx, %r9d
	sall	$15, %r9d
	andl	$-272236544, %r9d
	xorl	%edx, %r9d
	movl	%r9d, %r11d
	shrl	$18, %r11d
	xorl	%r11d, %r9d
	vcvtusi2sdl	%r9d, %xmm7, %xmm1
	vaddsd	%xmm0, %xmm1, %xmm8
	cmpq	$623, %rcx
	ja	.L1298
.L1279:
	movl	(%rbx,%rcx,4), %ebx
	vmovsd	.LC136(%rip), %xmm4
	vsubsd	%xmm9, %xmm6, %xmm5
	vmovaps	32(%rsp), %xmm6
	leaq	1(%rcx), %rax
	movq	%rax, 2496+_ZZ20generateRandomDoubleddE9generator(%rip)
	movl	%ebx, %esi
	shrl	$11, %esi
	xorl	%esi, %ebx
	movl	%ebx, %r10d
	sall	$7, %r10d
	andl	$-1658038656, %r10d
	xorl	%r10d, %ebx
	movl	%ebx, %r8d
	sall	$15, %r8d
	andl	$-272236544, %r8d
	xorl	%r8d, %ebx
	movl	%ebx, %edx
	shrl	$18, %edx
	xorl	%edx, %ebx
	vcvtusi2sdl	%ebx, %xmm7, %xmm1
	vfmadd132sd	.LC137(%rip), %xmm8, %xmm1
	vmulsd	.LC138(%rip), %xmm1, %xmm3
	vcmpgesd	.LC15(%rip), %xmm3, %xmm2
	vblendvpd	%xmm2, %xmm4, %xmm3, %xmm0
	vmovaps	48(%rsp), %xmm7
	vmovaps	64(%rsp), %xmm8
	vfmadd132sd	%xmm5, %xmm9, %xmm0
	vmovaps	80(%rsp), %xmm9
	addq	$104, %rsp
	popq	%rbx
	popq	%rsi
	ret
	.p2align 4
	.p2align 3
.L1296:
	leaq	_ZGVZ20generateRandomDoubleddE9generator(%rip), %rsi
	leaq	_ZZ20generateRandomDoubleddE9generator(%rip), %rbx
	movq	%rsi, %rcx
	call	__cxa_guard_acquire
	testl	%eax, %eax
	je	.L1275
	xorl	%ecx, %ecx
	call	_time64
	movl	$3, %r10d
	movl	%eax, %ecx
	movl	%eax, _ZZ20generateRandomDoubleddE9generator(%rip)
	shrl	$30, %ecx
	xorl	%eax, %ecx
	imull	$1812433253, %ecx, %r8d
	incl	%r8d
	movl	%r8d, %r9d
	movl	%r8d, 4+_ZZ20generateRandomDoubleddE9generator(%rip)
	shrl	$30, %r9d
	xorl	%r8d, %r9d
	imull	$1812433253, %r9d, %eax
	addl	$2, %eax
	movl	%eax, 8+_ZZ20generateRandomDoubleddE9generator(%rip)
	.p2align 4
	.p2align 3
.L1277:
	movl	%eax, %r11d
	shrl	$30, %r11d
	xorl	%eax, %r11d
	imull	$1812433253, %r11d, %eax
	addl	%r10d, %eax
	movl	%eax, %edx
	movl	%eax, (%rbx,%r10,4)
	shrl	$30, %edx
	xorl	%edx, %eax
	imull	$1812433253, %eax, %ecx
	leal	1(%rcx,%r10), %r8d
	movl	%r8d, %r9d
	movl	%r8d, 4(%rbx,%r10,4)
	shrl	$30, %r9d
	xorl	%r8d, %r9d
	imull	$1812433253, %r9d, %r11d
	leal	2(%r11,%r10), %eax
	movl	%eax, %edx
	movl	%eax, 8(%rbx,%r10,4)
	shrl	$30, %edx
	xorl	%eax, %edx
	imull	$1812433253, %edx, %ecx
	leal	3(%rcx,%r10), %r8d
	movl	%r8d, %r9d
	movl	%r8d, 12(%rbx,%r10,4)
	shrl	$30, %r9d
	xorl	%r8d, %r9d
	imull	$1812433253, %r9d, %r11d
	leal	4(%r11,%r10), %eax
	movl	%eax, %edx
	movl	%eax, 16(%rbx,%r10,4)
	shrl	$30, %edx
	xorl	%eax, %edx
	imull	$1812433253, %edx, %ecx
	leal	5(%rcx,%r10), %r8d
	movl	%r8d, %r9d
	movl	%r8d, 20(%rbx,%r10,4)
	shrl	$30, %r9d
	xorl	%r8d, %r9d
	imull	$1812433253, %r9d, %r11d
	leal	6(%r11,%r10), %eax
	movl	%eax, %edx
	movl	%eax, 24(%rbx,%r10,4)
	shrl	$30, %edx
	xorl	%eax, %edx
	imull	$1812433253, %edx, %ecx
	leal	7(%rcx,%r10), %r8d
	movl	%r8d, %r9d
	movl	%r8d, 28(%rbx,%r10,4)
	shrl	$30, %r9d
	xorl	%r8d, %r9d
	imull	$1812433253, %r9d, %r11d
	leal	8(%r11,%r10), %eax
	movl	%eax, 32(%rbx,%r10,4)
	addq	$9, %r10
	cmpq	$624, %r10
	jne	.L1277
	movq	%rsi, %rcx
	movq	$624, 2496+_ZZ20generateRandomDoubleddE9generator(%rip)
	call	__cxa_guard_release
	movq	2496+_ZZ20generateRandomDoubleddE9generator(%rip), %rsi
	cmpq	$623, %rsi
	jbe	.L1278
	.p2align 4
	.p2align 3
.L1297:
	movq	%rbx, %rcx
	call	_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv
	movq	2496+_ZZ20generateRandomDoubleddE9generator(%rip), %rsi
	jmp	.L1278
	.p2align 4
	.p2align 3
.L1298:
	movq	%rbx, %rcx
	call	_ZNSt23mersenne_twister_engineIjLy32ELy624ELy397ELy31ELj2567483615ELy11ELj4294967295ELy7ELj2636928640ELy15ELj4022730752ELy18ELj1812433253EE11_M_gen_randEv
	movq	2496+_ZZ20generateRandomDoubleddE9generator(%rip), %rcx
	jmp	.L1279
	.seh_endproc
	.section	.text$_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	.def	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE:
.LFB15265:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$184, %rsp
	.seh_stackalloc	184
	vmovaps	%xmm6, 160(%rsp)
	.seh_savexmm	%xmm6, 160
	.seh_endprologue
	movq	8(%rdx), %r12
	movq	(%rdx), %rsi
	movl	%r8d, %r13d
	movq	%rcx, %rbp
	movq	%rdx, %rdi
	movl	%r8d, %ebx
	movq	$0, 52(%rsp)
	andl	$15, %r13d
	cmpq	%r12, %rsi
	je	.L1300
	movzbl	(%rsi), %r14d
	cmpb	$125, %r14b
	je	.L1300
	cmpb	$123, %r14b
	je	.L1383
	leaq	64(%rsp), %rcx
	movq	%rsi, 72(%rsp)
	movq	%rsi, 80(%rsp)
	movw	$0, 88(%rsp)
	vmovq	%rcx, %xmm6
	movb	$0, 90(%rsp)
	movq	%r12, 96(%rsp)
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movzbl	88(%rsp), %r15d
	vmovdqu	64(%rsp), %ymm0
	movq	96(%rsp), %rax
	movzbl	89(%rsp), %r8d
	movq	80(%rsp), %rdx
	movzbl	90(%rsp), %r9d
	movzbl	%r15b, %r10d
	incl	%r10d
	vmovdqu	%ymm0, 112(%rsp)
	movq	%rax, 144(%rsp)
	cmpl	%r8d, %r10d
	je	.L1534
	vzeroupper
.L1302:
	movl	112(%rsp,%r15,4), %r9d
	cmpl	$55295, %r9d
	ja	.L1535
.L1305:
	cmpq	%rdx, %r12
	je	.L1309
	movzbl	(%rdx), %ecx
	cmpb	$62, %cl
	je	.L1384
	cmpb	$94, %cl
	je	.L1385
	movl	$1, %r8d
	cmpb	$60, %cl
	je	.L1308
.L1309:
	cmpb	$62, %r14b
	je	.L1311
	cmpb	$94, %r14b
	je	.L1312
	cmpb	$60, %r14b
	je	.L1313
	xorl	%r8d, %r8d
	movl	$32, %r9d
.L1314:
	movzbl	(%rsi), %r14d
	cmpb	$125, %r14b
	je	.L1316
.L1380:
	leal	-32(%r14), %r15d
	cmpb	$13, %r15b
	ja	.L1536
	movzbl	%r15b, %eax
	leaq	CSWTCH.923(%rip), %r10
	movb	$0, 45(%rsp)
	movl	(%r10,%rax,4), %r11d
	testl	%r11d, %r11d
	je	.L1320
	andl	$3, %r11d
	incq	%rsi
	movb	%r11b, 45(%rsp)
.L1320:
	cmpq	%rsi, %r12
	je	.L1321
	movzbl	(%rsi), %r14d
	cmpb	$125, %r14b
	je	.L1321
	cmpb	$35, %r14b
	jne	.L1319
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %r12
	jne	.L1537
.L1323:
	movzbl	45(%rsp), %esi
	andl	$15, %ebx
	orl	$16, %r8d
	movw	$0, 56(%rsp)
	sall	$11, %ebx
	sall	$2, %esi
	orl	%r8d, %esi
	orl	%ebx, %esi
	movzwl	52(%rsp), %ebx
	andw	$-31232, %bx
	orl	%ebx, %esi
	movw	%si, 52(%rsp)
	movq	%rcx, %rsi
	jmp	.L1317
	.p2align 4
	.p2align 3
.L1383:
	movl	$32, %r9d
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	movb	$0, 46(%rsp)
	movb	$0, 45(%rsp)
.L1301:
	leaq	1(%rsi), %r11
	cmpq	%r11, %r12
	je	.L1538
	movsbw	1(%rsi), %ax
	cmpb	$125, %al
	je	.L1539
	cmpb	$48, %al
	je	.L1540
	leal	-49(%rax), %r14d
	cmpb	$8, %r14b
	jbe	.L1541
.L1347:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L1300:
	movzbl	53(%rsp), %r15d
	leal	0(,%r13,8), %r9d
	andl	$-121, %r15d
	orl	%r9d, %r15d
	movl	$32, %r9d
	movb	%r15b, 53(%rsp)
.L1317:
	movq	52(%rsp), %rdi
	movl	%r9d, 8(%rbp)
	movq	%rsi, %rax
	movq	%rdi, 0(%rbp)
	vmovaps	160(%rsp), %xmm6
	addq	$184, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L1534:
	cmpq	%rdx, %r12
	je	.L1304
	addq	%rdx, %r9
	cmpq	%r9, %r12
	je	.L1304
	vmovq	%xmm6, %rcx
	movq	%r9, 80(%rsp)
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	80(%rsp), %rdx
	jmp	.L1302
	.p2align 4
	.p2align 3
.L1385:
	movl	$3, %r8d
.L1308:
	leaq	1(%rdx), %rsi
.L1310:
	cmpq	%rsi, %r12
	jne	.L1314
.L1316:
	andl	$15, %ebx
	sall	$11, %ebx
	orl	%r8d, %ebx
.L1533:
	movzwl	52(%rsp), %ecx
	movw	$0, 56(%rsp)
	andw	$-31232, %cx
	orl	%ecx, %ebx
	movw	%bx, 52(%rsp)
	jmp	.L1317
	.p2align 4
	.p2align 3
.L1321:
	movzbl	45(%rsp), %edi
	andl	$15, %ebx
	sall	$11, %ebx
	sall	$2, %edi
	orl	%r8d, %edi
	orl	%edi, %ebx
	jmp	.L1533
	.p2align 4
	.p2align 3
.L1311:
	movl	$2, %r8d
.L1315:
	incq	%rsi
	movl	$32, %r9d
	jmp	.L1310
	.p2align 4
	.p2align 3
.L1313:
	movl	$1, %r8d
	jmp	.L1315
	.p2align 4
	.p2align 3
.L1312:
	movl	$3, %r8d
	jmp	.L1315
	.p2align 4
	.p2align 3
.L1537:
	movzbl	1(%rsi), %r14d
	cmpb	$125, %r14b
	je	.L1323
	movl	$1, %r15d
.L1379:
	cmpb	$48, %r14b
	jne	.L1542
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %r12
	jne	.L1543
.L1326:
	movzbl	45(%rsp), %eax
	movzwl	52(%rsp), %r11d
	movzbl	%r15b, %r10d
	orl	$64, %r8d
	andl	$15, %ebx
	movw	$0, 56(%rsp)
	sall	$4, %r10d
	sall	$11, %ebx
	sall	$2, %eax
	orl	%r8d, %eax
	andw	$-31232, %r11w
	orl	%r10d, %eax
	orl	%ebx, %eax
	orl	%r11d, %eax
	movw	%ax, 52(%rsp)
	jmp	.L1317
	.p2align 4
	.p2align 3
.L1304:
	movl	112(%rsp,%r15,4), %edx
	cmpl	$55295, %edx
	ja	.L1544
.L1529:
	vzeroupper
	jmp	.L1309
	.p2align 4
	.p2align 3
.L1536:
	movb	$0, 45(%rsp)
.L1319:
	movq	%rsi, %rcx
	xorl	%r15d, %r15d
	jmp	.L1379
	.p2align 4
	.p2align 3
.L1384:
	movl	$2, %r8d
	jmp	.L1308
	.p2align 4
	.p2align 3
.L1543:
	movzbl	1(%rcx), %r14d
	cmpb	$125, %r14b
	je	.L1326
	movb	$1, 46(%rsp)
	cmpb	$48, %r14b
	jne	.L1325
	leaq	.LC27(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L1535:
	leal	-57344(%r9), %r11d
	cmpl	$1056767, %r11d
	jbe	.L1305
.L1306:
	cmpb	$62, %r14b
	je	.L1311
	cmpb	$94, %r14b
	je	.L1312
	cmpb	$60, %r14b
	je	.L1313
	xorl	%r8d, %r8d
	movl	$32, %r9d
	jmp	.L1380
.L1539:
	cmpl	$1, 16(%rdi)
	je	.L1358
	movq	24(%rdi), %rax
	movl	$2, 16(%rdi)
	leaq	1(%rax), %rcx
	movq	%rcx, 24(%rdi)
.L1344:
	leaq	1(%r11), %r10
	movl	$2, %edx
	cmpq	%r10, %rsi
	cmovne	%r10, %rsi
	movl	$0, %r10d
	cmovne	%edx, %r10d
.L1359:
	cmpq	%rsi, %r12
	je	.L1338
	movzbl	(%rsi), %r14d
	cmpb	$125, %r14b
	je	.L1338
.L1378:
	cmpb	$76, %r14b
	je	.L1545
	subl	$66, %r14d
	cmpb	$54, %r14b
	ja	.L1389
	leaq	.L1366(%rip), %rdi
	movzbl	%r14b, %ecx
	movslq	(%rdi,%rcx,4), %rdx
	addq	%rdi, %rdx
	jmp	*%rdx
	.section .rdata,"dr"
	.align 4
.L1366:
	.long	.L1390-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1391-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1392-.L1366
	.long	.L1393-.L1366
	.long	.L1394-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1395-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1396-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1389-.L1366
	.long	.L1397-.L1366
	.section	.text$_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L1389:
	xorl	%r14d, %r14d
.L1364:
	cmpq	%rsi, %r12
	je	.L1375
.L1376:
	cmpb	$125, (%rsi)
	jne	.L1377
.L1375:
	movzbl	45(%rsp), %edx
	movw	%ax, 56(%rsp)
	sall	$5, %r14d
	sall	$7, %r10d
	sall	$2, %edx
	orl	%r8d, %edx
	movzbl	%r15b, %r8d
	movzbl	46(%rsp), %r15d
	sall	$4, %r8d
	orl	%r8d, %edx
	sall	$6, %r15d
	orl	%r14d, %edx
	orl	%r15d, %edx
	orl	%r10d, %edx
	movzwl	52(%rsp), %r10d
	sall	$11, %r13d
	orl	%edx, %r13d
	andw	$-31232, %r10w
	orl	%r10d, %r13d
	movw	%r13w, 52(%rsp)
	jmp	.L1317
.L1397:
	xorl	%r14d, %r14d
.L1365:
	incq	%rsi
	movl	$5, %r13d
	jmp	.L1364
.L1396:
	xorl	%r14d, %r14d
.L1367:
	testl	%ebx, %ebx
	jne	.L1374
	incq	%rsi
	xorl	%r13d, %r13d
	jmp	.L1364
.L1395:
	xorl	%r14d, %r14d
.L1368:
	incq	%rsi
	movl	$4, %r13d
	jmp	.L1364
.L1393:
	xorl	%r14d, %r14d
.L1370:
	testl	%ebx, %ebx
	je	.L1374
	incq	%rsi
	movl	$7, %r13d
	jmp	.L1364
.L1392:
	xorl	%r14d, %r14d
.L1371:
	incq	%rsi
	movl	$2, %r13d
	jmp	.L1364
.L1391:
	xorl	%r14d, %r14d
.L1372:
	incq	%rsi
	movl	$6, %r13d
	jmp	.L1364
.L1390:
	xorl	%r14d, %r14d
.L1373:
	incq	%rsi
	movl	$3, %r13d
	jmp	.L1364
.L1394:
	xorl	%r14d, %r14d
.L1369:
	incq	%rsi
	movl	$1, %r13d
	jmp	.L1364
	.p2align 4
	.p2align 3
.L1557:
	cmpq	%rsi, %r12
	je	.L1329
	movq	%r12, %r14
	xorl	%eax, %eax
	movq	%rsi, %r11
	movl	$16, %r10d
	subq	%rsi, %r14
	andl	$3, %r14d
	je	.L1337
	cmpq	$1, %r14
	je	.L1464
	cmpq	$2, %r14
	je	.L1465
	movzbl	(%rsi), %edi
	leal	-48(%rdi), %ecx
	cmpb	$9, %cl
	ja	.L1330
	movl	$12, %r10d
	movzbl	%cl, %eax
	leaq	1(%rsi), %r11
.L1465:
	movzbl	(%r11), %r14d
	subl	$48, %r14d
	cmpb	$9, %r14b
	ja	.L1330
	subl	$4, %r10d
	js	.L1546
	leal	(%rax,%rax,4), %eax
	movzbl	%r14b, %ecx
	leal	(%rcx,%rax,2), %eax
.L1494:
	incq	%r11
.L1464:
	movzbl	(%r11), %r14d
	subl	$48, %r14d
	cmpb	$9, %r14b
	ja	.L1330
	subl	$4, %r10d
	js	.L1547
	leal	(%rax,%rax,4), %eax
	movzbl	%r14b, %ecx
	leal	(%rcx,%rax,2), %eax
.L1496:
	incq	%r11
	cmpq	%r11, %r12
	je	.L1497
.L1337:
	movzbl	(%r11), %r14d
	leal	-48(%r14), %edi
	cmpb	$9, %dil
	ja	.L1330
	subl	$4, %r10d
	js	.L1331
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %r14d
	leal	(%r14,%rax,2), %eax
.L1332:
	movzbl	1(%r11), %edi
	leaq	1(%r11), %rcx
	movq	%rcx, %r11
	subl	$48, %edi
	cmpb	$9, %dil
	ja	.L1330
	movl	%r10d, %r11d
	subl	$4, %r11d
	js	.L1548
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %edi
	leal	(%rdi,%rax,2), %eax
.L1499:
	movzbl	1(%rcx), %r14d
	leaq	1(%rcx), %r11
	subl	$48, %r14d
	cmpb	$9, %r14b
	ja	.L1330
	movl	%r10d, %r11d
	subl	$8, %r11d
	js	.L1549
	leal	(%rax,%rax,4), %eax
	movzbl	%r14b, %r14d
	leal	(%r14,%rax,2), %eax
.L1501:
	movzbl	2(%rcx), %edi
	leaq	2(%rcx), %r11
	subl	$48, %edi
	cmpb	$9, %dil
	ja	.L1330
	subl	$12, %r10d
	js	.L1550
	leal	(%rax,%rax,4), %eax
	movzbl	%dil, %r14d
	leal	(%r14,%rax,2), %eax
.L1503:
	leaq	3(%rcx), %r11
	cmpq	%r11, %r12
	jne	.L1337
.L1497:
	movq	%r12, %rsi
	movl	$1, %r10d
.L1338:
	movzbl	45(%rsp), %r13d
	movzbl	46(%rsp), %r14d
	movzbl	%r15b, %r12d
	andl	$15, %ebx
	movw	%ax, 56(%rsp)
	sall	$4, %r12d
	sall	$7, %r10d
	sall	$2, %r13d
	orl	%r8d, %r13d
	movzwl	52(%rsp), %r8d
	orl	%r12d, %r13d
	sall	$6, %r14d
	sall	$11, %ebx
	orl	%r14d, %r13d
	andw	$-31232, %r8w
	orl	%r10d, %r13d
	orl	%ebx, %r13d
	orl	%r8d, %r13d
	movw	%r13w, 52(%rsp)
	jmp	.L1317
	.p2align 4
	.p2align 3
.L1545:
	leaq	1(%rsi), %r14
	cmpq	%r14, %r12
	jne	.L1551
.L1362:
	movzbl	52(%rsp), %r11d
	movzbl	45(%rsp), %ecx
	movzbl	46(%rsp), %edi
	andl	$3, %r10d
	movq	%r14, %rsi
	movw	%ax, 56(%rsp)
	sall	$4, %r15d
	sall	$7, %r10d
	sall	$2, %ecx
	andl	$-16, %r11d
	orl	%r8d, %r11d
	orl	%ecx, %r11d
	andl	$-17, %r11d
	orl	%r15d, %r11d
	sall	$6, %edi
	orl	$32, %r11d
	andl	$-65, %r11d
	orl	%edi, %r11d
	movb	%r11b, 52(%rsp)
	movzwl	52(%rsp), %ebx
	sall	$3, %r13d
	andw	$-385, %bx
	orl	%r10d, %ebx
	movzbl	%bh, %edx
	movw	%bx, 52(%rsp)
	andl	$-121, %edx
	orl	%r13d, %edx
	movb	%dl, 53(%rsp)
	jmp	.L1317
	.p2align 4
	.p2align 3
.L1551:
	movzbl	1(%rsi), %esi
	cmpb	$125, %sil
	je	.L1362
	subl	$66, %esi
	cmpb	$54, %sil
	ja	.L1399
	leaq	.L1381(%rip), %rcx
	movzbl	%sil, %r11d
	movslq	(%rcx,%r11,4), %rdi
	addq	%rcx, %rdi
	jmp	*%rdi
	.section .rdata,"dr"
	.align 4
.L1381:
	.long	.L1400-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1401-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1402-.L1381
	.long	.L1403-.L1381
	.long	.L1404-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1405-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1406-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1399-.L1381
	.long	.L1407-.L1381
	.section	.text$_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L1374:
	cmpq	%rsi, %r12
	je	.L1375
.L1377:
	call	_ZNSt8__format29__failed_to_parse_format_specEv
	.p2align 4
	.p2align 3
.L1544:
	subl	$57344, %edx
	cmpl	$1056767, %edx
	jbe	.L1529
	vzeroupper
	jmp	.L1306
.L1540:
	leaq	2(%rsi), %r11
	xorl	%eax, %eax
.L1346:
	cmpq	%r11, %r12
	je	.L1347
.L1382:
	cmpb	$125, (%r11)
	jne	.L1347
	cmpl	$2, 16(%rdi)
	je	.L1358
	movl	$1, 16(%rdi)
	jmp	.L1344
.L1541:
	leaq	2(%rsi), %rdx
	cmpq	%rdx, %r12
	je	.L1347
	movzbl	2(%rsi), %ecx
	leal	-48(%rcx), %r10d
	cmpb	$9, %r10b
	ja	.L1348
	movq	%r12, %rdx
	xorl	%eax, %eax
	movq	%r11, %r14
	movl	$16, %ecx
	subq	%r11, %rdx
	andl	$3, %edx
	je	.L1522
	cmpq	$1, %rdx
	je	.L1466
	cmpq	$2, %rdx
	je	.L1467
	movzbl	(%r11), %ecx
	leal	-48(%rcx), %r10d
	cmpb	$9, %r10b
	ja	.L1349
	movl	$12, %ecx
	movzbl	%r10b, %eax
	leaq	1(%r11), %r14
.L1467:
	movzbl	(%r14), %edx
	leal	-48(%rdx), %r10d
	cmpb	$9, %r10b
	ja	.L1349
	subl	$4, %ecx
	js	.L1552
	leal	(%rax,%rax,4), %eax
	movzbl	%r10b, %edx
	leal	(%rdx,%rax,2), %eax
.L1505:
	incq	%r14
.L1466:
	movzbl	(%r14), %r10d
	leal	-48(%r10), %r10d
	cmpb	$9, %r10b
	ja	.L1349
	subl	$4, %ecx
	js	.L1553
	leal	(%rax,%rax,4), %eax
	movzbl	%r10b, %edx
	leal	(%rdx,%rax,2), %eax
.L1507:
	incq	%r14
	cmpq	%r14, %r12
	je	.L1508
.L1522:
	movb	%r13b, 47(%rsp)
.L1356:
	movzbl	(%r14), %r13d
	leal	-48(%r13), %r10d
	cmpb	$9, %r10b
	ja	.L1524
	subl	$4, %ecx
	js	.L1350
	leal	(%rax,%rax,4), %eax
	movzbl	%r10b, %r10d
	leal	(%r10,%rax,2), %eax
.L1351:
	movzbl	1(%r14), %edx
	leaq	1(%r14), %r10
	movq	%r10, %r14
	leal	-48(%rdx), %r13d
	cmpb	$9, %r13b
	ja	.L1524
	movl	%ecx, %r14d
	subl	$4, %r14d
	js	.L1554
	leal	(%rax,%rax,4), %eax
	movzbl	%r13b, %r14d
	leal	(%r14,%rax,2), %eax
.L1510:
	movzbl	1(%r10), %edx
	leaq	1(%r10), %r14
	leal	-48(%rdx), %r13d
	cmpb	$9, %r13b
	ja	.L1524
	movl	%ecx, %r14d
	subl	$8, %r14d
	js	.L1555
	leal	(%rax,%rax,4), %eax
	movzbl	%r13b, %r14d
	leal	(%r14,%rax,2), %eax
.L1512:
	movzbl	2(%r10), %edx
	leaq	2(%r10), %r14
	leal	-48(%rdx), %r13d
	cmpb	$9, %r13b
	ja	.L1524
	subl	$12, %ecx
	js	.L1556
	leal	(%rax,%rax,4), %eax
	movzbl	%r13b, %r13d
	leal	0(%r13,%rax,2), %eax
.L1514:
	leaq	3(%r10), %r14
	cmpq	%r14, %r12
	jne	.L1356
	movzbl	47(%rsp), %r13d
.L1508:
	movq	%r12, %r11
	jmp	.L1346
.L1348:
	subl	$48, %eax
	movq	%rdx, %r11
	jmp	.L1382
.L1524:
	movzbl	47(%rsp), %r13d
.L1349:
	cmpq	%r14, %r11
	je	.L1347
	movq	%r14, %r11
	jmp	.L1346
.L1554:
	movl	$10, %edx
	mulw	%dx
	jo	.L1347
	movzbl	%r13b, %r13d
	addw	%r13w, %ax
	jnc	.L1510
	jmp	.L1347
.L1350:
	movl	$10, %edx
	mulw	%dx
	jo	.L1347
	movzbl	%r10b, %r13d
	addw	%ax, %r13w
	jc	.L1347
	movl	%r13d, %eax
	jmp	.L1351
.L1556:
	movl	$10, %r14d
	mulw	%r14w
	jo	.L1347
	movzbl	%r13b, %edx
	addw	%dx, %ax
	jnc	.L1514
	jmp	.L1347
.L1555:
	movl	$10, %edx
	mulw	%dx
	jo	.L1347
	movzbl	%r13b, %r13d
	addw	%r13w, %ax
	jnc	.L1512
	jmp	.L1347
.L1553:
	movl	$10, %edx
	mulw	%dx
	jo	.L1347
	movzbl	%r10b, %r10d
	addw	%r10w, %ax
	jnc	.L1507
	jmp	.L1347
.L1552:
	movl	$10, %edx
	mulw	%dx
	jo	.L1347
	movzbl	%r10b, %r10d
	addw	%r10w, %ax
	jnc	.L1505
	jmp	.L1347
.L1542:
	movq	%rcx, %rsi
	movb	$0, 46(%rsp)
.L1325:
	movzbl	%r14b, %eax
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rdx
	cmpb	$9, (%rdx,%rax)
	jbe	.L1557
	cmpb	$123, %r14b
	je	.L1301
	xorl	%eax, %eax
	xorl	%r10d, %r10d
	cmpq	%rsi, %r12
	jne	.L1378
	jmp	.L1338
	.p2align 4
	.p2align 3
.L1330:
	cmpq	%rsi, %r11
	je	.L1329
	movq	%r11, %rsi
	movl	$1, %r10d
	jmp	.L1359
	.p2align 4
	.p2align 3
.L1331:
	movl	$10, %edx
	mulw	%dx
	jo	.L1329
	movzbl	%dil, %ecx
	addw	%ax, %cx
	jc	.L1329
	movl	%ecx, %eax
	jmp	.L1332
.L1548:
	movl	$10, %edx
	mulw	%dx
	jo	.L1329
	movzbl	%dil, %r14d
	addw	%ax, %r14w
	jc	.L1329
	movl	%r14d, %eax
	jmp	.L1499
.L1549:
	movl	$10, %edx
	mulw	%dx
	jo	.L1329
	movzbl	%r14b, %edi
	addw	%ax, %di
	jc	.L1329
	movl	%edi, %eax
	jmp	.L1501
.L1550:
	movl	$10, %r11d
	mulw	%r11w
	jo	.L1329
	movzbl	%dil, %edx
	addw	%ax, %dx
	jc	.L1329
	movl	%edx, %eax
	jmp	.L1503
.L1547:
	movl	$10, %edx
	mulw	%dx
	jo	.L1329
	movzbl	%r14b, %edi
	addw	%ax, %di
	jc	.L1329
	movl	%edi, %eax
	jmp	.L1496
.L1546:
	movl	$10, %edx
	mulw	%dx
	jo	.L1329
	movzbl	%r14b, %edi
	addw	%ax, %di
	jc	.L1329
	movl	%edi, %eax
	jmp	.L1494
.L1407:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1365
.L1406:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1367
.L1405:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1368
.L1404:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1369
.L1403:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1370
.L1402:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1371
.L1401:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1372
.L1400:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1373
.L1399:
	movq	%r14, %rsi
	movl	$1, %r14d
	jmp	.L1376
.L1358:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.L1329:
	leaq	.LC28(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L1538:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
	nop
	.seh_endproc
	.section	.text$_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
	.def	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE:
.LFB15317:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$56, %rsp
	.seh_stackalloc	56
	.seh_endprologue
	movq	8(%rdx), %rdi
	movq	%rcx, %rsi
	movq	(%rdx), %rcx
	movq	%rdx, %rbx
	movq	$0, 36(%rsp)
	cmpq	%rcx, %rdi
	je	.L1559
	cmpb	$125, (%rcx)
	je	.L1559
	leaq	36(%rsp), %rax
	movq	%rcx, %rdx
	movq	%rdi, %r8
	movl	$32, 44(%rsp)
	movq	%rax, %rcx
	call	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_
	movq	%rax, %rcx
	cmpq	%rdi, %rax
	je	.L1560
	movzbl	(%rax), %eax
	cmpb	$125, %al
	je	.L1560
	cmpb	$48, %al
	je	.L1562
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rbp
	movzbl	%al, %edx
	cmpb	$9, 0(%rbp,%rdx)
	jbe	.L1629
	cmpb	$123, %al
	je	.L1564
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
.L1565:
	cmpb	$46, %al
	je	.L1780
	xorl	%r11d, %r11d
	xorl	%ebp, %ebp
	movzwl	%r9w, %ebx
.L1595:
	sall	$16, %ebp
	orl	%ebx, %ebp
	cmpb	$115, %al
	je	.L1781
.L1625:
	call	_ZNSt8__format29__failed_to_parse_format_specEv
	.p2align 4
	.p2align 3
.L1559:
	movl	$32, 44(%rsp)
.L1560:
	movq	36(%rsp), %r9
	movl	44(%rsp), %edi
	movq	%rcx, %rax
	movq	%r9, (%rsi)
	movl	%edi, 8(%rsi)
	addq	$56, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L1629:
	movq	%rdi, %rdx
	xorl	%r9d, %r9d
	movq	%rcx, %r11
	movl	$16, %r13d
	subq	%rcx, %rdx
	andl	$3, %edx
	je	.L1563
	cmpq	$1, %rdx
	je	.L1702
	cmpq	$2, %rdx
	je	.L1703
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1566
	movl	$12, %r13d
	movzbl	%al, %r9d
	leaq	1(%rcx), %r11
.L1703:
	movzbl	(%r11), %eax
	leal	-48(%rax), %r8d
	cmpb	$9, %r8b
	ja	.L1566
	subl	$4, %r13d
	js	.L1782
	leal	(%r9,%r9,4), %r12d
	movzbl	%r8b, %edx
	leal	(%rdx,%r12,2), %r9d
.L1759:
	incq	%r11
.L1702:
	movzbl	(%r11), %r8d
	leal	-48(%r8), %r10d
	cmpb	$9, %r10b
	ja	.L1566
	subl	$4, %r13d
	js	.L1783
	leal	(%r9,%r9,4), %edx
	movzbl	%r10b, %r8d
	leal	(%r8,%rdx,2), %r9d
.L1761:
	incq	%r11
	cmpq	%r11, %rdi
	je	.L1762
.L1563:
	movzbl	(%r11), %r10d
	leal	-48(%r10), %r12d
	cmpb	$9, %r12b
	ja	.L1566
	subl	$4, %r13d
	js	.L1567
	leal	(%r9,%r9,4), %r8d
	movzbl	%r12b, %r10d
	leal	(%r10,%r8,2), %r9d
.L1568:
	movzbl	1(%r11), %r12d
	leaq	1(%r11), %r8
	movq	%r8, %r11
	leal	-48(%r12), %r10d
	cmpb	$9, %r10b
	ja	.L1566
	movl	%r13d, %r11d
	subl	$4, %r11d
	js	.L1784
	leal	(%r9,%r9,4), %r11d
	movzbl	%r10b, %r12d
	leal	(%r12,%r11,2), %r9d
.L1764:
	movzbl	1(%r8), %r10d
	leaq	1(%r8), %r11
	leal	-48(%r10), %r12d
	cmpb	$9, %r12b
	ja	.L1566
	movl	%r13d, %edx
	subl	$8, %edx
	js	.L1785
	leal	(%r9,%r9,4), %r10d
	movzbl	%r12b, %r12d
	leal	(%r12,%r10,2), %r9d
.L1766:
	movzbl	2(%r8), %edx
	leaq	2(%r8), %r11
	leal	-48(%rdx), %r10d
	cmpb	$9, %r10b
	ja	.L1566
	subl	$12, %r13d
	js	.L1786
	leal	(%r9,%r9,4), %edx
	movzbl	%r10b, %r12d
	leal	(%r12,%rdx,2), %r9d
.L1768:
	leaq	3(%r8), %r11
	cmpq	%r11, %rdi
	jne	.L1563
.L1762:
	movq	%rdi, %rcx
	movl	$1, %r10d
	jmp	.L1574
	.p2align 4
	.p2align 3
.L1564:
	leaq	1(%rcx), %r8
	cmpq	%r8, %rdi
	je	.L1610
	movsbw	1(%rcx), %r9w
	cmpb	$125, %r9b
	je	.L1787
	cmpb	$48, %r9b
	je	.L1788
	leal	-49(%r9), %r10d
	cmpb	$8, %r10b
	jbe	.L1582
.L1583:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L1780:
	leaq	1(%rcx), %r12
	cmpq	%r12, %rdi
	je	.L1609
	movzbl	1(%rcx), %r13d
	cmpb	$9, 0(%rbp,%r13)
	ja	.L1598
	movq	%rdi, %r13
	xorl	%eax, %eax
	movq	%r12, %rcx
	movl	$16, %r8d
	subq	%r12, %r13
	andl	$3, %r13d
	je	.L1606
	cmpq	$1, %r13
	je	.L1706
	cmpq	$2, %r13
	je	.L1707
	movzbl	(%r12), %edx
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L1599
	movl	$12, %r8d
	movzbl	%bpl, %eax
	leaq	1(%r12), %rcx
.L1707:
	movzbl	(%rcx), %ebx
	leal	-48(%rbx), %r11d
	cmpb	$9, %r11b
	ja	.L1599
	subl	$4, %r8d
	js	.L1789
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1737:
	incq	%rcx
.L1706:
	movzbl	(%rcx), %ebx
	leal	-48(%rbx), %r11d
	cmpb	$9, %r11b
	ja	.L1599
	subl	$4, %r8d
	js	.L1790
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1739:
	incq	%rcx
	cmpq	%rcx, %rdi
	je	.L1740
.L1606:
	movzbl	(%rcx), %ebx
	leal	-48(%rbx), %r11d
	cmpb	$9, %r11b
	ja	.L1599
	subl	$4, %r8d
	js	.L1600
	leal	(%rax,%rax,4), %eax
	movzbl	%r11b, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1601:
	movzbl	1(%rcx), %ebx
	leaq	1(%rcx), %r11
	movq	%r11, %rcx
	subl	$48, %ebx
	cmpb	$9, %bl
	ja	.L1599
	movl	%r8d, %ecx
	subl	$4, %ecx
	js	.L1791
	leal	(%rax,%rax,4), %eax
	movzbl	%bl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1742:
	movzbl	1(%r11), %ebx
	leaq	1(%r11), %rcx
	subl	$48, %ebx
	cmpb	$9, %bl
	ja	.L1599
	movl	%r8d, %ecx
	subl	$8, %ecx
	js	.L1792
	leal	(%rax,%rax,4), %eax
	movzbl	%bl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1744:
	movzbl	2(%r11), %ebx
	leaq	2(%r11), %rcx
	subl	$48, %ebx
	cmpb	$9, %bl
	ja	.L1599
	subl	$12, %r8d
	js	.L1793
	leal	(%rax,%rax,4), %edx
	movzbl	%bl, %eax
	leal	(%rax,%rdx,2), %eax
.L1746:
	leaq	3(%r11), %rcx
	cmpq	%rcx, %rdi
	jne	.L1606
.L1740:
	sall	$16, %eax
	movzwl	%r9w, %r9d
	movl	$1, %r11d
	orl	%r9d, %eax
	movl	%eax, %r12d
	.p2align 4
	.p2align 3
.L1607:
	movzwl	36(%rsp), %edx
	andl	$3, %r10d
	movl	%r12d, 40(%rsp)
	sall	$7, %r10d
	andw	$-385, %dx
	orl	%r10d, %edx
	leal	(%r11,%r11), %r10d
	movzbl	%dh, %edi
	movw	%dx, 36(%rsp)
	andl	$-7, %edi
	movl	%edi, %r8d
	orl	%r10d, %r8d
	movb	%r8b, 37(%rsp)
	jmp	.L1560
	.p2align 4
	.p2align 3
.L1781:
	leaq	1(%rcx), %r13
	cmpq	%rdi, %r13
	jne	.L1794
	.p2align 4
	.p2align 3
.L1626:
	movzwl	36(%rsp), %eax
	andl	$3, %r10d
	leal	(%r11,%r11), %ecx
	movl	%ebp, 40(%rsp)
	sall	$7, %r10d
	andw	$-385, %ax
	orl	%r10d, %eax
	movzbl	%ah, %edx
	movw	%ax, 36(%rsp)
	andl	$-7, %edx
	orl	%ecx, %edx
	movq	%r13, %rcx
	movb	%dl, 37(%rsp)
	jmp	.L1560
	.p2align 4
	.p2align 3
.L1794:
	cmpb	$125, 1(%rcx)
	je	.L1626
	jmp	.L1625
	.p2align 4
	.p2align 3
.L1566:
	cmpq	%r11, %rcx
	je	.L1571
	movq	%r11, %rcx
	movl	$1, %r10d
.L1594:
	cmpq	%rcx, %rdi
	je	.L1574
	movzbl	(%rcx), %eax
	cmpb	$125, %al
	jne	.L1565
.L1574:
	movzwl	36(%rsp), %r12d
	andl	$3, %r10d
	movw	%r9w, 40(%rsp)
	sall	$7, %r10d
	andw	$-385, %r12w
	orl	%r10d, %r12d
	movw	%r12w, 36(%rsp)
	jmp	.L1560
	.p2align 4
	.p2align 3
.L1598:
	cmpb	$123, %r13b
	je	.L1795
.L1609:
	leaq	.LC29(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L1787:
	cmpl	$1, 16(%rbx)
	je	.L1593
	movq	24(%rbx), %r9
	movl	$2, 16(%rbx)
	leaq	1(%r9), %r10
	movq	%r10, 24(%rbx)
.L1579:
	incq	%r8
	movl	$2, %r12d
	movl	$0, %r10d
	cmpq	%r8, %rcx
	cmovne	%r8, %rcx
	cmovne	%r12d, %r10d
	jmp	.L1594
	.p2align 4
	.p2align 3
.L1567:
	movl	$10, %edx
	movl	%r9d, %eax
	mulw	%dx
	jo	.L1571
	movzbl	%r12b, %r9d
	addw	%ax, %r9w
	jnc	.L1568
.L1571:
	leaq	.LC28(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L1784:
	movl	$10, %edx
	movl	%r9d, %eax
	mulw	%dx
	jo	.L1571
	movzbl	%r10b, %r9d
	addw	%ax, %r9w
	jnc	.L1764
	jmp	.L1571
	.p2align 4
	.p2align 3
.L1785:
	movl	$10, %r11d
	movl	%r9d, %eax
	mulw	%r11w
	jo	.L1571
	movzbl	%r12b, %r9d
	addw	%ax, %r9w
	jnc	.L1766
	jmp	.L1571
	.p2align 4
	.p2align 3
.L1786:
	movl	$10, %r11d
	movl	%r9d, %eax
	mulw	%r11w
	jo	.L1571
	movzbl	%r10b, %r9d
	addw	%ax, %r9w
	jnc	.L1768
	jmp	.L1571
	.p2align 4
	.p2align 3
.L1788:
	leaq	2(%rcx), %r8
	xorl	%r9d, %r9d
.L1581:
	cmpq	%r8, %rdi
	je	.L1583
.L1628:
	cmpb	$125, (%r8)
	jne	.L1583
	cmpl	$2, 16(%rbx)
	je	.L1593
	movl	$1, 16(%rbx)
	jmp	.L1579
.L1582:
	leaq	2(%rcx), %r11
	cmpq	%r11, %rdi
	je	.L1583
	movzbl	2(%rcx), %r12d
	subl	$48, %r12d
	cmpb	$9, %r12b
	ja	.L1584
	movq	%rdi, %r13
	xorl	%r9d, %r9d
	movq	%r8, %r11
	movl	$16, %r10d
	subq	%r8, %r13
	andl	$3, %r13d
	je	.L1592
	cmpq	$1, %r13
	je	.L1704
	cmpq	$2, %r13
	je	.L1705
	movzbl	(%r8), %eax
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L1585
	movl	$12, %r10d
	movzbl	%al, %r9d
	leaq	1(%r8), %r11
.L1705:
	movzbl	(%r11), %edx
	leal	-48(%rdx), %r12d
	cmpb	$9, %r12b
	ja	.L1585
	subl	$4, %r10d
	js	.L1796
	leal	(%r9,%r9,4), %edx
	movzbl	%r12b, %r12d
	leal	(%r12,%rdx,2), %r9d
.L1770:
	incq	%r11
.L1704:
	movzbl	(%r11), %r13d
	leal	-48(%r13), %r12d
	cmpb	$9, %r12b
	ja	.L1585
	subl	$4, %r10d
	js	.L1797
	leal	(%r9,%r9,4), %r13d
	movzbl	%r12b, %r12d
	leal	(%r12,%r13,2), %r9d
.L1772:
	incq	%r11
	cmpq	%r11, %rdi
	je	.L1773
.L1592:
	movzbl	(%r11), %edx
	leal	-48(%rdx), %r13d
	cmpb	$9, %r13b
	ja	.L1585
	subl	$4, %r10d
	js	.L1586
	leal	(%r9,%r9,4), %edx
	movzbl	%r13b, %r13d
	leal	0(%r13,%rdx,2), %r9d
.L1587:
	movzbl	1(%r11), %eax
	leaq	1(%r11), %r12
	movq	%r12, %r11
	leal	-48(%rax), %r13d
	cmpb	$9, %r13b
	ja	.L1585
	movl	%r10d, %r11d
	subl	$4, %r11d
	js	.L1798
	leal	(%r9,%r9,4), %r11d
	movzbl	%r13b, %r13d
	leal	0(%r13,%r11,2), %r9d
.L1775:
	movzbl	1(%r12), %edx
	leaq	1(%r12), %r11
	leal	-48(%rdx), %r13d
	cmpb	$9, %r13b
	ja	.L1585
	movl	%r10d, %eax
	subl	$8, %eax
	js	.L1799
	leal	(%r9,%r9,4), %edx
	movzbl	%r13b, %r13d
	leal	0(%r13,%rdx,2), %r9d
.L1777:
	movzbl	2(%r12), %eax
	leaq	2(%r12), %r11
	leal	-48(%rax), %r13d
	cmpb	$9, %r13b
	ja	.L1585
	subl	$12, %r10d
	js	.L1800
	leal	(%r9,%r9,4), %edx
	movzbl	%r13b, %r13d
	leal	0(%r13,%rdx,2), %r9d
.L1779:
	leaq	3(%r12), %r11
	cmpq	%r11, %rdi
	jne	.L1592
.L1773:
	movq	%rdi, %r8
	jmp	.L1581
	.p2align 4
	.p2align 3
.L1783:
	movl	$10, %r12d
	movl	%r9d, %eax
	mulw	%r12w
	jo	.L1571
	movzbl	%r10b, %r9d
	addw	%ax, %r9w
	jnc	.L1761
	jmp	.L1571
	.p2align 4
	.p2align 3
.L1795:
	leaq	2(%rcx), %r8
	cmpq	%r8, %rdi
	je	.L1610
	movsbw	2(%rcx), %ax
	cmpb	$125, %al
	je	.L1801
	cmpb	$48, %al
	je	.L1802
	leal	-49(%rax), %ebp
	cmpb	$8, %bpl
	ja	.L1583
	leaq	3(%rcx), %r11
	cmpq	%r11, %rdi
	je	.L1583
	movzbl	3(%rcx), %ecx
	subl	$48, %ecx
	cmpb	$9, %cl
	ja	.L1615
	movq	%rdi, %rdx
	xorl	%eax, %eax
	movq	%r8, %rcx
	movl	$16, %r11d
	subq	%r8, %rdx
	andl	$3, %edx
	je	.L1623
	cmpq	$1, %rdx
	je	.L1708
	cmpq	$2, %rdx
	je	.L1709
	movzbl	(%r8), %r13d
	subl	$48, %r13d
	cmpb	$9, %r13b
	ja	.L1616
	movl	$12, %r11d
	movzbl	%r13b, %eax
	leaq	1(%r8), %rcx
.L1709:
	movzbl	(%rcx), %ebp
	leal	-48(%rbp), %r13d
	cmpb	$9, %r13b
	ja	.L1616
	subl	$4, %r11d
	js	.L1803
	leal	(%rax,%rax,4), %eax
	movzbl	%r13b, %r13d
	leal	0(%r13,%rax,2), %eax
.L1748:
	incq	%rcx
.L1708:
	movzbl	(%rcx), %edx
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L1616
	subl	$4, %r11d
	js	.L1804
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1750:
	incq	%rcx
	cmpq	%rcx, %rdi
	je	.L1751
.L1623:
	movzbl	(%rcx), %r13d
	leal	-48(%r13), %ebp
	cmpb	$9, %bpl
	ja	.L1616
	subl	$4, %r11d
	js	.L1617
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1618:
	movzbl	1(%rcx), %edx
	leaq	1(%rcx), %r13
	movq	%r13, %rcx
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L1616
	movl	%r11d, %ecx
	subl	$4, %ecx
	js	.L1805
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ecx
	leal	(%rcx,%rax,2), %eax
.L1753:
	movzbl	1(%r13), %edx
	leaq	1(%r13), %rcx
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L1616
	movl	%r11d, %ecx
	subl	$8, %ecx
	js	.L1806
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ecx
	leal	(%rcx,%rax,2), %eax
.L1755:
	movzbl	2(%r13), %edx
	leaq	2(%r13), %rcx
	leal	-48(%rdx), %ebp
	cmpb	$9, %bpl
	ja	.L1616
	subl	$12, %r11d
	js	.L1807
	leal	(%rax,%rax,4), %eax
	movzbl	%bpl, %ebp
	leal	0(%rbp,%rax,2), %eax
.L1757:
	leaq	3(%r13), %rcx
	cmpq	%rcx, %rdi
	jne	.L1623
.L1751:
	movq	%rdi, %r8
	jmp	.L1614
	.p2align 4
	.p2align 3
.L1782:
	movl	$10, %r10d
	movl	%r9d, %eax
	mulw	%r10w
	jo	.L1571
	movzbl	%r8b, %r9d
	addw	%ax, %r9w
	jnc	.L1759
	jmp	.L1571
	.p2align 4
	.p2align 3
.L1599:
	cmpq	%rcx, %r12
	je	.L1571
	movl	$1, %r11d
.L1608:
	movzwl	%ax, %ebp
	movzwl	%r9w, %ebx
	sall	$16, %eax
	orl	%ebx, %eax
	movl	%eax, %r12d
	cmpq	%rcx, %rdi
	je	.L1607
	movzbl	(%rcx), %eax
	cmpb	$125, %al
	je	.L1607
	jmp	.L1595
.L1600:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1571
	movzbl	%r11b, %edx
	addw	%ax, %dx
	jc	.L1571
	movl	%edx, %eax
	jmp	.L1601
.L1791:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1571
	movzbl	%bl, %edx
	addw	%ax, %dx
	jc	.L1571
	movl	%edx, %eax
	jmp	.L1742
.L1792:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1571
	movzbl	%bl, %edx
	addw	%ax, %dx
	jc	.L1571
	movl	%edx, %eax
	jmp	.L1744
.L1584:
	leal	-48(%r9), %r9d
	movq	%r11, %r8
	jmp	.L1628
.L1793:
	movl	$10, %ecx
	mulw	%cx
	jo	.L1571
	movzbl	%bl, %r13d
	addw	%ax, %r13w
	jc	.L1571
	movl	%r13d, %eax
	jmp	.L1746
.L1801:
	cmpl	$1, 16(%rbx)
	je	.L1593
	movq	24(%rbx), %rax
	movl	$2, 16(%rbx)
	leaq	1(%rax), %r11
	movq	%r11, 24(%rbx)
.L1612:
	leaq	1(%r8), %rcx
	cmpq	%rcx, %r12
	je	.L1609
	movl	$2, %r11d
	jmp	.L1608
.L1802:
	leaq	3(%rcx), %r8
	xorl	%eax, %eax
.L1614:
	cmpq	%r8, %rdi
	je	.L1583
.L1627:
	cmpb	$125, (%r8)
	jne	.L1583
	cmpl	$2, 16(%rbx)
	je	.L1593
	movl	$1, 16(%rbx)
	jmp	.L1612
.L1790:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1571
	movzbl	%r11b, %edx
	addw	%ax, %dx
	jc	.L1571
	movl	%edx, %eax
	jmp	.L1739
.L1789:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1571
	movzbl	%r11b, %edx
	addw	%ax, %dx
	jc	.L1571
	movl	%edx, %eax
	jmp	.L1737
.L1585:
	cmpq	%r11, %r8
	je	.L1583
	movq	%r11, %r8
	jmp	.L1581
.L1586:
	movl	$10, %r12d
	movl	%r9d, %eax
	mulw	%r12w
	jo	.L1583
	movzbl	%r13b, %r9d
	addw	%ax, %r9w
	jnc	.L1587
	jmp	.L1583
.L1615:
	subl	$48, %eax
	movq	%r11, %r8
	jmp	.L1627
.L1798:
	movl	$10, %edx
	movl	%r9d, %eax
	mulw	%dx
	jo	.L1583
	movzbl	%r13b, %r9d
	addw	%ax, %r9w
	jnc	.L1775
	jmp	.L1583
.L1799:
	movl	$10, %r11d
	movl	%r9d, %eax
	mulw	%r11w
	jo	.L1583
	movzbl	%r13b, %r9d
	addw	%ax, %r9w
	jnc	.L1777
	jmp	.L1583
.L1800:
	movl	$10, %r11d
	movl	%r9d, %eax
	mulw	%r11w
	jo	.L1583
	movzbl	%r13b, %r9d
	addw	%ax, %r9w
	jnc	.L1779
	jmp	.L1583
.L1797:
	movl	$10, %edx
	movl	%r9d, %eax
	mulw	%dx
	jo	.L1583
	movzbl	%r12b, %r9d
	addw	%ax, %r9w
	jnc	.L1772
	jmp	.L1583
.L1807:
	movl	$10, %ecx
	mulw	%cx
	jo	.L1583
	movzbl	%bpl, %edx
	addw	%ax, %dx
	jc	.L1583
	movl	%edx, %eax
	jmp	.L1757
.L1806:
	movl	$10, %edx
	mulw	%dx
	jo	.L1583
	movzbl	%bpl, %ebp
	addw	%ax, %bp
	jc	.L1583
	movl	%ebp, %eax
	jmp	.L1755
.L1805:
	movl	$10, %edx
	mulw	%dx
	jo	.L1583
	movzbl	%bpl, %ebp
	addw	%ax, %bp
	jc	.L1583
	movl	%ebp, %eax
	jmp	.L1753
.L1617:
	movl	$10, %edx
	mulw	%dx
	jo	.L1583
	movzbl	%bpl, %r13d
	addw	%ax, %r13w
	jc	.L1583
	movl	%r13d, %eax
	jmp	.L1618
.L1616:
	cmpq	%rcx, %r8
	je	.L1583
	movq	%rcx, %r8
	jmp	.L1614
.L1796:
	movl	$10, %r13d
	movl	%r9d, %eax
	mulw	%r13w
	jo	.L1583
	movzbl	%r12b, %r9d
	addw	%ax, %r9w
	jnc	.L1770
	jmp	.L1583
.L1804:
	movl	$10, %r13d
	mulw	%r13w
	jo	.L1583
	movzbl	%bpl, %edx
	addw	%ax, %dx
	jc	.L1583
	movl	%edx, %eax
	jmp	.L1750
.L1803:
	movl	$10, %edx
	mulw	%dx
	jo	.L1583
	movzbl	%r13b, %ebp
	addw	%ax, %bp
	jc	.L1583
	movl	%ebp, %eax
	jmp	.L1748
.L1610:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
.L1562:
	leaq	.LC27(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L1593:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
	nop
	.seh_endproc
	.section	.text$_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc:
.LFB15508:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	8(%rcx), %rbp
	movq	(%rcx), %rdi
	leaq	16(%rcx), %r12
	movq	%rcx, %rbx
	movl	%edx, %r13d
	leaq	1(%rbp), %rsi
	cmpq	%rdi, %r12
	je	.L1824
	movq	16(%rcx), %rax
	cmpq	%rsi, %rax
	jb	.L1825
.L1810:
	movb	%r13b, (%rdi,%rbp)
	movq	(%rbx), %r8
	movq	%rsi, 8(%rbx)
	movb	$0, (%r8,%rsi)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L1825:
	testq	%rsi, %rsi
	js	.L1826
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rsi
	jb	.L1827
	movq	%rbp, %rcx
	movq	%rsi, %r14
	addq	$2, %rcx
	js	.L1814
.L1815:
	call	_Znwy
	movq	(%rbx), %r15
	movq	%rax, %rdi
	testq	%rbp, %rbp
	jne	.L1811
.L1816:
	cmpq	%r15, %r12
	je	.L1819
	movq	16(%rbx), %rcx
	leaq	1(%rcx), %rdx
	movq	%r15, %rcx
	call	_ZdlPvy
.L1819:
	movq	%rdi, (%rbx)
	movq	%r14, 16(%rbx)
	jmp	.L1810
	.p2align 4
	.p2align 3
.L1824:
	cmpq	$16, %rsi
	jne	.L1810
	movl	$31, %ecx
	movl	$30, %r14d
	call	_Znwy
	movq	(%rbx), %r15
	movq	%rax, %rdi
.L1811:
	cmpq	$1, %rbp
	je	.L1828
	movq	%rbp, %r8
	movq	%r15, %rdx
	movq	%rdi, %rcx
	call	memcpy
	jmp	.L1816
	.p2align 4
	.p2align 3
.L1828:
	movzbl	(%r15), %edx
	movb	%dl, (%rdi)
	jmp	.L1816
	.p2align 4
	.p2align 3
.L1827:
	leaq	1(%r14), %rcx
	testq	%r14, %r14
	jns	.L1815
.L1814:
	call	_ZSt17__throw_bad_allocv
.L1826:
	leaq	.LC2(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	.def	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE:
.LFB15627:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	(%rdx), %rdi
	movq	8(%rdx), %rbp
	movq	%rcx, %rsi
	jmp	.L1839
	.p2align 4
	.p2align 3
.L1841:
	movq	%rbp, %rdx
	call	memcpy
	addq	%rbx, 24(%rsi)
.L1840:
	movq	(%rsi), %rdx
	addq	%rbx, %rbp
	subq	%rbx, %rdi
	movq	%rsi, %rcx
	call	*(%rdx)
.L1839:
	movq	24(%rsi), %rcx
	movq	16(%rsi), %rbx
	movq	%rcx, %rax
	subq	8(%rsi), %rax
	subq	%rax, %rbx
	cmpq	%rbx, %rdi
	jb	.L1830
	cmpq	%rdi, %rbx
	movq	%rdi, %r8
	cmovbe	%rbx, %r8
	testq	%r8, %r8
	jne	.L1841
	addq	%rbx, %rcx
	movq	%rcx, 24(%rsi)
	jmp	.L1840
	.p2align 4
	.p2align 3
.L1830:
	testq	%rdi, %rdi
	jne	.L1842
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
	.p2align 4
	.p2align 3
.L1842:
	movq	%rdi, %r8
	movq	%rbp, %rdx
	call	memcpy
	addq	%rdi, 24(%rsi)
	addq	$40, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	ret
	.seh_endproc
	.section	.text$_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	.def	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi:
.LFB15658:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$200, %rsp
	.seh_stackalloc	200
	vmovaps	%xmm6, 160(%rsp)
	.seh_savexmm	%xmm6, 160
	vmovaps	%xmm7, 176(%rsp)
	.seh_savexmm	%xmm7, 176
	.seh_endprologue
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	movq	%rcx, %rbx
	movq	%r9, %rbp
	movb	$0, 48(%rsp)
	vmovq	%rdx, %xmm7
	vmovq	%rax, %xmm6
	movl	304(%rsp), %edx
	cmpl	$3, %r8d
	je	.L2093
	cmpl	$2, %r8d
	je	.L1910
	cmpl	$126, %edx
	ja	.L1911
	cmpq	$31, %r9
	ja	.L2094
	testq	%r9, %r9
	je	.L1851
	leaq	48(%rsp), %r13
	movq	%r9, %r8
	movq	%r13, %rcx
	call	memset
	vmovq	%xmm6, %r8
	testq	%r8, %r8
	jne	.L1852
	leaq	32(%rsp), %r14
.L1854:
	movq	%rbp, %r12
	jmp	.L1853
	.p2align 4
	.p2align 3
.L1910:
	movq	%r9, %r14
	xorl	%r12d, %r12d
.L1845:
	cmpl	$126, %edx
	ja	.L1846
	cmpq	$31, %rbp
	ja	.L1916
	testq	%rbp, %rbp
	jne	.L1879
	testq	%r14, %r14
	je	.L2095
.L1925:
.L1891:
	jmp	.L1891
	.p2align 4
	.p2align 3
.L1916:
	movl	$32, %ebp
.L1879:
	leaq	48(%rsp), %r13
	movl	%ebp, %r15d
	movq	%r13, %rdi
	cmpl	$4, %ebp
	jb	.L1885
	movzbl	%dl, %eax
	movl	%ebp, %ecx
	imull	$16843009, %eax, %eax
	shrl	$2, %ecx
	rep stosl
.L1885:
	movl	%r15d, %esi
	andl	$3, %esi
	je	.L1887
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L2032
	cmpl	$2, %esi
	je	.L2033
	cmpl	$3, %esi
	je	.L2034
	movl	$1, %r8d
	movb	%dl, (%rdi)
	leal	1(%r8), %r10d
	movb	%dl, (%rdi,%r8)
	leal	1(%r10), %r15d
	movb	%dl, (%rdi,%r10)
	movq	%r15, %rax
	movb	%dl, (%rdi,%r15)
	incl	%eax
.L2034:
	movl	%eax, %ecx
	incl	%eax
	movb	%dl, (%rdi,%rcx)
.L2033:
	movl	%eax, %r8d
	incl	%eax
	movb	%dl, (%rdi,%r8)
.L2032:
	movl	%eax, %r9d
	incl	%eax
	movb	%dl, (%rdi,%r9)
	cmpl	%esi, %eax
	jnb	.L1887
.L1886:
	movl	%eax, %r10d
	leal	1(%rax), %r11d
	leal	2(%rax), %r15d
	leal	3(%rax), %ecx
	movb	%dl, (%rdi,%r10)
	leal	4(%rax), %r8d
	movb	%dl, (%rdi,%r11)
	leal	5(%rax), %r9d
	leal	6(%rax), %r10d
	leal	7(%rax), %r11d
	addl	$8, %eax
	movb	%dl, (%rdi,%r15)
	movb	%dl, (%rdi,%rcx)
	movb	%dl, (%rdi,%r8)
	movb	%dl, (%rdi,%r9)
	movb	%dl, (%rdi,%r10)
	movb	%dl, (%rdi,%r11)
	cmpl	%esi, %eax
	jb	.L1886
	.p2align 4
	.p2align 3
.L1887:
	testq	%r14, %r14
	je	.L1889
	cmpq	%r14, %rbp
	jnb	.L1890
	testq	%rbp, %rbp
	je	.L1925
.L1898:
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	movq	%rbp, %rdi
	movq	%r13, %r15
	movq	%rcx, %rdx
	subq	8(%rbx), %rdx
	subq	%rdx, %rsi
	cmpq	%rsi, %rbp
	jnb	.L1896
	jmp	.L1892
	.p2align 4
	.p2align 3
.L2097:
	movq	%r15, %rdx
	call	memcpy
	addq	%rsi, 24(%rbx)
.L2089:
	addq	%rsi, %r15
	subq	%rsi, %rdi
	movq	(%rbx), %rsi
	movq	%rbx, %rcx
.LEHB36:
	call	*(%rsi)
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	movq	%rcx, %rax
	subq	8(%rbx), %rax
	subq	%rax, %rsi
	cmpq	%rsi, %rdi
	jb	.L2096
.L1896:
	cmpq	%rdi, %rsi
	movq	%rdi, %r8
	cmovbe	%rsi, %r8
	testq	%r8, %r8
	jne	.L2097
	addq	%rsi, %rcx
	movq	%rcx, 24(%rbx)
	jmp	.L2089
	.p2align 4
	.p2align 3
.L2096:
	testq	%rdi, %rdi
	jne	.L1892
.L1897:
	subq	%rbp, %r14
	cmpq	%r14, %rbp
	jb	.L1898
	testq	%r14, %r14
	je	.L1889
.L1890:
	leaq	32(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%r14, 32(%rsp)
	movq	%r13, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
.L1889:
	vmovq	%xmm6, %r14
	testq	%r14, %r14
	jne	.L2098
.L1900:
	testq	%r12, %r12
	je	.L1934
.L1849:
	cmpq	%r12, %rbp
	jnb	.L2091
	testq	%rbp, %rbp
	je	.L1930
.L1908:
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	movq	%rbp, %rdi
	movq	%r13, %r15
	movq	%rcx, %r9
	subq	8(%rbx), %r9
	subq	%r9, %rsi
	cmpq	%rsi, %rbp
	jnb	.L1906
	jmp	.L1902
	.p2align 4
	.p2align 3
.L2100:
	movq	%r15, %rdx
	call	memcpy
	addq	%rsi, 24(%rbx)
.L2090:
	movq	(%rbx), %r10
	movq	%rbx, %rcx
	addq	%rsi, %r15
	subq	%rsi, %rdi
	call	*(%r10)
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rsi
	movq	%rcx, %r11
	subq	8(%rbx), %r11
	subq	%r11, %rsi
	cmpq	%rsi, %rdi
	jb	.L2099
.L1906:
	cmpq	%rsi, %rdi
	movq	%rsi, %r8
	cmovbe	%rdi, %r8
	testq	%r8, %r8
	jne	.L2100
	addq	%rsi, %rcx
	movq	%rcx, 24(%rbx)
	jmp	.L2090
	.p2align 4
	.p2align 3
.L2099:
	testq	%rdi, %rdi
	jne	.L1902
.L1907:
	subq	%rbp, %r12
	cmpq	%r12, %rbp
	jb	.L1908
	testq	%r12, %r12
	je	.L1934
.L2091:
	leaq	32(%rsp), %r14
.L1853:
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movq	%r12, 32(%rsp)
	movq	%r13, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	nop
.L1934:
	vmovaps	160(%rsp), %xmm6
	vmovaps	176(%rsp), %xmm7
	movq	%rbx, %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L1902:
	movq	%rdi, %r8
	movq	%r15, %rdx
	call	memcpy
	addq	%rdi, 24(%rbx)
	jmp	.L1907
	.p2align 4
	.p2align 3
.L1892:
	movq	%rdi, %r8
	movq	%r15, %rdx
	call	memcpy
	addq	%rdi, 24(%rbx)
	jmp	.L1897
	.p2align 4
	.p2align 3
.L2093:
	movq	%r9, %r14
	andl	$1, %ebp
	shrq	%r14
	addq	%r14, %rbp
	movq	%rbp, %r12
	jmp	.L1845
	.p2align 4
	.p2align 3
.L2094:
	vpbroadcastb	%edx, %ymm0
	vmovdqu8	%ymm0, 48(%rsp)
	testq	%rax, %rax
	jne	.L1848
	vzeroupper
.L1850:
	movq	%rbp, %r12
	leaq	48(%rsp), %r13
	movl	$32, %ebp
	jmp	.L1849
	.p2align 4
	.p2align 3
.L2098:
	leaq	32(%rsp), %rdx
	movq	%rbx, %rcx
	vmovq	%xmm6, 32(%rsp)
	vmovq	%xmm7, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L1900
.L1911:
	movq	%r9, %r12
	xorl	%r14d, %r14d
.L1846:
	cmpl	$55295, %edx
	ja	.L1858
	movl	$32, %r10d
	lzcntl	%edx, %r15d
	subl	%r15d, %r10d
	cmpl	$7, %r10d
	jne	.L2101
	movl	%edx, %ecx
	movl	$1, %edi
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
.L1863:
	movzbl	%al, %r11d
	movzbl	%r9b, %esi
	movzbl	%r8b, %ebp
	movzbl	%cl, %edx
	sall	$8, %r11d
	movb	%cl, 96(%rsp)
	orl	%esi, %r11d
	leaq	96(%rsp), %r13
	movl	$1, %r8d
	sall	$8, %r11d
	movq	%r13, 80(%rsp)
	orl	%ebp, %r11d
	sall	$8, %r11d
	orl	%edx, %r11d
	movl	%r11d, 112(%rsp)
	cmpb	$1, %dil
	je	.L1864
	movzbl	113(%rsp), %r9d
	movl	$2, %r8d
	movb	%r9b, 97(%rsp)
	cmpb	$2, %dil
	je	.L1864
	movzbl	114(%rsp), %eax
	movl	$3, %r8d
	movb	%al, 98(%rsp)
	cmpb	$3, %dil
	je	.L1864
	movzbl	115(%rsp), %ecx
	movl	$4, %r8d
	movb	%cl, 99(%rsp)
.L1864:
	movq	%r8, 88(%rsp)
	movb	$0, 96(%rsp,%r8)
	movq	88(%rsp), %rdi
	movq	80(%rsp), %rsi
	testq	%r14, %r14
	je	.L2102
	testq	%rdi, %rdi
	jne	.L1871
	vmovq	%xmm6, %r11
	testq	%r11, %r11
	jne	.L2087
.L1869:
	cmpq	%r13, %rsi
	je	.L1934
	movq	96(%rsp), %r13
	movq	%rsi, %rcx
	leaq	1(%r13), %rdx
	call	_ZdlPvy
	jmp	.L1934
.L1851:
	testq	%rax, %rax
	je	.L1934
	leaq	32(%rsp), %rdx
	vmovq	%xmm7, 40(%rsp)
	movq	%rax, 32(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L1934
.L1848:
	vmovq	%xmm7, 40(%rsp)
	leaq	32(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%rax, 32(%rsp)
	vzeroupper
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
.LEHE36:
	jmp	.L1850
.L1871:
	leaq	-1(%r14), %rbp
	andl	$7, %r14d
	leaq	32(%rsp), %r15
	je	.L1872
	cmpq	$1, %r14
	je	.L2020
	cmpq	$2, %r14
	je	.L2021
	cmpq	$3, %r14
	je	.L2022
	cmpq	$4, %r14
	je	.L2023
	cmpq	$5, %r14
	je	.L2024
	cmpq	$6, %r14
	je	.L2025
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
.LEHB37:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2025:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2024:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2023:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2022:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2021:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2020:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	subq	$1, %rbp
	jb	.L2086
.L1872:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	leaq	-1(%rbp), %r14
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	leaq	-7(%r14), %rbp
	cmpq	$6, %r14
	jne	.L1872
.L2086:
	vmovq	%xmm6, %r10
	testq	%r10, %r10
	jne	.L1867
	leaq	-1(%r12), %rbp
	testq	%r12, %r12
	je	.L2088
.L2019:
	leaq	1(%rbp), %r12
	andl	$7, %r12d
	je	.L1877
	cmpq	$1, %r12
	je	.L2026
	cmpq	$2, %r12
	je	.L2027
	cmpq	$3, %r12
	je	.L2028
	cmpq	$4, %r12
	je	.L2029
	cmpq	$5, %r12
	je	.L2030
	cmpq	$6, %r12
	je	.L2031
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2031:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2030:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2029:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2028:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2027:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	decq	%rbp
.L2026:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	subq	$1, %rbp
	jb	.L2088
.L1877:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	leaq	-1(%rbp), %r14
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%r15, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
.LEHE37:
	leaq	-7(%r14), %rbp
	cmpq	$6, %r14
	jne	.L1877
.L2088:
	movq	80(%rsp), %rsi
	jmp	.L1869
.L2095:
	vmovq	%xmm6, %rbp
	testq	%rbp, %rbp
	jne	.L2103
	testq	%r12, %r12
	je	.L1934
.L1930:
.L1901:
	jmp	.L1901
	.p2align 4
	.p2align 3
.L1852:
	leaq	32(%rsp), %r14
	movq	%rbx, %rcx
	vmovq	%xmm6, 32(%rsp)
	movq	%r14, %rdx
	vmovq	%xmm7, 40(%rsp)
.LEHB38:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
.LEHE38:
	jmp	.L1854
.L2102:
	vmovq	%xmm6, %r14
	testq	%r14, %r14
	je	.L1870
.L2087:
	leaq	32(%rsp), %r15
.L1867:
	movq	%r15, %rdx
	movq	%rbx, %rcx
	vmovq	%xmm6, 32(%rsp)
	vmovq	%xmm7, 40(%rsp)
.LEHB39:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
.LEHE39:
.L1870:
	testq	%r12, %r12
	je	.L2088
	testq	%rdi, %rdi
	je	.L2088
	leaq	-1(%r12), %rbp
	leaq	32(%rsp), %r15
	jmp	.L2019
.L1858:
	leal	-57344(%rdx), %eax
	cmpl	$1056767, %eax
	ja	.L1912
	lzcntl	%edx, %ecx
	cmpl	$16, %ecx
	je	.L1861
	movl	%edx, %r9d
	movl	%edx, %eax
	movl	%edx, %ecx
	movl	$4, %edi
	shrl	$12, %edx
	shrl	$6, %r9d
	shrl	$18, %ecx
	movl	%edx, %r8d
	andl	$63, %r9d
	andl	$63, %eax
	orl	$-16, %ecx
	andl	$63, %r8d
	orl	$-128, %r9d
	orl	$-128, %eax
	orl	$-128, %r8d
	jmp	.L1863
.L1912:
	movl	$65533, %edx
.L1861:
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	%edx, %r9d
	movl	$3, %edi
	shrl	$6, %r8d
	shrl	$12, %ecx
	andl	$63, %r8d
	andl	$63, %r9d
	orl	$-32, %ecx
	xorl	%eax, %eax
	orl	$-128, %r8d
	orl	$-128, %r9d
	jmp	.L1863
.L2103:
	leaq	32(%rsp), %rdx
	movq	%rbx, %rcx
	vmovq	%xmm6, 32(%rsp)
	vmovq	%xmm7, 40(%rsp)
.LEHB40:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	testq	%r12, %r12
	je	.L1934
	jmp	.L1901
.L2101:
	movl	%edx, %r8d
	movl	%edx, %ecx
	andl	$63, %r8d
	shrl	$6, %ecx
	orl	$-128, %r8d
	cmpl	$11, %r10d
	jg	.L1861
	orl	$-64, %ecx
	movl	$2, %edi
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	jmp	.L1863
.L1919:
	movq	%rax, %rbx
	leaq	80(%rsp), %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbx, %rcx
	call	_Unwind_Resume
	nop
.LEHE40:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15658:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15658-.LLSDACSB15658
.LLSDACSB15658:
	.uleb128 .LEHB36-.LFB15658
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB37-.LFB15658
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L1919-.LFB15658
	.uleb128 0
	.uleb128 .LEHB38-.LFB15658
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB39-.LFB15658
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L1919-.LFB15658
	.uleb128 0
	.uleb128 .LEHB40-.LFB15658
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0
	.uleb128 0
.LLSDACSE15658:
	.section	.text$_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
	.def	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE:
.LFB15544:
	pushq	%rbx
	.seh_pushreg	%rbx
	addq	$-128, %rsp
	.seh_stackalloc	128
	.seh_endprologue
	movzwl	(%r9), %eax
	movq	(%rcx), %r10
	movq	8(%rcx), %r11
	movq	%rdx, %rcx
	movq	%r8, %rdx
	andw	$384, %ax
	cmpw	$128, %ax
	je	.L2131
	cmpw	$256, %ax
	je	.L2107
.L2122:
	movq	16(%rdx), %rbx
	testq	%r10, %r10
	jne	.L2132
.L2109:
	movq	%rbx, %rax
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4
	.p2align 3
.L2131:
	movzwl	4(%r9), %eax
.L2106:
	cmpq	%rax, %rcx
	jnb	.L2122
	movzbl	(%r9), %ebx
	movl	8(%r9), %r9d
	movl	%ebx, %r8d
	andl	$3, %r8d
	andl	$3, %ebx
	movq	16(%rdx), %rbx
	cmove	176(%rsp), %r8d
	subq	%rcx, %rax
	movl	%r9d, 32(%rsp)
	leaq	48(%rsp), %rdx
	movq	%r10, 48(%rsp)
	movq	%rax, %r9
	movq	%r11, 56(%rsp)
	movq	%rbx, %rcx
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4
	.p2align 3
.L2132:
	leaq	48(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%r10, 48(%rsp)
	movq	%r11, 56(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L2109
	.p2align 4
	.p2align 3
.L2107:
	movzbl	(%rdx), %eax
	movzwl	4(%r9), %r8d
	movl	%eax, %ebx
	andl	$15, %eax
	andl	$15, %ebx
	cmpq	%rax, %r8
	jnb	.L2110
	movl	$4, %ebx
	shrx	%rbx, (%rdx), %rax
	leaq	(%r8,%r8,4), %rbx
	salq	$4, %r8
	shrx	%rbx, %rax, %rax
	addq	8(%rdx), %r8
	andl	$31, %eax
	vmovdqa	(%r8), %xmm1
	vmovdqa	%xmm1, 96(%rsp)
.L2111:
	leaq	.L2115(%rip), %r8
	movb	%al, 112(%rsp)
	movzbl	%al, %eax
	vmovdqu	96(%rsp), %ymm0
	movslq	(%r8,%rax,4), %rbx
	addq	%r8, %rbx
	vmovdqu	%ymm0, 64(%rsp)
	jmp	*%rbx
	.section .rdata,"dr"
	.align 4
.L2115:
	.long	.L2129-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2119-.L2115
	.long	.L2118-.L2115
	.long	.L2117-.L2115
	.long	.L2116-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.long	.L2120-.L2115
	.section	.text$_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L2110:
	testb	%bl, %bl
	jne	.L2112
	movl	$4, %eax
	shrx	%rax, (%rdx), %rbx
	cmpq	%rbx, %r8
	jb	.L2133
.L2112:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L2117:
	movq	64(%rsp), %rax
	testq	%rax, %rax
	js	.L2120
.L2130:
	vzeroupper
	jmp	.L2106
.L2116:
	movq	64(%rsp), %rax
	jmp	.L2130
.L2118:
	movl	64(%rsp), %eax
	vzeroupper
	jmp	.L2106
.L2119:
	movslq	64(%rsp), %rax
	testl	%eax, %eax
	jns	.L2130
.L2120:
	leaq	.LC6(%rip), %rcx
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L2129:
	vzeroupper
	jmp	.L2112
	.p2align 4
	.p2align 3
.L2133:
	salq	$5, %r8
	addq	8(%rdx), %r8
	vmovdqu	(%r8), %xmm2
	movzbl	16(%r8), %eax
	vmovdqa	%xmm2, 96(%rsp)
	jmp	.L2111
	.seh_endproc
	.text
	.p2align 4
	.def	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0:
.LFB15942:
	pushq	%rbx
	.seh_pushreg	%rbx
	addq	$-128, %rsp
	.seh_stackalloc	128
	.seh_endprologue
	movq	(%rcx), %r10
	movq	8(%rcx), %r9
	movq	%rdx, %rcx
	movzwl	(%r8), %edx
	movq	%r8, %rax
	andw	$384, %dx
	cmpw	$128, %dx
	je	.L2156
	cmpw	$256, %dx
	je	.L2137
.L2151:
	movq	16(%rcx), %rbx
	leaq	48(%rsp), %rdx
	movq	$1, 48(%rsp)
	movq	%r9, 56(%rsp)
	movq	%rbx, %rcx
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	%rbx, %rax
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4
	.p2align 3
.L2156:
	movzwl	4(%r8), %edx
.L2136:
	cmpq	$1, %rdx
	jbe	.L2151
	movzbl	(%rax), %r11d
	movq	16(%rcx), %rcx
	movl	$1, %ebx
	movl	%r11d, %r8d
	andl	$3, %r8d
	andl	$3, %r11d
	movl	8(%rax), %r11d
	movq	%r10, 48(%rsp)
	leaq	48(%rsp), %r10
	movq	%r9, 56(%rsp)
	cmove	%ebx, %r8d
	leaq	-1(%rdx), %r9
	movq	%r10, %rdx
	movl	%r11d, 32(%rsp)
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4
	.p2align 3
.L2137:
	movzbl	(%rcx), %ebx
	movzwl	4(%r8), %r8d
	movl	%ebx, %r11d
	andl	$15, %ebx
	andl	$15, %r11d
	cmpq	%rbx, %r8
	jnb	.L2139
	leaq	(%r8,%r8,4), %r11
	movl	$4, %edx
	shrx	%rdx, (%rcx), %rbx
	salq	$4, %r8
	shrx	%r11, %rbx, %r11
	addq	8(%rcx), %r8
	andl	$31, %r11d
	vmovdqa	(%r8), %xmm1
	vmovdqa	%xmm1, 96(%rsp)
.L2140:
	leaq	.L2144(%rip), %r8
	movzbl	%r11b, %edx
	movb	%r11b, 112(%rsp)
	vmovdqu	96(%rsp), %ymm0
	movslq	(%r8,%rdx,4), %rbx
	addq	%r8, %rbx
	vmovdqu	%ymm0, 64(%rsp)
	jmp	*%rbx
	.section .rdata,"dr"
	.align 4
.L2144:
	.long	.L2154-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2148-.L2144
	.long	.L2147-.L2144
	.long	.L2146-.L2144
	.long	.L2145-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.long	.L2149-.L2144
	.text
	.p2align 4
	.p2align 3
.L2139:
	testb	%r11b, %r11b
	jne	.L2141
	movl	$4, %edx
	shrx	%rdx, (%rcx), %rbx
	cmpq	%rbx, %r8
	jb	.L2157
.L2141:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L2146:
	movq	64(%rsp), %rdx
	testq	%rdx, %rdx
	js	.L2149
.L2155:
	vzeroupper
	jmp	.L2136
.L2145:
	movq	64(%rsp), %rdx
	jmp	.L2155
.L2147:
	movl	64(%rsp), %edx
	vzeroupper
	jmp	.L2136
.L2148:
	movslq	64(%rsp), %rdx
	testl	%edx, %edx
	jns	.L2155
.L2149:
	leaq	.LC6(%rip), %rcx
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L2154:
	vzeroupper
	jmp	.L2141
	.p2align 4
	.p2align 3
.L2157:
	salq	$5, %r8
	addq	8(%rcx), %r8
	vmovdqu	(%r8), %xmm2
	movzbl	16(%r8), %r11d
	vmovdqa	%xmm2, 96(%rsp)
	jmp	.L2140
	.seh_endproc
	.section	.text$_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv
	.def	_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv
_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv:
.LFB15702:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$216, %rsp
	.seh_stackalloc	216
	vmovaps	%xmm6, 176(%rsp)
	.seh_savexmm	%xmm6, 176
	vmovaps	%xmm7, 192(%rsp)
	.seh_savexmm	%xmm7, 192
	.seh_endprologue
	movq	48(%rcx), %r10
	movq	32(%rcx), %r15
	movq	%rcx, %rsi
	cmpq	%r15, %r10
	je	.L2255
	vmovdqu	16(%rcx), %ymm0
	movl	4(%rcx), %r14d
	leaq	80(%rsp), %rax
	leaq	_ZNSt9__unicode9__v15_1_011__gcb_edgesE(%rip), %r12
	vmovq	%rax, %xmm6
	leaq	.L2190(%rip), %r13
	movq	%r10, 112(%rsp)
	vmovdqu	%ymm0, 80(%rsp)
	movzbl	104(%rsp), %ebp
.L2160:
	movzbl	105(%rsp), %edx
	movzbl	%bpl, %ecx
	incl	%ecx
	cmpl	%edx, %ecx
	je	.L2286
	jge	.L2225
	incl	%ebp
	movb	%bpl, 104(%rsp)
.L2225:
	cmpq	%r15, %r10
	je	.L2219
	movzbl	104(%rsp), %edi
	xorl	%ebx, %ebx
	movl	$1700, %eax
	movq	%r12, %r9
	movq	%rdi, %rbp
	movl	80(%rsp,%rdi,4), %edi
	movl	%edi, %r11d
	sall	$4, %r11d
	orl	$15, %r11d
	.p2align 4
	.p2align 3
.L2162:
	testq	%rax, %rax
	jle	.L2287
.L2163:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%r9,%rdx,4), %r8
	movl	(%r8), %ecx
	cmpl	%r11d, %ecx
	jnb	.L2230
	subq	%rdx, %rax
	leaq	4(%r8), %r9
	movl	%ecx, %ebx
	decq	%rax
	testq	%rax, %rax
	jg	.L2163
.L2287:
	movzbl	8(%rsi), %r8d
	andl	$15, %ebx
	movl	%ebx, %r11d
	cmpb	$3, %r8b
	je	.L2164
	cmpb	$1, %r8b
	je	.L2165
	cmpl	$10, %ebx
	je	.L2288
	cmpl	$4, %ebx
	je	.L2170
.L2175:
	movb	$3, 8(%rsi)
.L2164:
	cmpl	$13, %ebx
	jne	.L2170
	movzbl	9(%rsi), %edx
	incl	%edx
.L2168:
	movl	%edi, %r9d
	movb	%dl, 9(%rsi)
	andb	$127, %r9b
	cmpl	$2381, %r9d
	je	.L2182
	leal	-2765(%rdi), %eax
	testl	$-129, %eax
	je	.L2182
	movl	%edi, %ecx
	andb	$-2, %ch
	cmpl	$3149, %ecx
	je	.L2182
.L2228:
	leal	-1(%r14), %r8d
	cmpl	$1, %r8d
	jbe	.L2184
	cmpl	$2, %ebx
	setne	%cl
	cmpl	$3, %r14d
	je	.L2186
	leal	-1(%rbx), %edx
	cmpl	$2, %edx
	ja	.L2229
.L2184:
	movl	%edi, (%rsi)
	movl	%r11d, 4(%rsi)
	movw	$0, 8(%rsi)
	movb	$0, 10(%rsi)
	.p2align 4
	.p2align 3
.L2219:
	vmovdqu	80(%rsp), %ymm2
	movq	112(%rsp), %r15
	movq	%r15, 48(%rsi)
	vmovdqu	%ymm2, 16(%rsi)
	vzeroupper
.L2255:
	vmovaps	176(%rsp), %xmm6
	vmovaps	192(%rsp), %xmm7
	movq	%rsi, %rax
	addq	$216, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L2230:
	movq	%rdx, %rax
	jmp	.L2162
	.p2align 4
	.p2align 3
.L2182:
	movb	$1, 10(%rsi)
	jmp	.L2228
	.p2align 4
	.p2align 3
.L2288:
	cmpb	$2, %r8b
	je	.L2173
	movl	(%rsi), %edx
	cmpl	$168, %edx
	ja	.L2231
.L2174:
	movb	$3, 8(%rsi)
.L2170:
	xorl	%edx, %edx
	jmp	.L2168
	.p2align 4
	.p2align 3
.L2286:
	cmpq	%r15, %r10
	je	.L2219
	movzbl	106(%rsp), %ebx
	addq	%r15, %rbx
	movq	%rbx, 96(%rsp)
	cmpq	%rbx, %r10
	je	.L2289
	vmovq	%xmm6, %rcx
	movq	%r10, 40(%rsp)
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	96(%rsp), %r15
	movq	40(%rsp), %r10
	jmp	.L2225
	.p2align 4
	.p2align 3
.L2165:
	testl	%ebx, %ebx
	jne	.L2175
	cmpl	$168, %edi
	ja	.L2233
	leal	-1(%r14), %r8d
	movw	$3, 8(%rsi)
	cmpl	$1, %r8d
	jbe	.L2184
	cmpl	$3, %r14d
	je	.L2184
	.p2align 4
	.p2align 3
.L2229:
	leal	-7(%r14), %r9d
	cmpl	$5, %r9d
	ja	.L2188
	movslq	0(%r13,%r9,4), %rax
	addq	%r13, %rax
	jmp	*%rax
	.section .rdata,"dr"
	.align 4
.L2190:
	.long	.L2192-.L2190
	.long	.L2191-.L2190
	.long	.L2189-.L2190
	.long	.L2188-.L2190
	.long	.L2191-.L2190
	.long	.L2189-.L2190
	.section	.text$_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L2189:
	cmpl	$9, %ebx
	setne	%cl
.L2186:
	testb	%cl, %cl
	jne	.L2184
.L2195:
	movl	%ebx, %r14d
	jmp	.L2160
	.p2align 4
	.p2align 3
.L2289:
	movb	$0, 104(%rsp)
	jmp	.L2219
	.p2align 4
	.p2align 3
.L2191:
	leal	-8(%rbx), %r14d
	cmpl	$1, %r14d
	ja	.L2184
	jmp	.L2195
.L2192:
	cmpl	$8, %ebx
	ja	.L2193
	cmpl	$6, %ebx
	ja	.L2195
	jmp	.L2184
.L2188:
	movl	$1104, %edx
	btq	%rbx, %rdx
	setc	%r8b
	cmpl	$5, %r14d
	sete	%r9b
	orb	%r9b, %r8b
	jne	.L2195
	movzbl	10(%rsi), %eax
	movb	%al, 40(%rsp)
	testb	%al, %al
	jne	.L2290
.L2197:
	cmpl	$10, %r14d
	je	.L2291
	cmpl	$13, %r14d
	jne	.L2184
	cmpl	%ebx, %r14d
	jne	.L2184
	testb	$1, 9(%rsi)
	jne	.L2195
	movl	$13, %r11d
	jmp	.L2184
	.p2align 4
	.p2align 3
.L2231:
	movl	$156, %eax
	leaq	_ZNSt9__unicode9__v15_1_014__xpicto_edgesE(%rip), %r9
.L2171:
	testq	%rax, %rax
	jle	.L2292
.L2172:
	movq	%rax, %rcx
	sarq	%rcx
	leaq	(%r9,%rcx,4), %r8
	cmpl	(%r8), %edx
	jb	.L2232
	subq	%rcx, %rax
	leaq	4(%r8), %r9
	decq	%rax
	testq	%rax, %rax
	jg	.L2172
.L2292:
	leaq	_ZNSt9__unicode9__v15_1_014__xpicto_edgesE(%rip), %rcx
	subq	%rcx, %r9
	andl	$4, %r9d
	je	.L2174
.L2173:
	xorl	%edx, %edx
	movb	$1, 8(%rsi)
	jmp	.L2168
.L2193:
	leal	-11(%rbx), %r8d
	cmpl	$1, %r8d
	ja	.L2184
	jmp	.L2195
.L2233:
	leaq	_ZNSt9__unicode9__v15_1_014__xpicto_edgesE(%rip), %r8
	movl	$156, %eax
	movq	%r8, %rcx
.L2179:
	testq	%rax, %rax
	jle	.L2293
.L2180:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%rcx,%rdx,4), %r9
	cmpl	(%r9), %edi
	jb	.L2235
	subq	%rdx, %rax
	leaq	4(%r9), %rcx
	decq	%rax
	testq	%rax, %rax
	jg	.L2180
.L2293:
	subq	%r8, %rcx
	andl	$4, %ecx
	je	.L2174
	xorl	%edx, %edx
	movb	$2, 8(%rsi)
	jmp	.L2168
.L2235:
	movq	%rdx, %rax
	jmp	.L2179
.L2232:
	movq	%rcx, %rax
	jmp	.L2171
.L2291:
	cmpb	$2, 8(%rsi)
	jne	.L2184
	jmp	.L2195
.L2290:
	movl	(%rsi), %ecx
	sall	$2, %ecx
	cmpl	$3073, %ecx
	jbe	.L2197
	orl	$3, %ecx
	movl	$389, %eax
	movl	$0, 48(%rsp)
	movl	%r14d, 64(%rsp)
	movl	%ecx, 60(%rsp)
	leaq	_ZNSt9__unicode9__v15_1_012__incb_edgesE(%rip), %rcx
	movb	%r8b, 68(%rsp)
.L2199:
	testq	%rax, %rax
	jle	.L2294
.L2200:
	movq	%rax, %rdx
	movl	60(%rsp), %r8d
	sarq	%rdx
	leaq	(%rcx,%rdx,4), %r14
	movl	(%r14), %r9d
	cmpl	%r8d, %r9d
	jnb	.L2236
	subq	%rdx, %rax
	leaq	4(%r14), %rcx
	movl	%r9d, 48(%rsp)
	decq	%rax
	testq	%rax, %rax
	jg	.L2200
.L2294:
	movl	48(%rsp), %edx
	movl	64(%rsp), %r14d
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L2197
	leal	0(,%rdi,4), %r9d
	cmpl	$3073, %r9d
	jbe	.L2197
	orl	$3, %r9d
	movl	$389, %eax
	leaq	_ZNSt9__unicode9__v15_1_012__incb_edgesE(%rip), %rcx
	movl	$0, 48(%rsp)
	movl	%r9d, 60(%rsp)
.L2202:
	testq	%rax, %rax
	jle	.L2295
.L2203:
	movq	%rax, %rdx
	movl	60(%rsp), %r8d
	sarq	%rdx
	leaq	(%rcx,%rdx,4), %r14
	movl	(%r14), %r9d
	cmpl	%r8d, %r9d
	jnb	.L2237
	subq	%rdx, %rax
	leaq	4(%r14), %rcx
	movl	%r9d, 48(%rsp)
	decq	%rax
	testq	%rax, %rax
	jg	.L2203
.L2295:
	movl	48(%rsp), %eax
	movl	64(%rsp), %r14d
	movzbl	68(%rsp), %r8d
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.L2197
	vmovdqu	16(%rsi), %ymm1
	movq	48(%rsi), %rcx
	leaq	128(%rsp), %rdx
	movl	%ebx, 60(%rsp)
	vmovq	%rdx, %xmm7
	movl	%r8d, %ebx
	movq	%rsi, %r8
	movq	%r10, 48(%rsp)
	movl	%r11d, 64(%rsp)
	movq	%rcx, 160(%rsp)
	vmovdqu	%ymm1, 128(%rsp)
	movzbl	152(%rsp), %ecx
	movq	144(%rsp), %r9
.L2204:
	movzbl	153(%rsp), %r11d
	movzbl	%cl, %esi
	incl	%esi
	cmpl	%r11d, %esi
	je	.L2296
	jge	.L2211
	incl	%ecx
	movb	%cl, 152(%rsp)
.L2211:
	cmpq	%r15, %r9
	sete	%dl
	cmpb	%cl, %bpl
	sete	%sil
	testb	%sil, %dl
	jne	.L2215
	movzbl	%cl, %r11d
	movl	128(%rsp,%r11,4), %r10d
	movl	%r10d, %eax
	andb	$127, %al
	cmpl	$2381, %eax
	je	.L2243
	leal	-2765(%r10), %edx
	andl	$-129, %edx
	je	.L2243
	movl	%r10d, %eax
	andb	$-2, %ah
	cmpl	$3149, %eax
	je	.L2243
	sall	$2, %r10d
	cmpl	$3073, %r10d
	ja	.L2297
.L2281:
	movq	48(%rsp), %r10
	movl	60(%rsp), %ebx
	movl	64(%rsp), %r11d
	movq	%r8, %rsi
	jmp	.L2197
.L2236:
	movq	%rdx, %rax
	jmp	.L2199
.L2237:
	movq	%rdx, %rax
	jmp	.L2202
.L2296:
	movq	160(%rsp), %r10
	cmpq	%r10, %r9
	je	.L2211
	movzbl	154(%rsp), %eax
	addq	%rax, %r9
	movq	%r9, 144(%rsp)
	cmpq	%r10, %r9
	je	.L2298
	vmovq	%xmm7, %rcx
	movq	%r8, 288(%rsp)
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	144(%rsp), %r9
	movzbl	152(%rsp), %ecx
	movq	288(%rsp), %r8
	jmp	.L2211
.L2243:
	movzbl	40(%rsp), %ebx
	jmp	.L2204
.L2298:
	xorl	%ecx, %ecx
	movb	$0, 152(%rsp)
	jmp	.L2211
.L2215:
	movl	%ebx, %r9d
	movq	48(%rsp), %r10
	movl	64(%rsp), %r11d
	movl	60(%rsp), %ebx
	movq	%r8, %rsi
	testb	%r9b, %r9b
	jne	.L2195
	jmp	.L2197
.L2297:
	orl	$3, %r10d
	xorl	%r11d, %r11d
	movl	$389, %eax
	leaq	_ZNSt9__unicode9__v15_1_012__incb_edgesE(%rip), %rsi
	movl	%r10d, 68(%rsp)
	movl	%r14d, 72(%rsp)
	movb	%cl, 79(%rsp)
.L2206:
	testq	%rax, %rax
	jle	.L2299
.L2207:
	movq	%rax, %rdx
	movl	68(%rsp), %ecx
	sarq	%rdx
	leaq	(%rsi,%rdx,4), %r14
	movl	(%r14), %r10d
	cmpl	%ecx, %r10d
	jnb	.L2238
	subq	%rdx, %rax
	leaq	4(%r14), %rsi
	movl	%r10d, %r11d
	decq	%rax
	testq	%rax, %rax
	jg	.L2207
.L2299:
	andl	$3, %r11d
	movl	72(%rsp), %r14d
	movzbl	79(%rsp), %ecx
	cmpl	$1, %r11d
	je	.L2239
	cmpl	$2, %r11d
	je	.L2204
	jmp	.L2281
	.p2align 4
	.p2align 3
.L2238:
	movq	%rdx, %rax
	jmp	.L2206
.L2239:
	xorl	%ebx, %ebx
	jmp	.L2204
	.seh_endproc
	.section	.text$_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
	.def	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE:
.LFB15543:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$312, %rsp
	.seh_stackalloc	312
	.seh_endprologue
	movq	8(%rdx), %rax
	movq	(%rdx), %rsi
	movq	%rcx, %rdi
	movq	%r8, %rbp
	movq	%rax, %r14
	testw	$1920, (%rcx)
	jne	.L2301
	movq	16(%r8), %rdi
	testq	%rsi, %rsi
	jne	.L2376
.L2302:
	movq	%rdi, %rax
.L2303:
	addq	$312, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L2301:
	movzbl	1(%rcx), %edx
	vpbroadcastq	%rax, %xmm0
	andl	$6, %edx
	je	.L2304
	cmpb	$2, %dl
	je	.L2377
	movq	$-1, %r13
	cmpb	$4, %dl
	je	.L2378
.L2306:
	testq	%rsi, %rsi
	je	.L2375
	leaq	(%r14,%rsi), %r12
	cmpq	%r12, %r14
	je	.L2320
	vmovdqu	%xmm0, 88(%rsp)
	leaq	80(%rsp), %rcx
	movw	$0, 104(%rsp)
	movb	$0, 106(%rsp)
	movq	%r12, 112(%rsp)
	vzeroupper
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	96(%rsp), %r9
	vmovq	%r14, %xmm2
	movzwl	104(%rsp), %r11d
	movq	%r12, 112(%rsp)
	movq	%r12, 272(%rsp)
	movq	%r12, 224(%rsp)
	vpinsrq	$1, %r9, %xmm2, %xmm4
	vmovdqu	%xmm4, 88(%rsp)
	vmovdqu	80(%rsp), %ymm3
	vmovdqu	%ymm3, 240(%rsp)
	vmovdqu	%ymm3, 192(%rsp)
	cmpq	%r9, %r12
	je	.L2322
	movzbl	%r11b, %r15d
	xorl	%ebx, %ebx
	movl	$1700, %eax
	leaq	_ZNSt9__unicode9__v15_1_011__gcb_edgesE(%rip), %r9
	movl	240(%rsp,%r15,4), %r15d
	movl	%r15d, %r10d
	sall	$4, %r10d
	orl	$15, %r10d
	.p2align 4
	.p2align 3
.L2325:
	testq	%rax, %rax
	jle	.L2379
.L2326:
	movq	%rax, %rdx
	sarq	%rdx
	leaq	(%r9,%rdx,4), %r8
	movl	(%r8), %ecx
	cmpl	%r10d, %ecx
	jnb	.L2358
	subq	%rdx, %rax
	leaq	4(%r8), %r9
	movl	%ecx, %ebx
	decq	%rax
	testq	%rax, %rax
	jg	.L2326
.L2379:
	andl	$15, %ebx
	jmp	.L2322
	.p2align 4
	.p2align 3
.L2376:
	leaq	64(%rsp), %rdx
	movq	%rdi, %rcx
	movq	%rsi, 64(%rsp)
	movq	%rax, 72(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L2302
	.p2align 4
	.p2align 3
.L2304:
	testq	%rsi, %rsi
	je	.L2365
	leaq	(%rax,%rsi), %r12
	cmpq	%r12, %rax
	je	.L2339
	leaq	128(%rsp), %rcx
	vmovdqu	%xmm0, 136(%rsp)
	movl	%r9d, 60(%rsp)
	movw	$0, 152(%rsp)
	movb	$0, 154(%rsp)
	movq	%r12, 160(%rsp)
	call	_ZNSt9__unicode13_Utf_iteratorIcDiPKcS2_NS_5_ReplEE12_M_read_utf8Ev
	movq	144(%rsp), %rcx
	vmovq	%r14, %xmm4
	movzwl	152(%rsp), %ebx
	movl	60(%rsp), %r9d
	movq	%r12, 160(%rsp)
	movq	%r12, 272(%rsp)
	movq	%r12, 224(%rsp)
	vpinsrq	$1, %rcx, %xmm4, %xmm3
	cmpq	%rcx, %r12
	vmovdqu	%xmm3, 136(%rsp)
	vmovdqu	128(%rsp), %ymm0
	vmovdqu	%ymm0, 240(%rsp)
	vmovdqu	%ymm0, 192(%rsp)
	je	.L2341
	movzbl	%bl, %r8d
	xorl	%r9d, %r9d
	movl	$1700, %edx
	leaq	_ZNSt9__unicode9__v15_1_011__gcb_edgesE(%rip), %r15
	movl	240(%rsp,%r8,4), %r13d
	movl	%r13d, %r10d
	sall	$4, %r10d
	orl	$15, %r10d
	.p2align 4
	.p2align 3
.L2344:
	testq	%rdx, %rdx
	jle	.L2380
.L2345:
	movq	%rdx, %rbx
	sarq	%rbx
	leaq	(%r15,%rbx,4), %r8
	movl	(%r8), %ecx
	cmpl	%r10d, %ecx
	jnb	.L2366
	subq	%rbx, %rdx
	leaq	4(%r8), %r15
	movl	%ecx, %r9d
	decq	%rdx
	testq	%rdx, %rdx
	jg	.L2345
.L2380:
	andl	$15, %r9d
.L2341:
	movl	%r9d, 180(%rsp)
	movl	%r13d, 176(%rsp)
	movw	$0, 184(%rsp)
	movb	$0, 186(%rsp)
	movq	%r12, 224(%rsp)
	vmovdqu	176(%rsp), %ymm5
	vmovdqu	200(%rsp), %ymm1
	leaq	_ZNSt9__unicode9__v15_1_013__width_edgesE(%rip), %r9
	movl	$1, %ebx
	movl	$200, %edx
	movq	%r9, %r10
	vmovdqu	%ymm5, 240(%rsp)
	vmovdqu	%ymm1, 264(%rsp)
	cmpl	$4351, %r13d
	ja	.L2348
.L2346:
	leaq	240(%rsp), %r13
	vzeroupper
	jmp	.L2350
	.p2align 4
	.p2align 3
.L2355:
	movl	240(%rsp), %r11d
	movl	$1, %r10d
	cmpl	$4351, %r11d
	ja	.L2381
.L2351:
	addq	%r10, %rbx
.L2350:
	movq	%r13, %rcx
	call	_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv
	cmpq	%r12, 32(%rax)
	jne	.L2355
.L2337:
	leaq	64(%rsp), %rcx
	movq	%rdi, %r9
	movq	%rbp, %r8
	movq	%rsi, 64(%rsp)
	movq	%rbx, %rdx
	movq	%r14, 72(%rsp)
	movl	$1, 32(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
	jmp	.L2303
	.p2align 4
	.p2align 3
.L2320:
	vmovq	%r14, %xmm0
	movl	$0, 80(%rsp)
	movw	$0, 104(%rsp)
	movb	$0, 106(%rsp)
	vpinsrq	$1, %r14, %xmm0, %xmm5
	movq	%r14, 112(%rsp)
	vmovdqu	%xmm5, 88(%rsp)
	vmovdqu	80(%rsp), %ymm1
	vmovdqu	%ymm1, 192(%rsp)
.L2322:
	movl	%ebx, 180(%rsp)
	movl	%r15d, 176(%rsp)
	movw	$0, 184(%rsp)
	movb	$0, 186(%rsp)
	movq	%r12, 224(%rsp)
	vmovdqu	176(%rsp), %ymm0
	vmovdqu	200(%rsp), %ymm5
	leaq	_ZNSt9__unicode9__v15_1_013__width_edgesE(%rip), %r9
	movl	$1, %ebx
	movl	$200, %r11d
	movq	%r9, %r10
	vmovdqu	%ymm0, 240(%rsp)
	vmovdqu	%ymm5, 264(%rsp)
	cmpl	$4351, %r15d
	ja	.L2329
.L2327:
	cmpq	%rbx, %r13
	jb	.L2361
	leaq	240(%rsp), %r15
	vzeroupper
	jmp	.L2331
	.p2align 4
	.p2align 3
.L2332:
	addq	%rbx, %r10
	cmpq	%r10, %r13
	jb	.L2382
	movq	%r10, %rbx
.L2331:
	movq	%r15, %rcx
	call	_ZNSt9__unicode9__v15_1_022_Grapheme_cluster_viewISt17basic_string_viewIcSt11char_traitsIcEEE9_IteratorppEv
	cmpq	%r12, 32(%rax)
	je	.L2337
	movl	240(%rsp), %edx
	movl	$1, %r10d
	cmpl	$4351, %edx
	jbe	.L2332
	leaq	_ZNSt9__unicode9__v15_1_013__width_edgesE(%rip), %r11
	movl	$200, %eax
	movq	%r11, %r9
	.p2align 4
	.p2align 3
.L2334:
	testq	%rax, %rax
	jle	.L2383
.L2335:
	movq	%rax, %rcx
	sarq	%rcx
	leaq	(%r9,%rcx,4), %r8
	cmpl	(%r8), %edx
	jb	.L2363
	subq	%rcx, %rax
	leaq	4(%r8), %r9
	decq	%rax
	testq	%rax, %rax
	jg	.L2335
.L2383:
	subq	%r11, %r9
	movq	%r9, %r10
	sarq	$2, %r10
	shrq	$63, %r9
	addq	%r9, %r10
	andl	$1, %r10d
	subq	%r9, %r10
	incl	%r10d
	jmp	.L2332
	.p2align 4
	.p2align 3
.L2360:
	movq	%rbx, %r11
.L2329:
	testq	%r11, %r11
	jle	.L2384
.L2330:
	movq	%r11, %rbx
	sarq	%rbx
	leaq	(%r10,%rbx,4), %rdx
	cmpl	(%rdx), %r15d
	jb	.L2360
	subq	%rbx, %r11
	leaq	4(%rdx), %r10
	decq	%r11
	testq	%r11, %r11
	jg	.L2330
.L2384:
	subq	%r9, %r10
	movq	%r10, %rbx
	sarq	$2, %rbx
	shrq	$63, %r10
	addq	%r10, %rbx
	andl	$1, %ebx
	subq	%r10, %rbx
	leal	1(%rbx), %ebx
	jmp	.L2327
	.p2align 4
	.p2align 3
.L2378:
	movzbl	(%r8), %eax
	movzwl	6(%rcx), %r12d
	movl	%eax, %ecx
	andl	$15, %eax
	andl	$15, %ecx
	cmpq	%rax, %r12
	jnb	.L2307
	leaq	(%r12,%r12,4), %rdx
	movl	$4, %r10d
	shrx	%r10, (%r8), %r8
	salq	$4, %r12
	shrx	%rdx, %r8, %r11
	addq	8(%rbp), %r12
	andl	$31, %r11d
	vmovdqa	(%r12), %xmm4
	vmovdqa	%xmm4, 240(%rsp)
.L2308:
	leaq	.L2312(%rip), %rcx
	movzbl	%r11b, %r12d
	movb	%r11b, 256(%rsp)
	vmovdqu	240(%rsp), %ymm3
	movslq	(%rcx,%r12,4), %rax
	addq	%rcx, %rax
	vmovdqu	%ymm3, 176(%rsp)
	jmp	*%rax
	.section .rdata,"dr"
	.align 4
.L2312:
	.long	.L2374-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2316-.L2312
	.long	.L2315-.L2312
	.long	.L2314-.L2312
	.long	.L2313-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.long	.L2317-.L2312
	.section	.text$_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L2368:
	movq	%rbx, %rdx
.L2348:
	testq	%rdx, %rdx
	jle	.L2385
.L2349:
	movq	%rdx, %rbx
	sarq	%rbx
	leaq	(%r10,%rbx,4), %r11
	cmpl	(%r11), %r13d
	jb	.L2368
	subq	%rbx, %rdx
	leaq	4(%r11), %r10
	decq	%rdx
	testq	%rdx, %rdx
	jg	.L2349
.L2385:
	subq	%r9, %r10
	movq	%r10, %r9
	sarq	$2, %r9
	shrq	$63, %r10
	addq	%r10, %r9
	andl	$1, %r9d
	subq	%r10, %r9
	leal	1(%r9), %ebx
	jmp	.L2346
	.p2align 4
	.p2align 3
.L2363:
	movq	%rcx, %rax
	jmp	.L2334
	.p2align 4
	.p2align 3
.L2358:
	movq	%rdx, %rax
	jmp	.L2325
	.p2align 4
	.p2align 3
.L2366:
	movq	%rbx, %rdx
	jmp	.L2344
	.p2align 4
	.p2align 3
.L2361:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
.L2375:
	xorl	%ebx, %ebx
	vzeroupper
	jmp	.L2337
	.p2align 4
	.p2align 3
.L2381:
	leaq	_ZNSt9__unicode9__v15_1_013__width_edgesE(%rip), %rdx
	movl	$200, %eax
	movq	%rdx, %r15
	.p2align 4
	.p2align 3
.L2353:
	testq	%rax, %rax
	jle	.L2386
.L2354:
	movq	%rax, %rcx
	sarq	%rcx
	leaq	(%r15,%rcx,4), %r8
	cmpl	(%r8), %r11d
	jb	.L2370
	subq	%rcx, %rax
	leaq	4(%r8), %r15
	decq	%rax
	testq	%rax, %rax
	jg	.L2354
.L2386:
	subq	%rdx, %r15
	movq	%r15, %r10
	sarq	$2, %r10
	shrq	$63, %r15
	addq	%r15, %r10
	andl	$1, %r10d
	subq	%r15, %r10
	incl	%r10d
	jmp	.L2351
	.p2align 4
	.p2align 3
.L2370:
	movq	%rcx, %rax
	jmp	.L2353
	.p2align 4
	.p2align 3
.L2377:
	movzwl	6(%rcx), %r13d
	jmp	.L2306
	.p2align 4
	.p2align 3
.L2382:
	movq	272(%rsp), %rsi
	subq	%r14, %rsi
	jmp	.L2337
	.p2align 4
	.p2align 3
.L2339:
	vmovq	%rax, %xmm5
	movl	$0, 128(%rsp)
	movw	$0, 152(%rsp)
	movb	$0, 154(%rsp)
	vpinsrq	$1, %rax, %xmm5, %xmm1
	movq	%rax, 160(%rsp)
	vmovdqu	%xmm1, 136(%rsp)
	vmovdqu	128(%rsp), %ymm2
	vmovdqu	%ymm2, 192(%rsp)
	jmp	.L2341
	.p2align 4
	.p2align 3
.L2307:
	testb	%cl, %cl
	je	.L2387
.L2309:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L2314:
	movq	176(%rsp), %r13
	testq	%r13, %r13
	jns	.L2306
.L2317:
	leaq	.LC6(%rip), %rcx
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L2315:
	movl	176(%rsp), %r13d
	jmp	.L2306
.L2316:
	movslq	176(%rsp), %r13
	testl	%r13d, %r13d
	jns	.L2306
	jmp	.L2317
	.p2align 4
	.p2align 3
.L2313:
	movq	176(%rsp), %r13
	jmp	.L2306
.L2374:
	vzeroupper
	jmp	.L2309
.L2387:
	movl	$4, %r13d
	shrx	%r13, (%r8), %r9
	cmpq	%r9, %r12
	jnb	.L2309
	salq	$5, %r12
	addq	8(%r8), %r12
	vmovdqu	(%r12), %xmm2
	movzbl	16(%r12), %r11d
	vmovdqa	%xmm2, 240(%rsp)
	jmp	.L2308
.L2365:
	xorl	%ebx, %ebx
	jmp	.L2337
	.seh_endproc
	.section	.text$_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_
	.def	_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_
_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_:
.LFB15753:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$24, %rsp
	.seh_stackalloc	24
	.seh_endprologue
	movq	%rcx, %rax
	movsbq	(%r8), %rcx
	movq	%r8, %rbx
	movl	%edx, %r11d
	movq	128(%rsp), %r10
	movq	136(%rsp), %rdx
	leal	-1(%rcx), %r8d
	cmpb	$125, %r8b
	ja	.L2389
	movq	%rdx, %rsi
	subq	%r10, %rsi
	cmpq	%rcx, %rsi
	jle	.L2389
	leaq	-1(%r9), %rbp
	xorl	%r12d, %r12d
	movq	%rbp, %rdi
	andl	$3, %edi
	je	.L2391
	subq	%rcx, %rdx
	cmpq	%rbp, %r12
	jnb	.L2390
	movsbq	1(%rbx), %rcx
	leaq	1(%rbx), %rsi
	movl	$1, %r12d
	leal	-1(%rcx), %r9d
	cmpb	$125, %r9b
	ja	.L2441
	movq	%rdx, %r13
	subq	%r10, %r13
	cmpq	%rcx, %r13
	jle	.L2441
	cmpq	$1, %rdi
	je	.L2391
	cmpq	$2, %rdi
	je	.L2683
	leaq	2(%rbx), %rsi
	subq	%rcx, %rdx
	movl	$2, %r12d
	movsbq	(%rsi), %rcx
	leal	-1(%rcx), %r14d
	cmpb	$125, %r14b
	ja	.L2441
	movq	%rdx, %r15
	subq	%r10, %r15
	cmpq	%rcx, %r15
	jle	.L2441
.L2683:
	incq	%r12
	subq	%rcx, %rdx
	leaq	(%rbx,%r12), %rsi
	movsbq	(%rsi), %rcx
	leal	-1(%rcx), %r8d
	cmpb	$125, %r8b
	ja	.L2441
.L2782:
	movq	%rdx, %rdi
	subq	%r10, %rdi
	cmpq	%rcx, %rdi
	jle	.L2441
.L2391:
	subq	%rcx, %rdx
	cmpq	%rbp, %r12
	jnb	.L2390
	leaq	1(%r12), %rcx
	leaq	(%rbx,%rcx), %rsi
	movq	%rcx, %r12
	movsbq	(%rsi), %r9
	leal	-1(%r9), %r13d
	cmpb	$125, %r13b
	ja	.L2441
	movq	%rdx, %r14
	subq	%r10, %r14
	cmpq	%r9, %r14
	jle	.L2441
	incq	%r12
	subq	%r9, %rdx
	leaq	(%rbx,%r12), %rsi
	movsbq	(%rsi), %r15
	leal	-1(%r15), %r8d
	cmpb	$125, %r8b
	ja	.L2441
	movq	%rdx, %rdi
	subq	%r10, %rdi
	cmpq	%r15, %rdi
	jle	.L2441
	leaq	2(%rbx,%rcx), %rsi
	subq	%r15, %rdx
	leaq	2(%rcx), %r12
	movsbq	(%rsi), %r9
	leal	-1(%r9), %r13d
	cmpb	$125, %r13b
	ja	.L2441
	movq	%rdx, %r14
	subq	%r10, %r14
	cmpq	%r9, %r14
	jle	.L2441
	leaq	3(%rbx,%rcx), %rsi
	leaq	3(%rcx), %r12
	subq	%r9, %rdx
	movsbq	(%rsi), %rcx
	leal	-1(%rcx), %r15d
	cmpb	$125, %r15b
	jbe	.L2782
	.p2align 4
	.p2align 3
.L2441:
	xorl	%r8d, %r8d
	jmp	.L2393
.L2390:
	movq	%rdx, %rsi
	movl	$1, %r8d
	subq	%r10, %rsi
	cmpq	%rcx, %rsi
	jle	.L2783
.L2394:
	subq	%rcx, %rdx
	incq	%r8
	movq	%rdx, %rbp
	subq	%r10, %rbp
	cmpq	%rbp, %rcx
	jl	.L2394
.L2783:
	leaq	(%rbx,%r12), %rsi
.L2393:
	leaq	-1(%r8), %rbp
	leaq	-1(%r12), %rcx
	movq	%rbp, 8(%rsp)
	cmpq	%rdx, %r10
	je	.L2397
.L2395:
	movq	%rdx, %r15
	subq	%r10, %r15
	leaq	-1(%r15), %rdi
	cmpq	$30, %rdi
	jbe	.L2435
	leaq	1(%r10), %r9
	movq	%rax, %r13
	subq	%r9, %r13
	cmpq	$62, %r13
	jbe	.L2435
	cmpq	$62, %rdi
	jbe	.L2436
	movq	%r15, %r13
	xorl	%r14d, %r14d
	andq	$-64, %r13
	leaq	-64(%r13), %r9
	shrq	$6, %r9
	incq	%r9
	andl	$7, %r9d
	je	.L2773
	cmpq	$1, %r9
	je	.L2638
	cmpq	$2, %r9
	je	.L2639
	cmpq	$3, %r9
	je	.L2640
	cmpq	$4, %r9
	je	.L2641
	cmpq	$5, %r9
	je	.L2642
	cmpq	$6, %r9
	je	.L2643
	vmovdqu8	(%r10), %zmm0
	movl	$64, %r14d
	vmovdqu8	%zmm0, (%rax)
.L2643:
	vmovdqu8	(%r10,%r14), %zmm1
	vmovdqu8	%zmm1, (%rax,%r14)
	addq	$64, %r14
.L2642:
	vmovdqu8	(%r10,%r14), %zmm2
	vmovdqu8	%zmm2, (%rax,%r14)
	addq	$64, %r14
.L2641:
	vmovdqu8	(%r10,%r14), %zmm3
	vmovdqu8	%zmm3, (%rax,%r14)
	addq	$64, %r14
.L2640:
	vmovdqu8	(%r10,%r14), %zmm4
	vmovdqu8	%zmm4, (%rax,%r14)
	addq	$64, %r14
.L2639:
	vmovdqu8	(%r10,%r14), %zmm5
	vmovdqu8	%zmm5, (%rax,%r14)
	addq	$64, %r14
.L2638:
	vmovdqu8	(%r10,%r14), %zmm0
	vmovdqu8	%zmm0, (%rax,%r14)
	addq	$64, %r14
	cmpq	%r13, %r14
	je	.L2764
.L2773:
	movq	8(%rsp), %rdi
.L2400:
	vmovdqu8	(%r10,%r14), %zmm1
	vmovdqu8	%zmm1, (%rax,%r14)
	vmovdqu8	64(%r10,%r14), %zmm2
	vmovdqu8	%zmm2, 64(%rax,%r14)
	vmovdqu8	128(%r10,%r14), %zmm3
	vmovdqu8	%zmm3, 128(%rax,%r14)
	vmovdqu8	192(%r10,%r14), %zmm4
	vmovdqu8	%zmm4, 192(%rax,%r14)
	vmovdqu8	256(%r10,%r14), %zmm5
	vmovdqu8	%zmm5, 256(%rax,%r14)
	vmovdqu8	320(%r10,%r14), %zmm0
	vmovdqu8	%zmm0, 320(%rax,%r14)
	vmovdqu8	384(%r10,%r14), %zmm1
	vmovdqu8	%zmm1, 384(%rax,%r14)
	vmovdqu8	448(%r10,%r14), %zmm2
	vmovdqu8	%zmm2, 448(%rax,%r14)
	addq	$512, %r14
	cmpq	%r13, %r14
	jne	.L2400
	movq	%rdi, 8(%rsp)
.L2764:
	leaq	(%r10,%r13), %rdi
	leaq	(%rax,%r13), %rbp
	cmpq	%r13, %r15
	je	.L2407
	movq	%r15, %r14
	subq	%r13, %r14
	leaq	-1(%r14), %r9
	cmpq	$30, %r9
	jbe	.L2403
.L2399:
	vmovdqu8	(%r10,%r13), %ymm3
	movq	%r14, %r10
	andq	$-32, %r10
	addq	%r10, %rdi
	addq	%r10, %rbp
	andl	$31, %r14d
	vmovdqu8	%ymm3, (%rax,%r13)
	je	.L2407
.L2403:
	movq	%rdx, %r14
	xorl	%r13d, %r13d
	subq	%rdi, %r14
	movq	%r14, %r9
	andl	$7, %r9d
	je	.L2772
	cmpq	$1, %r9
	je	.L2644
	cmpq	$2, %r9
	je	.L2645
	cmpq	$3, %r9
	je	.L2646
	cmpq	$4, %r9
	je	.L2647
	cmpq	$5, %r9
	je	.L2648
	cmpq	$6, %r9
	je	.L2649
	movzbl	(%rdi), %r13d
	movb	%r13b, 0(%rbp)
	movl	$1, %r13d
.L2649:
	movzbl	(%rdi,%r13), %r10d
	movb	%r10b, 0(%rbp,%r13)
	incq	%r13
.L2648:
	movzbl	(%rdi,%r13), %r9d
	movb	%r9b, 0(%rbp,%r13)
	incq	%r13
.L2647:
	movzbl	(%rdi,%r13), %r10d
	movb	%r10b, 0(%rbp,%r13)
	incq	%r13
.L2646:
	movzbl	(%rdi,%r13), %r9d
	movb	%r9b, 0(%rbp,%r13)
	incq	%r13
.L2645:
	movzbl	(%rdi,%r13), %r10d
	movb	%r10b, 0(%rbp,%r13)
	incq	%r13
.L2644:
	movzbl	(%rdi,%r13), %r9d
	movb	%r9b, 0(%rbp,%r13)
	incq	%r13
	cmpq	%r14, %r13
	je	.L2407
.L2772:
	movq	8(%rsp), %r9
	movq	%rax, 96(%rsp)
.L2405:
	movzbl	(%rdi,%r13), %eax
	movb	%al, 0(%rbp,%r13)
	movzbl	1(%rdi,%r13), %r10d
	movb	%r10b, 1(%rbp,%r13)
	movzbl	2(%rdi,%r13), %eax
	movb	%al, 2(%rbp,%r13)
	movzbl	3(%rdi,%r13), %r10d
	movb	%r10b, 3(%rbp,%r13)
	movzbl	4(%rdi,%r13), %eax
	movb	%al, 4(%rbp,%r13)
	movzbl	5(%rdi,%r13), %r10d
	movb	%r10b, 5(%rbp,%r13)
	movzbl	6(%rdi,%r13), %eax
	movb	%al, 6(%rbp,%r13)
	movzbl	7(%rdi,%r13), %r10d
	movb	%r10b, 7(%rbp,%r13)
	addq	$8, %r13
	cmpq	%r14, %r13
	jne	.L2405
	movq	96(%rsp), %rax
	movq	%r9, 8(%rsp)
.L2407:
	addq	%r15, %rax
.L2397:
	testq	%r8, %r8
	je	.L2408
	movq	8(%rsp), %r9
	.p2align 4
	.p2align 3
.L2419:
	movb	%r11b, (%rax)
	movsbq	(%rsi), %rdi
	leaq	1(%rax), %r13
	testb	%dil, %dil
	jle	.L2437
	leal	-1(%rdi), %r8d
	cmpb	$30, %r8b
	jle	.L2410
	movq	%rax, %r15
	subq	%rdx, %r15
	cmpq	$62, %r15
	jbe	.L2410
	cmpb	$62, %r8b
	jle	.L2438
	vmovdqu8	(%rdx), %zmm4
	leal	-64(%rdi), %ebp
	leaq	64(%rdx), %r10
	leaq	65(%rax), %r8
	vmovdqu8	%zmm4, 1(%rax)
	cmpb	$64, %dil
	je	.L2418
	leal	-65(%rdi), %r14d
	movl	%ebp, %r15d
	cmpb	$30, %r14b
	jbe	.L2414
	movl	$64, %r14d
.L2411:
	vmovdqu8	(%rdx,%r14), %ymm5
	subl	$32, %ebp
	addq	$32, %r10
	addq	$32, %r8
	vmovdqu8	%ymm5, 1(%rax,%r14)
	cmpb	$32, %r15b
	je	.L2418
.L2414:
	movzbl	(%r10), %eax
	leal	-1(%rbp), %r15d
	movl	%r15d, %r14d
	andl	$7, %r14d
	movb	%al, (%r8)
	movl	$1, %eax
	testb	%r15b, %r15b
	jle	.L2418
	testb	%r14b, %r14b
	je	.L2416
	cmpb	$1, %r14b
	je	.L2676
	cmpb	$2, %r14b
	je	.L2677
	cmpb	$3, %r14b
	je	.L2678
	cmpb	$4, %r14b
	je	.L2679
	cmpb	$5, %r14b
	je	.L2680
	cmpb	$6, %r14b
	je	.L2681
	movzbl	1(%r10), %r15d
	movl	$2, %eax
	movb	%r15b, 1(%r8)
.L2681:
	movzbl	(%r10,%rax), %r14d
	movb	%r14b, (%r8,%rax)
	incq	%rax
.L2680:
	movzbl	(%r10,%rax), %r15d
	movb	%r15b, (%r8,%rax)
	incq	%rax
.L2679:
	movzbl	(%r10,%rax), %r14d
	movb	%r14b, (%r8,%rax)
	incq	%rax
.L2678:
	movzbl	(%r10,%rax), %r15d
	movb	%r15b, (%r8,%rax)
	incq	%rax
.L2677:
	movzbl	(%r10,%rax), %r14d
	movb	%r14b, (%r8,%rax)
	incq	%rax
.L2676:
	movzbl	(%r10,%rax), %r15d
	movl	%ebp, %r14d
	movb	%r15b, (%r8,%rax)
	incq	%rax
	subl	%eax, %r14d
	testb	%r14b, %r14b
	jle	.L2418
.L2416:
	movzbl	(%r10,%rax), %r15d
	movb	%r15b, (%r8,%rax)
	movzbl	1(%r10,%rax), %r14d
	movb	%r14b, 1(%r8,%rax)
	movzbl	2(%r10,%rax), %r15d
	movb	%r15b, 2(%r8,%rax)
	movzbl	3(%r10,%rax), %r14d
	movb	%r14b, 3(%r8,%rax)
	movzbl	4(%r10,%rax), %r15d
	movb	%r15b, 4(%r8,%rax)
	movzbl	5(%r10,%rax), %r14d
	movb	%r14b, 5(%r8,%rax)
	movzbl	6(%r10,%rax), %r15d
	movb	%r15b, 6(%r8,%rax)
	movzbl	7(%r10,%rax), %r14d
	movl	%ebp, %r15d
	movb	%r14b, 7(%r8,%rax)
	addq	$8, %rax
	subl	%eax, %r15d
	testb	%r15b, %r15b
	jg	.L2416
.L2418:
	leaq	0(%r13,%rdi), %rax
	addq	%rdi, %rdx
.L2409:
	subq	$1, %r9
	jnb	.L2419
.L2408:
	testq	%r12, %r12
	je	.L2781
	.p2align 4
	.p2align 3
.L2430:
	movb	%r11b, (%rax)
	movzbl	(%rbx,%rcx), %r13d
	leaq	1(%rax), %rbp
	testb	%r13b, %r13b
	jle	.L2439
	leal	-1(%r13), %esi
	cmpb	$30, %sil
	jle	.L2421
	movq	%rax, %r12
	subq	%rdx, %r12
	cmpq	$62, %r12
	jbe	.L2421
	movl	%r13d, %edi
	cmpb	$62, %sil
	jle	.L2440
	vmovdqu8	(%rdx), %zmm0
	subl	$64, %r13d
	leaq	64(%rdx), %r9
	leaq	65(%rax), %r8
	vmovdqu8	%zmm0, 1(%rax)
	cmpb	$64, %dil
	je	.L2429
	leal	-64(%rdi), %r15d
	subl	$65, %edi
	cmpb	$30, %dil
	jbe	.L2425
	movl	$64, %r14d
.L2422:
	vmovdqu8	(%rdx,%r14), %ymm1
	subl	$32, %r13d
	addq	$32, %r9
	addq	$32, %r8
	vmovdqu8	%ymm1, 1(%rax,%r14)
	cmpb	$32, %r15b
	je	.L2429
.L2425:
	movzbl	(%r9), %eax
	leal	-1(%r13), %r10d
	movl	$1, %r15d
	movl	%r10d, %r12d
	andl	$7, %r12d
	movb	%al, (%r8)
	testb	%r10b, %r10b
	jle	.L2429
	testb	%r12b, %r12b
	je	.L2427
	cmpb	$1, %r12b
	je	.L2670
	cmpb	$2, %r12b
	je	.L2671
	cmpb	$3, %r12b
	je	.L2672
	cmpb	$4, %r12b
	je	.L2673
	cmpb	$5, %r12b
	je	.L2674
	cmpb	$6, %r12b
	je	.L2675
	movzbl	1(%r9), %edi
	movl	$2, %r15d
	movb	%dil, 1(%r8)
.L2675:
	movzbl	(%r9,%r15), %r14d
	movb	%r14b, (%r8,%r15)
	incq	%r15
.L2674:
	movzbl	(%r9,%r15), %r10d
	movb	%r10b, (%r8,%r15)
	incq	%r15
.L2673:
	movzbl	(%r9,%r15), %r12d
	movb	%r12b, (%r8,%r15)
	incq	%r15
.L2672:
	movzbl	(%r9,%r15), %eax
	movb	%al, (%r8,%r15)
	incq	%r15
.L2671:
	movzbl	(%r9,%r15), %edi
	movb	%dil, (%r8,%r15)
	incq	%r15
.L2670:
	movzbl	(%r9,%r15), %r14d
	movl	%r13d, %r10d
	movb	%r14b, (%r8,%r15)
	incq	%r15
	subl	%r15d, %r10d
	testb	%r10b, %r10b
	jle	.L2429
.L2427:
	movzbl	(%r9,%r15), %r12d
	movb	%r12b, (%r8,%r15)
	movzbl	1(%r9,%r15), %eax
	movb	%al, 1(%r8,%r15)
	movzbl	2(%r9,%r15), %edi
	movb	%dil, 2(%r8,%r15)
	movzbl	3(%r9,%r15), %r14d
	movb	%r14b, 3(%r8,%r15)
	movzbl	4(%r9,%r15), %r10d
	movl	%r13d, %r14d
	movb	%r10b, 4(%r8,%r15)
	movzbl	5(%r9,%r15), %r12d
	movb	%r12b, 5(%r8,%r15)
	movzbl	6(%r9,%r15), %eax
	movb	%al, 6(%r8,%r15)
	movzbl	7(%r9,%r15), %edi
	movb	%dil, 7(%r8,%r15)
	addq	$8, %r15
	subl	%r15d, %r14d
	testb	%r14b, %r14b
	jg	.L2427
.L2429:
	movsbq	%sil, %rsi
	leaq	1(%rbp,%rsi), %rax
	leaq	1(%rdx,%rsi), %rdx
.L2420:
	subq	$1, %rcx
	jnb	.L2430
.L2781:
	vzeroupper
.L2388:
	addq	$24, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L2421:
	movzbl	%r13b, %r15d
	xorl	%r8d, %r8d
	andl	$7, %r13d
	je	.L2428
	cmpq	$1, %r13
	je	.L2664
	cmpq	$2, %r13
	je	.L2665
	cmpq	$3, %r13
	je	.L2666
	cmpq	$4, %r13
	je	.L2667
	cmpq	$5, %r13
	je	.L2668
	cmpq	$6, %r13
	je	.L2669
	movzbl	(%rdx), %r13d
	movl	$1, %r8d
	movb	%r13b, 1(%rax)
.L2669:
	movzbl	(%rdx,%r8), %r9d
	movb	%r9b, 1(%rax,%r8)
	incq	%r8
.L2668:
	movzbl	(%rdx,%r8), %r10d
	movb	%r10b, 1(%rax,%r8)
	incq	%r8
.L2667:
	movzbl	(%rdx,%r8), %r12d
	movb	%r12b, 1(%rax,%r8)
	incq	%r8
.L2666:
	movzbl	(%rdx,%r8), %edi
	movb	%dil, 1(%rax,%r8)
	incq	%r8
.L2665:
	movzbl	(%rdx,%r8), %r14d
	movb	%r14b, 1(%rax,%r8)
	incq	%r8
.L2664:
	movzbl	(%rdx,%r8), %r13d
	movb	%r13b, 1(%rax,%r8)
	incq	%r8
	cmpq	%r8, %r15
	je	.L2429
.L2428:
	movzbl	(%rdx,%r8), %r9d
	movb	%r9b, 1(%rax,%r8)
	movzbl	1(%rdx,%r8), %r10d
	movb	%r10b, 2(%rax,%r8)
	movzbl	2(%rdx,%r8), %r12d
	movb	%r12b, 3(%rax,%r8)
	movzbl	3(%rdx,%r8), %edi
	movb	%dil, 4(%rax,%r8)
	movzbl	4(%rdx,%r8), %r14d
	movb	%r14b, 5(%rax,%r8)
	movzbl	5(%rdx,%r8), %r13d
	movb	%r13b, 6(%rax,%r8)
	movzbl	6(%rdx,%r8), %r9d
	movb	%r9b, 7(%rax,%r8)
	movzbl	7(%rdx,%r8), %r10d
	movb	%r10b, 8(%rax,%r8)
	addq	$8, %r8
	cmpq	%r8, %r15
	jne	.L2428
	jmp	.L2429
	.p2align 4
	.p2align 3
.L2410:
	movq	%rdi, %r10
	xorl	%r15d, %r15d
	movzbl	%dil, %ebp
	andl	$7, %r10d
	je	.L2417
	cmpq	$1, %r10
	je	.L2657
	cmpq	$2, %r10
	je	.L2658
	cmpq	$3, %r10
	je	.L2659
	cmpq	$4, %r10
	je	.L2660
	cmpq	$5, %r10
	je	.L2661
	cmpq	$6, %r10
	je	.L2662
	movzbl	(%rdx), %r8d
	movl	$1, %r15d
	movb	%r8b, 1(%rax)
.L2662:
	movzbl	(%rdx,%r15), %r14d
	movb	%r14b, 1(%rax,%r15)
	incq	%r15
.L2661:
	movzbl	(%rdx,%r15), %r10d
	movb	%r10b, 1(%rax,%r15)
	incq	%r15
.L2660:
	movzbl	(%rdx,%r15), %r8d
	movb	%r8b, 1(%rax,%r15)
	incq	%r15
.L2659:
	movzbl	(%rdx,%r15), %r14d
	movb	%r14b, 1(%rax,%r15)
	incq	%r15
.L2658:
	movzbl	(%rdx,%r15), %r10d
	movb	%r10b, 1(%rax,%r15)
	incq	%r15
.L2657:
	movzbl	(%rdx,%r15), %r8d
	movb	%r8b, 1(%rax,%r15)
	incq	%r15
	cmpq	%r15, %rbp
	je	.L2418
.L2417:
	movzbl	(%rdx,%r15), %r14d
	movb	%r14b, 1(%rax,%r15)
	movzbl	1(%rdx,%r15), %r10d
	movb	%r10b, 2(%rax,%r15)
	movzbl	2(%rdx,%r15), %r8d
	movb	%r8b, 3(%rax,%r15)
	movzbl	3(%rdx,%r15), %r14d
	movb	%r14b, 4(%rax,%r15)
	movzbl	4(%rdx,%r15), %r10d
	movb	%r10b, 5(%rax,%r15)
	movzbl	5(%rdx,%r15), %r8d
	movb	%r8b, 6(%rax,%r15)
	movzbl	6(%rdx,%r15), %r14d
	movb	%r14b, 7(%rax,%r15)
	movzbl	7(%rdx,%r15), %r10d
	movb	%r10b, 8(%rax,%r15)
	addq	$8, %r15
	cmpq	%r15, %rbp
	jne	.L2417
	jmp	.L2418
	.p2align 4
	.p2align 3
.L2437:
	movq	%r13, %rax
	jmp	.L2409
	.p2align 4
	.p2align 3
.L2439:
	movq	%rbp, %rax
	jmp	.L2420
.L2440:
	movl	%r13d, %r15d
	movq	%rdx, %r9
	movq	%rbp, %r8
	xorl	%r14d, %r14d
	jmp	.L2422
.L2438:
	movl	%edi, %r15d
	movl	%edi, %ebp
	movq	%rdx, %r10
	movq	%r13, %r8
	xorl	%r14d, %r14d
	jmp	.L2411
.L2435:
	movq	%r15, %rbp
	xorl	%r14d, %r14d
	andl	$7, %ebp
	je	.L2771
	cmpq	$1, %rbp
	je	.L2650
	cmpq	$2, %rbp
	je	.L2651
	cmpq	$3, %rbp
	je	.L2652
	cmpq	$4, %rbp
	je	.L2653
	cmpq	$5, %rbp
	je	.L2654
	cmpq	$6, %rbp
	je	.L2655
	movzbl	(%r10), %edi
	movl	$1, %r14d
	movb	%dil, (%rax)
.L2655:
	movzbl	(%r10,%r14), %r13d
	movb	%r13b, (%rax,%r14)
	incq	%r14
.L2654:
	movzbl	(%r10,%r14), %r9d
	movb	%r9b, (%rax,%r14)
	incq	%r14
.L2653:
	movzbl	(%r10,%r14), %ebp
	movb	%bpl, (%rax,%r14)
	incq	%r14
.L2652:
	movzbl	(%r10,%r14), %edi
	movb	%dil, (%rax,%r14)
	incq	%r14
.L2651:
	movzbl	(%r10,%r14), %r13d
	movb	%r13b, (%rax,%r14)
	incq	%r14
.L2650:
	movzbl	(%r10,%r14), %r9d
	movb	%r9b, (%rax,%r14)
	incq	%r14
	cmpq	%r14, %r15
	je	.L2407
.L2771:
	movq	8(%rsp), %rdi
.L2406:
	movzbl	(%r10,%r14), %ebp
	movb	%bpl, (%rax,%r14)
	movzbl	1(%r10,%r14), %r13d
	movb	%r13b, 1(%rax,%r14)
	movzbl	2(%r10,%r14), %r9d
	movb	%r9b, 2(%rax,%r14)
	movzbl	3(%r10,%r14), %ebp
	movb	%bpl, 3(%rax,%r14)
	movzbl	4(%r10,%r14), %r13d
	movb	%r13b, 4(%rax,%r14)
	movzbl	5(%r10,%r14), %r9d
	movb	%r9b, 5(%rax,%r14)
	movzbl	6(%r10,%r14), %ebp
	movb	%bpl, 6(%rax,%r14)
	movzbl	7(%r10,%r14), %r13d
	movb	%r13b, 7(%rax,%r14)
	addq	$8, %r14
	cmpq	%r14, %r15
	jne	.L2406
	movq	%rdi, 8(%rsp)
	jmp	.L2407
.L2389:
	cmpq	%r10, %rdx
	je	.L2388
	movq	%rbx, %rsi
	movq	$-1, %rcx
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	movq	$-1, 8(%rsp)
	jmp	.L2395
.L2436:
	movq	%r15, %r14
	movq	%r10, %rdi
	movq	%rax, %rbp
	xorl	%r13d, %r13d
	jmp	.L2399
	.seh_endproc
	.section	.text$_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	.def	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_:
.LFB15640:
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$200, %rsp
	.seh_stackalloc	200
	leaq	176(%rsp), %rbp
	.seh_setframe	%rbp, 176
	vmovaps	%xmm6, 0(%rbp)
	.seh_savexmm	%xmm6, 176
	.seh_endprologue
	movzwl	(%rcx), %eax
	movq	(%rdx), %r12
	movq	8(%rdx), %r14
	movq	%rcx, %rsi
	movq	%r9, %rbx
	movq	%r8, 112(%rbp)
	andw	$384, %ax
	movq	%r12, %r15
	movq	%r14, %r13
	cmpw	$128, %ax
	je	.L2850
	cmpw	$256, %ax
	je	.L2787
	testb	$32, (%rcx)
	jne	.L2851
.L2789:
	movq	16(%rbx), %r12
	testq	%r15, %r15
	jne	.L2852
.L2817:
	movq	%r12, %rax
.L2846:
	vmovaps	0(%rbp), %xmm6
	leaq	24(%rbp), %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4
	.p2align 3
.L2850:
	movzwl	4(%rcx), %edi
.L2786:
	testb	$32, (%rsi)
	jne	.L2788
.L2802:
	cmpq	%rdi, %r15
	jnb	.L2789
	movq	16(%rbx), %rcx
	movzbl	(%rsi), %ebx
	subq	%r15, %rdi
	movq	%rdi, %r9
	movl	%ebx, %r8d
	andl	$3, %r8d
	je	.L2819
	movl	8(%rsi), %esi
	leaq	-80(%rbp), %rdx
.L2820:
	movq	%r15, -80(%rbp)
	movq	%r13, -72(%rbp)
	movl	%esi, 32(%rsp)
.LEHB41:
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
.LEHE41:
	jmp	.L2846
	.p2align 4
	.p2align 3
.L2851:
	xorl	%edi, %edi
.L2788:
	cmpb	$0, 32(%rbx)
	leaq	24(%rbx), %rdx
	je	.L2853
.L2803:
	leaq	-64(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	call	_ZNSt6localeC1ERKS_
	movq	-88(%rbp), %rdx
	leaq	-32(%rbp), %rcx
	movq	%rcx, -96(%rbp)
.LEHB42:
	call	_ZNKSt6locale4nameB5cxx11Ev
	cmpq	$1, -24(%rbp)
	movq	-32(%rbp), %rcx
	leaq	-16(%rbp), %r10
	je	.L2854
.L2805:
	vmovq	%r10, %xmm6
	cmpq	%r10, %rcx
	je	.L2808
	movq	-16(%rbp), %r11
	leaq	1(%r11), %rdx
	call	_ZdlPvy
.L2808:
	movq	.refptr._ZNSt7__cxx118numpunctIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	%rax, %r8
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdx
	movq	(%rdx,%r8,8), %r9
	testq	%r9, %r9
	je	.L2811
	movq	(%r9), %r10
	movq	-96(%rbp), %rcx
	movq	%r9, %rdx
	movq	%r9, -104(%rbp)
	call	*32(%r10)
.LEHE42:
	movq	-24(%rbp), %r8
	movq	-104(%rbp), %r11
	movq	-32(%rbp), %rcx
	testq	%r8, %r8
	je	.L2813
	movq	112(%rbp), %r15
	movq	%r12, %r13
	subq	112(%rbp), %r13
	leaq	63(%r15,%r13,2), %rax
	andq	$-16, %rax
	call	___chkstk_ms
	subq	%rax, %rsp
	leaq	111(%rsp), %r13
	andq	$-64, %r13
	movq	%r13, -104(%rbp)
	testq	%r15, %r15
	je	.L2814
	movq	%rcx, -128(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r13, %rcx
	movq	%r15, %r8
	movq	%r14, %rdx
	movq	%r11, -120(%rbp)
	call	memcpy
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r8
.L2814:
	movq	112(%rbp), %r15
	addq	%r14, %r12
	movq	%rcx, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r11, %rcx
	addq	%r14, %r15
	movq	(%r11), %r14
.LEHB43:
	call	*24(%r14)
.LEHE43:
	movq	112(%rbp), %rcx
	movq	%r15, 32(%rsp)
	movq	%r12, 40(%rsp)
	movq	-112(%rbp), %r9
	movq	-120(%rbp), %r8
	movsbl	%al, %edx
	addq	%r13, %rcx
	call	_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_
	movq	-32(%rbp), %rcx
	subq	%r13, %rax
	vmovq	%xmm6, %rdx
	movq	%rax, %r15
	cmpq	%rdx, %rcx
	je	.L2810
.L2824:
	movq	-16(%rbp), %r9
	leaq	1(%r9), %rdx
	call	_ZdlPvy
	movq	-104(%rbp), %r13
.L2810:
	movq	-88(%rbp), %rcx
	call	_ZNSt6localeD1Ev
	jmp	.L2802
	.p2align 4
	.p2align 3
.L2819:
	testb	$64, %bl
	je	.L2827
	cmpq	$0, 112(%rbp)
	je	.L2828
	cmpq	112(%rbp), %r15
	movq	%r15, %rdi
	leaq	-80(%rbp), %rdx
	cmova	112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2855
.L2821:
	addq	112(%rbp), %r13
	subq	112(%rbp), %r15
	movl	$2, %r8d
	movl	$48, %esi
	jmp	.L2820
	.p2align 4
	.p2align 3
.L2852:
	leaq	-80(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r15, -80(%rbp)
	movq	%r13, -72(%rbp)
.LEHB44:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L2817
	.p2align 4
	.p2align 3
.L2827:
	movl	$2, %r8d
	movl	$32, %esi
	leaq	-80(%rbp), %rdx
	jmp	.L2820
	.p2align 4
	.p2align 3
.L2787:
	movzbl	(%r9), %eax
	movzwl	4(%rcx), %edx
	movl	%eax, %ecx
	andl	$15, %eax
	andl	$15, %ecx
	cmpq	%rax, %rdx
	jnb	.L2790
	leaq	(%rdx,%rdx,4), %rcx
	movl	$4, %edi
	shrx	%rdi, (%r9), %rax
	salq	$4, %rdx
	shrx	%rcx, %rax, %r11
	addq	8(%r9), %rdx
	andl	$31, %r11d
	vmovdqa	(%rdx), %xmm1
	vmovdqa	%xmm1, -32(%rbp)
.L2791:
	leaq	.L2795(%rip), %rdx
	movzbl	%r11b, %r8d
	movb	%r11b, -16(%rbp)
	vmovdqu	-32(%rbp), %ymm0
	movslq	(%rdx,%r8,4), %r9
	addq	%rdx, %r9
	vmovdqu	%ymm0, -64(%rbp)
	jmp	*%r9
	.section .rdata,"dr"
	.align 4
.L2795:
	.long	.L2848-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2799-.L2795
	.long	.L2798-.L2795
	.long	.L2797-.L2795
	.long	.L2796-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.long	.L2800-.L2795
	.section	.text$_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L2854:
	cmpb	$67, (%rcx)
	jne	.L2805
	cmpq	%r10, %rcx
	je	.L2810
	movq	-16(%rbp), %r10
	leaq	1(%r10), %rdx
	call	_ZdlPvy
	jmp	.L2810
	.p2align 4
	.p2align 3
.L2853:
	movq	%rdx, %rcx
	movq	%rdx, -88(%rbp)
	call	_ZNSt6localeC1Ev
	movq	-88(%rbp), %rdx
	movb	$1, 32(%rbx)
	jmp	.L2803
	.p2align 4
	.p2align 3
.L2790:
	testb	%cl, %cl
	jne	.L2792
	movl	$4, %r8d
	shrx	%r8, (%r9), %r9
	cmpq	%r9, %rdx
	jb	.L2856
.L2792:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L2828:
	movl	$2, %r8d
	movl	$48, %esi
	leaq	-80(%rbp), %rdx
	jmp	.L2820
	.p2align 4
	.p2align 3
.L2813:
	vmovq	%xmm6, %r12
	cmpq	%r12, %rcx
	je	.L2810
	movq	%r14, -104(%rbp)
	jmp	.L2824
	.p2align 4
	.p2align 3
.L2855:
	movq	%r9, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	movq	%r13, -72(%rbp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rcx
	jmp	.L2821
.L2797:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	js	.L2800
.L2849:
	vzeroupper
	jmp	.L2786
.L2798:
	movl	-64(%rbp), %edi
	vzeroupper
	jmp	.L2786
.L2799:
	movslq	-64(%rbp), %rdi
	testl	%edi, %edi
	jns	.L2849
.L2800:
	leaq	.LC6(%rip), %rcx
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L2796:
	movq	-64(%rbp), %rdi
	jmp	.L2849
.L2848:
	vzeroupper
	jmp	.L2792
	.p2align 4
	.p2align 3
.L2856:
	salq	$5, %rdx
	addq	8(%rbx), %rdx
	vmovdqu	(%rdx), %xmm2
	vmovdqa	%xmm2, -32(%rbp)
	movzbl	16(%rdx), %r10d
	movb	%r10b, -16(%rbp)
	movzbl	16(%rdx), %r11d
	jmp	.L2791
.L2830:
	movq	-96(%rbp), %rcx
	movq	%rax, %r14
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L2823:
	movq	-88(%rbp), %rcx
	call	_ZNSt6localeD1Ev
	movq	%r14, %rcx
	call	_Unwind_Resume
.LEHE44:
.L2811:
.LEHB45:
	call	_ZSt16__throw_bad_castv
.LEHE45:
.L2829:
	movq	%rax, %r14
	vzeroupper
	jmp	.L2823
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15640:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15640-.LLSDACSB15640
.LLSDACSB15640:
	.uleb128 .LEHB41-.LFB15640
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB42-.LFB15640
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L2829-.LFB15640
	.uleb128 0
	.uleb128 .LEHB43-.LFB15640
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L2830-.LFB15640
	.uleb128 0
	.uleb128 .LEHB44-.LFB15640
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB45-.LFB15640
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L2829-.LFB15640
	.uleb128 0
.LLSDACSE15640:
	.section	.text$_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
.LC139:
	.ascii "0b\0"
.LC140:
	.ascii "0B\0"
.LC141:
	.ascii "0X\0"
.LC142:
	.ascii "0x\0"
.LC143:
	.ascii "0\0"
	.align 8
.LC144:
	.ascii "format error: integer not representable as character\0"
	.section	.text$_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15528:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$88, %rsp
	.seh_stackalloc	88
	.seh_endprologue
	movzbl	1(%rcx), %r10d
	movl	%edx, %eax
	movq	%rcx, %rbx
	movq	%r8, %rsi
	movl	%r10d, %edx
	andl	$120, %edx
	cmpb	$56, %dl
	je	.L3000
	shrb	$3, %r10b
	andl	$15, %r10d
	cmpb	$4, %r10b
	je	.L2861
	ja	.L2862
	cmpb	$1, %r10b
	jbe	.L2863
	cmpb	$16, %dl
	leaq	.LC139(%rip), %r13
	leaq	.LC140(%rip), %r9
	cmovne	%r9, %r13
	testb	%al, %al
	jne	.L3001
	movl	$48, %ecx
	leaq	57(%rsp), %rdi
	leaq	56(%rsp), %rbp
.L2868:
	movzbl	(%rbx), %eax
	movb	%cl, 56(%rsp)
	testb	$16, %al
	je	.L2998
.L2901:
	movq	$-2, %r12
	movl	$2, %r8d
.L2872:
	leaq	0(%rbp,%r12), %rdx
	movl	%r8d, %r9d
	testl	%r8d, %r8d
	je	.L2873
	xorl	%ecx, %ecx
	leal	-1(%r8), %r11d
	movzbl	0(%r13,%rcx), %r10d
	andl	$7, %r11d
	movb	%r10b, (%rdx,%rcx)
	movl	$1, %ecx
	cmpl	%r8d, %ecx
	jnb	.L2873
	testl	%r11d, %r11d
	je	.L2889
	cmpl	$1, %r11d
	je	.L2966
	cmpl	$2, %r11d
	je	.L2967
	cmpl	$3, %r11d
	je	.L2968
	cmpl	$4, %r11d
	je	.L2969
	cmpl	$5, %r11d
	je	.L2970
	cmpl	$6, %r11d
	je	.L2971
	movl	$1, %r12d
	movl	$2, %ecx
	movzbl	0(%r13,%r12), %r8d
	movb	%r8b, (%rdx,%r12)
.L2971:
	movl	%ecx, %r11d
	incl	%ecx
	movzbl	0(%r13,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L2970:
	movl	%ecx, %r12d
	incl	%ecx
	movzbl	0(%r13,%r12), %r8d
	movb	%r8b, (%rdx,%r12)
.L2969:
	movl	%ecx, %r11d
	incl	%ecx
	movzbl	0(%r13,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L2968:
	movl	%ecx, %r12d
	incl	%ecx
	movzbl	0(%r13,%r12), %r8d
	movb	%r8b, (%rdx,%r12)
.L2967:
	movl	%ecx, %r11d
	incl	%ecx
	movzbl	0(%r13,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L2966:
	movl	%ecx, %r12d
	incl	%ecx
	movzbl	0(%r13,%r12), %r8d
	movb	%r8b, (%rdx,%r12)
	cmpl	%r9d, %ecx
	jnb	.L2873
.L2889:
	movl	%ecx, %r11d
	leal	1(%rcx), %r12d
	movzbl	0(%r13,%r11), %r10d
	movzbl	0(%r13,%r12), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r12)
	leal	2(%rcx), %r11d
	leal	3(%rcx), %r12d
	movzbl	0(%r13,%r11), %r10d
	movzbl	0(%r13,%r12), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r12)
	leal	4(%rcx), %r11d
	leal	5(%rcx), %r12d
	movzbl	0(%r13,%r11), %r10d
	movzbl	0(%r13,%r12), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r12)
	leal	6(%rcx), %r11d
	leal	7(%rcx), %r12d
	movzbl	0(%r13,%r11), %r10d
	movzbl	0(%r13,%r12), %r8d
	addl	$8, %ecx
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r12)
	cmpl	%r9d, %ecx
	jb	.L2889
	jmp	.L2873
	.p2align 4
	.p2align 3
.L3000:
	testb	%al, %al
	js	.L2859
	movb	%al, 64(%rsp)
	leaq	32(%rsp), %rcx
	leaq	64(%rsp), %rax
	movq	%rbx, %r8
	movq	%rsi, %rdx
	movq	$1, 32(%rsp)
	movq	%rax, 40(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	addq	$88, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L2863:
	testb	%al, %al
	je	.L2877
	movzbl	%al, %r8d
	cmpb	$9, %al
	jbe	.L2896
	cmpl	$99, %r8d
	movl	$2, %r13d
	movl	$3, %edi
	movl	$3, %edx
	cmovbe	%r13, %rdi
	cmovbe	%r13d, %edx
.L2876:
	leaq	56(%rsp), %rbp
	movq	%rbp, %rcx
	addq	%rbp, %rdi
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
.L2997:
	movzbl	(%rbx), %eax
	movq	%rbp, %rdx
.L2873:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3002
.L2891:
	cmpl	$3, %eax
	je	.L2903
.L2892:
	leaq	32(%rsp), %rcx
	movq	%rbp, %r8
	subq	%rdx, %rdi
	movq	%rdx, 40(%rsp)
	subq	%rdx, %r8
	movq	%rsi, %r9
	movq	%rcx, %rdx
	movq	%rbx, %rcx
	movq	%rdi, 32(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	addq	$88, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L2861:
	testb	%al, %al
	je	.L2877
	movzbl	%al, %edi
	movl	$34, %ecx
	movl	$2863311531, %ebp
	lzcntl	%edi, %r8d
	subl	%r8d, %ecx
	imulq	%rbp, %rcx
	shrq	$33, %rcx
	cmpl	$63, %edi
	jbe	.L3003
	movl	%edi, %r9d
	andl	$7, %eax
	shrl	$3, %r9d
	shrl	$6, %edi
	andl	$7, %r9d
	addl	$48, %eax
	addl	$48, %r9d
	movb	%al, 58(%rsp)
	movb	%r9b, 57(%rsp)
.L2880:
	addl	$48, %edi
.L2881:
	movl	%ecx, %r11d
	movb	%dil, 56(%rsp)
	leaq	56(%rsp), %rbp
	movq	$-1, %r12
	leaq	56(%rsp,%r11), %rdi
	leaq	.LC143(%rip), %r13
	movl	$1, %r8d
.L2882:
	movzbl	(%rbx), %eax
	movq	%rbp, %rdx
	testb	$16, %al
	jne	.L2872
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.L2891
	.p2align 4
	.p2align 3
.L3002:
	movl	$43, %r9d
.L2893:
	movb	%r9b, -1(%rdx)
	decq	%rdx
	jmp	.L2892
	.p2align 4
	.p2align 3
.L2877:
	leaq	57(%rsp), %rdi
	leaq	56(%rsp), %rbp
	movb	$48, 56(%rsp)
	jmp	.L2997
	.p2align 4
	.p2align 3
.L2862:
	cmpb	$40, %dl
	je	.L3004
	testb	%al, %al
	jne	.L2899
	leaq	.LC141(%rip), %r13
	leaq	57(%rsp), %rdi
	leaq	56(%rsp), %rbp
	movb	$48, 56(%rsp)
	cmpb	$48, %dl
	je	.L2885
.L2884:
	movzbl	(%rbx), %eax
	testb	$16, %al
	jne	.L2901
.L2998:
	movq	%rbp, %rdx
	jmp	.L2873
	.p2align 4
	.p2align 3
.L2903:
	movl	$32, %r9d
	jmp	.L2893
	.p2align 4
	.p2align 3
.L3001:
	movzbl	%al, %r11d
	movl	$32, %ecx
	movl	$31, %r8d
	lzcntl	%r11d, %edi
	subl	%edi, %ecx
	subl	%edi, %r8d
	je	.L2871
	andl	$1, %eax
	movl	$30, %r10d
	movl	%r11d, %edx
	movl	%r8d, %ebp
	addl	$48, %eax
	shrl	%edx
	movb	%al, 56(%rsp,%rbp)
	subl	%edi, %r10d
	je	.L2871
	andl	$1, %edx
	movl	$29, %eax
	movl	%r11d, %r9d
	movl	%r10d, %r12d
	addl	$48, %edx
	shrl	$2, %r9d
	movb	%dl, 56(%rsp,%r12)
	subl	%edi, %eax
	je	.L2871
	andl	$1, %r9d
	movl	$28, %r8d
	movl	%r11d, %ebp
	movl	%eax, %r10d
	addl	$48, %r9d
	shrl	$3, %ebp
	movb	%r9b, 56(%rsp,%r10)
	subl	%edi, %r8d
	je	.L2871
	andl	$1, %ebp
	movl	$27, %edx
	movl	%r11d, %r12d
	movl	%r8d, %eax
	addl	$48, %ebp
	shrl	$4, %r12d
	movb	%bpl, 56(%rsp,%rax)
	subl	%edi, %edx
	je	.L2871
	andl	$1, %r12d
	movl	$26, %r9d
	movl	%r11d, %r10d
	movl	%edx, %r8d
	addl	$48, %r12d
	shrl	$5, %r10d
	movb	%r12b, 56(%rsp,%r8)
	subl	%edi, %r9d
	je	.L2871
	andl	$1, %r10d
	movl	%r9d, %ebp
	addl	$48, %r10d
	shrl	$6, %r11d
	movb	%r10b, 56(%rsp,%rbp)
	cmpl	$24, %edi
	jne	.L2871
	andl	$1, %r11d
	addl	$48, %r11d
	movb	%r11b, 57(%rsp)
.L2871:
	movslq	%ecx, %r11
	leaq	56(%rsp), %rbp
	movl	$49, %ecx
	leaq	56(%rsp,%r11), %rdi
	jmp	.L2868
	.p2align 4
	.p2align 3
.L3004:
	testb	%al, %al
	jne	.L2898
	leaq	.LC142(%rip), %r13
	leaq	57(%rsp), %rdi
	leaq	56(%rsp), %rbp
	movb	$48, 56(%rsp)
	jmp	.L2884
	.p2align 4
	.p2align 3
.L2899:
	leaq	.LC141(%rip), %r13
.L2883:
	movzbl	%al, %r12d
	movl	$35, %r8d
	movabsq	$3978425819141910832, %rdi
	movabsq	$7378413942531504440, %rcx
	lzcntl	%r12d, %r10d
	subl	%r10d, %r8d
	movq	%rdi, 64(%rsp)
	movq	%rcx, 72(%rsp)
	shrl	$2, %r8d
	cmpl	$15, %r12d
	ja	.L3005
	movzbl	64(%rsp,%r12), %eax
.L2887:
	leaq	56(%rsp), %rbp
	leaq	56(%rsp,%r8), %rdi
	movb	%al, 56(%rsp)
	cmpb	$48, %dl
	jne	.L2884
.L2885:
	movq	%rdi, %rdx
	movq	%rbp, %r12
	andl	$7, %edx
	je	.L2888
	cmpq	$1, %rdx
	je	.L2959
	cmpq	$2, %rdx
	je	.L2960
	cmpq	$3, %rdx
	je	.L2961
	cmpq	$4, %rdx
	je	.L2962
	cmpq	$5, %rdx
	je	.L2963
	cmpq	$6, %rdx
	je	.L2964
	movsbl	0(%rbp), %ecx
	leaq	57(%rsp), %r12
	call	toupper
	movb	%al, 0(%rbp)
.L2964:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L2963:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L2962:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L2961:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L2960:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L2959:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rdi, %r12
	je	.L2996
.L2888:
	movsbl	(%r12), %ecx
	addq	$8, %r12
	call	toupper
	movsbl	-7(%r12), %ecx
	movb	%al, -8(%r12)
	call	toupper
	movsbl	-6(%r12), %ecx
	movb	%al, -7(%r12)
	call	toupper
	movsbl	-5(%r12), %ecx
	movb	%al, -6(%r12)
	call	toupper
	movsbl	-4(%r12), %ecx
	movb	%al, -5(%r12)
	call	toupper
	movsbl	-3(%r12), %ecx
	movb	%al, -4(%r12)
	call	toupper
	movsbl	-2(%r12), %ecx
	movb	%al, -3(%r12)
	call	toupper
	movsbl	-1(%r12), %ecx
	movb	%al, -2(%r12)
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rdi, %r12
	jne	.L2888
.L2996:
	movq	$-2, %r12
	movl	$2, %r8d
	jmp	.L2882
	.p2align 4
	.p2align 3
.L3005:
	andl	$15, %eax
	movzbl	64(%rsp,%rax), %ebp
	shrl	$4, %r12d
	movb	%bpl, 57(%rsp)
	movzbl	64(%rsp,%r12), %eax
	jmp	.L2887
	.p2align 4
	.p2align 3
.L2898:
	leaq	.LC142(%rip), %r13
	jmp	.L2883
.L2896:
	movl	$1, %edi
	movl	$1, %edx
	jmp	.L2876
.L3003:
	cmpl	$7, %edi
	jbe	.L2880
	shrl	$3, %edi
	andl	$7, %eax
	addl	$48, %edi
	addl	$48, %eax
	movb	%al, 57(%rsp)
	jmp	.L2881
.L2859:
	leaq	.LC144(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15531:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$144, %rsp
	.seh_stackalloc	144
	.seh_endprologue
	movzbl	1(%rcx), %eax
	movq	%rcx, %rsi
	movq	%rdx, %rbx
	movq	%r8, %rdi
	movq	%rdx, %r10
	movl	%eax, %ecx
	andl	$120, %ecx
	cmpb	$56, %cl
	je	.L3191
	shrb	$3, %al
	andl	$15, %eax
	testq	%rdx, %rdx
	js	.L3192
	cmpb	$4, %al
	je	.L3016
	ja	.L3017
	cmpb	$1, %al
	jbe	.L3018
	cmpb	$16, %cl
	leaq	.LC139(%rip), %r14
	leaq	.LC140(%rip), %rbp
	cmovne	%rbp, %r14
	testq	%rdx, %rdx
	jne	.L3014
	movl	$48, %r8d
	leaq	68(%rsp), %rbp
	leaq	67(%rsp), %r12
.L3023:
	movb	%r8b, 67(%rsp)
	testb	$16, (%rsi)
	je	.L3189
.L3068:
	movq	$-2, %r10
	movl	$2, %r11d
.L3027:
	addq	%r12, %r10
	movl	%r11d, %r9d
	testl	%r11d, %r11d
	je	.L3028
	xorl	%edx, %edx
	leal	-1(%r11), %ecx
	movl	$1, %r11d
	movzbl	(%r14,%rdx), %r13d
	andl	$7, %ecx
	movb	%r13b, (%r10,%rdx)
	cmpl	%r9d, %r11d
	jnb	.L3028
	testl	%ecx, %ecx
	je	.L3050
	cmpl	$1, %ecx
	je	.L3155
	cmpl	$2, %ecx
	je	.L3156
	cmpl	$3, %ecx
	je	.L3157
	cmpl	$4, %ecx
	je	.L3158
	cmpl	$5, %ecx
	je	.L3159
	cmpl	$6, %ecx
	je	.L3160
	movl	$1, %r8d
	movl	$2, %r11d
	movzbl	(%r14,%r8), %eax
	movb	%al, (%r10,%r8)
.L3160:
	movl	%r11d, %ecx
	incl	%r11d
	movzbl	(%r14,%rcx), %edx
	movb	%dl, (%r10,%rcx)
.L3159:
	movl	%r11d, %r13d
	incl	%r11d
	movzbl	(%r14,%r13), %r8d
	movb	%r8b, (%r10,%r13)
.L3158:
	movl	%r11d, %eax
	incl	%r11d
	movzbl	(%r14,%rax), %ecx
	movb	%cl, (%r10,%rax)
.L3157:
	movl	%r11d, %edx
	incl	%r11d
	movzbl	(%r14,%rdx), %r13d
	movb	%r13b, (%r10,%rdx)
.L3156:
	movl	%r11d, %eax
	incl	%r11d
	movzbl	(%r14,%rax), %r8d
	movb	%r8b, (%r10,%rax)
.L3155:
	movl	%r11d, %ecx
	incl	%r11d
	movzbl	(%r14,%rcx), %edx
	movb	%dl, (%r10,%rcx)
	cmpl	%r9d, %r11d
	jnb	.L3028
.L3050:
	movl	%r11d, %r13d
	leal	1(%r11), %ecx
	leal	2(%r11), %edx
	movzbl	(%r14,%r13), %eax
	movzbl	(%r14,%rcx), %r8d
	movb	%al, (%r10,%r13)
	leal	3(%r11), %eax
	movzbl	(%r14,%rdx), %r13d
	movb	%r8b, (%r10,%rcx)
	movzbl	(%r14,%rax), %ecx
	movb	%r13b, (%r10,%rdx)
	leal	4(%r11), %edx
	leal	5(%r11), %r13d
	movb	%cl, (%r10,%rax)
	movzbl	(%r14,%rdx), %r8d
	movzbl	(%r14,%r13), %eax
	leal	6(%r11), %ecx
	movb	%r8b, (%r10,%rdx)
	movb	%al, (%r10,%r13)
	leal	7(%r11), %r13d
	movzbl	(%r14,%rcx), %edx
	movzbl	(%r14,%r13), %r8d
	addl	$8, %r11d
	movb	%dl, (%r10,%rcx)
	movb	%r8b, (%r10,%r13)
	cmpl	%r9d, %r11d
	jb	.L3050
	jmp	.L3028
	.p2align 4
	.p2align 3
.L3192:
	negq	%r10
	cmpb	$4, %al
	je	.L3011
	ja	.L3012
	cmpb	$1, %al
	jbe	.L3013
	cmpb	$16, %cl
	leaq	.LC140(%rip), %r14
	leaq	.LC139(%rip), %rbp
	cmove	%rbp, %r14
.L3014:
	movl	$64, %r11d
	movl	$63, %eax
	lzcntq	%r10, %r8
	subl	%r8d, %r11d
	subl	%r8d, %eax
	je	.L3026
	movl	$62, %r9d
	movl	%eax, %edx
	subl	%r8d, %r9d
	leaq	64(%rsp,%rdx), %r12
	subq	%r9, %rdx
	movq	%r12, %r13
	leaq	63(%rsp,%rdx), %rcx
	subq	%rcx, %r13
	andl	$7, %r13d
	je	.L3025
	cmpq	$1, %r13
	je	.L3142
	cmpq	$2, %r13
	je	.L3143
	cmpq	$3, %r13
	je	.L3144
	cmpq	$4, %r13
	je	.L3145
	cmpq	$5, %r13
	je	.L3146
	cmpq	$6, %r13
	je	.L3147
	movl	%r10d, %ebp
	decq	%r12
	andl	$1, %ebp
	addl	$48, %ebp
	shrq	%r10
	movb	%bpl, 4(%r12)
.L3147:
	movl	%r10d, %r8d
	decq	%r12
	andl	$1, %r8d
	addl	$48, %r8d
	shrq	%r10
	movb	%r8b, 4(%r12)
.L3146:
	movl	%r10d, %eax
	decq	%r12
	andl	$1, %eax
	addl	$48, %eax
	shrq	%r10
	movb	%al, 4(%r12)
.L3145:
	movl	%r10d, %edx
	decq	%r12
	andl	$1, %edx
	addl	$48, %edx
	shrq	%r10
	movb	%dl, 4(%r12)
.L3144:
	movl	%r10d, %r9d
	decq	%r12
	andl	$1, %r9d
	addl	$48, %r9d
	shrq	%r10
	movb	%r9b, 4(%r12)
.L3143:
	movl	%r10d, %r13d
	decq	%r12
	andl	$1, %r13d
	addl	$48, %r13d
	shrq	%r10
	movb	%r13b, 4(%r12)
.L3142:
	movl	%r10d, %ebp
	decq	%r12
	andl	$1, %ebp
	addl	$48, %ebp
	movb	%bpl, 4(%r12)
	shrq	%r10
	cmpq	%rcx, %r12
	je	.L3026
.L3025:
	movq	%r10, %rax
	movl	%r10d, %r8d
	movq	%r10, %rdx
	movq	%r10, %r9
	shrq	%rax
	andl	$1, %r8d
	andl	$1, %eax
	movq	%r10, %r13
	movq	%r10, %rbp
	addl	$48, %r8d
	addl	$48, %eax
	subq	$8, %r12
	movb	%r8b, 11(%r12)
	movq	%r10, %r8
	movb	%al, 10(%r12)
	movl	%r10d, %eax
	shrq	$2, %rdx
	shrq	$3, %r9
	shrq	$4, %r13
	shrq	$5, %rbp
	shrq	$6, %r8
	shrb	$7, %al
	andl	$1, %edx
	andl	$1, %r9d
	andl	$1, %r13d
	andl	$1, %ebp
	andl	$1, %r8d
	addl	$48, %edx
	addl	$48, %r9d
	addl	$48, %r13d
	addl	$48, %ebp
	addl	$48, %r8d
	addl	$48, %eax
	movb	%dl, 9(%r12)
	movb	%r9b, 8(%r12)
	movb	%r13b, 7(%r12)
	movb	%bpl, 6(%r12)
	movb	%r8b, 5(%r12)
	movb	%al, 4(%r12)
	shrq	$8, %r10
	cmpq	%rcx, %r12
	jne	.L3025
.L3026:
	movslq	%r11d, %r10
	leaq	67(%rsp), %r12
	movl	$49, %r8d
	leaq	67(%rsp,%r10), %rbp
	jmp	.L3023
	.p2align 4
	.p2align 3
.L3191:
	leaq	128(%rdx), %rbp
	cmpq	$255, %rbp
	ja	.L3008
	leaq	64(%rsp), %r11
	movb	%dl, 64(%rsp)
	leaq	32(%rsp), %rcx
	movq	%rsi, %r8
	movq	%rdi, %rdx
	movq	$1, 32(%rsp)
	movq	%r11, 40(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
.L3009:
	addq	$144, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L3017:
	cmpb	$40, %cl
	je	.L3193
	testq	%rdx, %rdx
	jne	.L3066
	leaq	68(%rsp), %rbp
	leaq	.LC141(%rip), %r14
	leaq	67(%rsp), %r12
	movb	$48, 67(%rsp)
	cmpb	$48, %cl
	je	.L3043
.L3044:
	testb	$16, (%rsi)
	jne	.L3068
	.p2align 4
	.p2align 3
.L3189:
	movq	%r12, %r10
.L3028:
	leaq	-1(%r10), %r9
	testq	%rbx, %rbx
	jns	.L3194
	movb	$45, -1(%r10)
.L3055:
	movq	%r9, %r10
.L3053:
	movq	%r12, %r8
	subq	%r10, %rbp
	leaq	32(%rsp), %rdx
	movq	%rdi, %r9
	subq	%r10, %r8
	movq	%rsi, %rcx
	movq	%rbp, 32(%rsp)
	movq	%r10, 40(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L3009
	.p2align 4
	.p2align 3
.L3012:
	cmpb	$40, %cl
	leaq	.LC142(%rip), %r14
	leaq	.LC141(%rip), %r11
	cmovne	%r11, %r14
.L3015:
	movl	$67, %ebp
	lzcntq	%r10, %r13
	movabsq	$3978425819141910832, %rax
	movabsq	$7378413942531504440, %rdx
	subl	%r13d, %ebp
	movq	%rax, 48(%rsp)
	movq	%rdx, 56(%rsp)
	shrl	$2, %ebp
	leal	-1(%rbp), %r12d
	cmpq	$255, %r10
	jbe	.L3045
	.p2align 6
	.p2align 4
	.p2align 3
.L3046:
	movq	%r10, %r13
	movq	%r10, %r8
	movl	%r12d, %r9d
	leal	-1(%r12), %eax
	shrq	$4, %r13
	andl	$15, %r8d
	andl	$15, %r13d
	subl	$2, %r12d
	movzbl	48(%rsp,%r8), %r11d
	movzbl	48(%rsp,%r13), %edx
	shrq	$8, %r10
	movb	%r11b, 67(%rsp,%r9)
	movb	%dl, 67(%rsp,%rax)
	cmpq	$255, %r10
	ja	.L3046
.L3045:
	cmpq	$15, %r10
	ja	.L3195
	movzbl	48(%rsp,%r10), %r10d
.L3048:
	leaq	67(%rsp), %r12
	leaq	67(%rsp,%rbp), %rbp
	movb	%r10b, 67(%rsp)
	cmpb	$48, %cl
	jne	.L3044
.L3043:
	movq	%rbp, %rcx
	movq	%r12, %r13
	subq	%r12, %rcx
	andl	$7, %ecx
	je	.L3049
	cmpq	$1, %rcx
	je	.L3148
	cmpq	$2, %rcx
	je	.L3149
	cmpq	$3, %rcx
	je	.L3150
	cmpq	$4, %rcx
	je	.L3151
	cmpq	$5, %rcx
	je	.L3152
	cmpq	$6, %rcx
	je	.L3153
	movsbl	(%r12), %ecx
	leaq	68(%rsp), %r13
	call	toupper
	movb	%al, (%r12)
.L3153:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
.L3152:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
.L3151:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
.L3150:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
.L3149:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
.L3148:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	toupper
	movb	%al, -1(%r13)
	cmpq	%rbp, %r13
	je	.L3188
.L3049:
	movsbl	0(%r13), %ecx
	addq	$8, %r13
	call	toupper
	movsbl	-7(%r13), %ecx
	movb	%al, -8(%r13)
	call	toupper
	movsbl	-6(%r13), %ecx
	movb	%al, -7(%r13)
	call	toupper
	movsbl	-5(%r13), %ecx
	movb	%al, -6(%r13)
	call	toupper
	movsbl	-4(%r13), %ecx
	movb	%al, -5(%r13)
	call	toupper
	movsbl	-3(%r13), %ecx
	movb	%al, -4(%r13)
	call	toupper
	movsbl	-2(%r13), %ecx
	movb	%al, -3(%r13)
	call	toupper
	movsbl	-1(%r13), %ecx
	movb	%al, -2(%r13)
	call	toupper
	movb	%al, -1(%r13)
	cmpq	%rbp, %r13
	jne	.L3049
.L3188:
	movl	$2, %r11d
	jmp	.L3041
	.p2align 4
	.p2align 3
.L3194:
	movzbl	(%rsi), %edx
.L3052:
	shrb	$2, %dl
	andl	$3, %edx
	cmpl	$1, %edx
	je	.L3196
	cmpl	$3, %edx
	jne	.L3053
	movb	$32, -1(%r10)
	jmp	.L3055
	.p2align 4
	.p2align 3
.L3196:
	movb	$43, -1(%r10)
	jmp	.L3055
	.p2align 4
	.p2align 3
.L3016:
	testq	%rdx, %rdx
	jne	.L3011
.L3190:
	movzbl	(%rsi), %edx
	movb	$48, 67(%rsp)
.L3029:
	leaq	67(%rsp), %r12
	leaq	68(%rsp), %rbp
	leaq	66(%rsp), %r9
	movq	%r12, %r10
	jmp	.L3052
	.p2align 4
	.p2align 3
.L3018:
	testq	%rdx, %rdx
	je	.L3190
	.p2align 4
	.p2align 3
.L3013:
	cmpq	$9, %r10
	jbe	.L3061
	movq	%r10, %r12
	movl	$1, %r9d
	movabsq	$3777893186295716171, %r14
	jmp	.L3035
	.p2align 4
	.p2align 3
.L3031:
	cmpq	$999, %r12
	jbe	.L3197
	cmpq	$9999, %r12
	jbe	.L3198
	movq	%r12, %rax
	addl	$4, %r9d
	mulq	%r14
	shrq	$11, %rdx
	cmpq	$99999, %r12
	jbe	.L3032
	movq	%rdx, %r12
.L3035:
	cmpq	$99, %r12
	ja	.L3031
	incl	%r9d
.L3032:
	movl	%r9d, %ebp
	cmpl	$64, %r9d
	ja	.L3199
.L3030:
	leaq	67(%rsp), %r12
	movq	%r10, %r8
	movl	%r9d, %edx
	movq	%r12, %rcx
	addq	%r12, %rbp
	call	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
	jmp	.L3189
	.p2align 4
	.p2align 3
.L3011:
	movl	$66, %r13d
	movl	$2863311531, %eax
	lzcntq	%r10, %r11
	subl	%r11d, %r13d
	imulq	%rax, %r13
	shrq	$33, %r13
	leal	-1(%r13), %edx
	cmpq	$63, %r10
	jbe	.L3037
	.p2align 6
	.p2align 4
	.p2align 3
.L3038:
	movq	%r10, %r8
	movq	%r10, %r14
	movl	%edx, %ecx
	leal	-1(%rdx), %ebp
	shrq	$3, %r8
	andl	$7, %r14d
	andl	$7, %r8d
	subl	$2, %edx
	shrq	$6, %r10
	addl	$48, %r14d
	addl	$48, %r8d
	movb	%r14b, 67(%rsp,%rcx)
	movb	%r8b, 67(%rsp,%rbp)
	cmpq	$63, %r10
	ja	.L3038
.L3037:
	leal	48(%r10), %r9d
	cmpq	$7, %r10
	ja	.L3200
.L3040:
	movl	%r13d, %r10d
	leaq	67(%rsp), %r12
	leaq	.LC143(%rip), %r14
	movl	$1, %r11d
	leaq	67(%rsp,%r10), %rbp
	movb	%r9b, 67(%rsp)
.L3041:
	movq	%r12, %r10
	testb	$16, (%rsi)
	je	.L3028
.L3042:
	movq	%r11, %r10
	negq	%r10
	jmp	.L3027
	.p2align 4
	.p2align 3
.L3195:
	movq	%r10, %r12
	andl	$15, %r12d
	movzbl	48(%rsp,%r12), %r9d
	shrq	$4, %r10
	movb	%r9b, 68(%rsp)
	movzbl	48(%rsp,%r10), %r10d
	jmp	.L3048
	.p2align 4
	.p2align 3
.L3200:
	movq	%r10, %r12
	movq	%r10, %r9
	shrq	$3, %r9
	andl	$7, %r12d
	addl	$48, %r9d
	addl	$48, %r12d
	movb	%r12b, 68(%rsp)
	jmp	.L3040
	.p2align 4
	.p2align 3
.L3193:
	testq	%rdx, %rdx
	jne	.L3064
	movzbl	(%rsi), %edx
	movb	$48, 67(%rsp)
	testb	$16, %dl
	je	.L3029
	leaq	68(%rsp), %rbp
	leaq	.LC142(%rip), %r14
	movl	$2, %r11d
	leaq	67(%rsp), %r12
	jmp	.L3042
	.p2align 4
	.p2align 3
.L3197:
	addl	$2, %r9d
	jmp	.L3032
	.p2align 4
	.p2align 3
.L3198:
	addl	$3, %r9d
	jmp	.L3032
.L3066:
	leaq	.LC141(%rip), %r14
	jmp	.L3015
.L3199:
	leaq	131(%rsp), %rbp
	leaq	67(%rsp), %r12
	jmp	.L3189
.L3064:
	leaq	.LC142(%rip), %r14
	jmp	.L3015
.L3061:
	movl	$1, %ebp
	movl	$1, %r9d
	jmp	.L3030
.L3008:
	leaq	.LC144(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15545:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$504, %rsp
	.seh_stackalloc	504
	vmovaps	%xmm6, 480(%rsp)
	.seh_savexmm	%xmm6, 480
	.seh_endprologue
	movzbl	1(%rcx), %eax
	vmovdqa	(%rdx), %xmm1
	movq	%rcx, %rbx
	movq	%r8, 592(%rsp)
	movl	%eax, %r9d
	andl	$120, %r9d
	vmovdqa	%xmm1, 32(%rsp)
	cmpb	$56, %r9b
	je	.L3404
	shrb	$3, %al
	vpextrq	$1, %xmm1, %rcx
	vmovq	%xmm1, %rdx
	andl	$15, %eax
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	testq	%rcx, %rcx
	js	.L3405
	cmpb	$4, %al
	je	.L3211
	ja	.L3212
	cmpb	$1, %al
	jbe	.L3213
	cmpb	$16, %r9b
	leaq	.LC139(%rip), %r14
	leaq	.LC140(%rip), %r12
	movq	%rdx, %r13
	cmovne	%r12, %r14
	orq	%rcx, %r13
	jne	.L3209
	movl	$48, %r15d
	leaq	132(%rsp), %rdi
	leaq	131(%rsp), %rbp
.L3218:
	movb	%r15b, 131(%rsp)
	testb	$16, (%rbx)
	je	.L3402
.L3271:
	movq	$-2, %rsi
	movl	$2, %r15d
.L3223:
	addq	%rbp, %rsi
	movl	%r15d, %r8d
	testl	%r15d, %r15d
	je	.L3224
	xorl	%edx, %edx
	leal	-1(%r15), %r12d
	movl	$1, %eax
	movzbl	(%r14,%rdx), %r11d
	andl	$7, %r12d
	movb	%r11b, (%rsi,%rdx)
	cmpl	%r15d, %eax
	jnb	.L3224
	testl	%r12d, %r12d
	je	.L3252
	cmpl	$1, %r12d
	je	.L3361
	cmpl	$2, %r12d
	je	.L3362
	cmpl	$3, %r12d
	je	.L3363
	cmpl	$4, %r12d
	je	.L3364
	cmpl	$5, %r12d
	je	.L3365
	cmpl	$6, %r12d
	je	.L3366
	movl	$1, %r13d
	movl	$2, %eax
	movzbl	(%r14,%r13), %r10d
	movb	%r10b, (%rsi,%r13)
.L3366:
	movl	%eax, %ecx
	incl	%eax
	movzbl	(%r14,%rcx), %r9d
	movb	%r9b, (%rsi,%rcx)
.L3365:
	movl	%eax, %r15d
	incl	%eax
	movzbl	(%r14,%r15), %r12d
	movb	%r12b, (%rsi,%r15)
.L3364:
	movl	%eax, %edx
	incl	%eax
	movzbl	(%r14,%rdx), %r11d
	movb	%r11b, (%rsi,%rdx)
.L3363:
	movl	%eax, %r13d
	incl	%eax
	movzbl	(%r14,%r13), %r10d
	movb	%r10b, (%rsi,%r13)
.L3362:
	movl	%eax, %ecx
	incl	%eax
	movzbl	(%r14,%rcx), %r9d
	movb	%r9b, (%rsi,%rcx)
.L3361:
	movl	%eax, %r15d
	incl	%eax
	movzbl	(%r14,%r15), %r12d
	movb	%r12b, (%rsi,%r15)
	cmpl	%r8d, %eax
	jnb	.L3224
.L3252:
	movl	%eax, %edx
	leal	1(%rax), %r13d
	leal	2(%rax), %ecx
	leal	3(%rax), %r15d
	movzbl	(%r14,%rdx), %r11d
	movzbl	(%r14,%r13), %r10d
	movzbl	(%r14,%rcx), %r9d
	movzbl	(%r14,%r15), %r12d
	movb	%r11b, (%rsi,%rdx)
	movb	%r10b, (%rsi,%r13)
	leal	4(%rax), %edx
	movb	%r9b, (%rsi,%rcx)
	leal	5(%rax), %r13d
	movb	%r12b, (%rsi,%r15)
	leal	6(%rax), %ecx
	leal	7(%rax), %r15d
	movzbl	(%r14,%rdx), %r11d
	movzbl	(%r14,%r13), %r10d
	movzbl	(%r14,%rcx), %r9d
	addl	$8, %eax
	movzbl	(%r14,%r15), %r12d
	movb	%r11b, (%rsi,%rdx)
	movb	%r10b, (%rsi,%r13)
	movb	%r9b, (%rsi,%rcx)
	movb	%r12b, (%rsi,%r15)
	cmpl	%r8d, %eax
	jb	.L3252
	.p2align 4
	.p2align 3
.L3224:
	cmpq	$0, 40(%rsp)
	leaq	-1(%rsi), %r9
	js	.L3394
.L3413:
	movzbl	(%rbx), %r11d
.L3254:
	shrb	$2, %r11b
	andl	$3, %r11d
	cmpl	$1, %r11d
	je	.L3406
	cmpl	$3, %r11d
	je	.L3407
.L3255:
	movq	592(%rsp), %r9
	movq	%rbp, %r8
	subq	%rsi, %rdi
	leaq	112(%rsp), %rdx
	subq	%rsi, %r8
	movq	%rbx, %rcx
	movq	%rdi, 112(%rsp)
	movq	%rsi, 120(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L3204
	.p2align 4
	.p2align 3
.L3405:
	negq	%rsi
	adcq	$0, %rdi
	negq	%rdi
	cmpb	$4, %al
	je	.L3206
	ja	.L3207
	cmpb	$1, %al
	jbe	.L3208
	cmpb	$16, %r9b
	leaq	.LC140(%rip), %r14
	leaq	.LC139(%rip), %r11
	cmove	%r11, %r14
.L3209:
	testq	%rdi, %rdi
	jne	.L3408
	lzcntq	%rsi, %rax
	movl	$128, %r15d
	addl	$64, %eax
	movl	$127, %ecx
	subl	%eax, %r15d
	subl	%eax, %ecx
	je	.L3262
.L3220:
	movl	%ecx, %r10d
	decl	%ecx
	leaq	128(%rsp,%r10), %rbp
	subq	%rcx, %r10
	leaq	127(%rsp,%r10), %r8
	movq	%rbp, %r9
	subq	%r8, %r9
	andl	$7, %r9d
	je	.L3400
	cmpq	$1, %r9
	je	.L3348
	cmpq	$2, %r9
	je	.L3349
	cmpq	$3, %r9
	je	.L3350
	cmpq	$4, %r9
	je	.L3351
	cmpq	$5, %r9
	je	.L3352
	cmpq	$6, %r9
	je	.L3353
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rbp)
	decq	%rbp
.L3353:
	movl	%esi, %eax
	andl	$1, %eax
	addl	$48, %eax
	shrq	%rdi
	rcrq	%rsi
	movb	%al, 3(%rbp)
	decq	%rbp
.L3352:
	movl	%esi, %ecx
	andl	$1, %ecx
	addl	$48, %ecx
	shrq	%rdi
	rcrq	%rsi
	movb	%cl, 3(%rbp)
	decq	%rbp
.L3351:
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rbp)
	decq	%rbp
.L3350:
	movl	%esi, %eax
	andl	$1, %eax
	addl	$48, %eax
	shrq	%rdi
	rcrq	%rsi
	movb	%al, 3(%rbp)
	decq	%rbp
.L3349:
	movl	%esi, %ecx
	andl	$1, %ecx
	addl	$48, %ecx
	shrq	%rdi
	rcrq	%rsi
	movb	%cl, 3(%rbp)
	decq	%rbp
.L3348:
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rbp)
	decq	%rbp
	cmpq	%r8, %rbp
	je	.L3221
.L3400:
	movq	%rbx, %r12
.L3222:
	movl	%esi, %ebx
	movq	%rdi, %r13
	movq	%rsi, %r11
	movq	%rsi, %r10
	andl	$1, %ebx
	movq	%rsi, %rax
	movq	%rsi, %rdx
	addl	$48, %ebx
	shrq	%r13
	rcrq	%r11
	movb	%bl, 3(%rbp)
	subq	$8, %rbp
	shrdq	$2, %rdi, %r10
	shrdq	$3, %rdi, %rax
	andl	$1, %r11d
	andl	$1, %r10d
	andl	$1, %eax
	addl	$48, %r11d
	addl	$48, %r10d
	addl	$48, %eax
	movb	%r11b, 10(%rbp)
	movb	%r10b, 9(%rbp)
	movq	%rsi, %r11
	movq	%rsi, %r10
	movb	%al, 8(%rbp)
	movq	%rsi, %rax
	shrdq	$4, %rdi, %rdx
	shrdq	$5, %rdi, %r11
	shrdq	$6, %rdi, %r10
	shrdq	$7, %rdi, %rax
	andl	$1, %edx
	andl	$1, %r11d
	andl	$1, %r10d
	andl	$1, %eax
	addl	$48, %edx
	addl	$48, %r11d
	addl	$48, %r10d
	addl	$48, %eax
	shrdq	$8, %rdi, %rsi
	movb	%dl, 7(%rbp)
	movb	%r11b, 6(%rbp)
	movb	%r10b, 5(%rbp)
	movb	%al, 4(%rbp)
	shrq	$8, %rdi
	cmpq	%r8, %rbp
	jne	.L3222
	movq	%r12, %rbx
.L3221:
	movslq	%r15d, %rdi
	leaq	131(%rsp), %rbp
	movl	$49, %r15d
	leaq	131(%rsp,%rdi), %rdi
	jmp	.L3218
	.p2align 4
	.p2align 3
.L3404:
	movl	$127, %eax
	vmovq	%xmm1, %rdi
	movl	$0, %r11d
	vpextrq	$1, %xmm1, %rbp
	cmpq	%rdi, %rax
	sbbq	%rbp, %r11
	jl	.L3203
	movq	%r8, %rdx
	leaq	272(%rsp), %r12
	leaq	112(%rsp), %rcx
	movq	%rbx, %r8
	movb	%dil, 272(%rsp)
	movq	$1, 112(%rsp)
	movq	%r12, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	nop
.L3204:
	vmovaps	480(%rsp), %xmm6
	addq	$504, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L3212:
	cmpb	$40, %r9b
	je	.L3409
	movq	%rdx, %r8
	orq	%rcx, %r8
	jne	.L3269
	movb	$48, 131(%rsp)
	cmpb	$48, %r9b
	je	.L3270
	leaq	.LC141(%rip), %r14
	leaq	132(%rsp), %rdi
	leaq	131(%rsp), %rbp
	jmp	.L3244
	.p2align 4
	.p2align 3
.L3207:
	cmpb	$40, %r9b
	leaq	.LC142(%rip), %r14
	leaq	.LC141(%rip), %rax
	cmovne	%rax, %r14
.L3210:
	testq	%rdi, %rdi
	jne	.L3410
	movl	$67, %r11d
	lzcntq	%rsi, %rcx
	movabsq	$3978425819141910832, %r8
	movabsq	$7378413942531504440, %rdx
	subl	%ecx, %r11d
	movl	$255, %r10d
	movq	%r8, 272(%rsp)
	movq	%rdx, 280(%rsp)
	shrl	$2, %r11d
	leal	-1(%r11), %r13d
	cmpq	%rsi, %r10
	jnb	.L3411
.L3246:
	leaq	272(%rsp), %rdx
	movl	$255, %r10d
	xorl	%r8d, %r8d
	movq	%rbx, %rbp
	.p2align 4
	.p2align 3
.L3248:
	movq	%rsi, %rcx
	movl	%r13d, %ebx
	leal	-1(%r13), %eax
	movq	%r8, %r15
	andl	$15, %ecx
	subl	$2, %r13d
	addq	%rdx, %rcx
	movzbl	(%rcx), %r12d
	movb	%r12b, 131(%rsp,%rbx)
	movq	%rsi, %r12
	shrdq	$4, %rdi, %r12
	andl	$15, %r12d
	leaq	(%r12,%rdx), %rbx
	movzbl	(%rbx), %ecx
	shrdq	$8, %rdi, %rsi
	shrq	$8, %rdi
	movb	%cl, 131(%rsp,%rax)
	cmpq	%rsi, %r10
	sbbq	%rdi, %r15
	jc	.L3248
	movq	%rbp, %rbx
.L3247:
	movl	$15, %r13d
	movl	$0, %r10d
	cmpq	%rsi, %r13
	sbbq	%rdi, %r10
	jc	.L3412
	addq	%rsi, %rdx
	movzbl	(%rdx), %esi
.L3250:
	leaq	131(%rsp), %rbp
	leaq	131(%rsp,%r11), %rdi
	movb	%sil, 131(%rsp)
	cmpb	$48, %r9b
	je	.L3243
.L3244:
	testb	$16, (%rbx)
	jne	.L3271
	.p2align 4
	.p2align 3
.L3402:
	cmpq	$0, 40(%rsp)
	movq	%rbp, %rsi
	leaq	-1(%rsi), %r9
	jns	.L3413
.L3394:
	movb	$45, -1(%rsi)
.L3257:
	movq	%r9, %rsi
	jmp	.L3255
	.p2align 4
	.p2align 3
.L3406:
	movb	$43, -1(%rsi)
	jmp	.L3257
	.p2align 4
	.p2align 3
.L3213:
	movq	%rdx, %rax
	orq	%rcx, %rax
	jne	.L3208
.L3403:
	movzbl	(%rbx), %r11d
	movb	$48, 131(%rsp)
.L3225:
	leaq	131(%rsp), %rbp
	leaq	132(%rsp), %rdi
	leaq	130(%rsp), %r9
	movq	%rbp, %rsi
	jmp	.L3254
	.p2align 4
	.p2align 3
.L3211:
	movq	%rdx, %rbp
	orq	%rcx, %rbp
	je	.L3403
	.p2align 4
	.p2align 3
.L3206:
	testq	%rdi, %rdi
	jne	.L3414
	movl	$66, %r14d
	movl	$2863311531, %r10d
	lzcntq	%rsi, %r8
	movl	$63, %r11d
	subl	%r8d, %r14d
	imulq	%r10, %r14
	shrq	$33, %r14
	leal	-1(%r14), %r12d
	cmpq	%rsi, %r11
	jnb	.L3238
.L3237:
	movl	$63, %eax
	xorl	%ebp, %ebp
	movq	%rbx, %r9
	.p2align 4
	.p2align 3
.L3239:
	movq	%rsi, %rdx
	movq	%rsi, %rcx
	movl	%r12d, %ebx
	leal	-1(%r12), %r10d
	shrdq	$3, %rdi, %rdx
	shrdq	$6, %rdi, %rsi
	andl	$7, %ecx
	andl	$7, %edx
	subl	$2, %r12d
	movq	%rbp, %r11
	shrq	$6, %rdi
	addl	$48, %ecx
	addl	$48, %edx
	cmpq	%rsi, %rax
	sbbq	%rdi, %r11
	movb	%cl, 131(%rsp,%rbx)
	movb	%dl, 131(%rsp,%r10)
	jc	.L3239
	movq	%r9, %rbx
.L3238:
	movl	$7, %r12d
	movl	$0, %r13d
	cmpq	%rsi, %r12
	sbbq	%rdi, %r13
	jc	.L3415
	addl	$48, %esi
.L3241:
	movl	%r14d, %edi
	leaq	131(%rsp), %rbp
	leaq	.LC143(%rip), %r14
	movl	$1, %r15d
	leaq	131(%rsp,%rdi), %rdi
	movb	%sil, 131(%rsp)
.L3242:
	movq	%rbp, %rsi
	testb	$16, (%rbx)
	je	.L3224
	movq	%r15, %rsi
	negq	%rsi
	jmp	.L3223
	.p2align 4
	.p2align 3
.L3407:
	movb	$32, -1(%rsi)
	jmp	.L3257
	.p2align 4
	.p2align 3
.L3208:
	movl	$9, %r14d
	movl	$0, %ebp
	cmpq	%rsi, %r14
	sbbq	%rdi, %rbp
	jnc	.L3264
	movq	%rdi, %r8
	movl	$1, %r11d
	leaq	80(%rsp), %rax
	movq	%rdi, 72(%rsp)
	movq	%rbx, 576(%rsp)
	movq	%rsi, %rbp
	xorl	%r14d, %r14d
	movl	$9999, %r13d
	leaq	96(%rsp), %r15
	vmovq	%rax, %xmm6
	movl	$99999, %r12d
	movl	%r11d, %edi
	movq	%r8, %rbx
	movq	%rsi, 64(%rsp)
	jmp	.L3231
	.p2align 4
	.p2align 3
.L3227:
	movl	$999, %r10d
	movq	%r14, %rcx
	cmpq	%rbp, %r10
	sbbq	%rbx, %rcx
	jnc	.L3416
	cmpq	%rbp, %r13
	movq	%r14, %r9
	sbbq	%rbx, %r9
	jnc	.L3417
	vmovq	%xmm6, %rdx
	movq	%r15, %rcx
	movq	%rbp, 96(%rsp)
	addl	$4, %edi
	movq	%rbx, 104(%rsp)
	movq	$10000, 80(%rsp)
	movq	$0, 88(%rsp)
	call	__udivti3
	cmpq	%rbp, %r12
	movq	%r14, %rbp
	vmovdqa	%xmm0, 48(%rsp)
	movq	48(%rsp), %r8
	sbbq	%rbx, %rbp
	movq	56(%rsp), %r11
	jnc	.L3418
	movq	%r8, %rbp
	movq	%r11, %rbx
.L3231:
	movl	$99, %esi
	movq	%r14, %rdx
	cmpq	%rbp, %rsi
	sbbq	%rbx, %rdx
	jc	.L3227
	movl	%edi, %r9d
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	576(%rsp), %rbx
	incl	%r9d
.L3228:
	cmpl	$128, %r9d
	ja	.L3266
	vmovdqa64	.LC24(%rip), %zmm0
	vmovdqa64	.LC25(%rip), %zmm2
	movl	$99, %r10d
	movl	$0, %r8d
	cmpq	%rsi, %r10
	movabsq	$4122263930388298034, %rax
	movabsq	$16106987313379638, %rdx
	leal	-1(%r9), %ecx
	vmovdqa64	.LC26(%rip), %zmm3
	sbbq	%rdi, %r8
	leaq	272(%rsp), %rbp
	movq	%rdx, 465(%rsp)
	vmovdqu64	%zmm0, 272(%rsp)
	vmovdqu64	%zmm2, 336(%rsp)
	vmovdqu64	%zmm3, 400(%rsp)
	movq	%rax, 457(%rsp)
	jnc	.L3233
	movabsq	$2951479051793528258, %r11
	movabsq	$1152921504606846975, %r14
	movabsq	$-8116567392432202711, %r15
	movl	%r9d, 64(%rsp)
	vmovq	%r11, %xmm4
	.p2align 4
	.p2align 3
.L3234:
	movq	%rsi, %r12
	movq	%rsi, %r9
	movq	%rdi, %rax
	vmovq	%xmm4, %r13
	shrdq	$60, %rdi, %r12
	shrq	$56, %rax
	andq	%r14, %r12
	andq	%r14, %r9
	addq	%r12, %r9
	xorl	%r12d, %r12d
	addq	%rax, %r9
	movabsq	$5165088340638674453, %rax
	movq	%r12, 56(%rsp)
	mulq	%r9
	movq	%r9, %r10
	subq	%rdx, %r10
	shrq	%r10
	addq	%r10, %rdx
	shrq	$4, %rdx
	leaq	(%rdx,%rdx,4), %r8
	movq	%rsi, %rdx
	leaq	(%r8,%r8,4), %r11
	movq	%rdi, %r8
	subq	%r11, %r9
	subq	%r9, %rdx
	movq	%r9, 48(%rsp)
	sbbq	%r12, %r8
	imulq	%rdx, %r13
	mulx	%r15, %r10, %rax
	movq	%r10, %r9
	imulq	%r15, %r8
	andl	$3, %r9d
	movl	$25, %edx
	addq	%r13, %r8
	mulx	%r9, %r12, %r13
	movq	%rsi, %r13
	movq	%r10, %rsi
	addq	%rax, %r8
	addq	48(%rsp), %r12
	movq	%rdi, %rax
	leal	-1(%rcx), %r9d
	movq	%r8, %rdi
	addq	%r12, %r12
	leaq	0(%rbp,%r12), %r10
	leaq	(%r12,%rbp), %rdx
	movzbl	1(%r10), %r11d
	movzbl	(%rdx), %r12d
	movl	$0, %r10d
	shrdq	$2, %r8, %rsi
	movl	%ecx, %r8d
	subl	$2, %ecx
	movb	%r11b, 131(%rsp,%r8)
	movl	$9999, %r8d
	movb	%r12b, 131(%rsp,%r9)
	shrq	$2, %rdi
	cmpq	%r13, %r8
	sbbq	%rax, %r10
	jc	.L3234
	movl	$999, %ecx
	movl	$0, %r14d
	movl	64(%rsp), %r9d
	cmpq	%r13, %rcx
	sbbq	%rax, %r14
	jc	.L3233
	vzeroupper
.L3226:
	addl	$48, %esi
.L3235:
	leaq	131(%rsp), %rbp
	leaq	131(%rsp,%r9), %rdi
	movb	%sil, 131(%rsp)
	jmp	.L3402
	.p2align 4
	.p2align 3
.L3412:
	movq	%rsi, %r8
	andl	$15, %r8d
	addq	%rdx, %r8
	movzbl	(%r8), %ebp
	shrdq	$4, %rdi, %rsi
	movzbl	(%rsi,%rdx), %esi
	movb	%bpl, 132(%rsp)
	jmp	.L3250
	.p2align 4
	.p2align 3
.L3415:
	movq	%rsi, %r15
	andl	$7, %r15d
	shrdq	$3, %rdi, %rsi
	addl	$48, %r15d
	addl	$48, %esi
	movb	%r15b, 132(%rsp)
	jmp	.L3241
	.p2align 4
	.p2align 3
.L3270:
	leaq	132(%rsp), %rdi
	leaq	.LC141(%rip), %r14
	leaq	131(%rsp), %rbp
.L3243:
	movq	%rdi, %r9
	movq	%rbp, %r12
	subq	%rbp, %r9
	andl	$7, %r9d
	je	.L3251
	cmpq	$1, %r9
	je	.L3354
	cmpq	$2, %r9
	je	.L3355
	cmpq	$3, %r9
	je	.L3356
	cmpq	$4, %r9
	je	.L3357
	cmpq	$5, %r9
	je	.L3358
	cmpq	$6, %r9
	je	.L3359
	movsbl	0(%rbp), %ecx
	leaq	132(%rsp), %r12
	call	toupper
	movb	%al, 0(%rbp)
.L3359:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3358:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3357:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3356:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3355:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3354:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rdi, %r12
	je	.L3395
.L3251:
	movsbl	(%r12), %ecx
	addq	$8, %r12
	call	toupper
	movsbl	-7(%r12), %ecx
	movb	%al, -8(%r12)
	call	toupper
	movsbl	-6(%r12), %ecx
	movb	%al, -7(%r12)
	call	toupper
	movsbl	-5(%r12), %ecx
	movb	%al, -6(%r12)
	call	toupper
	movsbl	-4(%r12), %ecx
	movb	%al, -5(%r12)
	call	toupper
	movsbl	-3(%r12), %ecx
	movb	%al, -4(%r12)
	call	toupper
	movsbl	-2(%r12), %ecx
	movb	%al, -3(%r12)
	call	toupper
	movsbl	-1(%r12), %ecx
	movb	%al, -2(%r12)
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rdi, %r12
	jne	.L3251
.L3395:
	movl	$2, %r15d
	jmp	.L3242
	.p2align 4
	.p2align 3
.L3410:
	movl	$131, %r11d
	lzcntq	%rdi, %r12
	movabsq	$3978425819141910832, %r15
	movabsq	$7378413942531504440, %rax
	subl	%r12d, %r11d
	movq	%r15, 272(%rsp)
	movq	%rax, 280(%rsp)
	shrl	$2, %r11d
	leal	-1(%r11), %r13d
	jmp	.L3246
	.p2align 4
	.p2align 3
.L3408:
	movl	$128, %r15d
	movl	$127, %ecx
	lzcntq	%rdi, %r13
	subl	%r13d, %r15d
	subl	%r13d, %ecx
	jmp	.L3220
.L3262:
	movl	$1, %r15d
	jmp	.L3221
	.p2align 4
	.p2align 3
.L3233:
	addq	%rsi, %rsi
	leaq	0(%rbp,%rsi), %r15
	addq	%rsi, %rbp
	movzbl	1(%r15), %r13d
	movzbl	0(%rbp), %esi
	movb	%r13b, 132(%rsp)
	vzeroupper
	jmp	.L3235
	.p2align 4
	.p2align 3
.L3409:
	movq	%rdx, %r10
	orq	%rcx, %r10
	jne	.L3267
	movzbl	(%rbx), %r11d
	movb	$48, 131(%rsp)
	testb	$16, %r11b
	je	.L3225
	movq	$-2, %rsi
	leaq	132(%rsp), %rdi
	leaq	.LC142(%rip), %r14
	movl	$2, %r15d
	leaq	131(%rsp), %rbp
	jmp	.L3223
	.p2align 4
	.p2align 3
.L3414:
	movl	$130, %r14d
	movl	$2863311531, %r15d
	lzcntq	%rdi, %r13
	subl	%r13d, %r14d
	imulq	%r15, %r14
	shrq	$33, %r14
	leal	-1(%r14), %r12d
	jmp	.L3237
	.p2align 4
	.p2align 3
.L3416:
	movl	%edi, %r9d
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	576(%rsp), %rbx
	addl	$2, %r9d
	jmp	.L3228
	.p2align 4
	.p2align 3
.L3417:
	movl	%edi, %r9d
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	576(%rsp), %rbx
	addl	$3, %r9d
	jmp	.L3228
	.p2align 4
	.p2align 3
.L3418:
	movl	%edi, %r9d
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	576(%rsp), %rbx
	jmp	.L3228
.L3269:
	leaq	.LC141(%rip), %r14
	jmp	.L3210
.L3411:
	leaq	272(%rsp), %rdx
	jmp	.L3247
.L3266:
	leaq	259(%rsp), %rdi
	leaq	131(%rsp), %rbp
	jmp	.L3402
.L3267:
	leaq	.LC142(%rip), %r14
	jmp	.L3210
.L3264:
	movl	$1, %r9d
	jmp	.L3226
.L3203:
	leaq	.LC144(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section	.text$_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15546:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$472, %rsp
	.seh_stackalloc	472
	.seh_endprologue
	movzbl	1(%rcx), %eax
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdi
	movq	%rcx, %rbx
	movq	%r8, %rbp
	movl	%eax, %r9d
	andl	$120, %r9d
	cmpb	$56, %r9b
	je	.L3612
	shrb	$3, %al
	andl	$15, %eax
	cmpb	$4, %al
	je	.L3423
	ja	.L3424
	cmpb	$1, %al
	ja	.L3613
	movq	%rsi, %r14
	orq	%rdi, %r14
	jne	.L3437
	leaq	116(%rsp), %rsi
	leaq	115(%rsp), %rdi
	movb	$48, 115(%rsp)
.L3438:
	movzbl	(%rbx), %eax
	movq	%rdi, %rdx
.L3436:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L3614
.L3469:
	cmpl	$3, %eax
	je	.L3483
.L3470:
	subq	%rdx, %rdi
	leaq	96(%rsp), %rax
	subq	%rdx, %rsi
	movq	%rdx, 104(%rsp)
	movq	%rbp, %r9
	movq	%rdi, %r8
	movq	%rax, %rdx
	movq	%rbx, %rcx
	movq	%rsi, 96(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L3422
	.p2align 4
	.p2align 3
.L3612:
	movl	$127, %r9d
	movl	$0, %r10d
	cmpq	%rsi, %r9
	sbbq	%rdi, %r10
	jc	.L3421
	leaq	256(%rsp), %rcx
	movq	%rbx, %r8
	movq	%rbp, %rdx
	movb	%sil, 256(%rsp)
	movq	%rcx, 104(%rsp)
	leaq	96(%rsp), %rcx
	movq	$1, 96(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
.L3422:
	addq	$472, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L3613:
	cmpb	$16, %r9b
	leaq	.LC139(%rip), %r14
	leaq	.LC140(%rip), %r11
	movq	%rsi, %rdx
	cmovne	%r11, %r14
	orq	%rdi, %rdx
	je	.L3472
	testq	%rdi, %rdi
	jne	.L3615
	lzcntq	%rsi, %r13
	movl	$128, %r10d
	addl	$64, %r13d
	movl	$127, %r15d
	subl	%r13d, %r10d
	subl	%r13d, %r15d
	je	.L3473
.L3432:
	movl	%r15d, %r9d
	decl	%r15d
	leaq	112(%rsp,%r9), %rax
	subq	%r15, %r9
	leaq	111(%rsp,%r9), %r12
	movq	%rax, %r11
	subq	%r12, %r11
	andl	$7, %r11d
	je	.L3608
	cmpq	$1, %r11
	je	.L3556
	cmpq	$2, %r11
	je	.L3557
	cmpq	$3, %r11
	je	.L3558
	cmpq	$4, %r11
	je	.L3559
	cmpq	$5, %r11
	je	.L3560
	cmpq	$6, %r11
	je	.L3561
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rax)
	decq	%rax
.L3561:
	movl	%esi, %ecx
	andl	$1, %ecx
	addl	$48, %ecx
	shrq	%rdi
	rcrq	%rsi
	movb	%cl, 3(%rax)
	decq	%rax
.L3560:
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rax)
	decq	%rax
.L3559:
	movl	%esi, %ecx
	andl	$1, %ecx
	addl	$48, %ecx
	shrq	%rdi
	rcrq	%rsi
	movb	%cl, 3(%rax)
	decq	%rax
.L3558:
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rax)
	decq	%rax
.L3557:
	movl	%esi, %ecx
	andl	$1, %ecx
	addl	$48, %ecx
	shrq	%rdi
	rcrq	%rsi
	movb	%cl, 3(%rax)
	decq	%rax
.L3556:
	movl	%esi, %edx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%rdi
	rcrq	%rsi
	movb	%dl, 3(%rax)
	decq	%rax
	cmpq	%rax, %r12
	je	.L3433
.L3608:
	movq	%rbx, %r13
.L3434:
	movl	%esi, %ebx
	movq	%rdi, %r8
	movq	%rsi, %r15
	movq	%rsi, %r9
	andl	$1, %ebx
	movq	%rsi, %rcx
	movq	%rsi, %r11
	movq	%rsi, %rdx
	addl	$48, %ebx
	shrq	%r8
	movq	%rsi, %r8
	rcrq	%r15
	movb	%bl, 3(%rax)
	subq	$8, %rax
	andl	$1, %r15d
	addl	$48, %r15d
	movb	%r15b, 10(%rax)
	movq	%rsi, %r15
	shrdq	$2, %rdi, %r9
	shrdq	$3, %rdi, %rcx
	shrdq	$4, %rdi, %r15
	shrdq	$5, %rdi, %r8
	shrdq	$6, %rdi, %r11
	shrdq	$7, %rdi, %rdx
	andl	$1, %r9d
	andl	$1, %ecx
	andl	$1, %r15d
	andl	$1, %r8d
	andl	$1, %r11d
	andl	$1, %edx
	addl	$48, %r9d
	addl	$48, %ecx
	addl	$48, %r15d
	addl	$48, %r8d
	addl	$48, %r11d
	addl	$48, %edx
	shrdq	$8, %rdi, %rsi
	movb	%r9b, 9(%rax)
	movb	%cl, 8(%rax)
	movb	%r15b, 7(%rax)
	movb	%r8b, 6(%rax)
	movb	%r11b, 5(%rax)
	movb	%dl, 4(%rax)
	shrq	$8, %rdi
	cmpq	%rax, %r12
	jne	.L3434
	movq	%r13, %rbx
.L3433:
	movslq	%r10d, %rsi
	leaq	115(%rsp), %rdi
	movl	$49, %r10d
	leaq	115(%rsp,%rsi), %rsi
	jmp	.L3430
	.p2align 4
	.p2align 3
.L3423:
	movq	%rsi, %rdx
	orq	%rdi, %rdx
	je	.L3448
	testq	%rdi, %rdi
	jne	.L3616
	movl	$66, %r12d
	movl	$2863311531, %r10d
	lzcntq	%rsi, %r8
	movl	$63, %r11d
	subl	%r8d, %r12d
	imulq	%r10, %r12
	shrq	$33, %r12
	leal	-1(%r12), %r13d
	cmpq	%rsi, %r11
	jnb	.L3452
.L3451:
	movl	$63, %eax
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	.p2align 4
	.p2align 3
.L3453:
	movq	%rsi, %r8
	movq	%rsi, %rcx
	movl	%r13d, %ebx
	leal	-1(%r13), %r11d
	shrdq	$3, %rdi, %r8
	shrdq	$6, %rdi, %rsi
	andl	$7, %ecx
	andl	$7, %r8d
	subl	$2, %r13d
	movq	%r9, %r14
	shrq	$6, %rdi
	addl	$48, %ecx
	addl	$48, %r8d
	cmpq	%rsi, %rax
	sbbq	%rdi, %r14
	movb	%cl, 115(%rsp,%rbx)
	movb	%r8b, 115(%rsp,%r11)
	jc	.L3453
	movq	%rdx, %rbx
.L3452:
	movl	$7, %r13d
	movl	$0, %r15d
	cmpq	%rsi, %r13
	sbbq	%rdi, %r15
	jc	.L3617
	addl	$48, %esi
.L3455:
	movb	%sil, 115(%rsp)
	movl	%r12d, %esi
	leaq	115(%rsp), %rdi
	movq	$-1, %r12
	leaq	115(%rsp,%rsi), %rsi
	leaq	.LC143(%rip), %r14
	movl	$1, %r13d
.L3456:
	movzbl	(%rbx), %eax
	movq	%rdi, %rdx
	testb	$16, %al
	jne	.L3435
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.L3469
	.p2align 4
	.p2align 3
.L3614:
	movl	$43, %r13d
.L3471:
	movb	%r13b, -1(%rdx)
	decq	%rdx
	jmp	.L3470
	.p2align 4
	.p2align 3
.L3424:
	cmpb	$40, %r9b
	je	.L3618
	movq	%rsi, %r12
	orq	%rdi, %r12
	jne	.L3479
	movb	$48, 115(%rsp)
	cmpb	$48, %r9b
	je	.L3480
	leaq	.LC141(%rip), %r14
	leaq	116(%rsp), %rsi
	leaq	115(%rsp), %rdi
	jmp	.L3458
	.p2align 4
	.p2align 3
.L3483:
	movl	$32, %r13d
	jmp	.L3471
	.p2align 4
	.p2align 3
.L3472:
	movl	$48, %r10d
	leaq	116(%rsp), %rsi
	leaq	115(%rsp), %rdi
.L3430:
	movzbl	(%rbx), %eax
	movb	%r10b, 115(%rsp)
	testb	$16, %al
	je	.L3610
.L3481:
	movq	$-2, %r12
	movl	$2, %r13d
.L3435:
	leaq	(%rdi,%r12), %rdx
	movl	%r13d, %r9d
	testl	%r13d, %r13d
	je	.L3436
	xorl	%r11d, %r11d
	leal	-1(%r13), %r8d
	movl	$1, %r12d
	movzbl	(%r14,%r11), %ecx
	andl	$7, %r8d
	movb	%cl, (%rdx,%r11)
	cmpl	%r13d, %r12d
	jnb	.L3436
	testl	%r8d, %r8d
	je	.L3467
	cmpl	$1, %r8d
	je	.L3569
	cmpl	$2, %r8d
	je	.L3570
	cmpl	$3, %r8d
	je	.L3571
	cmpl	$4, %r8d
	je	.L3572
	cmpl	$5, %r8d
	je	.L3573
	cmpl	$6, %r8d
	je	.L3574
	movl	$1, %r15d
	movl	$2, %r12d
	movzbl	(%r14,%r15), %r10d
	movb	%r10b, (%rdx,%r15)
.L3574:
	movl	%r12d, %r13d
	incl	%r12d
	movzbl	(%r14,%r13), %r8d
	movb	%r8b, (%rdx,%r13)
.L3573:
	movl	%r12d, %r11d
	incl	%r12d
	movzbl	(%r14,%r11), %ecx
	movb	%cl, (%rdx,%r11)
.L3572:
	movl	%r12d, %r15d
	incl	%r12d
	movzbl	(%r14,%r15), %r10d
	movb	%r10b, (%rdx,%r15)
.L3571:
	movl	%r12d, %r13d
	incl	%r12d
	movzbl	(%r14,%r13), %r8d
	movb	%r8b, (%rdx,%r13)
.L3570:
	movl	%r12d, %r11d
	incl	%r12d
	movzbl	(%r14,%r11), %ecx
	movb	%cl, (%rdx,%r11)
.L3569:
	movl	%r12d, %r15d
	incl	%r12d
	movzbl	(%r14,%r15), %r10d
	movb	%r10b, (%rdx,%r15)
	cmpl	%r9d, %r12d
	jnb	.L3436
.L3467:
	movl	%r12d, %r13d
	leal	1(%r12), %r11d
	leal	2(%r12), %r15d
	movzbl	(%r14,%r13), %r8d
	movzbl	(%r14,%r11), %ecx
	movzbl	(%r14,%r15), %r10d
	movb	%r8b, (%rdx,%r13)
	movb	%cl, (%rdx,%r11)
	leal	3(%r12), %r13d
	leal	4(%r12), %r11d
	movzbl	(%r14,%r13), %r8d
	movzbl	(%r14,%r11), %ecx
	movb	%r10b, (%rdx,%r15)
	leal	5(%r12), %r15d
	movzbl	(%r14,%r15), %r10d
	movb	%r8b, (%rdx,%r13)
	movb	%cl, (%rdx,%r11)
	leal	6(%r12), %r13d
	leal	7(%r12), %r11d
	movzbl	(%r14,%r13), %r8d
	movzbl	(%r14,%r11), %ecx
	addl	$8, %r12d
	movb	%r10b, (%rdx,%r15)
	movb	%r8b, (%rdx,%r13)
	movb	%cl, (%rdx,%r11)
	cmpl	%r9d, %r12d
	jb	.L3467
	jmp	.L3436
	.p2align 4
	.p2align 3
.L3479:
	leaq	.LC141(%rip), %r14
.L3457:
	testq	%rdi, %rdi
	jne	.L3619
	movl	$67, %r11d
	lzcntq	%rsi, %r8
	movabsq	$3978425819141910832, %r10
	movabsq	$7378413942531504440, %r13
	subl	%r8d, %r11d
	movl	$255, %eax
	movq	%r10, 256(%rsp)
	movq	%r13, 264(%rsp)
	shrl	$2, %r11d
	leal	-1(%r11), %r15d
	cmpq	%rsi, %rax
	jnb	.L3620
.L3461:
	leaq	256(%rsp), %rdx
	movl	$255, %r10d
	xorl	%r8d, %r8d
	movq	%rbx, %rax
	.p2align 4
	.p2align 3
.L3463:
	movq	%rsi, %rcx
	movq	%rsi, %r13
	movl	%r15d, %ebx
	shrdq	$4, %rdi, %rcx
	andl	$15, %r13d
	andl	$15, %ecx
	addq	%rdx, %r13
	movq	%rcx, 32(%rsp)
	movq	32(%rsp), %rcx
	movzbl	0(%r13), %r12d
	xorl	%r13d, %r13d
	movq	%r13, 40(%rsp)
	movq	%r8, %r13
	shrdq	$8, %rdi, %rsi
	addq	%rdx, %rcx
	movb	%r12b, 115(%rsp,%rbx)
	movzbl	(%rcx), %ebx
	leal	-1(%r15), %r12d
	subl	$2, %r15d
	shrq	$8, %rdi
	cmpq	%rsi, %r10
	sbbq	%rdi, %r13
	movb	%bl, 115(%rsp,%r12)
	jc	.L3463
	movq	%rax, %rbx
.L3462:
	movl	$15, %r15d
	movl	$0, %r10d
	cmpq	%rsi, %r15
	sbbq	%rdi, %r10
	jc	.L3621
	addq	%rsi, %rdx
	movzbl	(%rdx), %edi
.L3465:
	movb	%dil, 115(%rsp)
	leaq	115(%rsp,%r11), %rsi
	leaq	115(%rsp), %rdi
	cmpb	$48, %r9b
	je	.L3459
.L3458:
	movzbl	(%rbx), %eax
	testb	$16, %al
	jne	.L3481
.L3610:
	movq	%rdi, %rdx
	jmp	.L3436
	.p2align 4
	.p2align 3
.L3448:
	leaq	115(%rsp), %rdi
	movzbl	(%rcx), %eax
	leaq	116(%rsp), %rsi
	movb	$48, 115(%rsp)
	movq	%rdi, %rdx
	jmp	.L3436
	.p2align 4
	.p2align 3
.L3437:
	movl	$9, %edx
	movl	$0, %r9d
	cmpq	%rsi, %rdx
	sbbq	%rdi, %r9
	jnc	.L3475
	movq	%rdi, %r10
	movl	$1, %r8d
	leaq	64(%rsp), %rax
	movq	%rdi, 56(%rsp)
	movq	%rbp, 560(%rsp)
	movq	%rsi, %r13
	movl	$99, %r12d
	xorl	%r15d, %r15d
	leaq	80(%rsp), %r14
	movl	%r8d, %edi
	movq	%r10, %rbp
	movq	%rax, %rbx
	movq	%rcx, 544(%rsp)
	movq	%rsi, 48(%rsp)
	jmp	.L3444
	.p2align 4
	.p2align 3
.L3440:
	movl	$999, %r11d
	movq	%r15, %rcx
	cmpq	%r13, %r11
	sbbq	%rbp, %rcx
	jnc	.L3622
	movl	$9999, %edx
	movq	%r15, %r9
	cmpq	%r13, %rdx
	sbbq	%rbp, %r9
	jnc	.L3623
	movq	%rbx, %rdx
	movq	%r14, %rcx
	movq	%r13, 80(%rsp)
	movq	%rbp, 88(%rsp)
	movq	$10000, 64(%rsp)
	movq	$0, 72(%rsp)
	call	__udivti3
	movl	$99999, %eax
	addl	$4, %edi
	cmpq	%r13, %rax
	movq	%r15, %r13
	vmovdqa	%xmm0, 32(%rsp)
	sbbq	%rbp, %r13
	movq	32(%rsp), %r10
	movq	40(%rsp), %r8
	jnc	.L3624
	movq	%r10, %r13
	movq	%r8, %rbp
.L3444:
	cmpq	%r13, %r12
	movq	%r15, %rsi
	sbbq	%rbp, %rsi
	jc	.L3440
	movl	%edi, %r8d
	movq	544(%rsp), %rbx
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	560(%rsp), %rbp
	incl	%r8d
.L3441:
	cmpl	$128, %r8d
	ja	.L3477
	vmovdqa64	.LC24(%rip), %zmm0
	vmovdqa64	.LC25(%rip), %zmm1
	movl	$99, %edx
	movl	$0, %r9d
	cmpq	%rsi, %rdx
	movabsq	$4122263930388298034, %r11
	movabsq	$16106987313379638, %rcx
	leal	-1(%r8), %r12d
	vmovdqa64	.LC26(%rip), %zmm2
	sbbq	%rdi, %r9
	leaq	256(%rsp), %r13
	movq	%rcx, 449(%rsp)
	vmovdqu64	%zmm0, 256(%rsp)
	vmovdqu64	%zmm1, 320(%rsp)
	vmovdqu64	%zmm2, 384(%rsp)
	movq	%r11, 441(%rsp)
	jnc	.L3445
	movabsq	$2951479051793528258, %r10
	movabsq	$1152921504606846975, %r14
	movabsq	$-8116567392432202711, %r15
	movl	%r8d, 48(%rsp)
	vmovq	%r10, %xmm3
	movq	%rbx, 544(%rsp)
	.p2align 4
	.p2align 3
.L3446:
	movq	%rsi, %r11
	movq	%rsi, %r8
	movq	%rdi, %rbx
	movabsq	$5165088340638674453, %rax
	shrdq	$60, %rdi, %r11
	shrq	$56, %rbx
	andq	%r14, %r8
	andq	%r14, %r11
	addq	%r8, %r11
	vmovq	%xmm3, %r8
	addq	%rbx, %r11
	xorl	%ebx, %ebx
	mulq	%r11
	movq	%r11, %rcx
	movq	%rsi, %rax
	movq	%rbx, 40(%rsp)
	subq	%rdx, %rcx
	shrq	%rcx
	addq	%rcx, %rdx
	movq	%rdi, %rcx
	shrq	$4, %rdx
	leaq	(%rdx,%rdx,4), %r9
	leaq	(%r9,%r9,4), %r10
	subq	%r10, %r11
	subq	%r11, %rax
	movq	%r11, 32(%rsp)
	sbbq	%rbx, %rcx
	imulq	%rax, %r8
	movq	%rax, %rdx
	movq	%rdi, %rax
	imulq	%r15, %rcx
	mulx	%r15, %rbx, %r9
	movq	%rbx, %r10
	movl	$25, %edx
	andl	$3, %r10d
	addq	%r8, %rcx
	addq	%r9, %rcx
	movq	%rcx, %r8
	mulx	%r10, %rcx, %r11
	addq	32(%rsp), %rcx
	movq	%rsi, %r11
	movq	%rbx, %rsi
	movq	%r8, %rdi
	movl	%r12d, %ebx
	leal	-1(%r12), %r10d
	addq	%rcx, %rcx
	subl	$2, %r12d
	leaq	0(%r13,%rcx), %r9
	addq	%r13, %rcx
	shrdq	$2, %r8, %rsi
	movzbl	1(%r9), %r8d
	movzbl	(%rcx), %edx
	movl	$9999, %ecx
	shrq	$2, %rdi
	cmpq	%r11, %rcx
	movb	%r8b, 115(%rsp,%rbx)
	movl	$0, %ebx
	movb	%dl, 115(%rsp,%r10)
	sbbq	%rax, %rbx
	jc	.L3446
	movl	$999, %r12d
	movl	$0, %r14d
	movl	48(%rsp), %r8d
	movq	544(%rsp), %rbx
	cmpq	%r11, %r12
	sbbq	%rax, %r14
	jc	.L3445
	vzeroupper
.L3439:
	addl	$48, %esi
.L3447:
	movb	%sil, 115(%rsp)
	leaq	115(%rsp), %rdi
	leaq	115(%rsp,%r8), %rsi
	jmp	.L3438
	.p2align 4
	.p2align 3
.L3618:
	movq	%rsi, %rdx
	orq	%rdi, %rdx
	jne	.L3478
	leaq	.LC142(%rip), %r14
	leaq	116(%rsp), %rsi
	leaq	115(%rsp), %rdi
	movb	$48, 115(%rsp)
	jmp	.L3458
	.p2align 4
	.p2align 3
.L3617:
	movq	%rsi, %rax
	andl	$7, %eax
	shrdq	$3, %rdi, %rsi
	addl	$48, %eax
	addl	$48, %esi
	movb	%al, 116(%rsp)
	jmp	.L3455
	.p2align 4
	.p2align 3
.L3621:
	movq	%rsi, %r8
	andl	$15, %r8d
	addq	%rdx, %r8
	movzbl	(%r8), %eax
	shrdq	$4, %rdi, %rsi
	movzbl	(%rsi,%rdx), %edi
	movb	%al, 116(%rsp)
	jmp	.L3465
	.p2align 4
	.p2align 3
.L3478:
	leaq	.LC142(%rip), %r14
	jmp	.L3457
	.p2align 4
	.p2align 3
.L3480:
	leaq	116(%rsp), %rsi
	leaq	.LC141(%rip), %r14
	leaq	115(%rsp), %rdi
.L3459:
	movq	%rsi, %r9
	movq	%rdi, %r12
	subq	%rdi, %r9
	andl	$7, %r9d
	je	.L3466
	cmpq	$1, %r9
	je	.L3562
	cmpq	$2, %r9
	je	.L3563
	cmpq	$3, %r9
	je	.L3564
	cmpq	$4, %r9
	je	.L3565
	cmpq	$5, %r9
	je	.L3566
	cmpq	$6, %r9
	je	.L3567
	movsbl	(%rdi), %ecx
	leaq	116(%rsp), %r12
	call	toupper
	movb	%al, (%rdi)
.L3567:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3566:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3565:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3564:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3563:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
.L3562:
	movsbl	(%r12), %ecx
	incq	%r12
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rsi, %r12
	je	.L3603
.L3466:
	movsbl	(%r12), %ecx
	addq	$8, %r12
	call	toupper
	movsbl	-7(%r12), %ecx
	movb	%al, -8(%r12)
	call	toupper
	movsbl	-6(%r12), %ecx
	movb	%al, -7(%r12)
	call	toupper
	movsbl	-5(%r12), %ecx
	movb	%al, -6(%r12)
	call	toupper
	movsbl	-4(%r12), %ecx
	movb	%al, -5(%r12)
	call	toupper
	movsbl	-3(%r12), %ecx
	movb	%al, -4(%r12)
	call	toupper
	movsbl	-2(%r12), %ecx
	movb	%al, -3(%r12)
	call	toupper
	movsbl	-1(%r12), %ecx
	movb	%al, -2(%r12)
	call	toupper
	movb	%al, -1(%r12)
	cmpq	%rsi, %r12
	jne	.L3466
.L3603:
	movq	$-2, %r12
	movl	$2, %r13d
	jmp	.L3456
	.p2align 4
	.p2align 3
.L3619:
	movl	$131, %r11d
	lzcntq	%rdi, %r12
	movabsq	$3978425819141910832, %rcx
	movabsq	$7378413942531504440, %rdx
	subl	%r12d, %r11d
	movq	%rcx, 256(%rsp)
	movq	%rdx, 264(%rsp)
	shrl	$2, %r11d
	leal	-1(%r11), %r15d
	jmp	.L3461
	.p2align 4
	.p2align 3
.L3615:
	movl	$128, %r10d
	movl	$127, %r15d
	lzcntq	%rdi, %r8
	subl	%r8d, %r10d
	subl	%r8d, %r15d
	jmp	.L3432
.L3473:
	movl	$1, %r10d
	jmp	.L3433
	.p2align 4
	.p2align 3
.L3445:
	addq	%rsi, %rsi
	leaq	0(%r13,%rsi), %r15
	addq	%rsi, %r13
	movzbl	1(%r15), %r11d
	movzbl	0(%r13), %esi
	movb	%r11b, 116(%rsp)
	vzeroupper
	jmp	.L3447
	.p2align 4
	.p2align 3
.L3616:
	movl	$130, %r12d
	movl	$2863311531, %r15d
	lzcntq	%rdi, %r14
	subl	%r14d, %r12d
	imulq	%r15, %r12
	shrq	$33, %r12
	leal	-1(%r12), %r13d
	jmp	.L3451
	.p2align 4
	.p2align 3
.L3622:
	movl	%edi, %r8d
	movq	544(%rsp), %rbx
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	560(%rsp), %rbp
	addl	$2, %r8d
	jmp	.L3441
	.p2align 4
	.p2align 3
.L3623:
	movl	%edi, %r8d
	movq	544(%rsp), %rbx
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	560(%rsp), %rbp
	addl	$3, %r8d
	jmp	.L3441
	.p2align 4
	.p2align 3
.L3624:
	movl	%edi, %r8d
	movq	544(%rsp), %rbx
	movq	48(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	560(%rsp), %rbp
	jmp	.L3441
.L3620:
	leaq	256(%rsp), %rdx
	jmp	.L3462
.L3477:
	leaq	243(%rsp), %rsi
	leaq	115(%rsp), %rdi
	jmp	.L3438
.L3475:
	movl	$1, %r8d
	jmp	.L3439
.L3421:
	leaq	.LC144(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
	nop
	.seh_endproc
	.section .rdata,"dr"
.LC145:
	.ascii "basic_string_view::copy\0"
	.align 8
.LC146:
	.ascii "%s: __pos (which is %zu) > __size (which is %zu)\0"
	.text
	.align 2
	.p2align 4
	.def	_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0;	.scl	3;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0
_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0:
.LFB15945:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$136, %rsp
	.seh_stackalloc	136
	.seh_endprologue
	leaq	16(%rcx), %rax
	movq	(%rdx), %rbp
	movq	8(%rdx), %r12
	movq	%rcx, %rbx
	movl	%r8d, %edi
	movq	%r9, %rsi
	movq	%rax, (%rcx)
	movq	$0, 8(%rcx)
	movb	$0, 16(%rcx)
.LEHB46:
	call	_ZNSt6locale7classicEv
	movq	%rax, %rdx
	movq	%rsi, %rcx
	call	_ZNKSt6localeeqERKS_
	testb	%al, %al
	je	.L3672
.L3625:
	movq	%rbx, %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L3672:
	movq	.refptr._ZNSt7__cxx118numpunctIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	(%rsi), %rcx
	movq	8(%rcx), %r8
	movq	(%r8,%rax,8), %r13
	testq	%r13, %r13
	je	.L3627
	movq	0(%r13), %r14
	movq	%r13, %rcx
	call	*16(%r14)
	movq	0(%r13), %r9
	leaq	96(%rsp), %rcx
	movq	%r13, %rdx
	movb	%al, 63(%rsp)
	movq	%rcx, 88(%rsp)
	call	*32(%r9)
.LEHE46:
	cmpq	$0, 104(%rsp)
	jne	.L3652
	cmpb	$46, 63(%rsp)
	jne	.L3652
	movq	96(%rsp), %rcx
	leaq	112(%rsp), %r10
	cmpq	%r10, %rcx
	jne	.L3640
	jmp	.L3625
	.p2align 4
	.p2align 3
.L3652:
	testq	%rbp, %rbp
	je	.L3646
	movq	%rbp, %r8
	movl	$46, %edx
	movq	%r12, %rcx
	call	memchr
	movsbl	%dil, %edx
	movq	%rbp, %r8
	movq	%r12, %rcx
	movq	%rax, %r15
	call	memchr
	testq	%r15, %r15
	je	.L3632
	movq	%r15, %rdi
	subq	%r12, %rdi
	testq	%rax, %rax
	je	.L3647
.L3645:
	subq	%r12, %rax
	cmpq	%rdi, %rax
	cmova	%rdi, %rax
	movq	%rax, %rsi
.L3633:
	cmpq	$-1, %rsi
	je	.L3631
	movq	%rbp, %r15
	subq	%rsi, %r15
.L3634:
	leaq	(%r15,%rsi,2), %rdx
	movq	%rbx, %rcx
.LEHB47:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
.LEHE47:
	movq	104(%rsp), %rax
	movq	96(%rsp), %rdx
	leaq	(%r12,%rsi), %r11
	movq	%r13, %rcx
	movq	(%rbx), %r14
	movq	%r11, 80(%rsp)
	movq	%rax, 72(%rsp)
	movq	0(%r13), %rax
	movq	%rdx, 64(%rsp)
.LEHB48:
	call	*24(%rax)
	movq	80(%rsp), %r8
	movq	72(%rsp), %r9
	movq	%r14, %rcx
	movsbl	%al, %edx
	movq	%r12, 32(%rsp)
	movq	%r8, 40(%rsp)
	movq	64(%rsp), %r8
	call	_ZSt14__add_groupingIcEPT_S1_S0_PKcyPKS0_S5_
	movq	%rax, %rcx
	testq	%r15, %r15
	je	.L3635
	cmpq	$-1, %rdi
	je	.L3636
	movzbl	63(%rsp), %r13d
	incq	%rcx
	incq	%rsi
	movb	%r13b, (%rax)
.L3636:
	cmpq	$1, %r15
	jne	.L3673
.L3635:
	movq	(%rbx), %rbp
	subq	%r14, %rcx
	leaq	112(%rsp), %r12
	movq	%rcx, 8(%rbx)
	movb	$0, 0(%rbp,%rcx)
	movq	96(%rsp), %rcx
	cmpq	%r12, %rcx
	je	.L3625
.L3640:
	movq	112(%rsp), %r9
	leaq	1(%r9), %rdx
	call	_ZdlPvy
	jmp	.L3625
	.p2align 4
	.p2align 3
.L3646:
	movq	$-1, %rdi
.L3631:
	movq	%rbp, %rsi
	xorl	%r15d, %r15d
	jmp	.L3634
	.p2align 4
	.p2align 3
.L3673:
	cmpq	%rsi, %rbp
	jb	.L3674
	subq	%rsi, %rbp
	jne	.L3675
.L3638:
	addq	%rbp, %rcx
	jmp	.L3635
	.p2align 4
	.p2align 3
.L3675:
	leaq	(%r12,%rsi), %rdx
	movq	%rbp, %r8
	call	memcpy
	movq	%rax, %rcx
	jmp	.L3638
	.p2align 4
	.p2align 3
.L3632:
	movq	$-1, %rdi
	testq	%rax, %rax
	jne	.L3645
	jmp	.L3631
	.p2align 4
	.p2align 3
.L3647:
	movq	%rdi, %rsi
	jmp	.L3633
.L3674:
	movq	%rbp, %r9
	movq	%rsi, %r8
	leaq	.LC145(%rip), %rdx
	leaq	.LC146(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.LEHE48:
.L3651:
	movq	(%rbx), %r10
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rcx, 8(%rbx)
	movb	$0, (%r10)
.L3642:
	movq	88(%rsp), %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L3644:
	movq	%rbx, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rdi, %rcx
.LEHB49:
	call	_Unwind_Resume
.LEHE49:
.L3650:
	movq	%rax, %rdi
	jmp	.L3642
.L3649:
	movq	%rax, %rdi
	vzeroupper
	jmp	.L3644
.L3627:
.LEHB50:
	call	_ZSt16__throw_bad_castv
	nop
.LEHE50:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15945:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15945-.LLSDACSB15945
.LLSDACSB15945:
	.uleb128 .LEHB46-.LFB15945
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L3649-.LFB15945
	.uleb128 0
	.uleb128 .LEHB47-.LFB15945
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L3650-.LFB15945
	.uleb128 0
	.uleb128 .LEHB48-.LFB15945
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L3651-.LFB15945
	.uleb128 0
	.uleb128 .LEHB49-.LFB15945
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB50-.LFB15945
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L3649-.LFB15945
	.uleb128 0
.LLSDACSE15945:
	.text
	.seh_endproc
	.section .rdata,"dr"
.LC148:
	.ascii "basic_string_view::substr\0"
.LC149:
	.ascii "basic_string::insert\0"
	.align 8
.LC150:
	.ascii "%s: __pos (which is %zu) > this->size() (which is %zu)\0"
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15540:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$440, %rsp
	.seh_stackalloc	440
	vmovaps	%xmm6, 416(%rsp)
	.seh_savexmm	%xmm6, 416
	.seh_endprologue
	fldt	(%rdx)
	movzbl	1(%rcx), %eax
	leaq	208(%rsp), %r15
	movq	%rcx, %rsi
	fstpt	48(%rsp)
	movq	%r8, %rbp
	movq	%r15, 192(%rsp)
	movq	$0, 200(%rsp)
	movb	$0, 208(%rsp)
	movl	%eax, %edx
	andl	$6, %edx
	je	.L3677
	cmpb	$2, %dl
	je	.L4036
	movq	$-1, %r14
	cmpb	$4, %dl
	je	.L4037
.L3679:
	movl	%eax, %ebx
	shrb	$3, %bl
	andl	$15, %ebx
	cmpb	$8, %bl
	ja	.L3683
	leaq	.L3704(%rip), %rcx
	movzbl	%bl, %r13d
	movslq	(%rcx,%r13,4), %r8
	addq	%rcx, %r8
	jmp	*%r8
	.section .rdata,"dr"
	.align 4
.L3704:
	.long	.L3712-.L3704
	.long	.L3711-.L3704
	.long	.L3710-.L3704
	.long	.L3709-.L3704
	.long	.L3708-.L3704
	.long	.L3707-.L3704
	.long	.L3706-.L3704
	.long	.L3705-.L3704
	.long	.L3703-.L3704
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L3677:
	movl	%eax, %ecx
	shrb	$3, %cl
	andl	$15, %ecx
	cmpb	$8, %cl
	ja	.L3683
	leaq	.L3718(%rip), %rdi
	movzbl	%cl, %ebx
	movslq	(%rdi,%rbx,4), %r8
	addq	%rdi, %r8
	jmp	*%r8
	.section .rdata,"dr"
	.align 4
.L3718:
	.long	.L3723-.L3718
	.long	.L3722-.L3718
	.long	.L3721-.L3718
	.long	.L3820-.L3718
	.long	.L3821-.L3718
	.long	.L3822-.L3718
	.long	.L3823-.L3718
	.long	.L3824-.L3718
	.long	.L3825-.L3718
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L3723:
	leaq	160(%rsp), %r14
	leaq	289(%rsp), %rbx
	fldt	48(%rsp)
	leaq	144(%rsp), %r9
	leaq	416(%rsp), %r8
	movq	%rbx, %rdx
	movq	%r14, %rcx
	fstpt	144(%rsp)
	movq	%r9, 64(%rsp)
	call	_ZSt8to_charsPcS_e
	cmpl	$132, 168(%rsp)
	movq	160(%rsp), %r12
	je	.L4038
	leaq	416(%rsp), %r13
	movl	$6, %r14d
	movb	$101, 72(%rsp)
	movb	$0, 64(%rsp)
	movq	%r13, 80(%rsp)
.L3729:
	fldt	48(%rsp)
	movzbl	(%rsi), %r13d
	fxam
	fnstsw	%ax
	ffreep	%st(0)
	testb	$2, %ah
	jne	.L4034
	movl	%r13d, %edx
	andl	$12, %edx
	cmpb	$4, %dl
	je	.L4039
	xorl	%ecx, %ecx
	cmpb	$12, %dl
	je	.L4040
.L3745:
	movq	%r12, %rdi
	subq	%rbx, %rdi
	testb	$16, %r13b
	je	.L3749
	fldt	48(%rsp)
	fabs
	fldt	.LC147(%rip)
	fucomip	%st(1), %st
	ffreep	%st(0)
	jb	.L3749
	testq	%rdi, %rdi
	je	.L3838
	movq	%rcx, 88(%rsp)
	movq	%rdi, %r8
	movl	$46, %edx
	movq	%rbx, %rcx
	call	memchr
	movq	88(%rsp), %r11
	testq	%rax, %rax
	je	.L3751
	subq	%rbx, %rax
	movq	%rax, 96(%rsp)
	cmpq	$-1, %rax
	je	.L3751
	leaq	1(%rax), %r10
	cmpq	%rdi, %r10
	jnb	.L4041
	movsbl	72(%rsp), %edx
	movq	%rdi, %r8
	leaq	(%rbx,%r10), %rcx
	movq	%r11, 104(%rsp)
	subq	%r10, %r8
	movq	%r10, 88(%rsp)
	call	memchr
	movq	88(%rsp), %r9
	movq	104(%rsp), %r11
	testq	%rax, %rax
	je	.L3841
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%rdi, %rax
	movq	%rax, %r10
.L3756:
	movq	96(%rsp), %rax
	cmpq	%rax, %r10
	sete	%dl
	cmpb	$0, 64(%rsp)
	movzbl	%dl, %ecx
	movq	%rcx, 88(%rsp)
	je	.L3843
	cmpb	$48, (%rbx,%r11)
	je	.L4042
.L3754:
	movq	%r10, %rax
	subq	%r11, %rax
	decq	%rax
.L3758:
	testq	%r14, %r14
	jne	.L3760
	.p2align 4
	.p2align 3
.L3753:
	cmpq	$0, 88(%rsp)
	jne	.L3759
	leaq	240(%rsp), %r14
	andl	$32, %r13d
	movq	$0, 232(%rsp)
	movb	$0, 240(%rsp)
	movq	%r14, 224(%rsp)
	je	.L3795
.L3817:
	cmpb	$0, 32(%rbp)
	leaq	24(%rbp), %r13
	je	.L4043
.L3780:
	leaq	184(%rsp), %r12
	movq	%r13, %rdx
	leaq	112(%rsp), %r13
	movq	%r12, %rcx
	call	_ZNSt6localeC1ERKS_
	movzbl	72(%rsp), %r8d
	leaq	256(%rsp), %rcx
	movq	%r12, %r9
	movq	%r13, %rdx
	movq	%rdi, 112(%rsp)
	movq	%rbx, 120(%rsp)
.LEHB51:
	call	_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0
.LEHE51:
	movq	224(%rsp), %r11
	movq	%r11, %r9
	cmpq	%r14, %r11
	je	.L4044
	movq	264(%rsp), %rdx
	movq	256(%rsp), %r8
	leaq	272(%rsp), %rcx
	movq	%rdx, %r10
	cmpq	%rcx, %r8
	je	.L3814
	vmovq	%rdx, %xmm1
	vpinsrq	$1, 272(%rsp), %xmm1, %xmm4
	movq	240(%rsp), %rax
	movq	%r8, 224(%rsp)
	vmovdqu	%xmm4, 232(%rsp)
	testq	%r11, %r11
	je	.L3784
	movq	%r11, 256(%rsp)
	movq	%rax, 272(%rsp)
.L3793:
	movq	$0, 264(%rsp)
	movb	$0, (%r11)
	movq	256(%rsp), %r10
	cmpq	%rcx, %r10
	je	.L3794
	movq	272(%rsp), %rdx
	movq	%r10, %rcx
	incq	%rdx
	call	_ZdlPvy
.L3794:
	movq	%r12, %rcx
	call	_ZNSt6localeD1Ev
	movq	232(%rsp), %rax
	testq	%rax, %rax
	je	.L3795
	movzwl	(%rsi), %edi
	movq	224(%rsp), %rcx
	andw	$384, %di
	cmpw	$128, %di
	je	.L4045
	cmpw	$256, %di
	je	.L3850
.L4035:
	movq	16(%rbp), %r12
	movq	%rcx, %rbx
	movq	%rax, %rdi
.L3798:
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%rdi, 112(%rsp)
	movq	%rbx, 120(%rsp)
.LEHB52:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L3804
	.p2align 4
	.p2align 3
.L3749:
	leaq	240(%rsp), %r14
	andl	$32, %r13d
	movq	$0, 232(%rsp)
	movb	$0, 240(%rsp)
	movq	%r14, 224(%rsp)
	je	.L3795
	fldt	48(%rsp)
	fabs
	fldt	.LC147(%rip)
	fucomip	%st(1), %st
	ffreep	%st(0)
	jnb	.L3817
.L3795:
	movzwl	(%rsi), %r13d
	andw	$384, %r13w
	cmpw	$128, %r13w
	je	.L4046
	movq	%rbx, %r12
	cmpw	$256, %r13w
	je	.L3799
.L3802:
	movq	16(%rbp), %r12
	testq	%rdi, %rdi
	jne	.L4047
.L3804:
	movq	224(%rsp), %rcx
	cmpq	%r14, %rcx
	je	.L3807
	movq	240(%rsp), %rbx
	leaq	1(%rbx), %rdx
	call	_ZdlPvy
.L3807:
	movq	192(%rsp), %rcx
	cmpq	%r15, %rcx
	je	.L3868
	movq	208(%rsp), %r14
	leaq	1(%r14), %rdx
	call	_ZdlPvy
	nop
.L3868:
	vmovaps	416(%rsp), %xmm6
	movq	%r12, %rax
	addq	$440, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L4040:
	movb	$32, -1(%rbx)
	movzbl	(%rsi), %r13d
	decq	%rbx
	.p2align 4
	.p2align 3
.L4034:
	movl	$1, %ecx
	jmp	.L3745
	.p2align 4
	.p2align 3
.L4046:
	movzwl	4(%rsi), %r9d
	movq	%rbx, %r12
.L3801:
	cmpq	%r9, %rdi
	jnb	.L3802
.L3797:
	movl	8(%rsi), %r11d
	movzbl	(%rsi), %esi
	subq	%rdi, %r9
	movq	16(%rbp), %rcx
	movl	%esi, %r8d
	andl	$3, %r8d
	jne	.L3805
	testb	$64, %sil
	je	.L3852
	fldt	48(%rsp)
	fabs
	fldt	.LC147(%rip)
	fucomip	%st(1), %st
	ffreep	%st(0)
	jb	.L3852
	movzbl	(%r12), %ebp
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rdx
	cmpb	$15, (%rdx,%rbp)
	jbe	.L3853
	movq	24(%rcx), %rax
	movzbl	(%rbx), %r10d
	leaq	1(%rax), %r8
	movq	%r8, 24(%rcx)
	movb	%r10b, (%rax)
	movq	24(%rcx), %r13
	subq	8(%rcx), %r13
	cmpq	16(%rcx), %r13
	je	.L4048
.L3806:
	incq	%rbx
	decq	%rdi
	movl	$2, %r8d
	movl	$48, %r11d
	.p2align 4
	.p2align 3
.L3805:
	leaq	112(%rsp), %rdx
	movq	%rdi, 112(%rsp)
	movq	%rbx, 120(%rsp)
	movl	%r11d, 32(%rsp)
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	movq	%rax, %r12
	jmp	.L3804
	.p2align 4
	.p2align 3
.L4036:
	movzwl	6(%rcx), %r14d
	jmp	.L3679
	.p2align 4
	.p2align 3
.L3838:
	xorl	%r10d, %r10d
.L3750:
	cmpb	$0, 64(%rsp)
	je	.L3846
	testq	%r14, %r14
	je	.L3847
	movq	%r10, %rax
	movq	%r10, 96(%rsp)
	movq	$1, 88(%rsp)
	subq	%rcx, %rax
.L3760:
	subq	%rax, %r14
	addq	%r14, 88(%rsp)
	jmp	.L3753
	.p2align 4
	.p2align 3
.L3706:
	movl	%r14d, %r8d
.L3719:
	movl	$1, %edi
.L3715:
	movl	$2, %ebx
	movb	$101, 72(%rsp)
	movb	$0, 64(%rsp)
.L3725:
	leaq	289(%rsp), %r13
	leaq	160(%rsp), %rcx
	fldt	48(%rsp)
	leaq	144(%rsp), %r9
	movl	%r8d, 40(%rsp)
	movq	%r13, %rdx
	leaq	416(%rsp), %r8
	vmovq	%rcx, %xmm6
	fstpt	144(%rsp)
	movl	%ebx, 32(%rsp)
	movq	%r9, 80(%rsp)
	call	_ZSt8to_charsPcS_eSt12chars_formati
	cmpl	$132, 168(%rsp)
	movq	160(%rsp), %r12
	movq	80(%rsp), %r10
	je	.L3816
	leaq	416(%rsp), %rcx
	movq	%r13, %rbx
	movq	%rcx, 80(%rsp)
.L3727:
	testb	%dil, %dil
	je	.L3729
	cmpq	%r12, %rbx
	je	.L3729
	movq	%r12, %rax
	movq	__imp_toupper(%rip), %rdi
	movq	%rbx, %r13
	subq	%rbx, %rax
	andl	$7, %eax
	je	.L3746
	cmpq	$1, %rax
	je	.L3954
	cmpq	$2, %rax
	je	.L3955
	cmpq	$3, %rax
	je	.L3956
	cmpq	$4, %rax
	je	.L3957
	cmpq	$5, %rax
	je	.L3958
	cmpq	$6, %rax
	je	.L3959
	movsbl	(%rbx), %ecx
	leaq	1(%rbx), %r13
	call	*%rdi
	movb	%al, (%rbx)
.L3959:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
.L3958:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
.L3957:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
.L3956:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
.L3955:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
.L3954:
	movsbl	0(%r13), %ecx
	incq	%r13
	call	*%rdi
	movb	%al, -1(%r13)
	cmpq	%r13, %r12
	je	.L3729
.L3746:
	movsbl	0(%r13), %ecx
	addq	$8, %r13
	call	*%rdi
	movsbl	-7(%r13), %ecx
	movb	%al, -8(%r13)
	call	*%rdi
	movsbl	-6(%r13), %ecx
	movb	%al, -7(%r13)
	call	*%rdi
	movsbl	-5(%r13), %ecx
	movb	%al, -6(%r13)
	call	*%rdi
	movsbl	-4(%r13), %ecx
	movb	%al, -5(%r13)
	call	*%rdi
	movsbl	-3(%r13), %ecx
	movb	%al, -4(%r13)
	call	*%rdi
	movsbl	-2(%r13), %ecx
	movb	%al, -3(%r13)
	call	*%rdi
	movsbl	-1(%r13), %ecx
	movb	%al, -2(%r13)
	call	*%rdi
	movb	%al, -1(%r13)
	cmpq	%r13, %r12
	jne	.L3746
	jmp	.L3729
	.p2align 4
	.p2align 3
.L3707:
	movl	%r14d, %r8d
	xorl	%edi, %edi
	jmp	.L3715
	.p2align 4
	.p2align 3
.L3708:
	movl	%r14d, %r8d
.L3720:
	movl	$1, %edi
	movb	$69, 72(%rsp)
.L3716:
	movl	$1, %ebx
	movb	$0, 64(%rsp)
	jmp	.L3725
	.p2align 4
	.p2align 3
.L3709:
	movl	%r14d, %r8d
	xorl	%edi, %edi
	movb	$101, 72(%rsp)
	jmp	.L3716
	.p2align 4
	.p2align 3
.L3710:
	andl	$120, %eax
	movl	$80, %edi
	cmpb	$16, %al
	movl	$112, %eax
	cmove	%edi, %eax
	movl	$1, %edi
	movb	%al, 72(%rsp)
.L3713:
	leaq	289(%rsp), %rbx
	leaq	160(%rsp), %rcx
	fldt	48(%rsp)
	leaq	144(%rsp), %r9
	leaq	416(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm6
	fstpt	144(%rsp)
	movl	%r14d, 40(%rsp)
	movl	$4, 32(%rsp)
	movq	%r9, 64(%rsp)
	call	_ZSt8to_charsPcS_eSt12chars_formati
	cmpl	$132, 168(%rsp)
	movq	160(%rsp), %r12
	movq	64(%rsp), %r10
	jne	.L4033
	leaq	8(%r14), %r12
	movl	$1, %r13d
	movl	$4, %ebx
	movb	$0, 64(%rsp)
	jmp	.L3728
	.p2align 4
	.p2align 3
.L3711:
	andl	$120, %eax
	movl	$112, %edx
	movl	$101, %r9d
	cmpb	$16, %al
	cmovne	%edx, %r9d
	xorl	%edi, %edi
	movb	%r9b, 72(%rsp)
	jmp	.L3713
	.p2align 4
	.p2align 3
.L3712:
	movl	%r14d, %r8d
	movl	$3, %ebx
	xorl	%edi, %edi
	movb	$101, 72(%rsp)
	movb	$0, 64(%rsp)
	jmp	.L3725
	.p2align 4
	.p2align 3
.L3703:
	movl	%r14d, %r8d
.L3717:
	movl	$1, %edi
	movb	$69, 72(%rsp)
.L3714:
	movl	$3, %ebx
	movb	$1, 64(%rsp)
	jmp	.L3725
	.p2align 4
	.p2align 3
.L3721:
	andl	$120, %eax
	movl	$80, %r9d
	movl	$112, %r10d
	movl	$1, %edi
	cmpb	$16, %al
	cmove	%r9d, %r10d
	movb	%r10b, 72(%rsp)
.L3724:
	leaq	289(%rsp), %rbx
	leaq	160(%rsp), %rcx
	fldt	48(%rsp)
	leaq	144(%rsp), %r9
	leaq	416(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm6
	fstpt	144(%rsp)
	movl	$4, 32(%rsp)
	movq	%r9, 64(%rsp)
	call	_ZSt8to_charsPcS_eSt12chars_format
	cmpl	$132, 168(%rsp)
	movq	160(%rsp), %r12
	movq	64(%rsp), %r10
	movl	$6, %r14d
	je	.L3829
.L4033:
	leaq	416(%rsp), %r11
	movb	$0, 64(%rsp)
	movq	%r11, 80(%rsp)
	jmp	.L3727
	.p2align 4
	.p2align 3
.L3820:
	movl	$6, %r8d
	movl	$6, %r14d
	xorl	%edi, %edi
	movb	$101, 72(%rsp)
	jmp	.L3716
	.p2align 4
	.p2align 3
.L3821:
	movl	$6, %r8d
	movl	$6, %r14d
	jmp	.L3720
	.p2align 4
	.p2align 3
.L3822:
	movl	$6, %r8d
	movl	$6, %r14d
	xorl	%edi, %edi
	jmp	.L3715
	.p2align 4
	.p2align 3
.L3825:
	movl	$6, %r8d
	movl	$6, %r14d
	jmp	.L3717
	.p2align 4
	.p2align 3
.L3705:
	movl	%r14d, %r8d
	xorl	%edi, %edi
	movb	$101, 72(%rsp)
	jmp	.L3714
	.p2align 4
	.p2align 3
.L3823:
	movl	$6, %r8d
	movl	$6, %r14d
	jmp	.L3719
	.p2align 4
	.p2align 3
.L3824:
	movl	$6, %r8d
	movl	$6, %r14d
	xorl	%edi, %edi
	movb	$101, 72(%rsp)
	jmp	.L3714
	.p2align 4
	.p2align 3
.L3722:
	andl	$120, %eax
	movl	$101, %r11d
	movl	$112, %r12d
	cmpb	$16, %al
	cmove	%r11d, %r12d
	xorl	%edi, %edi
	movb	%r12b, 72(%rsp)
	jmp	.L3724
	.p2align 4
	.p2align 3
.L3852:
	movl	$2, %r8d
	movl	$32, %r11d
	jmp	.L3805
	.p2align 4
	.p2align 3
.L4047:
	leaq	112(%rsp), %r13
	jmp	.L3798
	.p2align 4
	.p2align 3
.L4039:
	movb	$43, -1(%rbx)
	movl	$1, %ecx
	movzbl	(%rsi), %r13d
	decq	%rbx
	jmp	.L3745
.L3850:
	movq	%rbx, %r12
	movq	%rax, %rdi
	movq	%rcx, %rbx
	.p2align 4
	.p2align 3
.L3799:
	movzwl	4(%rsi), %ecx
	movq	%rbp, %rdx
	call	_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0
.LEHE52:
	movq	%rax, %r9
	jmp	.L3801
.L3846:
	xorl	%r14d, %r14d
	movq	%r10, 96(%rsp)
	movq	$1, 88(%rsp)
.L3759:
	cmpq	$0, 200(%rsp)
	jne	.L3762
	movq	80(%rsp), %rcx
	subq	%r12, %rcx
	movq	88(%rsp), %r12
	cmpq	%r12, %rcx
	jnb	.L3763
.L3762:
	movq	88(%rsp), %rdx
	leaq	192(%rsp), %r13
	movq	%r10, 64(%rsp)
	movq	%r13, %rcx
	leaq	(%rdi,%rdx), %rdx
.LEHB53:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	200(%rsp), %r9
	movq	64(%rsp), %r10
	testq	%r9, %r9
	jne	.L3767
	cmpq	%r10, %rdi
	movq	%rbx, %r9
	movq	%r13, %rcx
	cmovbe	%rdi, %r10
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r10, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
	movq	64(%rsp), %r8
	cmpq	%r8, 96(%rsp)
	je	.L4049
.L3768:
	testq	%r14, %r14
	jne	.L4050
.L3769:
	cmpq	%r8, %rdi
	jb	.L4051
	movq	200(%rsp), %rdx
	movabsq	$9223372036854775807, %r11
	subq	%r8, %rdi
	leaq	(%rbx,%r8), %r9
	subq	%rdx, %r11
	cmpq	%rdi, %r11
	jb	.L4052
	movq	192(%rsp), %rax
	leaq	(%rdi,%rdx), %r12
	cmpq	%r15, %rax
	je	.L3848
	movq	208(%rsp), %rcx
.L3772:
	cmpq	%r12, %rcx
	jb	.L3773
	testq	%rdi, %rdi
	je	.L3774
	leaq	(%rax,%rdx), %rcx
	cmpq	$1, %rdi
	je	.L4053
	movq	%rdi, %r8
	movq	%r9, %rdx
	call	memcpy
	movq	192(%rsp), %rax
.L3774:
	movq	%r12, 200(%rsp)
	movb	$0, (%rax,%r12)
	jmp	.L3776
	.p2align 4
	.p2align 3
.L4037:
	movzbl	(%r8), %r12d
	movzwl	6(%rcx), %r11d
	movl	%r12d, %r14d
	andl	$15, %r12d
	andl	$15, %r14d
	cmpq	%r12, %r11
	jnb	.L3680
	leaq	(%r11,%r11,4), %r9
	movl	$4, %edi
	shrx	%rdi, (%r8), %rdx
	salq	$4, %r11
	shrx	%r9, %rdx, %r8
	addq	8(%rbp), %r11
	andl	$31, %r8d
	vmovdqa	(%r11), %xmm2
	vmovdqa	%xmm2, 256(%rsp)
.L3681:
	leaq	.L3685(%rip), %r11
	movzbl	%r8b, %r10d
	movb	%r8b, 272(%rsp)
	vmovdqu	256(%rsp), %ymm0
	movslq	(%r11,%r10,4), %r12
	addq	%r11, %r12
	vmovdqu	%ymm0, 224(%rsp)
	jmp	*%r12
	.section .rdata,"dr"
	.align 4
.L3685:
	.long	.L4031-.L3685
	.long	.L3699-.L3685
	.long	.L3698-.L3685
	.long	.L3697-.L3685
	.long	.L3696-.L3685
	.long	.L3695-.L3685
	.long	.L3694-.L3685
	.long	.L3693-.L3685
	.long	.L3692-.L3685
	.long	.L3691-.L3685
	.long	.L3690-.L3685
	.long	.L3689-.L3685
	.long	.L3688-.L3685
	.long	.L3687-.L3685
	.long	.L3686-.L3685
	.long	.L3684-.L3685
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4041:
	cmpq	%rax, %rdi
	movq	%rdi, %r10
	sete	%r8b
	cmpb	$0, 64(%rsp)
	movzbl	%r8b, %r9d
	movq	%r9, 88(%rsp)
	jne	.L4054
.L3843:
	xorl	%r14d, %r14d
	jmp	.L3753
	.p2align 4
	.p2align 3
.L3751:
	movsbl	72(%rsp), %edx
	movq	%rbx, %rcx
	movq	%rdi, %r8
	movq	%r11, 88(%rsp)
	call	memchr
	movq	88(%rsp), %rcx
	testq	%rax, %rax
	je	.L3844
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%rdi, %rax
	movq	%rax, %r10
	jmp	.L3750
	.p2align 4
	.p2align 3
.L4043:
	movq	%r13, %rcx
	call	_ZNSt6localeC1Ev
	movb	$1, 32(%rbp)
	jmp	.L3780
	.p2align 4
	.p2align 3
.L3680:
	testb	%r14b, %r14b
	jne	.L3682
	movl	$4, %r13d
	shrx	%r13, (%r8), %rbx
	cmpq	%rbx, %r11
	jb	.L4055
.L3682:
	leaq	192(%rsp), %r13
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L3767:
	cmpq	%r10, %r9
	jb	.L4056
	movq	88(%rsp), %r9
	xorl	%r8d, %r8d
	movq	%r10, %rdx
	movq	%r13, %rcx
	movl	$48, 32(%rsp)
	movq	%r10, 64(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %rbx
	cmpq	%rbx, 96(%rsp)
	jne	.L3776
	movq	192(%rsp), %rdi
	movq	96(%rsp), %r14
	movb	$46, (%rdi,%r14)
	.p2align 4
	.p2align 3
.L3776:
	movq	200(%rsp), %rdi
	movq	192(%rsp), %rbx
	movzbl	(%rsi), %r9d
.L3766:
	leaq	240(%rsp), %r14
	movq	$0, 232(%rsp)
	movb	$0, 240(%rsp)
	movq	%r14, 224(%rsp)
	testb	$32, %r9b
	je	.L3795
	jmp	.L3817
.L4054:
	cmpb	$48, (%rbx,%r11)
	jne	.L3754
.L4016:
	movq	$-1, %r9
.L3755:
	movq	%r10, %rax
	subq	%r9, %rax
	jmp	.L3758
.L4042:
	movq	%r9, %r8
	notq	%r8
	addq	%rdi, %r8
	andl	$7, %r8d
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
	cmpq	%rdi, %r9
	jnb	.L4016
	testq	%r8, %r8
	je	.L3757
	cmpq	$1, %r8
	je	.L3969
	cmpq	$2, %r8
	je	.L3970
	cmpq	$3, %r8
	je	.L3971
	cmpq	$4, %r8
	je	.L3972
	cmpq	$5, %r8
	je	.L3973
	cmpq	$6, %r8
	je	.L3974
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3974:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3973:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3972:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3971:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3970:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
.L3969:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	incq	%r9
	cmpq	%rdi, %r9
	jnb	.L4016
.L3757:
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	leaq	1(%r9), %r11
	cmpb	$48, (%rbx,%r11)
	movq	%r11, %r9
	jne	.L3755
	incq	%r9
	cmpb	$48, (%rbx,%r9)
	jne	.L3755
	cmpb	$48, 2(%rbx,%r11)
	leaq	2(%r11), %r9
	jne	.L3755
	cmpb	$48, 3(%rbx,%r11)
	leaq	3(%r11), %r9
	jne	.L3755
	cmpb	$48, 4(%rbx,%r11)
	leaq	4(%r11), %r9
	jne	.L3755
	cmpb	$48, 5(%rbx,%r11)
	leaq	5(%r11), %r9
	jne	.L3755
	cmpb	$48, 6(%rbx,%r11)
	leaq	6(%r11), %r9
	jne	.L3755
	leaq	7(%r11), %r9
	cmpq	%rdi, %r9
	jb	.L3757
	jmp	.L4016
	.p2align 4
	.p2align 3
.L3844:
	movq	%rdi, %r10
	jmp	.L3750
.L3694:
	movq	224(%rsp), %r14
	vzeroupper
	jmp	.L3679
.L3695:
	movq	224(%rsp), %r14
	testq	%r14, %r14
	js	.L4057
.L4032:
	vzeroupper
	jmp	.L3679
.L3696:
	movl	224(%rsp), %r14d
	vzeroupper
	jmp	.L3679
.L3697:
	movslq	224(%rsp), %r14
	testl	%r14d, %r14d
	jns	.L4032
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L4031:
	vzeroupper
	jmp	.L3682
.L4044:
	movq	256(%rsp), %r8
	leaq	272(%rsp), %rcx
	cmpq	%rcx, %r8
	je	.L4058
	vmovdqu	264(%rsp), %xmm5
	movq	%r8, 224(%rsp)
	vmovdqu	%xmm5, 232(%rsp)
.L3784:
	movq	%rcx, 256(%rsp)
	leaq	272(%rsp), %rcx
	movq	%rcx, %r11
	jmp	.L3793
.L4038:
	leaq	192(%rsp), %r13
	movl	$256, %edx
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	192(%rsp), %rdx
	movq	64(%rsp), %r10
.L3730:
	cmpq	%r15, %rdx
	je	.L3832
	movq	208(%rsp), %rax
	leaq	(%rax,%rax), %rbx
.L3735:
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movq	%r10, 64(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	192(%rsp), %rcx
	movq	64(%rsp), %r9
	fldt	48(%rsp)
	fstpt	144(%rsp)
	leaq	1(%rcx), %rdx
	leaq	-1(%rcx,%rbx), %r8
	movq	%rcx, 64(%rsp)
	movq	%r14, %rcx
	movq	%r9, 72(%rsp)
	call	_ZSt8to_charsPcS_e
	movq	168(%rsp), %rdi
	movq	160(%rsp), %r8
	movq	64(%rsp), %rdx
	testl	%edi, %edi
	movq	%r8, %r12
	je	.L3833
	movq	192(%rsp), %r9
	cmpl	$132, %edi
	movq	$0, 200(%rsp)
	movq	72(%rsp), %r10
	movb	$0, (%r9)
	movq	192(%rsp), %rdx
	je	.L3730
	xorl	%edi, %edi
	movl	$6, %r14d
	movb	$101, 72(%rsp)
	movb	$0, 64(%rsp)
.L3737:
	leaq	1(%rdx), %rbx
	addq	200(%rsp), %rdx
	movq	%rdx, 80(%rsp)
	jmp	.L3727
.L3829:
	xorl	%r13d, %r13d
	movl	$4, %ebx
	movb	$0, 64(%rsp)
.L3732:
	movb	%r13b, 80(%rsp)
	leaq	192(%rsp), %r13
	movl	$256, %edx
	movq	%r10, 88(%rsp)
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movzbl	80(%rsp), %r10d
	movq	192(%rsp), %rdx
	movq	88(%rsp), %r9
	testb	%r10b, %r10b
	jne	.L3742
.L3739:
	cmpq	%r15, %rdx
	je	.L3834
	movq	208(%rsp), %r10
	leaq	(%r10,%r10), %r12
.L3738:
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%r9, 80(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	192(%rsp), %rcx
	movq	80(%rsp), %r9
	fldt	48(%rsp)
	movl	%ebx, 32(%rsp)
	fstpt	144(%rsp)
	leaq	-1(%rcx,%r12), %r8
	leaq	1(%rcx), %rdx
	movq	%rcx, 80(%rsp)
	vmovq	%xmm6, %rcx
	movq	%r9, 88(%rsp)
	call	_ZSt8to_charsPcS_eSt12chars_format
	movq	168(%rsp), %r11
	movq	160(%rsp), %r8
	movq	80(%rsp), %rdx
	testl	%r11d, %r11d
	movq	%r8, %r12
	je	.L3736
	movq	192(%rsp), %r8
	cmpl	$132, %r11d
	movq	$0, 200(%rsp)
	movq	88(%rsp), %r9
	movb	$0, (%r8)
	movq	192(%rsp), %rdx
	jne	.L3737
	jmp	.L3739
.L3816:
	leaq	8(%r14), %r12
	movl	$1, %r13d
	cmpl	$2, %ebx
	je	.L4059
.L3728:
	cmpq	$128, %r12
	jbe	.L3732
	leaq	192(%rsp), %r13
	movq	%r12, %rdx
	movq	%r10, 80(%rsp)
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	192(%rsp), %rdx
	movq	80(%rsp), %r9
.L3742:
	cmpq	%r15, %rdx
	je	.L3835
	movq	208(%rsp), %r8
	leaq	(%r8,%r8), %r12
.L3740:
	movq	%r12, %rdx
	movq	%r13, %rcx
	movq	%r9, 80(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	192(%rsp), %rcx
	movq	80(%rsp), %r9
	fldt	48(%rsp)
	movl	%r14d, 40(%rsp)
	fstpt	144(%rsp)
	movl	%ebx, 32(%rsp)
	leaq	-1(%rcx,%r12), %r8
	leaq	1(%rcx), %rdx
	movq	%rcx, 88(%rsp)
	vmovq	%xmm6, %rcx
	call	_ZSt8to_charsPcS_eSt12chars_formati
	movq	168(%rsp), %r11
	movq	160(%rsp), %r8
	movq	80(%rsp), %r9
	testl	%r11d, %r11d
	movq	%r8, %r12
	jne	.L3741
	movq	88(%rsp), %rdx
.L3736:
	movq	192(%rsp), %r13
	subq	%rdx, %r8
	movq	%r8, 200(%rsp)
	movb	$0, 0(%r13,%r8)
	movq	192(%rsp), %r9
	leaq	1(%r9), %rbx
	addq	200(%rsp), %r9
	movq	%r9, 80(%rsp)
	jmp	.L3727
.L4058:
	movq	264(%rsp), %rdx
	movq	%rdx, %r10
.L3814:
	testq	%rdx, %rdx
	je	.L3785
	cmpq	$1, %rdx
	je	.L4060
	movl	%edx, %r11d
	cmpl	$8, %edx
	jnb	.L3787
	testb	$4, %dl
	jne	.L4061
	testl	%edx, %edx
	je	.L3788
	movzbl	272(%rsp), %eax
	andl	$2, %edx
	movb	%al, (%r9)
	jne	.L4011
.L4013:
	movq	224(%rsp), %r9
	movq	264(%rsp), %rdx
.L3788:
	movq	%rdx, %r10
	movq	%r9, %r11
.L3785:
	movq	%r10, 232(%rsp)
	movb	$0, (%r11,%r10)
	movq	256(%rsp), %r11
	jmp	.L3793
.L4045:
	movzwl	4(%rsi), %r9d
	cmpq	%r9, %rax
	jnb	.L4035
	movq	%rbx, %r12
	movq	%rax, %rdi
	movq	%rcx, %rbx
	jmp	.L3797
.L3763:
	movq	%r12, %rdx
	movq	%rdi, %r8
	leaq	(%rbx,%r10), %r12
	movq	%r10, 64(%rsp)
	leaq	(%rdx,%r10), %rcx
	subq	%r10, %r8
	movq	%r12, %rdx
	addq	%rbx, %rcx
	call	memmove
	movq	64(%rsp), %r10
	cmpq	%r10, 96(%rsp)
	jne	.L3765
	movq	96(%rsp), %r8
	movb	$46, (%r12)
	leaq	1(%rbx,%r8), %r12
.L3765:
	movq	%r14, %r8
	movl	$48, %edx
	movq	%r12, %rcx
	call	memset
	movq	88(%rsp), %r14
	movzbl	(%rsi), %r9d
	addq	%r14, %rdi
	jmp	.L3766
.L3847:
	movq	%r10, 96(%rsp)
	movq	$1, 88(%rsp)
	jmp	.L3759
.L4059:
	fldt	48(%rsp)
	movq	%r10, %rcx
	leaq	128(%rsp), %rdx
	leaq	256(%rsp), %r8
	movq	%r10, 80(%rsp)
	fstpt	128(%rsp)
	movl	$0, 256(%rsp)
	call	frexpl
	movl	256(%rsp), %eax
	movq	80(%rsp), %r10
	testl	%eax, %eax
	jle	.L3731
	imull	$4004, %eax, %edx
	imulq	$995517945, %rdx, %r11
	movq	%rdx, %r9
	shrq	$32, %r11
	subl	%r11d, %r9d
	shrl	%r9d
	addl	%r11d, %r9d
	shrl	$13, %r9d
	incl	%r9d
	addq	%r9, %r12
.L3731:
	movl	$1, %r13d
	jmp	.L3728
.L3841:
	movq	%rdi, %r10
	jmp	.L3756
.L3853:
	movl	$2, %r8d
	movl	$48, %r11d
	jmp	.L3805
.L3833:
	xorl	%edi, %edi
	movl	$6, %r14d
	movb	$101, 72(%rsp)
	movb	$0, 64(%rsp)
	jmp	.L3736
.L3741:
	movq	192(%rsp), %rax
	movq	$0, 200(%rsp)
	movb	$0, (%rax)
	movq	192(%rsp), %rdx
	cmpl	$132, %r11d
	jne	.L3737
	jmp	.L3742
.L3773:
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%rdi, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	movq	192(%rsp), %rax
	jmp	.L3774
.L4050:
	movq	200(%rsp), %rdx
	movq	%r8, 64(%rsp)
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$48, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %r8
	jmp	.L3769
.L4055:
	salq	$5, %r11
	addq	8(%r8), %r11
	vmovdqu	(%r11), %xmm3
	vmovdqa	%xmm3, 256(%rsp)
	movzbl	16(%r11), %ecx
	movb	%cl, 272(%rsp)
	movzbl	16(%r11), %r8d
	jmp	.L3681
.L4049:
	movl	$46, %edx
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc
.LEHE53:
	movq	64(%rsp), %r8
	jmp	.L3768
.L3848:
	movl	$15, %ecx
	jmp	.L3772
.L3834:
	movl	$30, %r12d
	jmp	.L3738
.L3832:
	movl	$30, %ebx
	jmp	.L3735
.L3835:
	movl	$30, %r12d
	jmp	.L3740
.L4053:
	movzbl	(%r9), %r13d
	movb	%r13b, (%rcx)
	movq	192(%rsp), %rax
	jmp	.L3774
.L3787:
	movq	272(%rsp), %r11
	movl	%edx, %r10d
	leaq	8(%r9), %r8
	andq	$-8, %r8
	movq	%r11, (%r9)
	movq	-8(%rcx,%r10), %rax
	movq	%rax, -8(%r9,%r10)
	subq	%r8, %r9
	movq	%rcx, %r10
	leal	(%rdx,%r9), %eax
	subq	%r9, %r10
	movl	%eax, %r9d
	andl	$-8, %r9d
	cmpl	$8, %r9d
	jb	.L4013
	xorl	%edx, %edx
	andl	$-8, %eax
	movq	(%r10,%rdx), %r11
	leal	-1(%rax), %r9d
	shrl	$3, %r9d
	andl	$7, %r9d
	movq	%r11, (%r8,%rdx)
	movl	$8, %edx
	cmpl	%eax, %edx
	jnb	.L4013
	testl	%r9d, %r9d
	je	.L3791
	cmpl	$1, %r9d
	je	.L3962
	cmpl	$2, %r9d
	je	.L3963
	cmpl	$3, %r9d
	je	.L3964
	cmpl	$4, %r9d
	je	.L3965
	cmpl	$5, %r9d
	je	.L3966
	cmpl	$6, %r9d
	je	.L3967
	movq	(%r10,%rdx), %r9
	movq	%r9, (%r8,%rdx)
	movl	$16, %edx
.L3967:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L3966:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L3965:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L3964:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L3963:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L3962:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	cmpl	%eax, %edx
	jnb	.L4013
.L3791:
	movl	%edx, %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	8(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	16(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	24(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	32(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	40(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	48(%rdx), %r9d
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	leal	56(%rdx), %r9d
	addl	$64, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	cmpl	%eax, %edx
	jb	.L3791
	jmp	.L4013
	.p2align 4
	.p2align 3
.L4060:
	movzbl	272(%rsp), %r8d
	movb	%r8b, (%r9)
	movq	264(%rsp), %r10
	movq	224(%rsp), %r11
	jmp	.L3785
.L4048:
	movq	%r9, 64(%rsp)
	movq	(%rcx), %r9
	movq	%rcx, 48(%rsp)
.LEHB54:
	call	*(%r9)
.LEHE54:
	movq	64(%rsp), %r9
	movq	48(%rsp), %rcx
	jmp	.L3806
.L4061:
	movl	272(%rsp), %edx
	movl	%edx, (%r9)
	movl	-4(%rcx,%r11), %r10d
	movl	%r10d, -4(%r9,%r11)
	movq	224(%rsp), %r9
	movq	264(%rsp), %rdx
	jmp	.L3788
.L4011:
	movzwl	-2(%rcx,%r11), %r8d
	movw	%r8w, -2(%r9,%r11)
	movq	224(%rsp), %r9
	movq	264(%rsp), %rdx
	jmp	.L3788
.L3698:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
.LEHB55:
	call	_ZSt20__throw_format_errorPKc
.L3699:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3684:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.LEHE55:
.L3855:
	movq	%r12, %rcx
	movq	%rax, %r15
	vzeroupper
	call	_ZNSt6localeD1Ev
.L3810:
	leaq	224(%rsp), %rcx
	leaq	192(%rsp), %r13
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L3811:
	movq	%r13, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r15, %rcx
.LEHB56:
	call	_Unwind_Resume
.LEHE56:
.L4057:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
.LEHB57:
	call	_ZSt20__throw_format_errorPKc
.L4052:
	leaq	.LC123(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L4051:
	movq	%rdi, %r9
	leaq	.LC148(%rip), %rdx
	leaq	.LC146(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L3683:
.L3856:
	movq	%rax, %r15
	vzeroupper
	jmp	.L3810
.L4056:
	movq	%r10, %r8
	leaq	.LC149(%rip), %rdx
	leaq	.LC150(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L3854:
	movq	%rax, %r15
	vzeroupper
	jmp	.L3811
.L3686:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3687:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3688:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3689:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3690:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3691:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3692:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L3693:
	leaq	.LC6(%rip), %rcx
	leaq	192(%rsp), %r13
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	nop
.LEHE57:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15540:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15540-.LLSDACSB15540
.LLSDACSB15540:
	.uleb128 .LEHB51-.LFB15540
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L3855-.LFB15540
	.uleb128 0
	.uleb128 .LEHB52-.LFB15540
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L3856-.LFB15540
	.uleb128 0
	.uleb128 .LEHB53-.LFB15540
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L3854-.LFB15540
	.uleb128 0
	.uleb128 .LEHB54-.LFB15540
	.uleb128 .LEHE54-.LEHB54
	.uleb128 .L3856-.LFB15540
	.uleb128 0
	.uleb128 .LEHB55-.LFB15540
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L3854-.LFB15540
	.uleb128 0
	.uleb128 .LEHB56-.LFB15540
	.uleb128 .LEHE56-.LEHB56
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB57-.LFB15540
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L3854-.LFB15540
	.uleb128 0
.LLSDACSE15540:
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15537:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$408, %rsp
	.seh_stackalloc	408
	vmovaps	%xmm6, 368(%rsp)
	.seh_savexmm	%xmm6, 368
	vmovaps	%xmm7, 384(%rsp)
	.seh_savexmm	%xmm7, 384
	.seh_endprologue
	movzbl	1(%rcx), %eax
	leaq	160(%rsp), %rdi
	movq	%rcx, %rsi
	vmovapd	%xmm1, %xmm6
	movq	%r8, 496(%rsp)
	movq	%rdi, 144(%rsp)
	movq	$0, 152(%rsp)
	movb	$0, 160(%rsp)
	movl	%eax, %edx
	andl	$6, %edx
	je	.L4063
	cmpb	$2, %dl
	je	.L4422
	movq	$-1, %r12
	cmpb	$4, %dl
	je	.L4423
.L4065:
	movl	%eax, %ecx
	shrb	$3, %cl
	andl	$15, %ecx
	cmpb	$8, %cl
	ja	.L4069
	leaq	.L4090(%rip), %r13
	movzbl	%cl, %r11d
	movslq	0(%r13,%r11,4), %r15
	addq	%r13, %r15
	jmp	*%r15
	.section .rdata,"dr"
	.align 4
.L4090:
	.long	.L4098-.L4090
	.long	.L4097-.L4090
	.long	.L4096-.L4090
	.long	.L4095-.L4090
	.long	.L4094-.L4090
	.long	.L4093-.L4090
	.long	.L4092-.L4090
	.long	.L4091-.L4090
	.long	.L4089-.L4090
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4063:
	movl	%eax, %ecx
	shrb	$3, %cl
	andl	$15, %ecx
	cmpb	$8, %cl
	ja	.L4069
	leaq	.L4104(%rip), %rbp
	movzbl	%cl, %ebx
	movslq	0(%rbp,%rbx,4), %r8
	addq	%rbp, %r8
	jmp	*%r8
	.section .rdata,"dr"
	.align 4
.L4104:
	.long	.L4109-.L4104
	.long	.L4108-.L4104
	.long	.L4107-.L4104
	.long	.L4206-.L4104
	.long	.L4207-.L4104
	.long	.L4208-.L4104
	.long	.L4209-.L4104
	.long	.L4210-.L4104
	.long	.L4211-.L4104
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4109:
	leaq	112(%rsp), %r13
	leaq	241(%rsp), %rbx
	vmovapd	%xmm1, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	movq	%r13, %rcx
	call	_ZSt8to_charsPcS_d
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4424
	leaq	368(%rsp), %r15
	movl	$6, %r12d
	xorl	%r14d, %r14d
	movb	$101, 56(%rsp)
	movq	%r15, 64(%rsp)
.L4115:
	movzbl	(%rsi), %r15d
	vmovmskpd	%xmm6, %r8d
	testb	$1, %r8b
	jne	.L4420
	movl	%r15d, %r9d
	andl	$12, %r9d
	cmpb	$4, %r9b
	je	.L4425
	xorl	%ecx, %ecx
	cmpb	$12, %r9b
	je	.L4426
.L4131:
	movq	%rbp, %r13
	subq	%rbx, %r13
	testb	$16, %r15b
	je	.L4135
	vandpd	.LC13(%rip), %xmm6, %xmm2
	vmovsd	.LC152(%rip), %xmm1
	vucomisd	%xmm2, %xmm1
	jb	.L4135
	testq	%r13, %r13
	je	.L4224
	movq	%rcx, 72(%rsp)
	movq	%r13, %r8
	movl	$46, %edx
	movq	%rbx, %rcx
	call	memchr
	movq	72(%rsp), %r11
	testq	%rax, %rax
	je	.L4137
	subq	%rbx, %rax
	movq	%rax, 80(%rsp)
	cmpq	$-1, %rax
	je	.L4137
	leaq	1(%rax), %r10
	cmpq	%r13, %r10
	jnb	.L4427
	movsbl	56(%rsp), %edx
	movq	%r13, %r8
	leaq	(%rbx,%r10), %rcx
	movq	%r11, 88(%rsp)
	subq	%r10, %r8
	movq	%r10, 72(%rsp)
	call	memchr
	movq	72(%rsp), %r9
	movq	88(%rsp), %r11
	testq	%rax, %rax
	je	.L4227
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%r13, %rax
	movq	%rax, %r10
.L4142:
	movq	80(%rsp), %r8
	cmpq	%r8, %r10
	sete	%cl
	movzbl	%cl, %edx
	movq	%rdx, 72(%rsp)
	testb	%r14b, %r14b
	je	.L4229
	cmpb	$48, (%rbx,%r11)
	je	.L4428
.L4140:
	movq	%r10, %r8
	subq	%r11, %r8
	decq	%r8
.L4144:
	testq	%r12, %r12
	jne	.L4146
	.p2align 4
	.p2align 3
.L4139:
	cmpq	$0, 72(%rsp)
	jne	.L4145
	leaq	192(%rsp), %rbp
	andl	$32, %r15d
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%rbp, 176(%rsp)
	je	.L4181
.L4203:
	movq	496(%rsp), %r10
	cmpb	$0, 32(%r10)
	leaq	24(%r10), %r15
	je	.L4429
.L4166:
	leaq	136(%rsp), %r12
	movq	%r15, %rdx
	leaq	96(%rsp), %r14
	movq	%r12, %rcx
	call	_ZNSt6localeC1ERKS_
	movzbl	56(%rsp), %r8d
	leaq	208(%rsp), %rcx
	movq	%r12, %r9
	movq	%r14, %rdx
	movq	%r13, 96(%rsp)
	movq	%rbx, 104(%rsp)
.LEHB58:
	call	_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0
.LEHE58:
	movq	176(%rsp), %r15
	movq	%r15, %r9
	cmpq	%rbp, %r15
	je	.L4430
	movq	216(%rsp), %rdx
	movq	208(%rsp), %rax
	leaq	224(%rsp), %rcx
	movq	%rdx, %r10
	cmpq	%rcx, %rax
	je	.L4200
	vmovq	%rdx, %xmm4
	vpinsrq	$1, 224(%rsp), %xmm4, %xmm0
	movq	192(%rsp), %r8
	movq	%rax, 176(%rsp)
	vmovdqu	%xmm0, 184(%rsp)
	testq	%r15, %r15
	je	.L4170
	movq	%r15, 208(%rsp)
	movq	%r8, 224(%rsp)
.L4179:
	movq	$0, 216(%rsp)
	movb	$0, (%r15)
	movq	208(%rsp), %r10
	cmpq	%rcx, %r10
	je	.L4180
	movq	224(%rsp), %rdx
	movq	%r10, %rcx
	incq	%rdx
	call	_ZdlPvy
.L4180:
	movq	%r12, %rcx
	call	_ZNSt6localeD1Ev
	movq	184(%rsp), %r15
	testq	%r15, %r15
	je	.L4181
	movzwl	(%rsi), %r13d
	movq	176(%rsp), %rcx
	andw	$384, %r13w
	cmpw	$128, %r13w
	je	.L4431
	cmpw	$256, %r13w
	je	.L4236
.L4421:
	movq	496(%rsp), %rbx
	movq	%r15, %r13
	movq	16(%rbx), %rsi
	movq	%rcx, %rbx
.L4184:
	movq	%r14, %rdx
	movq	%rsi, %rcx
	movq	%r13, 96(%rsp)
	movq	%rbx, 104(%rsp)
.LEHB59:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L4190
	.p2align 4
	.p2align 3
.L4135:
	leaq	192(%rsp), %rbp
	andl	$32, %r15d
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%rbp, 176(%rsp)
	je	.L4181
	vandpd	.LC13(%rip), %xmm6, %xmm3
	vmovsd	.LC152(%rip), %xmm5
	vucomisd	%xmm3, %xmm5
	jnb	.L4203
.L4181:
	movzwl	(%rsi), %r14d
	andw	$384, %r14w
	cmpw	$128, %r14w
	je	.L4432
	movq	%rbx, %r12
	cmpw	$256, %r14w
	je	.L4185
.L4188:
	movq	496(%rsp), %r12
	movq	16(%r12), %rsi
	testq	%r13, %r13
	jne	.L4433
.L4190:
	movq	176(%rsp), %rcx
	cmpq	%rbp, %rcx
	je	.L4193
	movq	192(%rsp), %rbp
	leaq	1(%rbp), %rdx
	call	_ZdlPvy
.L4193:
	movq	144(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L4254
	movq	160(%rsp), %rdi
	leaq	1(%rdi), %rdx
	call	_ZdlPvy
	nop
.L4254:
	vmovaps	368(%rsp), %xmm6
	vmovaps	384(%rsp), %xmm7
	movq	%rsi, %rax
	addq	$408, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L4426:
	movb	$32, -1(%rbx)
	movzbl	(%rsi), %r15d
	decq	%rbx
	.p2align 4
	.p2align 3
.L4420:
	movl	$1, %ecx
	jmp	.L4131
	.p2align 4
	.p2align 3
.L4432:
	movzwl	4(%rsi), %r9d
	movq	%rbx, %r12
.L4187:
	cmpq	%r9, %r13
	jnb	.L4188
.L4183:
	movl	8(%rsi), %r10d
	movzbl	(%rsi), %esi
	movq	496(%rsp), %rdx
	subq	%r13, %r9
	movl	%esi, %r8d
	movq	16(%rdx), %rcx
	andl	$3, %r8d
	jne	.L4191
	testb	$64, %sil
	je	.L4238
	vandpd	.LC13(%rip), %xmm6, %xmm2
	vmovsd	.LC152(%rip), %xmm1
	vucomisd	%xmm2, %xmm1
	jb	.L4238
	movzbl	(%r12), %r11d
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rax
	cmpb	$15, (%rax,%r11)
	jbe	.L4239
	movq	24(%rcx), %r14
	movzbl	(%rbx), %r15d
	leaq	1(%r14), %r8
	movq	%r8, 24(%rcx)
	movb	%r15b, (%r14)
	movq	24(%rcx), %r12
	subq	8(%rcx), %r12
	cmpq	16(%rcx), %r12
	je	.L4434
.L4192:
	incq	%rbx
	decq	%r13
	movl	$2, %r8d
	movl	$48, %r10d
	.p2align 4
	.p2align 3
.L4191:
	leaq	96(%rsp), %rdx
	movq	%r13, 96(%rsp)
	movq	%rbx, 104(%rsp)
	movl	%r10d, 32(%rsp)
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	movq	%rax, %rsi
	jmp	.L4190
	.p2align 4
	.p2align 3
.L4422:
	movzwl	6(%rcx), %r12d
	jmp	.L4065
	.p2align 4
	.p2align 3
.L4224:
	xorl	%r10d, %r10d
.L4136:
	testb	%r14b, %r14b
	je	.L4232
	testq	%r12, %r12
	je	.L4233
	movq	%r10, %r8
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
	subq	%rcx, %r8
.L4146:
	subq	%r8, %r12
	addq	%r12, 72(%rsp)
	jmp	.L4139
	.p2align 4
	.p2align 3
.L4092:
	movl	%r12d, %r10d
.L4105:
	movl	$1, %r13d
.L4101:
	movl	$2, %ebx
	xorl	%r14d, %r14d
	movb	$101, 56(%rsp)
.L4111:
	leaq	241(%rsp), %r15
	leaq	112(%rsp), %rcx
	vmovapd	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%r15, %rdx
	vmovq	%rcx, %xmm7
	movl	%r10d, 40(%rsp)
	movl	%ebx, 32(%rsp)
	call	_ZSt8to_charsPcS_dSt12chars_formati
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4202
	leaq	368(%rsp), %r11
	movq	%r15, %rbx
	movq	%r11, 64(%rsp)
.L4113:
	testb	%r13b, %r13b
	je	.L4115
	cmpq	%rbp, %rbx
	je	.L4115
	movq	%rbp, %rax
	movq	__imp_toupper(%rip), %r13
	movq	%rbx, %r15
	subq	%rbx, %rax
	andl	$7, %eax
	je	.L4132
	cmpq	$1, %rax
	je	.L4340
	cmpq	$2, %rax
	je	.L4341
	cmpq	$3, %rax
	je	.L4342
	cmpq	$4, %rax
	je	.L4343
	cmpq	$5, %rax
	je	.L4344
	cmpq	$6, %rax
	je	.L4345
	movsbl	(%rbx), %ecx
	leaq	1(%rbx), %r15
	call	*%r13
	movb	%al, (%rbx)
.L4345:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4344:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4343:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4342:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4341:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4340:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
	cmpq	%r15, %rbp
	je	.L4115
.L4132:
	movsbl	(%r15), %ecx
	addq	$8, %r15
	call	*%r13
	movsbl	-7(%r15), %ecx
	movb	%al, -8(%r15)
	call	*%r13
	movsbl	-6(%r15), %ecx
	movb	%al, -7(%r15)
	call	*%r13
	movsbl	-5(%r15), %ecx
	movb	%al, -6(%r15)
	call	*%r13
	movsbl	-4(%r15), %ecx
	movb	%al, -5(%r15)
	call	*%r13
	movsbl	-3(%r15), %ecx
	movb	%al, -4(%r15)
	call	*%r13
	movsbl	-2(%r15), %ecx
	movb	%al, -3(%r15)
	call	*%r13
	movsbl	-1(%r15), %ecx
	movb	%al, -2(%r15)
	call	*%r13
	movb	%al, -1(%r15)
	cmpq	%r15, %rbp
	jne	.L4132
	jmp	.L4115
	.p2align 4
	.p2align 3
.L4093:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	jmp	.L4101
	.p2align 4
	.p2align 3
.L4094:
	movl	%r12d, %r10d
.L4106:
	movl	$1, %r13d
	movb	$69, 56(%rsp)
.L4102:
	movl	$1, %ebx
	xorl	%r14d, %r14d
	jmp	.L4111
	.p2align 4
	.p2align 3
.L4095:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4102
	.p2align 4
	.p2align 3
.L4096:
	andl	$120, %eax
	movl	$80, %r14d
	movl	$1, %r13d
	cmpb	$16, %al
	movl	$112, %eax
	cmove	%r14d, %eax
	movb	%al, 56(%rsp)
.L4099:
	leaq	241(%rsp), %rbx
	leaq	112(%rsp), %rcx
	vmovapd	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm7
	movl	%r12d, 40(%rsp)
	movl	$4, 32(%rsp)
	call	_ZSt8to_charsPcS_dSt12chars_formati
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	jne	.L4419
	leaq	8(%r12), %rbp
	movl	$1, %r9d
	movl	$4, %ebx
	xorl	%r14d, %r14d
	jmp	.L4114
	.p2align 4
	.p2align 3
.L4097:
	andl	$120, %eax
	movl	$112, %edx
	movl	$101, %ebx
	cmpb	$16, %al
	cmovne	%edx, %ebx
	xorl	%r13d, %r13d
	movb	%bl, 56(%rsp)
	jmp	.L4099
	.p2align 4
	.p2align 3
.L4098:
	movl	%r12d, %r10d
	movl	$3, %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4111
	.p2align 4
	.p2align 3
.L4089:
	movl	%r12d, %r10d
.L4103:
	movl	$1, %r13d
	movb	$69, 56(%rsp)
.L4100:
	movl	$3, %ebx
	movl	$1, %r14d
	jmp	.L4111
	.p2align 4
	.p2align 3
.L4107:
	andl	$120, %eax
	movl	$80, %r9d
	movl	$112, %r10d
	movl	$1, %r13d
	cmpb	$16, %al
	cmove	%r9d, %r10d
	movb	%r10b, 56(%rsp)
.L4110:
	leaq	241(%rsp), %rbx
	leaq	112(%rsp), %rcx
	vmovapd	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm7
	movl	$4, 32(%rsp)
	movl	$6, %r12d
	call	_ZSt8to_charsPcS_dSt12chars_format
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4215
.L4419:
	leaq	368(%rsp), %r8
	xorl	%r14d, %r14d
	movq	%r8, 64(%rsp)
	jmp	.L4113
	.p2align 4
	.p2align 3
.L4206:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4102
	.p2align 4
	.p2align 3
.L4207:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4106
	.p2align 4
	.p2align 3
.L4208:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	jmp	.L4101
	.p2align 4
	.p2align 3
.L4211:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4103
	.p2align 4
	.p2align 3
.L4091:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4100
	.p2align 4
	.p2align 3
.L4209:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4105
	.p2align 4
	.p2align 3
.L4210:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4100
	.p2align 4
	.p2align 3
.L4108:
	andl	$120, %eax
	movl	$101, %r11d
	movl	$112, %r12d
	cmpb	$16, %al
	cmove	%r11d, %r12d
	xorl	%r13d, %r13d
	movb	%r12b, 56(%rsp)
	jmp	.L4110
	.p2align 4
	.p2align 3
.L4238:
	movl	$2, %r8d
	movl	$32, %r10d
	jmp	.L4191
	.p2align 4
	.p2align 3
.L4433:
	leaq	96(%rsp), %r14
	jmp	.L4184
	.p2align 4
	.p2align 3
.L4425:
	movb	$43, -1(%rbx)
	movl	$1, %ecx
	movzbl	(%rsi), %r15d
	decq	%rbx
	jmp	.L4131
.L4236:
	movq	%rbx, %r12
	movq	%r15, %r13
	movq	%rcx, %rbx
	.p2align 4
	.p2align 3
.L4185:
	movzwl	4(%rsi), %ecx
	movq	496(%rsp), %rdx
	call	_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0
.LEHE59:
	movq	%rax, %r9
	jmp	.L4187
.L4232:
	xorl	%r12d, %r12d
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
.L4145:
	cmpq	$0, 152(%rsp)
	jne	.L4148
	movq	64(%rsp), %r11
	subq	%rbp, %r11
	movq	72(%rsp), %rbp
	cmpq	%rbp, %r11
	jnb	.L4149
.L4148:
	movq	72(%rsp), %rcx
	leaq	144(%rsp), %r15
	movq	%r10, 64(%rsp)
	leaq	0(%r13,%rcx), %rdx
	movq	%r15, %rcx
.LEHB60:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	152(%rsp), %r9
	movq	64(%rsp), %r14
	testq	%r9, %r9
	jne	.L4153
	cmpq	%r14, %r13
	movq	%r14, %r10
	movq	%rbx, %r9
	movq	%r15, %rcx
	cmovbe	%r13, %r10
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r10, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
	movq	64(%rsp), %rdx
	cmpq	%rdx, 80(%rsp)
	je	.L4435
.L4154:
	testq	%r12, %r12
	jne	.L4436
.L4155:
	cmpq	%rdx, %r13
	jb	.L4437
	subq	%rdx, %r13
	leaq	(%rbx,%rdx), %r9
	movq	152(%rsp), %rdx
	movabsq	$9223372036854775807, %rax
	subq	%rdx, %rax
	cmpq	%r13, %rax
	jb	.L4438
	movq	144(%rsp), %r11
	leaq	0(%r13,%rdx), %rbp
	cmpq	%rdi, %r11
	je	.L4234
	movq	160(%rsp), %r8
.L4158:
	cmpq	%rbp, %r8
	jb	.L4159
	testq	%r13, %r13
	je	.L4160
	leaq	(%r11,%rdx), %rcx
	cmpq	$1, %r13
	je	.L4439
	movq	%r13, %r8
	movq	%r9, %rdx
	call	memcpy
	movq	144(%rsp), %r11
.L4160:
	movq	%rbp, 152(%rsp)
	movb	$0, (%r11,%rbp)
	jmp	.L4162
	.p2align 4
	.p2align 3
.L4423:
	movzbl	(%r8), %r12d
	movzwl	6(%rcx), %r11d
	movl	%r12d, %ecx
	andl	$15, %r12d
	andl	$15, %ecx
	cmpq	%r12, %r11
	jnb	.L4066
	movq	(%r8), %rdx
	movq	496(%rsp), %r8
	leaq	(%r11,%r11,4), %rbx
	salq	$4, %r11
	addq	8(%r8), %r11
	movq	%rdx, 56(%rsp)
	shrq	$4, %rdx
	shrx	%rbx, %rdx, %r14
	andl	$31, %r14d
	vmovdqa	(%r11), %xmm4
	vmovdqa	%xmm4, 208(%rsp)
.L4067:
	leaq	.L4071(%rip), %r10
	movzbl	%r14b, %r9d
	movb	%r14b, 224(%rsp)
	vmovdqu	208(%rsp), %ymm0
	movslq	(%r10,%r9,4), %rbp
	addq	%r10, %rbp
	vmovdqu	%ymm0, 176(%rsp)
	jmp	*%rbp
	.section .rdata,"dr"
	.align 4
.L4071:
	.long	.L4417-.L4071
	.long	.L4085-.L4071
	.long	.L4084-.L4071
	.long	.L4083-.L4071
	.long	.L4082-.L4071
	.long	.L4081-.L4071
	.long	.L4080-.L4071
	.long	.L4079-.L4071
	.long	.L4078-.L4071
	.long	.L4077-.L4071
	.long	.L4076-.L4071
	.long	.L4075-.L4071
	.long	.L4074-.L4071
	.long	.L4073-.L4071
	.long	.L4072-.L4071
	.long	.L4070-.L4071
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4427:
	cmpq	%rax, %r13
	movq	%r13, %r10
	sete	%dl
	movzbl	%dl, %eax
	movq	%rax, 72(%rsp)
	testb	%r14b, %r14b
	jne	.L4440
.L4229:
	xorl	%r12d, %r12d
	jmp	.L4139
	.p2align 4
	.p2align 3
.L4137:
	movsbl	56(%rsp), %edx
	movq	%rbx, %rcx
	movq	%r13, %r8
	movq	%r11, 72(%rsp)
	call	memchr
	movq	72(%rsp), %rcx
	testq	%rax, %rax
	je	.L4230
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%r13, %rax
	movq	%rax, %r10
	jmp	.L4136
	.p2align 4
	.p2align 3
.L4429:
	movq	%r15, %rcx
	call	_ZNSt6localeC1Ev
	movq	496(%rsp), %rax
	movb	$1, 32(%rax)
	jmp	.L4166
	.p2align 4
	.p2align 3
.L4066:
	testb	%cl, %cl
	jne	.L4068
	movq	(%r8), %rdx
	movq	%rdx, 56(%rsp)
	shrq	$4, %rdx
	cmpq	%rdx, %r11
	jb	.L4441
.L4068:
	leaq	144(%rsp), %r15
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L4153:
	cmpq	%r14, %r9
	jb	.L4442
	movq	72(%rsp), %r9
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r15, %rcx
	movl	$48, 32(%rsp)
	movq	%r14, 64(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %rbx
	cmpq	%rbx, 80(%rsp)
	jne	.L4162
	movq	144(%rsp), %r13
	movq	80(%rsp), %r12
	movb	$46, 0(%r13,%r12)
	.p2align 4
	.p2align 3
.L4162:
	movq	152(%rsp), %r13
	movq	144(%rsp), %rbx
	movzbl	(%rsi), %r9d
.L4152:
	leaq	192(%rsp), %rbp
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%rbp, 176(%rsp)
	testb	$32, %r9b
	je	.L4181
	jmp	.L4203
.L4440:
	cmpb	$48, (%rbx,%r11)
	jne	.L4140
.L4402:
	movq	$-1, %r9
.L4141:
	movq	%r10, %r8
	subq	%r9, %r8
	jmp	.L4144
.L4428:
	movq	%r9, %r14
	notq	%r14
	addq	%r13, %r14
	andl	$7, %r14d
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
	cmpq	%r13, %r9
	jnb	.L4402
	testq	%r14, %r14
	je	.L4143
	cmpq	$1, %r14
	je	.L4355
	cmpq	$2, %r14
	je	.L4356
	cmpq	$3, %r14
	je	.L4357
	cmpq	$4, %r14
	je	.L4358
	cmpq	$5, %r14
	je	.L4359
	cmpq	$6, %r14
	je	.L4360
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4360:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4359:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4358:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4357:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4356:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
.L4355:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	incq	%r9
	cmpq	%r13, %r9
	jnb	.L4402
.L4143:
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	leaq	1(%r9), %rax
	cmpb	$48, (%rbx,%rax)
	movq	%rax, %r9
	jne	.L4141
	incq	%r9
	cmpb	$48, (%rbx,%r9)
	jne	.L4141
	cmpb	$48, 2(%rbx,%rax)
	leaq	2(%rax), %r9
	jne	.L4141
	cmpb	$48, 3(%rbx,%rax)
	leaq	3(%rax), %r9
	jne	.L4141
	cmpb	$48, 4(%rbx,%rax)
	leaq	4(%rax), %r9
	jne	.L4141
	cmpb	$48, 5(%rbx,%rax)
	leaq	5(%rax), %r9
	jne	.L4141
	cmpb	$48, 6(%rbx,%rax)
	leaq	6(%rax), %r9
	jne	.L4141
	leaq	7(%rax), %r9
	cmpq	%r13, %r9
	jb	.L4143
	jmp	.L4402
	.p2align 4
	.p2align 3
.L4230:
	movq	%r13, %r10
	jmp	.L4136
.L4080:
	movq	176(%rsp), %r12
	vzeroupper
	jmp	.L4065
.L4081:
	movq	176(%rsp), %r12
	testq	%r12, %r12
	js	.L4443
.L4418:
	vzeroupper
	jmp	.L4065
.L4082:
	movl	176(%rsp), %r12d
	vzeroupper
	jmp	.L4065
.L4083:
	movslq	176(%rsp), %r12
	testl	%r12d, %r12d
	jns	.L4418
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L4417:
	vzeroupper
	jmp	.L4068
.L4430:
	movq	208(%rsp), %r11
	leaq	224(%rsp), %rcx
	cmpq	%rcx, %r11
	je	.L4444
	vmovdqu	216(%rsp), %xmm7
	movq	%r11, 176(%rsp)
	vmovdqu	%xmm7, 184(%rsp)
.L4170:
	movq	%rcx, 208(%rsp)
	leaq	224(%rsp), %rcx
	movq	%rcx, %r15
	jmp	.L4179
.L4424:
	leaq	144(%rsp), %r15
	movl	$256, %edx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r8
.L4116:
	cmpq	%rdi, %r8
	je	.L4218
	movq	160(%rsp), %r14
	leaq	(%r14,%r14), %rbx
.L4121:
	movq	%rbx, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %rax
	vmovapd	%xmm6, %xmm3
	movq	%r13, %rcx
	leaq	1(%rax), %rdx
	leaq	-1(%rax,%rbx), %r8
	movq	%rax, 56(%rsp)
	call	_ZSt8to_charsPcS_d
	movq	120(%rsp), %rdx
	movq	112(%rsp), %r8
	movq	56(%rsp), %r9
	testl	%edx, %edx
	movq	%r8, %rbp
	je	.L4219
	movq	144(%rsp), %r10
	movq	$0, 152(%rsp)
	movb	$0, (%r10)
	movq	144(%rsp), %r8
	cmpl	$132, %edx
	je	.L4116
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$6, %r12d
	movb	$101, 56(%rsp)
.L4123:
	leaq	1(%r8), %rbx
	addq	152(%rsp), %r8
	movq	%r8, 64(%rsp)
	jmp	.L4113
.L4215:
	xorl	%r9d, %r9d
	movl	$4, %ebx
	xorl	%r14d, %r14d
.L4118:
	leaq	144(%rsp), %r15
	movl	$256, %edx
	movb	%r9b, 64(%rsp)
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movzbl	64(%rsp), %r10d
	movq	144(%rsp), %r8
	testb	%r10b, %r10b
	jne	.L4128
.L4125:
	cmpq	%rdi, %r8
	je	.L4220
	movq	160(%rsp), %r9
	leaq	(%r9,%r9), %rbp
.L4124:
	movq	%rbp, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r10
	vmovq	%xmm7, %rcx
	vmovapd	%xmm6, %xmm3
	movl	%ebx, 32(%rsp)
	leaq	-1(%r10,%rbp), %r8
	leaq	1(%r10), %rdx
	movq	%r10, 64(%rsp)
	call	_ZSt8to_charsPcS_dSt12chars_format
	movq	120(%rsp), %rcx
	movq	112(%rsp), %r8
	movq	64(%rsp), %r9
	testl	%ecx, %ecx
	movq	%r8, %rbp
	je	.L4122
	movq	144(%rsp), %r11
	movq	$0, 152(%rsp)
	movb	$0, (%r11)
	movq	144(%rsp), %r8
	cmpl	$132, %ecx
	jne	.L4123
	jmp	.L4125
.L4202:
	leaq	8(%r12), %rbp
	movl	$1, %r9d
	cmpl	$2, %ebx
	je	.L4445
.L4114:
	cmpq	$128, %rbp
	jbe	.L4118
	leaq	144(%rsp), %r15
	movq	%rbp, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r8
.L4128:
	cmpq	%rdi, %r8
	je	.L4221
	movq	160(%rsp), %rcx
	leaq	(%rcx,%rcx), %rbp
.L4126:
	movq	%rbp, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r11
	vmovapd	%xmm6, %xmm3
	vmovq	%xmm7, %rcx
	movl	%r12d, 40(%rsp)
	movl	%ebx, 32(%rsp)
	leaq	-1(%r11,%rbp), %r8
	leaq	1(%r11), %rdx
	movq	%r11, 64(%rsp)
	call	_ZSt8to_charsPcS_dSt12chars_formati
	movq	112(%rsp), %r8
	movq	120(%rsp), %rdx
	movq	%r8, %rbp
	testl	%edx, %edx
	jne	.L4127
	movq	64(%rsp), %r9
.L4122:
	movq	144(%rsp), %r15
	subq	%r9, %r8
	movq	%r8, 152(%rsp)
	movb	$0, (%r15,%r8)
	movq	144(%rsp), %rdx
	leaq	1(%rdx), %rbx
	addq	152(%rsp), %rdx
	movq	%rdx, 64(%rsp)
	jmp	.L4113
.L4444:
	movq	216(%rsp), %rdx
	movq	%rdx, %r10
.L4200:
	testq	%rdx, %rdx
	je	.L4171
	cmpq	$1, %rdx
	je	.L4446
	movl	%edx, %r15d
	cmpl	$8, %edx
	jnb	.L4173
	testb	$4, %dl
	jne	.L4447
	testl	%edx, %edx
	je	.L4174
	movzbl	224(%rsp), %r10d
	andl	$2, %edx
	movb	%r10b, (%r9)
	jne	.L4397
.L4399:
	movq	176(%rsp), %r9
	movq	216(%rsp), %rdx
.L4174:
	movq	%rdx, %r10
	movq	%r9, %r15
.L4171:
	movq	%r10, 184(%rsp)
	movb	$0, (%r15,%r10)
	movq	208(%rsp), %r15
	jmp	.L4179
.L4431:
	movzwl	4(%rsi), %r9d
	cmpq	%r9, %r15
	jnb	.L4421
	movq	%rbx, %r12
	movq	%r15, %r13
	movq	%rcx, %rbx
	jmp	.L4183
.L4149:
	movq	%rbp, %rdx
	movq	%r13, %r8
	leaq	(%rbx,%r10), %rbp
	movq	%r10, 64(%rsp)
	leaq	(%rdx,%r10), %rcx
	subq	%r10, %r8
	movq	%rbp, %rdx
	addq	%rbx, %rcx
	call	memmove
	movq	64(%rsp), %rcx
	cmpq	%rcx, 80(%rsp)
	jne	.L4151
	movq	80(%rsp), %r14
	movb	$46, 0(%rbp)
	leaq	1(%rbx,%r14), %rbp
.L4151:
	movq	%r12, %r8
	movl	$48, %edx
	movq	%rbp, %rcx
	call	memset
	movq	72(%rsp), %r12
	movzbl	(%rsi), %r9d
	addq	%r12, %r13
	jmp	.L4152
.L4233:
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
	jmp	.L4145
.L4445:
	leaq	208(%rsp), %rdx
	vmovapd	%xmm6, %xmm0
	movl	$0, 208(%rsp)
	call	frexp
	movl	208(%rsp), %eax
	testl	%eax, %eax
	jle	.L4117
	imull	$4004, %eax, %edx
	imulq	$995517945, %rdx, %r9
	movq	%rdx, %r8
	shrq	$32, %r9
	subl	%r9d, %r8d
	shrl	%r8d
	addl	%r9d, %r8d
	shrl	$13, %r8d
	incl	%r8d
	addq	%r8, %rbp
.L4117:
	movl	$1, %r9d
	jmp	.L4114
.L4227:
	movq	%r13, %r10
	jmp	.L4142
.L4239:
	movl	$2, %r8d
	movl	$48, %r10d
	jmp	.L4191
.L4219:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$6, %r12d
	movb	$101, 56(%rsp)
	jmp	.L4122
.L4127:
	movq	144(%rsp), %rax
	movq	$0, 152(%rsp)
	movb	$0, (%rax)
	movq	144(%rsp), %r8
	cmpl	$132, %edx
	jne	.L4123
	jmp	.L4128
.L4159:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	movq	144(%rsp), %r11
	jmp	.L4160
.L4436:
	movq	%rdx, 64(%rsp)
	movq	152(%rsp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$48, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %rdx
	jmp	.L4155
.L4441:
	movq	496(%rsp), %r13
	salq	$5, %r11
	addq	8(%r13), %r11
	vmovdqu	(%r11), %xmm5
	vmovdqa	%xmm5, 208(%rsp)
	movzbl	16(%r11), %r15d
	movb	%r15b, 224(%rsp)
	movzbl	16(%r11), %r14d
	jmp	.L4067
.L4435:
	movl	$46, %edx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc
.LEHE60:
	movq	64(%rsp), %rdx
	jmp	.L4154
.L4234:
	movl	$15, %r8d
	jmp	.L4158
.L4220:
	movl	$30, %ebp
	jmp	.L4124
.L4218:
	movl	$30, %ebx
	jmp	.L4121
.L4221:
	movl	$30, %ebp
	jmp	.L4126
.L4439:
	movzbl	(%r9), %r15d
	movb	%r15b, (%rcx)
	movq	144(%rsp), %r11
	jmp	.L4160
.L4173:
	movq	224(%rsp), %r11
	movl	%edx, %r15d
	leaq	8(%r9), %r8
	movq	%rcx, %r10
	andq	$-8, %r8
	movq	%r11, (%r9)
	movq	-8(%rcx,%r15), %rax
	movq	%rax, -8(%r9,%r15)
	subq	%r8, %r9
	leal	(%rdx,%r9), %r15d
	subq	%r9, %r10
	movl	%r15d, %r9d
	andl	$-8, %r9d
	cmpl	$8, %r9d
	jb	.L4399
	xorl	%edx, %edx
	andl	$-8, %r15d
	movq	(%r10,%rdx), %r11
	leal	-1(%r15), %eax
	shrl	$3, %eax
	andl	$7, %eax
	movq	%r11, (%r8,%rdx)
	movl	$8, %edx
	cmpl	%r15d, %edx
	jnb	.L4399
	testl	%eax, %eax
	je	.L4177
	cmpl	$1, %eax
	je	.L4348
	cmpl	$2, %eax
	je	.L4349
	cmpl	$3, %eax
	je	.L4350
	cmpl	$4, %eax
	je	.L4351
	cmpl	$5, %eax
	je	.L4352
	cmpl	$6, %eax
	je	.L4353
	movq	(%r10,%rdx), %r9
	movq	%r9, (%r8,%rdx)
	movl	$16, %edx
.L4353:
	movl	%edx, %eax
	addl	$8, %edx
	movq	(%r10,%rax), %r11
	movq	%r11, (%r8,%rax)
.L4352:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %rax
	movq	%rax, (%r8,%r9)
.L4351:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
.L4350:
	movl	%edx, %eax
	addl	$8, %edx
	movq	(%r10,%rax), %r9
	movq	%r9, (%r8,%rax)
.L4349:
	movl	%edx, %eax
	addl	$8, %edx
	movq	(%r10,%rax), %r11
	movq	%r11, (%r8,%rax)
.L4348:
	movl	%edx, %r9d
	addl	$8, %edx
	movq	(%r10,%r9), %rax
	movq	%rax, (%r8,%r9)
	cmpl	%r15d, %edx
	jnb	.L4399
.L4177:
	movl	%edx, %r9d
	leal	8(%rdx), %eax
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	movq	(%r10,%rax), %r9
	movq	%r9, (%r8,%rax)
	leal	16(%rdx), %eax
	leal	24(%rdx), %r9d
	movq	(%r10,%rax), %r11
	movq	%r11, (%r8,%rax)
	movq	(%r10,%r9), %rax
	movq	%rax, (%r8,%r9)
	leal	32(%rdx), %r9d
	leal	40(%rdx), %eax
	movq	(%r10,%r9), %r11
	movq	%r11, (%r8,%r9)
	movq	(%r10,%rax), %r9
	movq	%r9, (%r8,%rax)
	leal	48(%rdx), %eax
	leal	56(%rdx), %r9d
	addl	$64, %edx
	movq	(%r10,%rax), %r11
	movq	%r11, (%r8,%rax)
	movq	(%r10,%r9), %rax
	movq	%rax, (%r8,%r9)
	cmpl	%r15d, %edx
	jb	.L4177
	jmp	.L4399
	.p2align 4
	.p2align 3
.L4446:
	movzbl	224(%rsp), %r8d
	movb	%r8b, (%r9)
	movq	216(%rsp), %r10
	movq	176(%rsp), %r15
	jmp	.L4171
.L4434:
	movq	%r9, 64(%rsp)
	movq	(%rcx), %r9
	movq	%rcx, 56(%rsp)
.LEHB61:
	call	*(%r9)
.LEHE61:
	movq	64(%rsp), %r9
	movq	56(%rsp), %rcx
	jmp	.L4192
.L4447:
	movl	224(%rsp), %eax
	movl	%eax, (%r9)
	movl	-4(%rcx,%r15), %edx
	movl	%edx, -4(%r9,%r15)
	movq	176(%rsp), %r9
	movq	216(%rsp), %rdx
	jmp	.L4174
.L4397:
	movzwl	-2(%rcx,%r15), %r8d
	movw	%r8w, -2(%r9,%r15)
	movq	176(%rsp), %r9
	movq	216(%rsp), %rdx
	jmp	.L4174
.L4084:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
.LEHB62:
	call	_ZSt20__throw_format_errorPKc
.L4085:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4070:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.LEHE62:
.L4241:
	movq	%r12, %rcx
	movq	%rax, %r13
	vzeroupper
	call	_ZNSt6localeD1Ev
.L4196:
	leaq	176(%rsp), %rcx
	leaq	144(%rsp), %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L4197:
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r13, %rcx
.LEHB63:
	call	_Unwind_Resume
.LEHE63:
.L4443:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
.LEHB64:
	call	_ZSt20__throw_format_errorPKc
.L4438:
	leaq	.LC123(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L4437:
	movq	%rdx, %r8
	movq	%r13, %r9
	leaq	.LC148(%rip), %rdx
	leaq	.LC146(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L4069:
.L4242:
	movq	%rax, %r13
	vzeroupper
	jmp	.L4196
.L4442:
	movq	%r14, %r8
	leaq	.LC149(%rip), %rdx
	leaq	.LC150(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L4240:
	movq	%rax, %r13
	vzeroupper
	jmp	.L4197
.L4072:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4073:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4074:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4075:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4076:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4077:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4078:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4079:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	nop
.LEHE64:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15537:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15537-.LLSDACSB15537
.LLSDACSB15537:
	.uleb128 .LEHB58-.LFB15537
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L4241-.LFB15537
	.uleb128 0
	.uleb128 .LEHB59-.LFB15537
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L4242-.LFB15537
	.uleb128 0
	.uleb128 .LEHB60-.LFB15537
	.uleb128 .LEHE60-.LEHB60
	.uleb128 .L4240-.LFB15537
	.uleb128 0
	.uleb128 .LEHB61-.LFB15537
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L4242-.LFB15537
	.uleb128 0
	.uleb128 .LEHB62-.LFB15537
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L4240-.LFB15537
	.uleb128 0
	.uleb128 .LEHB63-.LFB15537
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB64-.LFB15537
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L4240-.LFB15537
	.uleb128 0
.LLSDACSE15537:
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.seh_endproc
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	.def	_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_:
.LFB15533:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$408, %rsp
	.seh_stackalloc	408
	vmovaps	%xmm6, 368(%rsp)
	.seh_savexmm	%xmm6, 368
	vmovaps	%xmm7, 384(%rsp)
	.seh_savexmm	%xmm7, 384
	.seh_endprologue
	movzbl	1(%rcx), %eax
	leaq	160(%rsp), %rdi
	movq	%rcx, %rsi
	vmovaps	%xmm1, %xmm6
	movq	%r8, 496(%rsp)
	movq	%rdi, 144(%rsp)
	movq	$0, 152(%rsp)
	movb	$0, 160(%rsp)
	movl	%eax, %edx
	andl	$6, %edx
	je	.L4449
	cmpb	$2, %dl
	je	.L4808
	movq	$-1, %r12
	cmpb	$4, %dl
	je	.L4809
.L4451:
	movl	%eax, %ecx
	shrb	$3, %cl
	andl	$15, %ecx
	cmpb	$8, %cl
	ja	.L4455
	leaq	.L4476(%rip), %r13
	movzbl	%cl, %r11d
	movslq	0(%r13,%r11,4), %r15
	addq	%r13, %r15
	jmp	*%r15
	.section .rdata,"dr"
	.align 4
.L4476:
	.long	.L4484-.L4476
	.long	.L4483-.L4476
	.long	.L4482-.L4476
	.long	.L4481-.L4476
	.long	.L4480-.L4476
	.long	.L4479-.L4476
	.long	.L4478-.L4476
	.long	.L4477-.L4476
	.long	.L4475-.L4476
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4449:
	movl	%eax, %ecx
	shrb	$3, %cl
	andl	$15, %ecx
	cmpb	$8, %cl
	ja	.L4455
	leaq	.L4490(%rip), %rbp
	movzbl	%cl, %ebx
	movslq	0(%rbp,%rbx,4), %r8
	addq	%rbp, %r8
	jmp	*%r8
	.section .rdata,"dr"
	.align 4
.L4490:
	.long	.L4495-.L4490
	.long	.L4494-.L4490
	.long	.L4493-.L4490
	.long	.L4592-.L4490
	.long	.L4593-.L4490
	.long	.L4594-.L4490
	.long	.L4595-.L4490
	.long	.L4596-.L4490
	.long	.L4597-.L4490
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4495:
	leaq	112(%rsp), %r13
	leaq	241(%rsp), %rbx
	vmovaps	%xmm1, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	movq	%r13, %rcx
	call	_ZSt8to_charsPcS_f
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4810
	leaq	368(%rsp), %r15
	movl	$6, %r12d
	xorl	%r14d, %r14d
	movb	$101, 56(%rsp)
	movq	%r15, 64(%rsp)
.L4501:
	vmovd	%xmm6, %r8d
	movzbl	(%rsi), %r15d
	testl	%r8d, %r8d
	js	.L4806
	movl	%r15d, %edx
	andl	$12, %edx
	cmpb	$4, %dl
	je	.L4811
	xorl	%ecx, %ecx
	cmpb	$12, %dl
	je	.L4812
.L4517:
	movq	%rbp, %r13
	subq	%rbx, %r13
	testb	$16, %r15b
	je	.L4521
	vandps	.LC153(%rip), %xmm6, %xmm2
	vmovss	.LC154(%rip), %xmm1
	vucomiss	%xmm2, %xmm1
	jb	.L4521
	testq	%r13, %r13
	je	.L4610
	movq	%rcx, 72(%rsp)
	movq	%r13, %r8
	movl	$46, %edx
	movq	%rbx, %rcx
	call	memchr
	movq	72(%rsp), %r9
	testq	%rax, %rax
	je	.L4523
	subq	%rbx, %rax
	movq	%rax, 80(%rsp)
	cmpq	$-1, %rax
	je	.L4523
	leaq	1(%rax), %r10
	cmpq	%r13, %r10
	jnb	.L4813
	movsbl	56(%rsp), %edx
	movq	%r13, %r8
	leaq	(%rbx,%r10), %rcx
	movq	%r9, 88(%rsp)
	subq	%r10, %r8
	movq	%r10, 72(%rsp)
	call	memchr
	movq	72(%rsp), %r11
	movq	88(%rsp), %r9
	testq	%rax, %rax
	je	.L4613
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%r13, %rax
	movq	%rax, %r10
.L4528:
	movq	80(%rsp), %r8
	cmpq	%r8, %r10
	sete	%dl
	movzbl	%dl, %ecx
	movq	%rcx, 72(%rsp)
	testb	%r14b, %r14b
	je	.L4615
	cmpb	$48, (%rbx,%r9)
	je	.L4814
.L4526:
	movq	%r10, %r8
	subq	%r9, %r8
	decq	%r8
.L4530:
	testq	%r12, %r12
	jne	.L4532
	.p2align 4
	.p2align 3
.L4525:
	cmpq	$0, 72(%rsp)
	jne	.L4531
	leaq	192(%rsp), %r12
	andl	$32, %r15d
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%r12, 176(%rsp)
	je	.L4567
.L4589:
	movq	496(%rsp), %rax
	cmpb	$0, 32(%rax)
	leaq	24(%rax), %r15
	je	.L4815
.L4552:
	leaq	136(%rsp), %rbp
	movq	%r15, %rdx
	leaq	96(%rsp), %r14
	movq	%rbp, %rcx
	call	_ZNSt6localeC1ERKS_
	movzbl	56(%rsp), %r8d
	leaq	208(%rsp), %rcx
	movq	%rbp, %r9
	movq	%r14, %rdx
	movq	%r13, 96(%rsp)
	movq	%rbx, 104(%rsp)
.LEHB65:
	call	_ZNKSt8__format14__formatter_fpIcE11_M_localizeB5cxx11ESt17basic_string_viewIcSt11char_traitsIcEEcRKSt6locale.isra.0
.LEHE65:
	movq	176(%rsp), %rax
	movq	%rax, %r15
	cmpq	%r12, %rax
	je	.L4816
	movq	216(%rsp), %r9
	movq	208(%rsp), %rcx
	leaq	224(%rsp), %rdx
	movq	%r9, %r10
	cmpq	%rdx, %rcx
	je	.L4586
	vmovq	%r9, %xmm4
	vpinsrq	$1, 224(%rsp), %xmm4, %xmm0
	movq	192(%rsp), %r11
	movq	%rcx, 176(%rsp)
	vmovdqu	%xmm0, 184(%rsp)
	testq	%rax, %rax
	je	.L4556
	movq	%rax, 208(%rsp)
	movq	%r11, 224(%rsp)
.L4565:
	movq	$0, 216(%rsp)
	movb	$0, (%rax)
	movq	208(%rsp), %rcx
	cmpq	%rdx, %rcx
	je	.L4566
	movq	224(%rsp), %rdx
	leaq	1(%rdx), %rdx
	call	_ZdlPvy
.L4566:
	movq	%rbp, %rcx
	call	_ZNSt6localeD1Ev
	movq	184(%rsp), %r15
	testq	%r15, %r15
	je	.L4567
	movzwl	(%rsi), %r13d
	movq	176(%rsp), %r8
	andw	$384, %r13w
	cmpw	$128, %r13w
	je	.L4817
	cmpw	$256, %r13w
	je	.L4622
.L4807:
	movq	496(%rsp), %rcx
	movq	%r8, %rbx
	movq	%r15, %r13
	movq	16(%rcx), %rsi
.L4570:
	movq	%r14, %rdx
	movq	%rsi, %rcx
	movq	%r13, 96(%rsp)
	movq	%rbx, 104(%rsp)
.LEHB66:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	jmp	.L4576
	.p2align 4
	.p2align 3
.L4521:
	leaq	192(%rsp), %r12
	andl	$32, %r15d
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%r12, 176(%rsp)
	je	.L4567
	vandps	.LC153(%rip), %xmm6, %xmm3
	vmovss	.LC154(%rip), %xmm5
	vucomiss	%xmm3, %xmm5
	jnb	.L4589
.L4567:
	movzwl	(%rsi), %r14d
	andw	$384, %r14w
	cmpw	$128, %r14w
	je	.L4818
	movq	%rbx, %rbp
	cmpw	$256, %r14w
	je	.L4571
.L4574:
	movq	496(%rsp), %rbp
	movq	16(%rbp), %rsi
	testq	%r13, %r13
	jne	.L4819
.L4576:
	movq	176(%rsp), %rcx
	cmpq	%r12, %rcx
	je	.L4579
	movq	192(%rsp), %r12
	leaq	1(%r12), %rdx
	call	_ZdlPvy
.L4579:
	movq	144(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L4640
	movq	160(%rsp), %rdi
	leaq	1(%rdi), %rdx
	call	_ZdlPvy
	nop
.L4640:
	vmovaps	368(%rsp), %xmm6
	vmovaps	384(%rsp), %xmm7
	movq	%rsi, %rax
	addq	$408, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L4812:
	movb	$32, -1(%rbx)
	movzbl	(%rsi), %r15d
	decq	%rbx
	.p2align 4
	.p2align 3
.L4806:
	movl	$1, %ecx
	jmp	.L4517
	.p2align 4
	.p2align 3
.L4818:
	movzwl	4(%rsi), %r9d
	movq	%rbx, %rbp
.L4573:
	cmpq	%r9, %r13
	jnb	.L4574
.L4569:
	movl	8(%rsi), %edx
	movzbl	(%rsi), %esi
	movq	496(%rsp), %r10
	subq	%r13, %r9
	movl	%esi, %r8d
	movq	16(%r10), %rcx
	andl	$3, %r8d
	jne	.L4577
	testb	$64, %sil
	je	.L4624
	vandps	.LC153(%rip), %xmm6, %xmm2
	vmovss	.LC154(%rip), %xmm1
	vucomiss	%xmm2, %xmm1
	jb	.L4624
	movzbl	0(%rbp), %r11d
	leaq	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE(%rip), %rax
	cmpb	$15, (%rax,%r11)
	jbe	.L4625
	movq	24(%rcx), %r14
	movzbl	(%rbx), %r15d
	leaq	1(%r14), %r8
	movq	%r8, 24(%rcx)
	movb	%r15b, (%r14)
	movq	24(%rcx), %rbp
	subq	8(%rcx), %rbp
	cmpq	16(%rcx), %rbp
	je	.L4820
.L4578:
	incq	%rbx
	decq	%r13
	movl	$2, %r8d
	movl	$48, %edx
	.p2align 4
	.p2align 3
.L4577:
	movq	%rbx, 104(%rsp)
	leaq	96(%rsp), %rbx
	movl	%edx, 32(%rsp)
	movq	%r13, 96(%rsp)
	movq	%rbx, %rdx
	call	_ZNSt8__format14__write_paddedINS_10_Sink_iterIcEEcEET_S3_St17basic_string_viewIT0_St11char_traitsIS5_EENS_6_AlignEyDi
	movq	%rax, %rsi
	jmp	.L4576
	.p2align 4
	.p2align 3
.L4808:
	movzwl	6(%rcx), %r12d
	jmp	.L4451
	.p2align 4
	.p2align 3
.L4610:
	xorl	%r10d, %r10d
.L4522:
	testb	%r14b, %r14b
	je	.L4618
	testq	%r12, %r12
	je	.L4619
	movq	%r10, %r8
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
	subq	%rcx, %r8
.L4532:
	subq	%r8, %r12
	addq	%r12, 72(%rsp)
	jmp	.L4525
	.p2align 4
	.p2align 3
.L4478:
	movl	%r12d, %r10d
.L4491:
	movl	$1, %r13d
.L4487:
	movl	$2, %ebx
	xorl	%r14d, %r14d
	movb	$101, 56(%rsp)
.L4497:
	leaq	241(%rsp), %r15
	leaq	112(%rsp), %rcx
	vmovaps	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%r15, %rdx
	vmovq	%rcx, %xmm7
	movl	%r10d, 40(%rsp)
	movl	%ebx, 32(%rsp)
	call	_ZSt8to_charsPcS_fSt12chars_formati
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4588
	leaq	368(%rsp), %r11
	movq	%r15, %rbx
	movq	%r11, 64(%rsp)
.L4499:
	testb	%r13b, %r13b
	je	.L4501
	cmpq	%rbp, %rbx
	je	.L4501
	movq	%rbp, %rax
	movq	__imp_toupper(%rip), %r13
	movq	%rbx, %r15
	subq	%rbx, %rax
	andl	$7, %eax
	je	.L4518
	cmpq	$1, %rax
	je	.L4726
	cmpq	$2, %rax
	je	.L4727
	cmpq	$3, %rax
	je	.L4728
	cmpq	$4, %rax
	je	.L4729
	cmpq	$5, %rax
	je	.L4730
	cmpq	$6, %rax
	je	.L4731
	movsbl	(%rbx), %ecx
	leaq	1(%rbx), %r15
	call	*%r13
	movb	%al, (%rbx)
.L4731:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4730:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4729:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4728:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4727:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
.L4726:
	movsbl	(%r15), %ecx
	incq	%r15
	call	*%r13
	movb	%al, -1(%r15)
	cmpq	%r15, %rbp
	je	.L4501
.L4518:
	movsbl	(%r15), %ecx
	addq	$8, %r15
	call	*%r13
	movsbl	-7(%r15), %ecx
	movb	%al, -8(%r15)
	call	*%r13
	movsbl	-6(%r15), %ecx
	movb	%al, -7(%r15)
	call	*%r13
	movsbl	-5(%r15), %ecx
	movb	%al, -6(%r15)
	call	*%r13
	movsbl	-4(%r15), %ecx
	movb	%al, -5(%r15)
	call	*%r13
	movsbl	-3(%r15), %ecx
	movb	%al, -4(%r15)
	call	*%r13
	movsbl	-2(%r15), %ecx
	movb	%al, -3(%r15)
	call	*%r13
	movsbl	-1(%r15), %ecx
	movb	%al, -2(%r15)
	call	*%r13
	movb	%al, -1(%r15)
	cmpq	%r15, %rbp
	jne	.L4518
	jmp	.L4501
	.p2align 4
	.p2align 3
.L4479:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	jmp	.L4487
	.p2align 4
	.p2align 3
.L4480:
	movl	%r12d, %r10d
.L4492:
	movl	$1, %r13d
	movb	$69, 56(%rsp)
.L4488:
	movl	$1, %ebx
	xorl	%r14d, %r14d
	jmp	.L4497
	.p2align 4
	.p2align 3
.L4481:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4488
	.p2align 4
	.p2align 3
.L4482:
	andl	$120, %eax
	movl	$80, %r14d
	movl	$1, %r13d
	cmpb	$16, %al
	movl	$112, %eax
	cmove	%r14d, %eax
	movb	%al, 56(%rsp)
.L4485:
	leaq	241(%rsp), %rbx
	leaq	112(%rsp), %rcx
	vmovaps	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm7
	movl	%r12d, 40(%rsp)
	movl	$4, 32(%rsp)
	call	_ZSt8to_charsPcS_fSt12chars_formati
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	jne	.L4805
	leaq	8(%r12), %r9
	movl	$1, %ebp
	movl	$4, %ebx
	xorl	%r14d, %r14d
	jmp	.L4500
	.p2align 4
	.p2align 3
.L4483:
	andl	$120, %eax
	movl	$112, %edx
	movl	$101, %ebx
	cmpb	$16, %al
	cmovne	%edx, %ebx
	xorl	%r13d, %r13d
	movb	%bl, 56(%rsp)
	jmp	.L4485
	.p2align 4
	.p2align 3
.L4484:
	movl	%r12d, %r10d
	movl	$3, %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4497
	.p2align 4
	.p2align 3
.L4475:
	movl	%r12d, %r10d
.L4489:
	movl	$1, %r13d
	movb	$69, 56(%rsp)
.L4486:
	movl	$3, %ebx
	movl	$1, %r14d
	jmp	.L4497
	.p2align 4
	.p2align 3
.L4493:
	andl	$120, %eax
	movl	$80, %r9d
	movl	$112, %r10d
	movl	$1, %r13d
	cmpb	$16, %al
	cmove	%r9d, %r10d
	movb	%r10b, 56(%rsp)
.L4496:
	leaq	241(%rsp), %rbx
	leaq	112(%rsp), %rcx
	vmovaps	%xmm6, %xmm3
	leaq	368(%rsp), %r8
	movq	%rbx, %rdx
	vmovq	%rcx, %xmm7
	movl	$4, 32(%rsp)
	movl	$6, %r12d
	call	_ZSt8to_charsPcS_fSt12chars_format
	cmpl	$132, 120(%rsp)
	movq	112(%rsp), %rbp
	je	.L4601
.L4805:
	leaq	368(%rsp), %r8
	xorl	%r14d, %r14d
	movq	%r8, 64(%rsp)
	jmp	.L4499
	.p2align 4
	.p2align 3
.L4592:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4488
	.p2align 4
	.p2align 3
.L4593:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4492
	.p2align 4
	.p2align 3
.L4594:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	jmp	.L4487
	.p2align 4
	.p2align 3
.L4597:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4489
	.p2align 4
	.p2align 3
.L4477:
	movl	%r12d, %r10d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4486
	.p2align 4
	.p2align 3
.L4595:
	movl	$6, %r10d
	movl	$6, %r12d
	jmp	.L4491
	.p2align 4
	.p2align 3
.L4596:
	movl	$6, %r10d
	movl	$6, %r12d
	xorl	%r13d, %r13d
	movb	$101, 56(%rsp)
	jmp	.L4486
	.p2align 4
	.p2align 3
.L4494:
	andl	$120, %eax
	movl	$101, %r11d
	movl	$112, %r12d
	cmpb	$16, %al
	cmove	%r11d, %r12d
	xorl	%r13d, %r13d
	movb	%r12b, 56(%rsp)
	jmp	.L4496
	.p2align 4
	.p2align 3
.L4624:
	movl	$2, %r8d
	movl	$32, %edx
	jmp	.L4577
	.p2align 4
	.p2align 3
.L4819:
	leaq	96(%rsp), %r14
	jmp	.L4570
	.p2align 4
	.p2align 3
.L4811:
	movb	$43, -1(%rbx)
	movl	$1, %ecx
	movzbl	(%rsi), %r15d
	decq	%rbx
	jmp	.L4517
.L4622:
	movq	%rbx, %rbp
	movq	%r15, %r13
	movq	%r8, %rbx
	.p2align 4
	.p2align 3
.L4571:
	movzwl	4(%rsi), %ecx
	movq	496(%rsp), %rdx
	call	_ZNKSt8__format5_SpecIcE12_M_get_widthISt20basic_format_contextINS_10_Sink_iterIcEEcEEEyRT_.part.0.isra.0
.LEHE66:
	movq	%rax, %r9
	jmp	.L4573
.L4618:
	xorl	%r12d, %r12d
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
.L4531:
	cmpq	$0, 152(%rsp)
	jne	.L4534
	movq	64(%rsp), %r9
	subq	%rbp, %r9
	movq	72(%rsp), %rbp
	cmpq	%rbp, %r9
	jnb	.L4535
.L4534:
	movq	72(%rsp), %rdx
	leaq	144(%rsp), %r15
	movq	%r10, 64(%rsp)
	movq	%r15, %rcx
	leaq	0(%r13,%rdx), %rdx
.LEHB67:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	152(%rsp), %r9
	movq	64(%rsp), %rcx
	testq	%r9, %r9
	jne	.L4539
	cmpq	%rcx, %r13
	movq	%rcx, %r10
	movq	%rbx, %r9
	movq	%r15, %rcx
	cmovbe	%r13, %r10
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r10, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
	movq	64(%rsp), %r14
	cmpq	%r14, 80(%rsp)
	je	.L4821
.L4540:
	testq	%r12, %r12
	jne	.L4822
.L4541:
	cmpq	%r14, %r13
	jb	.L4823
	movq	152(%rsp), %rdx
	movabsq	$9223372036854775807, %rax
	subq	%r14, %r13
	leaq	(%rbx,%r14), %r9
	subq	%rdx, %rax
	cmpq	%r13, %rax
	jb	.L4824
	movq	144(%rsp), %r8
	leaq	0(%r13,%rdx), %rbp
	cmpq	%rdi, %r8
	je	.L4620
	movq	160(%rsp), %r11
.L4544:
	cmpq	%rbp, %r11
	jb	.L4545
	testq	%r13, %r13
	je	.L4546
	leaq	(%r8,%rdx), %rcx
	cmpq	$1, %r13
	je	.L4825
	movq	%r13, %r8
	movq	%r9, %rdx
	call	memcpy
	movq	144(%rsp), %r8
.L4546:
	movq	%rbp, 152(%rsp)
	movb	$0, (%r8,%rbp)
	jmp	.L4548
	.p2align 4
	.p2align 3
.L4809:
	movzbl	(%r8), %r12d
	movzwl	6(%rcx), %r11d
	movl	%r12d, %ecx
	andl	$15, %r12d
	andl	$15, %ecx
	cmpq	%r12, %r11
	jnb	.L4452
	movq	(%r8), %rdx
	movq	496(%rsp), %r8
	leaq	(%r11,%r11,4), %rbx
	salq	$4, %r11
	addq	8(%r8), %r11
	movq	%rdx, 56(%rsp)
	shrq	$4, %rdx
	shrx	%rbx, %rdx, %r14
	andl	$31, %r14d
	vmovdqa	(%r11), %xmm4
	vmovdqa	%xmm4, 208(%rsp)
.L4453:
	leaq	.L4457(%rip), %r10
	movzbl	%r14b, %r9d
	movb	%r14b, 224(%rsp)
	vmovdqu	208(%rsp), %ymm0
	movslq	(%r10,%r9,4), %rbp
	addq	%r10, %rbp
	vmovdqu	%ymm0, 176(%rsp)
	jmp	*%rbp
	.section .rdata,"dr"
	.align 4
.L4457:
	.long	.L4803-.L4457
	.long	.L4471-.L4457
	.long	.L4470-.L4457
	.long	.L4469-.L4457
	.long	.L4468-.L4457
	.long	.L4467-.L4457
	.long	.L4466-.L4457
	.long	.L4465-.L4457
	.long	.L4464-.L4457
	.long	.L4463-.L4457
	.long	.L4462-.L4457
	.long	.L4461-.L4457
	.long	.L4460-.L4457
	.long	.L4459-.L4457
	.long	.L4458-.L4457
	.long	.L4456-.L4457
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4813:
	cmpq	%rax, %r13
	movq	%r13, %r10
	sete	%r11b
	movzbl	%r11b, %eax
	movq	%rax, 72(%rsp)
	testb	%r14b, %r14b
	jne	.L4826
.L4615:
	xorl	%r12d, %r12d
	jmp	.L4525
	.p2align 4
	.p2align 3
.L4523:
	movsbl	56(%rsp), %edx
	movq	%rbx, %rcx
	movq	%r13, %r8
	movq	%r9, 72(%rsp)
	call	memchr
	movq	72(%rsp), %rcx
	testq	%rax, %rax
	je	.L4616
	subq	%rbx, %rax
	cmpq	$-1, %rax
	cmove	%r13, %rax
	movq	%rax, %r10
	jmp	.L4522
	.p2align 4
	.p2align 3
.L4815:
	movq	%r15, %rcx
	call	_ZNSt6localeC1Ev
	movq	496(%rsp), %rbp
	movb	$1, 32(%rbp)
	jmp	.L4552
	.p2align 4
	.p2align 3
.L4452:
	testb	%cl, %cl
	jne	.L4454
	movq	(%r8), %rdx
	movq	%rdx, 56(%rsp)
	shrq	$4, %rdx
	cmpq	%rdx, %r11
	jb	.L4827
.L4454:
	leaq	144(%rsp), %r15
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L4539:
	cmpq	%rcx, %r9
	jb	.L4828
	movq	72(%rsp), %r9
	movq	%rcx, %rdx
	movq	%rcx, 64(%rsp)
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movl	$48, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %rbx
	cmpq	%rbx, 80(%rsp)
	jne	.L4548
	movq	144(%rsp), %r13
	movq	80(%rsp), %r12
	movb	$46, 0(%r13,%r12)
	.p2align 4
	.p2align 3
.L4548:
	movq	152(%rsp), %r13
	movq	144(%rsp), %rbx
	movzbl	(%rsi), %r9d
.L4538:
	leaq	192(%rsp), %r12
	movq	$0, 184(%rsp)
	movb	$0, 192(%rsp)
	movq	%r12, 176(%rsp)
	testb	$32, %r9b
	je	.L4567
	jmp	.L4589
.L4826:
	cmpb	$48, (%rbx,%r9)
	jne	.L4526
.L4788:
	movq	$-1, %r11
.L4527:
	movq	%r10, %r8
	subq	%r11, %r8
	jmp	.L4530
.L4814:
	movq	%r11, %r14
	notq	%r14
	addq	%r13, %r14
	andl	$7, %r14d
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
	cmpq	%r13, %r11
	jnb	.L4788
	testq	%r14, %r14
	je	.L4529
	cmpq	$1, %r14
	je	.L4741
	cmpq	$2, %r14
	je	.L4742
	cmpq	$3, %r14
	je	.L4743
	cmpq	$4, %r14
	je	.L4744
	cmpq	$5, %r14
	je	.L4745
	cmpq	$6, %r14
	je	.L4746
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4746:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4745:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4744:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4743:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4742:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
.L4741:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	incq	%r11
	cmpq	%r13, %r11
	jnb	.L4788
.L4529:
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	leaq	1(%r11), %rax
	cmpb	$48, (%rbx,%rax)
	movq	%rax, %r11
	jne	.L4527
	incq	%r11
	cmpb	$48, (%rbx,%r11)
	jne	.L4527
	cmpb	$48, 2(%rbx,%rax)
	leaq	2(%rax), %r11
	jne	.L4527
	cmpb	$48, 3(%rbx,%rax)
	leaq	3(%rax), %r11
	jne	.L4527
	cmpb	$48, 4(%rbx,%rax)
	leaq	4(%rax), %r11
	jne	.L4527
	cmpb	$48, 5(%rbx,%rax)
	leaq	5(%rax), %r11
	jne	.L4527
	cmpb	$48, 6(%rbx,%rax)
	leaq	6(%rax), %r11
	jne	.L4527
	leaq	7(%rax), %r11
	cmpq	%r13, %r11
	jb	.L4529
	jmp	.L4788
	.p2align 4
	.p2align 3
.L4616:
	movq	%r13, %r10
	jmp	.L4522
.L4466:
	movq	176(%rsp), %r12
	vzeroupper
	jmp	.L4451
.L4467:
	movq	176(%rsp), %r12
	testq	%r12, %r12
	js	.L4829
.L4804:
	vzeroupper
	jmp	.L4451
.L4468:
	movl	176(%rsp), %r12d
	vzeroupper
	jmp	.L4451
.L4469:
	movslq	176(%rsp), %r12
	testl	%r12d, %r12d
	jns	.L4804
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	.p2align 4
	.p2align 3
.L4803:
	vzeroupper
	jmp	.L4454
.L4816:
	movq	208(%rsp), %r8
	leaq	224(%rsp), %rdx
	cmpq	%rdx, %r8
	je	.L4830
	vmovdqu	216(%rsp), %xmm7
	movq	%r8, 176(%rsp)
	vmovdqu	%xmm7, 184(%rsp)
.L4556:
	movq	%rdx, 208(%rsp)
	leaq	224(%rsp), %rdx
	movq	%rdx, %rax
	jmp	.L4565
.L4810:
	leaq	144(%rsp), %r15
	movl	$256, %edx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r8
.L4502:
	cmpq	%rdi, %r8
	je	.L4604
	movq	160(%rsp), %r14
	leaq	(%r14,%r14), %rbx
.L4507:
	movq	%rbx, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %rax
	vmovaps	%xmm6, %xmm3
	movq	%r13, %rcx
	leaq	1(%rax), %rdx
	leaq	-1(%rax,%rbx), %r8
	movq	%rax, 56(%rsp)
	call	_ZSt8to_charsPcS_f
	movq	120(%rsp), %rdx
	movq	112(%rsp), %r8
	movq	56(%rsp), %r9
	testl	%edx, %edx
	movq	%r8, %rbp
	je	.L4605
	movq	144(%rsp), %r10
	movq	$0, 152(%rsp)
	movb	$0, (%r10)
	movq	144(%rsp), %r8
	cmpl	$132, %edx
	je	.L4502
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$6, %r12d
	movb	$101, 56(%rsp)
.L4509:
	leaq	1(%r8), %rbx
	addq	152(%rsp), %r8
	movq	%r8, 64(%rsp)
	jmp	.L4499
.L4601:
	xorl	%ebp, %ebp
	movl	$4, %ebx
	xorl	%r14d, %r14d
.L4504:
	leaq	144(%rsp), %r15
	movl	$256, %edx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r8
	testb	%bpl, %bpl
	jne	.L4514
.L4511:
	cmpq	%rdi, %r8
	je	.L4606
	movq	160(%rsp), %rdx
	leaq	(%rdx,%rdx), %rbp
.L4510:
	movq	%rbp, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r10
	vmovq	%xmm7, %rcx
	vmovaps	%xmm6, %xmm3
	movl	%ebx, 32(%rsp)
	leaq	-1(%r10,%rbp), %r8
	leaq	1(%r10), %rdx
	movq	%r10, 64(%rsp)
	call	_ZSt8to_charsPcS_fSt12chars_format
	movq	120(%rsp), %rcx
	movq	112(%rsp), %r8
	movq	64(%rsp), %r9
	testl	%ecx, %ecx
	movq	%r8, %rbp
	je	.L4508
	movq	144(%rsp), %r9
	movq	$0, 152(%rsp)
	movb	$0, (%r9)
	movq	144(%rsp), %r8
	cmpl	$132, %ecx
	jne	.L4509
	jmp	.L4511
.L4588:
	leaq	8(%r12), %r9
	movl	$1, %ebp
	cmpl	$2, %ebx
	je	.L4831
.L4500:
	cmpq	$128, %r9
	jbe	.L4504
	leaq	144(%rsp), %r15
	movq	%r9, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %r8
.L4514:
	cmpq	%rdi, %r8
	je	.L4607
	movq	160(%rsp), %r9
	leaq	(%r9,%r9), %rbp
.L4512:
	movq	%rbp, %rdx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEy
	movq	144(%rsp), %rcx
	vmovaps	%xmm6, %xmm3
	movl	%r12d, 40(%rsp)
	movl	%ebx, 32(%rsp)
	leaq	-1(%rcx,%rbp), %r8
	leaq	1(%rcx), %rdx
	movq	%rcx, 64(%rsp)
	vmovq	%xmm7, %rcx
	call	_ZSt8to_charsPcS_fSt12chars_formati
	movq	112(%rsp), %r8
	movq	120(%rsp), %r11
	movq	%r8, %rbp
	testl	%r11d, %r11d
	jne	.L4513
	movq	64(%rsp), %r9
.L4508:
	movq	144(%rsp), %r15
	subq	%r9, %r8
	movq	%r8, 152(%rsp)
	movb	$0, (%r15,%r8)
	movq	144(%rsp), %r11
	leaq	1(%r11), %rbx
	addq	152(%rsp), %r11
	movq	%r11, 64(%rsp)
	jmp	.L4499
.L4830:
	movq	216(%rsp), %r9
	movq	%r9, %r10
.L4586:
	testq	%r9, %r9
	je	.L4557
	cmpq	$1, %r9
	je	.L4832
	movl	%r9d, %eax
	cmpl	$8, %r9d
	jnb	.L4559
	testb	$4, %r9b
	jne	.L4833
	testl	%r9d, %r9d
	je	.L4560
	movzbl	224(%rsp), %r10d
	andl	$2, %r9d
	movb	%r10b, (%r15)
	jne	.L4783
.L4785:
	movq	176(%rsp), %r15
	movq	216(%rsp), %r9
.L4560:
	movq	%r9, %r10
	movq	%r15, %rax
.L4557:
	movq	%r10, 184(%rsp)
	movb	$0, (%rax,%r10)
	movq	208(%rsp), %rax
	jmp	.L4565
.L4817:
	movzwl	4(%rsi), %r9d
	cmpq	%r9, %r15
	jnb	.L4807
	movq	%rbx, %rbp
	movq	%r15, %r13
	movq	%r8, %rbx
	jmp	.L4569
.L4535:
	leaq	(%rbx,%r10), %r14
	leaq	0(%rbp,%r10), %rcx
	movq	%r13, %r8
	movq	%r10, 64(%rsp)
	addq	%rbx, %rcx
	subq	%r10, %r8
	movq	%r14, %rdx
	call	memmove
	movq	64(%rsp), %rcx
	cmpq	%rcx, 80(%rsp)
	jne	.L4537
	movq	80(%rsp), %r10
	movb	$46, (%r14)
	leaq	1(%rbx,%r10), %r14
.L4537:
	movq	%r12, %r8
	movl	$48, %edx
	movq	%r14, %rcx
	call	memset
	movq	72(%rsp), %r12
	movzbl	(%rsi), %r9d
	addq	%r12, %r13
	jmp	.L4538
.L4619:
	movq	%r10, 80(%rsp)
	movq	$1, 72(%rsp)
	jmp	.L4531
.L4831:
	leaq	208(%rsp), %rdx
	vmovaps	%xmm6, %xmm0
	movq	%r9, 64(%rsp)
	movl	$0, 208(%rsp)
	call	frexpf
	movl	208(%rsp), %eax
	movq	64(%rsp), %r9
	testl	%eax, %eax
	jle	.L4503
	imull	$4004, %eax, %r8d
	imulq	$995517945, %r8, %r10
	movq	%r8, %rdx
	shrq	$32, %r10
	subl	%r10d, %edx
	shrl	%edx
	addl	%r10d, %edx
	shrl	$13, %edx
	incl	%edx
	addq	%rdx, %r9
.L4503:
	movl	$1, %ebp
	jmp	.L4500
.L4613:
	movq	%r13, %r10
	jmp	.L4528
.L4625:
	movl	$2, %r8d
	movl	$48, %edx
	jmp	.L4577
.L4605:
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movl	$6, %r12d
	movb	$101, 56(%rsp)
	jmp	.L4508
.L4513:
	movq	144(%rsp), %rax
	movq	$0, 152(%rsp)
	movb	$0, (%rax)
	movq	144(%rsp), %r8
	cmpl	$132, %r11d
	jne	.L4509
	jmp	.L4514
.L4545:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
	movq	144(%rsp), %r8
	jmp	.L4546
.L4822:
	movq	152(%rsp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, 64(%rsp)
	movl	$48, 32(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEyyyc.isra.0
	movq	64(%rsp), %r14
	jmp	.L4541
.L4827:
	movq	496(%rsp), %r13
	salq	$5, %r11
	addq	8(%r13), %r11
	vmovdqu	(%r11), %xmm5
	vmovdqa	%xmm5, 208(%rsp)
	movzbl	16(%r11), %r15d
	movb	%r15b, 224(%rsp)
	movzbl	16(%r11), %r14d
	jmp	.L4453
.L4821:
	movl	$46, %edx
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9push_backEc
.LEHE67:
	movq	64(%rsp), %r14
	jmp	.L4540
.L4620:
	movl	$15, %r11d
	jmp	.L4544
.L4606:
	movl	$30, %ebp
	jmp	.L4510
.L4604:
	movl	$30, %ebx
	jmp	.L4507
.L4607:
	movl	$30, %ebp
	jmp	.L4512
.L4825:
	movzbl	(%r9), %r15d
	movb	%r15b, (%rcx)
	movq	144(%rsp), %r8
	jmp	.L4546
.L4559:
	movq	224(%rsp), %r8
	movl	%r9d, %eax
	movq	%r8, (%r15)
	movq	-8(%rdx,%rax), %rcx
	movq	%rdx, %r8
	movq	%rcx, -8(%r15,%rax)
	leaq	8(%r15), %rcx
	andq	$-8, %rcx
	subq	%rcx, %r15
	subq	%r15, %r8
	addl	%r9d, %r15d
	movl	%r15d, %r9d
	andl	$-8, %r9d
	cmpl	$8, %r9d
	jb	.L4785
	xorl	%r10d, %r10d
	andl	$-8, %r15d
	movq	(%r8,%r10), %r11
	leal	-1(%r15), %eax
	shrl	$3, %eax
	andl	$7, %eax
	movq	%r11, (%rcx,%r10)
	movl	$8, %r10d
	cmpl	%r15d, %r10d
	jnb	.L4785
	testl	%eax, %eax
	je	.L4563
	cmpl	$1, %eax
	je	.L4734
	cmpl	$2, %eax
	je	.L4735
	cmpl	$3, %eax
	je	.L4736
	cmpl	$4, %eax
	je	.L4737
	cmpl	$5, %eax
	je	.L4738
	cmpl	$6, %eax
	je	.L4739
	movq	(%r8,%r10), %r9
	movq	%r9, (%rcx,%r10)
	movl	$16, %r10d
.L4739:
	movl	%r10d, %eax
	addl	$8, %r10d
	movq	(%r8,%rax), %r11
	movq	%r11, (%rcx,%rax)
.L4738:
	movl	%r10d, %eax
	addl	$8, %r10d
	movq	(%r8,%rax), %r9
	movq	%r9, (%rcx,%rax)
.L4737:
	movl	%r10d, %r11d
	addl	$8, %r10d
	movq	(%r8,%r11), %rax
	movq	%rax, (%rcx,%r11)
.L4736:
	movl	%r10d, %r11d
	addl	$8, %r10d
	movq	(%r8,%r11), %r9
	movq	%r9, (%rcx,%r11)
.L4735:
	movl	%r10d, %eax
	addl	$8, %r10d
	movq	(%r8,%rax), %r11
	movq	%r11, (%rcx,%rax)
.L4734:
	movl	%r10d, %eax
	addl	$8, %r10d
	movq	(%r8,%rax), %r9
	movq	%r9, (%rcx,%rax)
	cmpl	%r15d, %r10d
	jnb	.L4785
.L4563:
	movl	%r10d, %eax
	leal	8(%r10), %r9d
	movq	(%r8,%rax), %r11
	movq	%r11, (%rcx,%rax)
	movq	(%r8,%r9), %rax
	movq	%rax, (%rcx,%r9)
	leal	16(%r10), %r9d
	leal	24(%r10), %eax
	movq	(%r8,%r9), %r11
	movq	%r11, (%rcx,%r9)
	movq	(%r8,%rax), %r9
	movq	%r9, (%rcx,%rax)
	leal	32(%r10), %eax
	leal	40(%r10), %r9d
	movq	(%r8,%rax), %r11
	movq	%r11, (%rcx,%rax)
	movq	(%r8,%r9), %rax
	movq	%rax, (%rcx,%r9)
	leal	48(%r10), %r9d
	leal	56(%r10), %eax
	addl	$64, %r10d
	movq	(%r8,%r9), %r11
	movq	%r11, (%rcx,%r9)
	movq	(%r8,%rax), %r9
	movq	%r9, (%rcx,%rax)
	cmpl	%r15d, %r10d
	jb	.L4563
	jmp	.L4785
	.p2align 4
	.p2align 3
.L4832:
	movzbl	224(%rsp), %ecx
	movb	%cl, (%r15)
	movq	216(%rsp), %r10
	movq	176(%rsp), %rax
	jmp	.L4557
.L4820:
	movq	%r9, 64(%rsp)
	movq	(%rcx), %r9
	movq	%rcx, 56(%rsp)
.LEHB68:
	call	*(%r9)
.LEHE68:
	movq	64(%rsp), %r9
	movq	56(%rsp), %rcx
	jmp	.L4578
.L4833:
	movl	224(%rsp), %ecx
	movl	%ecx, (%r15)
	movl	-4(%rdx,%rax), %r9d
	movl	%r9d, -4(%r15,%rax)
	movq	176(%rsp), %r15
	movq	216(%rsp), %r9
	jmp	.L4560
.L4783:
	movzwl	-2(%rdx,%rax), %r11d
	movw	%r11w, -2(%r15,%rax)
	movq	176(%rsp), %r15
	movq	216(%rsp), %r9
	jmp	.L4560
.L4470:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
.LEHB69:
	call	_ZSt20__throw_format_errorPKc
.L4471:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4456:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.LEHE69:
.L4627:
	movq	%rbp, %rcx
	movq	%rax, %r13
	vzeroupper
	call	_ZNSt6localeD1Ev
.L4582:
	leaq	176(%rsp), %rcx
	leaq	144(%rsp), %r15
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L4583:
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r13, %rcx
.LEHB70:
	call	_Unwind_Resume
.LEHE70:
.L4829:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
.LEHB71:
	call	_ZSt20__throw_format_errorPKc
.L4824:
	leaq	.LC123(%rip), %rcx
	call	_ZSt20__throw_length_errorPKc
.L4823:
	movq	%r13, %r9
	movq	%r14, %r8
	leaq	.LC148(%rip), %rdx
	leaq	.LC146(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L4455:
.L4628:
	movq	%rax, %r13
	vzeroupper
	jmp	.L4582
.L4828:
	movq	%rcx, %r8
	leaq	.LC149(%rip), %rdx
	leaq	.LC150(%rip), %rcx
	call	_ZSt24__throw_out_of_range_fmtPKcz
.L4626:
	movq	%rax, %r13
	vzeroupper
	jmp	.L4583
.L4458:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4459:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4460:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4461:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4462:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4463:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4464:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
.L4465:
	leaq	.LC6(%rip), %rcx
	leaq	144(%rsp), %r15
	vzeroupper
	call	_ZSt20__throw_format_errorPKc
	nop
.LEHE71:
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15533:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15533-.LLSDACSB15533
.LLSDACSB15533:
	.uleb128 .LEHB65-.LFB15533
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L4627-.LFB15533
	.uleb128 0
	.uleb128 .LEHB66-.LFB15533
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L4628-.LFB15533
	.uleb128 0
	.uleb128 .LEHB67-.LFB15533
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L4626-.LFB15533
	.uleb128 0
	.uleb128 .LEHB68-.LFB15533
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L4628-.LFB15533
	.uleb128 0
	.uleb128 .LEHB69-.LFB15533
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L4626-.LFB15533
	.uleb128 0
	.uleb128 .LEHB70-.LFB15533
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB71-.LFB15533
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L4626-.LFB15533
	.uleb128 0
.LLSDACSE15533:
	.section	.text$_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC155:
	.ascii "format error: format-spec contains invalid formatting options for 'bool'\0"
	.align 8
.LC156:
	.ascii "format error: format-spec contains invalid formatting options for 'charT'\0"
	.section	.text$_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE
	.def	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE
_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE:
.LFB15254:
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$272, %rsp
	.seh_stackalloc	272
	.seh_endprologue
	movq	%rdx, %rbx
	movzbl	%r8b, %r8d
	leaq	.L4837(%rip), %rdx
	movq	%rcx, %rsi
	movslq	(%rdx,%r8,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section .rdata,"dr"
	.align 4
.L4837:
	.long	.L4852-.L4837
	.long	.L4851-.L4837
	.long	.L4850-.L4837
	.long	.L4849-.L4837
	.long	.L4848-.L4837
	.long	.L4847-.L4837
	.long	.L4846-.L4837
	.long	.L4845-.L4837
	.long	.L4844-.L4837
	.long	.L4843-.L4837
	.long	.L4842-.L4837
	.long	.L4841-.L4837
	.long	.L4840-.L4837
	.long	.L4839-.L4837
	.long	.L4838-.L4837
	.long	.L4836-.L4837
	.section	.text$_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L4838:
	movq	(%rbx), %rbp
	leaq	192(%rsp), %rdi
	movl	$1, %r8d
	movq	$0, 192(%rsp)
	movq	%rdi, %rcx
	movl	$32, 200(%rsp)
	leaq	8(%rbp), %rdx
.LEHB72:
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	(%rbx), %rcx
	vmovdqa	(%rsi), %xmm3
	leaq	64(%rsp), %rdx
	movq	%rax, 8(%rbp)
	movq	48(%rcx), %r8
	movq	%rdi, %rcx
	vmovdqa	%xmm3, 64(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r9
	movq	%rax, 16(%r9)
.L5418:
	addq	$272, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4
	.p2align 3
.L4836:
	movq	(%rbx), %rbp
	leaq	192(%rsp), %rdi
	movl	$1, %r8d
	movq	$0, 192(%rsp)
	movq	%rdi, %rcx
	movl	$32, 200(%rsp)
	leaq	8(%rbp), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	(%rbx), %r8
	vmovdqa	(%rsi), %xmm4
	leaq	64(%rsp), %rdx
	movq	%rdi, %rcx
	movq	%rax, 8(%rbp)
	movq	48(%r8), %r8
	vmovdqa	%xmm4, 64(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r11
	movq	%rax, 16(%r11)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4851:
	movq	(%rbx), %rbp
	leaq	148(%rsp), %r14
	xorl	%r8d, %r8d
	movq	$0, 148(%rsp)
	movq	%r14, %rcx
	movl	$32, 156(%rsp)
	leaq	8(%rbp), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
.LEHE72:
	movzbl	149(%rsp), %r8d
	andl	$120, %r8d
	jne	.L4853
	movzbl	148(%rsp), %r13d
	testb	$92, %r13b
	jne	.L5425
	movq	%rax, 8(%rbp)
	movq	(%rbx), %rax
	andl	$32, %r13d
	leaq	176(%rsp), %rbx
	movzbl	(%rsi), %esi
	movq	48(%rax), %r12
	movq	%rbx, 160(%rsp)
	movq	$0, 168(%rsp)
	movb	$0, 176(%rsp)
	jne	.L5426
	movl	$5, %edi
	leaq	.LC119(%rip), %r9
	testb	%sil, %sil
	je	.L4871
	movl	$4, %edi
	leaq	.LC120(%rip), %r9
.L4871:
	leaq	160(%rsp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, 32(%rsp)
	movq	%rsi, %rcx
.LEHB73:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
	movq	168(%rsp), %rdx
.L4870:
	movq	160(%rsp), %r13
	leaq	112(%rsp), %rcx
	movq	%r14, %r9
	movq	%r12, %r8
	movq	%rdx, 112(%rsp)
	movl	$1, 32(%rsp)
	movq	%r13, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
.LEHE73:
	movq	%rax, %r14
	movq	%rsi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r14, %rax
	jmp	.L4858
	.p2align 4
	.p2align 3
.L4850:
	movq	(%rbx), %r14
	leaq	192(%rsp), %r12
	movl	$7, %r8d
	movq	$0, 192(%rsp)
	movq	%r12, %rcx
	movl	$32, 200(%rsp)
	leaq	8(%r14), %rdx
.LEHB74:
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movzbl	193(%rsp), %edx
	movl	%edx, %ecx
	notl	%edx
	andl	$120, %ecx
	andl	$56, %edx
	jne	.L4875
	testb	$92, 192(%rsp)
	jne	.L5427
	movq	(%rbx), %r9
	movq	%rax, 8(%r14)
	movq	48(%r9), %rbx
	cmpb	$56, %cl
	je	.L5428
	movq	16(%rbx), %rax
	jmp	.L5424
	.p2align 4
	.p2align 3
.L4849:
	movq	(%rbx), %r13
	leaq	136(%rsp), %r14
	movl	$1, %r8d
	movq	$0, 136(%rsp)
	movq	%r14, %rcx
	movl	$32, 144(%rsp)
	leaq	8(%r13), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movzbl	137(%rsp), %ecx
	movq	(%rbx), %rbx
	movl	(%rsi), %esi
	movq	%rax, 8(%r13)
	movl	%ecx, %edi
	movq	48(%rbx), %rbx
	movl	%esi, %r9d
	andl	$120, %edi
	cmpb	$56, %dil
	je	.L5429
	shrb	$3, %cl
	andl	$15, %ecx
	testl	%esi, %esi
	js	.L5430
	cmpb	$3, %cl
	ja	.L4889
	cmpb	$1, %cl
	ja	.L5431
	testl	%esi, %esi
	jne	.L4885
.L5421:
	movzbl	136(%rsp), %r8d
	movb	$48, 195(%rsp)
.L4902:
	leaq	195(%rsp), %r12
	leaq	194(%rsp), %rdx
	leaq	196(%rsp), %r13
	movq	%r12, %rbp
.L4917:
	shrb	$2, %r8b
	andl	$3, %r8d
	cmpl	$1, %r8d
	je	.L5432
	cmpl	$3, %r8d
	je	.L5433
.L4920:
	movq	%r12, %r8
	subq	%rbp, %r13
	leaq	112(%rsp), %rdx
	movq	%rbx, %r9
	subq	%rbp, %r8
	movq	%r14, %rcx
	movq	%r13, 112(%rsp)
	movq	%rbp, 120(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L5424
	.p2align 4
	.p2align 3
.L4848:
	movq	(%rbx), %r14
	leaq	160(%rsp), %r13
	movl	$1, %r8d
	movq	$0, 160(%rsp)
	movq	%r13, %rcx
	movl	$32, 168(%rsp)
	leaq	8(%r14), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	(%rbx), %r9
	movq	%rax, 8(%r14)
	movzbl	161(%rsp), %eax
	movq	48(%r9), %rbx
	movl	(%rsi), %r9d
	movl	%eax, %esi
	andl	$120, %esi
	cmpb	$56, %sil
	je	.L5434
	shrb	$3, %al
	andl	$15, %eax
	cmpb	$4, %al
	je	.L4924
	ja	.L4925
	cmpb	$1, %al
	jbe	.L4926
	cmpb	$16, %sil
	leaq	.LC139(%rip), %rdx
	leaq	.LC140(%rip), %r12
	cmovne	%r12, %rdx
	testl	%r9d, %r9d
	jne	.L5435
	movl	$48, %r8d
	leaq	196(%rsp), %r12
	leaq	195(%rsp), %rbp
.L4931:
	movzbl	160(%rsp), %eax
	movb	%r8b, 195(%rsp)
	testb	$16, %al
	je	.L5422
.L5038:
	movq	$-2, %r8
	movl	$2, %r14d
.L4935:
	addq	%rbp, %r8
	movl	%r14d, %esi
	testl	%r14d, %r14d
	je	.L4936
	xorl	%ecx, %ecx
	leal	-1(%r14), %r10d
	movl	$1, %r14d
	movzbl	(%rdx,%rcx), %r11d
	andl	$7, %r10d
	movb	%r11b, (%r8,%rcx)
	cmpl	%esi, %r14d
	jnb	.L4936
	testl	%r10d, %r10d
	je	.L4952
	cmpl	$1, %r10d
	je	.L5313
	cmpl	$2, %r10d
	je	.L5314
	cmpl	$3, %r10d
	je	.L5315
	cmpl	$4, %r10d
	je	.L5316
	cmpl	$5, %r10d
	je	.L5317
	cmpl	$6, %r10d
	je	.L5318
	movl	$1, %edi
	movl	$2, %r14d
	movzbl	(%rdx,%rdi), %r9d
	movb	%r9b, (%r8,%rdi)
.L5318:
	movl	%r14d, %r10d
	incl	%r14d
	movzbl	(%rdx,%r10), %ecx
	movb	%cl, (%r8,%r10)
.L5317:
	movl	%r14d, %edi
	incl	%r14d
	movzbl	(%rdx,%rdi), %r11d
	movb	%r11b, (%r8,%rdi)
.L5316:
	movl	%r14d, %r9d
	incl	%r14d
	movzbl	(%rdx,%r9), %r10d
	movb	%r10b, (%r8,%r9)
.L5315:
	movl	%r14d, %ecx
	incl	%r14d
	movzbl	(%rdx,%rcx), %edi
	movb	%dil, (%r8,%rcx)
.L5314:
	movl	%r14d, %r9d
	incl	%r14d
	movzbl	(%rdx,%r9), %r11d
	movb	%r11b, (%r8,%r9)
.L5313:
	movl	%r14d, %r10d
	incl	%r14d
	movzbl	(%rdx,%r10), %ecx
	movb	%cl, (%r8,%r10)
	cmpl	%esi, %r14d
	jnb	.L4936
.L4952:
	movl	%r14d, %edi
	leal	1(%r14), %r10d
	leal	2(%r14), %ecx
	movzbl	(%rdx,%rdi), %r9d
	movzbl	(%rdx,%r10), %r11d
	movb	%r9b, (%r8,%rdi)
	leal	3(%r14), %r9d
	movzbl	(%rdx,%rcx), %edi
	movb	%r11b, (%r8,%r10)
	movzbl	(%rdx,%r9), %r10d
	movb	%dil, (%r8,%rcx)
	leal	4(%r14), %ecx
	leal	5(%r14), %edi
	movb	%r10b, (%r8,%r9)
	movzbl	(%rdx,%rcx), %r11d
	movzbl	(%rdx,%rdi), %r9d
	leal	6(%r14), %r10d
	movb	%r11b, (%r8,%rcx)
	movb	%r9b, (%r8,%rdi)
	leal	7(%r14), %edi
	movzbl	(%rdx,%r10), %ecx
	movzbl	(%rdx,%rdi), %r11d
	addl	$8, %r14d
	movb	%cl, (%r8,%r10)
	movb	%r11b, (%r8,%rdi)
	cmpl	%esi, %r14d
	jb	.L4952
	.p2align 4
	.p2align 3
.L4936:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L5040
	cmpl	$3, %eax
	je	.L5436
.L4955:
	subq	%r8, %rbp
	subq	%r8, %r12
	movq	%r8, 120(%rsp)
	leaq	112(%rsp), %rdx
	movq	%rbx, %r9
	movq	%rbp, %r8
	movq	%r13, %rcx
	movq	%r12, 112(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L5424
	.p2align 4
	.p2align 3
.L4847:
	movq	(%rbx), %rbp
	leaq	192(%rsp), %rdi
	movl	$1, %r8d
	movq	$0, 192(%rsp)
	movq	%rdi, %rcx
	movl	$32, 200(%rsp)
	leaq	8(%rbp), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	(%rbx), %rbx
	movq	(%rsi), %rdx
	movq	%rdi, %rcx
	movq	%rax, 8(%rbp)
	movq	48(%rbx), %r8
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r13
	movq	%rax, 16(%r13)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4846:
	movq	(%rbx), %r13
	leaq	148(%rsp), %rbp
	movl	$1, %r8d
	movq	$0, 148(%rsp)
	movq	%rbp, %rcx
	movl	$32, 156(%rsp)
	leaq	8(%r13), %rdx
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	(%rbx), %r11
	movq	(%rsi), %r8
	movq	%rax, 8(%r13)
	movzbl	149(%rsp), %eax
	movq	48(%r11), %r13
	movl	%eax, %esi
	andl	$120, %esi
	cmpb	$56, %sil
	je	.L5437
	shrb	$3, %al
	andl	$15, %eax
	cmpb	$4, %al
	je	.L4958
	ja	.L4959
	cmpb	$1, %al
	jbe	.L4960
	cmpb	$16, %sil
	leaq	.LC139(%rip), %r12
	leaq	.LC140(%rip), %r10
	cmovne	%r10, %r12
	testq	%r8, %r8
	jne	.L5438
	movl	$48, %ecx
	leaq	196(%rsp), %rbx
	leaq	195(%rsp), %rdi
.L4965:
	movzbl	148(%rsp), %eax
	movb	%cl, 195(%rsp)
	testb	$16, %al
	je	.L5423
.L5049:
	movq	$-2, %rcx
	movl	$2, %r10d
.L4969:
	addq	%rdi, %rcx
	movl	%r10d, %r11d
	testl	%r10d, %r10d
	je	.L4970
	xorl	%edx, %edx
	leal	-1(%r10), %r9d
	movzbl	(%r12,%rdx), %r14d
	andl	$7, %r9d
	movb	%r14b, (%rcx,%rdx)
	movl	$1, %edx
	cmpl	%r10d, %edx
	jnb	.L4970
	testl	%r9d, %r9d
	je	.L4993
	cmpl	$1, %r9d
	je	.L5319
	cmpl	$2, %r9d
	je	.L5320
	cmpl	$3, %r9d
	je	.L5321
	cmpl	$4, %r9d
	je	.L5322
	cmpl	$5, %r9d
	je	.L5323
	cmpl	$6, %r9d
	je	.L5324
	movl	$1, %esi
	movl	$2, %edx
	movzbl	(%r12,%rsi), %r8d
	movb	%r8b, (%rcx,%rsi)
.L5324:
	movl	%edx, %r9d
	incl	%edx
	movzbl	(%r12,%r9), %r10d
	movb	%r10b, (%rcx,%r9)
.L5323:
	movl	%edx, %r14d
	incl	%edx
	movzbl	(%r12,%r14), %esi
	movb	%sil, (%rcx,%r14)
.L5322:
	movl	%edx, %r8d
	incl	%edx
	movzbl	(%r12,%r8), %r9d
	movb	%r9b, (%rcx,%r8)
.L5321:
	movl	%edx, %r14d
	incl	%edx
	movzbl	(%r12,%r14), %r10d
	movb	%r10b, (%rcx,%r14)
.L5320:
	movl	%edx, %esi
	incl	%edx
	movzbl	(%r12,%rsi), %r8d
	movb	%r8b, (%rcx,%rsi)
.L5319:
	movl	%edx, %r9d
	incl	%edx
	movzbl	(%r12,%r9), %r14d
	movb	%r14b, (%rcx,%r9)
	cmpl	%r11d, %edx
	jnb	.L4970
.L4993:
	movl	%edx, %esi
	leal	1(%rdx), %r8d
	leal	2(%rdx), %r14d
	movzbl	(%r12,%rsi), %r10d
	movzbl	(%r12,%r8), %r9d
	movb	%r10b, (%rcx,%rsi)
	movb	%r9b, (%rcx,%r8)
	movzbl	(%r12,%r14), %esi
	leal	3(%rdx), %r8d
	movzbl	(%r12,%r8), %r10d
	leal	4(%rdx), %r9d
	movb	%sil, (%rcx,%r14)
	leal	5(%rdx), %esi
	movzbl	(%r12,%r9), %r14d
	movb	%r10b, (%rcx,%r8)
	movzbl	(%r12,%rsi), %r8d
	movb	%r14b, (%rcx,%r9)
	leal	6(%rdx), %r9d
	leal	7(%rdx), %r14d
	addl	$8, %edx
	movb	%r8b, (%rcx,%rsi)
	movzbl	(%r12,%r9), %r10d
	movzbl	(%r12,%r14), %esi
	movb	%r10b, (%rcx,%r9)
	movb	%sil, (%rcx,%r14)
	cmpl	%r11d, %edx
	jb	.L4993
	.p2align 4
	.p2align 3
.L4970:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L5051
	cmpl	$3, %eax
	je	.L5439
.L4996:
	subq	%rcx, %rdi
	subq	%rcx, %rbx
	movq	%rcx, 120(%rsp)
	leaq	112(%rsp), %rdx
	movq	%r13, %r9
	movq	%rdi, %r8
	movq	%rbp, %rcx
	movq	%rbx, 112(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L4957
	.p2align 4
	.p2align 3
.L4845:
	movq	(%rbx), %r14
	leaq	192(%rsp), %rdi
	movq	$0, 192(%rsp)
	movl	$32, 200(%rsp)
	movq	%rdi, %rcx
	leaq	8(%r14), %rdx
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	movq	(%rbx), %r10
	vmovss	(%rsi), %xmm1
	movq	%rdi, %rcx
	movq	%rax, 8(%r14)
	movq	48(%r10), %r8
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r8
	movq	%rax, 16(%r8)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4844:
	movq	(%rbx), %rbp
	leaq	192(%rsp), %r12
	movq	$0, 192(%rsp)
	movl	$32, 200(%rsp)
	movq	%r12, %rcx
	leaq	8(%rbp), %rdx
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	movq	(%rbx), %rcx
	vmovsd	(%rsi), %xmm1
	movq	%rax, 8(%rbp)
	movq	48(%rcx), %r8
	movq	%r12, %rcx
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %rdx
	movq	%rax, 16(%rdx)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4843:
	movq	(%rbx), %r14
	leaq	192(%rsp), %r13
	movq	$0, 192(%rsp)
	movl	$32, 200(%rsp)
	movq	%r13, %rcx
	leaq	8(%r14), %rdx
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	movq	(%rbx), %rbx
	fldt	(%rsi)
	leaq	80(%rsp), %rdx
	movq	%r13, %rcx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %r8
	fstpt	80(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r9
	movq	%rax, 16(%r9)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4842:
	movq	(%rbx), %r12
	leaq	192(%rsp), %rdi
	movq	$0, 192(%rsp)
	movl	$32, 200(%rsp)
	movq	%rdi, %rcx
	leaq	8(%r12), %rdx
	call	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
	movq	%rax, 8(%r12)
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	48(%rax), %rbx
	movq	%rsi, %rcx
	call	strlen
	leaq	112(%rsp), %rdx
	movq	%rdi, %rcx
	movq	%rsi, 120(%rsp)
	movq	%rax, 112(%rsp)
	movq	%rbx, %r8
	call	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
.L5424:
	movq	%rax, 16(%rbx)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4841:
	movq	(%rbx), %r13
	leaq	192(%rsp), %rbp
	movq	$0, 192(%rsp)
	movl	$32, 200(%rsp)
	movq	%rbp, %rcx
	leaq	8(%r13), %rdx
	call	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
	movq	(%rbx), %r8
	vmovdqu	(%rsi), %xmm2
	leaq	112(%rsp), %rdx
	movq	%rbp, %rcx
	movq	%rax, 8(%r13)
	movq	48(%r8), %r8
	vmovdqa	%xmm2, 112(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
	movq	56(%rsp), %r11
	movq	%rax, 16(%r11)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4840:
	movq	(%rbx), %r12
	movq	$0, 148(%rsp)
	movl	$32, 156(%rsp)
	movq	$0, 192(%rsp)
	movq	16(%r12), %r13
	movq	8(%r12), %rdx
	cmpq	%rdx, %r13
	je	.L4997
	cmpb	$125, (%rdx)
	je	.L4997
	leaq	192(%rsp), %r14
	movq	%r13, %r8
	movl	$32, 200(%rsp)
	movq	%r14, %rcx
	call	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_
	movq	%rax, %rdx
	cmpq	%rax, %r13
	je	.L4998
	cmpb	$125, (%rax)
	je	.L4998
	leaq	8(%r12), %r9
	movq	%r13, %r8
	movq	%r14, %rcx
	call	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE
	movq	%rax, %rdx
	cmpq	%rax, %r13
	je	.L5002
	movzbl	(%rax), %r8d
	cmpb	$112, %r8b
	je	.L5440
.L5003:
	cmpb	$125, %r8b
	jne	.L5441
.L5002:
	movq	192(%rsp), %rax
	movl	200(%rsp), %ebp
	movq	(%rbx), %rbx
	movq	%rax, 148(%rsp)
	movl	%ebp, 156(%rsp)
.L5000:
	movq	%rdx, 8(%r12)
	movq	(%rsi), %r12
	movq	48(%rbx), %r8
	testq	%r12, %r12
	jne	.L5005
	movl	$3, %edx
	movb	$48, 194(%rsp)
.L5006:
	leaq	112(%rsp), %rcx
	leaq	148(%rsp), %r9
	movq	%r14, 120(%rsp)
	movw	$30768, 192(%rsp)
	movq	%rdx, 112(%rsp)
	movl	$2, 32(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
	movq	56(%rsp), %r14
	movq	%rax, 16(%r14)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L4839:
	movq	(%rbx), %rcx
	movq	8(%rsi), %r10
	movq	(%rsi), %r8
	movq	48(%rcx), %rdx
	addq	$8, %rcx
	addq	$272, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	rex.W jmp	*%r10
	.p2align 4
	.p2align 3
.L4875:
	movq	(%rbx), %rbx
	movq	%rax, 8(%r14)
	movzbl	(%rsi), %edx
	movq	48(%rbx), %rbx
	cmpb	$56, %cl
	je	.L5014
	testb	%cl, %cl
	je	.L5014
	movq	%rbx, %r8
	movq	%r12, %rcx
	call	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	jmp	.L5424
	.p2align 4
	.p2align 3
.L4853:
	movq	(%rbx), %r11
	movzbl	(%rsi), %edx
	movq	%rax, 8(%rbp)
	movq	48(%r11), %r12
	cmpb	$56, %r8b
	je	.L5442
	movq	%r12, %r8
	movq	%r14, %rcx
	call	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
.L4858:
	movq	%rax, 16(%r12)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L5437:
	cmpq	$127, %r8
	ja	.L4880
	leaq	192(%rsp), %rcx
	movb	%r8b, 192(%rsp)
	movq	%r13, %rdx
	movq	%rbp, %r8
	movq	%rcx, 120(%rsp)
	leaq	112(%rsp), %rcx
	movq	$1, 112(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
.L4957:
	movq	%rax, 16(%r13)
	jmp	.L5418
	.p2align 4
	.p2align 3
.L5434:
	cmpl	$127, %r9d
	ja	.L4880
	leaq	192(%rsp), %rbp
	leaq	112(%rsp), %rcx
	movq	%r13, %r8
	movq	%rbx, %rdx
	movb	%r9b, 192(%rsp)
	movq	$1, 112(%rsp)
	movq	%rbp, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L5424
	.p2align 4
	.p2align 3
.L5429:
	leal	128(%rsi), %eax
	cmpl	$255, %eax
	ja	.L4880
	leaq	192(%rsp), %rdi
	leaq	112(%rsp), %rcx
	movq	%r14, %r8
	movq	%rbx, %rdx
	movb	%sil, 192(%rsp)
	movq	$1, 112(%rsp)
	movq	%rdi, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L5424
	.p2align 4
	.p2align 3
.L4997:
	movq	192(%rsp), %r9
	movq	%r12, %rbx
	leaq	192(%rsp), %r14
	movl	$32, 200(%rsp)
	movl	$32, 156(%rsp)
	movq	%r9, 148(%rsp)
	jmp	.L5000
	.p2align 4
	.p2align 3
.L5005:
	movl	$67, %r10d
	lzcntq	%r12, %rsi
	movabsq	$3978425819141910832, %rdx
	movabsq	$7378413942531504440, %r13
	subl	%esi, %r10d
	movq	%rdx, 160(%rsp)
	movq	%r13, 168(%rsp)
	shrl	$2, %r10d
	leal	-1(%r10), %r11d
	cmpq	$255, %r12
	jbe	.L5007
	.p2align 4
	.p2align 3
.L5008:
	movq	%r12, %rdi
	movq	%r12, %rbp
	movl	%r11d, %eax
	leal	-1(%r11), %r9d
	shrq	$4, %rdi
	andl	$15, %ebp
	andl	$15, %edi
	subl	$2, %r11d
	movzbl	160(%rsp,%rbp), %ebx
	movzbl	160(%rsp,%rdi), %ecx
	shrq	$8, %r12
	movb	%bl, 194(%rsp,%rax)
	movb	%cl, 194(%rsp,%r9)
	cmpq	$255, %r12
	ja	.L5008
.L5007:
	cmpq	$15, %r12
	jbe	.L5009
	movq	%r12, %rsi
	andl	$15, %esi
	movzbl	160(%rsp,%rsi), %edx
	shrq	$4, %r12
	movb	%dl, 195(%rsp)
	movzbl	160(%rsp,%r12), %r12d
.L5010:
	leal	2(%r10), %r10d
	movb	%r12b, 194(%rsp)
	movslq	%r10d, %rdx
	jmp	.L5006
	.p2align 4
	.p2align 3
.L5428:
	movzbl	(%rsi), %edx
.L5014:
	leaq	160(%rsp), %rsi
	movb	%dl, 160(%rsp)
	leaq	112(%rsp), %rcx
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	$1, 112(%rsp)
	movq	%rsi, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L5424
	.p2align 4
	.p2align 3
.L5430:
	negl	%r9d
	cmpb	$4, %cl
	je	.L4883
	ja	.L4884
	cmpb	$1, %cl
	jbe	.L4885
	cmpb	$16, %dil
	leaq	.LC140(%rip), %rax
	leaq	.LC139(%rip), %rcx
	cmove	%rcx, %rax
.L4886:
	movl	$32, %r8d
	movl	$31, %r11d
	lzcntl	%r9d, %r10d
	subl	%r10d, %r8d
	subl	%r10d, %r11d
	je	.L4899
	movl	$30, %ebp
	movl	%r11d, %r12d
	subl	%r10d, %ebp
	leaq	192(%rsp,%r12), %rdx
	subq	%rbp, %r12
	movq	%rdx, %rdi
	leaq	191(%rsp,%r12), %r13
	subq	%r13, %rdi
	andl	$7, %edi
	je	.L4898
	cmpq	$1, %rdi
	je	.L5268
	cmpq	$2, %rdi
	je	.L5269
	cmpq	$3, %rdi
	je	.L5270
	cmpq	$4, %rdi
	je	.L5271
	cmpq	$5, %rdi
	je	.L5272
	cmpq	$6, %rdi
	je	.L5273
	movl	%r9d, %ecx
	decq	%rdx
	andl	$1, %ecx
	addl	$48, %ecx
	shrl	%r9d
	movb	%cl, 4(%rdx)
.L5273:
	movl	%r9d, %r10d
	decq	%rdx
	andl	$1, %r10d
	addl	$48, %r10d
	shrl	%r9d
	movb	%r10b, 4(%rdx)
.L5272:
	movl	%r9d, %r11d
	decq	%rdx
	andl	$1, %r11d
	addl	$48, %r11d
	shrl	%r9d
	movb	%r11b, 4(%rdx)
.L5271:
	movl	%r9d, %r12d
	decq	%rdx
	andl	$1, %r12d
	addl	$48, %r12d
	shrl	%r9d
	movb	%r12b, 4(%rdx)
.L5270:
	movl	%r9d, %ebp
	decq	%rdx
	andl	$1, %ebp
	addl	$48, %ebp
	shrl	%r9d
	movb	%bpl, 4(%rdx)
.L5269:
	movl	%r9d, %edi
	decq	%rdx
	andl	$1, %edi
	addl	$48, %edi
	shrl	%r9d
	movb	%dil, 4(%rdx)
.L5268:
	movl	%r9d, %ecx
	decq	%rdx
	andl	$1, %ecx
	addl	$48, %ecx
	movb	%cl, 4(%rdx)
	shrl	%r9d
	cmpq	%r13, %rdx
	je	.L4899
.L4898:
	movl	%r9d, %r11d
	movl	%r9d, %r10d
	movl	%r9d, %r12d
	movl	%r9d, %ebp
	shrl	%r11d
	andl	$1, %r10d
	andl	$1, %r11d
	movl	%r9d, %edi
	movl	%r9d, %ecx
	addl	$48, %r10d
	addl	$48, %r11d
	subq	$8, %rdx
	movb	%r10b, 11(%rdx)
	movl	%r9d, %r10d
	movb	%r11b, 10(%rdx)
	movl	%r9d, %r11d
	shrl	$2, %r12d
	shrl	$3, %ebp
	shrl	$4, %edi
	shrl	$5, %ecx
	shrl	$6, %r10d
	shrb	$7, %r11b
	andl	$1, %r12d
	andl	$1, %ebp
	andl	$1, %edi
	andl	$1, %ecx
	andl	$1, %r10d
	addl	$48, %r12d
	addl	$48, %ebp
	addl	$48, %edi
	addl	$48, %ecx
	addl	$48, %r10d
	addl	$48, %r11d
	movb	%r12b, 9(%rdx)
	movb	%bpl, 8(%rdx)
	movb	%dil, 7(%rdx)
	movb	%cl, 6(%rdx)
	movb	%r10b, 5(%rdx)
	movb	%r11b, 4(%rdx)
	shrl	$8, %r9d
	cmpq	%r13, %rdx
	jne	.L4898
.L4899:
	movslq	%r8d, %r9
	leaq	195(%rsp), %r12
	movl	$49, %r11d
	leaq	195(%rsp,%r9), %r13
.L4896:
	movb	%r11b, 195(%rsp)
	testb	$16, 136(%rsp)
	je	.L5420
.L5028:
	movq	$-2, %rbp
	movl	$2, %r8d
.L4900:
	addq	%r12, %rbp
	movl	%r8d, %edi
	testl	%r8d, %r8d
	je	.L4901
	xorl	%edx, %edx
	leal	-1(%r8), %ecx
	movzbl	(%rax,%rdx), %r10d
	andl	$7, %ecx
	movb	%r10b, 0(%rbp,%rdx)
	movl	$1, %edx
	cmpl	%r8d, %edx
	jnb	.L4901
	testl	%ecx, %ecx
	je	.L4915
	cmpl	$1, %ecx
	je	.L5307
	cmpl	$2, %ecx
	je	.L5308
	cmpl	$3, %ecx
	je	.L5309
	cmpl	$4, %ecx
	je	.L5310
	cmpl	$5, %ecx
	je	.L5311
	cmpl	$6, %ecx
	je	.L5312
	movl	$1, %r11d
	movl	$2, %edx
	movzbl	(%rax,%r11), %r9d
	movb	%r9b, 0(%rbp,%r11)
.L5312:
	movl	%edx, %r8d
	incl	%edx
	movzbl	(%rax,%r8), %ecx
	movb	%cl, 0(%rbp,%r8)
.L5311:
	movl	%edx, %r11d
	incl	%edx
	movzbl	(%rax,%r11), %r10d
	movb	%r10b, 0(%rbp,%r11)
.L5310:
	movl	%edx, %r9d
	incl	%edx
	movzbl	(%rax,%r9), %r8d
	movb	%r8b, 0(%rbp,%r9)
.L5309:
	movl	%edx, %ecx
	incl	%edx
	movzbl	(%rax,%rcx), %r11d
	movb	%r11b, 0(%rbp,%rcx)
.L5308:
	movl	%edx, %r9d
	incl	%edx
	movzbl	(%rax,%r9), %r10d
	movb	%r10b, 0(%rbp,%r9)
.L5307:
	movl	%edx, %r8d
	incl	%edx
	movzbl	(%rax,%r8), %ecx
	movb	%cl, 0(%rbp,%r8)
	cmpl	%edi, %edx
	jnb	.L4901
.L4915:
	movl	%edx, %r11d
	leal	1(%rdx), %r8d
	leal	2(%rdx), %ecx
	movzbl	(%rax,%r11), %r9d
	movzbl	(%rax,%r8), %r10d
	movb	%r9b, 0(%rbp,%r11)
	leal	3(%rdx), %r9d
	movzbl	(%rax,%rcx), %r11d
	movb	%r10b, 0(%rbp,%r8)
	movzbl	(%rax,%r9), %r8d
	movb	%r11b, 0(%rbp,%rcx)
	leal	4(%rdx), %ecx
	leal	5(%rdx), %r11d
	movb	%r8b, 0(%rbp,%r9)
	movzbl	(%rax,%rcx), %r10d
	movzbl	(%rax,%r11), %r9d
	leal	6(%rdx), %r8d
	movb	%r10b, 0(%rbp,%rcx)
	movb	%r9b, 0(%rbp,%r11)
	leal	7(%rdx), %r11d
	movzbl	(%rax,%r8), %ecx
	movzbl	(%rax,%r11), %r10d
	addl	$8, %edx
	movb	%cl, 0(%rbp,%r8)
	movb	%r10b, 0(%rbp,%r11)
	cmpl	%edi, %edx
	jb	.L4915
	.p2align 4
	.p2align 3
.L4901:
	leaq	-1(%rbp), %rdx
	testl	%esi, %esi
	js	.L5402
	movzbl	136(%rsp), %r8d
	jmp	.L4917
	.p2align 4
	.p2align 3
.L5402:
	movb	$45, -1(%rbp)
	movq	%rdx, %rbp
	jmp	.L4920
	.p2align 4
	.p2align 3
.L4998:
	movq	192(%rsp), %rdi
	movl	200(%rsp), %ecx
	movq	%r12, %rbx
	movq	%rdi, 148(%rsp)
	movl	%ecx, 156(%rsp)
	jmp	.L5000
	.p2align 4
	.p2align 3
.L5051:
	movl	$43, %r8d
.L4995:
	movb	%r8b, -1(%rcx)
	decq	%rcx
	jmp	.L4996
	.p2align 4
	.p2align 3
.L5040:
	movl	$43, %r9d
.L4954:
	movb	%r9b, -1(%r8)
	decq	%r8
	jmp	.L4955
	.p2align 4
	.p2align 3
.L5009:
	movzbl	160(%rsp,%r12), %r12d
	jmp	.L5010
.L4885:
	cmpl	$9, %r9d
	jbe	.L5021
	movl	%r9d, %edx
	movl	$1, %ebp
	movl	$3518437209, %r12d
	jmp	.L4908
	.p2align 4
	.p2align 3
.L4904:
	cmpl	$999, %edx
	jbe	.L5443
	cmpl	$9999, %edx
	jbe	.L5444
	movl	%edx, %r13d
	addl	$4, %ebp
	imulq	%r12, %r13
	shrq	$45, %r13
	cmpl	$99999, %edx
	jbe	.L4905
	movl	%r13d, %edx
.L4908:
	cmpl	$99, %edx
	ja	.L4904
	incl	%ebp
.L4905:
	cmpl	$32, %ebp
	ja	.L5445
.L4903:
	leaq	195(%rsp), %r12
	movl	%r9d, %r8d
	movl	%ebp, %edx
	movq	%r12, %rcx
	leaq	(%r12,%rbp), %r13
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
.L5420:
	movq	%r12, %rbp
	jmp	.L4901
	.p2align 4
	.p2align 3
.L5442:
	leaq	192(%rsp), %r10
	movb	%dl, 192(%rsp)
	leaq	112(%rsp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdx
	movq	$1, 112(%rsp)
	movq	%r10, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
.LEHE74:
	jmp	.L4858
	.p2align 4
	.p2align 3
.L4889:
	cmpb	$4, %cl
	je	.L4892
	cmpb	$40, %dil
	je	.L5446
	testl	%esi, %esi
	jne	.L4888
	movb	$48, 195(%rsp)
	cmpb	$48, %dil
	je	.L5025
	leaq	.LC141(%rip), %rax
	leaq	196(%rsp), %r13
	leaq	195(%rsp), %r12
.L4913:
	testb	$16, 136(%rsp)
	jne	.L5028
	jmp	.L5420
	.p2align 4
	.p2align 3
.L4926:
	testl	%r9d, %r9d
	jne	.L4937
	leaq	196(%rsp), %r12
	leaq	195(%rsp), %rbp
	movb	$48, 195(%rsp)
.L4938:
	movzbl	160(%rsp), %eax
	movq	%rbp, %r8
	jmp	.L4936
	.p2align 4
	.p2align 3
.L4925:
	cmpb	$40, %sil
	je	.L5447
	testl	%r9d, %r9d
	jne	.L4949
	movb	$48, 195(%rsp)
	cmpb	$48, %sil
	je	.L5035
	leaq	.LC141(%rip), %rdx
	leaq	196(%rsp), %r12
	leaq	195(%rsp), %rbp
.L4948:
	movzbl	160(%rsp), %eax
	testb	$16, %al
	jne	.L5038
.L5422:
	movq	%rbp, %r8
	jmp	.L4936
	.p2align 4
	.p2align 3
.L4924:
	testl	%r9d, %r9d
	je	.L4945
	leaq	195(%rsp), %rbp
	leaq	96(%rsp), %rcx
	leaq	227(%rsp), %r8
	movl	$1, %r14d
	movq	%rbp, %rdx
	call	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r12
	leaq	.LC143(%rip), %rdx
.L4946:
	movzbl	160(%rsp), %eax
	movq	%rbp, %r8
	testb	$16, %al
	je	.L4936
	movq	%r14, %r8
	negq	%r8
	jmp	.L4935
	.p2align 4
	.p2align 3
.L4960:
	testq	%r8, %r8
	jne	.L4971
	leaq	196(%rsp), %rbx
	leaq	195(%rsp), %rdi
	movb	$48, 195(%rsp)
.L4972:
	movzbl	148(%rsp), %eax
	movq	%rdi, %rcx
	jmp	.L4970
	.p2align 4
	.p2align 3
.L4959:
	cmpb	$40, %sil
	je	.L5448
	testq	%r8, %r8
	jne	.L5047
	movb	$48, 195(%rsp)
	cmpb	$48, %sil
	je	.L5048
	leaq	.LC141(%rip), %r12
	leaq	196(%rsp), %rbx
	leaq	195(%rsp), %rdi
.L4986:
	movzbl	148(%rsp), %eax
	testb	$16, %al
	jne	.L5049
.L5423:
	movq	%rdi, %rcx
	jmp	.L4970
	.p2align 4
	.p2align 3
.L4958:
	testq	%r8, %r8
	je	.L4979
	movl	$66, %r9d
	movl	$2863311531, %r12d
	lzcntq	%r8, %rbx
	subl	%ebx, %r9d
	imulq	%r12, %r9
	shrq	$33, %r9
	leal	-1(%r9), %edx
	cmpq	$63, %r8
	jbe	.L4981
	.p2align 6
	.p2align 4
	.p2align 3
.L4980:
	movq	%r8, %rdi
	movq	%r8, %r14
	movl	%edx, %ecx
	leal	-1(%rdx), %r10d
	shrq	$3, %rdi
	andl	$7, %r14d
	andl	$7, %edi
	subl	$2, %edx
	shrq	$6, %r8
	addl	$48, %r14d
	addl	$48, %edi
	movb	%r14b, 195(%rsp,%rcx)
	movb	%dil, 195(%rsp,%r10)
	cmpq	$63, %r8
	ja	.L4980
.L4981:
	leal	48(%r8), %eax
	cmpq	$7, %r8
	jbe	.L4983
	movq	%r8, %rax
	movq	%r8, %r11
	shrq	$3, %rax
	andl	$7, %r11d
	addl	$48, %eax
	addl	$48, %r11d
	movb	%r11b, 196(%rsp)
.L4983:
	movl	%r9d, %r8d
	leaq	195(%rsp), %rdi
	leaq	.LC143(%rip), %r12
	movl	$1, %r10d
	leaq	195(%rsp,%r8), %rbx
	movb	%al, 195(%rsp)
.L4984:
	movzbl	148(%rsp), %eax
	movq	%rdi, %rcx
	testb	$16, %al
	je	.L4970
	movq	%r10, %rcx
	negq	%rcx
	jmp	.L4969
	.p2align 4
	.p2align 3
.L5439:
	movl	$32, %r8d
	jmp	.L4995
.L5436:
	movl	$32, %r9d
	jmp	.L4954
.L5432:
	movb	$43, -1(%rbp)
.L4921:
	movq	%rdx, %rbp
	jmp	.L4920
.L4892:
	testl	%esi, %esi
	je	.L5421
.L4883:
	leaq	195(%rsp), %r12
	leaq	227(%rsp), %r8
	leaq	96(%rsp), %rcx
	movq	%r12, %rdx
	call	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	leaq	.LC143(%rip), %rax
	movl	$1, %r8d
.L4910:
	movq	%r12, %rbp
	testb	$16, 136(%rsp)
	je	.L4901
	movq	%r8, %rbp
	negq	%rbp
	jmp	.L4900
	.p2align 4
	.p2align 3
.L5440:
	leaq	1(%rax), %r11
	cmpq	%r11, %r13
	je	.L5052
	movzbl	1(%rax), %r8d
	movq	%r11, %rdx
	jmp	.L5003
.L5431:
	cmpb	$16, %dil
	leaq	.LC140(%rip), %rax
	leaq	.LC139(%rip), %r10
	cmove	%r10, %rax
	testl	%esi, %esi
	jne	.L4886
	movl	$48, %r11d
	leaq	196(%rsp), %r13
	leaq	195(%rsp), %r12
	jmp	.L4896
.L5433:
	movb	$32, -1(%rbp)
	jmp	.L4921
.L4937:
	cmpl	$9, %r9d
	jbe	.L5032
	movl	%r9d, %eax
	movl	$1, %esi
	movl	$3518437209, %r14d
	jmp	.L4944
	.p2align 4
	.p2align 3
.L4940:
	cmpl	$999, %eax
	jbe	.L5449
	cmpl	$9999, %eax
	jbe	.L5450
	movl	%eax, %edx
	addl	$4, %esi
	imulq	%r14, %rdx
	shrq	$45, %rdx
	cmpl	$99999, %eax
	jbe	.L4941
	movl	%edx, %eax
.L4944:
	cmpl	$99, %eax
	ja	.L4940
	incl	%esi
.L4941:
	cmpl	$32, %esi
	ja	.L5451
.L4939:
	leaq	195(%rsp), %rbp
	movl	%r9d, %r8d
	movl	%esi, %edx
	movq	%rbp, %rcx
	leaq	0(%rbp,%rsi), %r12
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
	jmp	.L4938
.L4979:
	leaq	195(%rsp), %rdi
	movzbl	148(%rsp), %eax
	leaq	196(%rsp), %rbx
	movb	$48, 195(%rsp)
	movq	%rdi, %rcx
	jmp	.L4970
.L4971:
	cmpq	$9, %r8
	jbe	.L5043
	movq	%r8, %r11
	movl	$1, %ebx
	movabsq	$3777893186295716171, %r12
	jmp	.L4978
	.p2align 4
	.p2align 3
.L4974:
	cmpq	$999, %r11
	jbe	.L5452
	cmpq	$9999, %r11
	jbe	.L5453
	movq	%r11, %rax
	addl	$4, %ebx
	mulq	%r12
	shrq	$11, %rdx
	cmpq	$99999, %r11
	jbe	.L4975
	movq	%rdx, %r11
.L4978:
	cmpq	$99, %r11
	ja	.L4974
	incl	%ebx
.L4975:
	cmpl	$64, %ebx
	ja	.L5454
.L4973:
	leaq	195(%rsp), %rdi
	movl	%ebx, %edx
	movq	%rdi, %rcx
	addq	%rdi, %rbx
	call	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
	jmp	.L4972
.L5435:
	movl	$32, %r8d
	movl	$31, %r11d
	lzcntl	%r9d, %ebp
	subl	%ebp, %r8d
	subl	%ebp, %r11d
	je	.L4934
	movl	$30, %eax
	movl	%r11d, %ecx
	subl	%ebp, %eax
	leaq	192(%rsp,%rcx), %r14
	subq	%rax, %rcx
	movq	%r14, %r10
	leaq	191(%rsp,%rcx), %rsi
	subq	%rsi, %r10
	andl	$7, %r10d
	je	.L4933
	cmpq	$1, %r10
	je	.L5281
	cmpq	$2, %r10
	je	.L5282
	cmpq	$3, %r10
	je	.L5283
	cmpq	$4, %r10
	je	.L5284
	cmpq	$5, %r10
	je	.L5285
	cmpq	$6, %r10
	je	.L5286
	movl	%r9d, %edi
	decq	%r14
	andl	$1, %edi
	addl	$48, %edi
	shrl	%r9d
	movb	%dil, 4(%r14)
.L5286:
	movl	%r9d, %r12d
	decq	%r14
	andl	$1, %r12d
	addl	$48, %r12d
	shrl	%r9d
	movb	%r12b, 4(%r14)
.L5285:
	movl	%r9d, %ebp
	decq	%r14
	andl	$1, %ebp
	addl	$48, %ebp
	shrl	%r9d
	movb	%bpl, 4(%r14)
.L5284:
	movl	%r9d, %r11d
	decq	%r14
	andl	$1, %r11d
	addl	$48, %r11d
	shrl	%r9d
	movb	%r11b, 4(%r14)
.L5283:
	movl	%r9d, %ecx
	decq	%r14
	andl	$1, %ecx
	addl	$48, %ecx
	shrl	%r9d
	movb	%cl, 4(%r14)
.L5282:
	movl	%r9d, %eax
	decq	%r14
	andl	$1, %eax
	addl	$48, %eax
	shrl	%r9d
	movb	%al, 4(%r14)
.L5281:
	movl	%r9d, %r10d
	decq	%r14
	andl	$1, %r10d
	addl	$48, %r10d
	movb	%r10b, 4(%r14)
	shrl	%r9d
	cmpq	%rsi, %r14
	je	.L4934
.L4933:
	movl	%r9d, %edi
	movl	%r9d, %r12d
	movl	%r9d, %ebp
	movl	%r9d, %r11d
	andl	$1, %edi
	movl	%r9d, %ecx
	movl	%r9d, %eax
	movl	%r9d, %r10d
	addl	$48, %edi
	subq	$8, %r14
	movb	%dil, 11(%r14)
	shrl	%r12d
	movl	%r9d, %edi
	andl	$1, %r12d
	shrl	$2, %ebp
	shrl	$3, %r11d
	shrl	$4, %ecx
	shrl	$5, %eax
	shrl	$6, %r10d
	shrb	$7, %dil
	andl	$1, %ebp
	andl	$1, %r11d
	andl	$1, %ecx
	andl	$1, %eax
	andl	$1, %r10d
	addl	$48, %r12d
	addl	$48, %ebp
	addl	$48, %r11d
	addl	$48, %ecx
	addl	$48, %eax
	addl	$48, %r10d
	addl	$48, %edi
	movb	%r12b, 10(%r14)
	movb	%bpl, 9(%r14)
	movb	%r11b, 8(%r14)
	movb	%cl, 7(%r14)
	movb	%al, 6(%r14)
	movb	%r10b, 5(%r14)
	movb	%dil, 4(%r14)
	shrl	$8, %r9d
	cmpq	%rsi, %r14
	jne	.L4933
.L4934:
	movslq	%r8d, %r9
	leaq	195(%rsp), %rbp
	movl	$49, %r8d
	leaq	195(%rsp,%r9), %r12
	jmp	.L4931
.L5438:
	movl	$64, %ebx
	movl	$63, %eax
	lzcntq	%r8, %r11
	subl	%r11d, %ebx
	subl	%r11d, %eax
	je	.L4968
	movl	$62, %edx
	movl	%eax, %r9d
	subl	%r11d, %edx
	leaq	192(%rsp,%r9), %rcx
	subq	%rdx, %r9
	movq	%rcx, %r14
	leaq	191(%rsp,%r9), %rdi
	subq	%rdi, %r14
	andl	$7, %r14d
	je	.L4967
	cmpq	$1, %r14
	je	.L5294
	cmpq	$2, %r14
	je	.L5295
	cmpq	$3, %r14
	je	.L5296
	cmpq	$4, %r14
	je	.L5297
	cmpq	$5, %r14
	je	.L5298
	cmpq	$6, %r14
	je	.L5299
	movl	%r8d, %esi
	decq	%rcx
	andl	$1, %esi
	addl	$48, %esi
	shrq	%r8
	movb	%sil, 4(%rcx)
.L5299:
	movl	%r8d, %r10d
	decq	%rcx
	andl	$1, %r10d
	addl	$48, %r10d
	shrq	%r8
	movb	%r10b, 4(%rcx)
.L5298:
	movl	%r8d, %r11d
	decq	%rcx
	andl	$1, %r11d
	addl	$48, %r11d
	shrq	%r8
	movb	%r11b, 4(%rcx)
.L5297:
	movl	%r8d, %eax
	decq	%rcx
	andl	$1, %eax
	addl	$48, %eax
	shrq	%r8
	movb	%al, 4(%rcx)
.L5296:
	movl	%r8d, %r9d
	decq	%rcx
	andl	$1, %r9d
	addl	$48, %r9d
	shrq	%r8
	movb	%r9b, 4(%rcx)
.L5295:
	movl	%r8d, %edx
	decq	%rcx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%r8
	movb	%dl, 4(%rcx)
.L5294:
	movl	%r8d, %r14d
	decq	%rcx
	andl	$1, %r14d
	addl	$48, %r14d
	movb	%r14b, 4(%rcx)
	shrq	%r8
	cmpq	%rdi, %rcx
	je	.L4968
.L4967:
	movl	%r8d, %esi
	movq	%r8, %r10
	movq	%r8, %r11
	movq	%r8, %rax
	andl	$1, %esi
	movq	%r8, %r9
	movq	%r8, %rdx
	movq	%r8, %r14
	addl	$48, %esi
	subq	$8, %rcx
	movb	%sil, 11(%rcx)
	shrq	%r10
	movl	%r8d, %esi
	andl	$1, %r10d
	shrq	$2, %r11
	shrq	$3, %rax
	shrq	$4, %r9
	shrq	$5, %rdx
	shrq	$6, %r14
	shrb	$7, %sil
	andl	$1, %r11d
	andl	$1, %eax
	andl	$1, %r9d
	andl	$1, %edx
	andl	$1, %r14d
	addl	$48, %r10d
	addl	$48, %r11d
	addl	$48, %eax
	addl	$48, %r9d
	addl	$48, %edx
	addl	$48, %r14d
	addl	$48, %esi
	movb	%r10b, 10(%rcx)
	movb	%r11b, 9(%rcx)
	movb	%al, 8(%rcx)
	movb	%r9b, 7(%rcx)
	movb	%dl, 6(%rcx)
	movb	%r14b, 5(%rcx)
	movb	%sil, 4(%rcx)
	shrq	$8, %r8
	cmpq	%rdi, %rcx
	jne	.L4967
.L4968:
	movslq	%ebx, %r8
	leaq	195(%rsp), %rdi
	movl	$49, %ecx
	leaq	195(%rsp,%r8), %rbx
	jmp	.L4965
.L4945:
	leaq	195(%rsp), %rbp
	movzbl	160(%rsp), %eax
	leaq	196(%rsp), %r12
	movb	$48, 195(%rsp)
	movq	%rbp, %r8
	jmp	.L4936
.L4884:
	cmpb	$40, %dil
	je	.L5455
.L4888:
	leaq	195(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	227(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	cmpb	$48, %dil
	jne	.L5026
	cmpq	%r12, %r13
	je	.L5027
.L4912:
	movq	%r13, %r9
	movq	%r12, %rdi
	subq	%r12, %r9
	andl	$7, %r9d
	je	.L4914
	cmpq	$1, %r9
	je	.L5274
	cmpq	$2, %r9
	je	.L5275
	cmpq	$3, %r9
	je	.L5276
	cmpq	$4, %r9
	je	.L5277
	cmpq	$5, %r9
	je	.L5278
	cmpq	$6, %r9
	je	.L5279
	movsbl	(%r12), %ecx
	leaq	196(%rsp), %rdi
	call	toupper
	movb	%al, (%r12)
.L5279:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5278:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5277:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5276:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5275:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5274:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%r13, %rdi
	je	.L5027
.L4914:
	movsbl	(%rdi), %ecx
	addq	$8, %rdi
	call	toupper
	movsbl	-7(%rdi), %ecx
	movb	%al, -8(%rdi)
	call	toupper
	movsbl	-6(%rdi), %ecx
	movb	%al, -7(%rdi)
	call	toupper
	movsbl	-5(%rdi), %ecx
	movb	%al, -6(%rdi)
	call	toupper
	movsbl	-4(%rdi), %ecx
	movb	%al, -5(%rdi)
	call	toupper
	movsbl	-3(%rdi), %ecx
	movb	%al, -4(%rdi)
	call	toupper
	movsbl	-2(%rdi), %ecx
	movb	%al, -3(%rdi)
	call	toupper
	movsbl	-1(%rdi), %ecx
	movb	%al, -2(%rdi)
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%r13, %rdi
	jne	.L4914
.L5027:
	leaq	.LC141(%rip), %rax
	movl	$2, %r8d
	jmp	.L4910
.L5447:
	testl	%r9d, %r9d
	jne	.L4947
	leaq	.LC142(%rip), %rdx
	leaq	196(%rsp), %r12
	leaq	195(%rsp), %rbp
	movb	$48, 195(%rsp)
	jmp	.L4948
.L5448:
	testq	%r8, %r8
	jne	.L5046
	leaq	.LC142(%rip), %r12
	leaq	196(%rsp), %rbx
	leaq	195(%rsp), %rdi
	movb	$48, 195(%rsp)
	jmp	.L4986
.L5047:
	leaq	.LC141(%rip), %r12
.L4985:
	movl	$67, %r9d
	lzcntq	%r8, %rbx
	movabsq	$3978425819141910832, %rcx
	movabsq	$7378413942531504440, %rdx
	subl	%ebx, %r9d
	movq	%rcx, 160(%rsp)
	movq	%rdx, 168(%rsp)
	shrl	$2, %r9d
	movl	%r9d, %r14d
	leal	-1(%r14), %edi
	cmpq	$255, %r8
	jbe	.L4988
	.p2align 4
	.p2align 3
.L4989:
	movq	%r8, %rbx
	movq	%r8, %r11
	movl	%edi, %r10d
	leal	-1(%rdi), %r9d
	shrq	$4, %rbx
	andl	$15, %r11d
	andl	$15, %ebx
	subl	$2, %edi
	movzbl	160(%rsp,%r11), %eax
	movzbl	160(%rsp,%rbx), %ecx
	shrq	$8, %r8
	movb	%al, 195(%rsp,%r10)
	movb	%cl, 195(%rsp,%r9)
	cmpq	$255, %r8
	ja	.L4989
.L4988:
	cmpq	$15, %r8
	jbe	.L4990
	movq	%r8, %rdx
	andl	$15, %edx
	movzbl	160(%rsp,%rdx), %edi
	shrq	$4, %r8
	movb	%dil, 196(%rsp)
	movzbl	160(%rsp,%r8), %r8d
.L4991:
	leaq	195(%rsp), %rdi
	leaq	195(%rsp,%r14), %rbx
	movb	%r8b, 195(%rsp)
	cmpb	$48, %sil
	jne	.L4986
.L4987:
	movq	%rbx, %r14
	movq	%rdi, %rsi
	subq	%rdi, %r14
	andl	$7, %r14d
	je	.L4992
	cmpq	$1, %r14
	je	.L5300
	cmpq	$2, %r14
	je	.L5301
	cmpq	$3, %r14
	je	.L5302
	cmpq	$4, %r14
	je	.L5303
	cmpq	$5, %r14
	je	.L5304
	cmpq	$6, %r14
	je	.L5305
	movsbl	(%rdi), %ecx
	leaq	196(%rsp), %rsi
	call	toupper
	movb	%al, (%rdi)
.L5305:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
.L5304:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
.L5303:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
.L5302:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
.L5301:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
.L5300:
	movsbl	(%rsi), %ecx
	incq	%rsi
	call	toupper
	movb	%al, -1(%rsi)
	cmpq	%rbx, %rsi
	je	.L5417
.L4992:
	movsbl	(%rsi), %ecx
	addq	$8, %rsi
	call	toupper
	movsbl	-7(%rsi), %ecx
	movb	%al, -8(%rsi)
	call	toupper
	movsbl	-6(%rsi), %ecx
	movb	%al, -7(%rsi)
	call	toupper
	movsbl	-5(%rsi), %ecx
	movb	%al, -6(%rsi)
	call	toupper
	movsbl	-4(%rsi), %ecx
	movb	%al, -5(%rsi)
	call	toupper
	movsbl	-3(%rsi), %ecx
	movb	%al, -4(%rsi)
	call	toupper
	movsbl	-2(%rsi), %ecx
	movb	%al, -3(%rsi)
	call	toupper
	movsbl	-1(%rsi), %ecx
	movb	%al, -2(%rsi)
	call	toupper
	movb	%al, -1(%rsi)
	cmpq	%rbx, %rsi
	jne	.L4992
.L5417:
	movl	$2, %r10d
	jmp	.L4984
.L4949:
	leaq	195(%rsp), %rbp
	leaq	96(%rsp), %rcx
	leaq	227(%rsp), %r8
	movq	%rbp, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r12
	cmpb	$48, %sil
	jne	.L5036
	cmpq	%rbp, %r12
	je	.L5037
.L4950:
	movq	%r12, %r10
	movq	%rbp, %rdi
	subq	%rbp, %r10
	andl	$7, %r10d
	je	.L4951
	cmpq	$1, %r10
	je	.L5287
	cmpq	$2, %r10
	je	.L5288
	cmpq	$3, %r10
	je	.L5289
	cmpq	$4, %r10
	je	.L5290
	cmpq	$5, %r10
	je	.L5291
	cmpq	$6, %r10
	je	.L5292
	movsbl	0(%rbp), %ecx
	leaq	196(%rsp), %rdi
	call	toupper
	movb	%al, 0(%rbp)
.L5292:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5291:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5290:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5289:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5288:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L5287:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%r12, %rdi
	je	.L5037
.L4951:
	movsbl	(%rdi), %ecx
	addq	$8, %rdi
	call	toupper
	movsbl	-7(%rdi), %ecx
	movb	%al, -8(%rdi)
	call	toupper
	movsbl	-6(%rdi), %ecx
	movb	%al, -7(%rdi)
	call	toupper
	movsbl	-5(%rdi), %ecx
	movb	%al, -6(%rdi)
	call	toupper
	movsbl	-4(%rdi), %ecx
	movb	%al, -5(%rdi)
	call	toupper
	movsbl	-3(%rdi), %ecx
	movb	%al, -4(%rdi)
	call	toupper
	movsbl	-2(%rdi), %ecx
	movb	%al, -3(%rdi)
	call	toupper
	movsbl	-1(%rdi), %ecx
	movb	%al, -2(%rdi)
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%r12, %rdi
	jne	.L4951
.L5037:
	leaq	.LC141(%rip), %rdx
	movl	$2, %r14d
	jmp	.L4946
.L5426:
	cmpb	$0, 32(%r12)
	leaq	24(%r12), %rbp
	je	.L5456
.L4859:
	leaq	192(%rsp), %r13
	movq	%rbp, %rdx
	movq	%r13, %rcx
	call	_ZNSt6localeC1ERKS_
	movq	.refptr._ZNSt7__cxx118numpunctIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	192(%rsp), %rcx
	movq	8(%rcx), %r9
	movq	(%r9,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L4860
	movq	%r13, %rcx
	call	_ZNSt6localeD1Ev
	movq	(%rdi), %r8
	testb	%sil, %sil
	je	.L5457
	leaq	160(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%r13, %rcx
.LEHB75:
	call	*40(%r8)
.L4863:
	movq	160(%rsp), %r11
	movq	%r11, %r10
	cmpq	%rbx, %r11
	je	.L5458
	movq	192(%rsp), %rax
	leaq	208(%rsp), %rsi
	cmpq	%rsi, %rax
	je	.L5011
	vmovdqu	200(%rsp), %xmm0
	movq	176(%rsp), %rbx
	movq	%rax, 160(%rsp)
	vmovdqu	%xmm0, 168(%rsp)
	testq	%r11, %r11
	je	.L4869
	movq	%r11, 192(%rsp)
	movq	%rbx, 208(%rsp)
.L4868:
	movq	%r13, %rcx
	movq	$0, 200(%rsp)
	leaq	160(%rsp), %rsi
	movb	$0, (%r11)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	168(%rsp), %rdx
	jmp	.L4870
.L4990:
	movzbl	160(%rsp,%r8), %r8d
	jmp	.L4991
.L5446:
	testl	%esi, %esi
	jne	.L4911
	movzbl	136(%rsp), %r8d
	movb	$48, 195(%rsp)
	testb	$16, %r8b
	je	.L4902
	movq	$-2, %rbp
	leaq	196(%rsp), %r13
	leaq	.LC142(%rip), %rax
	movl	$2, %r8d
	leaq	195(%rsp), %r12
	jmp	.L4900
.L4947:
	leaq	195(%rsp), %rbp
	leaq	96(%rsp), %rcx
	leaq	227(%rsp), %r8
	movq	%rbp, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r12
	leaq	.LC142(%rip), %rdx
	jmp	.L4948
.L5046:
	leaq	.LC142(%rip), %r12
	jmp	.L4985
.L5457:
	leaq	160(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%r13, %rcx
	call	*48(%r8)
.LEHE75:
	jmp	.L4863
.L5455:
	leaq	96(%rsp), %rcx
	leaq	195(%rsp), %r12
.L5419:
	leaq	227(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	leaq	.LC142(%rip), %rax
	jmp	.L4913
.L5025:
	leaq	196(%rsp), %r13
	leaq	195(%rsp), %r12
	jmp	.L4912
.L5048:
	leaq	196(%rsp), %rbx
	leaq	.LC141(%rip), %r12
	leaq	195(%rsp), %rdi
	jmp	.L4987
.L5035:
	leaq	196(%rsp), %r12
	leaq	195(%rsp), %rbp
	jmp	.L4950
.L5036:
	leaq	.LC141(%rip), %rdx
	jmp	.L4948
.L5026:
	leaq	.LC141(%rip), %rax
	jmp	.L4913
.L5443:
	addl	$2, %ebp
	jmp	.L4905
.L5449:
	addl	$2, %esi
	jmp	.L4941
.L5452:
	addl	$2, %ebx
	jmp	.L4975
.L5450:
	addl	$3, %esi
	jmp	.L4941
.L5453:
	addl	$3, %ebx
	jmp	.L4975
.L5444:
	addl	$3, %ebp
	jmp	.L4905
.L5456:
	movq	%rbp, %rcx
	call	_ZNSt6localeC1Ev
	movb	$1, 32(%r12)
	jmp	.L4859
.L5458:
	movq	192(%rsp), %rbp
	leaq	208(%rsp), %rsi
	cmpq	%rsi, %rbp
	je	.L5011
	vmovdqu	200(%rsp), %xmm1
	movq	%rbp, 160(%rsp)
	vmovdqu	%xmm1, 168(%rsp)
.L4869:
	leaq	208(%rsp), %r11
	movq	%rsi, 192(%rsp)
	jmp	.L4868
.L5052:
	movq	%r13, %rdx
	jmp	.L5002
.L5011:
	movq	200(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L4866
	cmpq	$1, %rdx
	je	.L5459
	movl	%edx, %ecx
	movq	%r10, %rdi
	rep movsb
	movq	160(%rsp), %r10
	movq	200(%rsp), %rdx
.L4866:
	movq	%rdx, 168(%rsp)
	movb	$0, (%r10,%rdx)
	movq	192(%rsp), %r11
	jmp	.L4868
.L5451:
	leaq	227(%rsp), %r12
	leaq	195(%rsp), %rbp
	jmp	.L4938
.L5445:
	leaq	227(%rsp), %r13
	leaq	195(%rsp), %r12
	jmp	.L5420
.L5454:
	leaq	259(%rsp), %rbx
	leaq	195(%rsp), %rdi
	jmp	.L4972
.L4911:
	leaq	96(%rsp), %rcx
	leaq	195(%rsp), %r12
	jmp	.L5419
.L5021:
	movl	$1, %ebp
	jmp	.L4903
.L5043:
	movl	$1, %ebx
	jmp	.L4973
.L5032:
	movl	$1, %esi
	jmp	.L4939
.L5459:
	movzbl	208(%rsp), %r9d
	movb	%r9b, (%r10)
	movq	160(%rsp), %r10
	movq	200(%rsp), %rdx
	jmp	.L4866
.L4880:
	leaq	.LC144(%rip), %rcx
.LEHB76:
	call	_ZSt20__throw_format_errorPKc
.L5427:
	leaq	.LC156(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L5425:
	leaq	.LC155(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L5053:
	movq	%rax, %r12
	vzeroupper
.L4874:
	movq	%rsi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r12, %rcx
	call	_Unwind_Resume
.LEHE76:
.L4860:
.LEHB77:
	call	_ZSt16__throw_bad_castv
.LEHE77:
.L4852:
.LEHB78:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
.L5441:
	call	_ZNSt8__format29__failed_to_parse_format_specEv
.LEHE78:
.L5054:
	movq	%r13, %rcx
	movq	%rax, %r12
	vzeroupper
	leaq	160(%rsp), %rsi
	call	_ZNSt6localeD1Ev
	jmp	.L4874
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15254:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15254-.LLSDACSB15254
.LLSDACSB15254:
	.uleb128 .LEHB72-.LFB15254
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB73-.LFB15254
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L5053-.LFB15254
	.uleb128 0
	.uleb128 .LEHB74-.LFB15254
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB75-.LFB15254
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L5053-.LFB15254
	.uleb128 0
	.uleb128 .LEHB76-.LFB15254
	.uleb128 .LEHE76-.LEHB76
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB77-.LFB15254
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L5054-.LFB15254
	.uleb128 0
	.uleb128 .LEHB78-.LFB15254
	.uleb128 .LEHE78-.LEHB78
	.uleb128 0
	.uleb128 0
.LLSDACSE15254:
	.section	.text$_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
	.align 8
.LC157:
	.ascii "format error: unmatched '}' in format string\0"
	.section	.text$_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE,"x"
	.linkonce discard
	.p2align 4
	.globl	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
	.def	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE:
.LFB5437:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$1080, %rsp
	.seh_stackalloc	1080
	vmovaps	%xmm6, 1040(%rsp)
	.seh_savexmm	%xmm6, 1040
	vmovaps	%xmm7, 1056(%rsp)
	.seh_savexmm	%xmm7, 1056
	.seh_endprologue
	vmovq	.LC158(%rip), %xmm1
	movq	(%rdx), %rsi
	movq	8(%rdx), %rbp
	movq	%rcx, 1152(%rsp)
	movq	(%r8), %r12
	movq	8(%r8), %r10
	leaq	16+_ZTVNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE(%rip), %r8
	leaq	752(%rsp), %rdx
	leaq	432(%rsp), %rcx
	vmovq	%r8, %xmm2
	leaq	720(%rsp), %r14
	movq	$256, 736(%rsp)
	leaq	1024(%rsp), %rbx
	vpinsrq	$1, %rcx, %xmm2, %xmm3
	leaq	288(%rsp), %r13
	movq	%rdx, 744(%rsp)
	vmovdqa	%xmm3, 400(%rsp)
	movq	%rbx, 64(%rsp)
	movq	%rbx, 1008(%rsp)
	movq	$0, 1016(%rsp)
	movq	%rsi, %r15
	movq	%rbp, %rdi
	movb	$0, 1024(%rsp)
	movq	$256, 416(%rsp)
	vpinsrq	$1, %rdx, %xmm1, %xmm0
	movq	%rcx, 424(%rsp)
	movq	%r14, 688(%rsp)
	movq	$-1, 696(%rsp)
	vmovdqa	%xmm0, 720(%rsp)
	movq	$0, 704(%rsp)
	movq	%r14, 104(%rsp)
	cmpq	$2, %rsi
	je	.L5699
.L5461:
	leaq	0(%rbp,%rsi), %rdx
	leaq	16+_ZTVNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE(%rip), %rbx
	movq	%r12, 288(%rsp)
	movq	%r10, 296(%rsp)
	movq	%r14, 304(%rsp)
	movq	$0, 312(%rsp)
	movb	$0, 320(%rsp)
	movq	%rbp, 344(%rsp)
	movq	%rdx, 352(%rsp)
	movl	$0, 360(%rsp)
	movq	$0, 368(%rsp)
	movq	$-1, 376(%rsp)
	movq	%rbx, 336(%rsp)
	movq	%r13, 384(%rsp)
	cmpq	$2, %rsi
	je	.L5700
	testq	%rsi, %rsi
	jne	.L5467
	.p2align 4
	.p2align 3
.L5465:
	movq	728(%rsp), %r9
	movq	744(%rsp), %r8
	movq	1016(%rsp), %rbp
	subq	%r9, %r8
	movq	%rbp, %rdx
	jne	.L5701
	movq	1152(%rsp), %rsi
	movq	1008(%rsp), %rdi
	movq	64(%rsp), %r13
	leaq	16(%rsi), %r10
	movq	%r10, (%rsi)
	cmpq	%r13, %rdi
	je	.L5545
.L5546:
	movq	1152(%rsp), %r10
	movq	1024(%rsp), %rdx
	movq	%rdx, 16(%r10)
	movq	%rbp, %rdx
	movq	%rdi, (%r10)
.L5538:
	movq	1152(%rsp), %rax
	movq	%rdx, 8(%rax)
	vmovaps	1040(%rsp), %xmm6
	vmovaps	1056(%rsp), %xmm7
	addq	$1080, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L5700:
	cmpb	$123, 0(%rbp)
	je	.L5702
.L5467:
	movq	%rsi, %r8
	movq	%rbp, %rcx
	movl	$123, %edx
	movq	$-1, %r12
	call	memchr
	movq	%rbp, %rcx
	movq	%rsi, %r8
	movl	$125, %edx
	movq	%rax, %rbx
	subq	%rbp, %rbx
	testq	%rax, %rax
	cmove	%r12, %rbx
	call	memchr
	leaq	120(%rsp), %rcx
	movq	%rax, %rsi
	vmovq	%rcx, %xmm7
	subq	%rbp, %rsi
	testq	%rax, %rax
	leaq	336(%rsp), %rbp
	cmove	%r12, %rsi
	vmovq	%rbp, %xmm6
	.p2align 4
	.p2align 3
.L5473:
	cmpq	%rbx, %rsi
	je	.L5703
	cmpq	%rsi, %rbx
	jb	.L5478
.L5479:
	leaq	1(%rsi), %rbp
	cmpq	%r15, %rbp
	je	.L5519
	cmpb	$125, (%rdi,%rbp)
	jne	.L5519
	movq	344(%rsp), %r10
	movq	384(%rsp), %r15
	movq	%r10, 56(%rsp)
	movq	16(%r15), %r14
	testq	%rbp, %rbp
	jne	.L5704
.L5520:
	movq	56(%rsp), %rcx
	movq	%r14, 16(%r15)
	movq	352(%rsp), %r14
	leaq	2(%rcx,%rsi), %rdi
	leaq	-1(%rbx), %rsi
	movq	%r14, %r15
	subq	%rbp, %rsi
	cmpq	$-1, %rbx
	movq	%rdi, 344(%rsp)
	cmovne	%rsi, %rbx
	subq	%rdi, %r15
	je	.L5476
	movq	%r15, %r8
	movl	$125, %edx
	movq	%rdi, %rcx
	call	memchr
	movq	%rax, %rsi
	subq	%rdi, %rsi
	testq	%rax, %rax
	jne	.L5473
	movq	$-1, %rsi
	cmpq	$-1, %rbx
	je	.L5477
	.p2align 4
	.p2align 3
.L5478:
	leaq	1(%rbx), %r8
	cmpq	%r15, %r8
	je	.L5481
	movzbl	(%rdi,%r8), %edi
	cmpq	$-1, %rsi
	je	.L5518
	cmpb	$123, %dil
	movq	344(%rsp), %rdi
	sete	%r15b
	movzbl	%r15b, %r8d
	sete	%r9b
	addq	%rbx, %r8
	leaq	(%rdi,%r8), %r12
	jne	.L5705
	movq	352(%rsp), %r8
	incq	%rdi
	movq	%rdi, 344(%rsp)
.L5485:
	movsbw	(%rdi), %r13w
	cmpb	$125, %r13b
	je	.L5706
	cmpb	$58, %r13b
	je	.L5707
	cmpb	$48, %r13b
	je	.L5708
	leal	-49(%r13), %r10d
	cmpb	$8, %r10b
	ja	.L5498
	addq	$2, %r12
	cmpq	%r8, %r12
	je	.L5499
	movzbl	1(%rdi), %edx
	subl	$48, %edx
	cmpb	$9, %dl
	ja	.L5499
	movq	%r8, %rbx
	xorl	%eax, %eax
	movq	%rdi, %r12
	movl	$16, %ebp
	subq	%rdi, %rbx
	andl	$3, %ebx
	je	.L5500
	cmpq	$1, %rbx
	je	.L5638
	cmpq	$2, %rbx
	je	.L5639
	subl	$48, %r13d
	cmpb	$9, %r13b
	ja	.L5501
	movl	$12, %ebp
	movzbl	%r13b, %eax
	leaq	1(%rdi), %r12
.L5639:
	movzbl	(%r12), %ecx
	leal	-48(%rcx), %r15d
	cmpb	$9, %r15b
	ja	.L5501
	subl	$4, %ebp
	js	.L5709
	leal	(%rax,%rax,4), %r9d
	movzbl	%r15b, %eax
	leal	(%rax,%r9,2), %eax
.L5680:
	incq	%r12
.L5638:
	movzbl	(%r12), %r11d
	leal	-48(%r11), %r13d
	cmpb	$9, %r13b
	ja	.L5501
	subl	$4, %ebp
	js	.L5710
	leal	(%rax,%rax,4), %ebx
	movzbl	%r13b, %ecx
	leal	(%rcx,%rbx,2), %eax
.L5682:
	incq	%r12
	cmpq	%r8, %r12
	je	.L5497
.L5500:
	movzbl	(%r12), %r15d
	leal	-48(%r15), %r14d
	cmpb	$9, %r14b
	ja	.L5501
	subl	$4, %ebp
	js	.L5502
	leal	(%rax,%rax,4), %eax
	movzbl	%r14b, %r11d
	leal	(%r11,%rax,2), %eax
.L5503:
	movzbl	1(%r12), %r10d
	leaq	1(%r12), %r13
	movq	%r13, %r12
	leal	-48(%r10), %ebx
	cmpb	$9, %bl
	ja	.L5501
	movl	%ebp, %r12d
	subl	$4, %r12d
	js	.L5711
	leal	(%rax,%rax,4), %r15d
	movzbl	%bl, %r14d
	leal	(%r14,%r15,2), %eax
.L5684:
	movzbl	1(%r13), %esi
	leaq	1(%r13), %r12
	leal	-48(%rsi), %r9d
	cmpb	$9, %r9b
	ja	.L5501
	movl	%ebp, %r11d
	subl	$8, %r11d
	js	.L5712
	leal	(%rax,%rax,4), %eax
	movzbl	%r9b, %r12d
	leal	(%r12,%rax,2), %eax
.L5686:
	movzbl	2(%r13), %edx
	leaq	2(%r13), %r12
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	ja	.L5501
	subl	$12, %ebp
	js	.L5713
	leal	(%rax,%rax,4), %esi
	movzbl	%cl, %r9d
	leal	(%r9,%rsi,2), %eax
.L5688:
	leaq	3(%r13), %r12
	cmpq	%r8, %r12
	jne	.L5500
	jmp	.L5497
	.p2align 4
	.p2align 3
.L5703:
	movq	352(%rsp), %r14
.L5477:
	movq	344(%rsp), %rdi
	movq	384(%rsp), %rbx
	movq	%r14, %rbp
	subq	%rdi, %rbp
	movq	16(%rbx), %r15
	jne	.L5714
.L5480:
	movq	%r15, 16(%rbx)
	movq	%r14, 344(%rsp)
	.p2align 4
	.p2align 3
.L5476:
	movzbl	320(%rsp), %r8d
	testb	%r8b, %r8b
	je	.L5465
	leaq	312(%rsp), %rcx
	call	_ZNSt6localeD1Ev
	jmp	.L5465
.L5717:
	cmpq	$-1, %rbx
	je	.L5697
	leaq	1(%rbx), %r8
	cmpq	%r8, %r15
	je	.L5481
	movzbl	(%rdi,%r8), %edi
	.p2align 4
	.p2align 3
.L5518:
	cmpb	$123, %dil
	jne	.L5481
	movq	384(%rsp), %r14
	movq	344(%rsp), %rdi
	movq	$-1, %rsi
	movl	$1, %r9d
	movq	16(%r14), %rcx
	leaq	(%rdi,%r8), %r12
.L5548:
	leaq	80(%rsp), %r13
	movb	%r9b, 79(%rsp)
	movq	%r8, 80(%rsp)
	movq	%rdi, 88(%rsp)
	movq	%r13, %rdx
	movq	%rcx, 56(%rsp)
.LEHB79:
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	352(%rsp), %rbp
	movq	56(%rsp), %rax
	leaq	1(%r12), %rdi
	movzbl	79(%rsp), %r11d
	movq	%rbp, %r15
	movq	%rax, 16(%r14)
	movq	%rbp, %r8
	movq	%rdi, 344(%rsp)
	subq	%rdi, %r15
	testb	%r11b, %r11b
	je	.L5485
	leaq	-2(%rsi), %r11
	subq	%rbx, %r11
	cmpq	$-1, %rsi
	cmovne	%r11, %rsi
	testq	%r15, %r15
	je	.L5476
	movq	%r15, %r8
	movl	$123, %edx
	movq	%rdi, %rcx
	call	memchr
	movq	%rax, %rbx
	subq	%rdi, %rbx
	testq	%rax, %rax
	jne	.L5473
	cmpq	$-1, %rsi
	je	.L5489
	movq	$-1, %rbx
	jmp	.L5479
	.p2align 4
	.p2align 3
.L5704:
	movq	24(%r14), %rcx
	movq	16(%r14), %r12
	movq	56(%rsp), %rdi
	movq	%rbp, %r13
	movq	%rcx, %rdx
	subq	8(%r14), %rdx
	subq	%rdx, %r12
	cmpq	%r12, %rbp
	jb	.L5521
	.p2align 4
	.p2align 3
.L5523:
	cmpq	%r12, %r13
	movq	%r12, %r8
	cmovbe	%r13, %r8
	testq	%r8, %r8
	je	.L5522
	movq	%rdi, %rdx
	call	memcpy
	movq	24(%r14), %rcx
.L5522:
	movq	(%r14), %r9
	addq	%r12, %rcx
	addq	%r12, %rdi
	subq	%r12, %r13
	movq	%rcx, 24(%r14)
	movq	%r14, %rcx
	call	*(%r9)
	movq	24(%r14), %rcx
	movq	16(%r14), %r12
	movq	%rcx, %rax
	subq	8(%r14), %rax
	subq	%rax, %r12
	cmpq	%r12, %r13
	jnb	.L5523
	testq	%r13, %r13
	je	.L5520
.L5521:
	movq	%r13, %r8
	movq	%rdi, %rdx
	call	memcpy
	addq	%r13, 24(%r14)
	jmp	.L5520
	.p2align 4
	.p2align 3
.L5714:
	leaq	80(%rsp), %r13
.L5549:
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%rbp, 80(%rsp)
	movq	%rdi, 88(%rsp)
	call	_ZNSt8__format5_SinkIcE8_M_writeESt17basic_string_viewIcSt11char_traitsIcEE
	movq	352(%rsp), %r14
	jmp	.L5480
	.p2align 4
	.p2align 3
.L5706:
	cmpl	$1, 360(%rsp)
	je	.L5715
	movq	368(%rsp), %rdi
	movl	$2, 360(%rsp)
	leaq	1(%rdi), %r11
	movq	%r11, 368(%rsp)
.L5493:
	movq	384(%rsp), %r10
	movzbl	(%r10), %ebx
	movl	%ebx, %edx
	andl	$15, %ebx
	andl	$15, %edx
	cmpq	%rbx, %rdi
	jnb	.L5512
	leaq	(%rdi,%rdi,4), %rax
	movl	$4, %esi
	shrx	%rsi, (%r10), %r9
	salq	$4, %rdi
	shrx	%rax, %r9, %r8
	addq	8(%r10), %rdi
	movl	%r8d, %ebx
	andl	$31, %r8d
	andl	$31, %ebx
	vmovdqa	(%rdi), %xmm2
	vmovdqa	%xmm2, 256(%rsp)
.L5513:
	movb	%bl, 272(%rsp)
	vmovdqu	256(%rsp), %ymm3
	vmovq	%xmm6, 120(%rsp)
	leaq	224(%rsp), %rcx
	vmovq	%xmm7, %rdx
	vmovdqu	%ymm3, 224(%rsp)
	vzeroupper
	call	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE
.LEHE79:
	movq	344(%rsp), %rdi
	movq	352(%rsp), %rbp
	cmpq	%rbp, %rdi
	je	.L5514
	cmpb	$125, (%rdi)
	jne	.L5514
	incq	%rdi
	movq	%rbp, %r15
	subq	%rdi, %r15
	movq	%rdi, 344(%rsp)
	je	.L5476
	movq	%r15, %r8
	movl	$123, %edx
	movq	%rdi, %rcx
	call	memchr
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5716
	movq	%r15, %r8
	movl	$125, %edx
	movq	%rdi, %rcx
	subq	%rdi, %rbx
	call	memchr
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L5717
.L5517:
	subq	%rdi, %rsi
	jmp	.L5473
	.p2align 4
	.p2align 3
.L5512:
	movl	%edx, %ebx
	testb	%dl, %dl
	jne	.L5561
	movl	$4, %ecx
	xorl	%r8d, %r8d
	shrx	%rcx, (%r10), %r15
	cmpq	%r15, %rdi
	jnb	.L5513
	salq	$5, %rdi
	addq	8(%r10), %rdi
	vmovdqu	(%rdi), %xmm0
	vmovdqa	%xmm0, 256(%rsp)
	movzbl	16(%rdi), %r14d
	movb	%r14b, 272(%rsp)
	movzbl	16(%rdi), %r8d
	movl	%r8d, %ebx
	jmp	.L5513
	.p2align 4
	.p2align 3
.L5699:
	cmpb	$123, 0(%rbp)
	jne	.L5461
	cmpb	$125, 1(%rbp)
	jne	.L5461
	movb	$0, 256(%rsp)
	testb	$15, %r12b
	je	.L5462
	vmovdqa	(%r10), %xmm4
	movq	%r12, %r8
	shrq	$4, %r8
	movl	%r8d, %eax
	andl	$31, %r8d
	andl	$31, %eax
	vmovdqa	%xmm4, 128(%rsp)
.L5463:
	movb	%al, 144(%rsp)
	vmovdqu	128(%rsp), %ymm1
	movq	%r10, 56(%rsp)
	leaq	104(%rsp), %r10
	vmovq	%r10, %xmm6
	leaq	256(%rsp), %r11
	leaq	288(%rsp), %r13
	leaq	336(%rsp), %rcx
	vpinsrq	$1, %r11, %xmm6, %xmm7
	movq	%r13, %rdx
	leaq	1008(%rsp), %rbx
	vmovdqa	%xmm7, 288(%rsp)
	vmovdqu	%ymm1, 336(%rsp)
	vzeroupper
.LEHB80:
	call	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_15__do_vformat_toIS3_cS4_EET_S8_St17basic_string_viewIT0_St11char_traitsISA_EERKSt17basic_format_argsIT1_EPKSt6localeEUlRS8_E_EEDcOS8_NS1_6_Arg_tE
.LEHE80:
	cmpb	$0, 256(%rsp)
	jne	.L5465
	movq	104(%rsp), %r14
	movq	56(%rsp), %r10
	jmp	.L5461
	.p2align 4
	.p2align 3
.L5561:
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	jmp	.L5513
	.p2align 4
	.p2align 3
.L5701:
	movabsq	$9223372036854775807, %rax
	subq	%rbp, %rax
	cmpq	%r8, %rax
	jb	.L5718
	movq	1008(%rsp), %r14
	movq	64(%rsp), %rcx
	leaq	(%r8,%rbp), %r12
	cmpq	%rcx, %r14
	je	.L5565
	movq	1024(%rsp), %r15
.L5533:
	cmpq	%r12, %r15
	jb	.L5534
	leaq	(%r14,%rbp), %rcx
	cmpq	$1, %r8
	je	.L5719
	movq	%r9, %rdx
	call	memcpy
.L5536:
	movq	1008(%rsp), %r11
	movq	1152(%rsp), %r8
	movq	%r12, 1016(%rsp)
	movq	64(%rsp), %rbp
	movb	$0, (%r11,%r12)
	movq	728(%rsp), %rbx
	movq	1008(%rsp), %rdi
	leaq	16(%r8), %r10
	movq	%r10, (%r8)
	movq	%rbx, 744(%rsp)
	cmpq	%rbp, %rdi
	je	.L5537
	movq	1016(%rsp), %rbp
	jmp	.L5546
	.p2align 4
	.p2align 3
.L5707:
	cmpl	$1, 360(%rsp)
	je	.L5720
	movq	368(%rsp), %rdi
	addq	$2, %r12
	movl	$2, 360(%rsp)
	movq	%r12, 344(%rsp)
	leaq	1(%rdi), %r13
	movq	%r13, 368(%rsp)
	jmp	.L5493
	.p2align 4
	.p2align 3
.L5708:
	addq	$2, %r12
	xorl	%eax, %eax
.L5497:
	movzbl	(%r12), %r8d
	cmpb	$125, %r8b
	je	.L5568
	cmpb	$58, %r8b
	jne	.L5498
.L5568:
	cmpl	$2, 360(%rsp)
	movzwl	%ax, %edi
	je	.L5721
	xorl	%ebp, %ebp
	movl	$1, 360(%rsp)
	cmpb	$58, (%r12)
	sete	%bpl
	addq	%r12, %rbp
	movq	%rbp, 344(%rsp)
	jmp	.L5493
.L5702:
	cmpb	$125, 1(%rbp)
	jne	.L5467
	incq	%rbp
	movl	$2, 360(%rsp)
	movq	$1, 368(%rsp)
	movq	%rbp, 344(%rsp)
	testb	$15, 288(%rsp)
	je	.L5722
	vmovdqa	(%r10), %xmm4
	movq	%r12, %r8
	shrq	$4, %r8
	movl	%r8d, %r11d
	andl	$31, %r8d
	andl	$31, %r11d
	vmovdqa	%xmm4, 192(%rsp)
.L5475:
	movb	%r11b, 208(%rsp)
	vmovdqu	192(%rsp), %ymm1
	leaq	336(%rsp), %rbx
	leaq	112(%rsp), %rdx
	leaq	160(%rsp), %rcx
	movq	%rbx, 112(%rsp)
	vmovdqu	%ymm1, 160(%rsp)
	vzeroupper
.LEHB81:
	call	_ZNSt16basic_format_argISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE8_M_visitIZNS1_19_Formatting_scannerIS3_cE13_M_format_argEyEUlRT_E_EEDcOS9_NS1_6_Arg_tE
.LEHE81:
	jmp	.L5476
.L5537:
	movq	1016(%rsp), %rdx
.L5545:
	leaq	1(%rdx), %rdi
	cmpl	$8, %edi
	jnb	.L5539
	testb	$4, %dil
	jne	.L5723
	testl	%edi, %edi
	je	.L5538
	movzbl	1024(%rsp), %r14d
	movb	%r14b, (%r10)
	testb	$2, %dil
	je	.L5538
	movq	64(%rsp), %rcx
	movl	%edi, %r15d
	movzwl	-2(%rcx,%r15), %r9d
	movw	%r9w, -2(%r10,%r15)
	jmp	.L5538
	.p2align 4
	.p2align 3
.L5716:
	movq	%r15, %r8
	movl	$125, %edx
	movq	%rdi, %rcx
	call	memchr
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L5697
	movq	$-1, %rbx
	jmp	.L5517
.L5499:
	leal	-48(%r13), %eax
	jmp	.L5497
.L5534:
	leaq	1008(%rsp), %rbx
	movq	%r8, 32(%rsp)
	movq	%rbp, %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rcx
.LEHB82:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEyyPKcy
.LEHE82:
	jmp	.L5536
.L5697:
	leaq	80(%rsp), %r13
.L5489:
	movq	384(%rsp), %rbx
	subq	%rdi, %rbp
	movq	16(%rbx), %r15
	jmp	.L5549
.L5539:
	movq	1024(%rsp), %r11
	movl	%edi, %ebx
	leaq	8(%r10), %rbp
	andq	$-8, %rbp
	movq	%r11, (%r10)
	movq	64(%rsp), %r14
	movq	-8(%r14,%rbx), %r8
	movq	%r8, -8(%r10,%rbx)
	subq	%rbp, %r10
	addl	%r10d, %edi
	subq	%r10, %r14
	movl	%edi, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L5538
	xorl	%esi, %esi
	leal	-1(%rax), %edi
	movl	$8, %r12d
	movq	(%r14,%rsi), %r10
	shrl	$3, %edi
	andl	$7, %edi
	movq	%r10, 0(%rbp,%rsi)
	cmpl	%eax, %r12d
	jnb	.L5538
	testl	%edi, %edi
	je	.L5543
	cmpl	$1, %edi
	je	.L5641
	cmpl	$2, %edi
	je	.L5642
	cmpl	$3, %edi
	je	.L5643
	cmpl	$4, %edi
	je	.L5644
	cmpl	$5, %edi
	je	.L5645
	cmpl	$6, %edi
	je	.L5646
	movq	(%r14,%r12), %r13
	movq	%r13, 0(%rbp,%r12)
	movl	$16, %r12d
.L5646:
	movl	%r12d, %r15d
	addl	$8, %r12d
	movq	(%r14,%r15), %rcx
	movq	%rcx, 0(%rbp,%r15)
.L5645:
	movl	%r12d, %r9d
	addl	$8, %r12d
	movq	(%r14,%r9), %r11
	movq	%r11, 0(%rbp,%r9)
.L5644:
	movl	%r12d, %ebx
	addl	$8, %r12d
	movq	(%r14,%rbx), %r8
	movq	%r8, 0(%rbp,%rbx)
.L5643:
	movl	%r12d, %edi
	addl	$8, %r12d
	movq	(%r14,%rdi), %rsi
	movq	%rsi, 0(%rbp,%rdi)
.L5642:
	movl	%r12d, %r13d
	addl	$8, %r12d
	movq	(%r14,%r13), %r10
	movq	%r10, 0(%rbp,%r13)
.L5641:
	movl	%r12d, %r15d
	addl	$8, %r12d
	movq	(%r14,%r15), %rcx
	movq	%rcx, 0(%rbp,%r15)
	cmpl	%eax, %r12d
	jnb	.L5538
.L5543:
	movl	%r12d, %r9d
	leal	8(%r12), %ebx
	leal	16(%r12), %edi
	leal	24(%r12), %r13d
	movq	(%r14,%r9), %r11
	leal	32(%r12), %r15d
	movq	%r11, 0(%rbp,%r9)
	movq	(%r14,%rbx), %r8
	leal	40(%r12), %r9d
	movq	%r8, 0(%rbp,%rbx)
	movq	(%r14,%rdi), %rsi
	leal	48(%r12), %ebx
	movq	%rsi, 0(%rbp,%rdi)
	movq	(%r14,%r13), %r10
	leal	56(%r12), %edi
	addl	$64, %r12d
	movq	%r10, 0(%rbp,%r13)
	movq	(%r14,%r15), %rcx
	movq	%rcx, 0(%rbp,%r15)
	movq	(%r14,%r9), %r11
	movq	%r11, 0(%rbp,%r9)
	movq	(%r14,%rbx), %r8
	movq	%r8, 0(%rbp,%rbx)
	movq	(%r14,%rdi), %rsi
	movq	%rsi, 0(%rbp,%rdi)
	cmpl	%eax, %r12d
	jb	.L5543
	jmp	.L5538
	.p2align 4
	.p2align 3
.L5565:
	movl	$15, %r15d
	jmp	.L5533
.L5719:
	movzbl	(%r9), %r9d
	movb	%r9b, (%rcx)
	jmp	.L5536
.L5462:
	movq	%r12, %r9
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	shrq	$4, %r9
	je	.L5463
	vmovdqu	(%r10), %xmm5
	movzbl	16(%r10), %r8d
	movl	%r8d, %eax
	vmovdqa	%xmm5, 128(%rsp)
	jmp	.L5463
.L5501:
	cmpq	%rdi, %r12
	jne	.L5497
.L5498:
.LEHB83:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L5502:
	movl	$10, %esi
	mulw	%si
	jo	.L5498
	movzbl	%r14b, %r9d
	addw	%ax, %r9w
	jc	.L5498
	movl	%r9d, %eax
	jmp	.L5503
.L5722:
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	shrq	$4, %r12
	je	.L5475
	vmovdqu	(%r10), %xmm5
	movzbl	16(%r10), %r8d
	movl	%r8d, %r11d
	vmovdqa	%xmm5, 192(%rsp)
	jmp	.L5475
.L5712:
	movl	$10, %r10d
	mulw	%r10w
	jo	.L5498
	movzbl	%r9b, %ebx
	addw	%ax, %bx
	jc	.L5498
	movl	%ebx, %eax
	jmp	.L5686
.L5711:
	movl	$10, %edx
	mulw	%dx
	jo	.L5498
	movzbl	%bl, %ecx
	addw	%ax, %cx
	jc	.L5498
	movl	%ecx, %eax
	jmp	.L5684
.L5723:
	movl	1024(%rsp), %esi
	movl	%edi, %r13d
	movl	%esi, (%r10)
	movq	64(%rsp), %rax
	movl	-4(%rax,%r13), %r12d
	movl	%r12d, -4(%r10,%r13)
	jmp	.L5538
.L5713:
	movl	$10, %r15d
	mulw	%r15w
	jo	.L5498
	movzbl	%cl, %r14d
	addw	%ax, %r14w
	jc	.L5498
	movl	%r14d, %eax
	jmp	.L5688
.L5710:
	movl	$10, %r10d
	mulw	%r10w
	jo	.L5498
	movzbl	%r13b, %edx
	addw	%ax, %dx
	jc	.L5498
	movl	%edx, %eax
	jmp	.L5682
.L5709:
	movl	$10, %r14d
	mulw	%r14w
	jo	.L5498
	movzbl	%r15b, %esi
	addw	%ax, %si
	jc	.L5498
	movl	%esi, %eax
	jmp	.L5680
.L5705:
	movq	384(%rsp), %r14
	movq	16(%r14), %rcx
	jmp	.L5548
.L5514:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
.LEHE83:
.L5566:
	movq	%rax, %rbp
	vzeroupper
.L5531:
	leaq	16+_ZTVNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rdx
	movq	%rbx, %rcx
	movq	%rdx, 720(%rsp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbp, %rcx
.LEHB84:
	call	_Unwind_Resume
.LEHE84:
.L5718:
	leaq	.LC123(%rip), %rcx
	leaq	1008(%rsp), %rbx
.LEHB85:
	call	_ZSt20__throw_length_errorPKc
.LEHE85:
.L5721:
.LEHB86:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.L5567:
	cmpb	$0, 320(%rsp)
	movq	%rax, %rbp
	jne	.L5724
	vzeroupper
.L5530:
	leaq	1008(%rsp), %rbx
	jmp	.L5531
.L5481:
	call	_ZNSt8__format39__unmatched_left_brace_in_format_stringEv
.L5519:
	leaq	.LC157(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L5715:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.L5720:
	call	_ZNSt8__format39__conflicting_indexing_in_format_stringEv
.LEHE86:
.L5724:
	leaq	312(%rsp), %rcx
	vzeroupper
	call	_ZNSt6localeD1Ev
	jmp	.L5530
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA5437:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5437-.LLSDACSB5437
.LLSDACSB5437:
	.uleb128 .LEHB79-.LFB5437
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L5567-.LFB5437
	.uleb128 0
	.uleb128 .LEHB80-.LFB5437
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L5566-.LFB5437
	.uleb128 0
	.uleb128 .LEHB81-.LFB5437
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L5567-.LFB5437
	.uleb128 0
	.uleb128 .LEHB82-.LFB5437
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L5566-.LFB5437
	.uleb128 0
	.uleb128 .LEHB83-.LFB5437
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L5567-.LFB5437
	.uleb128 0
	.uleb128 .LEHB84-.LFB5437
	.uleb128 .LEHE84-.LEHB84
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB85-.LFB5437
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L5566-.LFB5437
	.uleb128 0
	.uleb128 .LEHB86-.LFB5437
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L5567-.LFB5437
	.uleb128 0
.LLSDACSE5437:
	.section	.text$_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE,"x"
	.linkonce discard
	.seh_endproc
	.section .rdata,"dr"
.LC159:
	.ascii "v.f64.a < 0 error\12\0"
.LC166:
	.ascii "{:.6e}\0"
.LC167:
	.ascii "{:.10e}\0"
	.align 8
.LC168:
	.ascii "int(log10)=%d , jiema=%d, frac=%d, weishu=%d   %s     %s  %s  %s\12\0"
.LC169:
	.ascii "exp=%d ok\12\0"
.LC170:
	.ascii "exp=%d not ok--------------\12\0"
.LC171:
	.ascii "all_error=%d\12\0"
.LC173:
	.ascii "test_func duration1= %lf s\12\0"
	.text
	.p2align 4
	.globl	_Z9test_funcv
	.def	_Z9test_funcv;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z9test_funcv
_Z9test_funcv:
.LFB13527:
	pushq	%r15
	.seh_pushreg	%r15
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$488, %rsp
	.seh_stackalloc	488
	vmovaps	%xmm6, 368(%rsp)
	.seh_savexmm	%xmm6, 368
	vmovaps	%xmm7, 384(%rsp)
	.seh_savexmm	%xmm7, 384
	vmovaps	%xmm8, 400(%rsp)
	.seh_savexmm	%xmm8, 400
	vmovaps	%xmm9, 416(%rsp)
	.seh_savexmm	%xmm9, 416
	vmovaps	%xmm10, 432(%rsp)
	.seh_savexmm	%xmm10, 432
	vmovaps	%xmm11, 448(%rsp)
	.seh_savexmm	%xmm11, 448
	vmovaps	%xmm12, 464(%rsp)
	.seh_savexmm	%xmm12, 464
	.seh_endprologue
	vxorps	%xmm7, %xmm7, %xmm7
	call	_ZNSt6chrono3_V212system_clock3nowEv
	vmovq	.LC13(%rip), %xmm8
	vmovapd	%xmm8, %xmm10
	movl	$1051721728, 104(%rsp)
	movq	%rax, 120(%rsp)
	movl	$-20, 108(%rsp)
	movl	$0, 116(%rsp)
.L5726:
	xorl	%ebx, %ebx
	movl	$1, 112(%rsp)
	vmovsd	.LC14(%rip), %xmm9
	.p2align 4
	.p2align 3
.L5772:
	movl	104(%rsp), %esi
	movl	%ebx, %eax
	movl	%ebx, %edx
	vxorpd	%xmm0, %xmm0, %xmm0
	movl	%ebx, 92(%rsp)
	sall	$29, %eax
	sarl	$3, %edx
	vmovd	%eax, %xmm6
	addl	%esi, %edx
	vpinsrd	$1, %edx, %xmm6, %xmm6
	vmovapd	%xmm6, %xmm11
	vcomisd	%xmm6, %xmm0
	ja	.L5895
	vucomisd	%xmm0, %xmm6
	jp	.L5729
	je	.L5896
.L5729:
	vmovq	%xmm6, %rdi
	vandpd	%xmm8, %xmm11, %xmm1
	leaq	_ZL5_10en(%rip), %r8
	movl	$314, %r12d
	shrq	$52, %rdi
	andl	$2047, %edi
	subl	$1023, %edi
	vcomisd	.LC15(%rip), %xmm1
	vcvtsi2sdl	%edi, %xmm7, %xmm2
	vmulsd	%xmm9, %xmm2, %xmm3
	vcvttsd2sil	%xmm3, %eax
	sbbl	$-1, %eax
	leal	324(%rax), %r9d
	xorl	%r11d, %r11d
	movslq	%r9d, %r10
	vmovsd	(%r8,%r10,8), %xmm4
	vcomisd	%xmm1, %xmm4
	seta	%r11b
	subl	%r11d, %eax
	subl	%eax, %r12d
	movslq	%r12d, %r13
	vmovsd	(%r8,%r13,8), %xmm5
	cmpl	$-302, %eax
	jl	.L5897
	vmulsd	%xmm11, %xmm5, %xmm12
.L5733:
	vmovsd	.LC12(%rip), %xmm1
	vaddsd	%xmm1, %xmm12, %xmm2
	vsubsd	%xmm1, %xmm2, %xmm3
	vcvttsd2siq	%xmm3, %rsi
	movl	%esi, %edx
	negl	%edx
	cmovs	%esi, %edx
	vcvtsi2sdl	%edx, %xmm7, %xmm4
	testl	%esi, %esi
	je	.L5734
	vmulsd	.LC161(%rip), %xmm4, %xmm5
	vcvttsd2sil	%xmm5, %ebp
	addw	$11824, %bp
.L5735:
	vmulsd	.LC162(%rip), %xmm4, %xmm12
	vcvttsd2sil	%xmm12, %edi
	movsbl	%dil, %r9d
	leaq	_ZZ20myDoubleToScientificILi0ELi6EEidPciE11short_array(%rip), %r11
	vcvtsi2sdl	%r9d, %xmm7, %xmm0
	vfnmadd132sd	.LC163(%rip), %xmm4, %xmm0
	vcvttsd2sil	%xmm0, %r10d
	vcvtsi2sdl	%r10d, %xmm7, %xmm1
	vmulsd	.LC164(%rip), %xmm1, %xmm2
	vcvttsd2sil	%xmm2, %r12d
	movsbl	%r12b, %r13d
	movsbq	%r12b, %r14
	vcvtsi2sdl	%r13d, %xmm7, %xmm3
	vfnmadd132sd	.LC165(%rip), %xmm1, %xmm3
	vcvttsd2sil	%xmm3, %r15d
	vcvtsi2sdl	%r15d, %xmm7, %xmm4
	vmulsd	.LC65(%rip), %xmm4, %xmm5
	vcvttsd2sil	%xmm5, %esi
	movsbq	%sil, %rcx
	movsbq	%dil, %r8
	movzwl	(%r11,%rcx,2), %edx
	movzwl	(%r11,%r14,2), %edi
	movzwl	(%r11,%r8,2), %r9d
	movl	%eax, %r8d
	movzwl	%bp, %ebp
	shrl	$22, %r8d
	movl	%eax, %r10d
	andl	$512, %r8d
	salq	$16, %rdx
	orq	%rdi, %rdx
	addl	$11109, %r8d
	salq	$16, %rdx
	orq	%r9, %rdx
	salq	$16, %rdx
	orq	%rbp, %rdx
	negl	%r10d
	cmovs	%eax, %r10d
	movq	%rdx, 176(%rsp)
	cmpl	$99, %r10d
	jg	.L5736
	cltq
	movswl	(%r11,%rax,2), %eax
	sall	$16, %eax
	addl	%r8d, %eax
	cltq
.L5737:
	leaq	176(%rsp), %r13
	movq	%rax, 184(%rsp)
.L5731:
	movl	$129, %r8d
	leaq	128(%rsp), %rbp
	leaq	240(%rsp), %rcx
	leaq	.LC166(%rip), %rax
	leaq	144(%rsp), %rdx
	movq	%r8, 128(%rsp)
	leaq	336(%rsp), %r14
	movq	%rbp, %r8
	vmovq	%rdx, %xmm12
	vmovsd	%xmm11, 336(%rsp)
	movq	%rcx, 96(%rsp)
	movq	%rax, 152(%rsp)
	leaq	288(%rsp), %rsi
	movq	$6, 144(%rsp)
	movq	%r14, 136(%rsp)
.LEHB87:
	call	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
.LEHE87:
	movq	%r13, %rcx
	movq	%rsi, 272(%rsp)
	call	strlen
	movq	%rax, %r15
	cmpq	$15, %rax
	ja	.L5898
	cmpq	$1, %rax
	je	.L5899
	testq	%rax, %rax
	jne	.L5900
.L5742:
	movq	%rsi, %r13
.L5741:
	movq	%r15, 280(%rsp)
	movb	$0, 0(%r13,%r15)
	movq	248(%rsp), %r8
	cmpq	280(%rsp), %r8
	je	.L5901
.L5749:
	leaq	208(%rsp), %r15
	vmovq	%xmm11, %r8
	leaq	.LC0(%rip), %rdx
	vmovapd	%xmm11, %xmm2
	movq	%r15, %rcx
	incl	116(%rsp)
	leaq	320(%rsp), %rdi
	call	_Z7sprintfPcPKcz.constprop.0
	leaq	160(%rsp), %r11
	leaq	.LC167(%rip), %rax
	movq	%rbp, %r8
	vmovq	%xmm12, %rdx
	movq	%r14, %rcx
	movq	%rdi, 304(%rsp)
	movq	$0, 312(%rsp)
	movb	$0, 320(%rsp)
	vmovsd	%xmm11, 160(%rsp)
	movq	$7, 144(%rsp)
	movq	%rax, 152(%rsp)
	movq	$129, 128(%rsp)
	movq	%r11, 136(%rsp)
.LEHB88:
	call	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
	movq	304(%rsp), %rbp
	movq	%rbp, %r14
	cmpq	%rdi, %rbp
	je	.L5902
	movq	344(%rsp), %r10
	movq	336(%rsp), %rcx
	leaq	352(%rsp), %rdx
	movq	%r10, %r8
	cmpq	%rdx, %rcx
	je	.L5780
	vmovq	%r10, %xmm0
	vpinsrq	$1, 352(%rsp), %xmm0, %xmm1
	movq	320(%rsp), %r9
	movq	%rcx, 304(%rsp)
	vmovdqu	%xmm1, 312(%rsp)
	testq	%rbp, %rbp
	je	.L5754
	movq	%rbp, 336(%rsp)
	movq	%r9, 352(%rsp)
.L5763:
	movq	$0, 344(%rsp)
	movb	$0, 0(%rbp)
	movq	336(%rsp), %rcx
	cmpq	%rdx, %rcx
	je	.L5764
	movq	352(%rsp), %rdx
	leaq	1(%rdx), %rdx
	call	_ZdlPvy
.L5764:
	movq	272(%rsp), %rbp
	movq	240(%rsp), %r9
	movq	304(%rsp), %r8
	testq	%rbx, %rbx
	je	.L5782
	blsi	%rbx, %rax
	je	.L5783
	xorl	%r12d, %r12d
	.p2align 4
	.p2align 4
	.p2align 3
.L5766:
	movl	%r12d, %ecx
	incl	%r12d
	sarq	%rax
	jne	.L5766
	addl	$33, %ecx
.L5765:
	vxorpd	%xmm3, %xmm3, %xmm3
	vucomisd	%xmm3, %xmm11
	jp	.L5789
	movl	$0, %edx
	je	.L5767
.L5789:
	vmovq	%xmm6, %r14
	vandpd	%xmm10, %xmm11, %xmm4
	leaq	_ZL5_10en(%rip), %r10
	shrq	$52, %r14
	andl	$2047, %r14d
	subl	$1023, %r14d
	vcomisd	.LC15(%rip), %xmm4
	vcvtsi2sdl	%r14d, %xmm7, %xmm5
	vmulsd	.LC14(%rip), %xmm5, %xmm6
	vcvttsd2sil	%xmm6, %edx
	sbbl	$-1, %edx
	leal	324(%rdx), %eax
	xorl	%r11d, %r11d
	cltq
	vmovsd	(%r10,%rax,8), %xmm11
	vcomisd	%xmm4, %xmm11
	seta	%r11b
	subl	%r11d, %edx
.L5767:
	movq	%r9, 48(%rsp)
	movq	%r8, 40(%rsp)
	movl	92(%rsp), %r9d
	movl	108(%rsp), %r8d
	movl	%ecx, 32(%rsp)
	leaq	.LC168(%rip), %rcx
	movq	%rbp, 64(%rsp)
	movq	%r15, 56(%rsp)
	call	_Z6printfPKcz
.LEHE88:
	movq	304(%rsp), %rcx
	cmpq	%rdi, %rcx
	je	.L5769
	movq	320(%rsp), %r15
	leaq	1(%r15), %rdx
	call	_ZdlPvy
.L5769:
	movq	272(%rsp), %rdi
	movl	$0, 112(%rsp)
.L5750:
	cmpq	%rsi, %rdi
	je	.L5770
	movq	288(%rsp), %rsi
	movq	%rdi, %rcx
	leaq	1(%rsi), %rdx
	call	_ZdlPvy
.L5770:
	movq	240(%rsp), %rcx
	leaq	256(%rsp), %r13
	cmpq	%r13, %rcx
	je	.L5771
	movq	256(%rsp), %rdx
	leaq	1(%rdx), %rdx
	call	_ZdlPvy
.L5771:
	incq	%rbx
	cmpq	$8388608, %rbx
	jne	.L5772
	movl	112(%rsp), %ebx
	movl	108(%rsp), %edx
	testl	%ebx, %ebx
	jne	.L5903
	leaq	.LC170(%rip), %rcx
.LEHB89:
	call	_Z6printfPKcz
.L5774:
	incl	108(%rsp)
	movl	108(%rsp), %ebp
	addl	$1048576, 104(%rsp)
	cmpl	$201, %ebp
	jne	.L5726
	movl	116(%rsp), %edx
	leaq	.LC171(%rip), %rcx
	call	_Z6printfPKcz
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	120(%rsp), %r9
	vmovaps	368(%rsp), %xmm6
	vmovaps	400(%rsp), %xmm8
	leaq	.LC173(%rip), %rcx
	vmovaps	432(%rsp), %xmm10
	vmovaps	448(%rsp), %xmm11
	vmovaps	464(%rsp), %xmm12
	subq	%r9, %rax
	vcvtsi2sdq	%rax, %xmm7, %xmm9
	vmovaps	384(%rsp), %xmm7
	vdivsd	.LC172(%rip), %xmm9, %xmm1
	vmovaps	416(%rsp), %xmm9
	vmovq	%xmm1, %rdx
	addq	$488, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_Z6printfPKcz
.LEHE89:
	.p2align 4
	.p2align 3
.L5736:
	movslq	%eax, %r11
	movl	%eax, %r13d
	imulq	$1717986919, %r11, %r14
	sarl	$31, %r13d
	sarq	$34, %r14
	subl	%r13d, %r14d
	imulq	$1374389535, %r11, %r12
	movslq	%r14d, %r15
	movl	%r14d, %ecx
	movl	%r14d, %edi
	leal	(%r14,%r14,4), %ebp
	imulq	$1717986919, %r15, %rsi
	addl	%ebp, %ebp
	sarl	$31, %ecx
	sarq	$34, %rsi
	sarq	$37, %r12
	subl	%ecx, %esi
	subl	%r13d, %r12d
	subl	%ebp, %eax
	leal	(%rsi,%rsi,4), %edx
	addl	$48, %r12d
	addl	$48, %eax
	addl	%edx, %edx
	subl	%edx, %edi
	sall	$16, %r12d
	addl	$48, %edi
	addl	%r8d, %r12d
	sall	$24, %edi
	salq	$32, %rax
	addl	%edi, %r12d
	movslq	%r12d, %r9
	addq	%r9, %rax
	jmp	.L5737
	.p2align 4
	.p2align 3
.L5734:
	incl	%eax
	movl	$11825, %ebp
	jmp	.L5735
	.p2align 4
	.p2align 3
.L5898:
	leaq	1(%rax), %rcx
.LEHB90:
	call	_Znwy
.LEHE90:
	movq	%rax, 272(%rsp)
	movq	%r15, 288(%rsp)
.L5739:
	cmpl	$8, %r15d
	jnb	.L5743
	testb	$4, %r15b
	jne	.L5904
	testl	%r15d, %r15d
	je	.L5744
	movzbl	0(%r13), %edi
	movb	%dil, (%rax)
	testb	$2, %r15b
	jne	.L5905
.L5744:
	movq	272(%rsp), %r13
	jmp	.L5741
	.p2align 4
	.p2align 3
.L5899:
	movzbl	176(%rsp), %r10d
	movb	%r10b, 288(%rsp)
	jmp	.L5742
	.p2align 4
	.p2align 3
.L5901:
	movq	272(%rsp), %rdi
	testq	%r8, %r8
	je	.L5750
	movq	240(%rsp), %rcx
	movq	%rdi, %rdx
	call	memcmp
	testl	%eax, %eax
	je	.L5750
	jmp	.L5749
	.p2align 4
	.p2align 3
.L5743:
	movq	0(%r13), %r8
	movl	%r15d, %edx
	leaq	8(%rax), %r11
	andq	$-8, %r11
	movq	%r8, (%rax)
	movq	-8(%r13,%rdx), %r10
	movq	%r10, -8(%rax,%rdx)
	subq	%r11, %rax
	subq	%rax, %r13
	addl	%r15d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L5744
	movq	0(%r13), %rcx
	leal	-1(%rax), %r12d
	movl	$8, %r8d
	shrl	$3, %r12d
	andl	$7, %r12d
	movq	%rcx, (%r11)
	cmpl	%eax, %r8d
	jnb	.L5744
	testl	%r12d, %r12d
	je	.L5747
	cmpl	$1, %r12d
	je	.L5859
	cmpl	$2, %r12d
	je	.L5860
	cmpl	$3, %r12d
	je	.L5861
	cmpl	$4, %r12d
	je	.L5862
	cmpl	$5, %r12d
	je	.L5863
	cmpl	$6, %r12d
	jne	.L5906
.L5864:
	movl	%r8d, %edx
	addl	$8, %r8d
	movq	0(%r13,%rdx), %r9
	movq	%r9, (%r11,%rdx)
.L5863:
	movl	%r8d, %r10d
	addl	$8, %r8d
	movq	0(%r13,%r10), %r12
	movq	%r12, (%r11,%r10)
.L5862:
	movl	%r8d, %ecx
	addl	$8, %r8d
	movq	0(%r13,%rcx), %rdi
	movq	%rdi, (%r11,%rcx)
.L5861:
	movl	%r8d, %edx
	addl	$8, %r8d
	movq	0(%r13,%rdx), %r9
	movq	%r9, (%r11,%rdx)
.L5860:
	movl	%r8d, %r10d
	addl	$8, %r8d
	movq	0(%r13,%r10), %r12
	movq	%r12, (%r11,%r10)
.L5859:
	movl	%r8d, %ecx
	addl	$8, %r8d
	movq	0(%r13,%rcx), %rdi
	movq	%rdi, (%r11,%rcx)
	cmpl	%eax, %r8d
	jnb	.L5744
.L5747:
	movl	%r8d, %edx
	leal	8(%r8), %r10d
	leal	16(%r8), %ecx
	movq	0(%r13,%rdx), %r9
	movq	%r9, (%r11,%rdx)
	movq	0(%r13,%r10), %r12
	leal	24(%r8), %edx
	movq	%r12, (%r11,%r10)
	movq	0(%r13,%rcx), %rdi
	leal	32(%r8), %r10d
	movq	%rdi, (%r11,%rcx)
	movq	0(%r13,%rdx), %r9
	leal	40(%r8), %ecx
	movq	%r9, (%r11,%rdx)
	movq	0(%r13,%r10), %r12
	leal	48(%r8), %edx
	movq	%r12, (%r11,%r10)
	movq	0(%r13,%rcx), %rdi
	leal	56(%r8), %r10d
	addl	$64, %r8d
	movq	%rdi, (%r11,%rcx)
	movq	0(%r13,%rdx), %r9
	movq	%r9, (%r11,%rdx)
	movq	0(%r13,%r10), %r12
	movq	%r12, (%r11,%r10)
	cmpl	%eax, %r8d
	jb	.L5747
	jmp	.L5744
	.p2align 4
	.p2align 3
.L5896:
	movabsq	$3472328296227679792, %rcx
	movabsq	$13563761821495344, %rbp
	leaq	176(%rsp), %r13
	movq	%rcx, 176(%rsp)
	movq	%rbp, 181(%rsp)
	jmp	.L5731
	.p2align 4
	.p2align 3
.L5902:
	movq	336(%rsp), %r12
	leaq	352(%rsp), %rdx
	cmpq	%rdx, %r12
	je	.L5907
	vmovdqu	344(%rsp), %xmm2
	movq	%r12, 304(%rsp)
	vmovdqu	%xmm2, 312(%rsp)
.L5754:
	movq	%rdx, 336(%rsp)
	leaq	352(%rsp), %rdx
	movq	%rdx, %rbp
	jmp	.L5763
	.p2align 4
	.p2align 3
.L5900:
	movq	%rsi, %rax
	jmp	.L5739
	.p2align 4
	.p2align 3
.L5897:
	movl	$6, %r14d
	subl	%eax, %r14d
	movslq	%r14d, %r15
	vmulsd	(%r8,%r15,8), %xmm11, %xmm0
	vmulsd	.LC160(%rip), %xmm0, %xmm12
	jmp	.L5733
.L5907:
	movq	344(%rsp), %r10
	movq	%r10, %r8
	.p2align 4
	.p2align 3
.L5780:
	testq	%r10, %r10
	je	.L5755
	cmpq	$1, %r10
	je	.L5908
	movl	%r10d, %r13d
	cmpl	$8, %r10d
	jb	.L5909
	movq	(%rdx), %r8
	movl	%r10d, %r9d
	leaq	8(%r14), %r13
	movq	%rdx, %rbp
	andq	$-8, %r13
	movq	%r8, (%r14)
	movq	-8(%rdx,%r9), %r12
	movq	%r12, -8(%r14,%r9)
	subq	%r13, %r14
	leal	(%r10,%r14), %eax
	subq	%r14, %rbp
	movl	%eax, %r11d
	andl	$-8, %r11d
	cmpl	$8, %r11d
	jb	.L5888
	movq	0(%rbp), %rcx
	andl	$-8, %eax
	movl	$8, %r9d
	leal	-1(%rax), %r10d
	shrl	$3, %r10d
	andl	$7, %r10d
	movq	%rcx, 0(%r13)
	cmpl	%eax, %r9d
	jnb	.L5888
	testl	%r10d, %r10d
	je	.L5761
	cmpl	$1, %r10d
	je	.L5853
	cmpl	$2, %r10d
	je	.L5854
	cmpl	$3, %r10d
	je	.L5855
	cmpl	$4, %r10d
	je	.L5856
	cmpl	$5, %r10d
	je	.L5857
	cmpl	$6, %r10d
	jne	.L5910
.L5858:
	movl	%r9d, %r12d
	addl	$8, %r9d
	movq	0(%rbp,%r12), %r14
	movq	%r14, 0(%r13,%r12)
.L5857:
	movl	%r9d, %r11d
	addl	$8, %r9d
	movq	0(%rbp,%r11), %r10
	movq	%r10, 0(%r13,%r11)
.L5856:
	movl	%r9d, %ecx
	addl	$8, %r9d
	movq	0(%rbp,%rcx), %r8
	movq	%r8, 0(%r13,%rcx)
.L5855:
	movl	%r9d, %r12d
	addl	$8, %r9d
	movq	0(%rbp,%r12), %r14
	movq	%r14, 0(%r13,%r12)
.L5854:
	movl	%r9d, %r11d
	addl	$8, %r9d
	movq	0(%rbp,%r11), %r10
	movq	%r10, 0(%r13,%r11)
.L5853:
	movl	%r9d, %ecx
	addl	$8, %r9d
	movq	0(%rbp,%rcx), %r8
	movq	%r8, 0(%r13,%rcx)
	cmpl	%eax, %r9d
	jnb	.L5888
.L5761:
	movl	%r9d, %r12d
	leal	8(%r9), %r10d
	leal	16(%r9), %ecx
	movq	0(%rbp,%r12), %r14
	movq	%r14, 0(%r13,%r12)
	movq	0(%rbp,%r10), %r11
	leal	24(%r9), %r12d
	movq	%r11, 0(%r13,%r10)
	movq	0(%rbp,%rcx), %r8
	leal	32(%r9), %r10d
	movq	%r8, 0(%r13,%rcx)
	movq	0(%rbp,%r12), %r14
	leal	40(%r9), %ecx
	movq	%r14, 0(%r13,%r12)
	movq	0(%rbp,%r10), %r11
	leal	48(%r9), %r12d
	movq	%r11, 0(%r13,%r10)
	movq	0(%rbp,%rcx), %r8
	leal	56(%r9), %r10d
	addl	$64, %r9d
	movq	%r8, 0(%r13,%rcx)
	movq	0(%rbp,%r12), %r14
	movq	%r14, 0(%r13,%r12)
	movq	0(%rbp,%r10), %r11
	movq	%r11, 0(%r13,%r10)
	cmpl	%eax, %r9d
	jb	.L5761
.L5888:
	movq	304(%rsp), %r14
	movq	344(%rsp), %r10
.L5758:
	movq	%r10, %r8
	movq	%r14, %rbp
.L5755:
	movq	%r8, 312(%rsp)
	movb	$0, 0(%rbp,%r8)
	movq	336(%rsp), %rbp
	jmp	.L5763
	.p2align 4
	.p2align 3
.L5783:
	movl	$32, %ecx
	jmp	.L5765
.L5909:
	testb	$4, %r10b
	jne	.L5911
	testl	%r10d, %r10d
	je	.L5758
	movzbl	(%rdx), %ebp
	movb	%bpl, (%r14)
	testb	$2, %r10b
	je	.L5888
	movl	%r10d, %r10d
	movzwl	-2(%rdx,%r10), %ecx
	movw	%cx, -2(%r14,%r10)
	movq	304(%rsp), %r14
	movq	344(%rsp), %r10
	jmp	.L5758
	.p2align 4
	.p2align 3
.L5906:
	movq	0(%r13,%r8), %rdi
	movq	%rdi, (%r11,%r8)
	movl	$16, %r8d
	jmp	.L5864
.L5904:
	movl	0(%r13), %r11d
	movl	%r15d, %r12d
	movl	%r11d, (%rax)
	movl	-4(%r13,%r12), %ecx
	movl	%ecx, -4(%rax,%r12)
	jmp	.L5744
.L5908:
	movzbl	352(%rsp), %r13d
	movb	%r13b, (%r14)
	movq	344(%rsp), %r8
	movq	304(%rsp), %rbp
	jmp	.L5755
.L5905:
	movl	%r15d, %r9d
	movzwl	-2(%r13,%r9), %r13d
	movw	%r13w, -2(%rax,%r9)
	jmp	.L5744
.L5903:
	leaq	.LC169(%rip), %rcx
.LEHB91:
	call	_Z6printfPKcz
	jmp	.L5774
.L5782:
	movl	$31, %ecx
	jmp	.L5765
.L5910:
	movq	0(%rbp,%r9), %r8
	movq	%r8, 0(%r13,%r9)
	movl	$16, %r9d
	jmp	.L5858
.L5911:
	movl	(%rdx), %r11d
	movl	%r11d, (%r14)
	movl	-4(%rdx,%r13), %eax
	movl	%eax, -4(%r14,%r13)
	movq	304(%rsp), %r14
	movq	344(%rsp), %r10
	jmp	.L5758
.L5895:
	leaq	.LC159(%rip), %rcx
	call	_Z6printfPKcz
	call	abort
.L5786:
	leaq	304(%rsp), %rcx
	movq	%rax, %r12
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	leaq	272(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L5777:
	movq	96(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r12, %rcx
	call	_Unwind_Resume
.LEHE91:
.L5785:
	movq	%rax, %r12
	vzeroupper
	jmp	.L5777
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13527:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13527-.LLSDACSB13527
.LLSDACSB13527:
	.uleb128 .LEHB87-.LFB13527
	.uleb128 .LEHE87-.LEHB87
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB88-.LFB13527
	.uleb128 .LEHE88-.LEHB88
	.uleb128 .L5786-.LFB13527
	.uleb128 0
	.uleb128 .LEHB89-.LFB13527
	.uleb128 .LEHE89-.LEHB89
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB90-.LFB13527
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L5785-.LFB13527
	.uleb128 0
	.uleb128 .LEHB91-.LFB13527
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0
	.uleb128 0
.LLSDACSE13527:
	.text
	.seh_endproc
	.section .rdata,"dr"
.LC174:
	.ascii "start test_func2\0"
.LC177:
	.ascii "{:.16e}\0"
.LC178:
	.ascii "stod\0"
.LC179:
	.ascii "\12\0"
.LC180:
	.ascii "%.20le\0"
	.align 8
.LC181:
	.ascii "int(log10)=%4d , jiema=%2d, frac=%10lld, weishu=%3d   format16:%s     sprintf6/myfunc:%s  sprintf16:%s  mysci:%s\12\0"
.LC182:
	.ascii "clz=%d\12\0"
.LC183:
	.ascii "ieee754_exp=%d\12\0"
.LC184:
	.ascii "exponent=%d\12\0"
.LC185:
	.ascii "mul_result.f=%llx\12\0"
.LC186:
	.ascii "mul_result.e=%d\12\0"
.LC187:
	.ascii "tmp_num=%lld\12\0"
.LC188:
	.ascii "rest_num=%llx\12\0"
.LC189:
	.ascii "add_num=%d\12\0"
.LC190:
	.ascii "getexponent=%d\12\0"
.LC191:
	.ascii "num0123456=%lld\12\0"
.LC192:
	.ascii "num0=%d\12\0"
.LC193:
	.ascii "num1=%d\12\0"
.LC194:
	.ascii "num2_rest=%lld\12\0"
.LC195:
	.ascii "result_8[%d]=%d num12=%d \0"
.LC196:
	.ascii "result_8[%d]=%d \0"
	.align 8
.LC197:
	.ascii "num0=%d num0123456=%lld exponent=%d\12\0"
	.align 8
.LC199:
	.ascii "test_func2 duration1= %lf ms one is %lf ns\12\0"
	.align 8
.LC200:
	.ascii "test_func2 duration cycle= %lld cycle one is %lf cycle\12\0"
.LC201:
	.ascii "max_diff_std_my=%d \12\0"
.LC203:
	.ascii "all_same=%d same_percent=%lf\12\0"
.LC204:
	.ascii "diff1=%d diff1_percent=%lf\12\0"
.LC205:
	.ascii "diff2=%d diff2_percent=%lf\12\0"
.LC206:
	.ascii "diff3=%d diff3_percent=%lf\12\0"
	.align 8
.LC207:
	.ascii "diff_other=%d diff_other_percent=%lf\12\0"
	.align 8
.LC208:
	.ascii "test_func2 all_error=%d, percent=%lf\12\0"
.LC209:
	.ascii "minb=%d,maxb=%d\12\0"
	.text
	.p2align 4
	.globl	_Z10test_func2v
	.def	_Z10test_func2v;	.scl	2;	.type	32;	.endef
	.seh_proc	_Z10test_func2v
_Z10test_func2v:
.LFB13677:
	pushq	%r15
	.seh_pushreg	%r15
	movl	$5800, %eax
	pushq	%r14
	.seh_pushreg	%r14
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	call	___chkstk_ms
	subq	%rax, %rsp
	.seh_stackalloc	5800
	vmovaps	%xmm6, 5632(%rsp)
	.seh_savexmm	%xmm6, 5632
	vmovaps	%xmm7, 5648(%rsp)
	.seh_savexmm	%xmm7, 5648
	vmovaps	%xmm8, 5664(%rsp)
	.seh_savexmm	%xmm8, 5664
	vmovaps	%xmm9, 5680(%rsp)
	.seh_savexmm	%xmm9, 5680
	vmovaps	%xmm10, 5696(%rsp)
	.seh_savexmm	%xmm10, 5696
	vmovaps	%xmm11, 5712(%rsp)
	.seh_savexmm	%xmm11, 5712
	vmovaps	%xmm12, 5728(%rsp)
	.seh_savexmm	%xmm12, 5728
	vmovaps	%xmm13, 5744(%rsp)
	.seh_savexmm	%xmm13, 5744
	vmovaps	%xmm14, 5760(%rsp)
	.seh_savexmm	%xmm14, 5760
	vmovaps	%xmm15, 5776(%rsp)
	.seh_savexmm	%xmm15, 5776
	.seh_endprologue
	movq	.refptr._ZSt4cout(%rip), %rbx
	leaq	5583(%rsp), %r14
	leaq	.LC174(%rip), %rdx
	movl	$16, %r8d
	andq	$-64, %r14
	movq	%r14, 112(%rsp)
	movq	%rbx, %rcx
.LEHB92:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x
	movq	(%rbx), %rdx
	movq	-24(%rdx), %rcx
	movq	240(%rcx,%rbx), %rsi
	testq	%rsi, %rsi
	je	.L6088
	cmpb	$0, 56(%rsi)
	je	.L5914
	movsbl	67(%rsi), %edx
.L5915:
	movq	%rbx, %rcx
	leaq	3008(%rsp), %rbx
	leaq	3024(%rsp), %r12
	call	_ZNSo3putEc
	movq	%rax, %rcx
	call	_ZNSo5flushEv
.LEHE92:
	xorl	%ecx, %ecx
	call	_time64
	leaq	496(%rsp), %rcx
	movq	%rbx, %rdx
	movl	$1634100580, 3024(%rsp)
	movq	%r12, 3008(%rsp)
	movl	$1953264993, 3027(%rsp)
	movq	$7, 3016(%rsp)
	movb	$0, 3031(%rsp)
	movq	%rcx, 240(%rsp)
.LEHB93:
	call	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
.LEHE93:
	movq	3008(%rsp), %rcx
	cmpq	%r12, %rcx
	je	.L5916
	movq	3024(%rsp), %r9
	leaq	1(%r9), %rdx
	call	_ZdlPvy
.L5916:
	movq	240(%rsp), %rcx
.LEHB94:
	call	_ZNSt13random_device9_M_getvalEv
.LEHE94:
	movl	%eax, %edx
	movabsq	$6364136223846793005, %r10
	movl	$2, %r15d
	movq	%rdx, 3008(%rsp)
	imulq	%r10, %rdx
	incq	%rdx
	movq	%rdx, %r8
	movq	%rdx, 8(%rbx)
	.p2align 4
	.p2align 3
.L5919:
	movq	%r8, %r11
	shrq	$62, %r11
	xorq	%r8, %r11
	imulq	%r10, %r11
	addq	%r15, %r11
	movq	%r11, %rax
	movq	%r11, (%rbx,%r15,8)
	shrq	$62, %rax
	xorq	%rax, %r11
	imulq	%r10, %r11
	leaq	1(%r15,%r11), %r14
	movq	%r14, %rsi
	movq	%r14, 8(%rbx,%r15,8)
	shrq	$62, %rsi
	xorq	%r14, %rsi
	imulq	%r10, %rsi
	leaq	2(%r15,%rsi), %rdi
	movq	%rdi, %rbp
	movq	%rdi, 16(%rbx,%r15,8)
	shrq	$62, %rbp
	xorq	%rdi, %rbp
	imulq	%r10, %rbp
	leaq	3(%r15,%rbp), %r8
	movq	%r8, %r12
	movq	%r8, 24(%rbx,%r15,8)
	shrq	$62, %r12
	xorq	%r8, %r12
	imulq	%r10, %r12
	leaq	4(%r15,%r12), %rcx
	movq	%rcx, %r13
	movq	%rcx, 32(%rbx,%r15,8)
	shrq	$62, %r13
	xorq	%rcx, %r13
	imulq	%r10, %r13
	leaq	5(%r15,%r13), %r9
	movq	%r9, %rdx
	movq	%r9, 40(%rbx,%r15,8)
	shrq	$62, %rdx
	xorq	%r9, %rdx
	imulq	%r10, %rdx
	leaq	6(%r15,%rdx), %r11
	movq	%r11, %rax
	movq	%r11, 48(%rbx,%r15,8)
	shrq	$62, %rax
	xorq	%r11, %rax
	imulq	%r10, %rax
	leaq	7(%r15,%rax), %r14
	movq	%r14, %rsi
	movq	%r14, 56(%rbx,%r15,8)
	shrq	$62, %rsi
	xorq	%r14, %rsi
	imulq	%r10, %rsi
	leaq	8(%r15,%rsi), %rdi
	movq	%rdi, %rbp
	movq	%rdi, 64(%rbx,%r15,8)
	shrq	$62, %rbp
	xorq	%rdi, %rbp
	imulq	%r10, %rbp
	leaq	9(%r15,%rbp), %r8
	movq	%r8, 72(%rbx,%r15,8)
	addq	$10, %r15
	cmpq	$312, %r15
	jne	.L5919
	vxorps	%xmm6, %xmm6, %xmm6
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	%rax, 280(%rsp)
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	salq	$32, %rdx
	vpxor	%xmm0, %xmm0, %xmm0
	movl	$0, 176(%rsp)
	movl	$-308, 164(%rsp)
	vmovdqu	%ymm0, 336(%rsp)
	movl	$308, 160(%rsp)
	movq	$0, 88(%rsp)
	movl	$0, 256(%rsp)
	orq	%rax, %rdx
	leaq	384(%rsp), %r10
	leaq	336(%rsp), %rdi
	leaq	_ZZ13my_dou_to_sciILi0ELi16EEidPcE11short_array(%rip), %r14
	movq	%rdx, 272(%rsp)
	movl	$0, 268(%rsp)
	movl	$0, 264(%rsp)
	movl	$0, 260(%rsp)
	movl	$0, 252(%rsp)
	movl	$0, 228(%rsp)
	vmovq	.LC13(%rip), %xmm9
	vmovsd	.LC176(%rip), %xmm8
	movq	%r10, 120(%rsp)
	.p2align 4
	.p2align 3
.L5978:
	cmpq	$312, %r15
	je	.L5920
	movq	3008(%rsp,%r15,8), %rcx
	incq	%r15
.L5921:
	movq	%rcx, %rsi
	movabsq	$6148914691236517205, %rbp
	movabsq	$8202884508482404352, %rdx
	shrq	$29, %rsi
	andq	%rbp, %rsi
	movabsq	$-2270628950310912, %rax
	xorq	%rcx, %rsi
	movq	%rsi, %r8
	salq	$17, %r8
	andq	%rdx, %r8
	xorq	%r8, %rsi
	movq	%rsi, %r10
	salq	$37, %r10
	andq	%rax, %r10
	xorq	%r10, %rsi
	movq	%rsi, %r12
	shrq	$43, %r12
	xorq	%r12, %rsi
	movq	%rsi, %r12
	shrq	$52, %r12
	movl	%r12d, %r13d
	andl	$2047, %r13d
	je	.L5978
	cmpl	$2047, %r13d
	je	.L5978
	vmovq	%rsi, %xmm7
	vxorpd	%xmm0, %xmm0, %xmm0
	movabsq	$4503599627370495, %r10
	movb	$45, 336(%rsp)
	vandpd	%xmm9, %xmm7, %xmm10
	vcomisd	%xmm7, %xmm0
	seta	%r9b
	seta	%r11b
	vmovq	%xmm10, %rbp
	movzbl	%r9b, %ecx
	vmovq	%xmm10, 168(%rsp)
	movq	%rbp, %rax
	andq	%rbp, %r10
	shrq	$52, %rax
	movq	%r10, 152(%rsp)
	andl	$2047, %eax
	movl	%eax, 180(%rsp)
	je	.L5984
	movabsq	$4503599627370496, %rdx
	leal	-1075(%rax), %r8d
	subl	$1023, %eax
	movl	$11, 148(%rsp)
	addq	%r10, %rdx
	movl	%eax, 248(%rsp)
	cmpl	$-1023, %eax
	je	.L5925
.L5926:
	movl	148(%rsp), %r9d
	vxorpd	%xmm11, %xmm11, %xmm11
	movl	%r9d, %ebp
	subl	%r9d, %r8d
	vucomisd	%xmm11, %xmm7
	shlx	%rbp, %rdx, %rdx
	jp	.L5999
	je	.L5986
.L5999:
	imull	$78913, %eax, %r10d
	leaq	_ZL5_10en(%rip), %rbp
	sarl	$18, %r10d
	leal	325(%r10), %r13d
	movslq	%r13d, %r9
	movl	$0, %r13d
	vcomisd	0(%rbp,%r9,8), %xmm10
	sbbl	$-1, %r10d
	xorl	%eax, %eax
	vucomisd	.LC175(%rip), %xmm10
	setnp	%al
	movl	$16, %r9d
	cmovne	%r13d, %eax
	leaq	_ZL10powers_ten(%rip), %r13
	subl	%eax, %r10d
	movl	$359, %eax
	subl	%r10d, %r9d
	subl	%r10d, %eax
	movl	%r10d, 132(%rsp)
	imull	$1741647, %r9d, %ebp
	cltq
	sarl	$19, %ebp
	subl	$63, %ebp
	movl	%ebp, 144(%rsp)
	movq	0(%r13,%rax,8), %rbp
	movl	%r10d, %r13d
	movq	%rbp, %r9
	movl	%ebp, %eax
	shrq	$32, %r9
	movq	%rax, 104(%rsp)
	negl	%r13d
	movq	%r9, %rax
	movq	%r9, 96(%rsp)
	cmovs	%r10d, %r13d
	movl	%r13d, 128(%rsp)
.L5927:
	movq	104(%rsp), %rbp
	movq	%rdx, %r9
	movl	144(%rsp), %r10d
	movl	%edx, %edx
	shrq	$32, %r9
	imulq	%rdx, %rax
	imulq	%r9, %rbp
	addl	%r10d, %r8d
	movl	%eax, %r13d
	movl	%ebp, %r10d
	addq	%r10, %r13
	movq	104(%rsp), %r10
	shrq	$32, %rbp
	shrq	$32, %rax
	imulq	%r10, %rdx
	addq	%rbp, %rax
	movl	$-64, %ebp
	subl	%r8d, %ebp
	addq	%r13, %rdx
	movq	96(%rsp), %r13
	shrq	$63, %rdx
	imulq	%r13, %r9
	addq	%rdx, %r9
	addq	%r9, %rax
	movl	$-65, %r9d
	subl	%r8d, %r9d
	movabsq	$1000000000000000, %r8
	shrx	%r9, %rax, %rdx
	andl	$1, %edx
	shrx	%rbp, %rax, %rax
	addq	%rdx, %rax
	vcvtsi2sdq	%rax, %xmm6, %xmm12
	vmulsd	%xmm8, %xmm12, %xmm13
	vcvttsd2siq	%xmm13, %r13
	movq	%r13, %r10
	imulq	%r8, %r10
	subq	%r10, %rax
	jns	.L5929
	decq	%r13
	addq	%r8, %rax
.L5929:
	vcvtsi2sdq	%rax, %xmm6, %xmm3
	vbroadcastsd	%xmm3, %zmm4
	vmulpd	.LC95(%rip), %zmm4, %zmm5
	vpbroadcastq	%rax, %zmm0
	imulq	$103, %r13, %rdx
	movzbl	%r11b, %r11d
	leal	2(%rcx), %r10d
	leaq	(%rdi,%r11), %rbp
	leaq	1(%rdi,%r11), %r8
	movslq	%r10d, %r11
	vmovq	%rbp, %xmm14
	vmovq	%r8, %xmm15
	sarq	$10, %rdx
	leal	(%rdx,%rdx,4), %eax
	leal	48(%rdx), %r9d
	addl	%eax, %eax
	movb	%r9b, 0(%rbp)
	leaq	(%rdi,%r11), %rbp
	movb	$46, (%r8)
	subl	%eax, %r13d
	movq	%rbp, 80(%rsp)
	vcvttpd2qq	%zmm5, %zmm2
	vpmullq	.LC96(%rip), %zmm2, %zmm1
	leal	0(%r13,%r13,4), %r13d
	vpsubq	%zmm1, %zmm0, %zmm11
	vcvtqq2pd	%zmm11, %zmm12
	vmulpd	.LC97(%rip), %zmm12, %zmm13
	vcvttpd2qq	%zmm13, %zmm5
	vmovq	%xmm5, %rdx
	vextracti64x4	$0x1, %zmm5, %ymm11
	vpextrq	$1, %xmm5, %r11
	leal	(%rdx,%r13,2), %r9d
	vmovq	%xmm11, %r8
	vpextrq	$1, %xmm11, %rax
	vextracti64x2	$1, %ymm5, %xmm2
	vextracti64x2	$1, %ymm11, %xmm12
	vmovq	%xmm2, %r13
	movslq	%r9d, %r10
	vmovq	%xmm12, %rbp
	valignq	$3, %ymm5, %ymm5, %ymm0
	movzwl	(%r14,%r10,2), %edx
	movzwl	(%r14,%r13,2), %r10d
	vmovq	%xmm0, %r9
	movzwl	(%r14,%rbp,2), %r13d
	valignq	$3, %ymm11, %ymm11, %ymm13
	vmovd	%edx, %xmm5
	vmovd	%r10d, %xmm4
	vmovd	%r13d, %xmm1
	vpinsrw	$1, (%r14,%r11,2), %xmm5, %xmm0
	movzwl	(%r14,%r8,2), %r11d
	vpinsrw	$1, (%r14,%r9,2), %xmm4, %xmm11
	vmovq	%xmm13, %r9
	movl	132(%rsp), %r10d
	vpinsrw	$1, (%r14,%r9,2), %xmm1, %xmm12
	movq	80(%rsp), %r8
	movslq	128(%rsp), %r13
	vmovd	%r11d, %xmm3
	vpinsrw	$1, (%r14,%rax,2), %xmm3, %xmm2
	leal	18(%rcx), %eax
	sarl	$31, %r10d
	vpunpckldq	%xmm11, %xmm0, %xmm13
	movslq	%eax, %rbp
	andw	$512, %r10w
	leaq	(%rdi,%rbp), %rdx
	addw	$11109, %r10w
	leaq	2(%rdi,%rbp), %r11
	movq	%rdx, 184(%rsp)
	movw	%r10w, 226(%rsp)
	vpunpckldq	%xmm12, %xmm2, %xmm5
	vmovq	%r11, %xmm12
	vpunpcklqdq	%xmm5, %xmm13, %xmm0
	vmovq	%rbp, %xmm13
	vmovdqu16	%xmm0, (%r8)
	movw	%r10w, (%rdx)
	cmpl	$99, %r13d
	jg	.L5931
	movzwl	(%r14,%r13,2), %r13d
	addl	$22, %ecx
	movw	%r13w, (%r11)
.L5932:
	movq	120(%rsp), %r9
	movslq	%ecx, %rcx
	leaq	(%rdi,%rcx), %r11
	movq	%rdi, %rcx
	movq	%r11, 192(%rsp)
	movb	$0, (%r11)
	movq	%r9, 368(%rsp)
	vzeroupper
	call	strlen
	movq	%rax, %rbp
	cmpq	$15, %rax
	ja	.L6089
	cmpq	$1, %rax
	je	.L6090
	testq	%rax, %rax
	jne	.L6091
.L5937:
	movq	120(%rsp), %r8
.L5936:
	leaq	.LC177(%rip), %rdx
	leaq	464(%rsp), %r9
	leaq	400(%rsp), %rcx
	movq	%rbp, 376(%rsp)
	movb	$0, (%r8,%rbp)
	leaq	288(%rsp), %r8
	movq	%rdx, 312(%rsp)
	leaq	304(%rsp), %rdx
	vmovsd	%xmm7, 464(%rsp)
	movq	%r9, 136(%rsp)
	movq	%rcx, 232(%rsp)
	movq	$7, 304(%rsp)
	movq	$129, 288(%rsp)
	movq	%r9, 296(%rsp)
	movq	%r8, 200(%rsp)
	movq	%rdx, 208(%rsp)
.LEHB95:
	call	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
.LEHE95:
	movq	408(%rsp), %r8
	movq	368(%rsp), %r13
	cmpq	376(%rsp), %r8
	je	.L6092
.L5944:
	incl	228(%rsp)
	vxorpd	%xmm3, %xmm3, %xmm3
	vucomisd	%xmm3, %xmm7
	jp	.L6000
	movl	$0, %ebp
	je	.L5947
.L6000:
	andl	$2047, %r12d
	vcomisd	.LC15(%rip), %xmm10
	leal	-1023(%r12), %ebp
	leaq	_ZL5_10en(%rip), %r12
	vcvtsi2sdl	%ebp, %xmm6, %xmm2
	vmulsd	.LC14(%rip), %xmm2, %xmm1
	vcvttsd2sil	%xmm1, %ebp
	sbbl	$-1, %ebp
	leal	324(%rbp), %eax
	xorl	%ecx, %ecx
	cltq
	vmovsd	(%r12,%rax,8), %xmm5
	vcomisd	%xmm10, %xmm5
	seta	%cl
	subl	%ecx, %ebp
.L5947:
	movl	160(%rsp), %r11d
	movl	164(%rsp), %r10d
	movq	__imp__errno(%rip), %r12
	cmpl	%ebp, %r11d
	cmovg	%ebp, %r11d
	cmpl	%ebp, %r10d
	cmovl	%ebp, %r10d
	movl	%r11d, 160(%rsp)
	movl	%r10d, 164(%rsp)
.LEHB96:
	call	*%r12
	movl	(%rax), %r8d
	vmovd	%r8d, %xmm11
	movl	%r8d, 432(%rsp)
	call	*%r12
.LEHE96:
	movq	136(%rsp), %rdx
	movq	%r13, %rcx
	movl	$0, (%rax)
.LEHB97:
	call	__mingw_strtod
.LEHE97:
	vmovsd	%xmm0, 216(%rsp)
	cmpq	%r13, 464(%rsp)
	je	.L6093
.LEHB98:
	call	*%r12
.LEHE98:
	cmpl	$34, (%rax)
	je	.L6094
	call	*%r12
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	je	.L6095
.L5951:
	movq	400(%rsp), %r13
	vmovq	%r13, %xmm11
.LEHB99:
	call	*%r12
	vmovd	(%rax), %xmm10
	vmovd	%xmm10, 432(%rsp)
	call	*%r12
.LEHE99:
	movq	136(%rsp), %rdx
	movq	%r13, %rcx
	movl	$0, (%rax)
.LEHB100:
	call	__mingw_strtod
.LEHE100:
	vmovq	%xmm0, %r13
	vmovq	%xmm11, %rax
	cmpq	464(%rsp), %rax
	je	.L6096
.LEHB101:
	call	*%r12
.LEHE101:
	cmpl	$34, (%rax)
	je	.L6097
	call	*%r12
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L6098
.L5957:
	movq	216(%rsp), %r10
	subq	%rsi, %r13
	movl	176(%rsp), %r9d
	movq	%r13, %r11
	negq	%r11
	cmovs	%r13, %r11
	movl	%r11d, %r12d
	subq	%rsi, %r10
	movq	%r10, %rsi
	negq	%rsi
	cmovns	%rsi, %r10
	subl	%r10d, %r12d
	movl	%r12d, %r8d
	negl	%r8d
	cmovns	%r8d, %r12d
	cmpl	%r12d, %r9d
	cmovl	%r12d, %r9d
	movl	%r9d, 176(%rsp)
	cmpl	%r10d, %r11d
	je	.L6099
	cmpl	$1, %r12d
	je	.L6100
	cmpl	$2, %r12d
	je	.L6101
	cmpl	$3, %r12d
	je	.L6102
	incl	256(%rsp)
.L5962:
	leaq	.LC179(%rip), %rcx
.LEHB102:
	call	_Z6printfPKcz
	vmovapd	%xmm7, %xmm0
	call	_Z12print_doubled
	leaq	432(%rsp), %r13
	vmovq	%xmm7, %r8
	leaq	.LC180(%rip), %rdx
	vmovapd	%xmm7, %xmm2
	movq	%r13, %rcx
	call	_Z7sprintfPcPKcz
	leaq	320(%rsp), %rdx
	movq	200(%rsp), %r8
	movq	136(%rsp), %rcx
	movq	%rdx, 296(%rsp)
	movq	208(%rsp), %rdx
	leaq	.LC177(%rip), %rax
	vmovsd	%xmm7, 320(%rsp)
	movq	$7, 304(%rsp)
	movq	%rax, 312(%rsp)
	movq	$129, 288(%rsp)
	call	_ZSt7vformatB5cxx11St17basic_string_viewIcSt11char_traitsIcEESt17basic_format_argsISt20basic_format_contextINSt8__format10_Sink_iterIcEEcEE
.LEHE102:
	movq	88(%rsp), %rcx
	movq	368(%rsp), %r11
	movq	400(%rsp), %r10
	movq	464(%rsp), %rsi
	testq	%rcx, %rcx
	je	.L5989
	blsi	%rcx, %r12
	je	.L5990
	xorl	%r9d, %r9d
	.p2align 4
	.p2align 4
	.p2align 3
.L5966:
	movl	%r9d, %r8d
	incl	%r9d
	sarq	%r12
	jne	.L5966
	addl	$33, %r8d
.L5965:
	movq	88(%rsp), %r9
	movl	%r8d, 32(%rsp)
	movl	%ebp, %edx
	xorl	%r8d, %r8d
	leaq	.LC181(%rip), %rcx
	movq	%r11, 64(%rsp)
	movq	%r13, 56(%rsp)
	movq	%r10, 48(%rsp)
	movq	%rsi, 40(%rsp)
.LEHB103:
	call	_Z6printfPKcz
	vmovapd	%xmm7, %xmm0
	call	_Z12print_doubled
	movl	180(%rsp), %ebp
	movb	$45, 336(%rsp)
	testl	%ebp, %ebp
	je	.L5991
	subl	$1075, %ebp
	movabsq	$4503599627370496, %rdx
	addq	%rdx, 152(%rsp)
	cmpl	$-1023, 248(%rsp)
	movl	%ebp, %r13d
	je	.L5967
.L5968:
	movl	148(%rsp), %edx
	leaq	.LC184(%rip), %r12
	movl	%edx, %ecx
	subl	%edx, %r13d
	shlx	%rcx, 152(%rsp), %rbp
	xorl	%edx, %edx
	movq	%r12, %rcx
	call	_Z6printfPKcz
	movl	132(%rsp), %edx
	movq	%r12, %rcx
	call	_Z6printfPKcz
	movq	96(%rsp), %r11
	movq	104(%rsp), %r10
	movq	%rbp, %rcx
	movl	%ebp, %esi
	movl	144(%rsp), %r9d
	shrq	$32, %rcx
	movq	%r11, %r8
	movq	%r10, %rdx
	imulq	%rsi, %r8
	leal	0(%r13,%r9), %r12d
	leal	64(%r13,%r9), %r13d
	imulq	%rcx, %rdx
	imulq	%r10, %rsi
	movl	%r8d, %eax
	imulq	%r11, %rcx
	movl	%edx, %ebp
	addq	%rbp, %rax
	addq	%rsi, %rax
	shrq	$63, %rax
	shrq	$32, %rdx
	shrq	$32, %r8
	addq	%rax, %rcx
	addq	%r8, %rdx
	leaq	(%rcx,%rdx), %rsi
	leaq	.LC185(%rip), %rcx
	movq	%rsi, %rdx
	call	_Z6printfPKcz
	movl	%r13d, %edx
	leaq	.LC186(%rip), %rcx
	call	_Z6printfPKcz
	notl	%r13d
	leaq	.LC187(%rip), %rcx
	shrx	%r13, %rsi, %rbp
	movl	$-64, %r13d
	andl	$1, %ebp
	subl	%r12d, %r13d
	shrx	%r13, %rsi, %r12
	movq	%r12, %rdx
	call	_Z6printfPKcz
	leaq	.LC188(%rip), %rcx
	bzhi	%r13, %rsi, %rdx
	call	_Z6printfPKcz
	movq	%rbp, %rdx
	leaq	.LC189(%rip), %rcx
	call	_Z6printfPKcz
	movl	$-1023, %edx
	leaq	.LC190(%rip), %rcx
	addq	%r12, %rbp
	call	_Z6printfPKcz
	movq	%rbp, %rdx
	leaq	.LC191(%rip), %rcx
	call	_Z6printfPKcz
	movabsq	$1000000000000000, %rcx
	vcvtsi2sdq	%rbp, %xmm6, %xmm0
	vmulsd	%xmm8, %xmm0, %xmm4
	vcvttsd2siq	%xmm4, %r13
	movq	%r13, %r11
	movq	%rbp, %rsi
	imulq	%rcx, %r11
	subq	%r11, %rsi
	jns	.L5969
	decq	%r13
	addq	%rcx, %rsi
.L5969:
	imulq	$103, %r13, %r12
	leaq	.LC192(%rip), %rcx
	sarq	$10, %r12
	leal	(%r12,%r12,4), %r8d
	movl	%r12d, %edx
	movl	%r12d, 96(%rsp)
	addl	%r8d, %r8d
	subl	%r8d, %r13d
	call	_Z6printfPKcz
	movl	%r13d, %edx
	leaq	.LC193(%rip), %rcx
	call	_Z6printfPKcz
	movq	%rsi, %rdx
	leaq	.LC194(%rip), %rcx
	call	_Z6printfPKcz
	vmovq	%xmm14, %r10
	vmovq	%xmm15, %rdx
	vcvtsi2sdq	%rsi, %xmm6, %xmm7
	vbroadcastsd	%xmm7, %zmm14
	vmulpd	.LC95(%rip), %zmm14, %zmm15
	vpbroadcastq	%rsi, %zmm2
	movq	112(%rsp), %r9
	leal	0(%r13,%r13,4), %eax
	addl	$48, %r12d
	leaq	.LC195(%rip), %rcx
	movb	%r12b, (%r10)
	movb	$46, (%rdx)
	xorl	%edx, %edx
	vcvttpd2qq	%zmm15, %zmm3
	vpmullq	.LC96(%rip), %zmm3, %zmm1
	vpsubq	%zmm1, %zmm2, %zmm5
	vcvtqq2pd	%zmm5, %zmm11
	vmulpd	.LC97(%rip), %zmm11, %zmm10
	vcvttpd2qq	%zmm10, %zmm0
	vmovdqa64	%zmm0, (%r9)
	vmovq	%xmm0, %r8
	leal	(%r8,%rax,2), %r13d
	movl	%r13d, %r9d
	vzeroupper
	call	_Z6printfPKcz
	movslq	%r13d, %rcx
	leaq	_ZZ13my_dou_to_sciILi1ELi16EEidPcE11short_array(%rip), %r12
	movq	80(%rsp), %rsi
	leaq	.LC196(%rip), %r13
	movzwl	(%r12,%rcx,2), %r11d
	movw	%r11w, (%rsi)
	movl	$1, %esi
	.p2align 4
	.p2align 3
.L5970:
	movq	112(%rsp), %r8
	movq	80(%rsp), %rdx
	movq	%r13, %rcx
	movq	(%r8,%rsi,8), %r8
	movzwl	(%r12,%r8,2), %r10d
	movw	%r10w, (%rdx,%rsi,2)
	movl	%esi, %edx
	call	_Z6printfPKcz
	incq	%rsi
	cmpq	$8, %rsi
	jne	.L5970
	movq	184(%rsp), %r9
	movzwl	226(%rsp), %eax
	movslq	128(%rsp), %rcx
	movw	%ax, (%r9)
	cmpl	$99, %ecx
	jg	.L5971
	movzwl	(%r12,%rcx,2), %edx
	vmovq	%xmm12, %r9
	movw	%dx, (%r9)
.L5972:
	movl	132(%rsp), %r9d
	movl	96(%rsp), %edx
	movq	%rbp, %r8
	leaq	.LC197(%rip), %rcx
	call	_Z6printfPKcz
.LEHE103:
	movq	192(%rsp), %rbp
	movq	464(%rsp), %rcx
	leaq	480(%rsp), %r11
	movb	$0, 0(%rbp)
	cmpq	%r11, %rcx
	je	.L5973
	movq	480(%rsp), %rsi
	leaq	1(%rsi), %rdx
	call	_ZdlPvy
.L5973:
	movq	400(%rsp), %rbp
.L5946:
	leaq	416(%rsp), %rcx
	cmpq	%rcx, %rbp
	je	.L6086
.L5974:
	movq	416(%rsp), %r13
	movq	%rbp, %rcx
	leaq	1(%r13), %rdx
	call	_ZdlPvy
.L6086:
	movq	368(%rsp), %r13
.L5975:
	movq	120(%rsp), %rax
	cmpq	%rax, %r13
	je	.L5976
	movq	384(%rsp), %r12
	movq	%r13, %rcx
	leaq	1(%r12), %rdx
	call	_ZdlPvy
.L5976:
	incq	88(%rsp)
	movq	88(%rsp), %r10
	cmpq	$1048576, %r10
	jne	.L5978
/APP
 # 17 "main.cpp" 1
	rdtsc
 # 0 "" 2
/NO_APP
	movq	%rdx, %rbx
	movq	%rax, %r14
	call	_ZNSt6chrono3_V212system_clock3nowEv
	movq	280(%rsp), %r9
	movq	272(%rsp), %r15
	vmovsd	.LC198(%rip), %xmm9
	leaq	.LC199(%rip), %rcx
	salq	$32, %rbx
	subq	%r9, %rax
	orq	%r14, %rbx
	vcvtsi2sdq	%rax, %xmm6, %xmm8
	vdivsd	%xmm9, %xmm8, %xmm12
	vdivsd	.LC72(%rip), %xmm8, %xmm1
	vmovq	%xmm12, %r8
	vmovapd	%xmm12, %xmm2
	vmovq	%xmm1, %rdx
	subq	%r15, %rbx
.LEHB104:
	call	_Z6printfPKcz
	vcvtsi2sdq	%rbx, %xmm6, %xmm13
	movq	%rbx, %rdx
	vdivsd	%xmm9, %xmm13, %xmm14
	leaq	.LC200(%rip), %rcx
	vmovq	%xmm14, %r8
	vmovapd	%xmm14, %xmm2
	call	_Z6printfPKcz
	movq	%rdi, %rdx
	leaq	.LC70(%rip), %rcx
	call	_Z6printfPKcz
	movl	176(%rsp), %edx
	leaq	.LC201(%rip), %rcx
	call	_Z6printfPKcz
	movl	252(%rsp), %edx
	vmovsd	.LC202(%rip), %xmm11
	leaq	.LC203(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm15
	vmulsd	%xmm11, %xmm15, %xmm3
	vmovq	%xmm3, %r8
	vmovapd	%xmm3, %xmm2
	call	_Z6printfPKcz
	movl	260(%rsp), %edx
	leaq	.LC204(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm2
	vmulsd	%xmm11, %xmm2, %xmm1
	vmovq	%xmm1, %r8
	vmovapd	%xmm1, %xmm2
	call	_Z6printfPKcz
	movl	264(%rsp), %edx
	leaq	.LC205(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm5
	vmulsd	%xmm11, %xmm5, %xmm10
	vmovq	%xmm10, %r8
	vmovapd	%xmm10, %xmm2
	call	_Z6printfPKcz
	movl	268(%rsp), %edx
	leaq	.LC206(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm0
	vmulsd	%xmm11, %xmm0, %xmm4
	vmovq	%xmm4, %r8
	vmovapd	%xmm4, %xmm2
	call	_Z6printfPKcz
	movl	256(%rsp), %edx
	leaq	.LC207(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm7
	vmulsd	%xmm11, %xmm7, %xmm8
	vmovq	%xmm8, %r8
	vmovapd	%xmm8, %xmm2
	call	_Z6printfPKcz
	movl	228(%rsp), %edx
	leaq	.LC208(%rip), %rcx
	vcvtsi2sdl	%edx, %xmm6, %xmm6
	vmulsd	%xmm11, %xmm6, %xmm9
	vmovq	%xmm9, %r8
	vmovapd	%xmm9, %xmm2
	call	_Z6printfPKcz
	movl	164(%rsp), %r8d
	movl	160(%rsp), %edx
	leaq	.LC209(%rip), %rcx
	call	_Z6printfPKcz
	leaq	.LC179(%rip), %rcx
	call	_Z6printfPKcz
.LEHE104:
	movq	240(%rsp), %rcx
	call	_ZNSt13random_device7_M_finiEv
	nop
	vmovaps	5632(%rsp), %xmm6
	vmovaps	5648(%rsp), %xmm7
	vmovaps	5664(%rsp), %xmm8
	vmovaps	5680(%rsp), %xmm9
	vmovaps	5696(%rsp), %xmm10
	vmovaps	5712(%rsp), %xmm11
	vmovaps	5728(%rsp), %xmm12
	vmovaps	5744(%rsp), %xmm13
	vmovaps	5760(%rsp), %xmm14
	vmovaps	5776(%rsp), %xmm15
	addq	$5800, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4
	.p2align 3
.L5920:
	movq	$-2147483648, %r13
	vmovdqu64	1248(%rbx), %zmm12
	movl	$2147483647, %r12d
	movl	$1, %r9d
	vpbroadcastq	%r13, %zmm4
	vpandq	(%rbx), %zmm4, %zmm5
	movabsq	$-5403634167711393303, %rcx
	leaq	4224(%rsp), %r15
	leaq	64(%rbx), %r11
	vpbroadcastq	%r12, %zmm3
	vpbroadcastq	%r9, %zmm2
	vpbroadcastq	%rcx, %zmm1
	vpternlogq	$248, 8(%rbx), %zmm3, %zmm5
	vpandq	%zmm2, %zmm5, %zmm10
	vpsrlq	$1, %zmm5, %zmm7
	vpmullq	%zmm1, %zmm10, %zmm11
	vpternlogq	$150, %zmm7, %zmm12, %zmm11
	vmovdqu64	%zmm11, (%rbx)
.L5922:
	vpandq	(%r11), %zmm4, %zmm13
	vmovdqu64	1248(%r11), %zmm5
	vpandq	64(%r11), %zmm4, %zmm10
	addq	$384, %r11
	vpternlogq	$248, -376(%r11), %zmm3, %zmm13
	vpternlogq	$248, -312(%r11), %zmm3, %zmm10
	vpandq	%zmm2, %zmm13, %zmm15
	vpsrlq	$1, %zmm13, %zmm14
	vmovdqu64	928(%r11), %zmm13
	vpandq	%zmm2, %zmm10, %zmm11
	vpmullq	%zmm1, %zmm15, %zmm0
	vpsrlq	$1, %zmm10, %zmm7
	vpmullq	%zmm1, %zmm11, %zmm12
	vpandq	-192(%r11), %zmm4, %zmm11
	vpternlogq	$150, %zmm14, %zmm5, %zmm0
	vpandq	-256(%r11), %zmm4, %zmm14
	vmovdqu64	992(%r11), %zmm5
	vmovdqu64	%zmm0, -384(%r11)
	vpternlogq	$248, -184(%r11), %zmm3, %zmm11
	vpternlogq	$150, %zmm7, %zmm13, %zmm12
	vpsrlq	$1, %zmm11, %zmm7
	vpternlogq	$248, -248(%r11), %zmm3, %zmm14
	vmovdqu64	%zmm12, -320(%r11)
	vpandq	%zmm2, %zmm11, %zmm12
	vpandq	%zmm2, %zmm14, %zmm0
	vpsrlq	$1, %zmm14, %zmm15
	vmovdqu64	1056(%r11), %zmm14
	vpmullq	%zmm1, %zmm0, %zmm10
	vpmullq	%zmm1, %zmm12, %zmm13
	vpandq	-64(%r11), %zmm4, %zmm12
	vpternlogq	$150, %zmm15, %zmm5, %zmm10
	vpandq	-128(%r11), %zmm4, %zmm15
	vmovdqu64	1120(%r11), %zmm5
	vmovdqu64	%zmm10, -256(%r11)
	vpternlogq	$248, -56(%r11), %zmm3, %zmm12
	vpternlogq	$150, %zmm7, %zmm14, %zmm13
	vpsrlq	$1, %zmm12, %zmm7
	vpternlogq	$248, -120(%r11), %zmm3, %zmm15
	vmovdqu64	%zmm13, -192(%r11)
	vpandq	%zmm2, %zmm12, %zmm13
	vpandq	%zmm2, %zmm15, %zmm0
	vpsrlq	$1, %zmm15, %zmm10
	vmovdqu64	1184(%r11), %zmm15
	vpmullq	%zmm1, %zmm0, %zmm11
	vpmullq	%zmm1, %zmm13, %zmm14
	vpternlogq	$150, %zmm10, %zmm5, %zmm11
	vmovdqu64	%zmm11, -128(%r11)
	vpternlogq	$150, %zmm7, %zmm15, %zmm14
	vmovdqu64	%zmm14, -64(%r11)
	cmpq	%r11, %r15
	jne	.L5922
	vpbroadcastq	%r13, %ymm4
	vpandq	4224(%rsp), %ymm4, %ymm10
	movq	$-2147483648, %rdx
	vpbroadcastq	%r12, %ymm3
	vpbroadcastq	%rdx, %zmm14
	vpandq	4256(%rsp), %zmm14, %zmm4
	vpbroadcastq	%r9, %ymm1
	movl	$2147483647, %ebp
	vmovdqu	5472(%rsp), %ymm12
	vpbroadcastq	%rbp, %zmm13
	movl	$1, %r8d
	movabsq	$-5403634167711393303, %r10
	leaq	2464(%rbx), %rsi
	leaq	4320(%rsp), %rax
	vpbroadcastq	%r8, %zmm15
	vpbroadcastq	%rcx, %ymm11
	vpternlogq	$248, 4232(%rsp), %ymm3, %ymm10
	vpbroadcastq	%r10, %zmm3
	vpandq	%ymm1, %ymm10, %ymm0
	vmovdqu64	3008(%rsp), %zmm1
	vpsrlq	$1, %ymm10, %ymm2
	vpmullq	%ymm11, %ymm0, %ymm5
	vpternlogq	$248, 4264(%rsp), %zmm13, %zmm4
	vpandq	%zmm15, %zmm4, %zmm10
	vpsrlq	$1, %zmm4, %zmm7
	vpternlogq	$150, %ymm2, %ymm12, %ymm5
	vpmullq	%zmm3, %zmm10, %zmm2
	vmovdqu	%ymm5, 4224(%rsp)
	vpternlogq	$150, %zmm7, %zmm1, %zmm2
	vmovdqu64	%zmm2, 4256(%rsp)
.L5923:
	vpandq	(%rax), %zmm14, %zmm0
	vmovdqu64	-1248(%rax), %zmm4
	vpandq	64(%rax), %zmm14, %zmm10
	addq	$384, %rax
	vpternlogq	$248, -376(%rax), %zmm13, %zmm0
	vpternlogq	$248, -312(%rax), %zmm13, %zmm10
	vpandq	%zmm15, %zmm0, %zmm5
	vpsrlq	$1, %zmm0, %zmm11
	vpandq	%zmm15, %zmm10, %zmm2
	vpsrlq	$1, %zmm10, %zmm7
	vpmullq	%zmm3, %zmm5, %zmm12
	vmovdqu64	-1504(%rax), %zmm10
	vmovdqu64	-1568(%rax), %zmm0
	vpmullq	%zmm3, %zmm2, %zmm1
	vpandq	-192(%rax), %zmm14, %zmm2
	vpternlogq	$150, %zmm11, %zmm4, %zmm12
	vpandq	-256(%rax), %zmm14, %zmm11
	vmovdqu64	%zmm12, -384(%rax)
	vpternlogq	$248, -184(%rax), %zmm13, %zmm2
	vpternlogq	$248, -248(%rax), %zmm13, %zmm11
	vpternlogq	$150, %zmm7, %zmm0, %zmm1
	vpsrlq	$1, %zmm2, %zmm7
	vpsrlq	$1, %zmm11, %zmm12
	vmovdqu64	%zmm1, -320(%rax)
	vpandq	%zmm15, %zmm11, %zmm5
	vmovdqu64	-1440(%rax), %zmm11
	vpmullq	%zmm3, %zmm5, %zmm4
	vpandq	%zmm15, %zmm2, %zmm1
	vmovdqu64	-1376(%rax), %zmm2
	vpmullq	%zmm3, %zmm1, %zmm0
	vpandq	-64(%rax), %zmm14, %zmm1
	vpternlogq	$150, %zmm12, %zmm10, %zmm4
	vpandq	-128(%rax), %zmm14, %zmm12
	vmovdqu64	%zmm4, -256(%rax)
	vpternlogq	$150, %zmm7, %zmm11, %zmm0
	vpternlogq	$248, -56(%rax), %zmm13, %zmm1
	vmovdqu64	%zmm0, -192(%rax)
	vpternlogq	$248, -120(%rax), %zmm13, %zmm12
	vpsrlq	$1, %zmm1, %zmm7
	vpandq	%zmm15, %zmm12, %zmm5
	vpsrlq	$1, %zmm12, %zmm4
	vpandq	%zmm15, %zmm1, %zmm0
	vmovdqu64	-1312(%rax), %zmm12
	vpmullq	%zmm3, %zmm5, %zmm10
	vpmullq	%zmm3, %zmm0, %zmm11
	vpternlogq	$150, %zmm4, %zmm2, %zmm10
	vmovdqu64	%zmm10, -128(%rax)
	vpternlogq	$150, %zmm7, %zmm12, %zmm11
	vmovdqu64	%zmm11, -64(%rax)
	cmpq	%rsi, %rax
	jne	.L5923
	movq	5496(%rsp), %r15
	movq	3008(%rsp), %rcx
	movq	5488(%rsp), %r12
	vpbroadcastq	%rdx, %xmm14
	vpandq	5472(%rsp), %xmm14, %xmm15
	vmovdqa	4224(%rsp), %xmm1
	vpbroadcastq	%rbp, %xmm13
	vpbroadcastq	%r8, %xmm4
	vpbroadcastq	%r10, %xmm10
	movq	%r15, %r13
	movq	%rcx, %r11
	andq	$-2147483648, %r12
	andq	$-2147483648, %r15
	andl	$2147483647, %r13d
	andl	$2147483647, %r11d
	orq	%r13, %r12
	orq	%r11, %r15
	vpternlogq	$248, 5480(%rsp), %xmm13, %xmm15
	movq	%r12, %r9
	movq	%r15, %rsi
	vpandq	%xmm4, %xmm15, %xmm5
	andl	$1, %r12d
	shrq	%r9
	vpsrlq	$1, %xmm15, %xmm3
	vpmullq	%xmm10, %xmm5, %xmm2
	xorq	4240(%rsp), %r9
	andl	$1, %r15d
	negq	%r12
	negq	%r15
	andq	%r10, %r12
	shrq	%rsi
	vpternlogq	$150, %xmm3, %xmm1, %xmm2
	xorq	4248(%rsp), %rsi
	andq	%r10, %r15
	vmovdqa	%xmm2, 5472(%rsp)
	xorq	%r9, %r12
	movq	%r12, 5488(%rsp)
	xorq	%rsi, %r15
	movq	%r15, 5496(%rsp)
	movl	$1, %r15d
	jmp	.L5921
	.p2align 4
	.p2align 3
.L5984:
	movq	%r10, %rdx
	movl	$-1074, %r8d
.L5925:
	movl	$-1012, %eax
	lzcntq	168(%rsp), %r13
	movl	$-1023, 248(%rsp)
	movl	%r13d, 148(%rsp)
	subl	%r13d, %eax
	jmp	.L5926
	.p2align 4
	.p2align 3
.L5931:
	vcvtsi2sdl	%r13d, %xmm6, %xmm4
	vmulsd	.LC54(%rip), %xmm4, %xmm11
	vcvttsd2sil	%xmm11, %r8d
	leal	48(%r8), %eax
	movb	%al, (%r11)
	imull	$-100, %r8d, %eax
	addl	$23, %ecx
	addl	%r13d, %eax
	cltq
	movzwl	(%r14,%rax,2), %r10d
	movw	%r10w, 339(%rsp,%rbp)
	jmp	.L5932
	.p2align 4
	.p2align 3
.L5986:
	movl	$2384185791, %eax
	movl	$0, 128(%rsp)
	movq	$67108864, 104(%rsp)
	movl	$-10, 144(%rsp)
	movq	%rax, 96(%rsp)
	movl	$0, 132(%rsp)
	jmp	.L5927
.L6090:
	movzbl	336(%rsp), %r8d
	movb	%r8b, 384(%rsp)
	jmp	.L5937
	.p2align 4
	.p2align 3
.L6089:
	leaq	1(%rax), %rcx
.LEHB105:
	call	_Znwy
.LEHE105:
	movq	%rax, 368(%rsp)
	movq	%rbp, 384(%rsp)
.L5934:
	cmpl	$8, %ebp
	jb	.L6103
	movq	(%rdi), %r8
	movl	%ebp, %r10d
	movq	%rdi, %rdx
	movq	%r8, (%rax)
	movq	-8(%rdi,%r10), %r13
	leaq	8(%rax), %r8
	andq	$-8, %r8
	movq	%r13, -8(%rax,%r10)
	subq	%r8, %rax
	subq	%rax, %rdx
	addl	%ebp, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L5939
	movq	(%rdx), %r11
	leal	-1(%rax), %ecx
	movl	$8, %r13d
	movl	%eax, %r9d
	shrl	$3, %ecx
	andl	$7, %ecx
	movq	%r11, (%r8)
	cmpl	%eax, %r13d
	jnb	.L5939
	testl	%ecx, %ecx
	je	.L5942
	cmpl	$1, %ecx
	je	.L6062
	cmpl	$2, %ecx
	je	.L6063
	cmpl	$3, %ecx
	je	.L6064
	cmpl	$4, %ecx
	je	.L6065
	cmpl	$5, %ecx
	je	.L6066
	cmpl	$6, %ecx
	jne	.L6104
.L6067:
	movl	%r13d, %eax
	addl	$8, %r13d
	movq	(%rdx,%rax), %rcx
	movq	%rcx, (%r8,%rax)
.L6066:
	movl	%r13d, %r11d
	addl	$8, %r13d
	movq	(%rdx,%r11), %r10
	movq	%r10, (%r8,%r11)
.L6065:
	movl	%r13d, %eax
	addl	$8, %r13d
	movq	(%rdx,%rax), %rcx
	movq	%rcx, (%r8,%rax)
.L6064:
	movl	%r13d, %r11d
	addl	$8, %r13d
	movq	(%rdx,%r11), %r10
	movq	%r10, (%r8,%r11)
.L6063:
	movl	%r13d, %eax
	addl	$8, %r13d
	movq	(%rdx,%rax), %rcx
	movq	%rcx, (%r8,%rax)
.L6062:
	movl	%r13d, %r11d
	addl	$8, %r13d
	movq	(%rdx,%r11), %r10
	movq	%r10, (%r8,%r11)
	cmpl	%r9d, %r13d
	jnb	.L5939
.L5942:
	movl	%r13d, %eax
	leal	8(%r13), %r11d
	movq	(%rdx,%rax), %rcx
	movq	%rcx, (%r8,%rax)
	movq	(%rdx,%r11), %r10
	leal	16(%r13), %eax
	movq	%r10, (%r8,%r11)
	movq	(%rdx,%rax), %rcx
	leal	24(%r13), %r11d
	movq	%rcx, (%r8,%rax)
	movq	(%rdx,%r11), %r10
	leal	32(%r13), %eax
	movq	%r10, (%r8,%r11)
	movq	(%rdx,%rax), %rcx
	leal	40(%r13), %r11d
	movq	%rcx, (%r8,%rax)
	movq	(%rdx,%r11), %r10
	leal	48(%r13), %eax
	movq	%r10, (%r8,%r11)
	movq	(%rdx,%rax), %rcx
	leal	56(%r13), %r11d
	addl	$64, %r13d
	movq	%rcx, (%r8,%rax)
	movq	(%rdx,%r11), %r10
	movq	%r10, (%r8,%r11)
	cmpl	%r9d, %r13d
	jb	.L5942
	jmp	.L5939
	.p2align 4
	.p2align 3
.L6092:
	movq	400(%rsp), %rbp
	testq	%r8, %r8
	je	.L5945
	movq	%r13, %rdx
	movq	%rbp, %rcx
	call	memcmp
	testl	%eax, %eax
	je	.L5946
	jmp	.L5944
.L6099:
	movq	400(%rsp), %rbp
	incl	252(%rsp)
	jmp	.L5946
.L6098:
	call	*%r12
	vmovd	%xmm10, (%rax)
	jmp	.L5957
.L6095:
	call	*%r12
	vmovd	%xmm11, (%rax)
	jmp	.L5951
.L6103:
	testb	$4, %bpl
	jne	.L6105
	testl	%ebp, %ebp
	je	.L5939
	movzbl	(%rdi), %ecx
	movb	%cl, (%rax)
	testb	$2, %bpl
	jne	.L6106
.L5939:
	movq	368(%rsp), %r8
	jmp	.L5936
.L6102:
	incl	268(%rsp)
	jmp	.L5962
.L5991:
	movl	$-1074, %r13d
.L5967:
	lzcntq	168(%rsp), %rax
	movl	$-1012, %esi
	movl	%eax, %edx
	leaq	.LC182(%rip), %rcx
	subl	%eax, %esi
.LEHB106:
	call	_Z6printfPKcz
	movl	%esi, %edx
	leaq	.LC183(%rip), %rcx
	call	_Z6printfPKcz
.LEHE106:
	jmp	.L5968
.L5971:
	vcvtsi2sdl	%ecx, %xmm6, %xmm4
	vmulsd	.LC54(%rip), %xmm4, %xmm7
	vcvttsd2sil	%xmm7, %esi
	imull	$-100, %esi, %eax
	leal	48(%rsi), %r13d
	vmovq	%xmm12, %r8
	vmovq	%xmm13, %r10
	movb	%r13b, (%r8)
	addl	%ecx, %eax
	cltq
	movzwl	(%r12,%rax,2), %r12d
	movw	%r12w, 339(%rsp,%r10)
	jmp	.L5972
.L5914:
	movq	%rsi, %rcx
.LEHB107:
	call	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rsi), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %r8
	movl	$10, %edx
	movq	48(%rdi), %rbp
	cmpq	%r8, %rbp
	je	.L5915
	movq	%rsi, %rcx
	call	*%rbp
.LEHE107:
	movsbl	%al, %edx
	jmp	.L5915
.L6100:
	incl	260(%rsp)
	jmp	.L5962
.L6091:
	movq	120(%rsp), %rax
	jmp	.L5934
.L5945:
	leaq	416(%rsp), %r8
	cmpq	%r8, %rbp
	jne	.L5974
	jmp	.L5975
.L6101:
	incl	264(%rsp)
	jmp	.L5962
.L5990:
	movl	$32, %r8d
	jmp	.L5965
.L6104:
	movq	(%rdx,%r13), %r10
	movq	%r10, (%r8,%r13)
	movl	$16, %r13d
	jmp	.L6067
.L6105:
	movl	(%rdi), %r10d
	movl	%ebp, %r13d
	movl	%r10d, (%rax)
	movl	-4(%rdi,%r13), %edx
	movl	%edx, -4(%rax,%r13)
	jmp	.L5939
.L6106:
	movl	%ebp, %r11d
	movzwl	-2(%rdi,%r11), %r9d
	movw	%r9w, -2(%rax,%r11)
	jmp	.L5939
.L5989:
	movl	$31, %r8d
	jmp	.L5965
.L5993:
	movq	%rax, %rdi
	vzeroupper
.L5980:
	leaq	368(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
.L5981:
	movq	240(%rsp), %rcx
	call	_ZNSt13random_device7_M_finiEv
	movq	%rdi, %rcx
.LEHB108:
	call	_Unwind_Resume
.LEHE108:
.L6097:
	leaq	.LC178(%rip), %rcx
.LEHB109:
	call	_ZSt20__throw_out_of_rangePKc
.LEHE109:
.L5998:
.L6087:
	leaq	432(%rsp), %rcx
	movq	%rax, %rdi
	vzeroupper
	call	_ZZN9__gnu_cxx6__stoaIddcJEEET0_PFT_PKT1_PPS3_DpT2_EPKcS5_PyS9_EN11_Save_errnoD1Ev
.L5954:
	movq	232(%rsp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	jmp	.L5980
.L5996:
	movq	%rax, %r13
	movq	%rbx, %rcx
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r13, %rcx
.LEHB110:
	call	_Unwind_Resume
.LEHE110:
.L5992:
	movq	%rax, %rdi
	vzeroupper
	jmp	.L5981
.L6093:
	leaq	.LC178(%rip), %rcx
.LEHB111:
	call	_ZSt24__throw_invalid_argumentPKc
.LEHE111:
.L6088:
.LEHB112:
	call	_ZSt16__throw_bad_castv
.LEHE112:
.L5994:
	movq	%rax, %rdi
	vzeroupper
	jmp	.L5954
.L5995:
	movq	136(%rsp), %rcx
	movq	%rax, %rdi
	vzeroupper
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	jmp	.L5954
.L6096:
	leaq	.LC178(%rip), %rcx
.LEHB113:
	call	_ZSt24__throw_invalid_argumentPKc
.LEHE113:
.L6094:
	leaq	.LC178(%rip), %rcx
.LEHB114:
	call	_ZSt20__throw_out_of_rangePKc
.LEHE114:
.L5997:
	jmp	.L6087
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA13677:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE13677-.LLSDACSB13677
.LLSDACSB13677:
	.uleb128 .LEHB92-.LFB13677
	.uleb128 .LEHE92-.LEHB92
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB93-.LFB13677
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L5996-.LFB13677
	.uleb128 0
	.uleb128 .LEHB94-.LFB13677
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L5992-.LFB13677
	.uleb128 0
	.uleb128 .LEHB95-.LFB13677
	.uleb128 .LEHE95-.LEHB95
	.uleb128 .L5993-.LFB13677
	.uleb128 0
	.uleb128 .LEHB96-.LFB13677
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L5994-.LFB13677
	.uleb128 0
	.uleb128 .LEHB97-.LFB13677
	.uleb128 .LEHE97-.LEHB97
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB98-.LFB13677
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L5997-.LFB13677
	.uleb128 0
	.uleb128 .LEHB99-.LFB13677
	.uleb128 .LEHE99-.LEHB99
	.uleb128 .L5994-.LFB13677
	.uleb128 0
	.uleb128 .LEHB100-.LFB13677
	.uleb128 .LEHE100-.LEHB100
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB101-.LFB13677
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L5998-.LFB13677
	.uleb128 0
	.uleb128 .LEHB102-.LFB13677
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L5994-.LFB13677
	.uleb128 0
	.uleb128 .LEHB103-.LFB13677
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L5995-.LFB13677
	.uleb128 0
	.uleb128 .LEHB104-.LFB13677
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L5992-.LFB13677
	.uleb128 0
	.uleb128 .LEHB105-.LFB13677
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L5992-.LFB13677
	.uleb128 0
	.uleb128 .LEHB106-.LFB13677
	.uleb128 .LEHE106-.LEHB106
	.uleb128 .L5995-.LFB13677
	.uleb128 0
	.uleb128 .LEHB107-.LFB13677
	.uleb128 .LEHE107-.LEHB107
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB108-.LFB13677
	.uleb128 .LEHE108-.LEHB108
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB109-.LFB13677
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L5998-.LFB13677
	.uleb128 0
	.uleb128 .LEHB110-.LFB13677
	.uleb128 .LEHE110-.LEHB110
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB111-.LFB13677
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L5997-.LFB13677
	.uleb128 0
	.uleb128 .LEHB112-.LFB13677
	.uleb128 .LEHE112-.LEHB112
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB113-.LFB13677
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L5998-.LFB13677
	.uleb128 0
	.uleb128 .LEHB114-.LFB13677
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L5997-.LFB13677
	.uleb128 0
.LLSDACSE13677:
	.text
	.seh_endproc
	.section	.text$_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy,"x"
	.linkonce discard
	.align 2
	.p2align 4
	.globl	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy
	.def	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy;	.scl	2;	.type	32;	.endef
	.seh_proc	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy
_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy:
.LFB15251:
	pushq	%r13
	.seh_pushreg	%r13
	pushq	%r12
	.seh_pushreg	%r12
	pushq	%rbp
	.seh_pushreg	%rbp
	pushq	%rdi
	.seh_pushreg	%rdi
	pushq	%rsi
	.seh_pushreg	%rsi
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$344, %rsp
	.seh_stackalloc	344
	.seh_endprologue
	movq	48(%rcx), %rsi
	movq	%rcx, %rbx
	movzbl	(%rsi), %eax
	movl	%eax, %ecx
	andl	$15, %eax
	andl	$15, %ecx
	cmpq	%rax, %rdx
	jnb	.L6108
	leaq	(%rdx,%rdx,4), %r11
	movl	$4, %r9d
	shrx	%r9, (%rsi), %r10
	salq	$4, %rdx
	shrx	%r11, %r10, %r8
	addq	8(%rsi), %rdx
	andl	$31, %r8d
	vmovdqa	(%rdx), %xmm2
	vmovdqa	%xmm2, 160(%rsp)
.L6109:
	leaq	.L6113(%rip), %rdx
	movzbl	%r8b, %r12d
	movb	%r8b, 176(%rsp)
	vmovdqu	160(%rsp), %ymm0
	movslq	(%rdx,%r12,4), %r13
	addq	%rdx, %r13
	vmovdqu	%ymm0, 192(%rsp)
	jmp	*%r13
	.section .rdata,"dr"
	.align 4
.L6113:
	.long	.L6696-.L6113
	.long	.L6127-.L6113
	.long	.L6126-.L6113
	.long	.L6125-.L6113
	.long	.L6124-.L6113
	.long	.L6123-.L6113
	.long	.L6122-.L6113
	.long	.L6121-.L6113
	.long	.L6120-.L6113
	.long	.L6119-.L6113
	.long	.L6118-.L6113
	.long	.L6117-.L6113
	.long	.L6116-.L6113
	.long	.L6115-.L6113
	.long	.L6114-.L6113
	.long	.L6112-.L6113
	.section	.text$_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy,"x"
	.linkonce discard
	.p2align 4
	.p2align 3
.L6108:
	testb	%cl, %cl
	jne	.L6110
	movl	$4, %edi
	shrx	%rdi, (%rsi), %rbp
	cmpq	%rbp, %rdx
	jnb	.L6110
	salq	$5, %rdx
	addq	8(%rsi), %rdx
	vmovdqu	(%rdx), %xmm3
	movzbl	16(%rdx), %r8d
	vmovdqa	%xmm3, 160(%rsp)
	jmp	.L6109
	.p2align 4
	.p2align 3
.L6112:
	leaq	256(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 256(%rsp)
	movq	%rsi, %rcx
	movl	$32, 264(%rsp)
	vzeroupper
.LEHB115:
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	vmovdqa	192(%rsp), %xmm2
	movq	48(%rbx), %r8
	leaq	64(%rsp), %rdx
	movq	%rsi, %rcx
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	vmovdqa	%xmm2, 64(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatIoNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r11
	movq	%rax, 16(%r11)
.L6697:
	addq	$344, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L6127:
	leaq	148(%rsp), %r12
	xorl	%r8d, %r8d
	leaq	8(%rbx), %rdx
	movq	$0, 148(%rsp)
	movq	%r12, %rcx
	movl	$32, 156(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
.LEHE115:
	movzbl	149(%rsp), %esi
	andl	$120, %esi
	jne	.L6128
	movzbl	148(%rsp), %r11d
	testb	$92, %r11b
	jne	.L6706
	movq	48(%rbx), %rbp
	movq	%rax, 8(%rbx)
	andl	$32, %r11d
	leaq	240(%rsp), %rbx
	movzbl	192(%rsp), %esi
	movq	%rbx, 224(%rsp)
	movq	$0, 232(%rsp)
	movb	$0, 240(%rsp)
	jne	.L6707
	movl	$5, %r10d
	leaq	.LC119(%rip), %r9
	testb	%sil, %sil
	je	.L6146
	movl	$4, %r10d
	leaq	.LC120(%rip), %r9
.L6146:
	leaq	224(%rsp), %rsi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r10, 32(%rsp)
	movq	%rsi, %rcx
.LEHB116:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEyyPKcy.isra.0
	movq	232(%rsp), %rdx
.L6145:
	movq	224(%rsp), %r13
	leaq	112(%rsp), %rcx
	movq	%r12, %r9
	movq	%rbp, %r8
	movq	%rdx, 112(%rsp)
	movl	$1, 32(%rsp)
	movq	%r13, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
.LEHE116:
	movq	%rax, %r12
	movq	%rsi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%r12, %rax
	jmp	.L6133
	.p2align 4
	.p2align 3
.L6126:
	leaq	256(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movl	$7, %r8d
	movq	$0, 256(%rsp)
	movq	%rsi, %rcx
	movl	$32, 264(%rsp)
	vzeroupper
.LEHB117:
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movzbl	257(%rsp), %edx
	movl	%edx, %ecx
	notl	%edx
	andl	$120, %ecx
	andl	$56, %edx
	jne	.L6150
	testb	$92, 256(%rsp)
	jne	.L6708
	movq	%rax, 8(%rbx)
	movq	48(%rbx), %rbx
	cmpb	$56, %cl
	je	.L6709
	movq	16(%rbx), %rax
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6125:
	leaq	136(%rsp), %rdi
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 136(%rsp)
	movq	%rdi, %rcx
	movl	$32, 144(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movzbl	137(%rsp), %ecx
	movq	48(%rbx), %rsi
	movq	%rax, 8(%rbx)
	movl	192(%rsp), %ebx
	movl	%ecx, %ebp
	andl	$120, %ebp
	movl	%ebx, %r9d
	cmpb	$56, %bpl
	je	.L6710
	shrb	$3, %cl
	andl	$15, %ecx
	testl	%ebx, %ebx
	js	.L6711
	cmpb	$3, %cl
	ja	.L6166
	cmpb	$1, %cl
	ja	.L6712
	testl	%ebx, %ebx
	jne	.L6162
.L6700:
	movzbl	136(%rsp), %r10d
	movb	$48, 259(%rsp)
.L6179:
	leaq	259(%rsp), %r12
	leaq	260(%rsp), %r13
	leaq	258(%rsp), %r8
	movq	%r12, %rdx
.L6194:
	shrb	$2, %r10b
	andl	$3, %r10d
	cmpl	$1, %r10d
	je	.L6713
	cmpl	$3, %r10d
	je	.L6714
.L6197:
	leaq	112(%rsp), %rax
	movq	%r12, %r8
	subq	%rdx, %r13
	movq	%rdx, 120(%rsp)
	subq	%rdx, %r8
	movq	%rsi, %r9
	movq	%rax, %rdx
	movq	%rdi, %rcx
	movq	%r13, 112(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
.L6703:
	movq	%rax, 16(%rsi)
	addq	$344, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L6124:
	leaq	224(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 224(%rsp)
	movq	%rsi, %rcx
	movl	$32, 232(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movl	192(%rsp), %r9d
	movq	%rax, 8(%rbx)
	movzbl	225(%rsp), %eax
	movq	48(%rbx), %rbx
	movl	%eax, %edi
	andl	$120, %edi
	cmpb	$56, %dil
	je	.L6715
	shrb	$3, %al
	andl	$15, %eax
	cmpb	$4, %al
	je	.L6201
	ja	.L6202
	cmpb	$1, %al
	jbe	.L6203
	cmpb	$16, %dil
	leaq	.LC139(%rip), %r13
	leaq	.LC140(%rip), %r12
	cmovne	%r12, %r13
	testl	%r9d, %r9d
	jne	.L6716
	movl	$48, %r8d
	leaq	260(%rsp), %rbp
	leaq	259(%rsp), %r12
.L6208:
	movzbl	224(%rsp), %eax
	movb	%r8b, 259(%rsp)
	testb	$16, %al
	je	.L6701
.L6313:
	movq	$-2, %rdx
	movl	$2, %ecx
.L6212:
	addq	%r12, %rdx
	movl	%ecx, %edi
	testl	%ecx, %ecx
	je	.L6213
	xorl	%r9d, %r9d
	leal	-1(%rcx), %r10d
	movl	$1, %r8d
	movzbl	0(%r13,%r9), %r11d
	andl	$7, %r10d
	movb	%r11b, (%rdx,%r9)
	cmpl	%ecx, %r8d
	jnb	.L6213
	testl	%r10d, %r10d
	je	.L6229
	cmpl	$1, %r10d
	je	.L6590
	cmpl	$2, %r10d
	je	.L6591
	cmpl	$3, %r10d
	je	.L6592
	cmpl	$4, %r10d
	je	.L6593
	cmpl	$5, %r10d
	je	.L6594
	cmpl	$6, %r10d
	je	.L6595
	movl	$1, %r8d
	movzbl	0(%r13,%r8), %ecx
	movb	%cl, (%rdx,%r8)
	movl	$2, %r8d
.L6595:
	movl	%r8d, %r10d
	incl	%r8d
	movzbl	0(%r13,%r10), %r9d
	movb	%r9b, (%rdx,%r10)
.L6594:
	movl	%r8d, %ecx
	incl	%r8d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r11b, (%rdx,%rcx)
.L6593:
	movl	%r8d, %r10d
	incl	%r8d
	movzbl	0(%r13,%r10), %r9d
	movb	%r9b, (%rdx,%r10)
.L6592:
	movl	%r8d, %ecx
	incl	%r8d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r11b, (%rdx,%rcx)
.L6591:
	movl	%r8d, %r10d
	incl	%r8d
	movzbl	0(%r13,%r10), %r9d
	movb	%r9b, (%rdx,%r10)
.L6590:
	movl	%r8d, %ecx
	incl	%r8d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r11b, (%rdx,%rcx)
	cmpl	%edi, %r8d
	jnb	.L6213
.L6229:
	movl	%r8d, %r10d
	leal	1(%r8), %ecx
	movzbl	0(%r13,%r10), %r9d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r9b, (%rdx,%r10)
	movb	%r11b, (%rdx,%rcx)
	leal	2(%r8), %r10d
	leal	3(%r8), %ecx
	movzbl	0(%r13,%r10), %r9d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r9b, (%rdx,%r10)
	movb	%r11b, (%rdx,%rcx)
	leal	4(%r8), %r10d
	leal	5(%r8), %ecx
	movzbl	0(%r13,%r10), %r9d
	movzbl	0(%r13,%rcx), %r11d
	movb	%r9b, (%rdx,%r10)
	movb	%r11b, (%rdx,%rcx)
	leal	6(%r8), %r10d
	leal	7(%r8), %ecx
	movzbl	0(%r13,%r10), %r9d
	movzbl	0(%r13,%rcx), %r11d
	addl	$8, %r8d
	movb	%r9b, (%rdx,%r10)
	movb	%r11b, (%rdx,%rcx)
	cmpl	%edi, %r8d
	jb	.L6229
	.p2align 4
	.p2align 3
.L6213:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L6315
	cmpl	$3, %eax
	je	.L6717
.L6232:
	subq	%rdx, %rbp
	movq	%r12, %r8
	movq	%rdx, 120(%rsp)
	movq	%rbx, %r9
	movq	%rbp, 112(%rsp)
	leaq	112(%rsp), %rbp
	subq	%rdx, %r8
	movq	%rsi, %rcx
	movq	%rbp, %rdx
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6123:
	leaq	256(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 256(%rsp)
	movq	%rsi, %rcx
	movl	$32, 264(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movq	48(%rbx), %r8
	movq	192(%rsp), %rdx
	movq	%rsi, %rcx
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatIxNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %rbx
	movq	%rax, 16(%rbx)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6122:
	leaq	148(%rsp), %r12
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 148(%rsp)
	movq	%r12, %rcx
	movl	$32, 156(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	movzbl	149(%rsp), %edx
	movq	192(%rsp), %r8
	movq	%rax, 8(%rbx)
	movq	48(%rbx), %rbx
	movl	%edx, %ecx
	andl	$120, %ecx
	cmpb	$56, %cl
	je	.L6718
	shrb	$3, %dl
	andl	$15, %edx
	cmpb	$4, %dl
	je	.L6235
	ja	.L6236
	cmpb	$1, %dl
	jbe	.L6237
	cmpb	$16, %cl
	leaq	.LC139(%rip), %r13
	leaq	.LC140(%rip), %rdx
	cmovne	%rdx, %r13
	testq	%r8, %r8
	jne	.L6719
	movl	$48, %ecx
	leaq	260(%rsp), %rsi
	leaq	259(%rsp), %rdi
.L6242:
	movzbl	148(%rsp), %eax
	movb	%cl, 259(%rsp)
	testb	$16, %al
	je	.L6702
.L6324:
	movq	$-2, %rcx
	movl	$2, %r10d
.L6246:
	addq	%rdi, %rcx
	movl	%r10d, %r9d
	testl	%r10d, %r10d
	je	.L6247
	xorl	%edx, %edx
	leal	-1(%r10), %ebp
	movzbl	0(%r13,%rdx), %r11d
	andl	$7, %ebp
	movb	%r11b, (%rcx,%rdx)
	movl	$1, %edx
	cmpl	%r10d, %edx
	jnb	.L6247
	testl	%ebp, %ebp
	je	.L6270
	cmpl	$1, %ebp
	je	.L6596
	cmpl	$2, %ebp
	je	.L6597
	cmpl	$3, %ebp
	je	.L6598
	cmpl	$4, %ebp
	je	.L6599
	cmpl	$5, %ebp
	je	.L6600
	cmpl	$6, %ebp
	je	.L6601
	movl	$1, %r10d
	movl	$2, %edx
	movzbl	0(%r13,%r10), %r8d
	movb	%r8b, (%rcx,%r10)
.L6601:
	movl	%edx, %ebp
	incl	%edx
	movzbl	0(%r13,%rbp), %r11d
	movb	%r11b, (%rcx,%rbp)
.L6600:
	movl	%edx, %r8d
	incl	%edx
	movzbl	0(%r13,%r8), %r10d
	movb	%r10b, (%rcx,%r8)
.L6599:
	movl	%edx, %ebp
	incl	%edx
	movzbl	0(%r13,%rbp), %r11d
	movb	%r11b, (%rcx,%rbp)
.L6598:
	movl	%edx, %r8d
	incl	%edx
	movzbl	0(%r13,%r8), %r10d
	movb	%r10b, (%rcx,%r8)
.L6597:
	movl	%edx, %ebp
	incl	%edx
	movzbl	0(%r13,%rbp), %r11d
	movb	%r11b, (%rcx,%rbp)
.L6596:
	movl	%edx, %r8d
	incl	%edx
	movzbl	0(%r13,%r8), %r10d
	movb	%r10b, (%rcx,%r8)
	cmpl	%r9d, %edx
	jnb	.L6247
.L6270:
	movl	%edx, %ebp
	leal	1(%rdx), %r8d
	movzbl	0(%r13,%rbp), %r11d
	movzbl	0(%r13,%r8), %r10d
	movb	%r11b, (%rcx,%rbp)
	movb	%r10b, (%rcx,%r8)
	leal	2(%rdx), %ebp
	leal	3(%rdx), %r8d
	movzbl	0(%r13,%rbp), %r11d
	movzbl	0(%r13,%r8), %r10d
	movb	%r11b, (%rcx,%rbp)
	movb	%r10b, (%rcx,%r8)
	leal	4(%rdx), %ebp
	leal	5(%rdx), %r8d
	movzbl	0(%r13,%rbp), %r11d
	movzbl	0(%r13,%r8), %r10d
	movb	%r11b, (%rcx,%rbp)
	movb	%r10b, (%rcx,%r8)
	leal	6(%rdx), %ebp
	leal	7(%rdx), %r8d
	movzbl	0(%r13,%rbp), %r11d
	movzbl	0(%r13,%r8), %r10d
	addl	$8, %edx
	movb	%r11b, (%rcx,%rbp)
	movb	%r10b, (%rcx,%r8)
	cmpl	%r9d, %edx
	jb	.L6270
	.p2align 4
	.p2align 3
.L6247:
	shrb	$2, %al
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L6326
	cmpl	$3, %eax
	je	.L6720
.L6273:
	subq	%rcx, %rdi
	subq	%rcx, %rsi
	movq	%rcx, 120(%rsp)
	leaq	112(%rsp), %rdx
	movq	%rbx, %r9
	movq	%rdi, %r8
	movq	%r12, %rcx
	movq	%rsi, 112(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE13_M_format_intINS_10_Sink_iterIcEEEENSt20basic_format_contextIT_cE8iteratorESt17basic_string_viewIcSt11char_traitsIcEEyRS7_
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6121:
	leaq	256(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movq	$0, 256(%rsp)
	movl	$32, 264(%rsp)
	movq	%rsi, %rcx
	vzeroupper
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	vmovss	192(%rsp), %xmm1
	movq	48(%rbx), %r8
	movq	%rsi, %rcx
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIfNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r8
	movq	%rax, 16(%r8)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6120:
	leaq	256(%rsp), %rbp
	leaq	8(%rbx), %rdx
	movq	$0, 256(%rsp)
	movl	$32, 264(%rsp)
	movq	%rbp, %rcx
	vzeroupper
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	movq	48(%rbx), %r8
	movq	%rbp, %rcx
	vmovsd	192(%rsp), %xmm1
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIdNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r10
	movq	%rax, 16(%r10)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6119:
	leaq	256(%rsp), %rdi
	leaq	8(%rbx), %rdx
	movq	$0, 256(%rsp)
	movl	$32, 264(%rsp)
	movq	%rdi, %rcx
	vzeroupper
	call	_ZNSt8__format14__formatter_fpIcE5parseERSt26basic_format_parse_contextIcE
	movq	48(%rbx), %r8
	fldt	192(%rsp)
	leaq	80(%rsp), %rdx
	movq	%rdi, %rcx
	movq	%rax, 8(%rbx)
	fstpt	80(%rsp)
	movq	%r8, 56(%rsp)
	call	_ZNKSt8__format14__formatter_fpIcE6formatIeNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %r9
	movq	%rax, 16(%r9)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6118:
	leaq	256(%rsp), %r12
	leaq	8(%rbx), %rdx
	movq	$0, 256(%rsp)
	movl	$32, 264(%rsp)
	movq	%r12, %rcx
	vzeroupper
	call	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
	movq	192(%rsp), %r13
	movq	%rax, 8(%rbx)
	movq	48(%rbx), %rbx
	movq	%r13, %rcx
	call	strlen
	leaq	112(%rsp), %rdx
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%rax, 112(%rsp)
	movq	%r13, 120(%rsp)
	call	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
.L6704:
	movq	%rax, 16(%rbx)
	addq	$344, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4
	.p2align 3
.L6117:
	leaq	256(%rsp), %rbp
	leaq	8(%rbx), %rdx
	movq	$0, 256(%rsp)
	movl	$32, 264(%rsp)
	movq	%rbp, %rcx
	vzeroupper
	call	_ZNSt8__format15__formatter_strIcE5parseERSt26basic_format_parse_contextIcE
	vmovdqa	192(%rsp), %xmm4
	movq	48(%rbx), %r8
	leaq	112(%rsp), %rdx
	movq	%rbp, %rcx
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	vmovdqa	%xmm4, 112(%rsp)
	call	_ZNKSt8__format15__formatter_strIcE6formatINS_10_Sink_iterIcEEEET_St17basic_string_viewIcSt11char_traitsIcEERSt20basic_format_contextIS5_cE
	movq	56(%rsp), %rsi
	movq	%rax, 16(%rsi)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6116:
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rdx
	movq	$0, 148(%rsp)
	movl	$32, 156(%rsp)
	movq	$0, 256(%rsp)
	cmpq	%rdx, %rdi
	je	.L6274
	cmpb	$125, (%rdx)
	je	.L6274
	leaq	256(%rsp), %rbp
	movq	%rdi, %r8
	movl	$32, 264(%rsp)
	vzeroupper
	movq	%rbp, %rcx
	call	_ZNSt8__format5_SpecIcE23_M_parse_fill_and_alignEPKcS3_
	movq	%rax, %rdx
	cmpq	%rax, %rdi
	je	.L6275
	cmpb	$125, (%rax)
	je	.L6275
	leaq	8(%rbx), %r9
	movq	%rdi, %r8
	movq	%rbp, %rcx
	call	_ZNSt8__format5_SpecIcE14_M_parse_widthEPKcS3_RSt26basic_format_parse_contextIcE
	movq	%rax, %rdx
	cmpq	%rax, %rdi
	je	.L6279
	movzbl	(%rax), %r8d
	cmpb	$112, %r8b
	je	.L6721
.L6280:
	cmpb	$125, %r8b
	jne	.L6722
.L6279:
	movq	256(%rsp), %r9
	movl	264(%rsp), %r10d
	movq	48(%rbx), %rsi
	movq	%r9, 148(%rsp)
	movl	%r10d, 156(%rsp)
.L6277:
	movq	192(%rsp), %rax
	movq	%rdx, 8(%rbx)
	testq	%rax, %rax
	jne	.L6282
	movl	$3, %edx
	movb	$48, 258(%rsp)
.L6283:
	leaq	112(%rsp), %rcx
	leaq	148(%rsp), %r9
	movq	%rsi, %r8
	movw	$30768, 256(%rsp)
	movq	%rdx, 112(%rsp)
	movq	%rbp, 120(%rsp)
	movl	$2, 32(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE
	jmp	.L6703
	.p2align 4
	.p2align 3
.L6115:
	movq	192(%rsp), %r8
	movq	200(%rsp), %rax
	leaq	8(%rbx), %rcx
	movq	%rsi, %rdx
	vzeroupper
	addq	$344, %rsp
	popq	%rbx
	popq	%rsi
	popq	%rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	rex.W jmp	*%rax
	.p2align 4
	.p2align 3
.L6114:
	leaq	256(%rsp), %rsi
	leaq	8(%rbx), %rdx
	movl	$1, %r8d
	movq	$0, 256(%rsp)
	movq	%rsi, %rcx
	movl	$32, 264(%rsp)
	vzeroupper
	call	_ZNSt8__format15__formatter_intIcE11_M_do_parseERSt26basic_format_parse_contextIcENS_10_Pres_typeE
	vmovdqa	192(%rsp), %xmm5
	movq	48(%rbx), %r8
	leaq	64(%rsp), %rdx
	movq	%rsi, %rcx
	movq	%rax, 8(%rbx)
	movq	%r8, 56(%rsp)
	vmovdqa	%xmm5, 64(%rsp)
	call	_ZNKSt8__format15__formatter_intIcE6formatInNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	movq	56(%rsp), %rbx
	movq	%rax, 16(%rbx)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6696:
	vzeroupper
.L6110:
	call	_ZNSt8__format33__invalid_arg_id_in_format_stringEv
	.p2align 4
	.p2align 3
.L6150:
	movq	%rax, 8(%rbx)
	movzbl	192(%rsp), %edx
	movq	48(%rbx), %rbx
	testb	%cl, %cl
	je	.L6152
	movq	%rbx, %r8
	movq	%rsi, %rcx
	call	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6128:
	movq	48(%rbx), %rbp
	movzbl	192(%rsp), %edx
	movq	%rax, 8(%rbx)
	cmpb	$56, %sil
	je	.L6723
	movq	%rbp, %r8
	movq	%r12, %rcx
	call	_ZNKSt8__format15__formatter_intIcE6formatIhNS_10_Sink_iterIcEEEENSt20basic_format_contextIT0_cE8iteratorET_RS7_
.L6133:
	movq	%rax, 16(%rbp)
	jmp	.L6697
	.p2align 4
	.p2align 3
.L6710:
	leal	128(%rbx), %r12d
	cmpl	$255, %r12d
	ja	.L6157
	movb	%bl, 256(%rsp)
	leaq	112(%rsp), %rcx
	leaq	256(%rsp), %rbx
	movq	%rdi, %r8
	movq	%rsi, %rdx
	movq	$1, 112(%rsp)
	movq	%rbx, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L6703
	.p2align 4
	.p2align 3
.L6715:
	cmpl	$127, %r9d
	ja	.L6157
	leaq	256(%rsp), %r12
	movb	%r9b, 256(%rsp)
.L6705:
	leaq	112(%rsp), %rcx
	movq	%rsi, %r8
	movq	%rbx, %rdx
	movq	$1, 112(%rsp)
	movq	%r12, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6718:
	cmpq	$127, %r8
	ja	.L6157
	leaq	256(%rsp), %rcx
	movb	%r8b, 256(%rsp)
	movq	%rbx, %rdx
	movq	%r12, %r8
	movq	%rcx, 120(%rsp)
	leaq	112(%rsp), %rcx
	movq	$1, 112(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
	jmp	.L6704
	.p2align 4
	.p2align 3
.L6274:
	movq	256(%rsp), %r13
	leaq	256(%rsp), %rbp
	movl	$32, 264(%rsp)
	movl	$32, 156(%rsp)
	movq	%r13, 148(%rsp)
	vzeroupper
	jmp	.L6277
	.p2align 4
	.p2align 3
.L6282:
	movl	$67, %edi
	lzcntq	%rax, %rdx
	movabsq	$3978425819141910832, %rcx
	movabsq	$7378413942531504440, %rbx
	subl	%edx, %edi
	movq	%rcx, 224(%rsp)
	movq	%rbx, 232(%rsp)
	shrl	$2, %edi
	leal	-1(%rdi), %r9d
	cmpq	$255, %rax
	jbe	.L6284
	.p2align 4
	.p2align 3
.L6285:
	movq	%rax, %r12
	movq	%rax, %r8
	movl	%r9d, %r10d
	leal	-1(%r9), %r13d
	shrq	$4, %r12
	andl	$15, %r8d
	andl	$15, %r12d
	subl	$2, %r9d
	movzbl	224(%rsp,%r8), %r11d
	movzbl	224(%rsp,%r12), %edx
	shrq	$8, %rax
	movb	%r11b, 258(%rsp,%r10)
	movb	%dl, 258(%rsp,%r13)
	cmpq	$255, %rax
	ja	.L6285
.L6284:
	cmpq	$15, %rax
	jbe	.L6286
	movq	%rax, %rcx
	andl	$15, %ecx
	movzbl	224(%rsp,%rcx), %ebx
	shrq	$4, %rax
	movb	%bl, 259(%rsp)
	movzbl	224(%rsp,%rax), %eax
.L6287:
	leal	2(%rdi), %edi
	movb	%al, 258(%rsp)
	movslq	%edi, %rdx
	jmp	.L6283
	.p2align 4
	.p2align 3
.L6709:
	movzbl	192(%rsp), %edx
.L6152:
	leaq	224(%rsp), %r12
	movb	%dl, 224(%rsp)
	jmp	.L6705
	.p2align 4
	.p2align 3
.L6711:
	negl	%r9d
	cmpb	$4, %cl
	je	.L6160
	ja	.L6161
	cmpb	$1, %cl
	jbe	.L6162
	cmpb	$16, %bpl
	leaq	.LC140(%rip), %rcx
	leaq	.LC139(%rip), %r10
	cmove	%r10, %rcx
.L6163:
	movl	$32, %r8d
	movl	$31, %r11d
	lzcntl	%r9d, %r13d
	subl	%r13d, %r8d
	subl	%r13d, %r11d
	je	.L6176
	movl	$30, %edx
	movl	%r11d, %r12d
	subl	%r13d, %edx
	leaq	256(%rsp,%r12), %rax
	subq	%rdx, %r12
	movq	%rax, %r10
	leaq	255(%rsp,%r12), %rbp
	subq	%rbp, %r10
	andl	$7, %r10d
	je	.L6175
	cmpq	$1, %r10
	je	.L6545
	cmpq	$2, %r10
	je	.L6546
	cmpq	$3, %r10
	je	.L6547
	cmpq	$4, %r10
	je	.L6548
	cmpq	$5, %r10
	je	.L6549
	cmpq	$6, %r10
	je	.L6550
	movl	%r9d, %r13d
	decq	%rax
	andl	$1, %r13d
	addl	$48, %r13d
	shrl	%r9d
	movb	%r13b, 4(%rax)
.L6550:
	movl	%r9d, %r11d
	decq	%rax
	andl	$1, %r11d
	addl	$48, %r11d
	shrl	%r9d
	movb	%r11b, 4(%rax)
.L6549:
	movl	%r9d, %r12d
	decq	%rax
	andl	$1, %r12d
	addl	$48, %r12d
	shrl	%r9d
	movb	%r12b, 4(%rax)
.L6548:
	movl	%r9d, %edx
	decq	%rax
	andl	$1, %edx
	addl	$48, %edx
	shrl	%r9d
	movb	%dl, 4(%rax)
.L6547:
	movl	%r9d, %r10d
	decq	%rax
	andl	$1, %r10d
	addl	$48, %r10d
	shrl	%r9d
	movb	%r10b, 4(%rax)
.L6546:
	movl	%r9d, %r13d
	decq	%rax
	andl	$1, %r13d
	addl	$48, %r13d
	shrl	%r9d
	movb	%r13b, 4(%rax)
.L6545:
	movl	%r9d, %r11d
	decq	%rax
	andl	$1, %r11d
	addl	$48, %r11d
	movb	%r11b, 4(%rax)
	shrl	%r9d
	cmpq	%rax, %rbp
	je	.L6176
.L6175:
	movl	%r9d, %edx
	movl	%r9d, %r10d
	movl	%r9d, %r12d
	movl	%r9d, %r13d
	shrl	%edx
	shrl	$2, %r10d
	andl	$1, %r12d
	andl	$1, %edx
	andl	$1, %r10d
	movl	%r9d, %r11d
	addl	$48, %r12d
	addl	$48, %edx
	addl	$48, %r10d
	subq	$8, %rax
	movb	%r12b, 11(%rax)
	movb	%dl, 10(%rax)
	movl	%r9d, %r12d
	movl	%r9d, %edx
	movb	%r10b, 9(%rax)
	shrl	$3, %r13d
	movl	%r9d, %r10d
	andl	$1, %r13d
	shrl	$4, %r11d
	shrl	$5, %r12d
	shrl	$6, %edx
	shrb	$7, %r10b
	andl	$1, %r11d
	andl	$1, %r12d
	andl	$1, %edx
	addl	$48, %r13d
	addl	$48, %r11d
	addl	$48, %r12d
	addl	$48, %edx
	addl	$48, %r10d
	movb	%r13b, 8(%rax)
	movb	%r11b, 7(%rax)
	movb	%r12b, 6(%rax)
	movb	%dl, 5(%rax)
	movb	%r10b, 4(%rax)
	shrl	$8, %r9d
	cmpq	%rax, %rbp
	jne	.L6175
.L6176:
	movslq	%r8d, %r9
	leaq	259(%rsp), %r12
	leaq	259(%rsp,%r9), %r13
	movl	$49, %r9d
.L6173:
	movb	%r9b, 259(%rsp)
	testb	$16, 136(%rsp)
	je	.L6699
.L6303:
	movq	$-2, %rdx
	movl	$2, %r8d
.L6177:
	addq	%r12, %rdx
	movl	%r8d, %ebp
	testl	%r8d, %r8d
	je	.L6178
	xorl	%eax, %eax
	leal	-1(%r8), %r11d
	movzbl	(%rcx,%rax), %r10d
	andl	$7, %r11d
	movb	%r10b, (%rdx,%rax)
	movl	$1, %eax
	cmpl	%r8d, %eax
	jnb	.L6178
	testl	%r11d, %r11d
	je	.L6192
	cmpl	$1, %r11d
	je	.L6584
	cmpl	$2, %r11d
	je	.L6585
	cmpl	$3, %r11d
	je	.L6586
	cmpl	$4, %r11d
	je	.L6587
	cmpl	$5, %r11d
	je	.L6588
	cmpl	$6, %r11d
	je	.L6589
	movl	$1, %r9d
	movl	$2, %eax
	movzbl	(%rcx,%r9), %r8d
	movb	%r8b, (%rdx,%r9)
.L6589:
	movl	%eax, %r11d
	incl	%eax
	movzbl	(%rcx,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L6588:
	movl	%eax, %r9d
	incl	%eax
	movzbl	(%rcx,%r9), %r8d
	movb	%r8b, (%rdx,%r9)
.L6587:
	movl	%eax, %r11d
	incl	%eax
	movzbl	(%rcx,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L6586:
	movl	%eax, %r9d
	incl	%eax
	movzbl	(%rcx,%r9), %r8d
	movb	%r8b, (%rdx,%r9)
.L6585:
	movl	%eax, %r11d
	incl	%eax
	movzbl	(%rcx,%r11), %r10d
	movb	%r10b, (%rdx,%r11)
.L6584:
	movl	%eax, %r9d
	incl	%eax
	movzbl	(%rcx,%r9), %r8d
	movb	%r8b, (%rdx,%r9)
	cmpl	%ebp, %eax
	jnb	.L6178
.L6192:
	movl	%eax, %r11d
	leal	1(%rax), %r9d
	movzbl	(%rcx,%r11), %r10d
	movzbl	(%rcx,%r9), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r9)
	leal	2(%rax), %r11d
	leal	3(%rax), %r9d
	movzbl	(%rcx,%r11), %r10d
	movzbl	(%rcx,%r9), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r9)
	leal	4(%rax), %r11d
	leal	5(%rax), %r9d
	movzbl	(%rcx,%r11), %r10d
	movzbl	(%rcx,%r9), %r8d
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r9)
	leal	6(%rax), %r11d
	leal	7(%rax), %r9d
	movzbl	(%rcx,%r11), %r10d
	movzbl	(%rcx,%r9), %r8d
	addl	$8, %eax
	movb	%r10b, (%rdx,%r11)
	movb	%r8b, (%rdx,%r9)
	cmpl	%ebp, %eax
	jb	.L6192
	.p2align 4
	.p2align 3
.L6178:
	leaq	-1(%rdx), %r8
	testl	%ebx, %ebx
	js	.L6681
	movzbl	136(%rsp), %r10d
	jmp	.L6194
	.p2align 4
	.p2align 3
.L6681:
	movb	$45, -1(%rdx)
	movq	%r8, %rdx
	jmp	.L6197
	.p2align 4
	.p2align 3
.L6275:
	movq	256(%rsp), %r11
	movl	264(%rsp), %r12d
	movq	%r11, 148(%rsp)
	movl	%r12d, 156(%rsp)
	jmp	.L6277
	.p2align 4
	.p2align 3
.L6326:
	movl	$43, %ebp
.L6272:
	movb	%bpl, -1(%rcx)
	decq	%rcx
	jmp	.L6273
	.p2align 4
	.p2align 3
.L6315:
	movl	$43, %eax
.L6231:
	movb	%al, -1(%rdx)
	decq	%rdx
	jmp	.L6232
	.p2align 4
	.p2align 3
.L6286:
	movzbl	224(%rsp,%rax), %eax
	jmp	.L6287
.L6162:
	cmpl	$9, %r9d
	jbe	.L6296
	movl	%r9d, %edx
	movl	$1, %ebp
	movl	$3518437209, %ecx
	jmp	.L6185
	.p2align 4
	.p2align 3
.L6181:
	cmpl	$999, %edx
	jbe	.L6724
	cmpl	$9999, %edx
	jbe	.L6725
	movl	%edx, %r13d
	addl	$4, %ebp
	imulq	%rcx, %r13
	shrq	$45, %r13
	cmpl	$99999, %edx
	jbe	.L6182
	movl	%r13d, %edx
.L6185:
	cmpl	$99, %edx
	ja	.L6181
	incl	%ebp
.L6182:
	cmpl	$32, %ebp
	ja	.L6726
.L6180:
	leaq	259(%rsp), %r12
	movl	%r9d, %r8d
	movl	%ebp, %edx
	movq	%r12, %rcx
	leaq	(%r12,%rbp), %r13
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
.L6699:
	movq	%r12, %rdx
	jmp	.L6178
	.p2align 4
	.p2align 3
.L6723:
	leaq	256(%rsp), %rdi
	movb	%dl, 256(%rsp)
	leaq	112(%rsp), %rcx
	movq	%r12, %r8
	movq	%rbp, %rdx
	movq	$1, 112(%rsp)
	movq	%rdi, 120(%rsp)
	call	_ZNSt8__format22__write_padded_as_specIcNS_10_Sink_iterIcEEEET0_St17basic_string_viewINSt13type_identityIT_E4typeESt11char_traitsIS8_EEyRSt20basic_format_contextIS3_S6_ERKNS_5_SpecIS6_EENS_6_AlignE.constprop.0
.LEHE117:
	jmp	.L6133
	.p2align 4
	.p2align 3
.L6166:
	cmpb	$4, %cl
	je	.L6169
	cmpb	$40, %bpl
	je	.L6727
	testl	%ebx, %ebx
	jne	.L6165
	movb	$48, 259(%rsp)
	cmpb	$48, %bpl
	je	.L6300
	leaq	.LC141(%rip), %rcx
	leaq	260(%rsp), %r13
	leaq	259(%rsp), %r12
.L6190:
	testb	$16, 136(%rsp)
	jne	.L6303
	jmp	.L6699
	.p2align 4
	.p2align 3
.L6203:
	testl	%r9d, %r9d
	jne	.L6214
	leaq	260(%rsp), %rbp
	leaq	259(%rsp), %r12
	movb	$48, 259(%rsp)
.L6215:
	movzbl	224(%rsp), %eax
	movq	%r12, %rdx
	jmp	.L6213
	.p2align 4
	.p2align 3
.L6202:
	cmpb	$40, %dil
	je	.L6728
	testl	%r9d, %r9d
	jne	.L6226
	movb	$48, 259(%rsp)
	cmpb	$48, %dil
	je	.L6310
	leaq	.LC141(%rip), %r13
	leaq	260(%rsp), %rbp
	leaq	259(%rsp), %r12
.L6225:
	movzbl	224(%rsp), %eax
	testb	$16, %al
	jne	.L6313
.L6701:
	movq	%r12, %rdx
	jmp	.L6213
	.p2align 4
	.p2align 3
.L6237:
	testq	%r8, %r8
	jne	.L6248
	leaq	260(%rsp), %rsi
	leaq	259(%rsp), %rdi
	movb	$48, 259(%rsp)
.L6249:
	movzbl	148(%rsp), %eax
	movq	%rdi, %rcx
	jmp	.L6247
	.p2align 4
	.p2align 3
.L6236:
	cmpb	$40, %cl
	je	.L6729
	testq	%r8, %r8
	jne	.L6322
	movb	$48, 259(%rsp)
	cmpb	$48, %cl
	je	.L6323
	leaq	.LC141(%rip), %r13
	leaq	260(%rsp), %rsi
	leaq	259(%rsp), %rdi
.L6263:
	movzbl	148(%rsp), %eax
	testb	$16, %al
	jne	.L6324
.L6702:
	movq	%rdi, %rcx
	jmp	.L6247
	.p2align 4
	.p2align 3
.L6235:
	testq	%r8, %r8
	je	.L6256
	movl	$66, %r13d
	movl	$2863311531, %eax
	lzcntq	%r8, %r11
	subl	%r11d, %r13d
	imulq	%rax, %r13
	shrq	$33, %r13
	leal	-1(%r13), %edi
	cmpq	$63, %r8
	jbe	.L6258
	.p2align 6
	.p2align 4
	.p2align 3
.L6257:
	movq	%r8, %r10
	movq	%r8, %rbp
	movl	%edi, %r9d
	leal	-1(%rdi), %esi
	shrq	$3, %r10
	andl	$7, %ebp
	andl	$7, %r10d
	subl	$2, %edi
	shrq	$6, %r8
	addl	$48, %ebp
	addl	$48, %r10d
	movb	%bpl, 259(%rsp,%r9)
	movb	%r10b, 259(%rsp,%rsi)
	cmpq	$63, %r8
	ja	.L6257
.L6258:
	leal	48(%r8), %ecx
	cmpq	$7, %r8
	jbe	.L6260
	movq	%r8, %rcx
	movq	%r8, %rdx
	shrq	$3, %rcx
	andl	$7, %edx
	addl	$48, %ecx
	addl	$48, %edx
	movb	%dl, 260(%rsp)
.L6260:
	movl	%r13d, %r8d
	leaq	259(%rsp), %rdi
	leaq	.LC143(%rip), %r13
	movl	$1, %r10d
	leaq	259(%rsp,%r8), %rsi
	movb	%cl, 259(%rsp)
.L6261:
	movzbl	148(%rsp), %eax
	movq	%rdi, %rcx
	testb	$16, %al
	je	.L6247
	movq	%r10, %rcx
	negq	%rcx
	jmp	.L6246
	.p2align 4
	.p2align 3
.L6201:
	testl	%r9d, %r9d
	je	.L6222
	leaq	259(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	291(%rsp), %r8
	leaq	.LC143(%rip), %r13
	movq	%r12, %rdx
	call	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %rbp
	movl	$1, %ecx
.L6223:
	movzbl	224(%rsp), %eax
	movq	%r12, %rdx
	testb	$16, %al
	je	.L6213
	movq	%rcx, %rdx
	negq	%rdx
	jmp	.L6212
	.p2align 4
	.p2align 3
.L6720:
	movl	$32, %ebp
	jmp	.L6272
.L6717:
	movl	$32, %eax
	jmp	.L6231
.L6713:
	movb	$43, -1(%rdx)
.L6198:
	movq	%r8, %rdx
	jmp	.L6197
.L6169:
	testl	%ebx, %ebx
	je	.L6700
.L6160:
	leaq	259(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	291(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail12__to_chars_8IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	leaq	.LC143(%rip), %rcx
	movl	$1, %r8d
.L6187:
	movq	%r12, %rdx
	testb	$16, 136(%rsp)
	je	.L6178
	movq	%r8, %rdx
	negq	%rdx
	jmp	.L6177
	.p2align 4
	.p2align 3
.L6721:
	leaq	1(%rax), %rcx
	cmpq	%rcx, %rdi
	je	.L6327
	movzbl	1(%rax), %r8d
	movq	%rcx, %rdx
	jmp	.L6280
.L6712:
	cmpb	$16, %bpl
	leaq	.LC140(%rip), %rcx
	leaq	.LC139(%rip), %r11
	cmove	%r11, %rcx
	testl	%ebx, %ebx
	jne	.L6163
	movl	$48, %r9d
	leaq	260(%rsp), %r13
	leaq	259(%rsp), %r12
	jmp	.L6173
.L6714:
	movb	$32, -1(%rdx)
	jmp	.L6198
.L6248:
	cmpq	$9, %r8
	jbe	.L6318
	movq	%r8, %r9
	movl	$1, %esi
	movabsq	$3777893186295716171, %r13
	jmp	.L6255
	.p2align 4
	.p2align 3
.L6251:
	cmpq	$999, %r9
	jbe	.L6730
	cmpq	$9999, %r9
	jbe	.L6731
	movq	%r9, %rax
	addl	$4, %esi
	mulq	%r13
	shrq	$11, %rdx
	cmpq	$99999, %r9
	jbe	.L6252
	movq	%rdx, %r9
.L6255:
	cmpq	$99, %r9
	ja	.L6251
	incl	%esi
.L6252:
	cmpl	$64, %esi
	ja	.L6732
.L6250:
	leaq	259(%rsp), %rdi
	movl	%esi, %edx
	movq	%rdi, %rcx
	addq	%rdi, %rsi
	call	_ZNSt8__detail18__to_chars_10_implIyEEvPcjT_
	jmp	.L6249
.L6222:
	leaq	259(%rsp), %r12
	movzbl	224(%rsp), %eax
	leaq	260(%rsp), %rbp
	movb	$48, 259(%rsp)
	movq	%r12, %rdx
	jmp	.L6213
.L6719:
	movl	$64, %esi
	movl	$63, %eax
	lzcntq	%r8, %r11
	subl	%r11d, %esi
	subl	%r11d, %eax
	je	.L6245
	movl	$62, %r9d
	movl	%eax, %edi
	subl	%r11d, %r9d
	leaq	256(%rsp,%rdi), %rcx
	subq	%r9, %rdi
	movq	%rcx, %rbp
	leaq	255(%rsp,%rdi), %r10
	subq	%r10, %rbp
	andl	$7, %ebp
	je	.L6244
	cmpq	$1, %rbp
	je	.L6571
	cmpq	$2, %rbp
	je	.L6572
	cmpq	$3, %rbp
	je	.L6573
	cmpq	$4, %rbp
	je	.L6574
	cmpq	$5, %rbp
	je	.L6575
	cmpq	$6, %rbp
	je	.L6576
	movl	%r8d, %edx
	decq	%rcx
	andl	$1, %edx
	addl	$48, %edx
	shrq	%r8
	movb	%dl, 4(%rcx)
.L6576:
	movl	%r8d, %r11d
	decq	%rcx
	andl	$1, %r11d
	addl	$48, %r11d
	shrq	%r8
	movb	%r11b, 4(%rcx)
.L6575:
	movl	%r8d, %eax
	decq	%rcx
	andl	$1, %eax
	addl	$48, %eax
	shrq	%r8
	movb	%al, 4(%rcx)
.L6574:
	movl	%r8d, %edi
	decq	%rcx
	andl	$1, %edi
	addl	$48, %edi
	shrq	%r8
	movb	%dil, 4(%rcx)
.L6573:
	movl	%r8d, %r9d
	decq	%rcx
	andl	$1, %r9d
	addl	$48, %r9d
	shrq	%r8
	movb	%r9b, 4(%rcx)
.L6572:
	movl	%r8d, %ebp
	decq	%rcx
	andl	$1, %ebp
	addl	$48, %ebp
	shrq	%r8
	movb	%bpl, 4(%rcx)
.L6571:
	movl	%r8d, %edx
	decq	%rcx
	andl	$1, %edx
	addl	$48, %edx
	movb	%dl, 4(%rcx)
	shrq	%r8
	cmpq	%r10, %rcx
	je	.L6245
.L6244:
	movq	%r8, %rax
	movl	%r8d, %r11d
	movq	%r8, %rdi
	movq	%r8, %r9
	shrq	%rax
	andl	$1, %r11d
	andl	$1, %eax
	movq	%r8, %rbp
	movq	%r8, %rdx
	addl	$48, %r11d
	addl	$48, %eax
	subq	$8, %rcx
	movb	%r11b, 11(%rcx)
	movq	%r8, %r11
	movb	%al, 10(%rcx)
	movl	%r8d, %eax
	shrq	$2, %rdi
	shrq	$3, %r9
	shrq	$4, %rbp
	shrq	$5, %rdx
	shrq	$6, %r11
	shrb	$7, %al
	andl	$1, %edi
	andl	$1, %r9d
	andl	$1, %ebp
	andl	$1, %edx
	andl	$1, %r11d
	addl	$48, %edi
	addl	$48, %r9d
	addl	$48, %ebp
	addl	$48, %edx
	addl	$48, %r11d
	addl	$48, %eax
	movb	%dil, 9(%rcx)
	movb	%r9b, 8(%rcx)
	movb	%bpl, 7(%rcx)
	movb	%dl, 6(%rcx)
	movb	%r11b, 5(%rcx)
	movb	%al, 4(%rcx)
	shrq	$8, %r8
	cmpq	%r10, %rcx
	jne	.L6244
.L6245:
	movslq	%esi, %r8
	leaq	259(%rsp), %rdi
	movl	$49, %ecx
	leaq	259(%rsp,%r8), %rsi
	jmp	.L6242
.L6716:
	movl	$32, %r8d
	movl	$31, %edx
	lzcntl	%r9d, %r10d
	subl	%r10d, %r8d
	subl	%r10d, %edx
	je	.L6211
	movl	$30, %eax
	movl	%edx, %ebp
	subl	%r10d, %eax
	leaq	256(%rsp,%rbp), %rcx
	subq	%rax, %rbp
	movq	%rcx, %rdi
	leaq	255(%rsp,%rbp), %r11
	subq	%r11, %rdi
	andl	$7, %edi
	je	.L6210
	cmpq	$1, %rdi
	je	.L6558
	cmpq	$2, %rdi
	je	.L6559
	cmpq	$3, %rdi
	je	.L6560
	cmpq	$4, %rdi
	je	.L6561
	cmpq	$5, %rdi
	je	.L6562
	cmpq	$6, %rdi
	je	.L6563
	movl	%r9d, %r12d
	decq	%rcx
	andl	$1, %r12d
	addl	$48, %r12d
	shrl	%r9d
	movb	%r12b, 4(%rcx)
.L6563:
	movl	%r9d, %r10d
	decq	%rcx
	andl	$1, %r10d
	addl	$48, %r10d
	shrl	%r9d
	movb	%r10b, 4(%rcx)
.L6562:
	movl	%r9d, %edx
	decq	%rcx
	andl	$1, %edx
	addl	$48, %edx
	shrl	%r9d
	movb	%dl, 4(%rcx)
.L6561:
	movl	%r9d, %ebp
	decq	%rcx
	andl	$1, %ebp
	addl	$48, %ebp
	shrl	%r9d
	movb	%bpl, 4(%rcx)
.L6560:
	movl	%r9d, %eax
	decq	%rcx
	andl	$1, %eax
	addl	$48, %eax
	shrl	%r9d
	movb	%al, 4(%rcx)
.L6559:
	movl	%r9d, %edi
	decq	%rcx
	andl	$1, %edi
	addl	$48, %edi
	shrl	%r9d
	movb	%dil, 4(%rcx)
.L6558:
	movl	%r9d, %r12d
	decq	%rcx
	andl	$1, %r12d
	addl	$48, %r12d
	movb	%r12b, 4(%rcx)
	shrl	%r9d
	cmpq	%r11, %rcx
	je	.L6211
.L6210:
	movl	%r9d, %edx
	movl	%r9d, %r10d
	movl	%r9d, %ebp
	movl	%r9d, %eax
	shrl	%edx
	andl	$1, %r10d
	andl	$1, %edx
	movl	%r9d, %edi
	movl	%r9d, %r12d
	addl	$48, %r10d
	addl	$48, %edx
	subq	$8, %rcx
	movb	%r10b, 11(%rcx)
	movl	%r9d, %r10d
	movb	%dl, 10(%rcx)
	movl	%r9d, %edx
	shrl	$2, %ebp
	shrl	$3, %eax
	shrl	$4, %edi
	shrl	$5, %r12d
	shrl	$6, %r10d
	shrb	$7, %dl
	andl	$1, %ebp
	andl	$1, %eax
	andl	$1, %edi
	andl	$1, %r12d
	andl	$1, %r10d
	addl	$48, %ebp
	addl	$48, %eax
	addl	$48, %edi
	addl	$48, %r12d
	addl	$48, %r10d
	addl	$48, %edx
	movb	%bpl, 9(%rcx)
	movb	%al, 8(%rcx)
	movb	%dil, 7(%rcx)
	movb	%r12b, 6(%rcx)
	movb	%r10b, 5(%rcx)
	movb	%dl, 4(%rcx)
	shrl	$8, %r9d
	cmpq	%r11, %rcx
	jne	.L6210
.L6211:
	movslq	%r8d, %r9
	leaq	259(%rsp), %r12
	movl	$49, %r8d
	leaq	259(%rsp,%r9), %rbp
	jmp	.L6208
.L6256:
	leaq	259(%rsp), %rdi
	movzbl	148(%rsp), %eax
	leaq	260(%rsp), %rsi
	movb	$48, 259(%rsp)
	movq	%rdi, %rcx
	jmp	.L6247
.L6214:
	cmpl	$9, %r9d
	jbe	.L6307
	movl	%r9d, %edx
	movl	$1, %edi
	movl	$3518437209, %r8d
	jmp	.L6221
	.p2align 4
	.p2align 3
.L6217:
	cmpl	$999, %edx
	jbe	.L6733
	cmpl	$9999, %edx
	jbe	.L6734
	movl	%edx, %r13d
	addl	$4, %edi
	imulq	%r8, %r13
	shrq	$45, %r13
	cmpl	$99999, %edx
	jbe	.L6218
	movl	%r13d, %edx
.L6221:
	cmpl	$99, %edx
	ja	.L6217
	incl	%edi
.L6218:
	cmpl	$32, %edi
	ja	.L6735
.L6216:
	leaq	259(%rsp), %r12
	movl	%r9d, %r8d
	movl	%edi, %edx
	movq	%r12, %rcx
	leaq	(%r12,%rdi), %rbp
	call	_ZNSt8__detail18__to_chars_10_implIjEEvPcjT_
	jmp	.L6215
.L6161:
	cmpb	$40, %bpl
	je	.L6736
.L6165:
	leaq	259(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	291(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	cmpb	$48, %bpl
	jne	.L6301
	cmpq	%r12, %r13
	je	.L6302
.L6189:
	movq	%r13, %rax
	movq	%r12, %rbp
	subq	%r12, %rax
	andl	$7, %eax
	je	.L6191
	cmpq	$1, %rax
	je	.L6551
	cmpq	$2, %rax
	je	.L6552
	cmpq	$3, %rax
	je	.L6553
	cmpq	$4, %rax
	je	.L6554
	cmpq	$5, %rax
	je	.L6555
	cmpq	$6, %rax
	je	.L6556
	movsbl	(%r12), %ecx
	leaq	260(%rsp), %rbp
	call	toupper
	movb	%al, (%r12)
.L6556:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6555:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6554:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6553:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6552:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6551:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
	cmpq	%r13, %rbp
	je	.L6302
.L6191:
	movsbl	0(%rbp), %ecx
	addq	$8, %rbp
	call	toupper
	movsbl	-7(%rbp), %ecx
	movb	%al, -8(%rbp)
	call	toupper
	movsbl	-6(%rbp), %ecx
	movb	%al, -7(%rbp)
	call	toupper
	movsbl	-5(%rbp), %ecx
	movb	%al, -6(%rbp)
	call	toupper
	movsbl	-4(%rbp), %ecx
	movb	%al, -5(%rbp)
	call	toupper
	movsbl	-3(%rbp), %ecx
	movb	%al, -4(%rbp)
	call	toupper
	movsbl	-2(%rbp), %ecx
	movb	%al, -3(%rbp)
	call	toupper
	movsbl	-1(%rbp), %ecx
	movb	%al, -2(%rbp)
	call	toupper
	movb	%al, -1(%rbp)
	cmpq	%r13, %rbp
	jne	.L6191
.L6302:
	leaq	.LC141(%rip), %rcx
	movl	$2, %r8d
	jmp	.L6187
.L6729:
	testq	%r8, %r8
	jne	.L6321
	leaq	.LC142(%rip), %r13
	leaq	260(%rsp), %rsi
	leaq	259(%rsp), %rdi
	movb	$48, 259(%rsp)
	jmp	.L6263
.L6728:
	testl	%r9d, %r9d
	jne	.L6224
	leaq	.LC142(%rip), %r13
	leaq	260(%rsp), %rbp
	leaq	259(%rsp), %r12
	movb	$48, 259(%rsp)
	jmp	.L6225
.L6322:
	leaq	.LC141(%rip), %r13
.L6262:
	movl	$67, %eax
	lzcntq	%r8, %r11
	movabsq	$3978425819141910832, %rdi
	movabsq	$7378413942531504440, %r9
	subl	%r11d, %eax
	movq	%rdi, 224(%rsp)
	movq	%r9, 232(%rsp)
	shrl	$2, %eax
	movl	%eax, %ebp
	leal	-1(%rbp), %r10d
	cmpq	$255, %r8
	jbe	.L6265
	.p2align 4
	.p2align 3
.L6266:
	movq	%r8, %rax
	movq	%r8, %rdx
	movl	%r10d, %esi
	leal	-1(%r10), %edi
	shrq	$4, %rax
	andl	$15, %edx
	andl	$15, %eax
	subl	$2, %r10d
	movzbl	224(%rsp,%rdx), %r11d
	movzbl	224(%rsp,%rax), %r9d
	shrq	$8, %r8
	movb	%r11b, 259(%rsp,%rsi)
	movb	%r9b, 259(%rsp,%rdi)
	cmpq	$255, %r8
	ja	.L6266
.L6265:
	cmpq	$15, %r8
	jbe	.L6267
	movq	%r8, %r10
	andl	$15, %r10d
	movzbl	224(%rsp,%r10), %esi
	shrq	$4, %r8
	movb	%sil, 260(%rsp)
	movzbl	224(%rsp,%r8), %r8d
.L6268:
	leaq	259(%rsp), %rdi
	leaq	259(%rsp,%rbp), %rsi
	movb	%r8b, 259(%rsp)
	cmpb	$48, %cl
	jne	.L6263
.L6264:
	movq	%rsi, %rcx
	movq	%rdi, %rbp
	subq	%rdi, %rcx
	andl	$7, %ecx
	je	.L6269
	cmpq	$1, %rcx
	je	.L6577
	cmpq	$2, %rcx
	je	.L6578
	cmpq	$3, %rcx
	je	.L6579
	cmpq	$4, %rcx
	je	.L6580
	cmpq	$5, %rcx
	je	.L6581
	cmpq	$6, %rcx
	je	.L6582
	movsbl	(%rdi), %ecx
	leaq	260(%rsp), %rbp
	call	toupper
	movb	%al, (%rdi)
.L6582:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6581:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6580:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6579:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6578:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
.L6577:
	movsbl	0(%rbp), %ecx
	incq	%rbp
	call	toupper
	movb	%al, -1(%rbp)
	cmpq	%rsi, %rbp
	je	.L6695
.L6269:
	movsbl	0(%rbp), %ecx
	addq	$8, %rbp
	call	toupper
	movsbl	-7(%rbp), %ecx
	movb	%al, -8(%rbp)
	call	toupper
	movsbl	-6(%rbp), %ecx
	movb	%al, -7(%rbp)
	call	toupper
	movsbl	-5(%rbp), %ecx
	movb	%al, -6(%rbp)
	call	toupper
	movsbl	-4(%rbp), %ecx
	movb	%al, -5(%rbp)
	call	toupper
	movsbl	-3(%rbp), %ecx
	movb	%al, -4(%rbp)
	call	toupper
	movsbl	-2(%rbp), %ecx
	movb	%al, -3(%rbp)
	call	toupper
	movsbl	-1(%rbp), %ecx
	movb	%al, -2(%rbp)
	call	toupper
	movb	%al, -1(%rbp)
	cmpq	%rsi, %rbp
	jne	.L6269
.L6695:
	movl	$2, %r10d
	jmp	.L6261
.L6707:
	cmpb	$0, 32(%rbp)
	leaq	24(%rbp), %rdi
	je	.L6737
.L6134:
	leaq	256(%rsp), %r13
	movq	%rdi, %rdx
	movq	%r13, %rcx
	call	_ZNSt6localeC1ERKS_
	movq	.refptr._ZNSt7__cxx118numpunctIcE2idE(%rip), %rcx
	call	_ZNKSt6locale2id5_M_idEv
	movq	256(%rsp), %r8
	movq	%rax, %r9
	movq	8(%r8), %rax
	movq	(%rax,%r9,8), %rdi
	testq	%rdi, %rdi
	je	.L6135
	movq	%r13, %rcx
	call	_ZNSt6localeD1Ev
	movq	(%rdi), %r11
	testb	%sil, %sil
	je	.L6738
	leaq	224(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%r13, %rcx
.LEHB118:
	call	*40(%r11)
.L6138:
	movq	224(%rsp), %r10
	movq	%r10, %rdx
	cmpq	%rbx, %r10
	je	.L6739
	movq	256(%rsp), %rcx
	leaq	272(%rsp), %rsi
	cmpq	%rsi, %rcx
	je	.L6288
	vmovdqu	264(%rsp), %xmm1
	movq	240(%rsp), %rbx
	movq	%rcx, 224(%rsp)
	vmovdqu	%xmm1, 232(%rsp)
	testq	%r10, %r10
	je	.L6144
	movq	%r10, 256(%rsp)
	movq	%rbx, 272(%rsp)
.L6143:
	movq	%r13, %rcx
	movq	$0, 264(%rsp)
	leaq	224(%rsp), %rsi
	movb	$0, (%r10)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	232(%rsp), %rdx
	jmp	.L6145
.L6226:
	leaq	259(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	291(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %rbp
	cmpb	$48, %dil
	jne	.L6311
	cmpq	%r12, %rbp
	je	.L6312
.L6227:
	movq	%rbp, %r11
	movq	%r12, %rdi
	subq	%r12, %r11
	andl	$7, %r11d
	je	.L6228
	cmpq	$1, %r11
	je	.L6564
	cmpq	$2, %r11
	je	.L6565
	cmpq	$3, %r11
	je	.L6566
	cmpq	$4, %r11
	je	.L6567
	cmpq	$5, %r11
	je	.L6568
	cmpq	$6, %r11
	je	.L6569
	movsbl	(%r12), %ecx
	leaq	260(%rsp), %rdi
	call	toupper
	movb	%al, (%r12)
.L6569:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L6568:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L6567:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L6566:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L6565:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
.L6564:
	movsbl	(%rdi), %ecx
	incq	%rdi
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%rbp, %rdi
	je	.L6312
.L6228:
	movsbl	(%rdi), %ecx
	addq	$8, %rdi
	call	toupper
	movsbl	-7(%rdi), %ecx
	movb	%al, -8(%rdi)
	call	toupper
	movsbl	-6(%rdi), %ecx
	movb	%al, -7(%rdi)
	call	toupper
	movsbl	-5(%rdi), %ecx
	movb	%al, -6(%rdi)
	call	toupper
	movsbl	-4(%rdi), %ecx
	movb	%al, -5(%rdi)
	call	toupper
	movsbl	-3(%rdi), %ecx
	movb	%al, -4(%rdi)
	call	toupper
	movsbl	-2(%rdi), %ecx
	movb	%al, -3(%rdi)
	call	toupper
	movsbl	-1(%rdi), %ecx
	movb	%al, -2(%rdi)
	call	toupper
	movb	%al, -1(%rdi)
	cmpq	%rbp, %rdi
	jne	.L6228
.L6312:
	leaq	.LC141(%rip), %r13
	movl	$2, %ecx
	jmp	.L6223
.L6267:
	movzbl	224(%rsp,%r8), %r8d
	jmp	.L6268
.L6727:
	testl	%ebx, %ebx
	jne	.L6188
	movzbl	136(%rsp), %r10d
	movb	$48, 259(%rsp)
	testb	$16, %r10b
	je	.L6179
	movq	$-2, %rdx
	leaq	260(%rsp), %r13
	leaq	.LC142(%rip), %rcx
	movl	$2, %r8d
	leaq	259(%rsp), %r12
	jmp	.L6177
.L6321:
	leaq	.LC142(%rip), %r13
	jmp	.L6262
.L6224:
	leaq	259(%rsp), %r12
	leaq	96(%rsp), %rcx
	leaq	291(%rsp), %r8
	leaq	.LC142(%rip), %r13
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %rbp
	jmp	.L6225
.L6738:
	leaq	224(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%r13, %rcx
	call	*48(%r11)
.LEHE118:
	jmp	.L6138
.L6736:
	leaq	96(%rsp), %rcx
	leaq	259(%rsp), %r12
.L6698:
	leaq	291(%rsp), %r8
	movq	%r12, %rdx
	call	_ZNSt8__detail13__to_chars_16IjEESt15to_chars_resultPcS2_T_
	movq	96(%rsp), %r13
	leaq	.LC142(%rip), %rcx
	jmp	.L6190
.L6300:
	leaq	260(%rsp), %r13
	leaq	259(%rsp), %r12
	jmp	.L6189
.L6310:
	leaq	260(%rsp), %rbp
	leaq	259(%rsp), %r12
	jmp	.L6227
.L6323:
	leaq	260(%rsp), %rsi
	leaq	.LC141(%rip), %r13
	leaq	259(%rsp), %rdi
	jmp	.L6264
.L6724:
	addl	$2, %ebp
	jmp	.L6182
.L6730:
	addl	$2, %esi
	jmp	.L6252
.L6311:
	leaq	.LC141(%rip), %r13
	jmp	.L6225
.L6733:
	addl	$2, %edi
	jmp	.L6218
.L6301:
	leaq	.LC141(%rip), %rcx
	jmp	.L6190
.L6725:
	addl	$3, %ebp
	jmp	.L6182
.L6731:
	addl	$3, %esi
	jmp	.L6252
.L6734:
	addl	$3, %edi
	jmp	.L6218
.L6737:
	movq	%rdi, %rcx
	call	_ZNSt6localeC1Ev
	movb	$1, 32(%rbp)
	jmp	.L6134
.L6739:
	movq	256(%rsp), %r9
	leaq	272(%rsp), %rsi
	cmpq	%rsi, %r9
	je	.L6288
	vmovdqu	264(%rsp), %xmm3
	movq	%r9, 224(%rsp)
	vmovdqu	%xmm3, 232(%rsp)
.L6144:
	leaq	272(%rsp), %r10
	movq	%rsi, 256(%rsp)
	jmp	.L6143
.L6327:
	movq	%rdi, %rdx
	jmp	.L6279
.L6288:
	movq	264(%rsp), %r8
	testq	%r8, %r8
	je	.L6141
	cmpq	$1, %r8
	je	.L6740
	movl	%r8d, %ecx
	movq	%rdx, %rdi
	rep movsb
	movq	224(%rsp), %rdx
	movq	264(%rsp), %r8
.L6141:
	movq	%r8, 232(%rsp)
	movb	$0, (%rdx,%r8)
	movq	256(%rsp), %r10
	jmp	.L6143
.L6732:
	leaq	323(%rsp), %rsi
	leaq	259(%rsp), %rdi
	jmp	.L6249
.L6726:
	leaq	291(%rsp), %r13
	leaq	259(%rsp), %r12
	jmp	.L6699
.L6735:
	leaq	291(%rsp), %rbp
	leaq	259(%rsp), %r12
	jmp	.L6215
.L6188:
	leaq	96(%rsp), %rcx
	leaq	259(%rsp), %r12
	jmp	.L6698
.L6307:
	movl	$1, %edi
	jmp	.L6216
.L6318:
	movl	$1, %esi
	jmp	.L6250
.L6296:
	movl	$1, %ebp
	jmp	.L6180
.L6740:
	movzbl	272(%rsp), %eax
	movb	%al, (%rdx)
	movq	224(%rsp), %rdx
	movq	264(%rsp), %r8
	jmp	.L6141
.L6706:
	leaq	.LC155(%rip), %rcx
.LEHB119:
	call	_ZSt20__throw_format_errorPKc
.LEHE119:
.L6135:
.LEHB120:
	call	_ZSt16__throw_bad_castv
.LEHE120:
.L6157:
	leaq	.LC144(%rip), %rcx
.LEHB121:
	call	_ZSt20__throw_format_errorPKc
.L6708:
	leaq	.LC156(%rip), %rcx
	call	_ZSt20__throw_format_errorPKc
.L6328:
	movq	%rax, %rbp
	vzeroupper
.L6149:
	movq	%rsi, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_disposeEv
	movq	%rbp, %rcx
	call	_Unwind_Resume
.L6722:
	call	_ZNSt8__format29__failed_to_parse_format_specEv
.LEHE121:
.L6329:
	movq	%r13, %rcx
	movq	%rax, %rbp
	vzeroupper
	leaq	224(%rsp), %rsi
	call	_ZNSt6localeD1Ev
	jmp	.L6149
	.seh_handler	__gxx_personality_seh0, @unwind, @except
	.seh_handlerdata
.LLSDA15251:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE15251-.LLSDACSB15251
.LLSDACSB15251:
	.uleb128 .LEHB115-.LFB15251
	.uleb128 .LEHE115-.LEHB115
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB116-.LFB15251
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L6328-.LFB15251
	.uleb128 0
	.uleb128 .LEHB117-.LFB15251
	.uleb128 .LEHE117-.LEHB117
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB118-.LFB15251
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L6328-.LFB15251
	.uleb128 0
	.uleb128 .LEHB119-.LFB15251
	.uleb128 .LEHE119-.LEHB119
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB120-.LFB15251
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L6329-.LFB15251
	.uleb128 0
	.uleb128 .LEHB121-.LFB15251
	.uleb128 .LEHE121-.LEHB121
	.uleb128 0
	.uleb128 0
.LLSDACSE15251:
	.section	.text$_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy,"x"
	.linkonce discard
	.seh_endproc
	.section	.text.startup,"x"
	.p2align 4
	.def	_GLOBAL__sub_I__Z8getcyclev;	.scl	3;	.type	32;	.endef
	.seh_proc	_GLOBAL__sub_I__Z8getcyclev
_GLOBAL__sub_I__Z8getcyclev:
.LFB15873:
	subq	$40, %rsp
	.seh_stackalloc	40
	.seh_endprologue
	movq	$0, all_info(%rip)
	movq	$0, 8+all_info(%rip)
	movq	$0, 16+all_info(%rip)
	movl	$130944, %ecx
	call	_Znwy
	vpxor	%xmm1, %xmm1, %xmm1
	vpxor	%xmm0, %xmm0, %xmm0
	leaq	130944(%rax), %rcx
	leaq	192(%rax), %rdx
	movq	%rax, all_info(%rip)
	vmovdqu8	%zmm0, (%rax)
	movq	%rcx, 16+all_info(%rip)
	vmovdqu64	%zmm1, 64(%rax)
	vmovdqu64	%zmm1, 128(%rax)
	.p2align 4
	.p2align 3
.L6742:
	vmovdqu64	(%rax), %zmm2
	addq	$576, %rdx
	vmovdqu64	%zmm2, -576(%rdx)
	vmovdqu64	(%rax), %zmm3
	vmovdqu64	%zmm3, -512(%rdx)
	vmovdqu64	(%rax), %zmm4
	vmovdqu64	%zmm4, -448(%rdx)
	vmovdqu64	(%rax), %zmm5
	vmovdqu64	%zmm5, -384(%rdx)
	vmovdqu64	(%rax), %zmm0
	vmovdqu64	%zmm0, -320(%rdx)
	vmovdqu64	(%rax), %zmm1
	vmovdqu64	%zmm1, -256(%rdx)
	vmovdqu64	(%rax), %zmm2
	vmovdqu64	%zmm2, -192(%rdx)
	vmovdqu64	(%rax), %zmm3
	vmovdqu64	%zmm3, -128(%rdx)
	vmovdqu64	(%rax), %zmm4
	vmovdqu64	%zmm4, -64(%rdx)
	cmpq	%rdx, %rcx
	jne	.L6742
	movq	%rcx, 8+all_info(%rip)
	leaq	__tcf_0(%rip), %rcx
	vzeroupper
	addq	$40, %rsp
	jmp	atexit
	.seh_endproc
	.section	.ctors,"w"
	.align 8
	.quad	_GLOBAL__sub_I__Z8getcyclev
	.section .rdata,"dr"
	.align 32
CSWTCH.923:
	.long	3
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	0
	.long	2
	.globl	_ZTSSt9exception
	.section	.rdata$_ZTSSt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTSSt9exception:
	.ascii "St9exception\0"
	.globl	_ZTISt9exception
	.section	.rdata$_ZTISt9exception,"dr"
	.linkonce same_size
	.align 8
_ZTISt9exception:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSSt9exception
	.globl	_ZTSSt13runtime_error
	.section	.rdata$_ZTSSt13runtime_error,"dr"
	.linkonce same_size
	.align 16
_ZTSSt13runtime_error:
	.ascii "St13runtime_error\0"
	.globl	_ZTISt13runtime_error
	.section	.rdata$_ZTISt13runtime_error,"dr"
	.linkonce same_size
	.align 8
_ZTISt13runtime_error:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt13runtime_error
	.quad	_ZTISt9exception
	.globl	_ZTSSt12format_error
	.section	.rdata$_ZTSSt12format_error,"dr"
	.linkonce same_size
	.align 16
_ZTSSt12format_error:
	.ascii "St12format_error\0"
	.globl	_ZTISt12format_error
	.section	.rdata$_ZTISt12format_error,"dr"
	.linkonce same_size
	.align 8
_ZTISt12format_error:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSSt12format_error
	.quad	_ZTISt13runtime_error
	.globl	_ZTSNSt8__format5_SinkIcEE
	.section	.rdata$_ZTSNSt8__format5_SinkIcEE,"dr"
	.linkonce same_size
	.align 16
_ZTSNSt8__format5_SinkIcEE:
	.ascii "NSt8__format5_SinkIcEE\0"
	.globl	_ZTINSt8__format5_SinkIcEE
	.section	.rdata$_ZTINSt8__format5_SinkIcEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format5_SinkIcEE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSNSt8__format5_SinkIcEE
	.globl	_ZTSNSt8__format9_Buf_sinkIcEE
	.section	.rdata$_ZTSNSt8__format9_Buf_sinkIcEE,"dr"
	.linkonce same_size
	.align 16
_ZTSNSt8__format9_Buf_sinkIcEE:
	.ascii "NSt8__format9_Buf_sinkIcEE\0"
	.globl	_ZTINSt8__format9_Buf_sinkIcEE
	.section	.rdata$_ZTINSt8__format9_Buf_sinkIcEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format9_Buf_sinkIcEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSNSt8__format9_Buf_sinkIcEE
	.quad	_ZTINSt8__format5_SinkIcEE
	.globl	_ZTSNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.rdata$_ZTSNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.ascii "NSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE\0"
	.globl	_ZTINSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.rdata$_ZTINSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.quad	_ZTINSt8__format9_Buf_sinkIcEE
	.globl	_ZTSNSt8__format8_ScannerIcEE
	.section	.rdata$_ZTSNSt8__format8_ScannerIcEE,"dr"
	.linkonce same_size
	.align 16
_ZTSNSt8__format8_ScannerIcEE:
	.ascii "NSt8__format8_ScannerIcEE\0"
	.globl	_ZTINSt8__format8_ScannerIcEE
	.section	.rdata$_ZTINSt8__format8_ScannerIcEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format8_ScannerIcEE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSNSt8__format8_ScannerIcEE
	.globl	_ZTSNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE
	.section	.rdata$_ZTSNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE,"dr"
	.linkonce same_size
	.align 32
_ZTSNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE:
	.ascii "NSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE\0"
	.globl	_ZTINSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE
	.section	.rdata$_ZTINSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE
	.quad	_ZTINSt8__format9_Buf_sinkIcEE
	.globl	_ZTSNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE
	.section	.rdata$_ZTSNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE,"dr"
	.linkonce same_size
	.align 32
_ZTSNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE:
	.ascii "NSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE\0"
	.globl	_ZTINSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE
	.section	.rdata$_ZTINSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE,"dr"
	.linkonce same_size
	.align 8
_ZTINSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE
	.quad	_ZTINSt8__format8_ScannerIcEE
	.globl	_ZTVSt12format_error
	.section	.rdata$_ZTVSt12format_error,"dr"
	.linkonce same_size
	.align 8
_ZTVSt12format_error:
	.quad	0
	.quad	_ZTISt12format_error
	.quad	_ZNSt12format_errorD1Ev
	.quad	_ZNSt12format_errorD0Ev
	.quad	_ZNKSt13runtime_error4whatEv
	.globl	_ZTVNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.rdata$_ZTVNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	_ZTINSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.quad	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE11_M_overflowEv
	.quad	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE10_M_reserveEy
	.quad	_ZNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE7_M_bumpEy
	.globl	_ZTVNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE
	.section	.rdata$_ZTVNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE,"dr"
	.linkonce same_size
	.align 8
_ZTVNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE:
	.quad	0
	.quad	_ZTINSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEEE
	.quad	_ZNSt8__format10_Iter_sinkIcNS_10_Sink_iterIcEEE11_M_overflowEv
	.quad	_ZNSt8__format5_SinkIcE10_M_reserveEy
	.quad	_ZNSt8__format5_SinkIcE7_M_bumpEy
	.globl	_ZTVNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE
	.section	.rdata$_ZTVNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE,"dr"
	.linkonce same_size
	.align 8
_ZTVNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE:
	.quad	0
	.quad	_ZTINSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcEE
	.quad	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE11_M_on_charsEPKc
	.quad	_ZNSt8__format19_Formatting_scannerINS_10_Sink_iterIcEEcE13_M_format_argEy
	.globl	_ZZ19my_dou_to_sci_pure3ILi16EEidPcE11short_array
	.section	.rdata$_ZZ19my_dou_to_sci_pure3ILi16EEidPcE11short_array,"dr"
	.linkonce same_size
	.align 32
_ZZ19my_dou_to_sci_pure3ILi16EEidPcE11short_array:
	.word	12336
	.word	12592
	.word	12848
	.word	13104
	.word	13360
	.word	13616
	.word	13872
	.word	14128
	.word	14384
	.word	14640
	.word	12337
	.word	12593
	.word	12849
	.word	13105
	.word	13361
	.word	13617
	.word	13873
	.word	14129
	.word	14385
	.word	14641
	.word	12338
	.word	12594
	.word	12850
	.word	13106
	.word	13362
	.word	13618
	.word	13874
	.word	14130
	.word	14386
	.word	14642
	.word	12339
	.word	12595
	.word	12851
	.word	13107
	.word	13363
	.word	13619
	.word	13875
	.word	14131
	.word	14387
	.word	14643
	.word	12340
	.word	12596
	.word	12852
	.word	13108
	.word	13364
	.word	13620
	.word	13876
	.word	14132
	.word	14388
	.word	14644
	.word	12341
	.word	12597
	.word	12853
	.word	13109
	.word	13365
	.word	13621
	.word	13877
	.word	14133
	.word	14389
	.word	14645
	.word	12342
	.word	12598
	.word	12854
	.word	13110
	.word	13366
	.word	13622
	.word	13878
	.word	14134
	.word	14390
	.word	14646
	.word	12343
	.word	12599
	.word	12855
	.word	13111
	.word	13367
	.word	13623
	.word	13879
	.word	14135
	.word	14391
	.word	14647
	.word	12344
	.word	12600
	.word	12856
	.word	13112
	.word	13368
	.word	13624
	.word	13880
	.word	14136
	.word	14392
	.word	14648
	.word	12345
	.word	12601
	.word	12857
	.word	13113
	.word	13369
	.word	13625
	.word	13881
	.word	14137
	.word	14393
	.word	14649
	.globl	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8
	.section	.data$_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8,"w"
	.linkonce same_size
	.align 64
_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E7index_8:
	.space 256
	.globl	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E11short_array
	.section	.rdata$_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E11short_array,"dr"
	.linkonce same_size
	.align 32
_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E11short_array:
	.word	12336
	.word	12592
	.word	12848
	.word	13104
	.word	13360
	.word	13616
	.word	13872
	.word	14128
	.word	14384
	.word	14640
	.word	12337
	.word	12593
	.word	12849
	.word	13105
	.word	13361
	.word	13617
	.word	13873
	.word	14129
	.word	14385
	.word	14641
	.word	12338
	.word	12594
	.word	12850
	.word	13106
	.word	13362
	.word	13618
	.word	13874
	.word	14130
	.word	14386
	.word	14642
	.word	12339
	.word	12595
	.word	12851
	.word	13107
	.word	13363
	.word	13619
	.word	13875
	.word	14131
	.word	14387
	.word	14643
	.word	12340
	.word	12596
	.word	12852
	.word	13108
	.word	13364
	.word	13620
	.word	13876
	.word	14132
	.word	14388
	.word	14644
	.word	12341
	.word	12597
	.word	12853
	.word	13109
	.word	13365
	.word	13621
	.word	13877
	.word	14133
	.word	14389
	.word	14645
	.word	12342
	.word	12598
	.word	12854
	.word	13110
	.word	13366
	.word	13622
	.word	13878
	.word	14134
	.word	14390
	.word	14646
	.word	12343
	.word	12599
	.word	12855
	.word	13111
	.word	13367
	.word	13623
	.word	13879
	.word	14135
	.word	14391
	.word	14647
	.word	12344
	.word	12600
	.word	12856
	.word	13112
	.word	13368
	.word	13624
	.word	13880
	.word	14136
	.word	14392
	.word	14648
	.word	12345
	.word	12601
	.word	12857
	.word	13113
	.word	13369
	.word	13625
	.word	13881
	.word	14137
	.word	14393
	.word	14649
	.globl	_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E8call_num
	.section	.data$_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E8call_num,"w"
	.linkonce same_size
	.align 8
_ZZ25my_dou_to_sci_avx512_pureILi0ELi16ELi32ELi3EEiPdPPcS1_E8call_num:
	.space 8
	.globl	_ZZ13my_dou_to_sciILi1ELi16EEidPcE11short_array
	.section	.rdata$_ZZ13my_dou_to_sciILi1ELi16EEidPcE11short_array,"dr"
	.linkonce same_size
	.align 32
_ZZ13my_dou_to_sciILi1ELi16EEidPcE11short_array:
	.word	12336
	.word	12592
	.word	12848
	.word	13104
	.word	13360
	.word	13616
	.word	13872
	.word	14128
	.word	14384
	.word	14640
	.word	12337
	.word	12593
	.word	12849
	.word	13105
	.word	13361
	.word	13617
	.word	13873
	.word	14129
	.word	14385
	.word	14641
	.word	12338
	.word	12594
	.word	12850
	.word	13106
	.word	13362
	.word	13618
	.word	13874
	.word	14130
	.word	14386
	.word	14642
	.word	12339
	.word	12595
	.word	12851
	.word	13107
	.word	13363
	.word	13619
	.word	13875
	.word	14131
	.word	14387
	.word	14643
	.word	12340
	.word	12596
	.word	12852
	.word	13108
	.word	13364
	.word	13620
	.word	13876
	.word	14132
	.word	14388
	.word	14644
	.word	12341
	.word	12597
	.word	12853
	.word	13109
	.word	13365
	.word	13621
	.word	13877
	.word	14133
	.word	14389
	.word	14645
	.word	12342
	.word	12598
	.word	12854
	.word	13110
	.word	13366
	.word	13622
	.word	13878
	.word	14134
	.word	14390
	.word	14646
	.word	12343
	.word	12599
	.word	12855
	.word	13111
	.word	13367
	.word	13623
	.word	13879
	.word	14135
	.word	14391
	.word	14647
	.word	12344
	.word	12600
	.word	12856
	.word	13112
	.word	13368
	.word	13624
	.word	13880
	.word	14136
	.word	14392
	.word	14648
	.word	12345
	.word	12601
	.word	12857
	.word	13113
	.word	13369
	.word	13625
	.word	13881
	.word	14137
	.word	14393
	.word	14649
	.globl	_ZZ13my_dou_to_sciILi0ELi16EEidPcE11short_array
	.section	.rdata$_ZZ13my_dou_to_sciILi0ELi16EEidPcE11short_array,"dr"
	.linkonce same_size
	.align 32
_ZZ13my_dou_to_sciILi0ELi16EEidPcE11short_array:
	.word	12336
	.word	12592
	.word	12848
	.word	13104
	.word	13360
	.word	13616
	.word	13872
	.word	14128
	.word	14384
	.word	14640
	.word	12337
	.word	12593
	.word	12849
	.word	13105
	.word	13361
	.word	13617
	.word	13873
	.word	14129
	.word	14385
	.word	14641
	.word	12338
	.word	12594
	.word	12850
	.word	13106
	.word	13362
	.word	13618
	.word	13874
	.word	14130
	.word	14386
	.word	14642
	.word	12339
	.word	12595
	.word	12851
	.word	13107
	.word	13363
	.word	13619
	.word	13875
	.word	14131
	.word	14387
	.word	14643
	.word	12340
	.word	12596
	.word	12852
	.word	13108
	.word	13364
	.word	13620
	.word	13876
	.word	14132
	.word	14388
	.word	14644
	.word	12341
	.word	12597
	.word	12853
	.word	13109
	.word	13365
	.word	13621
	.word	13877
	.word	14133
	.word	14389
	.word	14645
	.word	12342
	.word	12598
	.word	12854
	.word	13110
	.word	13366
	.word	13622
	.word	13878
	.word	14134
	.word	14390
	.word	14646
	.word	12343
	.word	12599
	.word	12855
	.word	13111
	.word	13367
	.word	13623
	.word	13879
	.word	14135
	.word	14391
	.word	14647
	.word	12344
	.word	12600
	.word	12856
	.word	13112
	.word	13368
	.word	13624
	.word	13880
	.word	14136
	.word	14392
	.word	14648
	.word	12345
	.word	12601
	.word	12857
	.word	13113
	.word	13369
	.word	13625
	.word	13881
	.word	14137
	.word	14393
	.word	14649
	.globl	_ZZ20myDoubleToScientificILi0ELi6EEidPciE11short_array
	.section	.rdata$_ZZ20myDoubleToScientificILi0ELi6EEidPciE11short_array,"dr"
	.linkonce same_size
	.align 32
_ZZ20myDoubleToScientificILi0ELi6EEidPciE11short_array:
	.word	12336
	.word	12592
	.word	12848
	.word	13104
	.word	13360
	.word	13616
	.word	13872
	.word	14128
	.word	14384
	.word	14640
	.word	12337
	.word	12593
	.word	12849
	.word	13105
	.word	13361
	.word	13617
	.word	13873
	.word	14129
	.word	14385
	.word	14641
	.word	12338
	.word	12594
	.word	12850
	.word	13106
	.word	13362
	.word	13618
	.word	13874
	.word	14130
	.word	14386
	.word	14642
	.word	12339
	.word	12595
	.word	12851
	.word	13107
	.word	13363
	.word	13619
	.word	13875
	.word	14131
	.word	14387
	.word	14643
	.word	12340
	.word	12596
	.word	12852
	.word	13108
	.word	13364
	.word	13620
	.word	13876
	.word	14132
	.word	14388
	.word	14644
	.word	12341
	.word	12597
	.word	12853
	.word	13109
	.word	13365
	.word	13621
	.word	13877
	.word	14133
	.word	14389
	.word	14645
	.word	12342
	.word	12598
	.word	12854
	.word	13110
	.word	13366
	.word	13622
	.word	13878
	.word	14134
	.word	14390
	.word	14646
	.word	12343
	.word	12599
	.word	12855
	.word	13111
	.word	13367
	.word	13623
	.word	13879
	.word	14135
	.word	14391
	.word	14647
	.word	12344
	.word	12600
	.word	12856
	.word	13112
	.word	13368
	.word	13624
	.word	13880
	.word	14136
	.word	14392
	.word	14648
	.word	12345
	.word	12601
	.word	12857
	.word	13113
	.word	13369
	.word	13625
	.word	13881
	.word	14137
	.word	14393
	.word	14649
	.globl	all_info
	.bss
	.align 16
all_info:
	.space 24
	.globl	fastPowDouble
fastPowDouble:
	.space 1
.lcomm _ZGVZ20generateRandomDoubleddE9generator,8,8
.lcomm _ZZ20generateRandomDoubleddE9generator,2504,32
	.section .rdata,"dr"
	.align 32
_ZL5_10en:
	.long	0
	.long	0
	.long	2
	.long	0
	.long	20
	.long	0
	.long	202
	.long	0
	.long	2024
	.long	0
	.long	20240
	.long	0
	.long	202402
	.long	0
	.long	2024023
	.long	0
	.long	20240225
	.long	0
	.long	202402253
	.long	0
	.long	2024022533
	.long	0
	.long	-1234611149
	.long	4
	.long	538790395
	.long	47
	.long	1092936657
	.long	471
	.long	-1955535317
	.long	4712
	.long	1919483311
	.long	47125
	.long	2014963922
	.long	471254
	.long	742442509
	.long	3275288
	.long	928053137
	.long	6715550
	.long	-987417227
	.long	10229445
	.long	-1690877591
	.long	13733435
	.long	-2113596989
	.long	17166794
	.long	579229236
	.long	20672061
	.long	898889185
	.long	24192230
	.long	-1023872167
	.long	27618847
	.long	-1279840209
	.long	31115687
	.long	-799900131
	.long	34651656
	.long	73866661
	.long	38071691
	.long	-981408498
	.long	41560301
	.long	-1687122135
	.long	45111700
	.long	-1035160845
	.long	48525305
	.long	853532592
	.long	52005880
	.long	1066915740
	.long	55570166
	.long	-406919487
	.long	58979673
	.long	1638834290
	.long	62452400
	.long	2048542862
	.long	66006876
	.long	-867144359
	.long	69434777
	.long	1063553199
	.long	72899840
	.long	1329441499
	.long	76444736
	.long	830900937
	.long	79890600
	.long	1038626171
	.long	83348178
	.long	-849200935
	.long	86883718
	.long	1079862152
	.long	90347124
	.long	1349827690
	.long	93797393
	.long	-1533940860
	.long	97323797
	.long	-2032454861
	.long	100804333
	.long	-393084929
	.long	104247464
	.long	582385663
	.long	107764947
	.long	-172879872
	.long	111262211
	.long	-216099841
	.long	114698372
	.long	803617023
	.long	118207142
	.long	-571481184
	.long	121720743
	.long	-714351481
	.long	125150097
	.long	1254544297
	.long	128650358
	.long	-289651638
	.long	132179913
	.long	1785419100
	.long	135602620
	.long	-2063193421
	.long	139094571
	.long	857987760
	.long	142639707
	.long	-1257124
	.long	146055921
	.long	2145912243
	.long	149539758
	.long	267453328
	.long	153100109
	.long	1408058484
	.long	156509984
	.long	1760073105
	.long	159985896
	.long	-2094875915
	.long	163544354
	.long	-1846168359
	.long	166964789
	.long	-160226800
	.long	170432962
	.long	-1274025325
	.long	173981747
	.long	1351217820
	.long	177420320
	.long	1689022275
	.long	180880936
	.long	2111277844
	.long	184420274
	.long	-1901676820
	.long	187876559
	.long	1917871272
	.long	191329795
	.long	1323597265
	.long	194859908
	.long	-1320235357
	.long	198333490
	.long	1570931276
	.long	201779519
	.long	889922271
	.long	205300623
	.long	-2128153141
	.long	208791097
	.long	-512707778
	.long	212230087
	.long	-640884723
	.long	215742393
	.long	673188872
	.long	219249364
	.long	841486090
	.long	222681481
	.long	2125599437
	.long	226185195
	.long	791628736
	.long	229708275
	.long	-84205904
	.long	233133679
	.long	-105257380
	.long	236629003
	.long	2081697786
	.long	240167815
	.long	1528380408
	.long	243586665
	.long	-1310749962
	.long	247073795
	.long	1328264922
	.long	250627970
	.long	-487152496
	.long	254040418
	.long	-1682682444
	.long	257519547
	.long	-2103353055
	.long	261082026
	.long	-1851466571
	.long	264494922
	.long	906892258
	.long	267966237
	.long	-2087610149
	.long	271518948
	.long	-767885431
	.long	274950158
	.long	-2033598613
	.long	278413842
	.long	679227205
	.long	281957015
	.long	2035129739
	.long	285406110
	.long	396428526
	.long	288862342
	.long	-1651947990
	.long	292396199
	.long	-1032467494
	.long	295862760
	.long	-216842543
	.long	299311714
	.long	-1344795003
	.long	302836475
	.long	1306986771
	.long	306320093
	.long	-1587492008
	.long	309761940
	.long	-910623186
	.long	313277817
	.long	504602333
	.long	316778092
	.long	630752916
	.long	320212999
	.long	-285300679
	.long	323720200
	.long	-1788925661
	.long	327236741
	.long	-88673428
	.long	330664870
	.long	-1184583609
	.long	334163600
	.long	1943989805
	.long	337696026
	.long	282503608
	.long	341117537
	.long	1426871334
	.long	344607993
	.long	-718818152
	.long	348155931
	.long	-898522691
	.long	351570978
	.long	2098072109
	.long	355053355
	.long	774424156
	.long	358616443
	.long	-105711629
	.long	362025177
	.long	2015344112
	.long	365499664
	.long	-1775787156
	.long	369056340
	.long	-572996061
	.long	372480116
	.long	357496748
	.long	375946898
	.long	-1700612713
	.long	379493942
	.long	547729790
	.long	382935778
	.long	-1462821410
	.long	386395034
	.long	1392698710
	.long	389932673
	.long	-740176042
	.long	393392144
	.long	148521771
	.long	396844053
	.long	1259394038
	.long	400372506
	.long	1860863098
	.long	403849200
	.long	-1968888424
	.long	407293932
	.long	-1387368706
	.long	410813415
	.long	-867105441
	.long	414306928
	.long	-10139978
	.long	417744652
	.long	1061066852
	.long	421255376
	.long	663166782
	.long	424765314
	.long	-1318525170
	.long	428196194
	.long	1573069010
	.long	431698363
	.long	446297219
	.long	435224341
	.long	1631613348
	.long	438648538
	.long	-107966963
	.long	442142352
	.long	-1678092088
	.long	445683994
	.long	1123610362
	.long	449101665
	.long	-1816712520
	.long	452587321
	.long	-61703501
	.long	456144259
	.long	-77129376
	.long	459555556
	.long	977330104
	.long	463033246
	.long	-925821018
	.long	466593925
	.long	-1652379960
	.long	470010195
	.long	-2065474950
	.long	473480104
	.long	-1508101864
	.long	477031058
	.long	-1479434577
	.long	480465563
	.long	-1849293221
	.long	483927874
	.long	909608946
	.long	487469331
	.long	31634679
	.long	490921644
	.long	39543349
	.long	494376535
	.long	-1024312638
	.long	497908716
	.long	-103324487
	.long	501378419
	.long	-129155609
	.long	504826064
	.long	912297313
	.long	508349189
	.long	1107056733
	.long	511835875
	.long	310079092
	.long	515276444
	.long	387598865
	.long	518790723
	.long	-294621621
	.long	522293993
	.long	1779206621
	.long	525727652
	.long	-2070959019
	.long	529233293
	.long	1926876085
	.long	532752760
	.long	-1886372190
	.long	536179670
	.long	863260235
	.long	539676876
	.long	-1607946001
	.long	543212159
	.long	-2009932502
	.long	546632479
	.long	1782551669
	.long	550121447
	.long	-1570259767
	.long	553672176
	.long	-889082885
	.long	557086060
	.long	-37611782
	.long	560566983
	.long	-47014727
	.long	564131705
	.long	1044357619
	.long	567540396
	.long	1305447024
	.long	571013463
	.long	558066956
	.long	574568365
	.long	885662760
	.long	577995468
	.long	1107078450
	.long	581460863
	.long	310106238
	.long	585006175
	.long	1804429135
	.long	588451259
	.long	1181794595
	.long	591909162
	.long	-670240405
	.long	595445108
	.long	117970659
	.long	598907753
	.long	1221205148
	.long	602358339
	.long	452764611
	.long	605885140
	.long	-1864505766
	.long	609364932
	.long	-1256890384
	.long	612808373
	.long	576370668
	.long	616326243
	.long	-176639244
	.long	619822781
	.long	1926684592
	.long	623259245
	.long	-812869731
	.long	626768392
	.long	-2118656318
	.long	630281285
	.long	-500836750
	.long	633710934
	.long	-1699787761
	.long	637211564
	.long	-525496439
	.long	640740427
	.long	-656870548
	.long	644163422
	.long	-1894830009
	.long	647655734
	.long	426343980
	.long	651200194
	.long	-1614553673
	.long	654616690
	.long	1203033381
	.long	658100879
	.long	-1932458697
	.long	661660569
	.long	-268089723
	.long	665070719
	.long	-335112154
	.long	668546975
	.long	-418890192
	.long	672105863
	.long	-261806370
	.long	675525492
	.long	746483861
	.long	678994002
	.long	-1214378822
	.long	682543206
	.long	851625973
	.long	685980992
	.long	1064532466
	.long	689441936
	.long	1330665582
	.long	692981684
	.long	-1315817659
	.long	696437200
	.long	-571030250
	.long	699890756
	.long	359954011
	.long	703421270
	.long	-848770567
	.long	706894101
	.long	1086520439
	.long	710340443
	.long	284408725
	.long	713861938
	.long	1251497277
	.long	717351679
	.long	490629773
	.long	720790975
	.long	-460454608
	.long	724303662
	.long	1322828606
	.long	727809917
	.long	-1567689715
	.long	731242332
	.long	-885870319
	.long	734746419
	.long	1593814698
	.long	738268800
	.long	1992268373
	.long	741694496
	.long	-1804631830
	.long	745190184
	.long	1556459666
	.long	748728313
	.long	-1275650889
	.long	752147447
	.long	-1594563611
	.long	755634933
	.long	-2070344081
	.long	759188441
	.long	-440446453
	.long	762601167
	.long	-550558067
	.long	766080643
	.long	-688197583
	.long	769643556
	.long	106747423
	.long	773055639
	.long	-940307546
	.long	776527292
	.long	-101642608
	.long	780080427
	.long	2083957018
	.long	783510843
	.long	1531204448
	.long	786974858
	.long	-233478088
	.long	790518444
	.long	390947107
	.long	793966764
	.long	488683884
	.long	797423319
	.long	-462886969
	.long	800957580
	.long	247566556
	.long	804423384
	.long	309458195
	.long	807872654
	.long	-1760660904
	.long	811397809
	.long	-26671241
	.long	814880686
	.long	-1107080875
	.long	818322842
	.long	1837374378
	.long	821839105
	.long	-462253750
	.long	825338656
	.long	495924637
	.long	828773865
	.long	1693647620
	.long	832281443
	.long	521658851
	.long	835797278
	.long	-1495410085
	.long	839225701
	.long	278221042
	.long	842724799
	.long	1784500887
	.long	846256535
	.long	1156884285
	.long	849678333
	.long	-1775120115
	.long	853169148
	.long	-572579160
	.long	856716413
	.long	1431759698
	.long	860131741
	.long	-1431525850
	.long	863614468
	.long	-357832744
	.long	867176898
	.long	-1521032754
	.long	870585907
	.long	-1901290943
	.long	874060736
	.long	-1302871854
	.long	877617840
	.long	1870059651
	.long	881040814
	.long	190090916
	.long	884507930
	.long	-1909870003
	.long	888055392
	.long	1490685808
	.long	891496444
	.long	1863357260
	.long	894956027
	.long	1255454751
	.long	898494074
	.long	1858401043
	.long	901952780
	.long	-1971965992
	.long	905405007
	.long	1830009806
	.long	908933859
	.long	606885217
	.long	912409806
	.long	-1388877127
	.long	915854849
	.long	411387239
	.long	919374722
	.long	1330858849
	.long	922867505
	.long	-1557651911
	.long	926305533
	.long	200418759
	.long	929816637
	.long	662132636
	.long	933325862
	.long	-1319817853
	.long	936757039
	.long	-1649772316
	.long	940259579
	.long	1116375951
	.long	943784861
	.long	-1825755534
	.long	947209348
	.long	-1208452593
	.long	950703525
	.long	-1829024695
	.long	954244487
	.long	2008686428
	.long	957662441
	.long	-710367437
	.long	961148451
	.long	1703504000
	.long	964704726
	.long	-18103648
	.long	968116299
	.long	-22629561
	.long	971594334
	.long	-1102028775
	.long	975155446
	.long	921844752
	.long	978570906
	.long	-995177708
	.long	982041152
	.long	-170230311
	.long	985592528
	.long	-1717006681
	.long	989026242
	.long	1074967121
	.long	992488883
	.long	269967078
	.long	996030752
	.long	168729423
	.long	999482292
	.long	210911779
	.long	1002937505
	.long	1337381548
	.long	1006470089
	.long	-774749268
	.long	1009939037
	.long	1179047063
	.long	1013386997
	.long	-1747416644
	.long	1016910514
	.long	-1629006314
	.long	1020396463
	.long	-2036257893
	.long	1023837339
	.long	1749644930
	.long	1027352002
	.long	-2127697391
	.long	1030854553
	.long	-512138091
	.long	1034288511
	.long	-640172613
	.long	1037794527
	.long	-400107883
	.long	1041313291
	.long	-500134854
	.long	1044740494
	.long	-1698910392
	.long	1048238066
	.long	-1598689907
	.long	1051772663
	.long	-1998362383
	.long	1055193269
	.long	-350469331
	.long	1058682594
	.long	-755914244
	.long	1062232653
	.long	1202590843
	.long	1065646817
	.long	-1717986918
	.long	1069128089
	.long	0
	.long	1072693248
	.long	0
	.long	1076101120
	.long	0
	.long	1079574528
	.long	0
	.long	1083129856
	.long	0
	.long	1086556160
	.long	0
	.long	1090021888
	.long	0
	.long	1093567616
	.long	0
	.long	1097011920
	.long	0
	.long	1100470148
	.long	0
	.long	1104006501
	.long	536870912
	.long	1107468383
	.long	-402653184
	.long	1110919286
	.long	-1577058304
	.long	1114446484
	.long	-448790528
	.long	1117925532
	.long	512753664
	.long	1121369284
	.long	640942080
	.long	1124887541
	.long	937459712
	.long	1128383353
	.long	-2049400832
	.long	1131820119
	.long	1733216256
	.long	1135329645
	.long	1620131072
	.long	1138841828
	.long	2025163840
	.long	1142271773
	.long	-689770672
	.long	1145772772
	.long	105764242
	.long	1149300943
	.long	-941536522
	.long	1152724226
	.long	2044304820
	.long	1156216899
	.long	740819601
	.long	1159760682
	.long	-1221459147
	.long	1163177460
	.long	-453082110
	.long	1166662001
	.long	790565505
	.long	1170221031
	.long	-85534943
	.long	1173631456
	.long	966823146
	.long	1177108057
	.long	-2012696540
	.long	1180667375
	.long	-1257935337
	.long	1184086197
	.long	575064476
	.long	1187555043
	.long	-354911229
	.long	1191104667
	.long	1925664130
	.long	1194541665
	.long	-814145309
	.long	1198002937
	.long	1129802011
	.long	1201543096
	.long	706126257
	.long	1204997843
	.long	-191084003
	.long	1208451719
	.long	-238855003
	.long	1211982633
	.long	924457447
	.long	1215454714
	.long	-991911839
	.long	1218901368
	.long	-166147975
	.long	1222423254
	.long	1506770252
	.long	1225912262
	.long	-264020834
	.long	1229351863
	.long	-330026042
	.long	1232864933
	.long	-1280008100
	.long	1236370471
	.long	-1600010125
	.long	1239803185
	.long	147470991
	.long	1243307646
	.long	-981572454
	.long	1246829326
	.long	1994259904
	.long	1250255314
	.long	345341232
	.long	1253751367
	.long	1826451006
	.long	1257288812
	.long	-2011903538
	.long	1260708231
	.long	1780087873
	.long	1264196073
	.long	-498057815
	.long	1267748913
	.long	1524911379
	.long	1271161918
	.long	-241344425
	.long	1274641741
	.long	1845803117
	.long	1278205089
	.long	-456985788
	.long	1281616356
	.long	502509589
	.long	1285088350
	.long	-1519346661
	.long	1288641909
	.long	-2023333487
	.long	1292071529
	.long	-381683211
	.long	1295535875
	.long	-477104014
	.long	1299079876
	.long	238680903
	.long	1302527419
	.long	-775390695
	.long	1305984297
	.long	1178245279
	.long	1309518964
	.long	-1411080348
	.long	1312984008
	.long	-690108611
	.long	1316433594
	.long	-1936377588
	.long	1319959145
	.long	-136494169
	.long	1323441281
	.long	1976865937
	.long	1326883746
	.long	323598773
	.long	1330400395
	.long	-334621679
	.long	1333899222
	.long	-1492018922
	.long	1337334732
	.long	-791281829
	.long	1340842687
	.long	-494551143
	.long	1344357815
	.long	-618188929
	.long	1347786533
	.long	1374747487
	.long	1351285999
	.long	-1825137381
	.long	1354817045
	.long	-133938078
	.long	1358239130
	.long	-1241164421
	.long	1361730305
	.long	298014061
	.long	1365276897
	.long	1446259400
	.long	1368692505
	.long	-1413401222
	.long	1372175583
	.long	-883375764
	.long	1375737355
	.long	-1104219705
	.long	1379146638
	.long	1840950841
	.long	1382621810
	.long	153704903
	.long	1386179343
	.long	1706678301
	.long	1389601513
	.long	-1087877596
	.long	1393068963
	.long	-1359846995
	.long	1396616844
	.long	-313033460
	.long	1400057111
	.long	-391291825
	.long	1403517021
	.long	1658368867
	.long	1407055477
	.long	1573351454
	.long	1410513417
	.long	-1254536155
	.long	1413965963
	.long	-1568170194
	.long	1417495214
	.long	630506365
	.long	1420970413
	.long	1861874780
	.long	1424415768
	.long	-1967623821
	.long	1427936030
	.long	380847848
	.long	1431428083
	.long	-597682014
	.long	1434866415
	.long	-747102518
	.long	1438377899
	.long	1680544575
	.long	1441886411
	.long	1026938894
	.long	1445317886
	.long	-863810030
	.long	1448820797
	.long	-1613623093
	.long	1452345382
	.long	1204196606
	.long	1455770160
	.long	1505245757
	.long	1459264700
	.long	-1206705050
	.long	1462804981
	.long	639102336
	.long	1466223219
	.long	-274863904
	.long	1469709583
	.long	-171789940
	.long	1473265193
	.long	1932746223
	.long	1476677044
	.long	-1879034517
	.long	1480155425
	.long	-201309499
	.long	1483716969
	.long	947923387
	.long	1487131618
	.long	-962579414
	.long	1490602202
	.long	2018001205
	.long	1494154001
	.long	-349361983
	.long	1497586922
	.long	-1510444303
	.long	1501049893
	.long	259428269
	.long	1504592175
	.long	1772755404
	.long	1508042941
	.long	-1005281217
	.long	1511498476
	.long	-182859697
	.long	1515031463
	.long	-114287310
	.long	1518499656
	.long	930882686
	.long	1521947931
	.long	89861534
	.long	1525471842
	.long	1129905282
	.long	1528957053
	.long	-1808843869
	.long	1532398236
	.long	-1187313012
	.long	1535913283
	.long	1405413015
	.long	1539415114
	.long	-390717379
	.long	1542849372
	.long	585345101
	.long	1546355764
	.long	-1781642960
	.long	1549873824
	.long	-1153311876
	.long	1553301320
	.long	-367898021
	.long	1556799258
	.long	-766807175
	.long	1560333168
	.long	115232855
	.long	1563754061
	.long	1217782893
	.long	1567243744
	.long	761114308
	.long	1570793132
	.long	951392885
	.long	1574207575
	.long	115499282
	.long	1577689197
	.long	609057963
	.long	1581253700
	.long	761322454
	.long	1584661845
	.long	2025394892
	.long	1588135594
	.long	384259967
	.long	1591691349
	.long	777033391
	.long	1595116853
	.long	2045033563
	.long	1598582914
	.long	408808306
	.long	1602129059
	.long	-281365721
	.long	1605572581
	.long	1795776497
	.long	1609031135
	.long	1170978797
	.long	1612567895
	.long	-1952492812
	.long	1616029014
	.long	780609457
	.long	1619480236
	.long	975761821
	.long	1623007831
	.long	-2074503422
	.long	1626486134
	.long	628096195
	.long	1629930196
	.long	785120244
	.long	1633448841
	.long	-1119912584
	.long	1636943925
	.long	747592918
	.long	1640380995
	.long	-139250676
	.long	1643890899
	.long	2060451975
	.long	1647402372
	.long	-1719402327
	.long	1650832613
	.long	-1769260
	.long	1654333982
	.long	1609506948
	.long	1657861459
	.long	938141861
	.long	1661285032
	.long	1172677327
	.long	1664778066
	.long	1806665153
	.long	1668321171
	.long	1184589618
	.long	1671738232
	.long	1480737022
	.long	1675223126
	.long	-148281185
	.long	1678781493
	.long	1962132166
	.long	1682192195
	.long	1378923384
	.long	1685669140
	.long	1723654230
	.long	1689228889
	.long	-533328842
	.long	1692646903
	.long	-666661053
	.long	1696116085
	.long	1314157332
	.long	1699666131
	.long	284477420
	.long	1703102340
	.long	355596776
	.long	1706563941
	.long	1518237793
	.long	1710104510
	.long	-124843203
	.long	1713558486
	.long	-1229795828
	.long	1717012684
	.long	-463502961
	.long	1720543999
	.long	-289689351
	.long	1724015327
	.long	-362111688
	.long	1727462295
	.long	-452639610
	.long	1730984573
	.long	-1356641580
	.long	1734472846
	.long	1525423497
	.long	1737912754
	.long	-240704277
	.long	1741426206
	.long	1460172563
	.long	1744931027
	.long	751473879
	.long	1748364040
	.long	939342349
	.long	1751868874
	.long	1660830792
	.long	1755389854
	.long	-71445158
	.long	1758816133
	.long	2058177201
	.long	1762312551
	.long	-1397993809
	.long	1765849312
	.long	-673750438
	.long	1769269016
	.long	231553777
	.long	1772757215
	.long	1755333846
	.long	1776309387
	.long	1120425484
	.long	1779722670
	.long	-746951793
	.long	1783202841
	.long	1213793907
	.long	1786766624
	.long	758621192
	.long	1790177076
	.long	948276490
	.long	1793649409
	.long	-2035879860
	.long	1797203393
	.long	-198683088
	.long	1800632216
	.long	825387963
	.long	1804096895
	.long	-42006870
	.long	1807641310
	.long	1584358442
	.long	1811088075
	.long	906706229
	.long	1814545278
	.long	-1014100862
	.long	1818080349
	.long	-1707554863
	.long	1821544634
	.long	1086781894
	.long	1824994537
	.long	-1862748105
	.long	1828520483
	.long	983266083
	.long	1832001878
	.long	-918401045
	.long	1835444651
	.long	-1148001306
	.long	1838961686
	.long	893111920
	.long	1842459790
	.long	-1031093748
	.long	1845895601
	.long	858616463
	.long	1849403934
	.long	-537106535
	.long	1852918354
	.long	-1745124993
	.long	1856347367
	.long	2113561055
	.long	1859847201
	.long	-289637076
	.long	1863377556
	.long	711695478
	.long	1866799930
	.long	-1257864300
	.long	1870291464
	.long	1898189373
	.long	1873837381
	.long	-848488756
	.long	1877253270
	.long	-2134352769
	.long	1880736700
	.long	-797099569
	.long	1884297813
	.long	1151109187
	.long	1887707371
	.long	365144660
	.long	1891182886
	.long	-1691052823
	.long	1894740847
	.long	-1056908015
	.long	1898162213
	.long	826348630
	.long	1901629999
	.long	-40806037
	.long	1905178298
	.long	-562374685
	.long	1908617780
	.long	370773468
	.long	1912078018
	.long	-1684016813
	.long	1915616882
	.long	-1589381420
	.long	1919074055
	.long	-1986726775
	.long	1922526921
	.long	-335924821
	.long	1926056571
	.long	1937530635
	.long	1929531021
	.long	-799312179
	.long	1932976688
	.long	74601601
	.long	1936497341
	.long	583496912
	.long	1939988662
	.long	-1418112508
	.long	1943427299
	.long	-1772640634
	.long	1946939164
	.long	-571029484
	.long	1950446961
	.long	1433696792
	.long	1953878734
	.long	-355362658
	.long	1957382017
	.long	851640163
	.long	1960905905
	.long	2138292028
	.long	1964330973
	.long	-548360437
	.long	1967825876
	.long	194145639
	.long	1971365477
	.long	1316423872
	.long	1974783998
	.long	-501953807
	.long	1978270717
	.long	-1387462954
	.long	1981825662
	.long	1486896780
	.long	1985237790
	.long	-288862673
	.long	1988716517
	.long	1786405307
	.long	1992278495
	.long	-1567851243
	.long	1995692331
	.long	-1959814054
	.long	1999163254
	.long	771457904
	.long	2002715476
	.long	-1665322458
	.long	2006147604
	.long	-1007911248
	.long	2009610905
	.long	887594588
	.long	2013153600
	.long	554746617
	.long	2016603592
	.long	693433272
	.long	2020059450
	.long	-1280692059
	.long	2023592840
	.long	1883922023
	.long	2027060277
	.long	-866322943
	.long	2030508866
	.long	2138321794
	.long	2034033171
	.long	799580209
	.long	2037517644
	.long	999475261
	.long	2040959135
	.long	175602253
	.long	2044474567
	.long	1720364144
	.long	2047975676
	.long	-2144512116
	.long	2051410235
	.long	1614327151
	.long	2054917002
	.long	2082696293
	.long	2058434358
	.long	455886719
	.long	2061862148
	.long	569858398
	.long	2065360453
	.long	893032411
	.long	2068893675
	.long	42548690
	.long	2072314854
	.long	-2094297786
	.long	2075804895
	.long	-1308936116
	.long	2079353611
	.long	-1636170145
	.long	2082768334
	.long	1176012790
	.long	2086250306
	.long	1808749818
	.long	2089814153
	.long	-960288200
	.long	2093222571
	.long	-1200360249
	.long	2096696662
	.long	1720775160
	.long	2100252844
	.long	-1071999173
	.long	2103677547
	.long	-1339998966
	.long	2107143942
	.long	1546226764
	.long	2110690504
	.long	966391728
	.long	2114133245
	.long	-2013235812
	.long	2117592124
	.long	-1442802941
	.long	2121129291
	.long	1245731810
	.long	2124589647
	.long	483422938
	.long	2128041187
	.long	-469463152
	.long	2131569179
	.long	1854069178
	.long	2135046737
	.long	-903638999
	.long	2138491109
	.long	1017934899
	.long	2142010143
	.long	-2048145248
	.long	2145504499
	.long	0
	.long	1076101120
	.long	0
	.long	1079574528
	.long	0
	.long	1083129856
	.long	0
	.long	1086556160
	.long	0
	.long	1090021888
	.long	0
	.long	1093567616
	.long	0
	.long	1097011920
	.long	0
	.long	1100470148
	.long	0
	.long	1104006501
	.long	536870912
	.long	1107468383
	.long	-402653184
	.long	1110919286
	.long	-1577058304
	.long	1114446484
	.long	-448790528
	.long	1117925532
	.long	512753664
	.long	1121369284
	.long	640942080
	.long	1124887541
	.long	937459712
	.long	1128383353
	.globl	array
	.data
	.align 32
array:
	.quad	22250738585072014
	.quad	44501477170144028
	.quad	89002954340288055
	.quad	17800590868057611
	.quad	35601181736115222
	.quad	71202363472230444
	.quad	14240472694446089
	.quad	28480945388892178
	.quad	56961890777784355
	.quad	11392378155556871
	.quad	22784756311113742
	.quad	45569512622227484
	.quad	91139025244454969
	.quad	18227805048890994
	.quad	36455610097781987
	.quad	72911220195563975
	.quad	14582244039112795
	.quad	29164488078225590
	.quad	58328976156451180
	.quad	11665795231290236
	.quad	23331590462580472
	.quad	46663180925160944
	.quad	93326361850321888
	.quad	18665272370064378
	.quad	37330544740128755
	.quad	74661089480257510
	.quad	14932217896051502
	.quad	29864435792103004
	.quad	59728871584206008
	.quad	11945774316841202
	.quad	23891548633682403
	.quad	47783097267364807
	.quad	95566194534729613
	.quad	19113238906945923
	.quad	38226477813891845
	.quad	76452955627783691
	.quad	15290591125556738
	.quad	30581182251113476
	.quad	61162364502226952
	.quad	12232472900445390
	.quad	24464945800890781
	.quad	48929891601781562
	.quad	97859783203563124
	.quad	19571956640712625
	.quad	39143913281425250
	.quad	78287826562850499
	.quad	15657565312570100
	.quad	31315130625140200
	.quad	62630261250280399
	.quad	12526052250056080
	.quad	25052104500112160
	.quad	50104209000224319
	.quad	10020841800044864
	.quad	20041683600089728
	.quad	40083367200179456
	.quad	80166734400358911
	.quad	16033346880071782
	.quad	32066693760143564
	.quad	64133387520287129
	.quad	12826677504057426
	.quad	25653355008114852
	.quad	51306710016229703
	.quad	10261342003245941
	.quad	20522684006491881
	.quad	41045368012983762
	.quad	82090736025967525
	.quad	16418147205193505
	.quad	32836294410387010
	.quad	65672588820774020
	.quad	13134517764154804
	.quad	26269035528309608
	.quad	52538071056619216
	.quad	10507614211323843
	.quad	21015228422647686
	.quad	42030456845295373
	.quad	84060913690590746
	.quad	16812182738118149
	.quad	33624365476236298
	.quad	67248730952472596
	.quad	13449746190494519
	.quad	26899492380989039
	.quad	53798984761978077
	.quad	10759796952395615
	.quad	21519593904791231
	.quad	43039187809582462
	.quad	86078375619164923
	.quad	17215675123832985
	.quad	34431350247665969
	.quad	68862700495331939
	.quad	13772540099066388
	.quad	27545080198132776
	.quad	55090160396265551
	.quad	11018032079253110
	.quad	22036064158506220
	.quad	44072128317012441
	.quad	88144256634024882
	.quad	17628851326804976
	.quad	35257702653609953
	.quad	70515405307219905
	.quad	14103081061443981
	.quad	28206162122887962
	.quad	56412324245775924
	.quad	11282464849155185
	.quad	22564929698310370
	.quad	45129859396620739
	.quad	90259718793241479
	.quad	18051943758648296
	.quad	36103887517296592
	.quad	72207775034593183
	.quad	14441555006918637
	.quad	28883110013837273
	.quad	57766220027674546
	.quad	11553244005534909
	.quad	23106488011069819
	.quad	46212976022139637
	.quad	92425952044279274
	.quad	18485190408855855
	.quad	36970380817711710
	.quad	73940761635423419
	.quad	14788152327084684
	.quad	29576304654169368
	.quad	59152609308338736
	.quad	11830521861667747
	.quad	23661043723335494
	.quad	47322087446670988
	.quad	94644174893341977
	.quad	18928834978668395
	.quad	37857669957336791
	.quad	75715339914673582
	.quad	15143067982934716
	.quad	30286135965869433
	.quad	60572271931738865
	.quad	12114454386347773
	.quad	24228908772695546
	.quad	48457817545391092
	.quad	96915635090782184
	.quad	19383127018156437
	.quad	38766254036312874
	.quad	77532508072625747
	.quad	15506501614525149
	.quad	31013003229050299
	.quad	62026006458100598
	.quad	12405201291620120
	.quad	24810402583240239
	.quad	49620805166480478
	.quad	99241610332960957
	.quad	19848322066592191
	.quad	39696644133184383
	.quad	79393288266368765
	.quad	15878657653273753
	.quad	31757315306547506
	.quad	63514630613095012
	.quad	12702926122619002
	.quad	25405852245238005
	.quad	50811704490476010
	.quad	10162340898095202
	.quad	20324681796190404
	.quad	40649363592380808
	.quad	81298727184761616
	.quad	16259745436952323
	.quad	32519490873904646
	.quad	65038981747809293
	.quad	13007796349561859
	.quad	26015592699123717
	.quad	52031185398247434
	.quad	10406237079649487
	.quad	20812474159298974
	.quad	41624948318597947
	.quad	83249896637195895
	.quad	16649979327439179
	.quad	33299958654878358
	.quad	66599917309756716
	.quad	13319983461951343
	.quad	26639966923902686
	.quad	53279933847805373
	.quad	10655986769561075
	.quad	21311973539122149
	.quad	42623947078244298
	.quad	85247894156488596
	.quad	17049578831297719
	.quad	34099157662595438
	.quad	68198315325190877
	.quad	13639663065038175
	.quad	27279326130076351
	.quad	54558652260152701
	.quad	10911730452030540
	.quad	21823460904061081
	.quad	43646921808122161
	.quad	87293843616244322
	.quad	17458768723248864
	.quad	34917537446497729
	.quad	69835074892995458
	.quad	13967014978599092
	.quad	27934029957198183
	.quad	55868059914396366
	.quad	11173611982879273
	.quad	22347223965758547
	.quad	44694447931517093
	.quad	89388895863034186
	.quad	17877779172606837
	.quad	35755558345213674
	.quad	71511116690427349
	.quad	14302223338085470
	.quad	28604446676170940
	.quad	57208893352341879
	.quad	11441778670468376
	.quad	22883557340936752
	.quad	45767114681873503
	.quad	91534229363747007
	.quad	18306845872749401
	.quad	36613691745498803
	.quad	73227383490997605
	.quad	14645476698199521
	.quad	29290953396399042
	.quad	58581906792798084
	.quad	11716381358559617
	.quad	23432762717119234
	.quad	46865525434238467
	.quad	93731050868476935
	.quad	18746210173695387
	.quad	37492420347390774
	.quad	74984840694781548
	.quad	14996968138956310
	.quad	29993936277912619
	.quad	59987872555825238
	.quad	11997574511165048
	.quad	23995149022330095
	.quad	47990298044660191
	.quad	95980596089320381
	.quad	19196119217864076
	.quad	38392238435728152
	.quad	76784476871456305
	.quad	15356895374291261
	.quad	30713790748582522
	.quad	61427581497165044
	.quad	12285516299433009
	.quad	24571032598866018
	.quad	49142065197732035
	.quad	98284130395464070
	.quad	19656826079092814
	.quad	39313652158185628
	.quad	78627304316371256
	.quad	15725460863274251
	.quad	31450921726548502
	.quad	62901843453097005
	.quad	12580368690619401
	.quad	25160737381238802
	.quad	50321474762477604
	.quad	10064294952495521
	.quad	20128589904991042
	.quad	40257179809982083
	.quad	80514359619964166
	.quad	16102871923992833
	.quad	32205743847985667
	.quad	64411487695971333
	.quad	12882297539194267
	.quad	25764595078388533
	.quad	51529190156777066
	.quad	10305838031355413
	.quad	20611676062710827
	.quad	41223352125421653
	.quad	82446704250843306
	.quad	16489340850168661
	.quad	32978681700337323
	.quad	65957363400674645
	.quad	13191472680134929
	.quad	26382945360269858
	.quad	52765890720539716
	.quad	10553178144107943
	.quad	21106356288215886
	.quad	42212712576431773
	.quad	84425425152863546
	.quad	16885085030572709
	.quad	33770170061145418
	.quad	67540340122290837
	.quad	13508068024458167
	.quad	27016136048916335
	.quad	54032272097832669
	.quad	10806454419566534
	.quad	21612908839133068
	.quad	43225817678266135
	.quad	86451635356532271
	.quad	17290327071306454
	.quad	34580654142612908
	.quad	69161308285225817
	.quad	13832261657045163
	.quad	27664523314090327
	.quad	55329046628180653
	.quad	11065809325636131
	.quad	22131618651272261
	.quad	44263237302544523
	.quad	88526474605089045
	.quad	17705294921017809
	.quad	35410589842035618
	.quad	70821179684071236
	.quad	14164235936814247
	.quad	28328471873628494
	.quad	56656943747256989
	.quad	11331388749451398
	.quad	22662777498902796
	.quad	45325554997805591
	.quad	90651109995611182
	.quad	18130221999122236
	.quad	36260443998244473
	.quad	72520887996488946
	.quad	14504177599297789
	.quad	29008355198595578
	.quad	58016710397191157
	.quad	11603342079438231
	.quad	23206684158876463
	.quad	46413368317752925
	.quad	92826736635505851
	.quad	18565347327101170
	.quad	37130694654202340
	.quad	74261389308404681
	.quad	14852277861680936
	.quad	29704555723361872
	.quad	59409111446723744
	.quad	11881822289344749
	.quad	23763644578689498
	.quad	47527289157378996
	.quad	95054578314757991
	.quad	19010915662951598
	.quad	38021831325903196
	.quad	76043662651806393
	.quad	15208732530361279
	.quad	30417465060722557
	.quad	60834930121445114
	.quad	12166986024289023
	.quad	24333972048578046
	.quad	48667944097156091
	.quad	97335888194312183
	.quad	19467177638862437
	.quad	38934355277724873
	.quad	77868710555449746
	.quad	15573742111089949
	.quad	31147484222179899
	.quad	62294968444359797
	.quad	12458993688871959
	.quad	24917987377743919
	.quad	49835974755487838
	.quad	99671949510975675
	.quad	19934389902195135
	.quad	39868779804390270
	.quad	79737559608780540
	.quad	15947511921756108
	.quad	31895023843512216
	.quad	63790047687024432
	.quad	12758009537404886
	.quad	25516019074809773
	.quad	51032038149619546
	.quad	10206407629923909
	.quad	20412815259847818
	.quad	40825630519695637
	.quad	81651261039391273
	.quad	16330252207878255
	.quad	32660504415756509
	.quad	65321008831513019
	.quad	13064201766302604
	.quad	26128403532605207
	.quad	52256807065210415
	.quad	10451361413042083
	.quad	20902722826084166
	.quad	41805445652168332
	.quad	83610891304336664
	.quad	16722178260867333
	.quad	33444356521734666
	.quad	66888713043469331
	.quad	13377742608693866
	.quad	26755485217387732
	.quad	53510970434775465
	.quad	10702194086955093
	.quad	21404388173910186
	.quad	42808776347820372
	.quad	85617552695640744
	.quad	17123510539128149
	.quad	34247021078256297
	.quad	68494042156512595
	.quad	13698808431302519
	.quad	27397616862605038
	.quad	54795233725210076
	.quad	10959046745042015
	.quad	21918093490084030
	.quad	43836186980168061
	.quad	87672373960336122
	.quad	17534474792067224
	.quad	35068949584134449
	.quad	70137899168268897
	.quad	14027579833653779
	.quad	28055159667307559
	.quad	56110319334615118
	.quad	11222063866923024
	.quad	22444127733846047
	.quad	44888255467692094
	.quad	89776510935384189
	.quad	17955302187076838
	.quad	35910604374153675
	.quad	71821208748307351
	.quad	14364241749661470
	.quad	28728483499322940
	.quad	57456966998645881
	.quad	11491393399729176
	.quad	22982786799458352
	.quad	45965573598916705
	.quad	91931147197833409
	.quad	18386229439566682
	.quad	36772458879133364
	.quad	73544917758266727
	.quad	14708983551653345
	.quad	29417967103306691
	.quad	58835934206613382
	.quad	11767186841322676
	.quad	23534373682645353
	.quad	47068747365290705
	.quad	94137494730581411
	.quad	18827498946116282
	.quad	37654997892232564
	.quad	75309995784465129
	.quad	15061999156893026
	.quad	30123998313786051
	.quad	60247996627572103
	.quad	12049599325514421
	.quad	24099198651028841
	.quad	48198397302057682
	.quad	96396794604115365
	.quad	19279358920823073
	.quad	38558717841646146
	.quad	77117435683292292
	.quad	15423487136658458
	.quad	30846974273316917
	.quad	61693948546633833
	.quad	12338789709326767
	.quad	24677579418653533
	.quad	49355158837307067
	.quad	98710317674614133
	.quad	19742063534922827
	.quad	39484127069845653
	.quad	78968254139691307
	.quad	15793650827938261
	.quad	31587301655876523
	.quad	63174603311753045
	.quad	12634920662350609
	.quad	25269841324701218
	.quad	50539682649402436
	.quad	10107936529880487
	.quad	20215873059760975
	.quad	40431746119521949
	.quad	80863492239043898
	.quad	16172698447808780
	.quad	32345396895617559
	.quad	64690793791235119
	.quad	12938158758247024
	.quad	25876317516494047
	.quad	51752635032988095
	.quad	10350527006597619
	.quad	20701054013195238
	.quad	41402108026390476
	.quad	82804216052780952
	.quad	16560843210556190
	.quad	33121686421112381
	.quad	66243372842224761
	.quad	13248674568444952
	.quad	26497349136889905
	.quad	52994698273779809
	.quad	10598939654755962
	.quad	21197879309511924
	.quad	42395758619023847
	.quad	84791517238047695
	.quad	16958303447609539
	.quad	33916606895219078
	.quad	67833213790438156
	.quad	13566642758087631
	.quad	27133285516175262
	.quad	54266571032350524
	.quad	10853314206470105
	.quad	21706628412940210
	.quad	43413256825880420
	.quad	86826513651760839
	.quad	17365302730352168
	.quad	34730605460704336
	.quad	69461210921408671
	.quad	13892242184281734
	.quad	27784484368563469
	.quad	55568968737126937
	.quad	11113793747425387
	.quad	22227587494850775
	.quad	44455174989701550
	.quad	88910349979403099
	.quad	17782069995880620
	.quad	35564139991761240
	.quad	71128279983522479
	.quad	14225655996704496
	.quad	28451311993408992
	.quad	56902623986817984
	.quad	11380524797363597
	.quad	22761049594727193
	.quad	45522099189454387
	.quad	91044198378908774
	.quad	18208839675781755
	.quad	36417679351563509
	.quad	72835358703127019
	.quad	14567071740625404
	.quad	29134143481250808
	.quad	58268286962501615
	.quad	11653657392500323
	.quad	23307314785000646
	.quad	46614629570001292
	.quad	93229259140002584
	.quad	18645851828000517
	.quad	37291703656001034
	.quad	74583407312002067
	.quad	14916681462400413
	.quad	29833362924800827
	.quad	59666725849601654
	.quad	11933345169920331
	.quad	23866690339840662
	.quad	47733380679681323
	.quad	95466761359362646
	.quad	19093352271872529
	.quad	38186704543745059
	.quad	76373409087490117
	.quad	15274681817498023
	.quad	30549363634996047
	.quad	61098727269992094
	.quad	12219745453998419
	.quad	24439490907996837
	.quad	48878981815993675
	.quad	97757963631987350
	.quad	19551592726397470
	.quad	39103185452794940
	.quad	78206370905589880
	.quad	15641274181117976
	.quad	31282548362235952
	.quad	62565096724471904
	.quad	12513019344894381
	.quad	25026038689788762
	.quad	50052077379577523
	.quad	10010415475915505
	.quad	20020830951831009
	.quad	40041661903662018
	.quad	80083323807324037
	.quad	16016664761464807
	.quad	32033329522929615
	.quad	64066659045859230
	.quad	12813331809171846
	.quad	25626663618343692
	.quad	51253327236687384
	.quad	10250665447337477
	.quad	20501330894674953
	.quad	41002661789349907
	.quad	82005323578699814
	.quad	16401064715739963
	.quad	32802129431479926
	.quad	65604258862959851
	.quad	13120851772591970
	.quad	26241703545183940
	.quad	52483407090367881
	.quad	10496681418073576
	.quad	20993362836147152
	.quad	41986725672294305
	.quad	83973451344588609
	.quad	16794690268917722
	.quad	33589380537835444
	.quad	67178761075670888
	.quad	13435752215134178
	.quad	26871504430268355
	.quad	53743008860536710
	.quad	10748601772107342
	.quad	21497203544214684
	.quad	42994407088429368
	.quad	85988814176858736
	.quad	17197762835371747
	.quad	34395525670743494
	.quad	68791051341486989
	.quad	13758210268297398
	.quad	27516420536594796
	.quad	55032841073189591
	.quad	11006568214637918
	.quad	22013136429275836
	.quad	44026272858551673
	.quad	88052545717103346
	.quad	17610509143420669
	.quad	35221018286841338
	.quad	70442036573682677
	.quad	14088407314736535
	.quad	28176814629473071
	.quad	56353629258946141
	.quad	11270725851789228
	.quad	22541451703578456
	.quad	45082903407156913
	.quad	90165806814313826
	.quad	18033161362862765
	.quad	36066322725725530
	.quad	72132645451451061
	.quad	14426529090290212
	.quad	28853058180580424
	.quad	57706116361160849
	.quad	11541223272232170
	.quad	23082446544464339
	.quad	46164893088928679
	.quad	92329786177857358
	.quad	18465957235571472
	.quad	36931914471142943
	.quad	73863828942285886
	.quad	14772765788457177
	.quad	29545531576914354
	.quad	59091063153828709
	.quad	11818212630765742
	.quad	23636425261531484
	.quad	47272850523062967
	.quad	94545701046125934
	.quad	18909140209225187
	.quad	37818280418450374
	.quad	75636560836900748
	.quad	15127312167380150
	.quad	30254624334760299
	.quad	60509248669520598
	.quad	12101849733904120
	.quad	24203699467808239
	.quad	48407398935616478
	.quad	96814797871232957
	.quad	19362959574246591
	.quad	38725919148493183
	.quad	77451838296986365
	.quad	15490367659397273
	.quad	30980735318794546
	.quad	61961470637589092
	.quad	12392294127517818
	.quad	24784588255035637
	.quad	49569176510071274
	.quad	99138353020142548
	.quad	19827670604028510
	.quad	39655341208057019
	.quad	79310682416114038
	.quad	15862136483222808
	.quad	31724272966445615
	.quad	63448545932891231
	.quad	12689709186578246
	.quad	25379418373156492
	.quad	50758836746312984
	.quad	10151767349262597
	.quad	20303534698525194
	.quad	40607069397050388
	.quad	81214138794100775
	.quad	16242827758820155
	.quad	32485655517640310
	.quad	64971311035280620
	.quad	12994262207056124
	.quad	25988524414112248
	.quad	51977048828224496
	.quad	10395409765644899
	.quad	20790819531289798
	.quad	41581639062579597
	.quad	83163278125159194
	.quad	16632655625031839
	.quad	33265311250063677
	.quad	66530622500127355
	.quad	13306124500025471
	.quad	26612249000050942
	.quad	53224498000101884
	.quad	10644899600020377
	.quad	21289799200040754
	.quad	42579598400081507
	.quad	85159196800163014
	.quad	17031839360032603
	.quad	34063678720065206
	.quad	68127357440130412
	.quad	13625471488026082
	.quad	27250942976052165
	.quad	54501885952104329
	.quad	10900377190420866
	.quad	21800754380841732
	.quad	43601508761683463
	.quad	87203017523366927
	.quad	17440603504673385
	.quad	34881207009346771
	.quad	69762414018693541
	.quad	13952482803738708
	.quad	27904965607477417
	.quad	55809931214954833
	.quad	11161986242990967
	.quad	22323972485981933
	.quad	44647944971963866
	.quad	89295889943927733
	.quad	17859177988785547
	.quad	35718355977571093
	.quad	71436711955142186
	.quad	14287342391028437
	.quad	28574684782056875
	.quad	57149369564113749
	.quad	11429873912822750
	.quad	22859747825645500
	.quad	45719495651290999
	.quad	91438991302581999
	.quad	18287798260516400
	.quad	36575596521032799
	.quad	73151193042065599
	.quad	14630238608413120
	.quad	29260477216826240
	.quad	58520954433652479
	.quad	11704190886730496
	.quad	23408381773460992
	.quad	46816763546921983
	.quad	93633527093843967
	.quad	18726705418768793
	.quad	37453410837537587
	.quad	74906821675075173
	.quad	14981364335015035
	.quad	29962728670030069
	.quad	59925457340060139
	.quad	11985091468012028
	.quad	23970182936024055
	.quad	47940365872048111
	.quad	95880731744096222
	.quad	19176146348819244
	.quad	38352292697638489
	.quad	76704585395276977
	.quad	15340917079055395
	.quad	30681834158110791
	.quad	61363668316221582
	.quad	12272733663244316
	.quad	24545467326488633
	.quad	49090934652977266
	.quad	98181869305954531
	.quad	19636373861190906
	.quad	39272747722381812
	.quad	78545495444763625
	.quad	15709099088952725
	.quad	31418198177905450
	.quad	62836396355810900
	.quad	12567279271162180
	.quad	25134558542324360
	.quad	50269117084648720
	.quad	10053823416929744
	.quad	20107646833859488
	.quad	40215293667718976
	.quad	80430587335437952
	.quad	16086117467087590
	.quad	32172234934175181
	.quad	64344469868350361
	.quad	12868893973670072
	.quad	25737787947340145
	.quad	51475575894680289
	.quad	10295115178936058
	.quad	20590230357872116
	.quad	41180460715744231
	.quad	82360921431488463
	.quad	16472184286297693
	.quad	32944368572595385
	.quad	65888737145190770
	.quad	13177747429038154
	.quad	26355494858076308
	.quad	52710989716152616
	.quad	10542197943230523
	.quad	21084395886461046
	.quad	42168791772922093
	.quad	84337583545844186
	.quad	16867516709168837
	.quad	33735033418337674
	.quad	67470066836675349
	.quad	13494013367335070
	.quad	26988026734670139
	.quad	53976053469340279
	.quad	10795210693868056
	.quad	21590421387736112
	.quad	43180842775472223
	.quad	86361685550944446
	.quad	17272337110188889
	.quad	34544674220377779
	.quad	69089348440755557
	.quad	13817869688151111
	.quad	27635739376302223
	.quad	55271478752604446
	.quad	11054295750520889
	.quad	22108591501041778
	.quad	44217183002083556
	.quad	88434366004167113
	.quad	17686873200833423
	.quad	35373746401666845
	.quad	70747492803333690
	.quad	14149498560666738
	.quad	28298997121333476
	.quad	56597994242666952
	.quad	11319598848533390
	.quad	22639197697066781
	.quad	45278395394133562
	.quad	90556790788267124
	.quad	18111358157653425
	.quad	36222716315306849
	.quad	72445432630613699
	.quad	14489086526122740
	.quad	28978173052245480
	.quad	57956346104490959
	.quad	11591269220898192
	.quad	23182538441796384
	.quad	46365076883592767
	.quad	92730153767185535
	.quad	18546030753437107
	.quad	37092061506874214
	.quad	74184123013748428
	.quad	14836824602749686
	.quad	29673649205499371
	.quad	59347298410998742
	.quad	11869459682199748
	.quad	23738919364399497
	.quad	47477838728798994
	.quad	94955677457597987
	.quad	18991135491519597
	.quad	37982270983039195
	.quad	75964541966078390
	.quad	15192908393215678
	.quad	30385816786431356
	.quad	60771633572862712
	.quad	12154326714572542
	.quad	24308653429145085
	.quad	48617306858290170
	.quad	97234613716580339
	.quad	19446922743316068
	.quad	38893845486632136
	.quad	77787690973264271
	.quad	15557538194652854
	.quad	31115076389305709
	.quad	62230152778611417
	.quad	12446030555722283
	.quad	24892061111444567
	.quad	49784122222889134
	.quad	99568244445778267
	.quad	19913648889155653
	.quad	39827297778311307
	.quad	79654595556622614
	.quad	15930919111324523
	.quad	31861838222649046
	.quad	63723676445298091
	.quad	12744735289059618
	.quad	25489470578119236
	.quad	50978941156238473
	.quad	10195788231247695
	.quad	20391576462495389
	.quad	40783152924990778
	.quad	81566305849981557
	.quad	16313261169996311
	.quad	32626522339992623
	.quad	65253044679985245
	.quad	13050608935997049
	.quad	26101217871994098
	.quad	52202435743988196
	.quad	10440487148797639
	.quad	20880974297595278
	.quad	41761948595190557
	.quad	83523897190381114
	.quad	16704779438076223
	.quad	33409558876152446
	.quad	66819117752304891
	.quad	13363823550460978
	.quad	26727647100921956
	.quad	53455294201843913
	.quad	10691058840368783
	.quad	21382117680737565
	.quad	42764235361475130
	.quad	85528470722950261
	.quad	17105694144590052
	.quad	34211388289180104
	.quad	68422776578360209
	.quad	13684555315672042
	.quad	27369110631344083
	.quad	54738221262688167
	.quad	10947644252537633
	.quad	21895288505075267
	.quad	43790577010150533
	.quad	87581154020301067
	.quad	17516230804060213
	.quad	35032461608120427
	.quad	70064923216240854
	.quad	14012984643248171
	.quad	28025969286496341
	.quad	56051938572992683
	.quad	11210387714598537
	.quad	22420775429197073
	.quad	44841550858394146
	.quad	89683101716788293
	.quad	17936620343357659
	.quad	35873240686715317
	.quad	71746481373430634
	.quad	14349296274686127
	.quad	28698592549372254
	.quad	57397185098744507
	.quad	11479437019748901
	.quad	22958874039497803
	.quad	45917748078995606
	.quad	91835496157991212
	.quad	18367099231598242
	.quad	36734198463196485
	.quad	73468396926392969
	.quad	14693679385278594
	.quad	29387358770557188
	.quad	58774717541114375
	.quad	11754943508222875
	.quad	23509887016445750
	.quad	47019774032891500
	.quad	94039548065783001
	.quad	18807909613156600
	.quad	37615819226313200
	.quad	75231638452626401
	.quad	15046327690525280
	.quad	30092655381050560
	.quad	60185310762101120
	.quad	12037062152420224
	.quad	24074124304840448
	.quad	48148248609680896
	.quad	96296497219361793
	.quad	19259299443872359
	.quad	38518598887744717
	.quad	77037197775489434
	.quad	15407439555097887
	.quad	30814879110195774
	.quad	61629758220391547
	.quad	12325951644078309
	.quad	24651903288156619
	.quad	49303806576313238
	.quad	98607613152626476
	.quad	19721522630525295
	.quad	39443045261050590
	.quad	78886090522101181
	.quad	15777218104420236
	.quad	31554436208840472
	.quad	63108872417680944
	.quad	12621774483536189
	.quad	25243548967072378
	.quad	50487097934144756
	.quad	10097419586828951
	.quad	20194839173657902
	.quad	40389678347315804
	.quad	80779356694631609
	.quad	16155871338926322
	.quad	32311742677852644
	.quad	64623485355705287
	.quad	12924697071141057
	.quad	25849394142282115
	.quad	51698788284564230
	.quad	10339757656912846
	.quad	20679515313825692
	.quad	41359030627651384
	.quad	82718061255302767
	.quad	16543612251060553
	.quad	33087224502121107
	.quad	66174449004242214
	.quad	13234889800848443
	.quad	26469779601696886
	.quad	52939559203393771
	.quad	10587911840678754
	.quad	21175823681357508
	.quad	42351647362715017
	.quad	84703294725430034
	.quad	16940658945086007
	.quad	33881317890172014
	.quad	67762635780344027
	.quad	13552527156068805
	.quad	27105054312137611
	.quad	54210108624275222
	.quad	10842021724855044
	.quad	21684043449710089
	.quad	43368086899420177
	.quad	86736173798840355
	.quad	17347234759768071
	.quad	34694469519536142
	.quad	69388939039072284
	.quad	13877787807814457
	.quad	27755575615628914
	.quad	55511151231257827
	.quad	11102230246251565
	.quad	22204460492503131
	.quad	44408920985006262
	.quad	88817841970012523
	.quad	17763568394002505
	.quad	35527136788005009
	.quad	71054273576010019
	.quad	14210854715202004
	.quad	28421709430404007
	.quad	56843418860808015
	.quad	11368683772161603
	.quad	22737367544323206
	.quad	45474735088646412
	.quad	90949470177292824
	.quad	18189894035458565
	.quad	36379788070917130
	.quad	72759576141834259
	.quad	14551915228366852
	.quad	29103830456733704
	.quad	58207660913467407
	.quad	11641532182693481
	.quad	23283064365386963
	.quad	46566128730773926
	.quad	93132257461547852
	.quad	18626451492309570
	.quad	37252902984619141
	.quad	74505805969238281
	.quad	14901161193847656
	.quad	29802322387695312
	.quad	59604644775390625
	.quad	11920928955078125
	.quad	23841857910156250
	.quad	47683715820312500
	.quad	95367431640625000
	.quad	19073486328125000
	.quad	38146972656250000
	.quad	76293945312500000
	.quad	15258789062500000
	.quad	30517578125000000
	.quad	61035156250000000
	.quad	12207031250000000
	.quad	24414062500000000
	.quad	48828125000000000
	.quad	97656250000000000
	.quad	19531250000000000
	.quad	39062500000000000
	.quad	78125000000000000
	.quad	15625000000000000
	.quad	31250000000000000
	.quad	62500000000000000
	.quad	12500000000000000
	.quad	25000000000000000
	.quad	50000000000000000
	.quad	10000000000000000
	.quad	20000000000000000
	.quad	40000000000000000
	.quad	80000000000000000
	.quad	16000000000000000
	.quad	32000000000000000
	.quad	64000000000000000
	.quad	12800000000000000
	.quad	25600000000000000
	.quad	51200000000000000
	.quad	10240000000000000
	.quad	20480000000000000
	.quad	40960000000000000
	.quad	81920000000000000
	.quad	16384000000000000
	.quad	32768000000000000
	.quad	65536000000000000
	.quad	13107200000000000
	.quad	26214400000000000
	.quad	52428800000000000
	.quad	10485760000000000
	.quad	20971520000000000
	.quad	41943040000000000
	.quad	83886080000000000
	.quad	16777216000000000
	.quad	33554432000000000
	.quad	67108864000000000
	.quad	13421772800000000
	.quad	26843545600000000
	.quad	53687091200000000
	.quad	10737418240000000
	.quad	21474836480000000
	.quad	42949672960000000
	.quad	85899345920000000
	.quad	17179869184000000
	.quad	34359738368000000
	.quad	68719476736000000
	.quad	13743895347200000
	.quad	27487790694400000
	.quad	54975581388800000
	.quad	10995116277760000
	.quad	21990232555520000
	.quad	43980465111040000
	.quad	87960930222080000
	.quad	17592186044416000
	.quad	35184372088832000
	.quad	70368744177664000
	.quad	14073748835532800
	.quad	28147497671065600
	.quad	56294995342131200
	.quad	11258999068426240
	.quad	22517998136852480
	.quad	45035996273704960
	.quad	90071992547409920
	.quad	18014398509481984
	.quad	36028797018963968
	.quad	72057594037927936
	.quad	14411518807585587
	.quad	28823037615171174
	.quad	57646075230342349
	.quad	11529215046068470
	.quad	23058430092136940
	.quad	46116860184273879
	.quad	92233720368547758
	.quad	18446744073709552
	.quad	36893488147419103
	.quad	73786976294838206
	.quad	14757395258967641
	.quad	29514790517935283
	.quad	59029581035870565
	.quad	11805916207174113
	.quad	23611832414348226
	.quad	47223664828696452
	.quad	94447329657392904
	.quad	18889465931478581
	.quad	37778931862957162
	.quad	75557863725914323
	.quad	15111572745182865
	.quad	30223145490365729
	.quad	60446290980731459
	.quad	12089258196146292
	.quad	24178516392292583
	.quad	48357032784585167
	.quad	96714065569170334
	.quad	19342813113834067
	.quad	38685626227668134
	.quad	77371252455336267
	.quad	15474250491067253
	.quad	30948500982134507
	.quad	61897001964269014
	.quad	12379400392853803
	.quad	24758800785707605
	.quad	49517601571415211
	.quad	99035203142830422
	.quad	19807040628566084
	.quad	39614081257132169
	.quad	79228162514264338
	.quad	15845632502852868
	.quad	31691265005705735
	.quad	63382530011411470
	.quad	12676506002282294
	.quad	25353012004564588
	.quad	50706024009129176
	.quad	10141204801825835
	.quad	20282409603651670
	.quad	40564819207303341
	.quad	81129638414606682
	.quad	16225927682921336
	.quad	32451855365842673
	.quad	64903710731685345
	.quad	12980742146337069
	.quad	25961484292674138
	.quad	51922968585348276
	.quad	10384593717069655
	.quad	20769187434139311
	.quad	41538374868278621
	.quad	83076749736557242
	.quad	16615349947311448
	.quad	33230699894622897
	.quad	66461399789245794
	.quad	13292279957849159
	.quad	26584559915698317
	.quad	53169119831396635
	.quad	10633823966279327
	.quad	21267647932558654
	.quad	42535295865117308
	.quad	85070591730234616
	.quad	17014118346046923
	.quad	34028236692093846
	.quad	68056473384187693
	.quad	13611294676837539
	.quad	27222589353675077
	.quad	54445178707350154
	.quad	10889035741470031
	.quad	21778071482940062
	.quad	43556142965880123
	.quad	87112285931760247
	.quad	17422457186352049
	.quad	34844914372704099
	.quad	69689828745408197
	.quad	13937965749081639
	.quad	27875931498163279
	.quad	55751862996326558
	.quad	11150372599265312
	.quad	22300745198530623
	.quad	44601490397061246
	.quad	89202980794122493
	.quad	17840596158824499
	.quad	35681192317648997
	.quad	71362384635297994
	.quad	14272476927059599
	.quad	28544953854119198
	.quad	57089907708238395
	.quad	11417981541647679
	.quad	22835963083295358
	.quad	45671926166590716
	.quad	91343852333181432
	.quad	18268770466636286
	.quad	36537540933272573
	.quad	73075081866545146
	.quad	14615016373309029
	.quad	29230032746618058
	.quad	58460065493236117
	.quad	11692013098647223
	.quad	23384026197294447
	.quad	46768052394588893
	.quad	93536104789177787
	.quad	18707220957835557
	.quad	37414441915671115
	.quad	74828883831342229
	.quad	14965776766268446
	.quad	29931553532536892
	.quad	59863107065073784
	.quad	11972621413014757
	.quad	23945242826029513
	.quad	47890485652059027
	.quad	95780971304118054
	.quad	19156194260823611
	.quad	38312388521647221
	.quad	76624777043294443
	.quad	15324955408658889
	.quad	30649910817317777
	.quad	61299821634635554
	.quad	12259964326927111
	.quad	24519928653854222
	.quad	49039857307708443
	.quad	98079714615416887
	.quad	19615942923083377
	.quad	39231885846166755
	.quad	78463771692333510
	.quad	15692754338466702
	.quad	31385508676933404
	.quad	62771017353866808
	.quad	12554203470773362
	.quad	25108406941546723
	.quad	50216813883093446
	.quad	10043362776618689
	.quad	20086725553237378
	.quad	40173451106474757
	.quad	80346902212949514
	.quad	16069380442589903
	.quad	32138760885179806
	.quad	64277521770359611
	.quad	12855504354071922
	.quad	25711008708143844
	.quad	51422017416287689
	.quad	10284403483257538
	.quad	20568806966515076
	.quad	41137613933030151
	.quad	82275227866060302
	.quad	16455045573212060
	.quad	32910091146424121
	.quad	65820182292848242
	.quad	13164036458569648
	.quad	26328072917139297
	.quad	52656145834278593
	.quad	10531229166855719
	.quad	21062458333711437
	.quad	42124916667422875
	.quad	84249833334845749
	.quad	16849966666969150
	.quad	33699933333938300
	.quad	67399866667876599
	.quad	13479973333575320
	.quad	26959946667150640
	.quad	53919893334301280
	.quad	10783978666860256
	.quad	21567957333720512
	.quad	43135914667441024
	.quad	86271829334882047
	.quad	17254365866976409
	.quad	34508731733952819
	.quad	69017463467905638
	.quad	13803492693581128
	.quad	27606985387162255
	.quad	55213970774324510
	.quad	11042794154864902
	.quad	22085588309729804
	.quad	44171176619459608
	.quad	88342353238919216
	.quad	17668470647783843
	.quad	35336941295567687
	.quad	70673882591135373
	.quad	14134776518227075
	.quad	28269553036454149
	.quad	56539106072908299
	.quad	11307821214581660
	.quad	22615642429163319
	.quad	45231284858326639
	.quad	90462569716653278
	.quad	18092513943330656
	.quad	36185027886661311
	.quad	72370055773322622
	.quad	14474011154664524
	.quad	28948022309329049
	.quad	57896044618658098
	.quad	11579208923731620
	.quad	23158417847463239
	.quad	46316835694926478
	.quad	92633671389852956
	.quad	18526734277970591
	.quad	37053468555941183
	.quad	74106937111882365
	.quad	14821387422376473
	.quad	29642774844752946
	.quad	59285549689505892
	.quad	11857109937901178
	.quad	23714219875802357
	.quad	47428439751604714
	.quad	94856879503209427
	.quad	18971375900641885
	.quad	37942751801283771
	.quad	75885503602567542
	.quad	15177100720513508
	.quad	30354201441027017
	.quad	60708402882054033
	.quad	12141680576410807
	.quad	24283361152821613
	.quad	48566722305643227
	.quad	97133444611286454
	.quad	19426688922257291
	.quad	38853377844514581
	.quad	77706755689029163
	.quad	15541351137805833
	.quad	31082702275611665
	.quad	62165404551223330
	.quad	12433080910244666
	.quad	24866161820489332
	.quad	49732323640978664
	.quad	99464647281957328
	.quad	19892929456391466
	.quad	39785858912782931
	.quad	79571717825565863
	.quad	15914343565113173
	.quad	31828687130226345
	.quad	63657374260452690
	.quad	12731474852090538
	.quad	25462949704181076
	.quad	50925899408362152
	.quad	10185179881672430
	.quad	20370359763344861
	.quad	40740719526689722
	.quad	81481439053379443
	.quad	16296287810675889
	.quad	32592575621351777
	.quad	65185151242703555
	.quad	13037030248540711
	.quad	26074060497081422
	.quad	52148120994162844
	.quad	10429624198832569
	.quad	20859248397665138
	.quad	41718496795330275
	.quad	83436993590660550
	.quad	16687398718132110
	.quad	33374797436264220
	.quad	66749594872528440
	.quad	13349918974505688
	.quad	26699837949011376
	.quad	53399675898022752
	.quad	10679935179604550
	.quad	21359870359209101
	.quad	42719740718418202
	.quad	85439481436836403
	.quad	17087896287367281
	.quad	34175792574734561
	.quad	68351585149469123
	.quad	13670317029893825
	.quad	27340634059787649
	.quad	54681268119575298
	.quad	10936253623915060
	.quad	21872507247830119
	.quad	43745014495660238
	.quad	87490028991320477
	.quad	17498005798264095
	.quad	34996011596528191
	.quad	69992023193056382
	.quad	13998404638611276
	.quad	27996809277222553
	.quad	55993618554445105
	.quad	11198723710889021
	.quad	22397447421778042
	.quad	44794894843556084
	.quad	89589789687112168
	.quad	17917957937422434
	.quad	35835915874844867
	.quad	71671831749689735
	.quad	14334366349937947
	.quad	28668732699875894
	.quad	57337465399751788
	.quad	11467493079950358
	.quad	22934986159900715
	.quad	45869972319801430
	.quad	91739944639602860
	.quad	18347988927920572
	.quad	36695977855841144
	.quad	73391955711682288
	.quad	14678391142336458
	.quad	29356782284672915
	.quad	58713564569345831
	.quad	11742712913869166
	.quad	23485425827738332
	.quad	46970851655476665
	.quad	93941703310953329
	.quad	18788340662190666
	.quad	37576681324381332
	.quad	75153362648762663
	.quad	15030672529752533
	.quad	30061345059505065
	.quad	60122690119010131
	.quad	12024538023802026
	.quad	24049076047604052
	.quad	48098152095208105
	.quad	96196304190416209
	.quad	19239260838083242
	.quad	38478521676166484
	.quad	76957043352332967
	.quad	15391408670466593
	.quad	30782817340933187
	.quad	61565634681866374
	.quad	12313126936373275
	.quad	24626253872746550
	.quad	49252507745493099
	.quad	98505015490986198
	.quad	19701003098197240
	.quad	39402006196394479
	.quad	78804012392788958
	.quad	15760802478557792
	.quad	31521604957115583
	.quad	63043209914231167
	.quad	12608641982846233
	.quad	25217283965692467
	.quad	50434567931384933
	.quad	10086913586276987
	.quad	20173827172553973
	.quad	40347654345107947
	.quad	80695308690215893
	.quad	16139061738043179
	.quad	32278123476086357
	.quad	64556246952172715
	.quad	12911249390434543
	.quad	25822498780869086
	.quad	51644997561738172
	.quad	10328999512347634
	.quad	20657999024695269
	.quad	41315998049390537
	.quad	82631996098781075
	.quad	16526399219756215
	.quad	33052798439512430
	.quad	66105596879024860
	.quad	13221119375804972
	.quad	26442238751609944
	.quad	52884477503219888
	.quad	10576895500643978
	.quad	21153791001287955
	.quad	42307582002575910
	.quad	84615164005151821
	.quad	16923032801030364
	.quad	33846065602060728
	.quad	67692131204121457
	.quad	13538426240824291
	.quad	27076852481648583
	.quad	54153704963297165
	.quad	10830740992659433
	.quad	21661481985318866
	.quad	43322963970637732
	.quad	86645927941275464
	.quad	17329185588255093
	.quad	34658371176510186
	.quad	69316742353020371
	.quad	13863348470604074
	.quad	27726696941208149
	.quad	55453393882416297
	.quad	11090678776483259
	.quad	22181357552966519
	.quad	44362715105933038
	.quad	88725430211866076
	.quad	17745086042373215
	.quad	35490172084746430
	.quad	70980344169492860
	.quad	14196068833898572
	.quad	28392137667797144
	.quad	56784275335594288
	.quad	11356855067118858
	.quad	22713710134237715
	.quad	45427420268475431
	.quad	90854840536950861
	.quad	18170968107390172
	.quad	36341936214780345
	.quad	72683872429560689
	.quad	14536774485912138
	.quad	29073548971824276
	.quad	58147097943648551
	.quad	11629419588729710
	.quad	23258839177459420
	.quad	46517678354918841
	.quad	93035356709837682
	.quad	18607071341967536
	.quad	37214142683935073
	.quad	74428285367870146
	.quad	14885657073574029
	.quad	29771314147148058
	.quad	59542628294296116
	.quad	11908525658859223
	.quad	23817051317718447
	.quad	47634102635436893
	.quad	95268205270873786
	.quad	19053641054174757
	.quad	38107282108349515
	.quad	76214564216699029
	.quad	15242912843339806
	.quad	30485825686679612
	.quad	60971651373359223
	.quad	12194330274671845
	.quad	24388660549343689
	.quad	48777321098687379
	.quad	97554642197374757
	.quad	19510928439474951
	.quad	39021856878949903
	.quad	78043713757899806
	.quad	15608742751579961
	.quad	31217485503159922
	.quad	62434971006319845
	.quad	12486994201263969
	.quad	24973988402527938
	.quad	49947976805055876
	.quad	99895953610111751
	.quad	19979190722022350
	.quad	39958381444044701
	.quad	79916762888089401
	.quad	15983352577617880
	.quad	31966705155235760
	.quad	63933410310471521
	.quad	12786682062094304
	.quad	25573364124188608
	.quad	51146728248377217
	.quad	10229345649675443
	.quad	20458691299350887
	.quad	40917382598701773
	.quad	81834765197403547
	.quad	16366953039480709
	.quad	32733906078961419
	.quad	65467812157922837
	.quad	13093562431584567
	.quad	26187124863169135
	.quad	52374249726338270
	.quad	10474849945267654
	.quad	20949699890535308
	.quad	41899399781070616
	.quad	83798799562141232
	.quad	16759759912428246
	.quad	33519519824856493
	.quad	67039039649712985
	.quad	13407807929942597
	.quad	26815615859885194
	.quad	53631231719770388
	.quad	10726246343954078
	.quad	21452492687908155
	.quad	42904985375816311
	.quad	85809970751632621
	.quad	17161994150326524
	.quad	34323988300653049
	.quad	68647976601306097
	.quad	13729595320261219
	.quad	27459190640522439
	.quad	54918381281044878
	.quad	10983676256208976
	.quad	21967352512417951
	.quad	43934705024835902
	.quad	87869410049671804
	.quad	17573882009934361
	.quad	35147764019868722
	.quad	70295528039737443
	.quad	14059105607947489
	.quad	28118211215894977
	.quad	56236422431789955
	.quad	11247284486357991
	.quad	22494568972715982
	.quad	44989137945431964
	.quad	89978275890863928
	.quad	17995655178172786
	.quad	35991310356345571
	.quad	71982620712691142
	.quad	14396524142538228
	.quad	28793048285076457
	.quad	57586096570152914
	.quad	11517219314030583
	.quad	23034438628061165
	.quad	46068877256122331
	.quad	92137754512244662
	.quad	18427550902448932
	.quad	36855101804897865
	.quad	73710203609795730
	.quad	14742040721959146
	.quad	29484081443918292
	.quad	58968162887836584
	.quad	11793632577567317
	.quad	23587265155134633
	.quad	47174530310269267
	.quad	94349060620538534
	.quad	18869812124107707
	.quad	37739624248215414
	.quad	75479248496430827
	.quad	15095849699286165
	.quad	30191699398572331
	.quad	60383398797144662
	.quad	12076679759428932
	.quad	24153359518857865
	.quad	48306719037715729
	.quad	96613438075431459
	.quad	19322687615086292
	.quad	38645375230172583
	.quad	77290750460345167
	.quad	15458150092069033
	.quad	30916300184138067
	.quad	61832600368276134
	.quad	12366520073655227
	.quad	24733040147310453
	.quad	49466080294620907
	.quad	98932160589241814
	.quad	19786432117848363
	.quad	39572864235696725
	.quad	79145728471393451
	.quad	15829145694278690
	.quad	31658291388557380
	.quad	63316582777114761
	.quad	12663316555422952
	.quad	25326633110845904
	.quad	50653266221691809
	.quad	10130653244338362
	.quad	20261306488676723
	.quad	40522612977353447
	.quad	81045225954706894
	.quad	16209045190941379
	.quad	32418090381882757
	.quad	64836180763765515
	.quad	12967236152753103
	.quad	25934472305506206
	.quad	51868944611012412
	.quad	10373788922202482
	.quad	20747577844404965
	.quad	41495155688809930
	.quad	82990311377619859
	.quad	16598062275523972
	.quad	33196124551047944
	.quad	66392249102095887
	.quad	13278449820419177
	.quad	26556899640838355
	.quad	53113799281676710
	.quad	10622759856335342
	.quad	21245519712670684
	.quad	42491039425341368
	.quad	84982078850682736
	.quad	16996415770136547
	.quad	33992831540273094
	.quad	67985663080546189
	.quad	13597132616109238
	.quad	27194265232218475
	.quad	54388530464436951
	.quad	10877706092887390
	.quad	21755412185774780
	.quad	43510824371549561
	.quad	87021648743099121
	.quad	17404329748619824
	.quad	34808659497239649
	.quad	69617318994479297
	.quad	13923463798895859
	.quad	27846927597791719
	.quad	55693855195583438
	.quad	11138771039116688
	.quad	22277542078233375
	.quad	44555084156466750
	.quad	89110168312933500
	.quad	17822033662586700
	.quad	35644067325173400
	.quad	71288134650346800
	.quad	14257626930069360
	.quad	28515253860138720
	.quad	57030507720277440
	.quad	11406101544055488
	.quad	22812203088110976
	.quad	45624406176221952
	.quad	91248812352443904
	.quad	18249762470488781
	.quad	36499524940977562
	.quad	72999049881955123
	.quad	14599809976391025
	.quad	29199619952782049
	.quad	58399239905564099
	.quad	11679847981112820
	.quad	23359695962225640
	.quad	46719391924451279
	.quad	93438783848902558
	.quad	18687756769780512
	.quad	37375513539561023
	.quad	74751027079122046
	.quad	14950205415824409
	.quad	29900410831648819
	.quad	59800821663297637
	.quad	11960164332659527
	.quad	23920328665319055
	.quad	47840657330638110
	.quad	95681314661276219
	.quad	19136262932255244
	.quad	38272525864510488
	.quad	76545051729020976
	.quad	15309010345804195
	.quad	30618020691608390
	.quad	61236041383216780
	.quad	12247208276643356
	.quad	24494416553286712
	.quad	48988833106573424
	.quad	97977666213146849
	.quad	19595533242629370
	.quad	39191066485258739
	.quad	78382132970517479
	.quad	15676426594103496
	.quad	31352853188206992
	.quad	62705706376413983
	.quad	12541141275282797
	.quad	25082282550565593
	.quad	50164565101131187
	.quad	10032913020226237
	.quad	20065826040452475
	.quad	40131652080904949
	.quad	80263304161809898
	.quad	16052660832361980
	.quad	32105321664723959
	.quad	64210643329447919
	.quad	12842128665889584
	.quad	25684257331779168
	.quad	51368514663558335
	.quad	10273702932711667
	.quad	20547405865423334
	.quad	41094811730846668
	.quad	82189623461693336
	.quad	16437924692338667
	.quad	32875849384677334
	.quad	65751698769354669
	.quad	13150339753870934
	.quad	26300679507741868
	.quad	52601359015483735
	.quad	10520271803096747
	.quad	21040543606193494
	.quad	42081087212386988
	.quad	84162174424773976
	.quad	9004289700580954565
	.quad	12420144738405671
	.quad	24840289476811343
	.quad	49680578953622686
	.quad	99361157907245372
	.quad	19872231581449074
	.quad	39744463162898149
	.quad	79488926325796297
	.quad	15897785265159259
	.quad	31795570530318519
	.quad	63591141060637038
	.quad	12718228212127408
	.quad	25436456424254815
	.quad	50872912848509630
	.quad	10174582569701926
	.quad	20349165139403852
	.quad	40698330278807704
	.quad	81396660557615409
	.quad	16279332111523082
	.quad	32558664223046163
	.quad	65117328446092327
	.quad	13023465689218465
	.quad	26046931378436931
	.quad	52093862756873862
	.quad	10418772551374772
	.quad	20837545102749545
	.quad	41675090205499089
	.quad	83350180410998178
	.quad	16670036082199636
	.quad	33340072164399271
	.quad	66680144328798543
	.quad	13336028865759709
	.quad	26672057731519417
	.quad	53344115463038834
	.quad	10668823092607767
	.quad	21337646185215534
	.quad	42675292370431067
	.quad	85350584740862135
	.quad	17070116948172427
	.quad	34140233896344854
	.quad	68280467792689708
	.quad	13656093558537942
	.quad	27312187117075883
	.quad	54624374234151766
	.quad	10924874846830353
	.quad	21849749693660706
	.quad	43699499387321413
	.quad	87398998774642826
	.quad	17479799754928565
	.quad	34959599509857130
	.quad	69919199019714261
	.quad	13983839803942852
	.quad	27967679607885704
	.quad	55935359215771409
	.quad	11187071843154282
	.quad	22374143686308563
	.quad	44748287372617127
	.quad	89496574745234254
	.quad	17899314949046851
	.quad	35798629898093702
	.quad	71597259796187403
	.quad	14319451959237481
	.quad	28638903918474961
	.quad	57277807836949922
	.quad	11455561567389984
	.quad	22911123134779969
	.quad	45822246269559938
	.quad	91644492539119876
	.quad	18328898507823975
	.quad	36657797015647950
	.quad	73315594031295901
	.quad	14663118806259180
	.quad	29326237612518360
	.quad	58652475225036721
	.quad	11730495045007344
	.quad	23460990090014688
	.quad	46921980180029376
	.quad	93843960360058753
	.quad	18768792072011751
	.quad	37537584144023501
	.quad	75075168288047002
	.quad	15015033657609400
	.quad	30030067315218801
	.quad	60060134630437602
	.quad	12012026926087520
	.quad	24024053852175041
	.quad	48048107704350081
	.quad	96096215408700163
	.quad	19219243081740033
	.quad	38438486163480065
	.quad	76876972326960130
	.quad	15375394465392026
	.quad	30750788930784052
	.quad	61501577861568104
	.quad	12300315572313621
	.quad	24600631144627242
	.quad	49201262289254483
	.quad	98402524578508967
	.quad	19680504915701793
	.quad	39361009831403587
	.quad	78722019662807173
	.quad	15744403932561435
	.quad	31488807865122869
	.quad	62977615730245739
	.quad	12595523146049148
	.quad	25191046292098296
	.quad	50382092584196591
	.quad	10076418516839318
	.quad	20152837033678636
	.quad	40305674067357273
	.quad	80611348134714546
	.quad	16122269626942909
	.quad	32244539253885818
	.quad	64489078507771637
	.quad	12897815701554327
	.quad	25795631403108655
	.quad	51591262806217309
	.quad	10318252561243462
	.quad	20636505122486924
	.quad	41273010244973847
	.quad	82546020489947695
	.quad	16509204097989539
	.quad	33018408195979078
	.quad	66036816391958156
	.quad	13207363278391631
	.quad	26414726556783262
	.quad	52829453113566525
	.quad	10565890622713305
	.quad	21131781245426610
	.quad	42263562490853220
	.quad	84527124981706439
	.quad	16905424996341288
	.quad	33810849992682576
	.quad	67621699985365152
	.quad	13524339997073030
	.quad	27048679994146061
	.quad	54097359988292121
	.quad	10819471997658424
	.quad	21638943995316848
	.quad	43277887990633697
	.quad	86555775981267394
	.quad	17311155196253479
	.quad	34622310392506958
	.quad	69244620785013915
	.quad	13848924157002783
	.quad	27697848314005566
	.quad	55395696628011132
	.quad	11079139325602226
	.quad	22158278651204453
	.quad	44316557302408906
	.quad	88633114604817811
	.quad	17726622920963562
	.quad	35453245841927125
	.quad	70906491683854249
	.quad	14181298336770850
	.quad	28362596673541700
	.quad	56725193347083399
	.quad	11345038669416680
	.quad	22690077338833360
	.quad	45380154677666719
	.quad	90760309355333439
	.quad	18152061871066688
	.quad	36304123742133376
	.quad	72608247484266751
	.quad	14521649496853350
	.quad	29043298993706700
	.quad	58086597987413401
	.quad	11617319597482680
	.quad	23234639194965360
	.quad	46469278389930721
	.quad	92938556779861441
	.quad	18587711355972288
	.quad	37175422711944577
	.quad	74350845423889153
	.quad	14870169084777831
	.quad	29740338169555661
	.quad	59480676339111323
	.quad	11896135267822265
	.quad	23792270535644529
	.quad	47584541071289058
	.quad	95169082142578116
	.quad	19033816428515623
	.quad	38067632857031246
	.quad	76135265714062493
	.quad	15227053142812499
	.quad	30454106285624997
	.quad	60908212571249994
	.quad	12181642514249999
	.quad	24363285028499998
	.quad	48726570056999995
	.quad	97453140113999991
	.quad	19490628022799998
	.quad	38981256045599996
	.quad	77962512091199993
	.quad	15592502418239999
	.quad	31185004836479997
	.quad	62370009672959994
	.quad	12474001934591999
	.quad	24948003869183998
	.quad	49896007738367995
	.quad	99792015476735991
	.quad	19958403095347198
	.quad	39916806190694396
	.quad	79833612381388792
	.quad	15966722476277758
	.quad	31933444952555517
	.quad	63866889905111034
	.quad	12773377981022207
	.quad	25546755962044414
	.quad	51093511924088827
	.quad	10218702384817765
	.quad	20437404769635531
	.quad	40874809539271062
	.quad	81749619078542123
	.quad	16349923815708425
	.quad	32699847631416849
	.quad	65399695262833699
	.quad	13079939052566740
	.quad	26159878105133480
	.quad	52319756210266959
	.quad	10463951242053392
	.quad	20927902484106784
	.quad	41855804968213567
	.quad	83711609936427134
	.quad	16742321987285427
	.quad	33484643974570854
	.quad	66969287949141708
	.quad	13393857589828342
	.quad	26787715179656683
	.quad	53575430359313366
	.quad	10715086071862673
	.quad	21430172143725346
	.quad	42860344287450693
	.quad	85720688574901386
	.quad	17144137714980277
	.quad	34288275429960554
	.quad	68576550859921109
	.quad	13715310171984222
	.quad	27430620343968443
	.quad	54861240687936887
	.quad	10972248137587377
	.quad	21944496275174755
	.quad	43888992550349509
	.quad	87777985100699019
	.quad	17555597020139804
	.quad	35111194040279608
	.quad	70222388080559215
	.quad	14044477616111843
	.quad	28088955232223686
	.quad	56177910464447372
	.quad	11235582092889474
	.quad	22471164185778949
	.quad	44942328371557898
	.quad	89884656743115795
	.space 520
	.section .rdata,"dr"
	.align 32
_ZL10powers_ten:
	.quad	-4671960508600951122
	.quad	-1228264617323800998
	.quad	-7685194413468457480
	.quad	-4994806998408183946
	.quad	-1631822729582842028
	.quad	-7937418233630358124
	.quad	-5310086773610559750
	.quad	-2025922448585811784
	.quad	-8183730558007214221
	.quad	-5617977179081629872
	.quad	-2410785455424649436
	.quad	-8424269937281487754
	.quad	-5918651403174471788
	.quad	-2786628235540701831
	.quad	-8659171674854020500
	.quad	-6212278575140137722
	.quad	-3153662200497784248
	.quad	-8888567902952197011
	.quad	-6499023860262858360
	.quad	-3512093806901185046
	.quad	-9112587656954322510
	.quad	-6779048552765515233
	.quad	-3862124672529506137
	.quad	-215969822234494767
	.quad	-7052510166537641086
	.quad	-4203951689744663453
	.quad	-643253593753441412
	.quad	-7319562523736982739
	.quad	-4537767136243840519
	.quad	-1060522901877412745
	.quad	-7580355841314464822
	.quad	-4863758783215693123
	.quad	-1468012460592228500
	.quad	-7835036815511224669
	.quad	-5182110000961642932
	.quad	-1865951482774665761
	.quad	-8083748704375247956
	.quad	-5492999862041672041
	.quad	-2254563809124702148
	.quad	-8326631408344020698
	.quad	-5796603242002637969
	.quad	-2634068034075909557
	.quad	-8563821548938525329
	.quad	-6093090917745768758
	.quad	-3004677628754823043
	.quad	-8795452545612846258
	.quad	-6382629663588669918
	.quad	-3366601061058449494
	.quad	-9021654690802612790
	.quad	-6665382345075878083
	.quad	-3720041912917459700
	.quad	-38366372719436721
	.quad	-6941508010590729807
	.quad	-4065198994811024354
	.quad	-469812725086392539
	.quad	-7211161980820077193
	.quad	-4402266457597708587
	.quad	-891147053569747830
	.quad	-7474495936122174249
	.quad	-4731433901725329908
	.quad	-1302606358729274481
	.quad	-7731658001846878407
	.quad	-5052886483881210104
	.quad	-1704422086424124726
	.quad	-7982792831656159810
	.quad	-5366805021142811858
	.quad	-2096820258001126919
	.quad	-8228041688891786180
	.quad	-5673366092687344821
	.quad	-2480021597431793123
	.quad	-8467542526035952558
	.quad	-5972742139117552793
	.quad	-2854241655469553087
	.quad	-8701430062309552536
	.quad	-6265101559459552766
	.quad	-3219690930897053053
	.quad	-8929835859451740014
	.quad	-6550608805887287114
	.quad	-3576574988931720988
	.quad	-9152888395723407474
	.quad	-6829424476226871438
	.quad	-3925094576856201393
	.quad	-294682202642863838
	.quad	-7101705404292871755
	.quad	-4265445736938701789
	.quad	-720121152745989333
	.quad	-7367604748107325189
	.quad	-4597819916706768582
	.quad	-1135588877456072824
	.quad	-7627272076051127371
	.quad	-4922404076636521309
	.quad	-1541319077368263733
	.quad	-7880853450996246689
	.quad	-5239380795317920457
	.quad	-1937539975720012667
	.quad	-8128491512466089773
	.quad	-5548928372155224312
	.quad	-2324474446766642487
	.quad	-8370325556870233410
	.quad	-5851220927660403859
	.quad	-2702340141148116919
	.quad	-8606491615858654931
	.quad	-6146428501395930759
	.quad	-3071349608317525545
	.quad	-8837122532839535322
	.quad	-6434717147622031248
	.quad	-3431710416100151156
	.quad	-9062348037703676329
	.quad	-6716249028702207507
	.quad	-3783625267450371479
	.quad	-117845565885576445
	.quad	-6991182506319567134
	.quad	-4127292114472071014
	.quad	-547429124662700863
	.quad	-7259672230555269896
	.quad	-4462904269766699465
	.quad	-966944318780986428
	.quad	-7521869226879198373
	.quad	-4790650515171610063
	.quad	-1376627125537124674
	.quad	-7777920981101784777
	.quad	-5110715207949843068
	.quad	-1776707991509915931
	.quad	-8027971522334779313
	.quad	-5423278384491086237
	.quad	-2167411962186469892
	.quad	-8272161504007625539
	.quad	-5728515861582144019
	.quad	-2548958808550292120
	.quad	-8510628282985014431
	.quad	-6026599335303880135
	.quad	-2921563150702462265
	.quad	-8743505996830120771
	.quad	-6317696477610263060
	.quad	-3285434578585440921
	.quad	-8970925639256982432
	.quad	-6601971030643840136
	.quad	-3640777769877412266
	.quad	-9193015133814464522
	.quad	-6879582898840692748
	.quad	-3987792605123478032
	.quad	-373054737976959636
	.quad	-7150688238876681628
	.quad	-4326674280168464131
	.quad	-796656831783192260
	.quad	-7415439547505577019
	.quad	-4657613415954583369
	.quad	-1210330751515841307
	.quad	-7673985747338482673
	.quad	-4980796165745715437
	.quad	-1614309188754756393
	.quad	-7926472270612804602
	.quad	-5296404319838617848
	.quad	-2008819381370884406
	.quad	-8173041140997884610
	.quad	-5604615407819967858
	.quad	-2394083241347571919
	.quad	-8413831053483314305
	.quad	-5905602798426754977
	.quad	-2770317479606055818
	.quad	-8648977452394866742
	.quad	-6199535797066195524
	.quad	-3137733727905356501
	.quad	-8878612607581929669
	.quad	-6486579741050024182
	.quad	-3496538657885142324
	.quad	-9102865688819295808
	.quad	-6766896092596731856
	.quad	-3846934097318526916
	.quad	-196981603220770741
	.quad	-7040642529654063569
	.quad	-4189117143640191558
	.quad	-624710411122851543
	.quad	-7307973034592864070
	.quad	-4523280274813692184
	.quad	-1042414325089727326
	.quad	-7569037980822161435
	.quad	-4849611457600313890
	.quad	-1450328303573004458
	.quad	-7823984217374209642
	.quad	-5168294253290374149
	.quad	-1848681798185579782
	.quad	-8072955151507069220
	.quad	-5479507920956448621
	.quad	-2237698882768172872
	.quad	-8316090829371189901
	.quad	-5783427518286599472
	.quad	-2617598379430861436
	.quad	-8553528014785370254
	.quad	-6080224000054324913
	.quad	-2988593981640518237
	.quad	-8785400266166405754
	.quad	-6370064314280619289
	.quad	-3350894374423386207
	.quad	-9011838011655698235
	.quad	-6653111496142234890
	.quad	-3704703351750405709
	.quad	-19193171260619232
	.quad	-6929524759678968876
	.quad	-4050219931171323191
	.quad	-451088895536766085
	.quad	-7199459587351560659
	.quad	-4387638465762062920
	.quad	-872862063775190746
	.quad	-7463067817500576072
	.quad	-4717148753448332186
	.quad	-1284749923383027329
	.quad	-7720497729755473936
	.quad	-5038936143766954516
	.quad	-1686984161281305242
	.quad	-7971894128441897632
	.quad	-5353181642124984136
	.quad	-2079791034228842266
	.quad	-8217398424034108272
	.quad	-5660062011615247436
	.quad	-2463391496091671391
	.quad	-8457148712698376476
	.quad	-5959749872445582690
	.quad	-2838001322129590459
	.quad	-8691279853972075893
	.quad	-6252413799037706962
	.quad	-3203831230369745799
	.quad	-8919923546622172980
	.quad	-6538218414850328321
	.quad	-3561087000135522498
	.quad	-9143208402725783417
	.quad	-6817324484979841367
	.quad	-3909969587797413805
	.quad	-275775966319379352
	.quad	-7089889006590693951
	.quad	-4250675239810979535
	.quad	-701658031336336515
	.quad	-7356065297226292178
	.quad	-4583395603105477318
	.quad	-1117558485454458744
	.quad	-7616003081050118571
	.quad	-4908317832885260309
	.quad	-1523711272679187483
	.quad	-7869848573065574033
	.quad	-5225624697904579637
	.quad	-1920344853953336642
	.quad	-8117744561361917257
	.quad	-5535494683275008668
	.quad	-2307682335666372931
	.quad	-8359830487432564938
	.quad	-5838102090863318268
	.quad	-2685941595151759931
	.quad	-8596242524610931813
	.quad	-6133617137336276862
	.quad	-3055335403242958174
	.quad	-8827113654667930715
	.quad	-6422206049907525489
	.quad	-3416071543957018958
	.quad	-9052573742614218704
	.quad	-6704031159840385477
	.quad	-3768352931373093942
	.quad	-98755145788979523
	.quad	-6979250993759194058
	.quad	-4112377723771604668
	.quad	-528786136287117932
	.quad	-7248020362820530563
	.quad	-4448339435098275300
	.quad	-948738275445456221
	.quad	-7510490449794491994
	.quad	-4776427043815727089
	.quad	-1358847786342270957
	.quad	-7766808894105001204
	.quad	-5096825099203863601
	.quad	-1759345355577441597
	.quad	-8017119874876982854
	.quad	-5409713825168840664
	.quad	-2150456263033662926
	.quad	-8261564192037121185
	.quad	-5715269221619013577
	.quad	-2532400508596379067
	.quad	-8500279345513818773
	.quad	-6013663163464885562
	.quad	-2905392935903719049
	.quad	-8733399612580906261
	.quad	-6305063497298744923
	.quad	-3269643353196043249
	.quad	-8961056123388608887
	.quad	-6589634135808373205
	.quad	-3625356651333078602
	.quad	-9183376934724255982
	.quad	-6867535149977932074
	.quad	-3972732919045027188
	.quad	-354230130378896081
	.quad	-7138922859127891907
	.quad	-4311967555482476979
	.quad	-778273425925708320
	.quad	-7403949918844649556
	.quad	-4643251380128424041
	.quad	-1192378206733142147
	.quad	-7662765406849295698
	.quad	-4966770740134231719
	.quad	-1596777406740401744
	.quad	-7915514906853832946
	.quad	-5282707615139903279
	.quad	-1991698500497491194
	.quad	-8162340590452013853
	.quad	-5591239719637629412
	.quad	-2377363631119648861
	.quad	-8403381297090862394
	.quad	-5892540602936190088
	.quad	-2753989735242849706
	.quad	-8638772612167862923
	.quad	-6186779746782440749
	.quad	-3121788665050663032
	.quad	-8868646943297746251
	.quad	-6474122660694794910
	.quad	-3480967307441105734
	.quad	-9093133594791772939
	.quad	-6754730975062328270
	.quad	-3831727700400522434
	.quad	-177973607073265138
	.quad	-7028762532061872568
	.quad	-4174267146649952805
	.quad	-606147914885053103
	.quad	-7296371474444240045
	.quad	-4508778324627912153
	.quad	-1024286887357502287
	.quad	-7557708332239520785
	.quad	-4835449396872013077
	.quad	-1432625727662628443
	.quad	-7812920107430224633
	.quad	-5154464115860392887
	.quad	-1831394126398103205
	.quad	-8062150356639896359
	.quad	-5466001927372482545
	.quad	-2220816390788215277
	.quad	-8305539271883716404
	.quad	-5770238071427257601
	.quad	-2601111570856684097
	.quad	-8543223759426509417
	.quad	-6067343680855748867
	.quad	-2972493582642298180
	.quad	-8775337516792518218
	.quad	-6357485877563259869
	.quad	-3335171328526686932
	.quad	-9002011107970261189
	.quad	-6640827866535438582
	.quad	-3689348814741910323
	.quad	-9223372036854775808
	.quad	-6917529027641081856
	.quad	-4035225266123964416
	.quad	-432345564227567616
	.quad	-7187745005283311616
	.quad	-4372995238176751616
	.quad	-854558029293551616
	.quad	-7451627795949551616
	.quad	-4702848726509551616
	.quad	-1266874889709551616
	.quad	-7709325833709551616
	.quad	-5024971273709551616
	.quad	-1669528073709551616
	.quad	-7960984073709551616
	.quad	-5339544073709551616
	.quad	-2062744073709551616
	.quad	-8206744073709551616
	.quad	-5646744073709551616
	.quad	-2446744073709551616
	.quad	-8446744073709551616
	.quad	-5946744073709551616
	.quad	-2821744073709551616
	.quad	-8681119073709551616
	.quad	-6239712823709551616
	.quad	-3187955011209551616
	.quad	-8910000909647051616
	.quad	-6525815118631426616
	.quad	-3545582879861895366
	.quad	-9133518327554766460
	.quad	-6805211891016070171
	.quad	-3894828845342699809
	.quad	-256850038250986858
	.quad	-7078060301547948642
	.quad	-4235889358507547899
	.quad	-683175679707046969
	.quad	-7344513827457986212
	.quad	-4568956265895094861
	.quad	-1099509313941480672
	.quad	-7604722348854507276
	.quad	-4894216917640746191
	.quad	-1506085128623544835
	.quad	-7858832233030797378
	.quad	-5211854272861108818
	.quad	-1903131822648998119
	.quad	-8106986416796705680
	.quad	-5522047002568494196
	.quad	-2290872734783229841
	.quad	-8349324486880600507
	.quad	-5824969590173362729
	.quad	-2669525969289315508
	.quad	-8585982758446904048
	.quad	-6120792429631242156
	.quad	-3039304518611664792
	.quad	-8817094351773372351
	.quad	-6409681921289327534
	.quad	-3400416383184271514
	.quad	-9042789267131251552
	.quad	-6691800565486676536
	.quad	-3753064688430957766
	.quad	-79644842111309304
	.quad	-6967307053960650171
	.quad	-4097447799023424810
	.quad	-510123730351893108
	.quad	-7236356359111015049
	.quad	-4433759430461380907
	.quad	-930513269649338229
	.quad	-7499099821171918249
	.quad	-4762188758037509908
	.quad	-1341049929119499481
	.quad	-7755685233340769031
	.quad	-5082920523248573385
	.quad	-1741964635633328828
	.quad	-8006256924911912373
	.quad	-5396135137712502563
	.quad	-2133482903713240299
	.quad	-8250955842461857043
	.quad	-5702008784649933400
	.quad	-2515824962385028846
	.quad	-8489919629131724885
	.quad	-6000713517987268202
	.quad	-2889205879056697348
	.quad	-8723282702051517699
	.quad	-6292417359137009219
	.quad	-3253835680493873620
	.quad	-8951176327949752869
	.quad	-6577284391509803182
	.quad	-3609919470959866073
	.quad	-9173728696990998152
	.quad	-6855474852811359786
	.quad	-3957657547586811828
	.quad	-335385916056126881
	.quad	-7127145225176161157
	.quad	-4297245513042813542
	.quad	-759870872876129023
	.quad	-7392448323188662496
	.quad	-4628874385558440215
	.quad	-1174406963520662365
	.quad	-7651533379841495834
	.quad	-4952730706374481889
	.quad	-1579227364540714457
	.quad	-7904546130479028392
	.quad	-5268996644671397586
	.quad	-1974559787411859078
	.quad	-8151628894773493780
	.quad	-5577850100039479321
	.quad	-2360626606621961247
	.quad	-8392920656779807635
	.quad	-5879464802547371640
	.quad	-2737644984756826646
	.quad	-8628557143114098510
	.quad	-6174010410465235233
	.quad	-3105826994654156138
	.quad	-8858670899299929442
	.quad	-6461652605697523898
	.quad	-3465379738694516969
	.quad	-9083391364325154962
	.quad	-6742553186979055798
	.quad	-3816505465296431844
	.quad	-158945813193151901
	.quad	-7016870160886801794
	.quad	-4159401682681114338
	.quad	-587566084924005019
	.quad	-7284757830718584993
	.quad	-4494261269970843337
	.quad	-1006140569036166267
	.quad	-7546366883288685773
	.quad	-4821272585683469312
	.quad	-1414904713676948736
	.quad	-7801844473689174816
	.quad	-5140619573684080616
	.quad	-1814088448677712866
	.quad	-8051334308064652397
	.quad	-5452481866653427593
	.quad	-2203916314889396587
	.quad	-8294976724446954723
	.quad	-5757034887131305500
	.quad	-2584607590486743971
	.quad	-8532908771695296838
	.quad	-6054449946191733143
	.quad	-2956376414312278525
	.quad	-8765264286586255934
	.quad	-6344894339805432013
	.quad	-3319431906329402113
	.quad	-8992173969096958177
	.quad	-6628531442943809817
	.quad	-3673978285252374367
	.quad	-9213765455923815835
	.quad	-6905520801477381890
	.quad	-4020214983419339459
	.quad	-413582710846786419
	.quad	-7176018221920323368
	.quad	-4358336758973016306
	.quad	-836234930288882479
	.quad	-7440175859071633405
	.quad	-4688533805412153852
	.quad	-1248981238337804411
	.quad	-7698142301602209613
	.quad	-5010991858575374112
	.quad	-1652053804791829737
	.quad	-7950062655635975441
	.quad	-5325892301117581398
	.quad	-2045679357969588843
	.quad	-8196078626372074883
	.quad	-5633412264537705700
	.quad	-2430079312244744221
	.quad	-8436328597794046994
	.quad	-5933724728815170838
	.quad	-2805469892591575644
	.quad	-8670947710510816633
	.quad	-6226998619711132888
	.quad	-3172062256211528206
	.quad	-8900067937773286985
	.quad	-6513398903789220827
	.quad	-3530062611309138129
	.quad	-9123818159709293187
	.quad	-6793086681209228580
	.quad	-3879672333084147821
	.quad	-237904397927796872
	.quad	-7066219276345954901
	.quad	-4221088077005055722
	.quad	-664674077828931748
	.quad	-7332950326284164199
	.quad	-4554501889427817344
	.quad	-1081441343357383777
	.quad	-7593429867239446716
	.quad	-4880101315621920491
	.quad	-1488440626100012710
	.quad	-7847804418953589800
	.quad	-5198069505264599346
	.quad	-1885900863153361278
	.quad	-8096217067111932655
	.quad	-5508585315462527915
	.quad	-2274045625900771989
	.quad	-8338807543829064349
	.quad	-5811823411358942533
	.quad	-2653093245771290262
	.quad	-8575712306248138270
	.quad	-6107954364382784933
	.quad	-3023256937051093262
	.quad	-8807064613298015145
	.quad	-6397144748195131027
	.quad	-3384744916816525880
	.quad	-9032994600651410531
	.quad	-6679557232386875260
	.quad	-3737760522056206171
	.quad	-60514634142869810
	.quad	-6955350673980375487
	.quad	-4082502324048081455
	.quad	-491441886632713914
	.quad	-7224680206786528052
	.quad	-4419164240055772162
	.quad	-912269281642327298
	.quad	-7487697328667536417
	.quad	-4747935642407032618
	.quad	-1323233534581402868
	.quad	-7744549986754458648
	.quad	-5069001465015685407
	.quad	-1724565812842218854
	.quad	-7995382660667468640
	.quad	-5382542307406947896
	.quad	-2116491865831296966
	.quad	-8240336443785642460
	.quad	-5688734536304665171
	.quad	-2499232151953443559
	.quad	-8479549122611984080
	.quad	-5987750384837592197
	.quad	-2873001962619602342
	.quad	-8713155254278333320
	.quad	-6279758049420528746
	.quad	-3238011543348273028
	.quad	-8941286242233752498
	.quad	-6564921784364802719
	.quad	-3594466212028615495
	.quad	-9164070410158966540
	.quad	-6843401994271320271
	.quad	-3942566474411762435
	.quad	-316522074587315140
	.quad	-7115355324258153818
	.quad	-4282508136895304369
	.quad	-741449152691742557
	.quad	-7380934748073420954
	.quad	-4614482416664388289
	.quad	-1156417002403097457
	.quad	-7640289654143017767
	.quad	-4938676049251384304
	.quad	-1561659043136842476
	.quad	-7893565929601608404
	.quad	-5255271393574622601
	.quad	-1957403223540890347
	.quad	-8140906042354138323
	.quad	-5564446534515285000
	.quad	-2343872149716718345
	.quad	-8382449121214030822
	.quad	-5866375383090150623
	.quad	-2721283210435300375
	.quad	-8618331034163144591
	.quad	-6161227774276542834
	.quad	-3089848699418290639
	.quad	-8848684464777513505
	.quad	-6449169562544503977
	.quad	-3449775934753242068
	.quad	-9073638986861858148
	.quad	-6730362715149934781
	.quad	-3801267375510030573
	.quad	-139898200960150312
	.quad	-7004965403241175801
	.quad	-4144520735624081847
	.quad	-568964901102714405
	.quad	-7273132090830278359
	.quad	-4479729095110460045
	.quad	-987975350460687152
	.quad	-7535013621679011326
	.quad	-4807081008671376254
	.quad	-1397165242411832413
	.quad	-7790757304148477114
	.quad	-5126760611758208489
	.quad	-1796764746270372707
	.quad	-8040506994060064798
	.quad	-5438947724147693093
	.quad	-2186998636757228463
	.quad	-8284403175614349645
	.quad	-5743817951090549152
	.quad	-2568086420435798537
	.quad	-8522583040413455941
	.quad	-6041542782089432023
	.quad	-2940242459184402124
	.quad	-8755180564631333184
	.quad	-6332289687361778576
	.quad	-3303676090774835316
	.quad	-8982326584375353928
	.quad	-6616222212041804506
	.quad	-3658591746624867729
	.quad	-9204148869281624187
	.quad	-6893500068174642329
	.quad	-4005189066790915007
	.quad	-394800315061255855
	.quad	-7164279224554366766
	.quad	-4343663012265570553
	.quad	-817892746904575287
	.quad	-7428711994456441410
	.quad	-4674203974643163859
	.quad	-1231068949876566920
	.quad	-7686947121313936181
	.quad	-4996997883215032322
	.quad	-1634561335591402499
	.quad	-7939129862385708418
	.quad	-5312226309554747618
	.quad	-2028596868516046619
	.quad	-8185402070463610993
	.quad	-5620066569652125837
	.quad	-2413397193637769392
	.quad	-8425902273664687726
	.quad	-5920691823653471754
	.quad	-2789178761139451788
	.quad	-8660765753353239223
	.quad	-6214271173264161125
	.quad	-3156152948152813503
	.quad	-8890124620236590295
	.quad	-6500969756868349965
	.quad	-3514526177658049552
	.quad	-9114107888677362826
	.quad	-6780948842419315629
	.quad	-3864500034596756632
	.quad	-218939024818557886
	.quad	-7054365918152680535
	.quad	-4206271379263462764
	.quad	-646153205651940551
	.quad	-7321374781173544701
	.quad	-4540032458039542972
	.quad	-1063354554122040811
	.quad	-7582125623967357363
	.quad	-4865971011531808799
	.quad	-1470777745987373095
	.quad	-7836765118883190040
	.quad	-5184270380176599647
	.quad	-1868651956793361654
	.quad	-8085436500636932890
	.quad	-5495109607368778208
	.quad	-2257200990783584856
	.quad	-8328279646880822391
	.quad	-5798663540173640085
	.quad	-2636643406789662202
	.quad	-8565431156884620732
	.quad	-6095102927678388012
	.globl	_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE
	.section	.rdata$_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE,"dr"
	.linkonce same_size
	.align 32
_ZNSt8__detail31__from_chars_alnum_to_val_tableILb0EE5valueE:
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.globl	_ZNSt9__unicode9__v15_1_014__xpicto_edgesE
	.section	.rdata$_ZNSt9__unicode9__v15_1_014__xpicto_edgesE,"dr"
	.linkonce same_size
	.align 32
_ZNSt9__unicode9__v15_1_014__xpicto_edgesE:
	.long	169
	.long	170
	.long	174
	.long	175
	.long	8252
	.long	8253
	.long	8265
	.long	8266
	.long	8482
	.long	8483
	.long	8505
	.long	8506
	.long	8596
	.long	8602
	.long	8617
	.long	8619
	.long	8986
	.long	8988
	.long	9000
	.long	9001
	.long	9096
	.long	9097
	.long	9167
	.long	9168
	.long	9193
	.long	9204
	.long	9208
	.long	9211
	.long	9410
	.long	9411
	.long	9642
	.long	9644
	.long	9654
	.long	9655
	.long	9664
	.long	9665
	.long	9723
	.long	9727
	.long	9728
	.long	9734
	.long	9735
	.long	9747
	.long	9748
	.long	9862
	.long	9872
	.long	9990
	.long	9992
	.long	10003
	.long	10004
	.long	10005
	.long	10006
	.long	10007
	.long	10013
	.long	10014
	.long	10017
	.long	10018
	.long	10024
	.long	10025
	.long	10035
	.long	10037
	.long	10052
	.long	10053
	.long	10055
	.long	10056
	.long	10060
	.long	10061
	.long	10062
	.long	10063
	.long	10067
	.long	10070
	.long	10071
	.long	10072
	.long	10083
	.long	10088
	.long	10133
	.long	10136
	.long	10145
	.long	10146
	.long	10160
	.long	10161
	.long	10175
	.long	10176
	.long	10548
	.long	10550
	.long	11013
	.long	11016
	.long	11035
	.long	11037
	.long	11088
	.long	11089
	.long	11093
	.long	11094
	.long	12336
	.long	12337
	.long	12349
	.long	12350
	.long	12951
	.long	12952
	.long	12953
	.long	12954
	.long	126976
	.long	127232
	.long	127245
	.long	127248
	.long	127279
	.long	127280
	.long	127340
	.long	127346
	.long	127358
	.long	127360
	.long	127374
	.long	127375
	.long	127377
	.long	127387
	.long	127405
	.long	127462
	.long	127489
	.long	127504
	.long	127514
	.long	127515
	.long	127535
	.long	127536
	.long	127538
	.long	127547
	.long	127548
	.long	127552
	.long	127561
	.long	127995
	.long	128000
	.long	128318
	.long	128326
	.long	128592
	.long	128640
	.long	128768
	.long	128884
	.long	128896
	.long	128981
	.long	129024
	.long	129036
	.long	129040
	.long	129096
	.long	129104
	.long	129114
	.long	129120
	.long	129160
	.long	129168
	.long	129198
	.long	129280
	.long	129292
	.long	129339
	.long	129340
	.long	129350
	.long	129351
	.long	129792
	.long	130048
	.long	131070
	.globl	_ZNSt9__unicode9__v15_1_012__incb_edgesE
	.section	.rdata$_ZNSt9__unicode9__v15_1_012__incb_edgesE,"dr"
	.linkonce same_size
	.align 32
_ZNSt9__unicode9__v15_1_012__incb_edgesE:
	.long	3074
	.long	3388
	.long	3394
	.long	3520
	.long	4622
	.long	4640
	.long	5702
	.long	5880
	.long	5886
	.long	5888
	.long	5894
	.long	5900
	.long	5906
	.long	5912
	.long	5918
	.long	5920
	.long	6210
	.long	6252
	.long	6446
	.long	6528
	.long	6594
	.long	6596
	.long	7002
	.long	7028
	.long	7038
	.long	7060
	.long	7070
	.long	7076
	.long	7082
	.long	7096
	.long	7238
	.long	7240
	.long	7362
	.long	7468
	.long	8110
	.long	8144
	.long	8182
	.long	8184
	.long	8282
	.long	8296
	.long	8302
	.long	8336
	.long	8342
	.long	8352
	.long	8358
	.long	8376
	.long	8550
	.long	8560
	.long	8802
	.long	8832
	.long	9002
	.long	9096
	.long	9102
	.long	9216
	.long	9301
	.long	9448
	.long	9458
	.long	9460
	.long	9542
	.long	9556
	.long	9569
	.long	9600
	.long	9697
	.long	9728
	.long	9813
	.long	9892
	.long	9897
	.long	9924
	.long	9929
	.long	9932
	.long	9945
	.long	9960
	.long	9970
	.long	9972
	.long	10097
	.long	10104
	.long	10109
	.long	10112
	.long	10177
	.long	10184
	.long	10234
	.long	10236
	.long	10482
	.long	10484
	.long	10837
	.long	10916
	.long	10921
	.long	10948
	.long	10953
	.long	10960
	.long	10965
	.long	10984
	.long	10994
	.long	10996
	.long	11237
	.long	11240
	.long	11349
	.long	11428
	.long	11433
	.long	11460
	.long	11465
	.long	11472
	.long	11477
	.long	11496
	.long	11506
	.long	11508
	.long	11633
	.long	11640
	.long	11645
	.long	11648
	.long	11717
	.long	11720
	.long	12373
	.long	12452
	.long	12457
	.long	12520
	.long	12530
	.long	12532
	.long	12630
	.long	12636
	.long	12641
	.long	12652
	.long	13042
	.long	13044
	.long	13397
	.long	13550
	.long	13556
	.long	14562
	.long	14572
	.long	14626
	.long	14640
	.long	15074
	.long	15084
	.long	15138
	.long	15152
	.long	15458
	.long	15464
	.long	15574
	.long	15576
	.long	15582
	.long	15584
	.long	15590
	.long	15592
	.long	15814
	.long	15820
	.long	15826
	.long	15828
	.long	15850
	.long	15864
	.long	15874
	.long	15876
	.long	15882
	.long	15892
	.long	15898
	.long	15904
	.long	16154
	.long	16156
	.long	16606
	.long	16608
	.long	16614
	.long	16620
	.long	16950
	.long	16952
	.long	19830
	.long	19840
	.long	23634
	.long	23636
	.long	24394
	.long	24396
	.long	24438
	.long	24440
	.long	25254
	.long	25256
	.long	25830
	.long	25840
	.long	26718
	.long	26724
	.long	27010
	.long	27012
	.long	27094
	.long	27124
	.long	27134
	.long	27136
	.long	27330
	.long	27384
	.long	27390
	.long	27452
	.long	27858
	.long	27860
	.long	28078
	.long	28112
	.long	28334
	.long	28336
	.long	28570
	.long	28572
	.long	28894
	.long	28896
	.long	29506
	.long	29516
	.long	29522
	.long	29572
	.long	29578
	.long	29604
	.long	29622
	.long	29624
	.long	29650
	.long	29652
	.long	29666
	.long	29672
	.long	30466
	.long	30720
	.long	32822
	.long	32824
	.long	33602
	.long	33652
	.long	33670
	.long	33672
	.long	33686
	.long	33732
	.long	46014
	.long	46024
	.long	46590
	.long	46592
	.long	46978
	.long	47104
	.long	49322
	.long	49344
	.long	49766
	.long	49772
	.long	170430
	.long	170432
	.long	170450
	.long	170488
	.long	170618
	.long	170624
	.long	170946
	.long	170952
	.long	172210
	.long	172212
	.long	172930
	.long	173000
	.long	173230
	.long	173240
	.long	173774
	.long	173776
	.long	174786
	.long	174788
	.long	174794
	.long	174804
	.long	174814
	.long	174820
	.long	174842
	.long	174848
	.long	174854
	.long	174856
	.long	175066
	.long	175068
	.long	176054
	.long	176056
	.long	257146
	.long	257148
	.long	260226
	.long	260288
	.long	264182
	.long	264184
	.long	265090
	.long	265092
	.long	265690
	.long	265708
	.long	272438
	.long	272440
	.long	272446
	.long	272448
	.long	272610
	.long	272620
	.long	272638
	.long	272640
	.long	273302
	.long	273308
	.long	275602
	.long	275616
	.long	277166
	.long	277172
	.long	277494
	.long	277504
	.long	277786
	.long	277828
	.long	278026
	.long	278040
	.long	278978
	.long	278980
	.long	279038
	.long	279040
	.long	279274
	.long	279276
	.long	279554
	.long	279564
	.long	279758
	.long	279764
	.long	280014
	.long	280016
	.long	280362
	.long	280364
	.long	280794
	.long	280796
	.long	281510
	.long	281516
	.long	281838
	.long	281844
	.long	282010
	.long	282036
	.long	282050
	.long	282068
	.long	282906
	.long	282908
	.long	283002
	.long	283004
	.long	283406
	.long	283408
	.long	284418
	.long	284420
	.long	285406
	.long	285408
	.long	285870
	.long	285872
	.long	286954
	.long	286956
	.long	287994
	.long	287996
	.long	288014
	.long	288016
	.long	288978
	.long	288980
	.long	289054
	.long	289056
	.long	289382
	.long	289384
	.long	292106
	.long	292108
	.long	292114
	.long	292120
	.long	292446
	.long	292448
	.long	294154
	.long	294156
	.long	371650
	.long	371668
	.long	371906
	.long	371932
	.long	455290
	.long	455292
	.long	476566
	.long	476568
	.long	476574
	.long	476584
	.long	476602
	.long	476620
	.long	476654
	.long	476684
	.long	476694
	.long	476720
	.long	476842
	.long	476856
	.long	477450
	.long	477460
	.long	491522
	.long	491548
	.long	491554
	.long	491620
	.long	491630
	.long	491656
	.long	491662
	.long	491668
	.long	491674
	.long	491692
	.long	492094
	.long	492096
	.long	492738
	.long	492764
	.long	494266
	.long	494268
	.long	494514
	.long	494528
	.long	496562
	.long	496576
	.long	500546
	.long	500572
	.long	501010
	.long	501036
	.globl	_ZNSt9__unicode9__v15_1_011__gcb_edgesE
	.section	.rdata$_ZNSt9__unicode9__v15_1_011__gcb_edgesE,"dr"
	.linkonce same_size
	.align 32
_ZNSt9__unicode9__v15_1_011__gcb_edgesE:
	.long	1
	.long	162
	.long	177
	.long	211
	.long	225
	.long	512
	.long	2033
	.long	2560
	.long	2769
	.long	2784
	.long	12292
	.long	14080
	.long	18484
	.long	18592
	.long	22804
	.long	23520
	.long	23540
	.long	23552
	.long	23572
	.long	23600
	.long	23620
	.long	23648
	.long	23668
	.long	23680
	.long	24581
	.long	24672
	.long	24836
	.long	25008
	.long	25025
	.long	25040
	.long	25780
	.long	26112
	.long	26372
	.long	26384
	.long	28004
	.long	28117
	.long	28128
	.long	28148
	.long	28240
	.long	28276
	.long	28304
	.long	28324
	.long	28384
	.long	28917
	.long	28928
	.long	28948
	.long	28960
	.long	29444
	.long	29872
	.long	31332
	.long	31504
	.long	32436
	.long	32576
	.long	32724
	.long	32736
	.long	33124
	.long	33184
	.long	33204
	.long	33344
	.long	33364
	.long	33408
	.long	33428
	.long	33504
	.long	34196
	.long	34240
	.long	35077
	.long	35104
	.long	35204
	.long	35328
	.long	36004
	.long	36389
	.long	36404
	.long	36918
	.long	36928
	.long	37796
	.long	37814
	.long	37828
	.long	37840
	.long	37862
	.long	37908
	.long	38038
	.long	38100
	.long	38118
	.long	38144
	.long	38164
	.long	38272
	.long	38436
	.long	38464
	.long	38932
	.long	38950
	.long	38976
	.long	39876
	.long	39888
	.long	39908
	.long	39926
	.long	39956
	.long	40016
	.long	40054
	.long	40080
	.long	40118
	.long	40148
	.long	40160
	.long	40308
	.long	40320
	.long	40484
	.long	40512
	.long	40932
	.long	40944
	.long	40980
	.long	41014
	.long	41024
	.long	41924
	.long	41936
	.long	41958
	.long	42004
	.long	42032
	.long	42100
	.long	42128
	.long	42164
	.long	42208
	.long	42260
	.long	42272
	.long	42756
	.long	42784
	.long	42836
	.long	42848
	.long	43028
	.long	43062
	.long	43072
	.long	43972
	.long	43984
	.long	44006
	.long	44052
	.long	44128
	.long	44148
	.long	44182
	.long	44192
	.long	44214
	.long	44244
	.long	44256
	.long	44580
	.long	44608
	.long	44964
	.long	45056
	.long	45076
	.long	45094
	.long	45120
	.long	46020
	.long	46032
	.long	46052
	.long	46086
	.long	46100
	.long	46160
	.long	46198
	.long	46224
	.long	46262
	.long	46292
	.long	46304
	.long	46420
	.long	46464
	.long	46628
	.long	46656
	.long	47140
	.long	47152
	.long	48100
	.long	48118
	.long	48132
	.long	48150
	.long	48176
	.long	48230
	.long	48272
	.long	48294
	.long	48340
	.long	48352
	.long	48500
	.long	48512
	.long	49156
	.long	49174
	.long	49220
	.long	49232
	.long	50116
	.long	50128
	.long	50148
	.long	50198
	.long	50256
	.long	50276
	.long	50320
	.long	50340
	.long	50400
	.long	50516
	.long	50544
	.long	50724
	.long	50752
	.long	51220
	.long	51238
	.long	51264
	.long	52164
	.long	52176
	.long	52198
	.long	52212
	.long	52230
	.long	52260
	.long	52278
	.long	52304
	.long	52324
	.long	52342
	.long	52368
	.long	52390
	.long	52420
	.long	52448
	.long	52564
	.long	52592
	.long	52772
	.long	52800
	.long	53046
	.long	53056
	.long	53252
	.long	53286
	.long	53312
	.long	54196
	.long	54224
	.long	54244
	.long	54262
	.long	54292
	.long	54352
	.long	54374
	.long	54416
	.long	54438
	.long	54484
	.long	54501
	.long	54512
	.long	54644
	.long	54656
	.long	54820
	.long	54848
	.long	55316
	.long	55334
	.long	55360
	.long	56484
	.long	56496
	.long	56564
	.long	56582
	.long	56612
	.long	56656
	.long	56676
	.long	56688
	.long	56710
	.long	56820
	.long	56832
	.long	57126
	.long	57152
	.long	58132
	.long	58144
	.long	58166
	.long	58180
	.long	58288
	.long	58484
	.long	58608
	.long	60180
	.long	60192
	.long	60214
	.long	60228
	.long	60368
	.long	60548
	.long	60656
	.long	61828
	.long	61856
	.long	62292
	.long	62304
	.long	62324
	.long	62336
	.long	62356
	.long	62368
	.long	62438
	.long	62464
	.long	63252
	.long	63478
	.long	63492
	.long	63568
	.long	63588
	.long	63616
	.long	63700
	.long	63872
	.long	63892
	.long	64464
	.long	64612
	.long	64624
	.long	66260
	.long	66326
	.long	66340
	.long	66432
	.long	66452
	.long	66486
	.long	66516
	.long	66544
	.long	66918
	.long	66948
	.long	66976
	.long	67044
	.long	67088
	.long	67348
	.long	67408
	.long	67620
	.long	67632
	.long	67654
	.long	67668
	.long	67696
	.long	67796
	.long	67808
	.long	68052
	.long	68064
	.long	69639
	.long	71176
	.long	72329
	.long	73728
	.long	79316
	.long	79360
	.long	94500
	.long	94550
	.long	94560
	.long	95012
	.long	95046
	.long	95056
	.long	95524
	.long	95552
	.long	96036
	.long	96064
	.long	97092
	.long	97126
	.long	97140
	.long	97254
	.long	97380
	.long	97398
	.long	97428
	.long	97600
	.long	97748
	.long	97760
	.long	98484
	.long	98529
	.long	98548
	.long	98560
	.long	100436
	.long	100464
	.long	101012
	.long	101024
	.long	102916
	.long	102966
	.long	103028
	.long	103062
	.long	103104
	.long	103174
	.long	103204
	.long	103222
	.long	103316
	.long	103360
	.long	106868
	.long	106902
	.long	106932
	.long	106944
	.long	107862
	.long	107876
	.long	107894
	.long	107908
	.long	108016
	.long	108036
	.long	108048
	.long	108068
	.long	108080
	.long	108116
	.long	108246
	.long	108340
	.long	108496
	.long	108532
	.long	108544
	.long	109316
	.long	109808
	.long	110596
	.long	110662
	.long	110672
	.long	111428
	.long	111542
	.long	111556
	.long	111574
	.long	111652
	.long	111670
	.long	111696
	.long	112308
	.long	112448
	.long	112644
	.long	112678
	.long	112688
	.long	113174
	.long	113188
	.long	113254
	.long	113284
	.long	113318
	.long	113332
	.long	113376
	.long	114276
	.long	114294
	.long	114308
	.long	114342
	.long	114388
	.long	114406
	.long	114420
	.long	114470
	.long	114496
	.long	115270
	.long	115396
	.long	115526
	.long	115556
	.long	115584
	.long	118020
	.long	118064
	.long	118084
	.long	118294
	.long	118308
	.long	118416
	.long	118484
	.long	118496
	.long	118596
	.long	118608
	.long	118646
	.long	118660
	.long	118688
	.long	121860
	.long	122880
	.long	131249
	.long	131268
	.long	131290
	.long	131297
	.long	131328
	.long	131713
	.long	131824
	.long	132609
	.long	132864
	.long	134404
	.long	134928
	.long	184052
	.long	184096
	.long	186356
	.long	186368
	.long	187908
	.long	188416
	.long	197284
	.long	197376
	.long	199060
	.long	199088
	.long	681716
	.long	681776
	.long	681796
	.long	681952
	.long	682468
	.long	682496
	.long	683780
	.long	683808
	.long	688164
	.long	688176
	.long	688228
	.long	688240
	.long	688308
	.long	688320
	.long	688694
	.long	688724
	.long	688758
	.long	688768
	.long	688836
	.long	688848
	.long	690182
	.long	690208
	.long	691014
	.long	691268
	.long	691296
	.long	691716
	.long	692000
	.long	692212
	.long	692224
	.long	692836
	.long	692960
	.long	693364
	.long	693542
	.long	693568
	.long	693767
	.long	694224
	.long	694276
	.long	694326
	.long	694336
	.long	695092
	.long	695110
	.long	695140
	.long	695206
	.long	695236
	.long	695270
	.long	695312
	.long	695892
	.long	695904
	.long	696980
	.long	697078
	.long	697108
	.long	697142
	.long	697172
	.long	697200
	.long	697396
	.long	697408
	.long	697540
	.long	697558
	.long	697568
	.long	698308
	.long	698320
	.long	699140
	.long	699152
	.long	699172
	.long	699216
	.long	699252
	.long	699280
	.long	699364
	.long	699392
	.long	699412
	.long	699424
	.long	700086
	.long	700100
	.long	700134
	.long	700160
	.long	700246
	.long	700260
	.long	700272
	.long	704054
	.long	704084
	.long	704102
	.long	704132
	.long	704150
	.long	704176
	.long	704198
	.long	704212
	.long	704224
	.long	704523
	.long	704540
	.long	704971
	.long	704988
	.long	705419
	.long	705436
	.long	705867
	.long	705884
	.long	706315
	.long	706332
	.long	706763
	.long	706780
	.long	707211
	.long	707228
	.long	707659
	.long	707676
	.long	708107
	.long	708124
	.long	708555
	.long	708572
	.long	709003
	.long	709020
	.long	709451
	.long	709468
	.long	709899
	.long	709916
	.long	710347
	.long	710364
	.long	710795
	.long	710812
	.long	711243
	.long	711260
	.long	711691
	.long	711708
	.long	712139
	.long	712156
	.long	712587
	.long	712604
	.long	713035
	.long	713052
	.long	713483
	.long	713500
	.long	713931
	.long	713948
	.long	714379
	.long	714396
	.long	714827
	.long	714844
	.long	715275
	.long	715292
	.long	715723
	.long	715740
	.long	716171
	.long	716188
	.long	716619
	.long	716636
	.long	717067
	.long	717084
	.long	717515
	.long	717532
	.long	717963
	.long	717980
	.long	718411
	.long	718428
	.long	718859
	.long	718876
	.long	719307
	.long	719324
	.long	719755
	.long	719772
	.long	720203
	.long	720220
	.long	720651
	.long	720668
	.long	721099
	.long	721116
	.long	721547
	.long	721564
	.long	721995
	.long	722012
	.long	722443
	.long	722460
	.long	722891
	.long	722908
	.long	723339
	.long	723356
	.long	723787
	.long	723804
	.long	724235
	.long	724252
	.long	724683
	.long	724700
	.long	725131
	.long	725148
	.long	725579
	.long	725596
	.long	726027
	.long	726044
	.long	726475
	.long	726492
	.long	726923
	.long	726940
	.long	727371
	.long	727388
	.long	727819
	.long	727836
	.long	728267
	.long	728284
	.long	728715
	.long	728732
	.long	729163
	.long	729180
	.long	729611
	.long	729628
	.long	730059
	.long	730076
	.long	730507
	.long	730524
	.long	730955
	.long	730972
	.long	731403
	.long	731420
	.long	731851
	.long	731868
	.long	732299
	.long	732316
	.long	732747
	.long	732764
	.long	733195
	.long	733212
	.long	733643
	.long	733660
	.long	734091
	.long	734108
	.long	734539
	.long	734556
	.long	734987
	.long	735004
	.long	735435
	.long	735452
	.long	735883
	.long	735900
	.long	736331
	.long	736348
	.long	736779
	.long	736796
	.long	737227
	.long	737244
	.long	737675
	.long	737692
	.long	738123
	.long	738140
	.long	738571
	.long	738588
	.long	739019
	.long	739036
	.long	739467
	.long	739484
	.long	739915
	.long	739932
	.long	740363
	.long	740380
	.long	740811
	.long	740828
	.long	741259
	.long	741276
	.long	741707
	.long	741724
	.long	742155
	.long	742172
	.long	742603
	.long	742620
	.long	743051
	.long	743068
	.long	743499
	.long	743516
	.long	743947
	.long	743964
	.long	744395
	.long	744412
	.long	744843
	.long	744860
	.long	745291
	.long	745308
	.long	745739
	.long	745756
	.long	746187
	.long	746204
	.long	746635
	.long	746652
	.long	747083
	.long	747100
	.long	747531
	.long	747548
	.long	747979
	.long	747996
	.long	748427
	.long	748444
	.long	748875
	.long	748892
	.long	749323
	.long	749340
	.long	749771
	.long	749788
	.long	750219
	.long	750236
	.long	750667
	.long	750684
	.long	751115
	.long	751132
	.long	751563
	.long	751580
	.long	752011
	.long	752028
	.long	752459
	.long	752476
	.long	752907
	.long	752924
	.long	753355
	.long	753372
	.long	753803
	.long	753820
	.long	754251
	.long	754268
	.long	754699
	.long	754716
	.long	755147
	.long	755164
	.long	755595
	.long	755612
	.long	756043
	.long	756060
	.long	756491
	.long	756508
	.long	756939
	.long	756956
	.long	757387
	.long	757404
	.long	757835
	.long	757852
	.long	758283
	.long	758300
	.long	758731
	.long	758748
	.long	759179
	.long	759196
	.long	759627
	.long	759644
	.long	760075
	.long	760092
	.long	760523
	.long	760540
	.long	760971
	.long	760988
	.long	761419
	.long	761436
	.long	761867
	.long	761884
	.long	762315
	.long	762332
	.long	762763
	.long	762780
	.long	763211
	.long	763228
	.long	763659
	.long	763676
	.long	764107
	.long	764124
	.long	764555
	.long	764572
	.long	765003
	.long	765020
	.long	765451
	.long	765468
	.long	765899
	.long	765916
	.long	766347
	.long	766364
	.long	766795
	.long	766812
	.long	767243
	.long	767260
	.long	767691
	.long	767708
	.long	768139
	.long	768156
	.long	768587
	.long	768604
	.long	769035
	.long	769052
	.long	769483
	.long	769500
	.long	769931
	.long	769948
	.long	770379
	.long	770396
	.long	770827
	.long	770844
	.long	771275
	.long	771292
	.long	771723
	.long	771740
	.long	772171
	.long	772188
	.long	772619
	.long	772636
	.long	773067
	.long	773084
	.long	773515
	.long	773532
	.long	773963
	.long	773980
	.long	774411
	.long	774428
	.long	774859
	.long	774876
	.long	775307
	.long	775324
	.long	775755
	.long	775772
	.long	776203
	.long	776220
	.long	776651
	.long	776668
	.long	777099
	.long	777116
	.long	777547
	.long	777564
	.long	777995
	.long	778012
	.long	778443
	.long	778460
	.long	778891
	.long	778908
	.long	779339
	.long	779356
	.long	779787
	.long	779804
	.long	780235
	.long	780252
	.long	780683
	.long	780700
	.long	781131
	.long	781148
	.long	781579
	.long	781596
	.long	782027
	.long	782044
	.long	782475
	.long	782492
	.long	782923
	.long	782940
	.long	783371
	.long	783388
	.long	783819
	.long	783836
	.long	784267
	.long	784284
	.long	784715
	.long	784732
	.long	785163
	.long	785180
	.long	785611
	.long	785628
	.long	786059
	.long	786076
	.long	786507
	.long	786524
	.long	786955
	.long	786972
	.long	787403
	.long	787420
	.long	787851
	.long	787868
	.long	788299
	.long	788316
	.long	788747
	.long	788764
	.long	789195
	.long	789212
	.long	789643
	.long	789660
	.long	790091
	.long	790108
	.long	790539
	.long	790556
	.long	790987
	.long	791004
	.long	791435
	.long	791452
	.long	791883
	.long	791900
	.long	792331
	.long	792348
	.long	792779
	.long	792796
	.long	793227
	.long	793244
	.long	793675
	.long	793692
	.long	794123
	.long	794140
	.long	794571
	.long	794588
	.long	795019
	.long	795036
	.long	795467
	.long	795484
	.long	795915
	.long	795932
	.long	796363
	.long	796380
	.long	796811
	.long	796828
	.long	797259
	.long	797276
	.long	797707
	.long	797724
	.long	798155
	.long	798172
	.long	798603
	.long	798620
	.long	799051
	.long	799068
	.long	799499
	.long	799516
	.long	799947
	.long	799964
	.long	800395
	.long	800412
	.long	800843
	.long	800860
	.long	801291
	.long	801308
	.long	801739
	.long	801756
	.long	802187
	.long	802204
	.long	802635
	.long	802652
	.long	803083
	.long	803100
	.long	803531
	.long	803548
	.long	803979
	.long	803996
	.long	804427
	.long	804444
	.long	804875
	.long	804892
	.long	805323
	.long	805340
	.long	805771
	.long	805788
	.long	806219
	.long	806236
	.long	806667
	.long	806684
	.long	807115
	.long	807132
	.long	807563
	.long	807580
	.long	808011
	.long	808028
	.long	808459
	.long	808476
	.long	808907
	.long	808924
	.long	809355
	.long	809372
	.long	809803
	.long	809820
	.long	810251
	.long	810268
	.long	810699
	.long	810716
	.long	811147
	.long	811164
	.long	811595
	.long	811612
	.long	812043
	.long	812060
	.long	812491
	.long	812508
	.long	812939
	.long	812956
	.long	813387
	.long	813404
	.long	813835
	.long	813852
	.long	814283
	.long	814300
	.long	814731
	.long	814748
	.long	815179
	.long	815196
	.long	815627
	.long	815644
	.long	816075
	.long	816092
	.long	816523
	.long	816540
	.long	816971
	.long	816988
	.long	817419
	.long	817436
	.long	817867
	.long	817884
	.long	818315
	.long	818332
	.long	818763
	.long	818780
	.long	819211
	.long	819228
	.long	819659
	.long	819676
	.long	820107
	.long	820124
	.long	820555
	.long	820572
	.long	821003
	.long	821020
	.long	821451
	.long	821468
	.long	821899
	.long	821916
	.long	822347
	.long	822364
	.long	822795
	.long	822812
	.long	823243
	.long	823260
	.long	823691
	.long	823708
	.long	824139
	.long	824156
	.long	824587
	.long	824604
	.long	825035
	.long	825052
	.long	825483
	.long	825500
	.long	825931
	.long	825948
	.long	826379
	.long	826396
	.long	826827
	.long	826844
	.long	827275
	.long	827292
	.long	827723
	.long	827740
	.long	828171
	.long	828188
	.long	828619
	.long	828636
	.long	829067
	.long	829084
	.long	829515
	.long	829532
	.long	829963
	.long	829980
	.long	830411
	.long	830428
	.long	830859
	.long	830876
	.long	831307
	.long	831324
	.long	831755
	.long	831772
	.long	832203
	.long	832220
	.long	832651
	.long	832668
	.long	833099
	.long	833116
	.long	833547
	.long	833564
	.long	833995
	.long	834012
	.long	834443
	.long	834460
	.long	834891
	.long	834908
	.long	835339
	.long	835356
	.long	835787
	.long	835804
	.long	836235
	.long	836252
	.long	836683
	.long	836700
	.long	837131
	.long	837148
	.long	837579
	.long	837596
	.long	838027
	.long	838044
	.long	838475
	.long	838492
	.long	838923
	.long	838940
	.long	839371
	.long	839388
	.long	839819
	.long	839836
	.long	840267
	.long	840284
	.long	840715
	.long	840732
	.long	841163
	.long	841180
	.long	841611
	.long	841628
	.long	842059
	.long	842076
	.long	842507
	.long	842524
	.long	842955
	.long	842972
	.long	843403
	.long	843420
	.long	843851
	.long	843868
	.long	844299
	.long	844316
	.long	844747
	.long	844764
	.long	845195
	.long	845212
	.long	845643
	.long	845660
	.long	846091
	.long	846108
	.long	846539
	.long	846556
	.long	846987
	.long	847004
	.long	847435
	.long	847452
	.long	847883
	.long	847900
	.long	848331
	.long	848348
	.long	848779
	.long	848796
	.long	849227
	.long	849244
	.long	849675
	.long	849692
	.long	850123
	.long	850140
	.long	850571
	.long	850588
	.long	851019
	.long	851036
	.long	851467
	.long	851484
	.long	851915
	.long	851932
	.long	852363
	.long	852380
	.long	852811
	.long	852828
	.long	853259
	.long	853276
	.long	853707
	.long	853724
	.long	854155
	.long	854172
	.long	854603
	.long	854620
	.long	855051
	.long	855068
	.long	855499
	.long	855516
	.long	855947
	.long	855964
	.long	856395
	.long	856412
	.long	856843
	.long	856860
	.long	857291
	.long	857308
	.long	857739
	.long	857756
	.long	858187
	.long	858204
	.long	858635
	.long	858652
	.long	859083
	.long	859100
	.long	859531
	.long	859548
	.long	859979
	.long	859996
	.long	860427
	.long	860444
	.long	860875
	.long	860892
	.long	861323
	.long	861340
	.long	861771
	.long	861788
	.long	862219
	.long	862236
	.long	862667
	.long	862684
	.long	863115
	.long	863132
	.long	863563
	.long	863580
	.long	864011
	.long	864028
	.long	864459
	.long	864476
	.long	864907
	.long	864924
	.long	865355
	.long	865372
	.long	865803
	.long	865820
	.long	866251
	.long	866268
	.long	866699
	.long	866716
	.long	867147
	.long	867164
	.long	867595
	.long	867612
	.long	868043
	.long	868060
	.long	868491
	.long	868508
	.long	868939
	.long	868956
	.long	869387
	.long	869404
	.long	869835
	.long	869852
	.long	870283
	.long	870300
	.long	870731
	.long	870748
	.long	871179
	.long	871196
	.long	871627
	.long	871644
	.long	872075
	.long	872092
	.long	872523
	.long	872540
	.long	872971
	.long	872988
	.long	873419
	.long	873436
	.long	873867
	.long	873884
	.long	874315
	.long	874332
	.long	874763
	.long	874780
	.long	875211
	.long	875228
	.long	875659
	.long	875676
	.long	876107
	.long	876124
	.long	876555
	.long	876572
	.long	877003
	.long	877020
	.long	877451
	.long	877468
	.long	877899
	.long	877916
	.long	878347
	.long	878364
	.long	878795
	.long	878812
	.long	879243
	.long	879260
	.long	879691
	.long	879708
	.long	880139
	.long	880156
	.long	880587
	.long	880604
	.long	881035
	.long	881052
	.long	881483
	.long	881500
	.long	881931
	.long	881948
	.long	882379
	.long	882396
	.long	882827
	.long	882844
	.long	883264
	.long	883464
	.long	883824
	.long	883897
	.long	884672
	.long	1028580
	.long	1028592
	.long	1040388
	.long	1040640
	.long	1040900
	.long	1041152
	.long	1044465
	.long	1044480
	.long	1047012
	.long	1047040
	.long	1048321
	.long	1048512
	.long	1056724
	.long	1056736
	.long	1060356
	.long	1060368
	.long	1062756
	.long	1062832
	.long	1089556
	.long	1089600
	.long	1089620
	.long	1089648
	.long	1089732
	.long	1089792
	.long	1090436
	.long	1090480
	.long	1090548
	.long	1090560
	.long	1093204
	.long	1093232
	.long	1102404
	.long	1102464
	.long	1108660
	.long	1108688
	.long	1109972
	.long	1110016
	.long	1111140
	.long	1111312
	.long	1112100
	.long	1112160
	.long	1114118
	.long	1114132
	.long	1114150
	.long	1114160
	.long	1115012
	.long	1115248
	.long	1115908
	.long	1115920
	.long	1115956
	.long	1115984
	.long	1116148
	.long	1116198
	.long	1116208
	.long	1116934
	.long	1116980
	.long	1117046
	.long	1117076
	.long	1117104
	.long	1117141
	.long	1117152
	.long	1117220
	.long	1117232
	.long	1117397
	.long	1117408
	.long	1118212
	.long	1118256
	.long	1118836
	.long	1118918
	.long	1118932
	.long	1119056
	.long	1119318
	.long	1119344
	.long	1120052
	.long	1120064
	.long	1120260
	.long	1120294
	.long	1120304
	.long	1121078
	.long	1121124
	.long	1121270
	.long	1121296
	.long	1121317
	.long	1121344
	.long	1121428
	.long	1121488
	.long	1121510
	.long	1121524
	.long	1121536
	.long	1123014
	.long	1123060
	.long	1123110
	.long	1123140
	.long	1123158
	.long	1123172
	.long	1123200
	.long	1123300
	.long	1123312
	.long	1123348
	.long	1123360
	.long	1125876
	.long	1125894
	.long	1125940
	.long	1126064
	.long	1126404
	.long	1126438
	.long	1126464
	.long	1127348
	.long	1127376
	.long	1127396
	.long	1127414
	.long	1127428
	.long	1127446
	.long	1127504
	.long	1127542
	.long	1127568
	.long	1127606
	.long	1127648
	.long	1127796
	.long	1127808
	.long	1127974
	.long	1128000
	.long	1128036
	.long	1128144
	.long	1128196
	.long	1128272
	.long	1131350
	.long	1131396
	.long	1131526
	.long	1131556
	.long	1131606
	.long	1131620
	.long	1131632
	.long	1132004
	.long	1132016
	.long	1133316
	.long	1133334
	.long	1133364
	.long	1133462
	.long	1133476
	.long	1133494
	.long	1133524
	.long	1133542
	.long	1133556
	.long	1133590
	.long	1133604
	.long	1133632
	.long	1137396
	.long	1137414
	.long	1137444
	.long	1137504
	.long	1137542
	.long	1137604
	.long	1137638
	.long	1137652
	.long	1137680
	.long	1138116
	.long	1138144
	.long	1139462
	.long	1139508
	.long	1139638
	.long	1139668
	.long	1139686
	.long	1139700
	.long	1139728
	.long	1141428
	.long	1141446
	.long	1141460
	.long	1141478
	.long	1141508
	.long	1141606
	.long	1141620
	.long	1141632
	.long	1143252
	.long	1143296
	.long	1143332
	.long	1143398
	.long	1143412
	.long	1143488
	.long	1147590
	.long	1147636
	.long	1147782
	.long	1147796
	.long	1147824
	.long	1151748
	.long	1151766
	.long	1151840
	.long	1151862
	.long	1151888
	.long	1151924
	.long	1151958
	.long	1151972
	.long	1151989
	.long	1152006
	.long	1152021
	.long	1152038
	.long	1152052
	.long	1152064
	.long	1154326
	.long	1154372
	.long	1154432
	.long	1154468
	.long	1154502
	.long	1154564
	.long	1154576
	.long	1154630
	.long	1154640
	.long	1155092
	.long	1155248
	.long	1155892
	.long	1155990
	.long	1156005
	.long	1156020
	.long	1156080
	.long	1156212
	.long	1156224
	.long	1156372
	.long	1156470
	.long	1156500
	.long	1156544
	.long	1157189
	.long	1157284
	.long	1157494
	.long	1157508
	.long	1157536
	.long	1164022
	.long	1164036
	.long	1164144
	.long	1164164
	.long	1164262
	.long	1164276
	.long	1164288
	.long	1165604
	.long	1165952
	.long	1165974
	.long	1165988
	.long	1166102
	.long	1166116
	.long	1166150
	.long	1166164
	.long	1166192
	.long	1168148
	.long	1168240
	.long	1168292
	.long	1168304
	.long	1168324
	.long	1168352
	.long	1168372
	.long	1168485
	.long	1168500
	.long	1168512
	.long	1169574
	.long	1169648
	.long	1169668
	.long	1169696
	.long	1169718
	.long	1169748
	.long	1169766
	.long	1169780
	.long	1169792
	.long	1175348
	.long	1175382
	.long	1175408
	.long	1175556
	.long	1175589
	.long	1175606
	.long	1175616
	.long	1176390
	.long	1176420
	.long	1176496
	.long	1176550
	.long	1176580
	.long	1176598
	.long	1176612
	.long	1176624
	.long	1262337
	.long	1262596
	.long	1262608
	.long	1262708
	.long	1262944
	.long	1486596
	.long	1486672
	.long	1487620
	.long	1487728
	.long	1504500
	.long	1504512
	.long	1504534
	.long	1505408
	.long	1505524
	.long	1505584
	.long	1506884
	.long	1506896
	.long	1507078
	.long	1507104
	.long	1821140
	.long	1821168
	.long	1821185
	.long	1821248
	.long	1896452
	.long	1897184
	.long	1897220
	.long	1897584
	.long	1906260
	.long	1906278
	.long	1906292
	.long	1906336
	.long	1906390
	.long	1906404
	.long	1906481
	.long	1906612
	.long	1906736
	.long	1906772
	.long	1906880
	.long	1907364
	.long	1907424
	.long	1909796
	.long	1909840
	.long	1941508
	.long	1942384
	.long	1942452
	.long	1943248
	.long	1943380
	.long	1943392
	.long	1943620
	.long	1943632
	.long	1943988
	.long	1944064
	.long	1944084
	.long	1944320
	.long	1966084
	.long	1966192
	.long	1966212
	.long	1966480
	.long	1966516
	.long	1966624
	.long	1966644
	.long	1966672
	.long	1966692
	.long	1966768
	.long	1968372
	.long	1968384
	.long	1970948
	.long	1971056
	.long	1977060
	.long	1977072
	.long	1978052
	.long	1978112
	.long	1986244
	.long	1986304
	.long	2002180
	.long	2002288
	.long	2004036
	.long	2004144
	.long	2039405
	.long	2039808
	.long	2047924
	.long	2048000
	.long	14680065
	.long	14680580
	.long	14682113
	.long	14684164
	.long	14688001
	.long	14745600
	.globl	_ZNSt9__unicode9__v15_1_013__width_edgesE
	.section	.rdata$_ZNSt9__unicode9__v15_1_013__width_edgesE,"dr"
	.linkonce same_size
	.align 32
_ZNSt9__unicode9__v15_1_013__width_edgesE:
	.long	4352
	.long	4448
	.long	8986
	.long	8988
	.long	9001
	.long	9003
	.long	9193
	.long	9197
	.long	9200
	.long	9201
	.long	9203
	.long	9204
	.long	9725
	.long	9727
	.long	9748
	.long	9750
	.long	9800
	.long	9812
	.long	9855
	.long	9856
	.long	9875
	.long	9876
	.long	9889
	.long	9890
	.long	9898
	.long	9900
	.long	9917
	.long	9919
	.long	9924
	.long	9926
	.long	9934
	.long	9935
	.long	9940
	.long	9941
	.long	9962
	.long	9963
	.long	9970
	.long	9972
	.long	9973
	.long	9974
	.long	9978
	.long	9979
	.long	9981
	.long	9982
	.long	9989
	.long	9990
	.long	9994
	.long	9996
	.long	10024
	.long	10025
	.long	10060
	.long	10061
	.long	10062
	.long	10063
	.long	10067
	.long	10070
	.long	10071
	.long	10072
	.long	10133
	.long	10136
	.long	10160
	.long	10161
	.long	10175
	.long	10176
	.long	11035
	.long	11037
	.long	11088
	.long	11089
	.long	11093
	.long	11094
	.long	11904
	.long	11930
	.long	11931
	.long	12020
	.long	12032
	.long	12246
	.long	12272
	.long	12351
	.long	12353
	.long	12439
	.long	12441
	.long	12544
	.long	12549
	.long	12592
	.long	12593
	.long	12687
	.long	12688
	.long	12772
	.long	12783
	.long	12831
	.long	12832
	.long	12872
	.long	12880
	.long	42125
	.long	42128
	.long	42183
	.long	43360
	.long	43389
	.long	44032
	.long	55204
	.long	63744
	.long	64256
	.long	65040
	.long	65050
	.long	65072
	.long	65107
	.long	65108
	.long	65127
	.long	65128
	.long	65132
	.long	65281
	.long	65377
	.long	65504
	.long	65511
	.long	94176
	.long	94181
	.long	94192
	.long	94194
	.long	94208
	.long	100344
	.long	100352
	.long	101590
	.long	101632
	.long	101641
	.long	110576
	.long	110580
	.long	110581
	.long	110588
	.long	110589
	.long	110591
	.long	110592
	.long	110883
	.long	110898
	.long	110899
	.long	110928
	.long	110931
	.long	110933
	.long	110934
	.long	110948
	.long	110952
	.long	110960
	.long	111356
	.long	126980
	.long	126981
	.long	127183
	.long	127184
	.long	127374
	.long	127375
	.long	127377
	.long	127387
	.long	127488
	.long	127491
	.long	127504
	.long	127548
	.long	127552
	.long	127561
	.long	127568
	.long	127570
	.long	127584
	.long	127590
	.long	127744
	.long	128592
	.long	128640
	.long	128710
	.long	128716
	.long	128717
	.long	128720
	.long	128723
	.long	128725
	.long	128728
	.long	128732
	.long	128736
	.long	128747
	.long	128749
	.long	128756
	.long	128765
	.long	128992
	.long	129004
	.long	129008
	.long	129009
	.long	129280
	.long	129536
	.long	129648
	.long	129661
	.long	129664
	.long	129673
	.long	129680
	.long	129726
	.long	129727
	.long	129734
	.long	129742
	.long	129756
	.long	129760
	.long	129769
	.long	129776
	.long	129785
	.long	131072
	.long	196606
	.long	196608
	.long	262142
	.section .rdata,"dr"
	.align 8
.LC8:
	.long	-17155601
	.long	1072049730
	.align 8
.LC9:
	.long	1061284247
	.long	1083176841
	.align 8
.LC10:
	.long	0
	.long	1093664768
	.align 8
.LC12:
	.long	0
	.long	1127743488
	.align 16
.LC13:
	.long	-1
	.long	2147483647
	.long	0
	.long	0
	.align 8
.LC14:
	.long	1352628735
	.long	1070810131
	.set	.LC15,.LC97+56
	.align 8
.LC16:
	.long	0
	.long	1080016896
	.set	.LC17,.LC32
	.set	.LC18,.LC32+24
	.align 8
.LC19:
	.long	16
	.long	-52
	.set	.LC20,.LC40+4
	.align 64
.LC21:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.long	15
	.align 64
.LC24:
	.quad	3688503277381496880
	.quad	3976738051646829616
	.quad	3544667369688283184
	.quad	3832902143785906737
	.quad	4121136918051239473
	.quad	3689066235924983858
	.quad	3977301010190316594
	.quad	3545230328231770162
	.align 64
.LC25:
	.quad	3833465102329393715
	.quad	4121699876594726451
	.quad	3689629194468470836
	.quad	3977863968733803572
	.quad	3545793286775257140
	.quad	3834028060872880693
	.quad	4122262835138213429
	.quad	3690192153011957814
	.align 64
.LC26:
	.quad	3978426927277290550
	.quad	3546356245318744118
	.quad	3834591019416367671
	.quad	4122825793681700407
	.quad	3690755111555444792
	.quad	3978989885820777528
	.quad	3546919203862231096
	.quad	3835153977959854649
	.align 64
.LC32:
	.long	0
	.long	1073741824
	.long	0
	.long	1074266112
	.long	0
	.long	1074790400
	.long	0
	.long	1075052544
	.long	0
	.long	1075314688
	.long	0
	.long	1075576832
	.long	0
	.long	1075838976
	.long	0
	.long	1075970048
	.align 64
.LC33:
	.long	0
	.long	1076101120
	.long	0
	.long	1076232192
	.long	0
	.long	1076363264
	.long	0
	.long	1076494336
	.long	0
	.long	1076625408
	.long	0
	.long	1076756480
	.long	0
	.long	1076887552
	.long	0
	.long	1076953088
	.align 64
.LC34:
	.long	0
	.long	1077018624
	.long	0
	.long	1077084160
	.long	0
	.long	1077149696
	.long	0
	.long	1077215232
	.long	0
	.long	1077280768
	.long	0
	.long	1077346304
	.long	0
	.long	1077411840
	.long	0
	.long	1077477376
	.align 64
.LC35:
	.long	0
	.long	1077542912
	.long	0
	.long	1077608448
	.long	0
	.long	1077673984
	.long	0
	.long	1077739520
	.long	0
	.long	1077805056
	.long	0
	.long	1077870592
	.long	0
	.long	1077936128
	.long	0
	.long	1077968896
	.set	.LC36,.LC32
	.align 64
.LC39:
	.quad	12
	.quad	12
	.quad	13
	.quad	13
	.quad	13
	.quad	13
	.quad	14
	.quad	14
	.align 64
.LC40:
	.quad	-51
	.quad	-51
	.quad	-50
	.quad	-50
	.quad	-50
	.quad	-50
	.quad	-49
	.quad	-49
	.set	.LC41,.LC33
	.align 64
.LC42:
	.quad	14
	.quad	14
	.quad	14
	.quad	14
	.quad	14
	.quad	14
	.quad	15
	.quad	15
	.align 64
.LC43:
	.quad	-49
	.quad	-49
	.quad	-49
	.quad	-49
	.quad	-49
	.quad	-49
	.quad	-48
	.quad	-48
	.set	.LC44,.LC34
	.set	.LC47,.LC35
	.align 64
.LC48:
	.quad	15
	.quad	15
	.quad	15
	.quad	15
	.quad	15
	.quad	15
	.quad	16
	.quad	16
	.align 64
.LC49:
	.quad	-48
	.quad	-48
	.quad	-48
	.quad	-48
	.quad	-48
	.quad	-48
	.quad	-47
	.quad	-47
	.set	.LC54,.LC95+56
	.align 64
.LC57:
	.quad	1
	.quad	2
	.quad	3
	.quad	4
	.quad	5
	.quad	6
	.quad	7
	.quad	8
	.set	.LC65,.LC95
	.align 64
.LC68:
	.long	0
	.long	23
	.long	46
	.long	69
	.long	92
	.long	115
	.long	138
	.long	161
	.long	184
	.long	207
	.long	230
	.long	253
	.long	276
	.long	299
	.long	322
	.long	345
	.align 64
.LC69:
	.long	368
	.long	391
	.long	414
	.long	437
	.long	460
	.long	483
	.long	506
	.long	529
	.long	552
	.long	575
	.long	598
	.long	621
	.long	644
	.long	667
	.long	690
	.long	713
	.align 8
.LC72:
	.long	0
	.long	1093567616
	.align 8
.LC73:
	.long	0
	.long	1067450368
	.align 64
.LC84:
	.quad	-8070450532247928832
	.quad	-7493989779944505344
	.quad	-6917529027641081856
	.quad	-6341068275337658368
	.quad	-5764607523034234880
	.quad	-5188146770730811392
	.quad	-4611686018427387904
	.quad	-4035225266123964416
	.align 64
.LC95:
	.long	-1747416644
	.long	1016910514
	.long	-2036257893
	.long	1023837339
	.long	-2127697391
	.long	1030854553
	.long	-640172613
	.long	1037794527
	.long	-500134854
	.long	1044740494
	.long	-1598689907
	.long	1051772663
	.long	-350469331
	.long	1058682594
	.long	1202590843
	.long	1065646817
	.align 64
.LC96:
	.quad	10000000000000000
	.quad	100000000000000
	.quad	1000000000000
	.quad	10000000000
	.quad	100000000
	.quad	1000000
	.quad	10000
	.quad	100
	.align 64
.LC97:
	.long	-2036257893
	.long	1023837339
	.long	-2127697391
	.long	1030854553
	.long	-640172613
	.long	1037794527
	.long	-500134854
	.long	1044740494
	.long	-1598689907
	.long	1051772663
	.long	-350469331
	.long	1058682594
	.long	1202590843
	.long	1065646817
	.long	0
	.long	1072693248
	.align 8
.LC98:
	.long	0
	.long	1097011920
	.align 8
.LC101:
	.long	742442509
	.long	3275288
	.align 8
.LC105:
	.long	350224384
	.long	1106250694
	.align 8
.LC106:
	.long	710614365
	.long	1102299338
	.align 64
.LC109:
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.quad	-5403634167711393303
	.align 8
.LC121:
	.quad	.LC119
	.align 8
.LC128:
	.long	-2147483648
	.long	-2147483648
	.align 8
.LC129:
	.long	2147483647
	.long	2147483647
	.align 8
.LC130:
	.long	1
	.long	1
	.align 8
.LC131:
	.long	-1727483681
	.long	-1727483681
	.align 8
.LC136:
	.long	-1
	.long	1072693247
	.align 8
.LC137:
	.long	0
	.long	1106247680
	.align 8
.LC138:
	.long	0
	.long	1005584384
	.align 16
.LC147:
	.long	-1
	.long	-1
	.long	32766
	.long	0
	.align 8
.LC152:
	.long	-1
	.long	2146435071
	.align 16
.LC153:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.align 4
.LC154:
	.long	2139095039
	.align 8
.LC158:
	.quad	_ZTVNSt8__format9_Seq_sinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE+16
	.align 8
.LC160:
	.long	-2048145248
	.long	2145504499
	.align 8
.LC161:
	.long	269967078
	.long	996030752
	.align 8
.LC162:
	.long	210911779
	.long	1002937505
	.set	.LC163,.LC95+16
	.align 8
.LC164:
	.long	-774749268
	.long	1009939037
	.set	.LC165,.LC95+8
	.align 8
.LC172:
	.long	0
	.long	1104006501
	.set	.LC175,.LC57+8
	.align 8
.LC176:
	.long	-1629006314
	.long	1020396463
	.align 8
.LC198:
	.long	0
	.long	1093664766
	.align 8
.LC202:
	.long	0
	.long	1051721728
	.def	__udivti3;	.scl	2;	.type	32;	.endef
	.def	__main;	.scl	2;	.type	32;	.endef
	.def	__gxx_personality_seh0;	.scl	2;	.type	32;	.endef
	.ident	"GCC: (GNU) 14.2.0"
	.def	_ZdlPvy;	.scl	2;	.type	32;	.endef
	.def	_ZNSt13runtime_errorD2Ev;	.scl	2;	.type	32;	.endef
	.def	__mingw_vsprintf;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
	.def	memmove;	.scl	2;	.type	32;	.endef
	.def	memset;	.scl	2;	.type	32;	.endef
	.def	_Znwy;	.scl	2;	.type	32;	.endef
	.def	_ZSt17__throw_bad_allocv;	.scl	2;	.type	32;	.endef
	.def	_ZSt20__throw_length_errorPKc;	.scl	2;	.type	32;	.endef
	.def	__mingw_vfprintf;	.scl	2;	.type	32;	.endef
	.def	__cxa_allocate_exception;	.scl	2;	.type	32;	.endef
	.def	_ZNSt13runtime_errorC2EPKc;	.scl	2;	.type	32;	.endef
	.def	__cxa_throw;	.scl	2;	.type	32;	.endef
	.def	__cxa_free_exception;	.scl	2;	.type	32;	.endef
	.def	_Unwind_Resume;	.scl	2;	.type	32;	.endef
	.def	pow;	.scl	2;	.type	32;	.endef
	.def	__mingw_vsnprintf;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6localeC1ERKS_;	.scl	2;	.type	32;	.endef
	.def	_ZNKSt6locale2id5_M_idEv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6localeD1Ev;	.scl	2;	.type	32;	.endef
	.def	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_x;	.scl	2;	.type	32;	.endef
	.def	_ZNKSt5ctypeIcE13_M_widen_initEv;	.scl	2;	.type	32;	.endef
	.def	_ZSt16__throw_bad_castv;	.scl	2;	.type	32;	.endef
	.def	_ZNSolsEi;	.scl	2;	.type	32;	.endef
	.def	_ZNSo3putEc;	.scl	2;	.type	32;	.endef
	.def	_ZNSt13random_device7_M_initERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE;	.scl	2;	.type	32;	.endef
	.def	_ZNSt13random_device9_M_getvalEv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6chrono3_V212system_clock3nowEv;	.scl	2;	.type	32;	.endef
	.def	_ZNSt13random_device7_M_finiEv;	.scl	2;	.type	32;	.endef
	.def	_Znay;	.scl	2;	.type	32;	.endef
	.def	strlen;	.scl	2;	.type	32;	.endef
	.def	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE15_M_replace_coldEPcyPKcyy;	.scl	2;	.type	32;	.endef
	.def	_ZNSt8ios_baseC2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6localeC1Ev;	.scl	2;	.type	32;	.endef
	.def	_ZNSo9_M_insertIdEERSoT_;	.scl	2;	.type	32;	.endef
	.def	_ZNSt8ios_baseD2Ev;	.scl	2;	.type	32;	.endef
	.def	_ZNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEED1Ev;	.scl	2;	.type	32;	.endef
	.def	__cxa_guard_acquire;	.scl	2;	.type	32;	.endef
	.def	__cxa_guard_release;	.scl	2;	.type	32;	.endef
	.def	_ZNKSt6locale4nameB5cxx11Ev;	.scl	2;	.type	32;	.endef
	.def	toupper;	.scl	2;	.type	32;	.endef
	.def	_ZNSt6locale7classicEv;	.scl	2;	.type	32;	.endef
	.def	_ZNKSt6localeeqERKS_;	.scl	2;	.type	32;	.endef
	.def	memchr;	.scl	2;	.type	32;	.endef
	.def	_ZSt24__throw_out_of_range_fmtPKcz;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_e;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_eSt12chars_formati;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_eSt12chars_format;	.scl	2;	.type	32;	.endef
	.def	frexpl;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_d;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_dSt12chars_formati;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_dSt12chars_format;	.scl	2;	.type	32;	.endef
	.def	frexp;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_f;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_fSt12chars_formati;	.scl	2;	.type	32;	.endef
	.def	_ZSt8to_charsPcS_fSt12chars_format;	.scl	2;	.type	32;	.endef
	.def	frexpf;	.scl	2;	.type	32;	.endef
	.def	memcmp;	.scl	2;	.type	32;	.endef
	.def	abort;	.scl	2;	.type	32;	.endef
	.def	_ZNSo5flushEv;	.scl	2;	.type	32;	.endef
	.def	__mingw_strtod;	.scl	2;	.type	32;	.endef
	.def	_ZSt20__throw_out_of_rangePKc;	.scl	2;	.type	32;	.endef
	.def	_ZSt24__throw_invalid_argumentPKc;	.scl	2;	.type	32;	.endef
	.def	atexit;	.scl	2;	.type	32;	.endef
	.def	_ZNKSt13runtime_error4whatEv;	.scl	2;	.type	32;	.endef
	.section	.rdata$.refptr._ZNSt7__cxx118numpunctIcE2idE, "dr"
	.globl	.refptr._ZNSt7__cxx118numpunctIcE2idE
	.linkonce	discard
.refptr._ZNSt7__cxx118numpunctIcE2idE:
	.quad	_ZNSt7__cxx118numpunctIcE2idE
	.section	.rdata$.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE, "dr"
	.globl	.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE
	.linkonce	discard
.refptr._ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE
	.section	.rdata$.refptr._ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE, "dr"
	.globl	.refptr._ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.linkonce	discard
.refptr._ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE:
	.quad	_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.section	.rdata$.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE, "dr"
	.globl	.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE
	.linkonce	discard
.refptr._ZTVSt9basic_iosIcSt11char_traitsIcEE:
	.quad	_ZTVSt9basic_iosIcSt11char_traitsIcEE
	.section	.rdata$.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE, "dr"
	.globl	.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE
	.linkonce	discard
.refptr._ZTVSt15basic_streambufIcSt11char_traitsIcEE:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE
	.section	.rdata$.refptr._ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE, "dr"
	.globl	.refptr._ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.linkonce	discard
.refptr._ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE
	.section	.rdata$.refptr._ZSt4cout, "dr"
	.globl	.refptr._ZSt4cout
	.linkonce	discard
.refptr._ZSt4cout:
	.quad	_ZSt4cout
	.section	.rdata$.refptr._ZNSt5ctypeIcE2idE, "dr"
	.globl	.refptr._ZNSt5ctypeIcE2idE
	.linkonce	discard
.refptr._ZNSt5ctypeIcE2idE:
	.quad	_ZNSt5ctypeIcE2idE
