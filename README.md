# doubleToSci

#### 介绍
高性能浮点数转科学计数法算法设计c++代码。
单个double值可在10-12ns完成转换，采用avx512指令，并行处理多个double值，单个double值的转换时间约为6到7ns。

相比  **sprintf(char_ptr,"%.16le",num)** 快10-20倍左右。
相比  **std::format("{:.16e}",num)**   快10倍左右。

API如下：

``` int d2sci(double value,char* buffer) ```

输入参数说明：

1. value:  要转换为科学计数法的浮点数

2. buffer:  是要写的字符数组指针，推荐预留32字节可写。否则可能出现未知错误。

返回值说明:
					一个int值，写入的长度

#### 编译教程,需要CPU支持avx512指令集

使用了std::format库，请使用支持c++20的编译器

```g++ -O3 -std=c++20 -mfma -mavx512f -march=native main.cpp -o main```

如果cpu不支持avx,avx2,avx512f等simd指令集:

```g++ -O3 -std=c++20 -mfma -march=native main.cpp -o main```

#### 使用说明

运行
```./main```


### 备注